﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.WebServices.DTO
{
    public class ColegiadoConselheirosAtivosDTO
    {
        public string CdColegiado { get; set; }
        public string NoColegiado { get; set; }
        public int QtdConselheiroAtivo { get; set; }
    }
}
