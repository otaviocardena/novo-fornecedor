﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.WebServices.DTO
{
    public class AfastamentosDTO
    {
        public DateTime DtInicioAfastamento { get; set; }
        public DateTime DtFimAfastamento { get; set; }
    }
} 