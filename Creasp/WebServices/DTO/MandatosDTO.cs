﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.WebServices.DTO
{
    public class MandatosDTO
    {
        public string Regiao { get; set; }
        public char MandatoAtivo { get; set; }
        public string Cargo { get; set; }
        public DateTime DtInicioMandato { get; set; }
        public DateTime DtFimMandato { get; set; }
        public string Colegiado { get; set; }
        public string Entidade { get; set; }
    }
}
