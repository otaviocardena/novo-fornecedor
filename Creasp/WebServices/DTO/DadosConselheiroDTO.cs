﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.WebServices.DTO
{
    public class DadosConselheiroDTO
    {
        public string Nome { get; set; }
        public string Creasp { get; set; }
        public string CPF { get; set; }
        public string Regiao { get; set; }
        public string MandatoAtivo { get; set; }
        public string Cargo { get; set; }
        public DateTime DtInicioMandato { get; set; }
        public DateTime DtFimMandato { get; set; }
        public string CdColegiado { get; set; }
        public string Colegiado { get; set; }
        public string Entidade { get; set; }
        public DateTime? DtInicioAfastamento { get; set; }
        public DateTime? DtFimAfastamento { get; set; }        
    }
}
