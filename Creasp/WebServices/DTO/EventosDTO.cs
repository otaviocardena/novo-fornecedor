﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.WebServices.DTO
{
    public class EventosDTO
    {
        public DateTime DtEvento { get; set; }
        public string LocalEvento { get; set; }
        public string Evento { get; set; }

        public List<TitSupDTO> Membros { get; set; } = new List<TitSupDTO>();
    }


    
}
