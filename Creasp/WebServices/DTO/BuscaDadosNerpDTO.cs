﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.WebServices
{
    public class BuscaDadosNerpContratoDTO
    {
        public int NrNerp { get; set; }

        public decimal ValorNerp { get; set; }
        public decimal ValorFaeasp { get; set; }
        public DateTime? DtPagto { get; set; }
    }

    public class BuscaDadosNerpConvenioDTO
    {
        public int NrNerp { get; set; }

        public decimal ValorNerp { get; set; }
        public decimal ValorFaeasp { get; set; }
        public DateTime? DtPagto { get; set; }
    }
}
