﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.WebServices.DTO
{
    public class TitSupDTO
    {
        public DadosConselheiroDTO Titular { get; set; }
        public List<Afastamento> LicencasTitular { get; set; } = new List<Afastamento>();

        public DadosConselheiroDTO Suplente { get; set; }        
        public List<Afastamento> LicencasSuplente { get; set; } = new List<Afastamento>();
    }
    public class Afastamento
    {
        public DateTime DtInicioAfastamento { get; set; }
        public DateTime DtFimAfastamento { get; set; }
    }
}
