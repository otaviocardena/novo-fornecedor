﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.WebServices.DTO
{
    public class CargoMandatoLicencaDTO
    {
        public int IdFuncionalidade { get; set; }   // 0->Composição de Plenário ; 1->Inspetores
        public string Creasp { get; set; }
        public DateTime DtIniMandato { get; set; }
        public DateTime DtFimMandato { get; set; }
        public string SituacaoMandato { get; set; } //ATIVO ou LICENCIADO
        public DateTime? DtIniLicenca { get; set; }
        public DateTime? DtFimLicenca { get; set; }
        public string DsCargo { get; set; }
        public int? TpEntidade { get; set; }
        public string CdEntidade { get; set; }
        public string NomeEntidade { get; set; }
        public string CreaspTitularDoSuplente { get; set; }
    }

    public enum SituacaoMandato
    {
        ATIVO,
        LICENCIADO
    };
}
