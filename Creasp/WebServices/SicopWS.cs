﻿using System;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Creasp.Core;
using Creasp.Nerp;
using Creasp.WebServices.DTO;
using Creasp.Siplen;
using System.Collections.Generic;
using NPoco.Linq;
using NBox.Core;

namespace Creasp.WebServices
{
    public class SicopWS : System.Web.Services.WebService
    {
        private CreaspContext db = new CreaspContext();

        /// <summary>
        /// Retornar as informações dos mandatos atuais bem como as respectivas licenças, 
        /// de todos profissionais com registro no CREA-SP que possuem cadastro de Conselheiros e Inspetores no sistema SIPLEN.
        /// </summary>
        [WebMethod]
        public List<CargoMandatoLicencaDTO> ListaCargosMandatosLicenca()
        {
            List<CargoMandatoLicencaDTO> dtos = new List<CargoMandatoLicencaDTO>();

            var hoje = DateTime.Now.Date;

            var mandatos = db.Query<Mandato>()
                .Include(x => x.Pessoa)
                .Include(x => x.Pessoa.Cep)
                .Include(x => x.Cargo)
                .Include(x => x.Entidade)
                .Where(x => x.TpMandato == TpMandato.Conselheiro || x.TpMandato == TpMandato.Inspetor)
                .Where(x => x.DtIni <= hoje && hoje <= x.DtFim)
                .OrderBy(x => x.Pessoa.NoPessoa)
                .ToList()
                .ForEach((x, i) => x.Licencas = db.Query<Licenca>()
                                    .Where(l => l.DtIni <= hoje && hoje <= l.DtFim)
                                    .Where(l => l.IdPessoa == x.IdPessoa)
                                    .ToList());

            var titulares = mandatos.Where(x => x.TpMandato == TpMandato.Conselheiro && x.IdMandatoTit == null)
                .Select(x => new CargoMandatoLicencaDTO()
                {
                    Creasp = x.Pessoa.NrCreasp,
                    DsCargo = x.Cargo.NoCargo,
                    DtIniMandato = x.DtIni,
                    DtFimMandato = x.DtFim,
                    IdFuncionalidade = 0,
                    CdEntidade = x.Entidade?.CdExterno,
                    NomeEntidade = x.Entidade?.NoEntidade,
                    TpEntidade = x.Entidade?.TpEntidade == TpEntidade.InstituicaoEnsino ? 1 : 2, // 2 => Entidade de Classe
                    DtIniLicenca = x.Licencas.Where(l => l.DtIni <= hoje && hoje <= l.DtFim).FirstOrDefault()?.DtIni,
                    DtFimLicenca = x.Licencas.Where(l => l.DtIni <= hoje && hoje <= l.DtFim).FirstOrDefault()?.DtFim,
                    SituacaoMandato = x.Licencas.Where(l => l.DtIni <= hoje && hoje <= l.DtFim).FirstOrDefault() == null ? SituacaoMandato.ATIVO.ToString() : SituacaoMandato.LICENCIADO.ToString()

                })
                .ToList();

            var suplentes = mandatos.Where(x => x.TpMandato == TpMandato.Conselheiro && x.IdMandatoTit != null)
                .Select(x => new CargoMandatoLicencaDTO()
                {
                    Creasp = x.Pessoa.NrCreasp,
                    DsCargo = x.Cargo.NoCargo,
                    DtIniMandato = x.DtIni,
                    DtFimMandato = x.DtFim,
                    IdFuncionalidade = 0,
                    CdEntidade = x.Entidade?.CdExterno,
                    NomeEntidade = x.Entidade?.NoEntidade,
                    TpEntidade = x.Entidade?.TpEntidade == TpEntidade.InstituicaoEnsino ? 1 : 2, // 2 => Entidade de Classe
                    DtIniLicenca = x.Licencas.Where(l => l.DtIni <= hoje && hoje <= l.DtFim).FirstOrDefault()?.DtIni,
                    DtFimLicenca = x.Licencas.Where(l => l.DtIni <= hoje && hoje <= l.DtFim).FirstOrDefault()?.DtFim,
                    SituacaoMandato = x.Licencas.Where(l => l.DtIni <= hoje && hoje <= l.DtFim).FirstOrDefault() == null ? SituacaoMandato.ATIVO.ToString() : SituacaoMandato.LICENCIADO.ToString(),
                    CreaspTitularDoSuplente = db.Query<Mandato>()
                                                .Include(y => y.Pessoa)
                                                .Where(m => m.IdMandato == x.IdMandatoTit).FirstOrDefault().Pessoa.NrCreasp
                    
                })
                .ToList();

            List<CargoMandatoLicencaDTO> inspetoria = new List<CargoMandatoLicencaDTO>();

            inspetoria = mandatos.Where(x => x.TpMandato == TpMandato.Inspetor)
                .Select(x => new CargoMandatoLicencaDTO()
                {
                    Creasp = x.Pessoa.NrCreasp,
                    DsCargo = x.Cargo.NoCargo,
                    DtIniMandato = x.DtIni,
                    DtFimMandato = x.DtFim,
                    IdFuncionalidade = 1,
                    CdEntidade = x.Inspetoria?.Cep.NrCep,
                    NomeEntidade = x.Inspetoria?.Cep.NoCidade,
                    TpEntidade = 3,
                    DtIniLicenca = x.Licencas.Where(l => l.DtIni <= hoje && hoje <= l.DtFim).FirstOrDefault()?.DtIni,
                    DtFimLicenca = x.Licencas.Where(l => l.DtIni <= hoje && hoje <= l.DtFim).FirstOrDefault()?.DtFim,
                    SituacaoMandato = x.Licencas.Where(l => l.DtIni <= hoje && hoje <= l.DtFim).FirstOrDefault() == null ? SituacaoMandato.ATIVO.ToString() : SituacaoMandato.LICENCIADO.ToString()
                })
                .ToList();

            dtos.AddRange(titulares);
            dtos.AddRange(suplentes);
            dtos.AddRange(inspetoria);
            return dtos;

        }

        /// <summary>
        ///Para cada colegiado retornar Conselheiro/membro e  Conselheiro suplente/membro suplente 
        ///e respectivas informações de mandatos atuais e licenças quando houver.
        /// </summary>
        [WebMethod]
        public List<TitSupDTO> DadosConselheiros(string nome, string creasp, string cpf, string cdColegiado, string entidade, string cargo)
        {
            var periodo = new Periodo { Fim = DateTime.Today, Inicio = DateTime.Today };
            List<TitSupDTO> dtos = new List<TitSupDTO>();

            //PLENARIO
            ConselheiroService conselheiroService = new ConselheiroService(db);
            var titularSuplentePlenario = conselheiroService.ListaPlenario(periodo, null).ToList();

            foreach (var item in titularSuplentePlenario)
            {
                dtos.Add(TitularSuplentePlenario(periodo, item.IdMandatoTit, item.Titular.IdPessoa, item.IdMandatoSup, item.Suplente?.IdPessoa));
            }

            //Comissões
            ComissaoService comissaoService = new ComissaoService(db);
            var comissoes = comissaoService.ListaComissoes(periodo, null).ToList();

            List<TitularSuplenteDTO> TitularSuplenteComissao = new List<TitularSuplenteDTO>();
            foreach (var comissao in comissoes)
            {
                TitularSuplenteComissao.AddRange(comissaoService.ListaMembrosSemCargosFaltantes(comissao.IdComissao, periodo));
            }
            foreach (var item in TitularSuplenteComissao)
            {
                dtos.Add(TitularSuplentePlenario(periodo, item.IdMandatoTit, item.Titular.IdPessoa, item.IdMandatoSup, item.Suplente?.IdPessoa));
            }

            //Grupo de Trabalho
            GrupoService grupoService = new GrupoService(db);
            var grupos = grupoService.ListaGrupos(periodo, null).ToList();

            List<Mandato> TitularSuplenteGrupo = new List<Mandato>();
            foreach (var grupo in grupos)
            {
                TitularSuplenteGrupo.AddRange(grupoService.ListaMembrosSemCargosFaltantes(grupo.IdGrupo));
            }
            foreach (var item in TitularSuplenteGrupo)
            {
                dtos.Add(TitularSuplentePlenario(periodo, item.IdMandato, item.IdPessoa, null, null));
            }

            //Câmara

            CamaraService camaraService = new CamaraService(db);
            CamaraRepService camaraRepService = new CamaraRepService(db);
            List<Mandato> TitularSuplenteCamara = new List<Mandato>();

            var camaras = camaraService.ListaCamaras(periodo).ToList();

            foreach (var camara in camaras)
            {
                TitularSuplenteCamara.AddRange(camaraRepService.ListaConselheiros(periodo, camara.CdCamara));
            }
            foreach (var item in TitularSuplenteCamara)
            {
                dtos.Add(TitularSuplentePlenario(periodo, item.IdMandato, item.IdPessoa, null, null));
            }

            //Diretoria            
            DiretoriaService diretoriaService = new DiretoriaService(db);
            var mandatosDiretores = diretoriaService.ListaDiretores(periodo,false).ToList();

            foreach (var item in mandatosDiretores)
            {
                dtos.Add(TitularSuplentePlenario(periodo, item.IdMandato, item.IdPessoa, null, null));
            }

            return dtos
                .Where(x => x.Titular.Nome.StartsWith(nome))
                .Where(x => x.Titular.Creasp.StartsWith(creasp))
                .Where(x => x.Titular.CPF.StartsWith(cpf))
                .Where(x => x.Titular.CdColegiado.StartsWith(cdColegiado))
                .Where(x => x.Titular.Entidade.StartsWith(entidade))
                .Where(x => x.Titular.Cargo.StartsWith(cargo))
                .ToList();
        }

        /// <summary>
        /// Retorna a quantidade de conselheiros ativos
        /// Se informado colegiado, retorna a quantidade de conselheiros ativos no mesmo.
        /// </summary>
        [WebMethod]
        public List<ColegiadoConselheirosAtivosDTO> QtdeConselheirosAtivos(string cdColegiado)
        {
            var hoje = DateTime.Today;
            var periodo = new Periodo { Fim = hoje, Inicio = hoje };

            List<ColegiadoConselheirosAtivosDTO> dtosPlenarioColegiados = new List<ColegiadoConselheirosAtivosDTO>();

            ConselheiroService conselheiroService = new ConselheiroService(db);

            dtosPlenarioColegiados.Add(new ColegiadoConselheirosAtivosDTO()
            {
                CdColegiado = TpMandato.Conselheiro,
                NoColegiado = "PLENÁRIO",
                QtdConselheiroAtivo = conselheiroService.ListaPlenario(periodo, null).Count()
            });

            CamaraRepService camaraService = new CamaraRepService(db);

            dtosPlenarioColegiados.Add(new ColegiadoConselheirosAtivosDTO()
            {
                CdColegiado = TpMandato.Camara,
                NoColegiado = "CÂMARA",
                QtdConselheiroAtivo = camaraService.ListaConselheiros(periodo, null).ToList().Count
            });


            //Colegiado Inspetorias
            var mandatosInspetor = db.Query<Mandato>()
                .WherePeriodo(periodo)
                .Where(x => x.TpMandato == TpMandato.Inspetor)
                .ToList();

            dtosPlenarioColegiados.Add(new ColegiadoConselheirosAtivosDTO()
            {
                CdColegiado = TpMandato.Inspetor,
                NoColegiado = "INSPETORIA",
                QtdConselheiroAtivo = mandatosInspetor.Count
            });

            ComissaoService comissaoService = new ComissaoService(db);

            dtosPlenarioColegiados.Add(new ColegiadoConselheirosAtivosDTO()
            {
                CdColegiado = TpMandato.Comissao,
                NoColegiado = "COMISSÃO",
                QtdConselheiroAtivo = comissaoService.QuantidadeDeMandatos(periodo)
            });

            GrupoService grupoService = new GrupoService(db);
            dtosPlenarioColegiados.Add(new ColegiadoConselheirosAtivosDTO()
            {
                CdColegiado = TpMandato.Grupo,
                NoColegiado = "GRUPO DE TRABALHO",
                QtdConselheiroAtivo = grupoService.QuantidadeDeMandatos(periodo)
            });

            return dtosPlenarioColegiados.Where(x => x.CdColegiado.StartsWith(cdColegiado)).ToList();

        }

        /// <summary>
        ///Consulta de dados coordenadores, Adjunto e Ad-hoc Câmaras / Comissões
        /// </summary>
        [WebMethod]
        public List<DadosConselheiroDTO> CamarasComissoes(string Cdcolegiado)
        {
            var periodo = new Periodo { Fim = DateTime.Today, Inicio = DateTime.Today };

            var lic = db.Query<Licenca>()
               .WherePeriodo(periodo)
               .OrderBy(x => x.DtIni)
               .ToList();

            var mandatos = db.Query<Mandato>()
                .Include(x => x.Pessoa)
                .Include(x => x.Pessoa.Cep)
                .Include(x => x.Cargo)
                .Include(x => x.Camara)
                .Include(x => x.Comissao)
                .Where(x => x.CdCargo == CdCargo.Coordenador || x.CdCargo == CdCargo.Adjunto)
                .Where(x => x.TpMandato == TpMandato.Camara || x.TpMandato == TpMandato.Comissao)
                .WherePeriodo(periodo)
                .OrderByDescending(x => x.CdCargo)
                .ToList()
                .ForEach((x, i) => x.Licencas = lic.Where(a => a.IdPessoa == x.IdPessoa)
                .ToList());

            if (!Cdcolegiado.Equals(string.Empty))
            {
                mandatos = mandatos.Where(x => x.TpMandato == Cdcolegiado).ToList();
            }

            List<DadosConselheiroDTO> coordenadores = new List<DadosConselheiroDTO>();
            coordenadores = mandatos
                            .Select(x => new DadosConselheiroDTO()
                            {
                                Nome = x.Pessoa.NoPessoa,
                                Creasp = x.Pessoa.NrCreasp,
                                CPF = x.Pessoa.NrCpf,
                                MandatoAtivo = x.Licencas.Count == 0 ? "S" : "N",
                                Cargo = x.Cargo.NoCargo,
                                DtInicioMandato = x.DtIni,
                                DtFimMandato = x.DtFim,
                                Colegiado = x.TpMandato == TpMandato.Camara ? "Câmara" : "Comissão",
                                Entidade = x.TpMandato == TpMandato.Camara ? x.Camara.NoCamara : x.Comissao.NoComissao,
                                DtInicioAfastamento = x.Licencas.FirstOrDefault()?.DtIni,
                                DtFimAfastamento = x.Licencas.FirstOrDefault()?.DtFim
                            })
                            .OrderBy(x => x.Colegiado)
                            .ThenBy(x => x.Entidade)
                            .ThenByDescending(x => x.Cargo)
                            .ToList();
            return coordenadores;
        }

        /// <summary>
        /// Consulta de dados de presidente, vice-presidente, diretores e substitutos para coordenar Plenário.
        /// </summary>
        [WebMethod]
        public List<DadosConselheiroDTO> Plenario()
        {
            List<DadosConselheiroDTO> diretores = new List<DadosConselheiroDTO>();
            var periodo = new Periodo { Fim = DateTime.Today, Inicio = DateTime.Today };

            var licencas = db.Query<Licenca>()
                .WherePeriodo(periodo)
                .OrderBy(x => x.DtIni)
                .ToList();

            diretores = db.Query<Mandato>()
                .Include(x => x.Pessoa)
                .Include(x => x.Cargo)
                .Where(x => x.TpMandato == TpMandato.Diretor)
                .WherePeriodo(periodo)
                .Where(x => x.CdCargo == CdCargo.Presidente || x.CdCargo == "VICE-PRESIDENTE" || x.CdCargo == "DIR-ADM" || x.CdCargo == "DIR-TEC" || x.CdCargo == "DIR-ADM-ADJ")
                .OrderBy(x => x.Cargo.NrOrdem)
                .ToList()
                .ForEach((x, i) => x.Licencas = licencas.Where(a => a.IdPessoa == x.IdPessoa).ToList())
                .Select(x => new DadosConselheiroDTO
                {
                    Nome = x.Pessoa.NoPessoa,
                    Creasp = x.Pessoa.NrCreasp,
                    CPF = x.Pessoa.NrCpf,
                    Regiao = db.Query<Unidade>()
                        .Include(u => u.Cep)
                        .Where(u => u.Cep == x.Pessoa.Cep)
                        .Where(u => u.TpUnidade == TpUnidade.UGI || u.TpUnidade == TpUnidade.UOP)
                        .FirstOrDefault()?.NoUnidade,
                    MandatoAtivo = x.Licencas.Count == 0 ? "S" : "N",
                    Cargo = x.Cargo.NoCargo,
                    DtInicioMandato = x.DtIni,
                    DtFimMandato = x.DtFim,
                    Colegiado = "Diretoria",
                    DtInicioAfastamento = x.Licencas.FirstOrDefault()?.DtIni,
                    DtFimAfastamento = x.Licencas.FirstOrDefault()?.DtFim
                })
                .ToList();

            return diretores;
        }

        /// <summary>
        ///Consulta de dados de eventos
        /// </summary>
        [WebMethod]
        public List<EventosDTO> Eventos(DateTime DtIni, DateTime DtFim)
        {

            var periodo = new Periodo { Fim = DtFim, Inicio = DtIni };

            var lic = db.Query<Licenca>()
               .WherePeriodo(periodo)
               .OrderBy(x => x.DtIni)
               .ToList();


            List<EventosDTO> eventosDtos = new List<EventosDTO>();
            EventoService eventoService = new EventoService(db);
            ConselheiroService conselheiroService = new ConselheiroService(db);

            var eventos = eventoService.ListaEventos(periodo, null, null).ForEach((x, i) => x.Participantes = eventoService.ListaParticipantes(x.IdEvento, false, TpPessoa.Conselheiro));

            foreach (var evento in eventos)
            {
                var eventoDto = new EventosDTO()
                {
                    DtEvento = evento.DtEvento,
                    Evento = evento.NoEvento,
                    LocalEvento = evento.NoLocal
                };
                foreach (var participante in evento.Participantes)
                {
                    TitSupDTO membros = new TitSupDTO();
                    var mandatoTit = db.Query<Mandato>()
                                    .Where(x => x.IdPessoa == participante.IdPessoaTit)
                                    .WherePeriodo(periodo)
                                    .FirstOrDefault();
                    mandatoTit.Licencas = lic.Where(a => a.IdPessoa == mandatoTit.IdPessoa).ToList();

                    membros.Titular = new DadosConselheiroDTO()
                    {
                        Nome = participante.Titular.NoPessoa,
                        Creasp = participante.Titular.NrCreasp,
                        CPF = participante.Titular.NrCpf,
                        Regiao = db.Query<Unidade>()
                                .Include(u => u.Cep)
                                .Where(u => u.Cep.NrCep == participante.Titular.NrCep)
                                .Where(u => u.TpUnidade == TpUnidade.UGI || u.TpUnidade == TpUnidade.UOP)
                                .FirstOrDefault()?.NoUnidade,
                        MandatoAtivo = mandatoTit.Licencas.Count == 0 ? "S" : "N",
                        Cargo = mandatoTit?.CdCargo,
                        DtInicioMandato = mandatoTit.DtIni,
                        DtFimMandato = mandatoTit.DtFim,
                        Colegiado = mandatoTit.TpMandato == TpMandato.Camara ? "Câmara" :
                                    mandatoTit.TpMandato == TpMandato.Comissao ? "Comissão" :
                                    mandatoTit.TpMandato == TpMandato.Grupo ? "Grupo de Trabalho" :
                                    mandatoTit.TpMandato == TpMandato.Conselheiro ? "Plenário" :
                                    mandatoTit.TpMandato == TpMandato.Diretor ? "Diretoria" : "Outro",
                        Entidade = mandatoTit.IdEntidade != null ? mandatoTit.Entidade?.NoEntidade :
                                    mandatoTit.IdComissao != null ? mandatoTit.Comissao?.NoComissao :
                                    mandatoTit.IdGrupo != null ? mandatoTit.Grupo?.NoGrupo : mandatoTit.Camara?.NoCamara,
                        DtInicioAfastamento = mandatoTit.Licencas.FirstOrDefault()?.DtIni,
                        DtFimAfastamento = mandatoTit.Licencas.FirstOrDefault()?.DtFim
                    };

                    var mandatoSup = db.Query<Mandato>()
                                    .Where(x => x.IdPessoa == participante.IdPessoaSup)
                                    .WherePeriodo(periodo)
                                    .FirstOrDefault();
                    if (mandatoSup != null)
                    {
                        mandatoSup.Licencas = lic.Where(a => a.IdPessoa == mandatoSup?.IdPessoa).ToList();
                        membros.Suplente = new DadosConselheiroDTO()
                        {
                            Nome = participante.Suplente.NoPessoa,
                            Creasp = participante.Suplente.NrCreasp,
                            CPF = participante.Suplente.NrCpf,
                            Regiao = db.Query<Unidade>()
                                         .Include(u => u.Cep)
                                         .Where(u => u.Cep.NrCep == participante.Suplente.NrCep)
                                         .Where(u => u.TpUnidade == TpUnidade.UGI || u.TpUnidade == TpUnidade.UOP)
                                         .FirstOrDefault()?.NoUnidade,
                            MandatoAtivo = mandatoSup?.Licencas.Count == 0 ? "S" : "N",
                            Cargo = mandatoSup?.CdCargo,
                            DtInicioMandato = mandatoSup.DtIni,
                            DtFimMandato = mandatoSup.DtFim,
                            Colegiado = mandatoSup?.TpMandato == TpMandato.Camara ? "Câmara" :
                                    mandatoSup.TpMandato == TpMandato.Comissao ? "Comissão" :
                                    mandatoSup.TpMandato == TpMandato.Grupo ? "Grupo de Trabalho" :
                                    mandatoSup.TpMandato == TpMandato.Conselheiro ? "Plenário" :
                                    mandatoSup.TpMandato == TpMandato.Diretor ? "Diretoria" : "Outro",
                            Entidade = mandatoSup.IdEntidade.HasValue ? mandatoSup.Entidade?.NoEntidade :
                                    mandatoSup.IdComissao != null ? mandatoSup.Comissao?.NoComissao :
                                    mandatoSup.IdGrupo != null ? mandatoSup.Grupo?.NoGrupo : mandatoSup.Camara?.NoCamara,
                            DtInicioAfastamento = mandatoSup?.Licencas.FirstOrDefault()?.DtIni,
                            DtFimAfastamento = mandatoSup?.Licencas.FirstOrDefault()?.DtFim
                        };
                    }

                    eventoDto.Membros.Add(membros);
                }

                eventosDtos.Add(eventoDto);
            };

            return eventosDtos;
        }

        [WebMethod]
        public List<Comissao> ListaComissoes()
        {
            var periodo = new Periodo { Fim = DateTime.Today, Inicio = DateTime.Today };

            ComissaoService comissaoService = new ComissaoService(db);

            return comissaoService.ListaComissoes(periodo, null).ToList();
        }

        [WebMethod]
        public List<Grupo> ListaGrupos()
        {
            var periodo = new Periodo { Fim = DateTime.Today, Inicio = DateTime.Today };

            GrupoService grupoService = new GrupoService(db);

            return grupoService.ListaGrupos(periodo, null).ToList();
        }

        [WebMethod]
        public List<Camara> ListaCamaras()
        {
            var periodo = new Periodo { Fim = DateTime.Today, Inicio = DateTime.Today };

            CamaraService camaraService = new CamaraService(db);

            return camaraService.ListaCamaras(periodo).ToList();
        }

        private TitSupDTO TitularSuplentePlenario(Periodo periodo, int? idMandatoTit, int idPessoaTit, int? idMandatoSup, int? idPessoaSup)
        {

            TitSupDTO membro = new TitSupDTO();

            //TITULAR
            if (idMandatoTit.HasValue)
            {
                membro.Titular = MontaDadosConselheiroDTO(idMandatoTit);
                membro.LicencasTitular = db.Query<Licenca>()
                .Where(x => x.IdPessoa == idPessoaTit)
                .Where(x => x.DtFim <= membro.Titular.DtFimMandato && membro.Titular.DtInicioMandato <= x.DtIni)
                .ToList()
                .Select(x => new Afastamento
                {
                    DtFimAfastamento = x.DtFim,
                    DtInicioAfastamento = x.DtIni
                }).ToList();
            }

            //SUPLENTE
            if (idMandatoSup.HasValue)
            {
                //TITULAR
                membro.Suplente = MontaDadosConselheiroDTO(idMandatoSup);
                membro.LicencasSuplente = db.Query<Licenca>()
                    .Where(x => x.IdPessoa == idPessoaSup)
                    .Where(x => x.DtFim <= membro.Suplente.DtFimMandato && membro.Suplente.DtInicioMandato <= x.DtIni)
                    .ToList()
                    .Select(x => new Afastamento
                    {
                        DtFimAfastamento = x.DtFim,
                        DtInicioAfastamento = x.DtIni
                    }).ToList();
            }

            return membro;

        }

        private DadosConselheiroDTO MontaDadosConselheiroDTO(int? IdMandato)
        {
            DadosConselheiroDTO dadoConselheiro = new DadosConselheiroDTO();

            if (IdMandato.HasValue)
            {
                Mandato mandato = new Mandato();

                mandato = db.Query<Mandato>()
                    .Include(x => x.Cargo)
                    .Include(x => x.Pessoa)
                    .Include(x => x.Pessoa.Cep)
                    .Include(x => x.Camara)
                    .Include(x => x.Comissao)
                    .Include(x => x.Grupo)
                    .Where(x => x.IdMandato == IdMandato)
                    .ToList()
                    .ForEach((x, i) => x.Licencas = db.Query<Licenca>()
                                                    .Where(l => l.DtFim <= x.DtFim && x.DtIni <= l.DtIni)
                                                    .Where(l => l.IdPessoa == x.IdPessoa)
                                                    .ToList())
                    .FirstOrDefault();

                if (mandato != null)
                {
                    string colegiado;
                    string entidade;
                    switch (mandato.TpMandato)
                    {
                        case TpMandato.Camara:
                            colegiado = "CÂMARA";
                            entidade = mandato.Camara?.NoCamara;
                            break;
                        case TpMandato.Conselheiro:
                            colegiado = "PLENÁRIO";
                            entidade = mandato.Camara?.NoCamara;
                            break;
                        case TpMandato.Comissao:
                            colegiado = "COMISSÃO";
                            entidade = mandato.Comissao?.NoComissao;
                            break;
                        case TpMandato.Diretor:
                            colegiado = "DIRETORIA";
                            entidade = "DIRETORIA";
                            break;
                        default:
                            colegiado = "OUTRO";
                            entidade = "OUTRO";
                            break;
                    }

                    dadoConselheiro.Cargo = mandato.Cargo.NoCargo;
                    dadoConselheiro.CdColegiado = mandato.TpMandato;
                    dadoConselheiro.Colegiado = colegiado;
                    dadoConselheiro.CPF = mandato.Pessoa.NrCpf;
                    dadoConselheiro.Creasp = mandato.Pessoa.NrCreasp;
                    dadoConselheiro.DtFimMandato = mandato.DtFim;
                    dadoConselheiro.DtInicioMandato = mandato.DtIni;
                    dadoConselheiro.Entidade = entidade;
                    dadoConselheiro.MandatoAtivo = mandato.Licencas.Where(x => DateTime.Today <= x.DtFim && x.DtIni <= DateTime.Today).ToList().Count == 0 ? "S" : "N";
                    dadoConselheiro.Nome = mandato.Pessoa.NoPessoa;
                }
            }

            return dadoConselheiro;
        }

        //[WebMethod]
        //public BuscaDadosNerpContratoDTO BuscaDadosNerpContrato(int ano, int mes, int codEntidade, string nrProcesso)
        //{
        //    if (ano < 2015 || ano > 2050) throw new ArgumentException("Campo ano inválido");
        //    if (mes < 1 || mes > 12) throw new ArgumentException("O campo mês deve ser entre 1 e 12");
        //    if (string.IsNullOrWhiteSpace(nrProcesso)) throw new ArgumentNullException("O campo número de processo é obrigatório");

        //    var item = db.Query<NerpItem>()
        //        .Where(x => x.Nerp.NrAno == ano)
        //        .Where(x => x.Nerp.NrMesRef == mes)
        //        .FirstOrDefault();


        //    return null;
        //}

        //[WebMethod]
        //public BuscaDadosNerpConvenioDTO BuscaDadosNerpConvenio(int ano, int mes, int codEntidade)
        //{
        //    if (ano < 2015 || ano > 2050) throw new ArgumentException("Campo ano inválido");
        //    if (mes < 1 || mes > 12) throw new ArgumentException("O campo mês deve ser entre 1 e 12");


        //    return null;
        //}
    }
}
