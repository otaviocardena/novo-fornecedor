﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Creasp.Core;
using NBox.Core;
using NBox.Web.Controls;

namespace Creasp.Siplen
{
    /// <summary>
    /// Relatorio DW para mandato de conselheiro
    /// </summary>
    public class DWConselheiroDataSource : DWDataSource<Mandato>
    {
        public override string Nome => "Conselheiro";
        public override string Descricao => "Dados de mandato de conselheiro aonde titular e suplente estão em linhas distintas";
        public override bool PorPeriodo => true;

        public override void InicializaCampos()
        {
            Add("Mandato")
                .Coluna((c) => c.AnoIni + " - " + c.AnoFim)
                .Filtro((c, v) => c.AnoIni == v.To<int>())
                .Combo((db, p) =>
                {
                    var list = new List<ListItem>();
                    for (var i = DateTime.Today.Year + 1; i >= 2000; i--)
                    {
                        list.Add(new ListItem(string.Format("{0} - {1}", i, i + 2), i.ToString()));
                    }
                    return list;
                });

            Add("Mandato ano inicio")
                .Filtro((c, v) => c.AnoIni == v.To<int>())
                .Mask(MaskType.Custom, "R9999");

            Add("Mandato ano fim")
                .Filtro((c, v) => c.AnoFim == v.To<int>())
                .Mask(MaskType.Custom, "R9999");

            Add("Câmara")
                .Coluna((c) => c.Camara.NoCamara)
                .Filtro((c, v) => c.Camara.CdCamara == v.To<int>())
                .Combo((db, p) => db.Camaras.ListaCamaras(p).Select(k => new ListItem { Text = k.NoCamara, Value = k.CdCamara.ToString() }));

            Add("Entidade")
                .Coluna((c) => c.Entidade.NoEntidade)
                .Filtro((c, v) => c.Entidade.IdEntidade == v.To<int>())
                .Combo((db, p) => db.Entidades.ListaEntidades().Select(k => new ListItem { Text = k.NoEntidade, Value = k.IdEntidade.ToString() }));

            Add("Tipo entidade")
                .Coluna((c) => c.Entidade.TpEntidade == "I" ? "Instituição de Ensino" : "Entidade de classe");

            Add("CNPJ Entidade")
                .Coluna((c) => c.Entidade.NrCnpj.Mask("99.999.999/9999-99"))
                .Mask(MaskType.Custom, "99.999.999/9999-99");

            Add("Nome do conselheiro")
                .Coluna((c) => c.Pessoa.NoPessoa)
                .Filtro((c, v) => c.Pessoa?.NoPessoa.StartsWith(v) ?? false);

            Add("Titular/Suplente")
                .Coluna((c) => c.Cargo.NoCargo)
                .Filtro((c, v) => c.CdCargo == v)
                .Combo((db, p) => new ListItem[] { new ListItem { Text = "Titular", Value = CdCargo.Titular }, new ListItem { Text = "Suplente", Value = CdCargo.Suplente } });

            Add("Data de nascimento")
                .Coluna((c) => c.Pessoa.DtNasc);

            Add("Email")
                .Coluna((c) => c.Pessoa.NoEmail);

            Add("Endereço de ressarcimento", false)
                .Coluna((c) => c.Pessoa.EnderecoFmt);

            Add("Endereço de correspondência", false)
                .Coluna((c) => c.Pessoa.NoEndCorresp);

            Add("Telefones")
                .Coluna((c) => c.Pessoa.NrTelefone);

            Add("Sexo")
                .Coluna((c) => c.Pessoa.TpSexo)
                .Filtro((c, v) => c.Pessoa.TpSexo == v)
                .Combo((db, p) => new ListItem[] { new ListItem { Text = "Masculino", Value = TpSexo.Masculino }, new ListItem { Text = "Feminino", Value = TpSexo.Feminino } });

            Add("CREASP")
                .Coluna((c) => c.Pessoa.NrCreasp)
                .Filtro((c, v) => c.Pessoa.NrCreasp == v)
                .Mask(MaskType.Custom, "R9999999999");

            Add("Matrícula")
                .Coluna((c) => c.Pessoa.NrMatricula);

            Add("CPF")
                .Coluna((c) => c.Pessoa.NrCpfFmt)
                .Filtro((c, v) => c?.Pessoa.NrCpf == v)
                .Mask(MaskType.Custom, "999.999.999-99");

            Add("Titulos")
                .Coluna((c) => c.Pessoa.NoTituloAbr);

            Add("Data de inicio mandato")
                .Coluna((c) => c.DtIni);

            Add("Data fim do mandato")
                .Coluna((c) => c.DtFim);

            Add("Faltas").Desc("Quantidade de faltas")
                .Coluna((c) => c.NrFaltas);

            Add("Aniversário")
                .Filtro((c, v) => c.Pessoa.DtNasc?.Month == v.To<int>())
                .Combo((db, p) => Enumerable.Range(1, 12).Select(i => new ListItem { Text = new DateTime(2000, i, 1).ToString("MMMM").Captalize(), Value = i.ToString() }));
            
            Add("Histórico", false)
                .Coluna((c) => c.Pessoa.NoHistorico);

            Add("Licenças", false).Desc("Licenças no período")
                .Coluna((c) => string.Join("\n", c.Licencas.Select(x => x.GetPeriodo() + " - " + x.NoMotivo)) );

            Add("Anuidade", false).Desc("Anuidade (pode demorar)")
                .Coluna((c, db) => db.Pessoas.ConsultaAnuidade(c.Pessoa.NrCreasp))
                .Filtro((c, v, db) => db.Pessoas.ConsultaAnuidade(c.Pessoa.NrCreasp) == v)
                .Combo((db, p) => new ListItem[] { new ListItem { Text = "Quite", Value = AnuidadeSituacao.Quite }, new ListItem { Text = "Em dia", Value = AnuidadeSituacao.EmDia }, new ListItem { Text = "Débito", Value = AnuidadeSituacao.EmDebito } });
        }

        public override IEnumerable<Mandato> GetDados(CreaspContext db, Periodo periodo)
        {
            var lic = db.Query<Licenca>()
                .WherePeriodo(periodo)
                .OrderBy(x => x.DtIni)
                .ToList();

            var mandatos = db.Query<Mandato>()
                .Include(x => x.Pessoa)
                .Include(x => x.Pessoa.Cep)
                .Include(x => x.Cargo)
                .Include(x => x.Camara)
                .Include(x => x.Entidade)
                .Include(x => x.Entidade.Unidade)
                .Where(x => x.TpMandato == TpMandato.Conselheiro)
                .WherePeriodo(periodo)
                .OrderBy(x => x.Pessoa.NoPessoa)
                .ToList();

            // conta as faltas e adiciona as licencas
            mandatos = db.Mandatos.ContaFaltas(mandatos, TpMandato.Conselheiro, periodo.Fim).ToList()
                .ForEach((x, i) => x.Licencas = lic.Where(a => a.IdPessoa == x.IdPessoa).ToList());

            return mandatos;
        }
    }
}