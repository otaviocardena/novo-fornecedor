﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Creasp.Core;
using NBox.Core;
using NBox.Web.Controls;

namespace Creasp.Siplen
{
    /// <summary>
    /// Relatorio DW para mandato de inspetores
    /// </summary>
    public class DWInspetorDataSource : DWDataSource<Mandato>
    {
        public override string Nome => "Inspetor";
        public override string Descricao => "Dados de mandatos de inpetores (um mandato de inspetor por linha)";
        public override bool PorPeriodo => true;

        public override void InicializaCampos()
        {
            Add("GRE")
                .Coluna((c, db) => db.Unidades.BuscaUnidadePai(c.Inspetoria.Unidade.CdUnidade, TpUnidade.DeptoRegional)?.NoUnidade)
                .Filtro((c, v, db) => db.Unidades.BuscaUnidadePai(c.Inspetoria.Unidade.CdUnidade, TpUnidade.DeptoRegional)?.CdUnidade == v.To<int>())
                .Combo((db, p) => db.Unidades.ListaUnidades(TpUnidade.DeptoRegional, null).Select(x => new ListItem { Text = x.NoUnidade, Value = x.CdUnidade.ToString() }));

            Add("Unidade")
                .Coluna((c) => c.Inspetoria.Unidade.NoUnidade);

            Add("Município")
                .Coluna((c) => c.Inspetoria.NoInspetoria);

            Add("Representação Municipal")
                .Coluna((c) => c.Inspetoria.FgRepresentacao ? "Sim" : "Não")
                .Filtro((c, v) => c.Inspetoria.FgRepresentacao == v.To<bool>())
                .Combo((db, p) => new ListItem[] { new ListItem { Text = "Sim", Value = "true" }, new ListItem { Text = "Não", Value = "false" } });

            Add("Mandato")
                .Coluna((c) => c.AnoIni + " - " + c.AnoFim);

            Add("Câmara")
                .Coluna((c) => c.Camara.NoCamara)
                .Filtro((c, v) => c.Camara.CdCamara == v.To<int>())
                .Combo((db, p) => db.Camaras.ListaCamaras(p).Select(k => new ListItem { Text = k.NoCamara, Value = k.CdCamara.ToString() }));

            Add("Tipo de inspetor")
                .Coluna((c) => c.Cargo.NoCargo)
                .Filtro((c, v) => c.Cargo.NoCargo == v)
                .Combo((db, p) => new ListItem[] { new ListItem { Text = "Inspetor", Value = CdCargo.Inspetor }, new ListItem { Text = "Inspetor chefe", Value = CdCargo.InspetorChefe }, new ListItem { Text = "Inspetor especial", Value = CdCargo.InspetorEspecial } });

            Add("Data inicio mandato")
                .Coluna((c) => c.DtIni);

            Add("Data fim mandato")
                .Coluna((c) => c.DtFim);

            Add("CREASP")
                .Coluna((c) => c.Pessoa.NrCreasp)
                .Filtro((c, v) => c.Pessoa.NrCreasp == v)
                .Mask(MaskType.Custom, "R9999999999");

            Add("CPF")
                .Coluna((c) => c.Pessoa.NrCpfFmt)
                .Filtro((c, v) => c?.Pessoa.NrCpf == v)
                .Mask(MaskType.Custom, "999.999.999-99");

            Add("Titulos")
                .Coluna((c) => c.Pessoa.NoTitulo);

            Add("Nome do inspetor")
                .Coluna((c) => c.Pessoa.NoPessoa)
                .Filtro((c, v) => c.Pessoa?.NoPessoa.StartsWith(v) ?? false);

            Add("Email")
                .Coluna((c) => c.Pessoa.NoEmail);

            Add("Telefones")
                .Coluna((c) => c.Pessoa.NrTelefone);

            Add("Faltas").Desc("Quantidade de faltas")
                .Coluna((c) => c.NrFaltas);

            Add("Aniversário")
                .Filtro((c, v) => c.Pessoa.DtNasc?.Month == v.To<int>())
                .Combo((db, p) => Enumerable.Range(1, 12).Select(i => new ListItem { Text = new DateTime(2000, i, 1).ToString("MMMM").Captalize(), Value = i.ToString() }));

            Add("Data de nascimento")
                .Coluna((c) => c.Pessoa.DtNasc);

            Add("Sexo")
                .Coluna((c) => c.Pessoa.TpSexo)
                .Filtro((c, v) => c.Pessoa.TpSexo == v)
                .Combo((db, p) => new ListItem[] { new ListItem { Text = "Masculino", Value = TpSexo.Masculino }, new ListItem { Text = "Feminino", Value = TpSexo.Feminino } });

            Add("Endereço de ressarcimento", false)
                .Coluna((c) => c.Pessoa.EnderecoFmt);

            Add("Endereço de correspondência", false)
                .Coluna((c) => c.Pessoa.NoEndCorresp);

            Add("Histórico", false)
                .Coluna((c) => c.Pessoa.NoHistorico);

            Add("Anuidade", false).Desc("Anuidade (pode demorar)")
                .Coluna((c, db) => db.Pessoas.ConsultaAnuidade(c.Pessoa.NrCreasp))
                .Filtro((c, v, db) => db.Pessoas.ConsultaAnuidade(c.Pessoa.NrCreasp) == v)
                .Combo((db, p) => new ListItem[] { new ListItem { Text = "Quite", Value = AnuidadeSituacao.Quite }, new ListItem { Text = "Em dia", Value = AnuidadeSituacao.EmDia }, new ListItem { Text = "Débito", Value = AnuidadeSituacao.EmDebito } });
        }

        public override IEnumerable<Mandato> GetDados(CreaspContext db, Periodo periodo)
        {
            var lic = db.Query<Licenca>()
                .WherePeriodo(periodo)
                .OrderBy(x => x.DtIni)
                .ToList();

            var srv = new MandatoService(db);

            var mandatos = db.Query<Mandato>()
                .Include(x => x.Pessoa)
                .Include(x => x.Pessoa.Cep)
                .Include(x => x.Cargo)
                .Include(x => x.Camara)
                .Include(x => x.Inspetoria)
                .Include(x => x.Inspetoria.Unidade)
                .Where(x => x.TpMandato == TpMandato.Inspetor)
                .WherePeriodo(periodo)
                .OrderBy(x => x.Pessoa.NoPessoa)
                .ToEnumerable();

            // conta as faltas + licenças
            return srv.ContaFaltas(mandatos, TpMandato.Inspetor, periodo.Fim).ToList()
                .ForEach((x, i) => x.Licencas = lic.Where(a => a.IdPessoa == x.IdPessoa).ToList());
        }
    }
}