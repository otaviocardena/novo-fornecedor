﻿using NBox.Services;
using NBox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using NPoco;
using Creasp.Core;
using NPoco.Linq;
using System.ComponentModel;

namespace Creasp.Siplen
{
    /// <summary>
    /// Classe responsavel por ser a fonte de dados para os relatorios do SIPLEN
    /// </summary>
    public class RelatorioService : DataService
    {
        public RelatorioService(ServiceContext context) : base(context) { }

        /// <summary>
        /// Relatório da lista de presenca a um evento
        /// </summary>
        public IEnumerable<RListaPresencaDTO> ListaPresenca(int idEvento, TpParticipante? tpParticipante = null)
        {
            var srv = new EventoService(context);
            var participantes = srv.ListaParticipantes(idEvento, false, null);
            var pessoas = new PessoaService(context);
            var funcionarios = new FuncionarioService(context);
            var result = new List<RListaPresencaDTO>();
            var index = 0;

            participantes = participantes.Where(x => BuscaTipoParticipante(x.TpMandato) == tpParticipante).ToList();
            participantes = participantes.OrderBy(x => x.TpMandato == "0" ? 1 : 0).ThenBy(x => x.Titular?.NoPessoa).ToList();
            
            foreach (var p in participantes.Where(x => x.NrOrdem == 0))
            {
                // no caso de funciona, tenta buscar a unidade
                var noUnidade = "";

                if(p.Titular.NrMatricula != null)
                {
                    try
                    {
                        noUnidade = funcionarios.BuscaUnidade(p.Titular)?.NoUnidade ?? "Unidade não encontrada";
                    }
                    catch(Exception ex)
                    {
                        noUnidade = ex.Message;
                    }
                }

                result.Add(new RListaPresencaDTO
                {
                    NrSeq = ++index,

                    NoTitular = p.Titular.NoPessoa,
                    NoTituloTit = p.Titular.NoTituloAbr,
                    NoPercursoTit = p.Titular.Cep?.NoCidade + " - " + p.Evento.Cep?.NoCidade,
                    NoPresencaTit = srv.GetPresenca(p.TpPresencaTit),

                    NoSuplente = p.Suplente?.NoPessoa,
                    NoTituloSup = p.Suplente?.NoTituloAbr,
                    NoPercursoSup = (p.Suplente == null ? "" : p.Suplente?.Cep?.NoCidade + " - " + p.Evento.Cep?.NoCidade),
                    NoPresencaSup = srv.GetPresenca(p.TpPresencaSup),

                    NrMatricula = p.Titular.NrMatricula,
                    NoUnidade = noUnidade
                });
            }

            // adiciona os suplentes não diretos na coluna ao lado (somente conselheiros em comissões)
            foreach (var p in participantes.Where(x => x.NrOrdem > 0))
            {
                // procura pela primeira caixa de suplente vazia. Se não tiver, cria um novo
                var item = result.FirstOrDefault(x => x.NoSuplente == null);

                if (item == null)
                {
                    item = new RListaPresencaDTO { NrSeq = result.Count + 1 };
                    result.Add(item);
                }

                item.NoSuplente = p.NrOrdem + "º Suplente - " + p.Titular.NoPessoa;
                item.NoTituloSup = p.Titular.NoTituloAbr;
                item.NoPercursoSup = p.Titular.Cep?.NoCidade + " - " + p.Evento.Cep?.NoCidade;
                item.NoPresencaSup = srv.GetPresenca(p.TpPresencaTit);
            }

            return result;
        }

        /// <summary>
        /// Identifica o tipo de participante conforme o tipo de mandato quando adicionado ao evento (Conselheiro, Diretor, Inspetor, Funcionario, Convidado ou Grupo de Trabalho)
        /// </summary>
        public TpParticipante BuscaTipoParticipante(string tpMandato)
        {

            var tpParticipante = new TpParticipante();

            if (tpMandato == TpMandato.Camara || tpMandato == TpMandato.Comissao || tpMandato == TpMandato.Conselheiro ||
                tpMandato == TpMandato.Indicacao || tpMandato == TpMandato.Representante)
            {
                tpParticipante = TpParticipante.Conselheiro;
            }
            else
            {
                switch (tpMandato)
                {
                    case TpMandato.Diretor: tpParticipante = TpParticipante.Diretor; break;
                    case TpMandato.Inspetor: tpParticipante = TpParticipante.Inspetor; break;
                    case TpMandato.Grupo: tpParticipante = TpParticipante.GrupoDeTrabalho; break;
                    case TpMandato.Funcionario: tpParticipante = TpParticipante.Funcionario; break;
                    default: tpParticipante = TpParticipante.Outros; break;
                }
            }

            return tpParticipante;
        }
    }
}