﻿using NBox.Services;
using NBox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using NPoco;
using Creasp.Core;
using NPoco.Linq;

namespace Creasp.Siplen
{
    public class IndicacaoService : DataService
    {
        protected ICreanet creanet = Factory.Get<ICreanet>();

        public IndicacaoService(ServiceContext context) : base(context) { }

        /// <summary>
        /// Inclui uma nova indicação
        /// </summary>
        public int NovaIndicacao(int anoIni, int idEntidade, int cdCamara, int? idPessoaTit, int? idPessoaSup, string noHistorico, int? inspetoria)
        {
            // Faz as validações (somente se há indicação)
            if(idPessoaTit.HasValue)
            {
                ValidaCadastro(idPessoaTit.Value, cdCamara);
            }
            if (idPessoaSup.HasValue)
            {
                if (!idPessoaTit.HasValue) throw new NBoxException("Não é possivel indicar apenas suplente sem indicar titular");
                if (idPessoaTit == idPessoaSup) throw new NBoxException("Titular e suplente não podem ser a mesma pessoa");

                ValidaCadastro(idPessoaSup.Value, cdCamara);
            }

            // Inclui uma nova indicação
            var ind = new Indicacao
            {
                AnoIni = anoIni,
                AnoFim = anoIni + 2,
                CdCamara = cdCamara,
                IdEntidade = idEntidade,
                IdPessoaTit = idPessoaTit,
                IdPessoaSup = idPessoaSup,
                NoHistorico = noHistorico,
                IdInspetoria = inspetoria
            };

            db.Insert(ind);

            return ind.IdIndicacao;
        }

        /// <summary>
        /// Altera uma indicação já existente (somente as indicações) - exclui/inclui
        /// </summary>
        public void AlteraIndicacao(int idIndicacao, int? idPessoaTit, int? idPessoaSup, string noHistorico, int? inspetoria)
        {
            var ind = db.SingleById<Indicacao>(idIndicacao);

            if (ind.DtPosseTit.HasValue && idPessoaTit != ind.IdPessoaTit) throw new NBoxException("Não é permitido alterar o titular pois já tomou posse");
            if (ind.DtPosseSup.HasValue && idPessoaSup != ind.IdPessoaSup) throw new NBoxException("Não é permitido alterar o suplente pois já tomou posse");

            ind.IdPessoaTit = idPessoaTit;
            ind.IdPessoaSup = idPessoaSup;
            ind.NoHistorico = noHistorico;
            ind.IdInspetoria = inspetoria;

            db.AuditUpdate(ind);
        }

        /// <summary>
        /// Exclui um mandato que ainda não tenha data de posse
        /// </summary>
        public void ExcluiIndicacao(int idIndicacao)
        {
            var ind = db.SingleById<Indicacao>(idIndicacao);
            var docs = new DocumentoService(context);

            if (ind.DtPosseTit.HasValue) throw new NBoxException("Esta indicação não pode ser excluida pois já houve posse");

            docs.ExcluiDocumento(TpRefDoc.Indicacao, idIndicacao);
            docs.ExcluiDocumento(TpRefDoc.Indicacao, -idIndicacao);

            db.AuditDelete<Indicacao>(idIndicacao);
        }

        /// <summary>
        /// Dá posse aos mandatos dos conselheiros
        /// </summary>
        public void PosseIndicados(IEnumerable<Indicacao> indicacoes)
        {
            using (var trans = db.GetTransaction())
            {
                foreach (var ind in indicacoes)
                {
                    // valida as datas de posse (titular/suplente)
                    if (ind.DtPosseTit.HasValue && ind.IdMandatoTit == null)
                    {
                        //if (ind.DtPosseTit.Value.Year < ind.AnoIni || ind.DtPosseTit.Value.Year > ind.AnoFim) throw new NBoxException("A data de posse {0:d} é inválida para o período do mandato {1}-{2}", ind.DtPosseTit.Value, ind.AnoIni, ind.AnoFim);

                        ValidaMandatoIndicacaoEmAberto(ind.IdPessoaTit.Value, new Periodo(ind.DtPosseTit.Value, new DateTime(ind.AnoFim, 12, 31)));
                        ValidaMandatoConsecutivo(ind.IdPessoaTit.Value, ind.AnoIni);
                    }
                    if (ind.DtPosseSup.HasValue && ind.IdMandatoSup == null)
                    {
                        if (ind.DtPosseTit == null) throw new NBoxException("Não é permitido posse apenas do suplente");
                        //if (ind.DtPosseSup.Value.Year < ind.AnoIni || ind.DtPosseSup.Value.Year > ind.AnoFim) throw new NBoxException("A data de posse {0:d} é inválida para o período do mandato {1}-{2}", ind.DtPosseSup.Value, ind.AnoIni, ind.AnoFim);

                        ValidaMandatoIndicacaoEmAberto(ind.IdPessoaSup.Value, new Periodo(ind.DtPosseSup.Value, new DateTime(ind.AnoFim, 12, 31)));
                        ValidaMandatoConsecutivo(ind.IdPessoaSup.Value, ind.AnoIni);
                    }


                    // cria mandato de titular
                    if (ind.IdPessoaTit.HasValue && ind.DtPosseTit.HasValue && ind.IdMandatoTit == null)
                    {
                        var titular = new Mandato
                        {
                            TpMandato = TpMandato.Conselheiro,
                            CdCargo = CdCargo.Titular,
                            IdPessoa = ind.IdPessoaTit.Value,
                            DtIni = ind.DtPosseTit.Value,
                            DtFim = new DateTime(ind.AnoFim, 12, 31),
                            AnoIni = ind.AnoIni,
                            AnoFim = ind.AnoFim,
                            CdCamara = ind.CdCamara,
                            IdEntidade = ind.IdEntidade
                        };

                        db.Insert(titular);
                        ind.IdMandatoTit = titular.IdMandato;

                        TransfereDocumentos(ind.IdIndicacao, titular.IdMandato);
                    }

                    // cria mandato de suplente
                    if (ind.IdPessoaSup.HasValue && ind.DtPosseSup.HasValue && ind.IdMandatoSup == null)
                    {
                        // não permite posse de supente sem posse do titular
                        if (ind.IdMandatoTit == null) throw new NBoxException("O titular {0} ainda não tomou posse", db.SingleById<Pessoa>(ind.IdPessoaTit).NoPessoa);

                        // não permite que o suplente tenha data anterior a posse do titular
                        // if (ind.DtPosseSup.Value < ind.DtPosseTit.Value) throw new NBoxException("A data de posse do suplente {0} deve ser maior ou igual a {1:d}", db.SingleById<Pessoa>(ind.IdPessoaSup).NoPessoa, ind.DtPosseTit);

                        var suplente = new Mandato
                        {
                            TpMandato = TpMandato.Conselheiro,
                            CdCargo = CdCargo.Suplente,
                            IdPessoa = ind.IdPessoaSup.Value,
                            DtIni = ind.DtPosseSup.Value,
                            DtFim = new DateTime(ind.AnoFim, 12, 31),
                            AnoIni = ind.AnoIni,
                            AnoFim = ind.AnoFim,
                            CdCamara = ind.CdCamara,
                            IdEntidade = ind.IdEntidade,
                            IdMandatoTit = ind.IdMandatoTit
                        };

                        db.Insert(suplente);
                        ind.IdMandatoSup = suplente.IdMandato;

                        TransfereDocumentos(-ind.IdIndicacao, suplente.IdMandato);
                    }

                    // atualizando o Id da vaga na indicação
                    db.Update(ind);
                }

                trans.Complete();
            }
        }

        /// <summary>
        /// Remove de uma indicação o titular ou suplente, removendo também os arquivos de referencia
        /// </summary>
        public void RemoveTitularSuplente(int idIndicacao, bool titular)
        {
            var ind = db.SingleById<Indicacao>(idIndicacao);
            var docs = new DocumentoService(context);
            docs.ExcluiDocumento(TpRefDoc.Indicacao, titular ? idIndicacao : -idIndicacao);

            if (titular)
            {
                ind.IdPessoaTit = null;
            }
            else
            {
                ind.IdPessoaSup = null;
            }

            db.AuditUpdate(ind);
        }

        /// <summary>
        /// Lista os representantes do plenário futuro
        /// </summary>
        public List<Indicacao> ListaPlenarioFuturo(bool validaPosse, int nrAnoIni, bool fgSomenteSemMandato = true, int? idEntidade = null, string noPessoa = null, int? cdCamara = null)
        {
            return db.Query<Indicacao>()
                .Include(x => x.Titular, JoinType.Left)
                .Include(x => x.Suplente, JoinType.Left)
                .Include(x => x.Entidade)
                .Include(x => x.Camara)
                .Where(x => x.AnoIni == nrAnoIni)
                .Where(fgSomenteSemMandato, x => x.DtPosseTit == null || x.DtPosseSup == null)
                .Where(idEntidade.HasValue, x => x.IdEntidade == idEntidade.Value)
                .Where(!string.IsNullOrEmpty(noPessoa), x => x.Titular.NoPessoa.Contains(noPessoa.ToLike()) || x.Suplente.NoPessoa.Contains(noPessoa.ToLike()))
                .Where(cdCamara.HasValue, x => x.CdCamara == cdCamara)
                .OrderBy(x => x.Camara.NoCamara).ThenBy(x => x.Titular.NoPessoa)
                .ToList()
                .ForEach((x, i) => x.MensagemTit = validaPosse ? ValidaPosse(x.IdPessoaTit, x.DtPosseTit, x) : null)
                .ForEach((x, i) => x.MensagemSup = validaPosse ? ValidaPosse(x.IdPessoaSup, x.DtPosseSup, x) : null);
        }

        #region Validações para indicação

        /// <summary>
        /// Valida se uma posse pode ser realizada. Retorna null se está tudo ok ou a mensagem de erro
        /// </summary>
        private string ValidaPosse(int? idPessoa, DateTime? dtPosse, Indicacao ind)
        {
            try
            {
                if (dtPosse.HasValue) throw new NBoxException("Já tomou posse em {0:d}", dtPosse);
                if (idPessoa == null) return null;

                ValidaAnuidade(idPessoa.Value);

                // se for suplente, valida usando o IdIndicacao negativo
                ValidaDocumentacao(idPessoa.Value == ind.IdPessoaSup ? -ind.IdIndicacao : ind.IdIndicacao);

                return null;
            }
            catch(NBoxException ex)
            {
                return ex.Message;
            }
        }

        /// <summary>
        /// Valida se o conselheiro está em dia com a anuidade
        /// </summary>
        private void ValidaAnuidade(int idPessoa)
        {
            var pessoa = db.SingleById<Pessoa>(idPessoa);
            var situacao = creanet.ConsultaAnuidade(pessoa.NrCreasp);
            if (situacao == AnuidadeSituacao.EmDebito) throw new NBoxException("{0} não está em dia com a anuidade do CREA", pessoa.NoPessoa);
        }

        /// <summary>
        /// Valida se o profissional indica tem a mesma camara de indicação
        /// </summary>
        public void ValidaCadastro(int idPessoa, int cdCamara)
        {
            var pessoa = db.SingleById<Pessoa>(idPessoa);

            if (string.IsNullOrEmpty(pessoa.NrCep)) throw new NBoxException("{0} não possui endereço informado", pessoa.NoPessoa);
            if (string.IsNullOrEmpty(pessoa.NoEmail)) throw new NBoxException("{0} não possui email informado", pessoa.NoPessoa);

            var profissional = creanet.BuscaProfissionais(pessoa.NrCreasp, null, null).SingleOrDefault();
            if (profissional == null) throw new NBoxException("Não foi possível encontrar o profissional {0} no CREANET", pessoa.NoPessoa);

            if (!profissional.Camaras.Contains(cdCamara))
            {
                throw new NBoxException("{0} não possui título para a câmara {1}",
                    pessoa.NoPessoa,
                    db.SingleById<Camara>(cdCamara).NoCamara);
            }
        }

        /// <summary>
        /// Valida se a documentação obrigatória foi anexada
        /// </summary>
        private void ValidaDocumentacao(int idIndicacao)
        {
            var doc = new DocumentoService(context);
            var docs = doc.ListaDocumentos(TpRefDoc.Indicacao, idIndicacao);
            doc.AdicionaDocumentosFaltantes(docs, TpRefDoc.Conselheiro);
            var faltante = docs.FirstOrDefault(x => x.Tipo.FgObrigatorio && x.NoArquivo == null);

            if (faltante != null)
            {
                throw new NBoxException("O documento '{0}' está não foi enviado", faltante.Tipo.NoTipoDoc);
            }
        }

        /// <summary>
        /// Verifica se o indicato tem mandato ativo no ano da indicação (ou outra indicação) de conselheiro/inspetor
        /// </summary>
        public void ValidaMandatoIndicacaoEmAberto(int idPessoa, Periodo periodo)
        {
            var mandato = db.Query<Mandato>()
                .Include(x => x.Cargo)
                .Include(x => x.Entidade, JoinType.Left)
                .Include(x => x.Inspetoria, JoinType.Left)
                .Where(x => x.TpMandato == TpMandato.Conselheiro || x.TpMandato == TpMandato.Inspetor)
                .Where(x => x.IdPessoa == idPessoa)
                .WherePeriodo(periodo)
                .FirstOrDefault();

            if (mandato != null)
            {
                throw new NBoxException("{0} já é {1} por {2} ({3} - {4})",
                    db.SingleById<Pessoa>(idPessoa).NoPessoa,
                    mandato.Cargo.NoCargo,
                    mandato.Entidade?.NoEntidade ?? mandato.Inspetoria?.NoInspetoria, 
                    mandato.AnoIni, mandato.AnoFim);
            }
        }

        /// <summary>
        /// Verifica se o conselheiro possui 2 mandatos consecutivos
        /// </summary>
        public void ValidaMandatoConsecutivo(int idPessoa, int anoIni, int c = 2)
        {
            // busca o ultimo mandato com data de fim menor que a data de inicio
            var mandato = db.Query<Mandato>()
                .Where(x => x.TpMandato == TpMandato.Conselheiro)
                .Where(x => x.IdPessoa == idPessoa)
                .Where(x => x.AnoFim < anoIni)
                .Where(x => x.FgValido)
                .OrderByDescending(x => x.DtIni)
                .FirstOrDefault();

            // se não existe mandato, encerra OK
            if (mandato == null) return;
            
            // Verifica se existe pelo menos 1 ano entre o fim do primeiro mandato pro segundo mandato
            if(mandato.AnoFim < anoIni - 1) return;

            // verifica se foi titular (ou interino)
            if (FinalizouMandatoComoTitular(mandato) == false) return;

            // o mandato é consecutivo, gera exception
            if (c == 1) throw new NBoxException("{0} já possui 2 mandatos consecutivos", db.SingleById<Pessoa>(idPessoa).NoPessoa);

            // se não, chama a mesma função passando o mandato lido como inicio
            ValidaMandatoConsecutivo(idPessoa, mandato.AnoIni, 1);
        }

        /// <summary>
        /// Verifica se o mandato foi encerrado como titular
        /// </summary>
        private bool FinalizouMandatoComoTitular(Mandato mandato)
        {
            // titular sempre considera para contagem
            if (mandato.CdCargo == CdCargo.Titular) return true;

            // ultimo dia do mandato
            var dtFim = new DateTime(mandato.AnoFim, 12, 31);

            // se o suplente não ficou até o final do mandato, não pode ter sido titular interino
            if (mandato.DtFim != dtFim) return false;

            // como o mandato é de suplente, pega o mandato do titular
            var titular = db.SingleById<Mandato>(mandato.IdMandatoTit);

            // verifica se o titular esta em licenca no ultimo dia do mandato
            var lic = db.Query<Licenca>()
                .Where(x => x.IdPessoa == titular.IdPessoa)
                .WherePeriodo(Periodo.UmDia(dtFim))
                .SingleOrDefault();

            // se existe licenca, entao o suplente assumiu até o ultimo dia do mandato
            return lic == null ? false : true;
        }

        /// <summary>
        /// Transfere todos os documentos da indicação para o mandato do conselheiro
        /// </summary>
        private void TransfereDocumentos(int idIndicacao, int idMandato)
        {
            var docs = db.Query<Documento>()
                .Where(x => x.TpRef == TpRefDoc.Indicacao)
                .Where(x => x.IdRef == idIndicacao)
                .ToList();

            foreach (var d in docs)
            {
                d.IdRef = idMandato;
                d.TpRef = TpRefDoc.Conselheiro;
                db.Update(d);
            }
        }

        #endregion
    }
}
