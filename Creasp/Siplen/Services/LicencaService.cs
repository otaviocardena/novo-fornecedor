﻿using NBox.Services;
using NBox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using NPoco;
using Creasp.Core;

namespace Creasp.Siplen
{
    public class LicencaService : DataService
    {
        public LicencaService(ServiceContext context) : base(context) { }

        /// <summary>
        /// Inclui uma nova licenca a uma pessoa
        /// </summary>
        public void Inclui(int idPessoa, string noMotivo, Periodo periodo, HttpPostedFile file)
        {
            ValidaColisao(idPessoa, periodo);

            var lic = new Licenca
            {
                IdPessoa = idPessoa,
                DtIni = periodo.Inicio,
                DtFim = periodo.Fim,
                NoMotivo = noMotivo
            };

            db.Insert(lic);

            if (file != null && file.ContentLength > 0)
            {
                var doc = new DocumentoService(context);
                doc.EnviaDocumento(TpRefDoc.Licenca, lic.IdLicenca, idPessoa, null, file);
            }
        }

        /// <summary>
        /// Inclui uma nova licenca a uma pessoa
        /// </summary>
        public void Altera(int idLicenca, string noMotivo, Periodo periodo, HttpPostedFile file)
        {
            //TODO: é preciso fazer validações antes alterar um periodo de licenca. Verifica se o suplente não andou assumindo nada
            var lic = db.SingleById<Licenca>(idLicenca);

            ValidaColisao(lic.IdPessoa, periodo, idLicenca);

            lic.NoMotivo = noMotivo;
            lic.DtIni = periodo.Inicio;
            lic.DtFim = periodo.Fim;

            db.Update(lic);

            if (file != null && file.ContentLength > 0)
            {
                var doc = new DocumentoService(context);
                doc.EnviaDocumento(TpRefDoc.Licenca, lic.IdLicenca, lic.IdPessoa, null, file);
            }
        }

        public void Exclui(int idLicenca)
        {
            db.AuditDelete<Licenca>(idLicenca);
        }

        public QueryPage<Licenca> ListaLicencas(Pager p, Periodo periodo, string noPessoa = null)
        {
            return db.Query<Licenca>()
                .Include(x => x.Pessoa)
                .WherePeriodo(periodo)
                .Where(noPessoa != null, x => x.Pessoa.NoPessoa.Contains(noPessoa.ToLike()))
                .ToPage(p.DefaultSort<Licenca>(x => x.DtFim, false));
        }

        /// <summary>
        /// Busca a licenca junto com a referencia ao documento (caso exista)
        /// </summary>
        public Licenca BuscaLicenca(int idLicenca)
        {
            var lic = db.SingleById<Licenca>(idLicenca);

            lic.Documento = db.Query<Documento>()
                .Include(x => x.Tipo)
                .Where(x => x.Tipo.TpRef == TpRefDoc.Licenca)
                .Where(x => x.IdRef == idLicenca)
                .FirstOrDefault();

            return lic;
        }

        #region Validações

        /// <summary>
        /// Verifica se a pessoa já não possui uma licenca que colida com o periodo informado
        /// </summary>
        private void ValidaColisao(int idPessoa, Periodo periodo, int? idLicenca = null)
        {
            var lic = db.Query<Licenca>()
                .Include(x => x.Pessoa)
                .Where(idLicenca.HasValue, x => x.IdLicenca != idLicenca)
                .Where(x => x.IdPessoa == idPessoa)
                .WherePeriodo(periodo)
                .SingleOrDefault();

            if(lic != null)
            {
                throw new NBoxException("{0} já possui uma licença no período {1}", lic.Pessoa.NoPessoa, lic.GetPeriodo());
            }
        }

        #endregion
    }
}