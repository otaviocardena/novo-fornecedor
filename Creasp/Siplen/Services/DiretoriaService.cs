﻿using NBox.Services;
using NBox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using NPoco;
using Creasp.Core;
using NPoco.Linq;

namespace Creasp.Siplen
{
    public class DiretoriaService : MandatoService
    {
        public DiretoriaService(ServiceContext context) : base(context) { }

        /// <summary>
        /// Inclui um novo diretor
        /// </summary>
        public void IncluiDiretor(string cdCargo, int idPessoa, Periodo periodo)
        {
            if (cdCargo != CdCargo.Presidente) periodo.ValidaMax('M', 13);

            ValidaMesmoCargoPeriodo(cdCargo, periodo);
            ValidaMandatoRepetido(idPessoa, periodo);

            // verifica se o cargo está disponivel no periodo informado
            if (db.Query<Cargo>()
                .WherePeriodo(Periodo.UmDia(periodo.Inicio))
                .Count(x => x.CdCargo == cdCargo) == 0)
            {
                throw new NBoxException("Cargo {0} não disponível em {1:d}", db.SingleById<Cargo>(cdCargo).NoCargo, periodo.Inicio);
            }

            var cons = new ConselheiroService(context);
            var titular = cdCargo == CdCargo.Presidente ? // O presidente do conselho é o presidente do crea (não tem mandato de conselheiro)
                new Mandato { DtFim = periodo.Fim } :
                cons.BuscaMandatoTitular(idPessoa, periodo.Inicio);

            var diretor = new Mandato
            {
                TpMandato = TpMandato.Diretor,
                CdCargo = cdCargo,
                IdPessoa = idPessoa,
                DtIni = periodo.Inicio,
                DtFim = periodo.FimMaisProximo(titular.DtFim),
                AnoIni = periodo.Inicio.Year,
                AnoFim = periodo.Inicio.Year
            };

            db.Insert(diretor);
        }

        /// <summary>
        /// Permite alterar um periodo de mandato de diretor
        /// </summary>
        public void AlteraDiretor(int idMandato, Periodo periodo)
        {
            var diretor = db.SingleById<Mandato>(idMandato);

            using (var trans = db.GetTransaction())
            {
                diretor.DtIni = periodo.Inicio;
                diretor.DtFim = periodo.Fim;

                db.Update(diretor);

                trans.Complete();
            }
        }

        /// <summary>
        /// Lista os diretores conforme o tipo informado (se informado) dentro do periodo informado (obrigatorio)
        /// </summary>
        /// <returns></returns>
        public List<Mandato> ListaDiretores(Periodo periodo,bool incluiLabelPresidenteExercicio, string cdCargo = null)
        {
            var diretoriasrv = new DiretoriaService(context);
            var presidenteExercicio = diretoriasrv.BuscaPresidenteExercicio(periodo.Inicio);

            var lic = db.Query<Licenca>()
                .WherePeriodo(periodo)
                .OrderBy(x => x.DtIni)
                .ToList();

            var mandatos = db.Query<Mandato>()
                .Include(x => x.Pessoa)
                .Include(x => x.Cargo)
                .Where(x => x.TpMandato == TpMandato.Diretor)
                .WherePeriodo(periodo)
                .Where(cdCargo != null, x => x.CdCargo == cdCargo)
                .OrderBy(x => x.Cargo.NrOrdem).ThenBy(x => x.DtIni)
                .ToList()
                .ForEach((x, i) => x.Licencas = lic.Where(a => a.IdPessoa == x.IdPessoa).ToList());

            if (incluiLabelPresidenteExercicio)
            {
                foreach (var mandato in mandatos)
                {
                    if (mandato.IdPessoa == presidenteExercicio?.Pessoa?.IdPessoa)
                    {
                        mandato.Pessoa.NoPessoa = presidenteExercicio.Pessoa.NoPessoa + " (Presidente em Exercício)";
                    }
                }
            }            
            
            // Verifica possiveis cargos faltantes
            var cargos = db.Query<Cargo>()
                .WherePeriodo(periodo)
                .Where(cdCargo != null, x => x.CdCargo == cdCargo)
                .Where(x => x.NrOrdem >= CdCargo.NrDiretor)
                .ToList();

            AdicionaCargosFaltantes(mandatos, cargos);

            return mandatos.OrderBy(x => x.Cargo.NrOrdem).ThenBy(x => x.DtIni).ToList();
        }

        /// <summary>
        /// Retorna o presidente em exercício(presidente ou vice-presidente).
        /// Não testa período de licença para o vice, "pois a licença é para o cargo de conselheiro".
        /// </summary>
        public PresidenteExercicioDTO BuscaPresidenteExercicio(DateTime data)
        {
            var presidenteExercicioDTO = new PresidenteExercicioDTO();

            //Busca presidente
            var presidenteMandato = db.Query<Mandato>()
                .Include(x => x.Pessoa)
                .Include(x => x.Cargo)
                .Where(x => x.TpMandato == TpMandato.Diretor)
                .Where(x => x.CdCargo == "PRESIDENTE")
                .Where(x => x.DtIni <= data && data <= x.DtFim)
                .FirstOrDefault();

            if (presidenteMandato != null)
            {
                var licencasPresidente = db.Query<Licenca>()
                                        .Where(x => x.IdPessoa == presidenteMandato.IdPessoa)
                                        .ToList();

                foreach (var lic in licencasPresidente)
                {
                    presidenteMandato.Licencas.Add(lic);
                }

                var licencaPresidenteNaData = presidenteMandato.Licencas.Where(x => x.DtIni <= data && data <= x.DtFim).FirstOrDefault();

                //Não há licencas para o cargo presidente
                if (licencaPresidenteNaData == null)
                {
                    presidenteExercicioDTO = MontaPeriodoPresidenteExercicio(presidenteMandato, data);
                }
                else
                {
                    var vicePresidenteMandato = db.Query<Mandato>()
                        .Include(x => x.Pessoa)
                        .Include(x => x.Cargo)
                        .Where(x => x.TpMandato == TpMandato.Diretor)
                        .Where(x => x.CdCargo == "VICE-PRESIDENTE")
                        .Where(x => x.DtIni <= data && data <= x.DtFim)
                        .FirstOrDefault();

                    if (vicePresidenteMandato != null)
                    {
                        presidenteExercicioDTO.Pessoa = vicePresidenteMandato.Pessoa;
                        presidenteExercicioDTO.DtIni = licencaPresidenteNaData.DtIni > vicePresidenteMandato.DtIni ? licencaPresidenteNaData.DtIni : vicePresidenteMandato.DtIni;
                        presidenteExercicioDTO.DtFim = licencaPresidenteNaData.DtFim < vicePresidenteMandato.DtFim ? licencaPresidenteNaData.DtFim : vicePresidenteMandato.DtFim;
                    }
                }
            }             

            return presidenteExercicioDTO;
        }

        /// <summary>
        /// Monta o período de mandato do presidente, respeitando o período de licença
        /// </summary>
        private PresidenteExercicioDTO MontaPeriodoPresidenteExercicio(Mandato mandatoPresidente, DateTime data)
        {
            var presidenteExercicioDTO = new PresidenteExercicioDTO();

            if (mandatoPresidente.Licencas.Count == 0)
            {
                presidenteExercicioDTO.Pessoa = mandatoPresidente.Pessoa;
                presidenteExercicioDTO.DtIni = mandatoPresidente.DtIni;
                presidenteExercicioDTO.DtFim = mandatoPresidente.DtFim;
            }
            else
            {
                // Retorna licença anterior e posterior a data passada por parametro e garante que esteja dentro do mandato do presidente
                var licAntesData = mandatoPresidente.Licencas.Where(x => x.DtFim < data && mandatoPresidente.DtIni <= x.DtFim).OrderByDescending(x => x.DtFim).FirstOrDefault();
                var licAposData = mandatoPresidente.Licencas.Where(x => x.DtIni > data && mandatoPresidente.DtFim >= x.DtIni).OrderBy(x => x.DtIni).FirstOrDefault();

                presidenteExercicioDTO.Pessoa = mandatoPresidente.Pessoa;
                presidenteExercicioDTO.DtIni = licAntesData != null ? licAntesData.DtFim.AddDays(1) : mandatoPresidente.DtIni;
                presidenteExercicioDTO.DtFim = licAposData != null ? licAposData.DtIni.AddDays(-1) : mandatoPresidente.DtFim;
            }
            
            return presidenteExercicioDTO;
        }

        #region Validações

        /// <summary>
        /// Valida se não existe um mesmo cargo num periodo existente
        /// </summary>
        private void ValidaMesmoCargoPeriodo(string cdCargo, Periodo periodo)
        {
            var mandato = db.Query<Mandato>()
                .Include(x => x.Cargo)
                .Where(x => x.TpMandato == TpMandato.Diretor)
                .Where(x => x.CdCargo == cdCargo)
                .WherePeriodo(periodo)
                .FirstOrDefault();

            if (mandato != null)
            {
                throw new NBoxException("Já existe um mandato de {0} no período {1}", mandato.Cargo.CdCargo, mandato.GetPeriodo());
            }
        }

        /// <summary>
        /// Verifica se um mesmo conselheiro já não ocupa um cargo de diretor no periodo
        /// </summary>
        private void ValidaMandatoRepetido(int idPessoa, Periodo periodo)
        {
            var mandato = db.Query<Mandato>()
                .Include(x => x.Pessoa)
                .Include(x => x.Cargo)
                .Where(x => x.TpMandato == TpMandato.Diretor)
                .Where(x => x.IdPessoa == idPessoa)
                .WherePeriodo(periodo)
                .FirstOrDefault();

            if (mandato != null)
            {
                throw new NBoxException("{0} já é {1} no período {2}", mandato.Pessoa.NoPessoa, mandato.Cargo.CdCargo, mandato.GetPeriodo());
            }
        }

        #endregion
    }
}