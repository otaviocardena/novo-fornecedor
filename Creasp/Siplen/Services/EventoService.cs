﻿using NBox.Services;
using NBox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using NPoco;
using Creasp.Core;
using NPoco.Linq;
using System.Data;
using Creasp.Nerp;
using System.Text.RegularExpressions;

namespace Creasp.Siplen
{
    public class EventoService : DataService
    {
        protected readonly int nrAnoBase;

        public EventoService(ServiceContext context) : base(context)
        {
            nrAnoBase = (context as CreaspContext).NrAnoBase;
        }

        /// <summary>
        /// Inclui um novo evento
        /// </summary>
        public int IncluiEvento(string noEvento, DateTime dtEvento, DateTime dtEventoFim, string nrCep, string noLocal, bool fgContaFalta, string noHistorico, bool fgPermiteDiariaDup)
        {
            var e = new Evento
            {
                NrAno = nrAnoBase,
                NoEvento = noEvento,
                DtEvento = dtEvento,
                DtEventoFim = dtEventoFim,
                NrCep = nrCep,
                NoLocal = noLocal,
                FgContaFalta = fgContaFalta,
                NoHistorico = noHistorico,
                FgPermiteDiariaDup = fgPermiteDiariaDup
            };

            db.Insert(e);

            return e.IdEvento;
        }

        /// <summary>
        /// Altera os dados de um evento.
        /// </summary>
        public void AlteraEvento(int idEvento, string noEvento, DateTime dtEvento, DateTime dtEventoFim, string nrCep, string noLocal, bool fgContaFalta, string noHistorico, bool fgPermiteDiariaDup)
        {
            var e = db.SingleById<Evento>(idEvento);

            e.NoEvento = noEvento;
            e.NoHistorico = noHistorico;
            e.DtEvento = dtEvento;
            e.DtEventoFim = dtEventoFim;
            e.NrCep = nrCep;
            e.NoLocal = noLocal;
            e.FgContaFalta = fgContaFalta;
            e.FgPermiteDiariaDup = fgPermiteDiariaDup;

            db.AuditUpdate(e);
        }

        public void AlteraEventoBasico(int idEvento, string noEvento, string noLocal)
        {
            var e = db.SingleById<Evento>(idEvento);

            e.NoEvento = noEvento;
            e.NoLocal = noLocal;

            db.AuditUpdate(e);
        }

        /// <summary>
        /// Exclui um evento - só permite exclusão se ainda não foi emitida as nerps
        /// </summary>
        public void ExcluiEvento(int idEvento)
        {
            //TODO: testar se já foi emitida nerp
            using (var trans = db.GetTransaction())
            {
                db.DeleteMany<EventoParticipante>().Where(x => x.IdEvento == idEvento).Execute();

                db.AuditDelete<Evento>(idEvento);

                var doc = new DocumentoService(context);
                doc.ExcluiDocumento(TpRefDoc.Evento, idEvento);

                trans.Complete();
            }
        }

        /// <summary>
        /// Adiciona um membro ao evento (não inclui duplicidade do titular)
        /// </summary>
        public int AdicionaMembros(int idEvento, int idCentroCusto, string tpMandato, List<EventoConvidadoDTO> lista)
        {
            var count = 0;

            using (var trans = db.GetTransaction())
            {
                foreach (var p in lista)
                {
                    var dup = db.Query<EventoParticipante>()
                        .Where(x => x.IdEvento == idEvento && x.IdPessoaTit == p.IdPessoaTit)
                        .Count();

                    if (dup > 0) continue; // não inclui duplicidade de titular (não valida suplente)

                    var e = new EventoParticipante
                    {
                        IdEvento = idEvento,
                        IdCentroCusto = idCentroCusto,
                        TpMandato = tpMandato,
                        IdPessoaTit = p.IdPessoaTit,
                        IdPessoaSup = p.IdPessoaSup,
                        IdMandatoTit = p.IdMandatoTit,
                        IdMandatoSup = p.IdMandatoSup,
                        TpPresencaTit = p.FgLicencaTit ? TpPresenca.Licenciado : null,
                        TpPresencaSup = p.FgLicencaSup ? TpPresenca.Licenciado : null,
                        NrOrdem = p.NrOrdem
                    };

                    db.Insert(e);
                    count++;
                }

                trans.Complete();
            }

            return count;
        }

        /// <summary>
        /// Exclui participantes de um evento - não permite apos emissão das nerps
        /// </summary>
        public void ExcluiParticipantes(int[] idParticipantes)
        {
            using (var trans = db.GetTransaction())
            {
                foreach (var idParticipante in idParticipantes)
                {
                    var nerpItem = db.Query<NerpItem>()
                        .Include(x => x.Nerp)
                        .Include(x => x.PessoaCredor)
                        .Where(x => x.IdParticipante == idParticipante)
                        .Where(x => x.Nerp.TpSituacao != NerpSituacao.Cancelada)
                        .ToList();

                    if (nerpItem.Count > 0)
                    {
                        throw new NBoxException("Já existe uma NERP emitida para {0} e não é possível excluir deste evento", nerpItem.First().PessoaCredor.NoPessoa);
                    }

                    // exclui o participante das nerp (apenas NERP canceladas)
                    db.DeleteMany<NerpItem>()
                        .Where(x => x.IdParticipante == idParticipante)
                        .Execute();

                    var participante = db.SingleById<EventoParticipante>(idParticipante);

                    db.AuditDelete<EventoParticipante>(idParticipante, typeof(Evento), participante.IdEvento);
                }

                trans.Complete();
            }
        }

        /// <summary>
        /// Registra as presenças dos participantes de um evento
        /// </summary>
        public void RegistraPresenca(int idEvento, List<EventoParticipante> participantes)
        {
            var diretoriasrv = new DiretoriaService(context);

            var e = db.SingleById<Evento>(idEvento);

            var lic = db.Query<Licenca>()
                .WherePeriodo(Periodo.UmDia(e.DtEvento))
                .ToList();

            var presidenteExercicio = diretoriasrv.BuscaPresidenteExercicio(e.DtEvento);

            using (var trans = db.GetTransaction())
            {
                foreach(var p in participantes)
                {
                    var orig = db.SingleById<EventoParticipante>(p.IdParticipante);

                    // faz validações APENAS se houve alteração
                    if (orig.TpPresencaTit == p.TpPresencaTit && orig.TpPresencaSup == p.TpPresencaSup) continue;

                    if (p.IdPessoaTit != presidenteExercicio?.Pessoa?.IdPessoa)
                    {
                        if (lic.Count(x => x.IdPessoa == p.IdPessoaTit) > 0 && p.TpPresencaTit != TpPresenca.Licenciado)
                        {
                            throw new NBoxException("{0} está afastado na data do evento", db.SingleById<Pessoa>(p.IdPessoaTit).NoPessoa);
                        }
                    }                    

                    db.Update(p);
                }

                trans.Complete();
            }
        }

        /// <summary>
        /// Retorna os totalizadores de presença de um evento
        /// </summary>
        public Dictionary<string, int> AgrupaPresenca(int idEvento)
        {
            var participantes = db.Query<EventoParticipante>()
                .Where(x => x.IdEvento == idEvento)
                .ToList();

            var dict = new Dictionary<string, int>();

            dict["PRESENTE_TIT"] = participantes.Where(x => x.TpPresencaTit == TpPresenca.Presente).Count();
            dict["LICENCIADO_TIT"] = participantes.Where(x => x.TpPresencaTit == TpPresenca.Licenciado).Count();
            dict["JUSTIFICADO_TIT"] = participantes.Where(x => x.TpPresencaTit == TpPresenca.Justificado).Count();
            dict["FALTA_TIT"] = participantes.Where(x => x.TpPresencaTit == TpPresenca.Falta).Count();

            dict["PRESENTE_SUP"] = participantes.Where(x => x.TpPresencaSup == TpPresenca.Presente).Count();
            dict["LICENCIADO_SUP"] = participantes.Where(x => x.TpPresencaSup == TpPresenca.Licenciado).Count();
            dict["JUSTIFICADO_SUP"] = participantes.Where(x => x.TpPresencaSup == TpPresenca.Justificado).Count();
            dict["FALTA_SUP"] = participantes.Where(x => x.TpPresencaSup == TpPresenca.Falta).Count();

            dict["SUPLENTES"] = participantes.Where(x => x.IdPessoaSup != null).Count();

            return dict;
        }
            
        /// <summary>
        /// Lista os eventos dentro o periodo informado
        /// </summary>
        public List<Evento> ListaEventos(Periodo periodo, string noEvento, string noLocal)
        {
            return db.Query<Evento>()
                .Include(x => x.Cep, JoinType.Left)
                .Where(x => x.NrAno == nrAnoBase)
                .Where(x => x.DtEvento >= periodo.Inicio && x.DtEvento <= (periodo.EmAberto ? periodo.Fim : periodo.Fim.AddDays(1))) // +1 dia por causa da hora
                .Where(noEvento != null, x => x.NoEvento.StartsWith(noEvento.ToLike()))
                .Where(noLocal != null, x => x.NoLocal.StartsWith(noLocal.ToLike()))
                .OrderBy(x => x.DtEvento).ThenBy(x => x.NoEvento)
                .ToList();
        }
            
        /// <summary>
        /// Retorna um evento com ou sem os participantes
        /// </summary>
        public Evento BuscaEvento(int idEvento, bool incluiParticipantes)
        {
            var e = db.Query<Evento>()
                .Include(x => x.Cep, JoinType.Left)
                .Where(x => x.NrAno == nrAnoBase)
                .Single(x => x.IdEvento == idEvento);

            if (incluiParticipantes) e.Participantes = ListaParticipantes(idEvento, false);

            return e;
        }
            
        /// <summary>
        /// Busca um evento com as informações dos participantes
        /// </summary>
        public List<EventoParticipante> ListaParticipantes(int idEvento, bool incluiFaltas, TpPessoa? tpPessoa = null)
        {   
            var e = db.SingleById<Evento>(idEvento);
            var pessoas = new PessoaService(context);
            var lic = db.Query<Licenca>()
                .WherePeriodo(Periodo.UmDia(e.DtEvento))
                .ToList();
            
            // busca os participantes do evento incluindo os licencas
            var participantes = db.Query<EventoParticipante>()
                .Include(x => x.Evento)
                .Include(x => x.Evento.Cep, JoinType.Left)
                .Include(x => x.CentroCusto)
                .Include(x => x.Titular)
                .Include(x => x.Titular.Cep)
                .Include(x => x.Suplente, JoinType.Left)
                .Include(x => x.Suplente.Cep, JoinType.Left)
                .Where(x => x.IdEvento == idEvento)
                .OrderBy(x => x.CentroCusto.NrCentroCusto)
                .ThenBy(x => x.NrOrdem)
                .ThenBy(x => x.Titular.NoPessoa)
                .ToList()
                .ForEach((x, i) => x.LicencaTit = lic.FirstOrDefault(l => l.IdPessoa == x.IdPessoaTit)?.GetPeriodo() ?? Periodo.Vazio)
                .ForEach((x, i) => x.LicencaSup = lic.FirstOrDefault(l => l.IdPessoa == x.IdPessoaSup)?.GetPeriodo() ?? Periodo.Vazio);

            if (tpPessoa != null)
            {
                return participantes
                    .Where(x => pessoas.BuscaTipoPessoa(x.Titular, e.DtEvento) == tpPessoa)
                    .ToList();
            }
            else
            {
                return participantes;
            }
        }

        /// <summary>
        /// Retorna a string que escreve a presenca do participante no evento
        /// </summary>
        public string GetPresenca(string tpPresenca)
        {
            return
                tpPresenca == TpPresenca.Presente ? "PRESENTE" :
                tpPresenca == TpPresenca.Licenciado ? "LICENCIADO" :
                tpPresenca == TpPresenca.Justificado ? "JUSTIFICADO" :
                tpPresenca == TpPresenca.Falta ? "FALTA" : "";
        }

        /// <summary>
        /// Faz uma busca por convidados a serem adicionados em um evento conforme o tipo de mandato
        /// </summary>
        public IEnumerable<EventoConvidadoDTO> ListaConvidados(int idEvento, string tpMandato, string noPessoa,
            int? cdCamara, int? idComissao, int? idGrupo, int? idInspetoria, string cdCargo, int? idGerencia)
        {
            var e = db.SingleById<Evento>(idEvento);
            IEnumerable<EventoConvidadoDTO> lista = null;

            switch(tpMandato)
            {
                case TpMandato.Conselheiro:
                    lista = new ConselheiroService(context).ListaPlenario(Periodo.UmDia(e.DtEvento), null, null, noPessoa, cdCamara)
                        .Where(x => x.Titular != null)
                        .Select(x => new EventoConvidadoDTO
                        {
                            IdPessoaTit = x.Titular.IdPessoa,
                            IdMandatoTit = x.Titular.IdMandato,
                            NoPessoaTit = x.Titular.Pessoa.NoPessoa,
                            NoTituloTit = x.Titular.Pessoa.NoTituloAbr,
                            FgLicencaTit = x.Titular.Licencas.Count > 0,
                            IdPessoaSup = x.Suplente?.IdPessoa,
                            IdMandatoSup = x.Suplente?.IdMandato,
                            NoPessoaSup = x.Suplente?.Pessoa.NoPessoa,
                            NoTituloSup = x.Suplente?.Pessoa.NoTituloAbr,
                            FgLicencaSup = (x.Suplente?.Licencas.Count ?? 0) > 0
                        });
                    break;

                case TpMandato.Camara:
                    lista = new CamaraRepService(context).ListaConselheiros(Periodo.UmDia(e.DtEvento), cdCamara)
                        .Where(x => x.Pessoa != null)
                        .Where(x => x.CdCargo == CdCargo.Coordenador || x.CdCargo == CdCargo.Adjunto)
                        .OrderBy(x => x.Pessoa.NoPessoa)
                        .Select(x => new EventoConvidadoDTO
                        {
                            IdPessoaTit = x.IdPessoa,
                            IdMandatoTit = x.IdMandato,
                            NoPessoaTit = x.Pessoa.NoPessoa,
                            NoTituloTit = x.Pessoa.NoTituloAbr,
                            FgLicencaTit = x.Licencas.Count > 0
                        });
                    break;

                case TpMandato.Representante:
                    lista = new CamaraRepService(context).ListaConselheiros(Periodo.UmDia(e.DtEvento), cdCamara)
                        .Where(x => x.Pessoa != null)
                        .Where(x => x.CdCargo == CdCargo.Representante)
                        .OrderBy(x => x.Pessoa.NoPessoa)
                        .Select(x => new EventoConvidadoDTO
                        {
                            IdPessoaTit = x.IdPessoa,
                            IdMandatoTit = x.IdMandato,
                            NoPessoaTit = x.Pessoa.NoPessoa + " :: " + x.Camara.NoCamara,
                            NoTituloTit = x.Pessoa.NoTituloAbr,
                            FgLicencaTit = x.Licencas.Count > 0
                        });
                    break;

                case TpMandato.Diretor:
                    lista = new DiretoriaService(context).ListaDiretores(Periodo.UmDia(e.DtEvento),true)
                        .Where(x => x.Pessoa != null)
                        .Select(x => new EventoConvidadoDTO
                        {
                            IdPessoaTit = x.IdPessoa,
                            IdMandatoTit = x.IdMandato,
                            NoPessoaTit = x.Pessoa.NoPessoa,
                            NoTituloTit = x.Pessoa.NoTituloAbr,
                            FgLicencaTit = x.Licencas.Count > 0
                        });
                    break;

                case TpMandato.Comissao:
                    lista = new ComissaoService(context).ListaMembros(idComissao.Value, Periodo.UmDia(e.DtEvento))
                        .Where(x => x.Titular?.Pessoa != null)
                        .Select(x => new EventoConvidadoDTO
                        {
                            IdPessoaTit = x.Titular.IdPessoa,
                            IdMandatoTit = x.Titular.IdMandato,
                            NoPessoaTit = x.Titular.Pessoa.NoPessoa,
                            NoTituloTit = x.Titular.Pessoa.NoTituloAbr,
                            FgLicencaTit = x.Titular.Licencas.Count > 0,
                            IdPessoaSup = x.Suplente?.IdPessoa,
                            IdMandatoSup = x.Suplente?.IdMandato,
                            NoPessoaSup = x.Suplente?.Pessoa.NoPessoa ?? (x.Titular.NrOrdem > 0 ? x.Titular.NrOrdem + "º Suplente" : null),
                            NoTituloSup = x.Suplente?.Pessoa.NoTituloAbr,
                            FgLicencaSup = (x.Suplente?.Licencas.Count ?? 0) > 0,
                            NrOrdem = x.Titular.NrOrdem
                        })
                        .OrderBy(x => x.NrOrdem).ThenBy(x => x.NoPessoaTit);
                    break;

                case TpMandato.Grupo:
                    lista = new GrupoService(context).ListaMembros(idGrupo.Value, Periodo.UmDia(e.DtEvento))
                        .Where(x => x.Pessoa != null)
                        .OrderBy(x => x.Pessoa.NoPessoa)
                        .Select(x => new EventoConvidadoDTO
                        {
                            IdPessoaTit = x.Pessoa.IdPessoa,
                            IdMandatoTit = x.IdMandato,
                            NoPessoaTit = x.Pessoa.NoPessoa,
                            NoTituloTit = x.Pessoa.NoTituloAbr,
                            FgLicencaTit = x.Licencas.Count > 0
                        });
                    break;

                case TpMandato.Inspetor:
                    List<int> unidades = new List<int>();

                    var itens = new InspetorService(context)
                        .ListaInspetores(Pager.NoPage, Periodo.UmDia(e.DtEvento), noPessoa, null, idInspetoria, cdCamara, cdCargo, true)
                        .Items;

                    //Se houver uma gerênia selecionada e não houver um inspetoria/município
                    if (idGerencia.HasValue && !idInspetoria.HasValue)
                    {
                        //Busca todas as unidades filhas da gerência selecionada
                        unidades = new UnidadeService(context).ListaUnidadesFilhas(idGerencia.Value).Select(x => x.CdUnidade).ToList();
                        //Filtra a lista de inspetores pela unidade das inspetorias.
                        itens = itens.Where(x => unidades.Contains(x.Inspetoria.CdUnidade)).ToList();
                    }

                    lista = itens.Select(x => new EventoConvidadoDTO
                        {
                            IdPessoaTit = x.IdPessoa,
                            IdMandatoTit = x.IdMandato,
                            NoPessoaTit = x.Pessoa.NoPessoa,
                            NoTituloTit = x.Pessoa.NoTituloAbr,
                            FgLicencaTit = x.Licencas.Count > 0
                        });
                    break;

                case TpMandato.Indicacao:
                    var indicacoes = new IndicacaoService(context);
                    lista = indicacoes.ListaPlenarioFuturo(false, e.DtEvento.Year, false, null, noPessoa, cdCamara)
                        .Where(x => x.IdPessoaTit != null)
                        .Select(x => new EventoConvidadoDTO
                        {
                            IdPessoaTit = x.IdPessoaTit.Value,
                            NoPessoaTit = x.Titular.NoPessoa,
                            NoTituloTit = x.Titular.NoTituloAbr
                        })
                        .Union(indicacoes.ListaPlenarioFuturo(false, e.DtEvento.Year, false, null, noPessoa, cdCamara)
                            .Where(x => x.IdPessoaSup != null)
                            .Select(x => new EventoConvidadoDTO
                            {
                                IdPessoaTit = x.IdPessoaSup.Value,
                                NoPessoaTit = x.Suplente.NoPessoa,
                                NoTituloTit = x.Suplente.NoTituloAbr
                            })).OrderBy(x => x.NoPessoaTit);
                    break;

                case TpMandato.Funcionario: // Apenas quem tem numero de matricula
                    lista = db.Query<Pessoa>()
                        .Where(x => x.NrMatricula != null)
                        .Where(noPessoa != null, x => x.NoPessoa.StartsWith(noPessoa.ToLike()))
                        .OrderBy(x => x.NoPessoa)
                        .ToEnumerable()
                        .Select(x => new EventoConvidadoDTO
                        {
                            IdPessoaTit = x.IdPessoa,
                            NoPessoaTit = x.NoPessoa + " (CPF: " + x.NrCpfFmt + ")",
                            NoTituloTit = x.NoTituloAbr
                        });
                    break;

                case TpMandato.Outro: // Não tem vinculo com mandato (qualquer pessoa)
                    lista = db.Query<Pessoa>()
                        .Where(x => x.NoPessoa.StartsWith(noPessoa.ToLike()))
                        .OrderBy(x => x.NoPessoa)
                        .ToEnumerable()
                        .Select(x => new EventoConvidadoDTO
                        {
                            IdPessoaTit = x.IdPessoa,
                            NoPessoaTit = x.NoPessoa + " (CPF: " + x.NrCpfFmt + ")",
                            NoTituloTit = x.NoTituloAbr
                        }).Take(1000);
                    break;

            }

            // marca os participantes que já estão adicionados no evento
            var participantes = db.Query<EventoParticipante>()
                .Where(x => x.IdEvento == idEvento)
                .ProjectTo(x => x.IdPessoaTit);

            foreach(var c in lista)
            {
                c.FgConvidado = participantes.Contains(c.IdPessoaTit);
                yield return c;
            }
        }

        #region CRUD: Centro de Custo padrão

        /// <summary>
        /// Busca o centro de custo padrão conforme o tipo de mandato/id de refencia
        /// </summary>
        public CentroCusto BuscaCentroCustoPadrao(string tpMandato, int? idRef)
        {
            return db.Query<EventoCentroCusto>()
                .Include(x => x.CentroCusto)
                .Where(x => x.CentroCusto.NrAno == nrAnoBase)
                .Where(x => x.TpMandato == tpMandato && x.IdRef == idRef)
                .FirstOrDefault()?.CentroCusto;
        }

        /// <summary>
        /// Lista os centros de custos padrões para o usuario editar os valores
        /// </summary>
        public IEnumerable<EventoCentroCusto> ListaCentroCustoPadrao()
        {
            var list = db.Query<EventoCentroCusto>()
                .Include(x => x.CentroCusto)
                .Where(x => x.CentroCusto.NrAno == nrAnoBase)
                .ToList();

            var grupos = new GrupoService(context).ListaGrupos(Periodo.Hoje, null).ToList();
            var comissoes = new ComissaoService(context).ListaComissoes(Periodo.Hoje, null).ToList();
            var camaras = new CamaraService(context).ListaCamaras(Periodo.Hoje).ToList();

            // Plenario
            {
                var e = list.FirstOrDefault(x => x.TpMandato == TpMandato.Conselheiro && x.IdRef == null) ??
                    new EventoCentroCusto { TpMandato = TpMandato.Conselheiro };
                e.NoRef = "PLENÁRIO";
                yield return e;
            }
            // Diretoria
            {
                var e = list.FirstOrDefault(x => x.TpMandato == TpMandato.Diretor) ??
                    new EventoCentroCusto { TpMandato = TpMandato.Diretor };
                e.NoRef = "DIRETORIA";
                yield return e;
            }
            // Inspetores
            {
                var e = list.FirstOrDefault(x => x.TpMandato == TpMandato.Inspetor) ??
                    new EventoCentroCusto { TpMandato = TpMandato.Inspetor };
                e.NoRef = "INSPETORES";
                yield return e;
            }
            // Camara (como conselheiro, por isso o TpMandato == C)
            foreach (var camara in camaras)
            {
                var e = list.FirstOrDefault(x => x.TpMandato == TpMandato.Conselheiro && x.IdRef == camara.CdCamara) ??
                    new EventoCentroCusto { TpMandato = TpMandato.Conselheiro, IdRef = camara.CdCamara };
                e.NoRef = camara.NoCamara;
                e.NoCategoria = "CÂMARAS ESPECIALIZADAS";
                yield return e;
            }
            // Comissões
            foreach (var comissao in comissoes)
            {
                var e = list.FirstOrDefault(x => x.TpMandato == TpMandato.Comissao && x.IdRef == comissao.IdComissao) ??
                    new EventoCentroCusto { TpMandato = TpMandato.Comissao, IdRef = comissao.IdComissao };
                e.NoRef = comissao.NoComissao;
                e.NoCategoria = "COMISSÕES";
                yield return e;
            }
            // Grupos
            foreach (var grupo in grupos)
            {
                var e = list.FirstOrDefault(x => x.TpMandato == TpMandato.Grupo && x.IdRef == grupo.IdGrupo) ??
                    new EventoCentroCusto { TpMandato = TpMandato.Grupo, IdRef = grupo.IdGrupo };
                e.NoRef = grupo.NoGrupo;
                e.NoCategoria = "GRUPOS DE TRABALHO";
                yield return e;
            }
        }

        /// <summary>
        /// Atualiza os centros de custo padrão
        /// </summary>
        public void AtualizaCentroCustos(EventoCentroCusto item)
        {
            if (item.NrCentroCusto != null)
            {
                db.Save(item.IdEventoCentroCusto == 0, item);
            }
            else if (item.IdEventoCentroCusto > 0)
            {
                db.Delete(item);
            }
        }

        #endregion
    }
}