﻿using NBox.Services;
using NBox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using NPoco;
using Creasp.Core;
using NPoco.Linq;
using System.Data;

namespace Creasp.Siplen
{
    public class GrupoService : MandatoService
    {
        public GrupoService(ServiceContext context) : base(context) { }

        /// <summary>
        /// Inclui um novo grupo
        /// </summary>
        public int IncluiGrupo(string noGrupo, bool fgTecnico, int? cdCamara, Periodo periodo, string noHistorico)
        {
            var grupo = new Grupo
            {
                NoGrupo = noGrupo,
                FgTecnico = fgTecnico,
                CdCamara = cdCamara,
                DtIni = periodo.Inicio,
                DtFim = periodo.Fim,
                NoHistorico = noHistorico
            };

            db.Insert(grupo);

            return grupo.IdGrupo;
        }

        /// <summary>
        /// Permite alterar nome ou extender o prazo de um grupo. Se grupo técnico, valida mandato dos conselheiros
        /// </summary>
        public void AlteraGrupo(int idGrupo, string noGrupo, Periodo periodo, string noHistorico)
        {
            var grupo = db.Query<Grupo>()
                .IncludeMany(x => x.Membros)
                .Single(x => x.IdGrupo == idGrupo);

            var cons = new ConselheiroService(context);

            using (var trans = db.GetTransaction())
            {
                // atualiza as datas dos membros
                foreach(var membro in grupo.Membros)
                {
                    if (membro.DtFim == grupo.DtFim || membro.DtFim > periodo.Fim)
                    {
                        membro.DtFim = periodo.Fim;
                    }
                    if (membro.DtIni == grupo.DtIni || membro.DtIni < periodo.Inicio)
                    {
                        membro.DtIni = periodo.Inicio;
                    }

                    // se grupo técnico, valida se o mandato está dentro do periodo
                    if (grupo.FgTecnico)
                    {
                        var titular = cons.BuscaMandatoTitular(membro.IdPessoa, periodo.Inicio);

                        if(!titular.GetPeriodo().NoIntervalo(periodo))
                        {
                            throw new NBoxException("O mandato de titular de {0} não suporta o período informado", db.SingleById<Pessoa>(membro.IdPessoa).NoPessoa);
                        }
                    }

                    db.Update(membro);
                }

                grupo.DtIni = periodo.Inicio;
                grupo.DtFim = periodo.Fim;
                grupo.NoGrupo = noGrupo;
                grupo.NoHistorico = noHistorico;

                db.AuditUpdate(grupo);

                trans.Complete();
            }
        }

        /// <summary>
        /// Exclui um grupo e seus membros
        /// </summary>
        public void ExcluiGrupo(int idGrupo)
        {
            using (var trans = db.GetTransaction())
            {
                db.DeleteMany<Mandato>().Where(x => x.TpMandato == TpMandato.Grupo && x.IdGrupo == idGrupo).Execute();

                db.AuditDelete<Grupo>(idGrupo);

                trans.Complete();
            }
        }

        /// <summary>
        /// Inclui um novo membro no grupo que ja existe
        /// </summary>
        public void IncluiMembro(int idGrupo, string cdCargo, int idPessoa, Periodo periodo)
        {
            var grupo = db.Query<Grupo>()
                .Include(x => x.Camara, JoinType.Left)
                .Single(x => x.IdGrupo == idGrupo);

            // o periodo deve estar dentro do periodo do grupo
            if(grupo.GetPeriodo().NoIntervalo(periodo) == false) throw new NBoxException("O período {0} não está dentro do tempo do grupo {1}", periodo, grupo.GetPeriodo());

            // não permite 2 coordenadores/adjuntos no mesmo grupo
            if (cdCargo == CdCargo.Coordenador || cdCargo == CdCargo.Adjunto) ValidaMesmoCargoPeriodo(idGrupo, cdCargo, periodo);

            // valida se a pessoa já não está no grupo
            ValidaMandatoRepetido(idPessoa, idGrupo, periodo);

            int? cdCamara = null;

            // se grupo tecnico, deve validar mandado de conselheiro
            if (grupo.FgTecnico)
            {
                var cons = new ConselheiroService(context);

                // busca pelo mandato
                var titular = cons.BuscaMandatoTitular(idPessoa, periodo.Inicio);
                cdCamara = titular.CdCamara;

                if (periodo.Fim > titular.DtFim) throw new NBoxException("O período deve ser menor ou igual ao período de mandato do titular");

                // valida mesma camara
                if (cdCamara != grupo.CdCamara) throw new NBoxException("{0} não está na câmara {1}", db.SingleById<Pessoa>(idPessoa).NoPessoa, grupo.Camara.NoCamara);
            }

            var membro = new Mandato
            {
                TpMandato = TpMandato.Grupo,
                CdCargo = cdCargo,
                IdPessoa = idPessoa,
                DtIni = periodo.Inicio,
                DtFim = periodo.Fim,
                AnoIni = periodo.Inicio.Year,
                AnoFim = periodo.Inicio.Year,
                CdCamara = cdCamara,
                IdGrupo = idGrupo
            };

            db.Insert(membro);
        }

        /// <summary>
        /// Permite alterar o periodo de mandato no grupo
        /// </summary>
        public void AlteraMembro(int idMandato, Periodo periodo)
        {
            using (var trans = db.GetTransaction())
            {               
                var membro = db.SingleById<Mandato>(idMandato);

                membro.DtIni = periodo.Inicio;
                membro.DtFim = periodo.Fim;

                db.Update(membro);
                trans.Complete();
            }
        }

        /// <summary>
        /// Lista todos os grupos dentro do periodo informado
        /// </summary>
        public IEnumerable<Grupo> ListaGrupos(Periodo periodo, string noGrupo)
        {
            return db.Query<Grupo>()
                .WherePeriodo(periodo)
                .Where(!string.IsNullOrEmpty(noGrupo), x => x.NoGrupo.Contains(noGrupo.ToLike()))
                .OrderBy(x => x.NoGrupo)
                .ToEnumerable();
        }

        /// <summary>
        /// Lista os membros de um grupo
        /// </summary>
        public List<Mandato> ListaMembros(int idGrupo, Periodo periodo)
        {
            var grupo = db.SingleById<Grupo>(idGrupo);

            var lic = db.Query<Licenca>()
                .WherePeriodo(grupo.GetPeriodo())
                .WherePeriodo(periodo)
                .OrderBy(x => x.DtIni)
                .ToList();

            var membros = db.Query<Mandato>()
                .Include(x => x.Pessoa)
                .Include(x => x.Cargo)
                .Where(x => x.TpMandato == TpMandato.Grupo)
                .Where(x => x.IdGrupo == idGrupo)
                .WherePeriodo(periodo)
                .OrderBy(x => x.Cargo.NrOrdem).ThenBy(x => x.DtIni)
                .ToList()
                .ForEach((x, i) => x.Licencas = lic.Where(a => a.IdPessoa == x.IdPessoa).ToList());

            AdicionaCargosFaltantes(membros, CdCargo.Coordenador, CdCargo.Adjunto);

            return membros;
        }

        public List<Mandato> ListaMembrosSemCargosFaltantes(int idGrupo)
        {
            var grupo = db.SingleById<Grupo>(idGrupo);

            var lic = db.Query<Licenca>()
                .WherePeriodo(grupo.GetPeriodo())
                .OrderBy(x => x.DtIni)
                .ToList();

            var membros = db.Query<Mandato>()
                .Include(x => x.Pessoa)
                .Include(x => x.Cargo)
                .Where(x => x.TpMandato == TpMandato.Grupo)
                .Where(x => x.IdGrupo == idGrupo)
                .OrderBy(x => x.Cargo.NrOrdem).ThenBy(x => x.DtIni)
                .ToList()
                .ForEach((x, i) => x.Licencas = lic.Where(a => a.IdPessoa == x.IdPessoa).ToList());

            return membros;
        }

        public int QuantidadeDeMandatos(Periodo periodo)
        {
            return db.Query<Mandato>()
                .Include(x => x.Pessoa)
                .Include(x => x.Cargo)
                .Where(x => x.TpMandato == TpMandato.Grupo)
                .WherePeriodo(periodo)
                .OrderBy(x => x.Cargo.NrOrdem).ThenBy(x => x.DtIni)
                .ToList().Count;
        }

        #region Validações

        /// <summary>
        /// Valida se não existe um mesmo cargo no mesmo grupo num periodo existente
        /// </summary>
        private void ValidaMesmoCargoPeriodo(int idGrupo, string cdCargo, Periodo periodo)
        {
            var mandato = db.Query<Mandato>()
                .Include(x => x.Cargo)
                .Where(x => x.TpMandato == TpMandato.Grupo)
                .Where(x => x.CdCargo == cdCargo)
                .Where(x => x.IdGrupo == idGrupo)
                .WherePeriodo(periodo)
                .FirstOrDefault();

            if (mandato != null)
            {
                throw new NBoxException("Já existe um mandato de {0} no período {1} neste grupo de trabalho", mandato.Cargo.CdCargo, mandato.GetPeriodo());
            }
        }

        /// <summary>
        /// Verifica se a pessoa já não está neste grupo num periodo valido
        /// </summary>
        private void ValidaMandatoRepetido(int idPessoa, int idGrupo, Periodo periodo)
        {
            var mandato = db.Query<Mandato>()
                .Include(x => x.Pessoa)
                .Include(x => x.Cargo)
                .Where(x => x.TpMandato == TpMandato.Grupo)
                .Where(x => x.IdGrupo == idGrupo)
                .Where(x => x.IdPessoa == idPessoa)
                .WherePeriodo(periodo)
                .FirstOrDefault();

            if (mandato != null)
            {
                throw new NBoxException("{0} já é {1} no período {2} neste grupo de trabalho",
                    mandato.Pessoa.NoPessoa,
                    mandato.Cargo.CdCargo,
                    mandato.GetPeriodo());
            }
        }

        #endregion
    }
}