﻿using NBox.Services;
using NBox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using NPoco;
using Creasp.Core;
using NPoco.Linq;

namespace Creasp.Siplen
{
    public class InspetoriaService : DataService
    {
        public InspetoriaService(ServiceContext context) : base(context) { }

        public void Inclui(Inspetoria insp)
        {
            db.Insert(insp);
        }

        public void Altera(Inspetoria insp)
        {
            db.AuditUpdate(insp);
        }

        public void Exclui(int idInspetoria)
        {
            db.AuditDelete<Inspetoria>(idInspetoria);
        }
    }
}
