﻿using NBox.Services;
using NBox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using NPoco;
using Creasp.Core;
using NPoco.Linq;

namespace Creasp.Siplen
{
    public class ComissaoService : MandatoService
    {
        public ComissaoService(ServiceContext context) : base(context) { }

        public int IncluiComissao(string noComissao, bool fgPermanente, string tpSuplente, Periodo periodo, string noHistorico)
        {
            var comissao = new Comissao { NoComissao = noComissao, FgPermanente = fgPermanente, TpSuplente = tpSuplente, DtIni = periodo.Inicio, DtFim = periodo.Fim, NoHistorico = noHistorico };

            db.Insert(comissao);

            return comissao.IdComissao;
        }

        public void AlteraComissao(int idComissao, string noComissao, Periodo periodo, string noHistorico)
        {
            var comissao = db.SingleById<Comissao>(idComissao);
            var membros = db.Query<Mandato>()
                .Where(x => x.TpMandato == TpMandato.Comissao)
                .Where(x => x.IdComissao == idComissao)
                .ToEnumerable();

            foreach (var membro in membros)
            {
                if (!periodo.NoIntervalo(membro.GetPeriodo()))
                {
                    throw new NBoxException("Esta comissão possui um membro no período {0} e não pode ser alterada para estas datas", membro.GetPeriodo());
                }
            }

            comissao.NoComissao = noComissao;
            comissao.DtIni = periodo.Inicio;
            comissao.DtFim = periodo.Fim;
            comissao.NoHistorico = noHistorico;

            db.AuditUpdate(comissao);
        }

        /// <summary>
        /// Exclui uma comissão - não permite que seja excluida comissões que tenham membros
        /// </summary>
        public void ExcluiComissao(int idComissao)
        {
            if (db.FirstOrDefault<Mandato>(x => x.TpMandato == TpMandato.Comissao && x.IdComissao == idComissao) != null)
            {
                throw new NBoxException("Existem membros associados a esta comissão e ela não pode ser excluida");
            }

            db.AuditDelete<Comissao>(idComissao);
        }

        public void IncluiMembro(int idComissao, string cdCargo, int idPessoaTit, int? idPessoaSup, int nrOrdem, Periodo periodo)
        {
            using (var trans = db.GetTransaction())
            {
                // valida se tituar/suplente são a mesma pessoa
                if (idPessoaSup.HasValue && idPessoaTit == idPessoaSup) throw new NBoxException("Titular e suplente não podem ser a mesma pessoa");

                var comissao = db.SingleById<Comissao>(idComissao);

                // valida periodo do cargo vs periodo da comissão
                if (comissao.GetPeriodo().NoIntervalo(periodo) == false) throw new NBoxException("O período do cargo não está dentro do período da comissão ({0})", comissao.GetPeriodo());

                // não permite 2 coordenadores/adjuntos na mesma camara
                if (cdCargo == CdCargo.Coordenador || cdCargo == CdCargo.Adjunto) ValidaMesmoCargoPeriodo(idComissao, cdCargo, periodo);

                var cons = new ConselheiroService(context);

                // valida se titlar já não está nesta comissao
                ValidaMandatoRepetido(idPessoaTit, idComissao, periodo);

                // procura pelo mandato do titular na comissao (titular como conselheiro)
                var titular = cons.BuscaMandatoTitular(idPessoaTit, periodo.Inicio);
                var cdCamara = titular.CdCamara.Value;

                // inclui o mandato de titular
                var tit = new Mandato
                {
                    TpMandato = TpMandato.Comissao,
                    CdCargo = cdCargo,
                    IdPessoa = idPessoaTit,
                    DtIni = periodo.Inicio,
                    DtFim = periodo.FimMaisProximo(titular.DtFim),
                    AnoIni = periodo.Inicio.Year,
                    AnoFim = periodo.Inicio.Year,
                    CdCamara = cdCamara,
                    IdComissao = idComissao,
                    NrOrdem = cdCargo == CdCargo.Suplente && comissao.TpSuplente == TpSuplente.Substituicao ? nrOrdem : 0
                };

                db.Insert(tit);

                // verifica suplente
                if (idPessoaSup != null)
                {
                    // valida se titlar já não está nesta comissao
                    ValidaMandatoRepetido(idPessoaSup.Value, idComissao, periodo);

                    // procura pelo mandato do suplente na comissao (titular como conselheiro)
                    var suplente = cons.BuscaMandatoTitular(idPessoaSup.Value, periodo.Inicio);

                    // valida se tem a mesma camara do titular
                    if (suplente.CdCamara.Value != cdCamara)
                    {
                        throw new NBoxException("O suplente não está na mesma câmara especializada do titular");
                    }

                    // inclui mandato de suplente
                    var sup = new Mandato
                    {
                        TpMandato = TpMandato.Comissao,
                        CdCargo = CdCargo.Suplente,
                        IdPessoa = idPessoaSup.Value,
                        DtIni = periodo.Inicio,
                        DtFim = periodo.FimMaisProximo(suplente.DtFim, titular.DtFim), // considerar a data fim do titular
                        AnoIni = periodo.Inicio.Year,
                        AnoFim = periodo.Inicio.Year,
                        CdCamara = cdCamara,
                        IdComissao = idComissao,
                        IdMandatoTit = comissao.TpSuplente == TpSuplente.Direto ? tit.IdMandato : (int?)null
                    };

                    db.Insert(sup);
                }

                trans.Complete();
            }
        }

        /// <summary>
        /// Altera apenas o periodo de cargo na comissao (precisa validar o suplente também)
        /// </summary>
        public void AlteraMembro(int idMandato, Periodo periodo, Periodo periodoSup)
        {
            // esta logica não está 100%, pois não permite mais e um suplente por titular.
            using (var trans = db.GetTransaction())
            {
                var titular = db.SingleById<Mandato>(idMandato);
                var suplente = db.Query<Mandato>()
                    .Where(x => x.IdMandatoTit == idMandato)
                    .SingleOrDefault();

                titular.DtIni = periodo.Inicio;
                titular.DtFim = periodo.Fim;

                db.Update(titular);

                if (suplente != null)
                {
                    suplente.DtIni = periodoSup.Inicio;
                    suplente.DtFim = periodoSup.Fim;

                    db.Update(suplente);
                }

                trans.Complete();
            }
        }

        /// <summary>
        /// Lista apenas as comissões conforme periodo informado
        /// </summary>
        public IEnumerable<Comissao> ListaComissoes(Periodo periodo, string noComissao)
        {
            return db.Query<Comissao>()
                .Where(!string.IsNullOrEmpty(noComissao), x => x.NoComissao.Contains(noComissao.ToLike()))
                .WherePeriodo(periodo)
                .OrderBy(x => x.NoComissao)
                .ToEnumerable();
        }

        /// <summary>
        /// Lista os membros de uma comissão
        /// </summary>
        public IEnumerable<TitularSuplenteDTO> ListaMembros(int idComissao, Periodo periodo)
        {
            var srv = new MandatoService(context);

            var lic = db.Query<Licenca>()
                .WherePeriodo(periodo)
                .OrderBy(x => x.DtIni)
                .ToList();

            var comissao = db.SingleById<Comissao>(idComissao);

            var membros = db.Query<Mandato>()
                .Include(x => x.Pessoa)
                .Include(x => x.Camara)
                .Include(x => x.Cargo)
                .Where(x => x.TpMandato == TpMandato.Comissao)
                .Where(x => x.IdComissao == idComissao)
                .WherePeriodo(periodo)
                .OrderBy(x => x.Cargo.NrOrdem).ThenBy(x => x.NrOrdem).ThenBy(x => x.Pessoa.NoPessoa)
                .ToList()
                .ForEach((x, i) => x.Licencas = lic.Where(a => a.IdPessoa == x.IdPessoa).ToList());

            membros = srv.ContaFaltas(membros, TpMandato.Comissao, periodo.Fim, true).ToList();

            // adiciona os cargos não preenchidos no periodo para melhor exibir ao usuario
            AdicionaCargosFaltantes(membros, CdCargo.Coordenador, CdCargo.Adjunto);

            // se a comissão for de suplencia direta, monta lado a lado
            if (comissao.TpSuplente == TpSuplente.Direto)
            {
                return MontaTitularSuplente(membros)
                    .ToList()
                    .OrderBy(x => x.Titular.Cargo?.NrOrdem)
                    .ThenBy(x => x.Titular?.NrOrdem)
                    .ThenBy(x => x.Titular?.Pessoa?.NoPessoa);
            }
            else
            {
                return membros.Select(x => new TitularSuplenteDTO { IdMandatoTit = x.IdMandato, Titular = x })
                    .OrderBy(x => x.Titular.Cargo?.NrOrdem)
                    .ThenBy(x => x.Titular?.NrOrdem)
                    .ThenBy(x => x.Titular?.Pessoa?.NoPessoa);
            }
        }

        public IEnumerable<TitularSuplenteDTO> ListaMembrosSemCargosFaltantes(int idComissao, Periodo periodo)
        {
            var lic = db.Query<Licenca>()
                .WherePeriodo(periodo)
                .OrderBy(x => x.DtIni)
                .ToList();

            var comissao = db.SingleById<Comissao>(idComissao);

            var membros = db.Query<Mandato>()
                .Include(x => x.Pessoa)
                .Include(x => x.Camara)
                .Include(x => x.Cargo)
                .Where(x => x.TpMandato == TpMandato.Comissao)
                .Where(x => x.IdComissao == idComissao)
                .WherePeriodo(periodo)
                .OrderBy(x => x.Cargo.NrOrdem).ThenBy(x => x.NrOrdem).ThenBy(x => x.Pessoa.NoPessoa)
                .ToList()
                .ForEach((x, i) => x.Licencas = lic.Where(a => a.IdPessoa == x.IdPessoa).ToList());

            // se a comissão for de suplencia direta, monta lado a lado
            if (comissao.TpSuplente == TpSuplente.Direto)
            {
                return MontaTitularSuplente(membros)
                    .ToList()
                    .OrderBy(x => x.Titular.Cargo?.NrOrdem)
                    .ThenBy(x => x.Titular?.NrOrdem)
                    .ThenBy(x => x.Titular?.Pessoa?.NoPessoa);
            }
            else
            {
                return membros.Select(x => new TitularSuplenteDTO { IdMandatoTit = x.IdMandato, Titular = x })
                    .OrderBy(x => x.Titular.Cargo?.NrOrdem)
                    .ThenBy(x => x.Titular?.NrOrdem)
                    .ThenBy(x => x.Titular?.Pessoa?.NoPessoa);
            }
        }

        public int QuantidadeDeMandatos(Periodo periodo)
        {
            return db.Query<Mandato>()
                .Include(x => x.Pessoa)
                .Include(x => x.Camara)
                .Include(x => x.Cargo)
                .Where(x => x.TpMandato == TpMandato.Comissao)
                .WherePeriodo(periodo)
                .OrderBy(x => x.Cargo.NrOrdem).ThenBy(x => x.NrOrdem).ThenBy(x => x.Pessoa.NoPessoa)
                .ToList().Count;
        }

        public override void ExcluiMandato(int idMandato)
        {
            try
            {
                var sups = db.Query<Mandato>()
                    .Where(x => x.IdMandatoTit == idMandato)
                    .ToList();

                foreach (var sup in sups)
                {
                    db.AuditDelete<Mandato>(sup.IdMandato);
                }

                db.AuditDelete<Mandato>(idMandato);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                throw new NBoxException("Não é possível excluir um membro, pois ele está associado a um evento.");
            }
        }

        #region Validações

        /// <summary>
        /// Valida se não existe um mesmo cargo na mesma comissao num periodo existente
        /// </summary>
        private void ValidaMesmoCargoPeriodo(int idComissao, string cdCargo, Periodo periodo)
        {
            var mandato = db.Query<Mandato>()
                .Include(x => x.Cargo)
                .Where(x => x.TpMandato == TpMandato.Comissao)
                .Where(x => x.CdCargo == cdCargo)
                .Where(x => x.IdComissao == idComissao)
                .WherePeriodo(periodo)
                .FirstOrDefault();

            if (mandato != null)
            {
                throw new NBoxException("Já existe um mandato de {0} no período {1} nesta comissão", mandato.Cargo.CdCargo, mandato.GetPeriodo());
            }
        }

        /// <summary>
        /// Verifica se a pessoa já não está na comissão num periodo valido
        /// </summary>
        private void ValidaMandatoRepetido(int idPessoa, int idComissao, Periodo periodo)
        {
            var mandato = db.Query<Mandato>()
                .Include(x => x.Pessoa)
                .Include(x => x.Cargo)
                .Where(x => x.TpMandato == TpMandato.Comissao)
                .Where(x => x.IdComissao == idComissao)
                .Where(x => x.IdPessoa == idPessoa)
                .WherePeriodo(periodo)
                .FirstOrDefault();

            if (mandato != null)
            {
                throw new NBoxException("{0} já é {1} no período {2} nesta comissão",
                    mandato.Pessoa.NoPessoa,
                    mandato.Cargo.CdCargo,
                    mandato.GetPeriodo());
            }
        }

        #endregion
    }
}