﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NPoco;

namespace Creasp.Siplen
{
    public class CdCargo
    {
        public const string Titular = "TITULAR";
        public const string Suplente = "SUPLENTE";

        public const string Inspetor = "INSPETOR";
        public const string InspetorChefe = "INSPETOR-CHEFE";
        public const string InspetorEspecial = "INSPETOR-ESP";

        public const string Coordenador = "COORDENADOR";
        public const string Adjunto = "ADJUNTO";
        public const string Membro = "MEMBRO";
        public const string Representante = "REPRESENTANTE";

        // Presidente possui regras especificas
        public const string Presidente = "PRESIDENTE";

        // Numero de NrOrdem para diretor (MaiorIgual)
        public const int NrDiretor = 100;
    }

    public class TpMandato
    {
        public const string Conselheiro = "C";
        public const string Inspetor = "I";
        public const string Diretor = "D";
        public const string Comissao = "S";
        public const string Grupo = "G";
        public const string Camara = "R";

        // usados apenas para eventos (selecionar pessoas)
        public const string Representante = "0"; 
        public const string Funcionario = "F";
        public const string Indicacao = "N";
        public const string Outro = "X";
    }

    public class TpSuplente
    {
        public const string SemSuplente = "N";
        public const string Direto = "D";
        public const string Substituicao = "S";
    }

    public class TpPresenca
    {
        public const string Presente = "P";
        public const string Justificado = "J";
        public const string Licenciado = "L";
        public const string Falta = "F";
    }
}