﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using Creasp.Core;

namespace Creasp.Siplen
{
    [PrimaryKey("IdGrupo", AutoIncrement = true)]
    public class Grupo : IPeriodo, IAuditoria
    {
        public int IdGrupo { get; set; }

        public string NoGrupo { get; set; }
        public bool FgTecnico { get; set; }
        public int? CdCamara { get; set; }
        public DateTime DtIni { get; set; }
        public DateTime DtFim { get; set; }
        public string NoHistorico { get; set; }

        #region IAuditoria
        public DateTime DtCadastro { get; set; } = DateTime.Now;
        public DateTime? DtAlteracao { get; set; }
        public string LoginCadastro { get; set; }
        public string LoginAlteracao { get; set; }
        #endregion

        [Reference(ReferenceType.OneToOne, ColumnName = "CdCamara", ReferenceMemberName = "CdCamara")]
        public Camara Camara { get; set; }

        [Reference(ReferenceType.Many, ColumnName = "IdGrupo", ReferenceMemberName = "IdGrupo")]
        public List<Mandato> Membros { get; set; } = new List<Mandato>();

    }
}