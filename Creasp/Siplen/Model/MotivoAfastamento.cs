﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;

namespace Creasp.Siplen
{
    [PrimaryKey("CdMotivo", AutoIncrement = false)]
    public class MotivoAfastamento
    {
        public int CdMotivo { get; set; }

        public string NoMotivo { get; set; }
        public bool FgAtivo { get; set; } = true;
    }
}