﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using Creasp.Core;

namespace Creasp.Siplen
{
    [PrimaryKey("IdInspetoria", AutoIncrement = true)]
    public class Inspetoria : IPeriodo, IAuditoria
    {
        public int IdInspetoria { get; set; }

        public string NoInspetoria { get; set; }
        public string NrProcesso { get; set; }
        public DateTime? DtAprovacao { get; set; }
        public string NrDecisaoPlen { get; set; }
        public int CdUnidade { get; set; }
        public string NrTelefone { get; set; }
        public string NoEmail { get; set; }
        public string NrCep { get; set; }
        public string NoLogradouro { get; set; }
        public string NrEndereco { get; set; }
        public string NoComplemento { get; set; }
        public bool FgRepresentacao { get; set; }
        public DateTime DtIni { get; set; }
        public DateTime DtFim { get; set; }
        public string NoHistorico { get; set; }

        #region IAuditoria
        public DateTime DtCadastro { get; set; } = DateTime.Now;
        public DateTime? DtAlteracao { get; set; }
        public string LoginCadastro { get; set; }
        public string LoginAlteracao { get; set; }
        #endregion

        [Reference(ReferenceType.OneToOne, ColumnName = "CdUnidade", ReferenceMemberName = "CdUnidade")]
        public Unidade Unidade { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "NrCep", ReferenceMemberName = "NrCep")]
        public Cep Cep { get; set; }
    }
}