﻿
using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using Creasp.Core;

namespace Creasp.Siplen
{
    [PrimaryKey("IdMandato", AutoIncrement = true)]
    public class Mandato : IPeriodo, IAuditoria
    {
        public int IdMandato { get; set; }

        public string TpMandato { get; set; }
        public string CdCargo { get; set; }
        public int NrOrdem { get; set; } = 0;
        public int IdPessoa { get; set; }
        public DateTime DtIni { get; set; }
        public DateTime DtFim { get; set; }
        public int AnoIni { get; set; }
        public int AnoFim { get; set; }
        public int? CdCamara { get; set; }
        public int? IdEntidade { get; set; }
        public int? IdComissao { get; set; }
        public int? IdGrupo { get; set; }
        public int? IdInspetoria { get; set; }
        public int? IdMandatoTit { get; set; }
        public int? CdMotivo { get; set; }
        public string NoHistorico { get; set; }
        public bool FgValido { get; set; } = true;

        #region IAuditoria
        public DateTime DtCadastro { get; set; } = DateTime.Now;
        public DateTime? DtAlteracao { get; set; }
        public string LoginCadastro { get; set; }
        public string LoginAlteracao { get; set; }
        #endregion

        [Reference(ReferenceType.OneToOne, ColumnName = "CdCargo", ReferenceMemberName = "CdCargo")]
        public Cargo Cargo { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdPessoa", ReferenceMemberName = "IdPessoa")]
        public Pessoa Pessoa { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "CdCamara", ReferenceMemberName = "CdCamara")]
        public Camara Camara { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdEntidade", ReferenceMemberName = "IdEntidade")]
        public Entidade Entidade { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdComissao", ReferenceMemberName = "IdComissao")]
        public Comissao Comissao { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdGrupo", ReferenceMemberName = "IdGrupo")]
        public Grupo Grupo { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdInspetoria", ReferenceMemberName = "IdInspetoria")]
        public Inspetoria Inspetoria { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "CdMotivo", ReferenceMemberName = "CdMotivo")]
        public MotivoAfastamento Motivo { get; set; }

        [Ignore]
        public List<Licenca> Licencas { get; set; } = new List<Licenca>();

        /// <summary>
        /// Para vinculo ao mandato de supente conforme data (exibido na tela de Camaras Especializadas)
        /// </summary>
        [Ignore]
        public Mandato Titular { get; set; }

        /// <summary>
        /// Quantidade de faltas do titular na data informada.
        /// </summary>
        [Ignore]
        public int NrFaltas { get; set; }

        /// <summary>
        /// Quantidade de faltas justificadas do titular na data informada.
        /// </summary>
        [Ignore]
        public int NrFaltasJustificadas { get; set; }

        /// <summary>
        /// Indica que o mandato é apenas uma indicação - não possui periodo definido
        /// </summary>
        [Ignore]
        public bool IsIndicado => this.GetPeriodo().IsVazio;

        /// <summary>
        /// Exibe mensagem de validação
        /// </summary>
        [Ignore]
        public string Mensagem { get; set; }
    }
}