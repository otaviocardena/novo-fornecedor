﻿using System;
using System.Collections.Generic;
using Creasp.Core;
using NPoco;

namespace Creasp.Siplen
{
    [PrimaryKey("IdComissao", AutoIncrement = true)]
    public class Comissao : IPeriodo, IAuditoria
    {
        public int IdComissao { get; set; }

        public string NoComissao { get; set; }
        public bool FgPermanente { get; set; }
        public string TpSuplente { get; set; }
        public DateTime DtIni { get; set; }
        public DateTime DtFim { get; set; }
        public string NoHistorico { get; set; }

        #region IAuditoria
        public DateTime DtCadastro { get; set; } = DateTime.Now;
        public DateTime? DtAlteracao { get; set; }
        public string LoginCadastro { get; set; }
        public string LoginAlteracao { get; set; }
        #endregion

        [Reference(ReferenceType.Many, ColumnName = "IdComissao", ReferenceMemberName = "IdComissao")]
        public List<Mandato> Membros { get; set; } = new List<Mandato>();

        /// <summary>
        /// Utilizado para exibir mensagens pro usuario de validação de uma comissão
        /// </summary>
        [Ignore]
        public string Mensagem { get; set; }
    }
}