﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using Creasp.Core;

namespace Creasp.Siplen
{
    [PrimaryKey("CdCargo", AutoIncrement = false)]
    public class Cargo : IPeriodo
    {
        public string CdCargo { get; set; }

        public string NoCargo { get; set; }
        public int NrOrdem { get; set; }
        public DateTime DtIni { get; set; }
        public DateTime DtFim { get; set; }
    }
}