﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using Creasp.Core;

namespace Creasp.Siplen
{
    [PrimaryKey("IdLicenca", AutoIncrement = true)]
    public class Licenca : IPeriodo, IAuditoria
    {
        public int IdLicenca { get; set; }

        public int IdPessoa { get; set; }
        public DateTime DtIni { get; set; }
        public DateTime DtFim { get; set; }
        public string NoMotivo { get; set; }

        #region IAuditoria
        public DateTime DtCadastro { get; set; } = DateTime.Now;
        public DateTime? DtAlteracao { get; set; }
        public string LoginCadastro { get; set; }
        public string LoginAlteracao { get; set; }
        #endregion

        [Ignore]
        public string NoMotivoFmt { get; } = "LICENÇA";

        [Reference(ReferenceType.OneToOne, ColumnName = "IdPessoa", ReferenceMemberName = "IdPessoa")]
        public Pessoa Pessoa { get; set; }

        [Ignore]
        public Documento Documento { get; set; }

    }
}