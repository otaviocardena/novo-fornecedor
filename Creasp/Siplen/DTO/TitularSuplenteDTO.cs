﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp.Core;
using Creasp;
using NPoco;

namespace Creasp.Siplen
{
    public class TitularSuplenteDTO
    {
        public int? IdMandatoTit { get; set; }
        public int? IdMandatoSup { get; set; }
        public Mandato Titular { get; set; }
        public Mandato Suplente { get; set; }

        /// <summary>
        /// Relacionamentos em que Titular/Suplente devem ser iguais usado no Plenario e Comissoes
        /// </summary>
        public Camara Camara => Titular.Camara;
        public Entidade Entidade => Titular.Entidade;
        public Comissao Comissao => Titular.Comissao;
        public int AnoIni => Titular.AnoIni;
        public int AnoFim => Titular.AnoFim;
    }
}