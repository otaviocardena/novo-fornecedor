﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp.Core;
using NPoco;

namespace Creasp.Siplen
{
    /// <summary>
    /// Classe com as informações sobre os documentos faltantes para a posse de um inspetor/conselheiro
    /// </summary>
    public class EmailPosseDTO
    {
        public string NomePessoa { get; set; }
        public string Creasp { get; set; }
        public string Cargo { get; set; }
        public string Camara { get; set; }
        public string Inspetoria { get; set; }
        public string Entidade { get; set; }
        public string[] DocumentosFaltantes { get; set; }
        public bool DocumentacaoOK { get; set; }
        public bool EmDia { get; set; }
        public bool EmDebito { get; set; }
    }
}