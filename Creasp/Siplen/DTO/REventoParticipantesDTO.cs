﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp.Core;
using NPoco;

namespace Creasp.Siplen
{
    /// <summary>
    /// Classe para relatorio de participantes de um evento
    /// </summary>
    public class REventoParticipantesDTO
    {
        public int IdEvento { get; set; }
        public string NoEvento { get; set; }

        public string NoTitular { get; set; }
        public string NoTituloTit { get; set; }
        public string NoPresencaTit { get; set; }

        public string NoSuplente { get; set; }
        public string NoTituloSup { get; set; }
        public string NoPresencaSup { get; set; }
    }
}