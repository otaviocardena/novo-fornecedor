﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp.Core;
using NPoco;

namespace Creasp.Siplen
{
    /// <summary>
    /// Classe de dados para listas possiveis convidados a um evento
    /// </summary>
    [Serializable]
    public class EventoConvidadoDTO
    {
        public bool FgConvidado { get; set; }

        public int? IdPessoaTit { get; set; }
        public int? IdMandatoTit { get; set; }
        public string NoPessoaTit { get; set; }
        public string NoTituloTit { get; set; }
        public bool FgLicencaTit { get; set; } = false;

        public int? IdPessoaSup { get; set; }
        public int? IdMandatoSup { get; set; }
        public string NoPessoaSup { get; set; }
        public string NoTituloSup { get; set; }
        public bool FgLicencaSup { get; set; } = false;

        public int NrOrdem { get; set; }
    }
}