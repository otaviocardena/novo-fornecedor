﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp.Core;
using NPoco;

namespace Creasp.Siplen
{
    /// <summary>
    /// Classe usada no temaplte de envio de email para os participantes de um evento
    /// </summary>
    public class EmailConvocacaoDTO
    {
        public string NomeEvento { get; set; }
        public string DataEvento { get; set; }
        public string HoraEvento { get; set; }
        public string LocalEvento { get; set; }

        public string NomePessoa { get; set; }
        public bool EmDia { get; set; }
        public bool EmDebito { get; set; }
    }
}