﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp.Core;
using NPoco;

namespace Creasp.Siplen
{
    /// <summary>
    /// Classe de dados para o relatorio de Lista de presenca
    /// </summary>
    public class RListaPresencaDTO
    {
        public int NrSeq { get; set; }
        public string NoTitular { get; set; }
        public string NoTituloTit { get; set; }
        public string NoPresencaTit { get; set; }
        public string NoPercursoTit { get; set; }
        public string NoSuplente { get; set; }
        public string NoTituloSup { get; set; }
        public string NoPresencaSup { get; set; }
        public string NoPercursoSup { get; set; }
        public string NrCreaspTit { get; set; }
        public string NrCreaspSup { get; set; }
        public string NoCentroCusto { get; set; }

        public int? NrMatricula { get; set; }
        public string NoUnidade { get; set; }
    }
}