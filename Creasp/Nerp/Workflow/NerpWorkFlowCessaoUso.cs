﻿using NBox.Services;
using NBox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using NPoco;
using Creasp.Core;
using NPoco.Linq;
using Creasp.Siplen;
using System.Configuration;
using System.Text.RegularExpressions;

namespace Creasp.Nerp
{
    public class NerpWorkFlowCessaoUso : NerpWorkFlow
    {
        public NerpWorkFlowCessaoUso(Nerp nerp, ServiceContext context, IDatabase db) : base(nerp, context, db)
        {
        }

        protected override void MontaFluxo()
        {
            // Solicitante - Documentacao
            Passo(NerpSituacao.Cadastrada)
                .Encaminhar("Encaminhar para aprovação do gestor", () => { Assina(nerp.Solicitante); }, NerpSituacao.AgAprovacaoGestor, nerp.Gestor.Pessoa);

            // Gestor
            Passo(NerpSituacao.AgAprovacaoGestor)
                .Aprovar("Aprovar e encaminhar para aprovação do superintendente", () => Assina(nerp.Gestor), NerpSituacao.AgAprovacaoSuperintendente, nerp.Superintendente.Pessoa)
                .Reprovar("Reprovar e retornar ao solicitante", LimpaAssinaturas, NerpSituacao.Cadastrada, nerp.Solicitante.Pessoa);

            // Superindentene - Aprovação
            Passo(NerpSituacao.AgAprovacaoSuperintendente)
                .Aprovar("Aprovar e encaminhar para empenho", () => Assina(nerp.Superintendente), NerpSituacao.AgSolicEmpenho, uco)
                .Reprovar("Reprovar e retornar ao solicitante", LimpaAssinaturas, NerpSituacao.Cadastrada, nerp.Solicitante.Pessoa);

            // UCO - Empenho
            Passo(NerpSituacao.AgSolicEmpenho)
                .Encaminhar("Encaminhar para pagamento", ValidaEmpenho, NerpSituacao.AgRetencao, uco)
                .Reprovar("Reprovar e retornar ao solicitante", LimpaAssinaturas, NerpSituacao.Cadastrada, nerp.Solicitante.Pessoa);

            // UCO - Retenção
            Passo(NerpSituacao.AgRetencao)
                .Encaminhar("Encaminhar para pagamento", null, NerpSituacao.AgSolicPagto, ufi);

            // UFI - Pagamento
            Passo(n => n.TpSituacao == NerpSituacao.AgSolicPagto && n.FgAdiantamento == false)
                .Encaminhar("Finalizar", ValidaPagamento, NerpSituacao.Finalizada, null);
        }
    }
}