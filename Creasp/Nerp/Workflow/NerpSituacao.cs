﻿using NBox.Services;
using NBox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using NPoco;
using Creasp.Core;
using NPoco.Linq;
using Creasp.Siplen;
using System.Configuration;
using System.Text.RegularExpressions;
using NBox.Web.Controls;
using System.Web.UI.WebControls;
using System.Reflection;

namespace Creasp.Nerp
{
    /// <summary>
    /// Enum para TpSituacao da NERP
    /// </summary>
    public class NerpSituacao
    {
        public const string Cadastrada = "CADASTRADA";
        public const string AgAprovacaoGestor = "AG-APR-GESTOR";
        public const string AgAprovacaoSuperintendente = "AG-APR-SUPER";
        public const string AgSolicEmpenho = "AG-SOLIC-EMP";
        public const string AgValidaEmpenho = "AG-VALIDA-EMP";
        public const string AgRetencao = "AG-RETENCAO";
        public const string AgConfAuditoria = "AG-CONF-CONT";
        public const string AgConfPrestCont = "AG-CONF-PREST-CONT";
        public const string AgSolicPagto = "AG-SOLIC-PAGTO";
        public const string AgSolicAdiantamento = "AG-SOLIC-ADIANTA";
        public const string AgPrestacaoContas = "AG-PREST-CONTAS";
        public const string AgAprovacaoPrestacaoContas = "AG-APR-PREST-CONTAS";
        public const string Finalizada = "FINALIZADA";
        public const string Cancelada = "CANCELADA";

        public static string NoSituacao(string tpSituacao)
        {
            return
                tpSituacao == Cadastrada ? "Cadastrada" :
                tpSituacao == AgAprovacaoGestor ? "Aguardando aprovação do gestor/superior" :
                tpSituacao == AgAprovacaoSuperintendente ? "Aguardando aprovação do superintendente" :
                tpSituacao == AgSolicEmpenho ? "Aguardando solicitação de empenho" :
                tpSituacao == AgValidaEmpenho ? "Aguardando validação de empenho" :
                tpSituacao == AgRetencao ? "Aguardando apontamento de retenções" :
                tpSituacao == AgConfAuditoria ? "Aguardando conferência da auditoria" :
                tpSituacao == AgConfPrestCont ? "Aguardando conferência da prestação de contas" :
                tpSituacao == AgSolicPagto ? "Aguardando solicitação de pagamento" :
                tpSituacao == AgSolicAdiantamento ? "Aguardando solicitação de adiantamento" :
                tpSituacao == AgPrestacaoContas ? "Aguardando prestação de contas" :
                tpSituacao == AgAprovacaoPrestacaoContas ? "Aguardando aprovação da prestação de contas" :
                tpSituacao == Finalizada ? "Finalizada" :
                tpSituacao == Cancelada ? "Cancelada" : "-";
        }

        public static List<ListItem> ListaSituacoes()
        {
            return typeof(NerpSituacao)
                .GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy)
                .Where(x => x.IsLiteral && !x.IsInitOnly)
                .Select(x => new ListItem { Value = x.GetValue(null).ToString(), Text = NoSituacao(x.GetValue(null).ToString()) })
                .ToList();
        }
    }

    /// <summary>
    /// Classe que monta um passo a ser executado no workflow da NERP
    /// </summary>
    public class NerpWorkflowPasso
    {
        public Func<Nerp, bool> Condicao { get; set; }
        public List<NerpWorkflowAcao> Acoes { get; set; } = new List<NerpWorkflowAcao>();

        public NerpWorkflowPasso NovaAcao(string descricao, ThemeStyle tema, Action acao, Func<string> proxSituacao, Func<object> responsavel)
        {
            var novaAcao = new NerpWorkflowAcao
            {
                Descricao = descricao,
                ButtonTheme = tema,
                Acao = acao,
                ProxSituacao = proxSituacao,
                Responsavel = responsavel,
            };
            Acoes.Add(novaAcao);
            return this;
        }

        public NerpWorkflowPasso Encaminhar(string descricao, Action acao, string proxSituacao, object responsavel)
        {
            return NovaAcao(descricao, ThemeStyle.Primary, acao, () => proxSituacao, () => responsavel);
        }

        public NerpWorkflowPasso Aprovar(string descricao, Action acao, string proxSituacao, object responsavel)
        {
            return NovaAcao(descricao, ThemeStyle.Success, acao, () => proxSituacao, () => responsavel);
        }

        public NerpWorkflowPasso Reprovar(string descricao, Action acao, string proxSituacao, object responsavel)
        {
            return NovaAcao(descricao, ThemeStyle.Danger, acao, () => proxSituacao, () => responsavel);
        }
    }

    /// <summary>
    /// Classe que implementa uma ação do workflow
    /// </summary>
    public class NerpWorkflowAcao
    {
        public string Descricao { get; set; }
        public Func<string> ProxSituacao { get; set; }
        public Func<object> Responsavel { get; set; }
        public Action Acao { get; set; } = null;

        public ThemeStyle ButtonTheme { get; set; } = ThemeStyle.Primary;

        public string ButtonIcon =>
            ButtonTheme == ThemeStyle.Primary ? "right-fat" :
            ButtonTheme == ThemeStyle.Success ? "thumbs-up" : "thumbs-down";

    }
}