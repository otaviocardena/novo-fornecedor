﻿using NBox.Services;
using NBox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using NPoco;
using Creasp.Core;
using NPoco.Linq;
using Creasp.Siplen;
using System.Configuration;
using System.Text.RegularExpressions;
using NBox.Web.Controls;

namespace Creasp.Nerp
{
    /// <summary>
    /// Classe que elenca todas as permissões que o usuario pode fazer em uma NERP para saber se o usuario tem a permissão
    /// </summary>
    [Serializable]
    public class NerpPermissao
    {
        public bool FgAcao { get; set; }
        public bool FgDocumentar { get; set; }
        public bool FgEditarItem { get; set; }
        public bool FgSolicEmpenho { get; set; }
        public bool FgSolicPagto { get; set; }
        public bool FgAnexarComprovAdiant { get; set; }
        public bool FgSolicAdiantamento { get; set; }
        public bool FgRetencao { get; set; }

        //public bool FgCancelaEmpenho { get; set; }
        //public bool FgCancelaPagto { get; set; }
        //public bool FgCancelaAdiantamento { get; set; }
    }
}