﻿using NBox.Services;
using NBox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using NPoco;
using Creasp.Core;
using NPoco.Linq;
using Creasp.Siplen;
using System.Configuration;
using System.Text.RegularExpressions;

namespace Creasp.Nerp
{
    public class NerpWorkFlowComEmpenho : NerpWorkFlow
    {
        public NerpWorkFlowComEmpenho(Nerp nerp, ServiceContext context, IDatabase db) : base(nerp, context, db)
        {
        }

        protected override void MontaFluxo()
        {
            //NERPS com valor acima de R$40.000,00 passam pela conferência da auditoria.
            var vlrTotalItens = nerp.Items.Sum(x => x.VrTotal);

            // Solicitante - Documentação
            Passo(NerpSituacao.Cadastrada)
                .Encaminhar("Encaminhar para aprovação do gestor", () => { ValidaSaldoEmpenho(); Assina(nerp.Solicitante); }, NerpSituacao.AgAprovacaoGestor, nerp.Gestor.Pessoa);

            // Gestor - Aprovação
            Passo(NerpSituacao.AgAprovacaoGestor)
                .Aprovar("Aprovar e encaminhar para UFI", () => { ValidaDocumentacaoItem(); Assina(nerp.Gestor); }, NerpSituacao.AgValidaEmpenho, ufi)
                .Reprovar("Reprovar e retornar ao solicitante", LimpaAssinaturas, NerpSituacao.Cadastrada, nerp.Solicitante.Pessoa);

            // UFI - Verifica Empenho
            Passo(NerpSituacao.AgValidaEmpenho)
                .Aprovar("Aprovar e encaminhar para retenção", null, NerpSituacao.AgRetencao, uco)
                .Reprovar("Reprovar e retornar ao solicitante", LimpaAssinaturas, NerpSituacao.Cadastrada, nerp.Solicitante.Pessoa);

            if (vlrTotalItens >= 40000)
            {
                MontaFluxoAuditoria();
            }
            else
            {
                // UCO - Retenção
                Passo(NerpSituacao.AgRetencao)
                    .Encaminhar("Encaminhar para pagamento", null, NerpSituacao.AgSolicPagto, ufi)
                    .Reprovar("Reprovar e retornar para validação de empenho", null, NerpSituacao.AgValidaEmpenho, ufi);

                // UFI - Pagamento
                Passo(NerpSituacao.AgSolicPagto)
                    .Encaminhar("Finalizar", ValidaPagamento, NerpSituacao.Finalizada, null)
                    .Reprovar("Reprovar e retornar a UCO", BloqueiaSeExistePagamento, NerpSituacao.AgRetencao, uco)
                    .Reprovar("Reprovar e retornar ao solicitante", () => { BloqueiaSeExistePagamento(); LimpaAssinaturas(); }, NerpSituacao.Cadastrada, nerp.Solicitante.Pessoa);
            }
        }

        private void MontaFluxoAuditoria()
        {
            // UCO - Retenção
            Passo(NerpSituacao.AgRetencao)
                .Encaminhar("Encaminhar para auditoria", null, NerpSituacao.AgConfAuditoria, cont)
                .Reprovar("Reprovar e retornar para validação de empenho", null, NerpSituacao.AgValidaEmpenho, ufi);

            // CONT - Auditoria
            Passo(NerpSituacao.AgConfAuditoria)
                .Encaminhar("Encaminhar para pagamento", null, NerpSituacao.AgSolicPagto, ufi)
                .Reprovar("Reprovar e retornar a retenção", null, NerpSituacao.AgRetencao, uco);

            // UFI - Pagamento
            Passo(NerpSituacao.AgSolicPagto)
                .Encaminhar("Finalizar", ValidaPagamento, NerpSituacao.Finalizada, null)
                .Reprovar("Reprovar e retornar ao solicitante", () => { BloqueiaSeExistePagamento(); LimpaAssinaturas(); }, NerpSituacao.Cadastrada, nerp.Solicitante.Pessoa)
                .Reprovar("Reprovar e retornar a UCO", BloqueiaSeExistePagamento, NerpSituacao.AgRetencao, uco);
        }
    }
}