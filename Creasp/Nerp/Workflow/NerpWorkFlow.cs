﻿using NBox.Services;
using NBox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using NPoco;
using Creasp.Core;
using NPoco.Linq;
using Creasp.Siplen;
using System.Configuration;
using System.Text.RegularExpressions;

namespace Creasp.Nerp
{
    public class NerpWorkFlow
    {
        protected Nerp nerp;
        protected ServiceContext context;
        protected IDatabase db;
        protected Pessoa usuario;
        protected Unidade uco;
        protected Unidade ufi;
        protected Unidade cont;

        public Nerp Nerp { get { return nerp; } }

        private List<NerpWorkflowPasso> _fluxo = new List<NerpWorkflowPasso>();

        public NerpWorkFlow(Nerp nerp, ServiceContext context, IDatabase db)
        {
            this.nerp = nerp;
            this.context = context;
            this.db = db;

            this.usuario = db.SingleById<Pessoa>(Auth.Current<CreaspUser>().IdPessoa);
            this.uco = new UnidadeService(context).UCO;
            this.ufi = new UnidadeService(context).UFI;
            this.cont = new UnidadeService(context).CONT;

            MontaFluxo();
        }

        #region MontaFluxo API

        protected virtual void MontaFluxo()
        {
        }

        /// <summary>
        /// Cria e adiciona um passo no fluxo
        /// </summary>
        protected NerpWorkflowPasso Passo(string tpSituacao)
        {
            var p = new NerpWorkflowPasso { Condicao = n => n.TpSituacao == tpSituacao };
            _fluxo.Add(p);
            return p;
        }

        /// <summary>
        /// Cria e adiciona um passo no fluxo
        /// </summary>
        protected NerpWorkflowPasso Passo(Func<Nerp, bool> condicao)
        {
            var p = new NerpWorkflowPasso { Condicao = condicao };
            _fluxo.Add(p);
            return p;
        }

        /// <summary>
        /// Retorna as acoes disponiveis conforme a situação atual da NERP
        /// </summary>
        public IEnumerable<NerpWorkflowAcao> AcoesDisponiveis()
        {
            return _fluxo.FirstOrDefault(x => x.Condicao(nerp) == true)?.Acoes;
        }

        /// <summary>
        /// Executa uma ação do workflow conforme a situacao atual e descricao da acao
        /// </summary>
        public void ExecucaoAcao(string descricaoAcao, string noMotivo)
        {
            var acao = this.AcoesDisponiveis().FirstOrDefault(x => x.Descricao == descricaoAcao);

            if (acao == null) throw new NBoxException("Não foi encontrada a ação {0} da situacao {1}", descricaoAcao, nerp.TpSituacao);

            using (var trans = db.GetTransaction())
            {
                acao.Acao?.Invoke();

                var proxSituacao = acao.ProxSituacao();
                var responsavel = acao.Responsavel();

                nerp.TpSituacao = proxSituacao;
                nerp.IdPessoaResp = (responsavel as Pessoa)?.IdPessoa;
                nerp.CdUnidadeResp = (responsavel as Unidade)?.CdUnidade;

                // se a nova situação for final, desativa a NERP
                if (nerp.TpSituacao == NerpSituacao.Finalizada || nerp.TpSituacao == NerpSituacao.Cancelada)
                {
                    nerp.FgAtivo = false;
                }
                // atualiza a coluna de motivo apenas com a ultima informação - não mantem historico disto
                nerp.NoHistorico = noMotivo;

                // atualiza a NERP
                db.Update(nerp);

                // faz log da operação na tabela de auditoria
                var noResponsavel = (responsavel as Pessoa)?.NoPessoa ?? (responsavel as Unidade)?.NoUnidade ?? "-";
                new AuditLog().Table("Nerp", nerp.IdNerp, 
                    descricaoAcao + " => " + noResponsavel + 
                    (string.IsNullOrEmpty(noMotivo) ? "" : (" :: " + noMotivo)));

                trans.Complete();
            }
        }

        #endregion

        #region Ações padrões

        /// <summary>
        /// Valida se a NERP está com todos os items documentados
        /// </summary>
        protected void ValidaDocumentacaoItem()
        {
            var srv = new NerpService(context);
            var diretoriasrv = new DiretoriaService(context);

            var docs = srv.ListaDocumentosItem(Pager.NoPage, nerp.IdNerp, null, null).Items;
            var presidenteExercicio = diretoriasrv.BuscaPresidenteExercicio(nerp.DtEmissao);

            // 22-02-2017 - Excluir presidente
            var qtDocs = docs.Count(x => x.IdDocumento == null && x.IdPessoa != presidenteExercicio?.Pessoa?.IdPessoa);

            if (qtDocs > 0)
            {
                throw new NBoxException("Toda documentação deve estar anexada a NERP, verifique aba Documentos");
            }
        }

        /// <summary>
        /// Valida se existe saldo orçamentario para a NERP (nerp sem empenho). Utiliza o "SaldoOrcamentarioDesbloqueado" para validar
        /// </summary>
        protected void ValidaSaldoOrcamento()
        {
            var nerpService = new NerpService(context);
            nerpService.ValidaSaldoOrcamentario(nerp.IdNerp);
        }

        /// <summary>
        /// Verifica se existe saldo no empenho para pagar esta NERP. Utilizado em NERPs que já possuem empenho prévio informado pelo usuario
        /// </summary>
        public void ValidaSaldoEmpenho()
        {
            var siscont = Factory.Get<ISiscont>();
            var centrocustos = new CentroCustoService(context);
            var emps = new NerpService(context).ListaEmpenhos(nerp.IdNerp);
            var nrAnoBase = (context as CreaspContext).NrAnoBase; //TODO: Remover ano base assim que não existirem mais nerps de 2018

            foreach (var emp in emps)
            {
                var empenho = siscont.ConsultaEmpenho(emp.NrAnoEmpenho.Value, null, null, emp.NrEmpenho.Value, null, emp.FgRestoAPagar).OrderByDescending(e => e.SaldoEmpenho).FirstOrDefault();

                if (empenho == null)
                {
                    throw new NBoxException("Não foi encontrado o empenho {0}/{1} no SISCONT", emp.NrEmpenho, emp.NrAnoEmpenho);
                }

                // valida por centro de custo
                foreach (var nrCentroCusto in emp.VrCentroCusto.Keys)
                {
                    var valor = emp.VrCentroCusto[nrCentroCusto];
                    var centrocusto = centrocustos.BuscaCentroCusto(nrCentroCusto.UnMask());

                    //a partir de 2019, o empenho é verificado no centro de custo de pagamento e no nivel orçado.
                    var saldo = empenho.CentrosCustos.FirstOrDefault(x => x.CentroCustoCodigo == centrocusto.NrCentroCustoFmt);

                    //Se não houver no centro de custo de pagamento, verifica saldo no nível orçado.
                    if (saldo == null)
                    {
                        var centroCustoOrçado = centrocustos.BuscaCentroCustoOrçado(centrocusto);
                        saldo = empenho.CentrosCustos.FirstOrDefault(x => x.CentroCustoCodigo == centroCustoOrçado.NrCentroCustoFmt);

                        if (saldo == null) throw new NBoxException("Não foi encontrado os centro de custos {0}, {1} no empenho {2}", centrocusto.NrCentroCustoFmt, centroCustoOrçado.NrCentroCustoFmt, emp.NrEmpenho);
                        
                    }                    

                    if (saldo.SaldoPagar < valor)
                    {
                        throw new NBoxException("Não existe saldo no empenho {0}/{1} (saldo atual: {2:n})", emp.NrEmpenho, emp.NrAnoEmpenho, saldo.SaldoPagar);
                    }
                }
            }
        }

        /// <summary>
        /// Preenche os dados de assinatura de uma NERP
        /// </summary>
        protected void Assina(NerpAssinatura assinatura)
        {
            assinatura.IdPessoaAssina = usuario.IdPessoa;
            assinatura.DtAssinatura = DateTime.Now;
            db.Update(assinatura);
        }

        /// <summary>
        /// Limpa os dados de assinatura
        /// </summary>
        protected void LimpaAssinaturas()
        {
            foreach(var ass in nerp.Assinaturas)
            {
                ass.IdPessoaAssina = null;
                ass.DtAssinatura = null;
                db.Update(ass);
            }
        }

        /// <summary>
        /// Verifica se todos os empenhos foram feitos e já estão aprovados no SISCONT
        /// </summary>
        protected void ValidaEmpenho()
        {
            var srv = new NerpService(context);

            // primeiro valida pendentes no SISCONT
            srv.VerificaSiscontEmpenho(nerp);

            // verifica se ficou algum pendente
            var emps = srv.ListaEmpenhos(nerp.IdNerp);

            if (emps.Count(x => x.NrEmpenho == null) > 0) throw new NBoxException("Existem empenhos ainda não confirmados no SISCONT.NET");
        }

        /// <summary>
        /// Verifica se todos os items estão pagos (valida no SISCONT também)
        /// </summary>
        protected void ValidaPagamento()
        {
            var srv = new NerpService(context);

            // verifica se existe alguma atualização do siscont
            srv.VerificaSiscontPagto(nerp);

            // conta os pagamentos que faltam (aceita somente os solicitados)
            var q = db.Query<NerpItem>()
                .Where(x => x.IdNerp == nerp.IdNerp)
                .Where(x => x.NrSolicPagto == null)
                .Count();

            if (q > 0) throw new NBoxException("Existem pagamentos ainda não solicitados ao SISCONT.NET");

            // valida se documentação está ok
            ValidaDocumentosPagto();
        }

        /// <summary>
        /// Verifica se já existe algum pagamento solicitado. Se já existe, não permite mais voltar
        /// </summary>
        protected void BloqueiaSeExistePagamento()
        {
            var q = db.Query<NerpItem>()
                .Where(x => x.IdNerp == nerp.IdNerp)
                .Where(x => x.NrSolicPagto != null || x.NrSolicAdiantamento != null)
                .Count();

            if (q > 0)
            {
                throw new NBoxException("Já existem pagamentos/adiantamentos solicitados nesta NERP. Utilize a operação desfazer solicitação (caso ainda não tenha sido confirmado o pagamento) ou cancele a NERP.");
            }
        }

        /// <summary>
        /// Se já existir empenho, não permite voltar no fluxo
        /// </summary>
        protected void BloqueiaSeExisteEmpenho()
        {
            var q = db.Query<NerpEmpenho>()
                .Where(x => x.IdNerp == nerp.IdNerp)
                .Where(x => x.NrSolicEmpenho != null)
                .Count();

            if (q > 0)
            {
                throw new NBoxException("Já existem empenhos solicitados nesta NERP. Para corrigir somente cancelando a NERP");
            }
        }

        /// <summary>
        /// Verifica se esta faltando algum documento de pagamento
        /// </summary>
        public void ValidaDocumentosPagto()
        {
            var doc = new DocumentoService(context);
            var docs = doc.ListaDocumentos(TpRefDoc.NerpPagto, nerp.IdNerp);
            doc.AdicionaDocumentosFaltantes(docs, TpRefDoc.NerpPagto);
            var faltante = docs.FirstOrDefault(x => x.Tipo.FgObrigatorio && x.NoArquivo == null);

            if (faltante != null)
            {
                throw new NBoxException("O documento '{0}' está não foi anexado ao pagamento", faltante.Tipo.NoTipoDoc);
            }
        }

        /// <summary>
        /// Verifica se acatado o adiantamento no SISCONT
        /// </summary>
        public void ValidaAdiantamento()
        {
            var srv = new NerpService(context);

            // verifica se existe alguma atualização do siscont
            srv.VerificaSiscontAdiantamento(nerp);

            // conta os pagamentos que faltam
            var q = db.Query<NerpItem>()
                .Where(x => x.IdNerp == nerp.IdNerp)
                .Where(x => x.NrAdiantamento == null)
                .Count();

            if (q > 0) throw new NBoxException("Existem adiantamentos ainda não confirmados no SISCONT.NET");

        }

        /// <summary>
        /// Valida se foi realizada todas as prestações de contas no SISCONT
        /// </summary>
        public void ValidaPrestacaoContas()
        {
            //TODO
        }

        /// <summary>
        /// Envia e-mail ao responsável pelo novo status do fluxo do Nerp
        /// </summary>
        /// <param name="pessoa">Pessoa responsável pelo novo status do Nerp</param>
        public void EnviaEmailNerpWorkflow(Pessoa pessoa)
        {
            if (pessoa.FgAceitaEmail)
            {
                EmailService email = new EmailService(context);
                EmailNerpResponsavelDataSource template = new EmailNerpResponsavelDataSource(pessoa.IdPessoa, nerp.IdNerp);

                string noAssunto = template.GetTitulo();
                string noMensagem = template.GetMensagem();

                email.EnviaEmail(template, pessoa.IdPessoa, noAssunto, noMensagem, usuario.NoEmail);
            }
        }

        public void ValidaDiariaDuplicada(DateTime dtEvento, List<NerpItem> items, bool podeDiariaDuplicada)
        {
            if (podeDiariaDuplicada)
            {
                return;
            }

            var nerpService = new NerpService(context);

            foreach (NerpItem nerpItem in items)
            {
                if (nerpService.CredorTemDuasDiarias(nerpItem.IdPessoaCredor, dtEvento))
                {
                    var pessoa = db.Query<Pessoa>()
                        .Where(p => p.IdPessoa == nerpItem.IdPessoaCredor)
                        .FirstOrDefault();
                    
                    throw new NBoxException(String.Format("Credor {0} tem mais de uma diária cadastrada para esta data do evento. Por favor verifique outras NERPS e regularize ou altere o Evento para que possa duplicar diárias.", pessoa.NoPessoa));
                }
            }
        }

        #endregion

    }
}