﻿using NBox.Services;
using NBox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using NPoco;
using Creasp.Core;
using NPoco.Linq;
using Creasp.Siplen;
using System.Configuration;
using System.Text.RegularExpressions;

namespace Creasp.Nerp
{
    public class NerpWorkFlowEvento : NerpWorkFlow
    {
        public NerpWorkFlowEvento(Nerp nerp, ServiceContext context, IDatabase db) : base(nerp, context, db)
        {
        }

        protected override void MontaFluxo()
        {
            //NERPS com valor acima de R$40.000,00 passam pela conferência da auditoria.
            var vlrTotal = nerp.Items.Sum(x => x.VrTotal);

            // Workflow de adiantamento
            if (nerp.FgAdiantamento)
            {
                // Solicitante - Documentacao (Adiantamento)
                Passo(NerpSituacao.Cadastrada)
                    .Encaminhar("Encaminhar para aprovação do superior", () => { ValidaSaldoOrcamento(); ValidaDiariaDuplicada(nerp.Evento.DtEvento, nerp.Items, nerp.Evento.FgPermiteDiariaDup); Assina(nerp.Solicitante); EnviaEmailNerpWorkflow(nerp.Gestor.Pessoa); }, NerpSituacao.AgAprovacaoGestor, nerp.Gestor.Pessoa);

                // Gestor
                Passo(NerpSituacao.AgAprovacaoGestor)
                    .Aprovar("Aprovar e encaminhar para aprovação do superintendente", () => { Assina(nerp.Gestor); EnviaEmailNerpWorkflow(nerp.Superintendente.Pessoa); }, NerpSituacao.AgAprovacaoSuperintendente, nerp.Superintendente.Pessoa)
                    .Reprovar("Reprovar e retornar ao solicitante", () => { LimpaAssinaturas(); EnviaEmailNerpWorkflow(nerp.Solicitante.Pessoa); }, NerpSituacao.Cadastrada, nerp.Solicitante.Pessoa);

                // Superindentene - Aprovação
                Passo(NerpSituacao.AgAprovacaoSuperintendente)
                    .Aprovar("Aprovar e encaminhar para empenho", () => Assina(nerp.Superintendente), NerpSituacao.AgSolicEmpenho, ufi)
                    .Reprovar("Reprovar e retornar ao solicitante", () => { LimpaAssinaturas(); EnviaEmailNerpWorkflow(nerp.Solicitante.Pessoa); }, NerpSituacao.Cadastrada, nerp.Solicitante.Pessoa);
                
                // Fluxo Alternativo para departamento de auditoria
                if (vlrTotal >= 40000)
                {
                    MontaFluxoAdiantamentoAuditoria();
                }
                else
                {
                    // UFI - Empenho
                    Passo(NerpSituacao.AgSolicEmpenho)
                        .Encaminhar("Encaminhar para adiantamento de pagamento", ValidaEmpenho, NerpSituacao.AgSolicAdiantamento, ufi)
                        .Reprovar("Reprovar e retornar ao solicitante", () => { BloqueiaSeExisteEmpenho(); LimpaAssinaturas(); EnviaEmailNerpWorkflow(nerp.Solicitante.Pessoa); }, NerpSituacao.Cadastrada, nerp.Solicitante.Pessoa);

                    // UFI - Adiantamento
                    Passo(NerpSituacao.AgSolicAdiantamento)
                        .Encaminhar("Encaminhar ao solicitante para prestação de conta", ValidaAdiantamento, NerpSituacao.AgPrestacaoContas, nerp.Solicitante.Pessoa);

                    // Solicitante - Adiantamento
                    Passo(NerpSituacao.AgPrestacaoContas)
                        .Encaminhar("Encaminhar para aprovação", ValidaDocumentacaoItem, NerpSituacao.AgAprovacaoPrestacaoContas, ufi);

                    // UFI - Aprovar/Reprovar prestação de contas
                    Passo(NerpSituacao.AgAprovacaoPrestacaoContas)
                        .Aprovar("Aprovar prestação de contas e finalizar", ValidaPrestacaoContas, NerpSituacao.Finalizada, null)
                        .Reprovar("Retornar à solicitação de empenho", BloqueiaSeExistePagamento, NerpSituacao.AgSolicEmpenho, ufi)
                        .Reprovar("Reprovar e encaminhar para solicitante", null, NerpSituacao.AgPrestacaoContas, nerp.Solicitante.Pessoa);
                }
            }
            else
            {
                // Solicitante - Documentacao (Ressarcimento)
                Passo(NerpSituacao.Cadastrada)
                    .Encaminhar("Encaminhar para aprovação do superior", () => { ValidaDocumentacaoItem(); ValidaSaldoOrcamento(); ValidaDiariaDuplicada(nerp.Evento.DtEvento, nerp.Items, nerp.Evento.FgPermiteDiariaDup); Assina(nerp.Solicitante); EnviaEmailNerpWorkflow(nerp.Gestor.Pessoa); }, NerpSituacao.AgAprovacaoGestor, nerp.Gestor.Pessoa);

                // Gestor
                Passo(NerpSituacao.AgAprovacaoGestor)
                    .Aprovar("Aprovar e encaminhar para aprovação do superintendente", () => { Assina(nerp.Gestor); EnviaEmailNerpWorkflow(nerp.Superintendente.Pessoa); }, NerpSituacao.AgAprovacaoSuperintendente, nerp.Superintendente.Pessoa)
                    .Reprovar("Reprovar e retornar ao solicitante", () => { LimpaAssinaturas(); EnviaEmailNerpWorkflow(nerp.Solicitante.Pessoa); }, NerpSituacao.Cadastrada, nerp.Solicitante.Pessoa);

                // Superindentene - Aprovação
                Passo(NerpSituacao.AgAprovacaoSuperintendente)
                    .Aprovar("Aprovar e encaminhar para empenho", () => Assina(nerp.Superintendente), NerpSituacao.AgSolicEmpenho, ufi)
                    .Reprovar("Reprovar e retornar ao solicitante", () => { LimpaAssinaturas(); EnviaEmailNerpWorkflow(nerp.Solicitante.Pessoa); }, NerpSituacao.Cadastrada, nerp.Solicitante.Pessoa);

                if (vlrTotal >= 40000)
                {
                    MontaFluxoRessarcimentoAuditoria();
                }
                else
                {
                    // UFI - Empenho
                    Passo(NerpSituacao.AgSolicEmpenho)
                        .Encaminhar("Encaminhar para pagamento", ValidaEmpenho, NerpSituacao.AgSolicPagto, ufi)
                        .Reprovar("Reprovar e retornar ao solicitante", () => { BloqueiaSeExisteEmpenho(); LimpaAssinaturas(); EnviaEmailNerpWorkflow(nerp.Solicitante.Pessoa); }, NerpSituacao.Cadastrada, nerp.Solicitante.Pessoa);

                    // UFI - Pagamento
                    Passo(NerpSituacao.AgSolicPagto)
                        .Encaminhar("Finalizar", ValidaPagamento, NerpSituacao.Finalizada, null)
                        .Reprovar("Retornar à solicitação de empenho", BloqueiaSeExistePagamento, NerpSituacao.AgSolicEmpenho, ufi);
                }
            }
        }

        private void MontaFluxoAdiantamentoAuditoria()
        {
            // UFI - Empenho
            Passo(NerpSituacao.AgSolicEmpenho)
                .Encaminhar("Encaminhar para auditoria", ValidaEmpenho, NerpSituacao.AgConfAuditoria, cont)
                .Encaminhar("Encaminhar para adiantamento de pagamento", ValidaEmpenho, NerpSituacao.AgSolicAdiantamento, ufi)
                .Reprovar("Reprovar e retornar ao solicitante", () => { BloqueiaSeExisteEmpenho(); LimpaAssinaturas(); EnviaEmailNerpWorkflow(nerp.Solicitante.Pessoa); }, NerpSituacao.Cadastrada, nerp.Solicitante.Pessoa);
            
            // CONT - Auditoria
            Passo(NerpSituacao.AgConfAuditoria)
                .Encaminhar("Encaminhar para adiantamento de pagamento", null, NerpSituacao.AgSolicAdiantamento, ufi)
                .Reprovar("Reprovar e retornar ao financeiro", null, NerpSituacao.AgSolicEmpenho, ufi);
            
            // UFI - Adiantamento
            Passo(NerpSituacao.AgSolicAdiantamento)
                .Encaminhar("Encaminhar ao solicitante para prestação de conta", ValidaAdiantamento, NerpSituacao.AgPrestacaoContas, nerp.Solicitante.Pessoa);

            // Solicitante - Adiantamento
            Passo(NerpSituacao.AgPrestacaoContas)
                .Encaminhar("Encaminhar para aprovação", ValidaDocumentacaoItem, NerpSituacao.AgAprovacaoPrestacaoContas, ufi);

            //// Solicitante - Adiantamento
            //Passo(NerpSituacao.AgPrestacaoContas)
            //    .Encaminhar("Encaminhar para conferência", ValidaDocumentacaoItem, NerpSituacao.AgConfPrestCont, cont);

            //// CONT - Conferência e Auditoria   
            //Passo(NerpSituacao.AgConfPrestCont)
            //    .Encaminhar("Encaminhar para aprovação", null , NerpSituacao.AgAprovacaoPrestacaoContas, ufi)
            //    .Reprovar("Reprovar e retornar ao solicitante", null, NerpSituacao.AgPrestacaoContas, nerp.Solicitante.Pessoa);

            // UFI - Aprovar/Reprovar prestação de contas
            Passo(NerpSituacao.AgAprovacaoPrestacaoContas)
                .Aprovar("Aprovar prestação de contas e finalizar", ValidaPrestacaoContas, NerpSituacao.Finalizada, null)
                .Reprovar("Retornar à solicitação de empenho", BloqueiaSeExistePagamento, NerpSituacao.AgSolicEmpenho, ufi)
                .Reprovar("Reprovar e encaminhar para solicitante", null, NerpSituacao.AgPrestacaoContas, nerp.Solicitante.Pessoa);
        }

        private void MontaFluxoRessarcimentoAuditoria()
        {
            // UFI - Empenho
            Passo(NerpSituacao.AgSolicEmpenho)
                .Encaminhar("Encaminhar para controladoria", ValidaEmpenho, NerpSituacao.AgConfAuditoria, cont)
                .Encaminhar("Encaminhar para pagamento", ValidaEmpenho, NerpSituacao.AgSolicPagto, ufi)
                .Reprovar("Reprovar e retornar ao solicitante", () => { BloqueiaSeExisteEmpenho(); LimpaAssinaturas(); EnviaEmailNerpWorkflow(nerp.Solicitante.Pessoa); }, NerpSituacao.Cadastrada, nerp.Solicitante.Pessoa);
            
            // CONT -  Auditoria
            Passo(NerpSituacao.AgConfAuditoria)
                .Encaminhar("Encaminhar para pagamento", null, NerpSituacao.AgSolicPagto, ufi)
                .Reprovar("Reprovar e retornar ao financeiro", null, NerpSituacao.AgSolicEmpenho, ufi);

            // UFI - Pagamento
            Passo(NerpSituacao.AgSolicPagto)
                .Encaminhar("Finalizar", ValidaPagamento, NerpSituacao.Finalizada, null)
                .Reprovar("Retornar à solicitação de empenho", BloqueiaSeExistePagamento, NerpSituacao.AgSolicEmpenho, ufi);
        }


    }
}