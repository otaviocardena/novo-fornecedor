﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using Creasp.Core;

namespace Creasp.Nerp
{
    [PrimaryKey("CdTipoDespesa,DtIni", AutoIncrement = false)]
    public class TipoDespesaValor : IPeriodo
    {
        public string CdTipoDespesa { get; set; }
        public DateTime DtIni { get; set; }

        public DateTime DtFim { get; set; }
        public decimal VrUnit { get; set; }
    }
}