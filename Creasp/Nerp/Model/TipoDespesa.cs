﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using Creasp.Core;

namespace Creasp.Nerp
{
    [PrimaryKey("CdTipoDespesa", AutoIncrement = false)]
    public class TipoDespesa : IFgAtivo
    {
        public string CdTipoDespesa { get; set; }

        public string NoTipoDespesa { get; set; }
        public string NrContaContabil { get; set; }
        public string NrCpfAgrupado { get; set; }
        public bool FgInterno { get; set; }
        public bool FgAtivo { get; set; } = true;

        [Reference(ReferenceType.OneToOne, ColumnName = "NrContaContabil", ReferenceMemberName = "NrContaContabil")]
        public ContaContabil ContaContabil { get; set; }

        [Reference(ReferenceType.Many, ColumnName = "CdTipoDespesa", ReferenceMemberName = "CdTipoDespesa")]
        public List<TipoDespesaValor> Valores { get; set; } = new List<TipoDespesaValor>();

        [Ignore]
        public string NrCpfFmtAgrupado => Pessoa.FormataCpf(NrCpfAgrupado);

        /// <summary>
        /// Retorna o valor unitario conforme a data
        /// </summary>
        public decimal VrUnit(DateTime data)
        {
            var valor = this.Valores.Where(x => x.DtIni <= data && x.DtFim >= data).FirstOrDefault();

            return valor?.VrUnit ?? 0;
        }

        /// <summary>
        /// Indica que a despesa é do tipo diaria
        /// </summary>
        [Ignore]
        public bool IsDiaria => CdTipoDespesa.StartsWith("DIARIA-");

        /// <summary>
        /// Indica que a despesa é do tipo locomoção
        /// </summary>
        [Ignore]
        public bool IsKm => CdTipoDespesa.StartsWith("KM-");
    }
}