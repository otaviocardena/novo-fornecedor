﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using Creasp.Core;
using Creasp.Siplen;

namespace Creasp.Nerp
{
    [PrimaryKey("IdPessoa,IdPessoaCedido", AutoIncrement = false)]
    public class SubDelegacao : IPeriodo
    {
        public int IdPessoa { get; set; }
        public int IdPessoaCedido { get; set; }

        public DateTime DtIni { get; set; }
        public DateTime DtFim { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdPessoa", ReferenceMemberName = "IdPessoa")]
        public Pessoa Pessoa { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdPessoaCedido", ReferenceMemberName = "IdPessoa")]
        public Pessoa PessoaCedido { get; set; }
    }
}