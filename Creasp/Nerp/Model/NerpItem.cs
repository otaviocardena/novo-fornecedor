﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using Creasp.Core;
using Creasp.Siplen;
using NBox.Core;

namespace Creasp.Nerp
{
    [PrimaryKey("IdNerpItem", AutoIncrement = true)]
    public class NerpItem : IAuditoria
    {
        public int IdNerpItem { get; set; }

        public int IdNerp { get; set; }
        public int IdPessoaCredor { get; set; }
        public int? IdParticipante { get; set; }
        public int IdCentroCusto { get; set; }
        public int IdContaContabil { get; set; }
        public string NrProcesso { get; set; }
        public string CdTipoDespesa { get; set; }
        public decimal QtItem { get; set; }
        public decimal VrUnit { get; set; }
        public decimal VrRateio { get; set; } // usado apenas em ART
        public decimal VrTotal { get; set; }
        public int? IdNerpEmpenho { get; set; }

        public Guid? NrSolicPagto { get; set; }
        public int? NrPagto { get; set; }
        public int? NrAnoPagto { get; set; }
        public DateTime? DtPagto { get; set; }

        public Guid? NrSolicAdiantamento { get; set; }
        public int? NrAdiantamento { get; set; }
        public int? NrAnoAdiantamento { get; set; }
        public DateTime? DtAdiantamento { get; set; }

        public string NoHistorico { get; set; }

        #region Auditoria

        public DateTime DtCadastro { get; set; }
        public DateTime? DtAlteracao { get; set; }
        public string LoginCadastro { get; set; }
        public string LoginAlteracao { get; set; }

        #endregion

        [Reference(ReferenceType.OneToOne, ColumnName = "IdNerp", ReferenceMemberName = "IdNerp")]
        public Nerp Nerp { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdPessoaCredor", ReferenceMemberName = "IdPessoa")]
        public Pessoa PessoaCredor { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdParticipante", ReferenceMemberName = "IdParticipante")]
        public EventoParticipante Participante { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdCentroCusto", ReferenceMemberName = "IdCentroCusto")]
        public CentroCusto CentroCusto { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdContaContabil", ReferenceMemberName = "IdContaContabil")]
        public ContaContabil ContaContabil { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "CdTipoDespesa", ReferenceMemberName = "CdTipoDespesa")]
        public TipoDespesa TipoDespesa { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdNerpEmpenho", ReferenceMemberName = "IdNerpEmpenho")]
        public NerpEmpenho Empenho { get; set; }

        [Reference(ReferenceType.Many, ColumnName = "IdNerpItem", ReferenceMemberName = "IdNerpItem")]
        public List<NerpRetencao> Retencoes { get; set; } = new List<NerpRetencao>();

        /// <summary>
        /// Exibe informações sobre o pagamento
        /// </summary>
        [Ignore]
        public string NoPagto =>
            NrPagto != null ? $"{NrPagto}" :
            NrSolicPagto != null ? "(solicitado)" :
            "(sem pagamento)";

        /// <summary>
        /// Exibe informações sobre o adiantameto
        /// </summary>
        [Ignore]
        public string NoAdiantamento =>
            NrAdiantamento != null ? $"{NrAdiantamento}" :
            NrSolicAdiantamento != null ? "(solicitado)" :
            "(sem adiantamento)";
    }
}