﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using Creasp.Core;

namespace Creasp.Nerp
{
    [PrimaryKey("IdNerp,TpAprovacao", AutoIncrement = false)]
    public class NerpAssinatura
    {
        public int IdNerp { get; set; }
        public string TpAprovacao { get; set; }

        public int IdPessoa { get; set; }
        public int CdUnidade { get; set; }
        public int? IdPessoaAssina { get; set; }
        public DateTime? DtAssinatura { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdNerp", ReferenceMemberName = "IdNerp")]
        public Nerp Nerp { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdPessoa", ReferenceMemberName = "IdPessoa")]
        public Pessoa Pessoa { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "CdUnidade", ReferenceMemberName = "CdUnidade")]
        public Unidade Unidade { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdPessoaAssina", ReferenceMemberName = "IdPessoa")]
        public Pessoa PessoaAssina { get; set; }

        /// <summary>
        /// Lista o titulo de cada pessoa que assina a NERP
        /// </summary>
        [Ignore]
        public string NoAssinante => 
            TpAprovacao == "S" ? "Solicitante" :
            TpAprovacao == "G" ? "Superior/Gestor" : "Presidente/Delegação";
    }
}