﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NBox.Core;
using NPoco;

namespace Creasp.Nerp
{
    public class TpNerp
    {
        public const string Evento = "EVENTO";
        public const string ART = "ART";
        public const string CessaoUso = "CESSAO-USO";
        public const string ComEmpenho = "COM-EMP";
        public const string SemEmpenho = "SEM-EMP";
    }

    public class TpAprovacao
    {
        public const string Solicitante = "S";
        public const string Gestor = "G";
        public const string Superintendente = "P";
    }
}