﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using Creasp.Core;

namespace Creasp.Nerp
{
    [PrimaryKey("IdNerpRetencao", AutoIncrement = true)]
    public class NerpRetencao
    {
        public int IdNerpRetencao { get; set; }

        public int IdNerpItem { get; set; }
        public string CdTributo { get; set; }
        public string NoTributo { get; set; }
        public decimal VrBase { get; set; }
        public decimal PcRetencao { get; set; }
        public decimal VrRetencao { get; set; }
        public int IdPessoaFav { get; set; }
        public DateTime? DtVencto { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdPessoaFav", ReferenceMemberName = "IdPessoa")]
        public Pessoa Favorecido { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdNerpItem", ReferenceMemberName = "IdNerpItem")]
        public NerpItem Item { get; set; }

    }
}