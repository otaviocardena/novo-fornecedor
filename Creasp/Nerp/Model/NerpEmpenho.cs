﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using Creasp.Core;
using NBox.Core;

namespace Creasp.Nerp
{
    [PrimaryKey("IdNerpEmpenho", AutoIncrement = true)]
    public class NerpEmpenho
    {
        public int IdNerpEmpenho { get; set; }

        public int IdNerp { get; set; }
        public int IdContaContabil { get; set; }
        public int IdPessoa { get; set; }
        public decimal VrEmpenho { get; set; }
        public Guid NrSolicEmpenho { get; set; }
        public int? NrEmpenho { get; set; }
        public int? NrAnoEmpenho { get; set; }
        public bool FgRestoAPagar { get; set; } = false;
        public bool FgProcessado { get; set; } = false;

        [Reference(ReferenceType.OneToOne, ColumnName = "IdNerp", ReferenceMemberName = "IdNerp")]
        public Nerp Nerp { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdContaContabil", ReferenceMemberName = "IdContaContabil")]
        public ContaContabil ContaContabil { get; set ; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdPessoa", ReferenceMemberName = "IdPessoa")]
        public Pessoa Pessoa { get; set; }

        [Reference(ReferenceType.Many, ColumnName = "IdNerpEmpenho", ReferenceMemberName = "IdNerpEmpenho")]
        public List<NerpItem> Items { get; set; } = new List<NerpItem>();

        /// <summary>
        /// Exibe informações do empenho
        /// </summary>
        [Ignore]
        public string NoEmpenho => 
            (NrEmpenho != null ? $"{NrEmpenho}" :
            NrSolicEmpenho != Guid.Empty ? "(solicitado)" :
            "(sem empenho)") + 
            (FgRestoAPagar ? " [R.P.]" : "");

        /// <summary>
        /// Distribuição por centro de custo quando calculado. O Centro de custo é formatado (Fmt)
        /// </summary>
        [Ignore]
        public Dictionary<string, decimal> VrCentroCusto { get; set; }
    }
}