﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using Creasp.Core;
using Creasp.Siplen;

namespace Creasp.Nerp
{
    [PrimaryKey("IdNerp", AutoIncrement = true)]
    public class Nerp : IAuditoria, IFgAtivo
    {
        public int IdNerp { get; set; }

        public int NrNerp { get; set; }
        public int NrAno { get; set; }
        public string TpNerp { get; set; }
        public int? IdEvento { get; set; }
        public DateTime DtEmissao { get; set; }
        public string NoNerp { get; set; }
        public string NoJustificativa { get; set; }
        public int? IdPessoaResp { get; set; }
        public int? CdUnidadeResp { get; set; }
        public DateTime? DtVencto { get; set; }
        public decimal VrBruto { get; set; }
        public decimal VrRetencoes { get; set; }
        public decimal VrLiquido { get; set; }
        public string TpSituacao { get; set; }
        public bool FgAdiantamento { get; set; }
        public int? NrMesRef { get; set; }
        public string NoHistorico { get; set; }
        public bool FgAtivo { get; set; } = true;
        //public int? IdFornecedorRelacionado { get; set; }

        #region Auditoria

        public DateTime DtCadastro { get; set; }
        public DateTime? DtAlteracao { get; set; }
        public string LoginCadastro { get; set; }
        public string LoginAlteracao { get; set; }

        #endregion

        [Reference(ReferenceType.Many, ColumnName = "IdNerp", ReferenceMemberName = "IdNerp")]
        public List<NerpItem> Items { get; set; } = new List<NerpItem>();

        [Reference(ReferenceType.OneToOne, ColumnName = "IdPessoaResp", ReferenceMemberName = "IdPessoa")]
        public Pessoa PessoaResp { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "CdUnidadeResp", ReferenceMemberName = "CdUnidade")]
        public Unidade UnidadeResp { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdEvento", ReferenceMemberName = "IdEvento")]
        public Evento Evento { get; set; }

        [Reference(ReferenceType.Many, ColumnName = "IdNerp", ReferenceMemberName = "IdNerp")]
        public List<NerpAssinatura> Assinaturas { get; set; } = new List<NerpAssinatura>();

        /// <summary>
        /// Responsável atual pelo andamento da NERP
        /// </summary>
        [Ignore]
        public string NoResponsavel => PessoaResp?.NoPessoa ?? UnidadeResp?.NoUnidade ?? "-";

        /// <summary>
        /// Verifica o total solicitado do pagamento/adiantamento. Requer dados de Items
        /// </summary>
        [Ignore]
        public decimal VrSolicPago => Items
            .Where(x => x.NrSolicPagto != null || x.NrSolicAdiantamento != null)
            .Sum(x => x.VrTotal);

        /// <summary>
        /// Verifica o total do pagamento/adiantamento. Requer dados de Items
        /// </summary>
        [Ignore]
        public decimal VrPago => Items
            .Where(x => x.NrPagto != null || x.NrAdiantamento != null)
            .Sum(x => x.VrTotal);

        /// <summary>
        /// Numero da NERP formatado
        /// </summary>
        [Ignore]
        public string NrNerpFmt => $"{NrNerp}/{NrAno}";

        /// <summary>
        /// Retorna o solicitante de uma NERP (precisa estar carregado as assinaturas)
        /// </summary>
        [Ignore]
        public NerpAssinatura Solicitante => Assinaturas.Single(x => x.TpAprovacao == TpAprovacao.Solicitante);

        /// <summary>
        /// Retorna o gestor/superior imediato de uma NERP (obrigatorio)
        /// </summary>
        [Ignore]
        public NerpAssinatura Gestor => Assinaturas.Single(x => x.TpAprovacao == TpAprovacao.Gestor);

        /// <summary>
        /// Retorna a assinatura do superintendente (é opcional, NERP com empenho não tem)
        /// </summary>
        [Ignore]
        public NerpAssinatura Superintendente => Assinaturas.SingleOrDefault(x => x.TpAprovacao == TpAprovacao.Superintendente);

        /// <summary>
        /// Retorna a descrição pro tipo de NERP
        /// </summary>
        [Ignore]
        public string NoTpNerp =>
            this.TpNerp == "EVENTO" && FgAdiantamento == false ? "Evento SIPLEN - Ressarcimento" :
            this.TpNerp == "EVENTO" && FgAdiantamento == true ? "Evento SIPLEN - Adiantamento" :
            this.TpNerp == "ART" ? "Convênio de repasse de ART" :
            this.TpNerp == "CESSAO-USO" ? "Repasse de cessão de uso" :
            this.TpNerp == "SEM-EMP" ? "Pagamentos Diversos" : "Contrato";

        /// <summary>
        /// Retorna a data de emissão respeitando o ultimo dia do ano conforme ano base
        /// </summary>
        public static DateTime DataEmissao(int nrAnoBase)
        {
            var ultimoDia = new DateTime(nrAnoBase, 12, 30);

            return DateTime.Today > ultimoDia ? ultimoDia : DateTime.Today;
        }
    }
}