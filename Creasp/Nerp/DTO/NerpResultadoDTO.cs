﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp.Core;
using NPoco;

namespace Creasp.Nerp
{
    /// <summary>
    /// Descreve os resultados da consulta de NERPs
    /// </summary>
    [Serializable]
    public class NerpResultadoDTO : IFgAtivo
    {
        public int IdNerp { get; set; }
        public int NrNerp { get; set; }
        public string TpNerp { get; set; }
        public string NoResponsavel { get; set; }
        public DateTime DtEmissao { get; set; }
        public DateTime? DtVencto { get; set; }
        public string NoNerp { get; set; }
        public string TpSituacao { get; set; }
        public decimal VrBruto { get; set; }
        public decimal VrSolic { get; set; }
        public decimal VrPagto { get; set; }
        public bool FgAtivo { get; set; }
    }
}