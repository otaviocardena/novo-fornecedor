﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NPoco;

namespace Creasp.Nerp
{
    /// <summary>
    /// Representa os itens de uma nerp de forma agrupada, incluindo totalizadores
    /// </summary>
    [Serializable]
    public class NerpItemAgrupadoDTO
    {
        public string NoItem { get; set; }
        public string NrCentroCustoFmt { get; set; }
        public decimal? QtItem { get; set; }
        public decimal VrTotal { get; set; }
    }
}