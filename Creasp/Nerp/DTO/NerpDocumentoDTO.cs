﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NPoco;

namespace Creasp.Nerp
{
    /// <summary>
    /// Representa um registro editavel pelo usuário na emissão de uma nerp de evento
    /// </summary>
    [Serializable]
    public class NerpDocumentoDTO
    {
        public int IdNerpItem { get; set; }
        public int IdPessoa { get; set; }
        public string NoPessoa { get; set; }
        public string NoDespesa { get; set; }
        public decimal VrTotal { get; set; }
        public string NoArquivo { get; set; }
        public int? IdDocumento { get; set; }
    }
}