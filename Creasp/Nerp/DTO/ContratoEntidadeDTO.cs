﻿using System;
using NBox.Core;

namespace Creasp.Nerp
{
    /// <summary>
    /// Classe DTO para exibir dados de um contrato de cessao de uso de entidade
    /// </summary>
    [Serializable]
    public class ContratoEntidadeDTO
    {
        public int IdEntidade { get; set; }
        public int IdPessoaEntidade { get; set; }
        public string NoEntidade { get; set; }

        public int IdPessoaGestor { get; set; }
        public string NoGestor { get; set; }
        public int CdUnidade { get; set; }

        public string NrContrato { get; set; }
        public string NrProcesso { get; set; }

        public decimal VrContratoMes { get; set; }
    }
}