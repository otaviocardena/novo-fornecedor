﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp.Core;
using NPoco;

namespace Creasp.Nerp
{
    /// <summary>
    /// Representa um registro editavel pelo usuário na emissão de uma nerp de evento
    /// </summary>
    [Serializable]
    public class NerpParticipanteDTO
    {
        public int IdPessoa { get; set; }
        public string NoPessoa { get; set; }

        public int IdParticipante { get; set; }
        public string NoCentroCusto { get; set; }
        public int IdCentroCusto { get; set; }

        public List<NerpDespesaDTO> Despesas { get; set; } = new List<NerpDespesaDTO>(); // Alteravel pelo usuario

        public NerpDespesaDTO Diaria => Despesas.SingleOrDefault(x => x.CdTipoDespesa.StartsWith("DIARIA-"));
        public NerpDespesaDTO Km => Despesas.SingleOrDefault(x => x.CdTipoDespesa.StartsWith("KM-"));

        public List<NerpDespesaDTO> Outros => Despesas
            .Where(x => !x.CdTipoDespesa.StartsWith("DIARIA-") && !x.CdTipoDespesa.StartsWith("KM-"))
            .ToList();

        public decimal VrTotal => Despesas.Sum(x => x.VrUnit * x.QtUnit);
    }

    /// <summary>
    /// Representa uma despesa extra (qt/diaria) para um participante de evento
    /// </summary>
    [Serializable]
    public class NerpDespesaDTO
    {
        public string CdTipoDespesa { get; set; }
        public string NoTipoDespesa { get; set; }
        public string NoInfo { get; set; }
        public decimal VrUnit { get; set; }
        public decimal QtUnit { get; set; }

        public decimal VrTotal => VrUnit * QtUnit;
    }
}