﻿using System;
using NBox.Core;

namespace Creasp.Nerp
{
    /// <summary>
    /// Classe DTO para exibir dados de uma ROTA
    /// </summary>
    [Serializable]
    public class ConvenioArtDTO
    {
        public int IdEntidade { get; set; }
        public int IdPessoaEntidade { get; set; }
        public string NoEntidade { get; set; }

        public int IdPessoaGestor { get; set; }
        public string NoGestor { get; set; }
        public int CdUnidade { get; set; }

        public int QtIndicados { get; set; }
        public decimal VrIndicado { get; set; }
        public decimal VrRateio { get; set; }
        public decimal VrTotal { get; set; }

    }
}