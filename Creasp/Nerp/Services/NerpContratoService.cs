﻿using NBox.Services;
using NBox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using NPoco;
using Creasp.Core;
using NPoco.Linq;
using Creasp.Siplen;
using System.Configuration;
using System.Text.RegularExpressions;

namespace Creasp.Nerp
{
    public partial class NerpService : DataService
    {
        /// <summary>
        /// Inclui uma NERP com empenho. Busca as informações no SISCONT sobre o empenho
        /// </summary>
        public int IncluiNerpComEmpenho(int nrEmp, bool fgRestoAPagar, bool processado,  int idPessoaGestor, string nrCentroCusto, string noNerp, string noJustificativa, decimal vrItem, DateTime? dtVencto, int? idFornecedor = null)
        {
            ValidaLimiteDtEmissao(nrAnoBase);

            var empenho = siscont.ConsultaEmpenho(nrAnoBase, null, null, nrEmp, null, fgRestoAPagar, processado).FirstOrDefault();

            if (empenho == null) throw new NBoxException("Empenho {0}/{1} não encontrado", nrEmp, nrAnoBase);

            if (dtVencto != null && dtVencto < DateTime.Today) throw new NBoxException("A data de vencimento deve ser igual ou superior a hoje");

            using (var trans = db.GetTransaction())
            {
                var pessoas = new PessoaService(context);
                var funcionarios = new FuncionarioService(context);

                // busca o funcionario com a hierarquia dele (baseado no login do usuario)
                var func = funcionarios.BuscaFuncionarioHierarquia(user.Login);

                var pessoaFav = pessoas.BuscaOuIncluiPessoa(empenho.FavorecidoCPFCNPJ.UnMask());
                var contaContabil = db.Query<ContaContabil>()
                    .Where(x => x.NrContaContabil == empenho.PlanoContaCodigo.UnMask())
                    .Where(x => x.NrAno == nrAnoBase)
                    .Where(x => x.FgAtivo)
                    .Single("Não foi encontrada a conta contabil no sistema da NERP : " + empenho.PlanoContaCodigo);

                var centroCusto = db.Query<CentroCusto>()
                    .Where(x => x.NrCentroCusto == nrCentroCusto.UnMask())
                    .Where(x => x.NrAno == nrAnoBase)
                    .Where(x => x.TpCentroCusto == TpCentroCusto.SubArea)
                    .Where(x => x.FgAtivo)
                    .Single("Não foi encontrado o centro de custo no sistema da NERP : " + nrCentroCusto);

                // cria a nerp com os dados básicos
                var nerp = new Nerp
                {
                    DtEmissao = Nerp.DataEmissao(nrAnoBase),
                    TpNerp = TpNerp.ComEmpenho,
                    NrNerp = db.ExecuteScalar<int>("SELECT ISNULL(MAX(NrNerp), 0) + 1 FROM Nerp WHERE NrAno = @0", nrAnoBase),
                    NrAno = nrAnoBase,
                    NoNerp = noNerp,
                    NoJustificativa = noJustificativa,
                    IdPessoaResp = func.Pessoa.IdPessoa,
                    FgAdiantamento = false,
                    TpSituacao = NerpSituacao.Cadastrada,
                    DtVencto = dtVencto,
                    VrBruto = vrItem,
                    VrRetencoes = 0,
                    VrLiquido = vrItem
                    //IdFornecedorRelacionado = idFornecedor
                };

                db.Insert(nerp);

                // define gestor + unidade do gestor
                var gestor = funcionarios.BuscaFuncionario(null, db.SingleById<Pessoa>(idPessoaGestor).NrCpf);

                // aponta as pessoas que vão assinar a nerp
                db.Insert(new NerpAssinatura
                {
                    IdNerp = nerp.IdNerp,
                    TpAprovacao = TpAprovacao.Solicitante,
                    IdPessoa = func.Pessoa.IdPessoa,
                    CdUnidade = func.Unidade.CdUnidade
                });

                db.Insert(new NerpAssinatura
                {
                    IdNerp = nerp.IdNerp,
                    TpAprovacao = TpAprovacao.Gestor,
                    IdPessoa = gestor.Pessoa.IdPessoa,
                    CdUnidade = gestor.Unidade.CdUnidade,
                });

                // inclui o empenho da nerp
                var emp = new NerpEmpenho
                {
                    IdNerp = nerp.IdNerp,
                    IdContaContabil = contaContabil.IdContaContabil,
                    IdPessoa = pessoaFav.IdPessoa,
                    VrEmpenho = vrItem,
                    NrSolicEmpenho = Guid.Empty,
                    NrEmpenho = nrEmp,
                    NrAnoEmpenho = nrAnoBase,
                    FgRestoAPagar = empenho.RestoAPagar,
                    FgProcessado = empenho.Processado
                };

                db.Insert(emp);

                // inclui o item da nerp
                var item = new NerpItem
                {
                    IdNerp = nerp.IdNerp,
                    IdPessoaCredor = pessoaFav.IdPessoa,
                    IdCentroCusto = centroCusto.IdCentroCusto,
                    IdContaContabil = contaContabil.IdContaContabil,
                    IdNerpEmpenho = emp.IdNerpEmpenho,
                    QtItem = 1,
                    VrUnit = vrItem,
                    VrTotal = vrItem
                };

                db.Insert(item);

                trans.Complete();

                return nerp.IdNerp;
            }
        }

        /// <summary>
        /// Altera os dados de uma NERP com emepnho
        /// </summary>
        public void AlteraNerpComEmpenho(int idNerp, int nrEmp, bool fgRestoAPagar, bool fgProcessado,  int idPessoaGestor, string nrCentroCusto, string noNerp, string noJustificativa, decimal vrItem, DateTime? dtVencto)
        {
            var empenho = siscont.ConsultaEmpenho(nrAnoBase, null, null, nrEmp, null, fgRestoAPagar, fgProcessado).FirstOrDefault();

            if (empenho == null) throw new NBoxException("Empenho {0}/{1} não encontrado", nrEmp, nrAnoBase);

            if (dtVencto != null && dtVencto < DateTime.Today) throw new NBoxException("A data de vencimento deve ser igual ou superior a hoje");

            using (var trans = db.GetTransaction())
            {
                var nerp = db.SingleById<Nerp>(idNerp);
                var emp = ListaEmpenhos(idNerp).Single();
                var item = ListNerpItem(Pager.NoPage, idNerp, null).Items.Single();
                var assinatura = db.Query<NerpAssinatura>().Where(x => x.IdNerp == idNerp && x.TpAprovacao == TpAprovacao.Gestor).Single();

                var pessoas = new PessoaService(context);
                var funcionarios = new FuncionarioService(context);

                var pessoaFav = pessoas.BuscaOuIncluiPessoa(empenho.FavorecidoCPFCNPJ.UnMask());
                var contaContabil = db.Query<ContaContabil>()
                    .Where(x => x.NrContaContabil == empenho.PlanoContaCodigo.UnMask())
                    .Where(x => x.NrAno == nrAnoBase)
                    .Where(x => x.FgAtivo)
                    .Single("Não foi encontrada a conta contabil no sistema da NERP : " + empenho.PlanoContaCodigo);

                CentroCusto centroCusto;


                if (nrAnoBase < DateTime.Today.Year)
                {
                    centroCusto = db.Query<CentroCusto>()
                                    .Where(x => x.NrCentroCusto == nrCentroCusto.UnMask())
                                    .Where(x => x.TpCentroCusto == TpCentroCusto.SubArea)
                                    .Where(x => x.NrAno == nrAnoBase)
                                    .Single("Não foi encontrado o centro de custo no sistema da NERP : " + nrCentroCusto);
                }
                else
                {
                    centroCusto = db.Query<CentroCusto>()
                                    .Where(x => x.NrCentroCusto == nrCentroCusto.UnMask())
                                    .Where(x => x.NrAno == nrAnoBase)
                                    .Where(x => x.TpCentroCusto == TpCentroCusto.SubArea)
                                    .Where(x => x.FgAtivo)
                                    .Single("Não foi encontrado o centro de custo no sistema da NERP : " + nrCentroCusto);
                }


                // atualiza a NERP
                nerp.DtVencto = dtVencto;
                nerp.NoNerp = noNerp;
                nerp.NoJustificativa = noJustificativa;
                nerp.VrBruto = nerp.VrLiquido = vrItem;

                // atualiza o empenho
                emp.IdContaContabil = contaContabil.IdContaContabil;
                emp.IdPessoa = pessoaFav.IdPessoa;
                emp.VrEmpenho = vrItem;
                emp.NrEmpenho = nrEmp;
                emp.FgRestoAPagar = fgRestoAPagar;
                emp.FgProcessado = fgProcessado;
                emp.NrAnoEmpenho = nrAnoBase;

                // atualiza o item
                item.IdPessoaCredor = pessoaFav.IdPessoa;
                item.IdCentroCusto = centroCusto.IdCentroCusto;
                item.IdContaContabil = contaContabil.IdContaContabil;
                item.VrUnit = item.VrTotal = vrItem;

                // atualiza o gestor
                var gestor = funcionarios.BuscaFuncionario(null, db.SingleById<Pessoa>(idPessoaGestor).NrCpf);

                assinatura.IdPessoa = gestor.Pessoa.IdPessoa;
                assinatura.CdUnidade = gestor.Unidade.CdUnidade;

                // atualiza os registros
                db.Update(nerp);
                db.Update(emp);
                db.Update(item);
                db.Update(assinatura);

                trans.Complete();
            }
        }

        /// <summary>
        /// Inclui uma NERP sem empenho.
        /// </summary>
        public int IncluiNerpSemEmpenho(int idPessoaFav, int idContaContabil, int idCentroCusto, string noNerp, string noJustificativa, decimal vrItem, DateTime? dtVencto, int idChefe, int idSuperintendente)
        {
            if (dtVencto != null && dtVencto < DateTime.Today) throw new NBoxException("A data de vencimento deve ser igual ou superior a hoje");

            using (var trans = db.GetTransaction())
            {
                var pessoas = new PessoaService(context);
                var funcionarios = new FuncionarioService(context);

                // busca o funcionario com a hierarquia dele (baseado no login do usuario)
                // chefe e superintendente ja veio pela UI
                var func = funcionarios.BuscaFuncionarioHierarquia(user.Login);
                var chefe = funcionarios.BuscaFuncionario(db.SingleById<Pessoa>(idChefe).NrMatricula, null);
                var superintendente = funcionarios.BuscaFuncionario(db.SingleById<Pessoa>(idSuperintendente).NrMatricula, null);

                // cria a nerp com os dados básicos
                var nerp = new Nerp
                {
                    DtEmissao = Nerp.DataEmissao(nrAnoBase),
                    TpNerp = TpNerp.SemEmpenho,
                    NrNerp = db.ExecuteScalar<int>("SELECT ISNULL(MAX(NrNerp), 0) + 1 FROM Nerp WHERE NrAno = @0", nrAnoBase),
                    NrAno = nrAnoBase,
                    NoNerp = noNerp,
                    NoJustificativa = noJustificativa,
                    IdPessoaResp = func.Pessoa.IdPessoa,
                    TpSituacao = NerpSituacao.Cadastrada,
                    DtVencto = dtVencto,
                    FgAdiantamento = false,
                    VrBruto = vrItem,
                    VrRetencoes = 0,
                    VrLiquido = vrItem
                };

                db.Insert(nerp);

                // valida se a pessoa está autorizando não é o favorecido
                if (idPessoaFav == chefe.Pessoa.IdPessoa || idPessoaFav == superintendente.Pessoa.IdPessoa) throw new NBoxException("Os autorizadores da NERP não podem estar entre os credores da NERP");

                // não permite que o chefe superior seja o mesmo que solicitou
                if (func.Pessoa.IdPessoa == chefe.Pessoa.IdPessoa) throw new NBoxException("Não é permitido que o autorizador da NERP seja a mesma pessoa que solicita a NERP");

                db.Insert(new NerpAssinatura
                {
                    IdNerp = nerp.IdNerp,
                    TpAprovacao = TpAprovacao.Solicitante,
                    IdPessoa = func.Pessoa.IdPessoa,
                    CdUnidade = func.Unidade.CdUnidade
                });

                db.Insert(new NerpAssinatura
                {
                    IdNerp = nerp.IdNerp,
                    TpAprovacao = TpAprovacao.Gestor,
                    IdPessoa = chefe.Pessoa.IdPessoa,
                    CdUnidade = chefe.Unidade.CdUnidade
                });

                db.Insert(new NerpAssinatura
                {
                    IdNerp = nerp.IdNerp,
                    TpAprovacao = TpAprovacao.Superintendente,
                    IdPessoa = superintendente.Pessoa.IdPessoa,
                    CdUnidade = superintendente.Unidade.CdUnidade
                });

                // inclui o item da nerp
                var item = new NerpItem
                {
                    IdNerp = nerp.IdNerp,
                    IdPessoaCredor = idPessoaFav,
                    IdCentroCusto = idCentroCusto,
                    IdContaContabil = idContaContabil,
                    QtItem = 1,
                    VrUnit = vrItem,
                    VrTotal = vrItem
                };

                db.Insert(item);

                trans.Complete();

                return nerp.IdNerp;
            }
        }

        /// <summary>
        /// Altera uma NERP sem empenho
        /// </summary>
        public void AlteraNerpSemEmpenho(int idNerp, int idPessoaFav, int idContaContabil, int idCentroCusto, string noNerp, string noJustificativa, decimal vrItem, DateTime? dtVencto, int idChefe, int idSuperintendente)
        {
            using (var trans = db.GetTransaction())
            {
                var pessoas = new PessoaService(context);
                var funcionarios = new FuncionarioService(context);

                var nerp = db.SingleById<Nerp>(idNerp);
                var item = ListNerpItem(Pager.NoPage, idNerp, null).Items.Single();

                var chefe = funcionarios.BuscaFuncionario(db.SingleById<Pessoa>(idChefe).NrMatricula, null);
                var superintendente = funcionarios.BuscaFuncionario(db.SingleById<Pessoa>(idSuperintendente).NrMatricula, null);

                // altera os dados basicos da nerp
                nerp.NoNerp = noNerp;
                nerp.NoJustificativa = noJustificativa;
                nerp.DtVencto = dtVencto;
                nerp.VrBruto = nerp.VrLiquido = vrItem;

                // altera o item
                item.IdPessoaCredor = idPessoaFav;
                item.IdCentroCusto = idCentroCusto;
                item.IdContaContabil = idContaContabil;
                item.VrUnit = item.VrTotal = vrItem;

                // altera os registros
                db.Update(nerp);
                db.Update(item);

                // exclui-incluir assinaturas
                db.DeleteMany<NerpAssinatura>()
                    .Where(x => x.IdNerp == idNerp)
                    .Where(x => x.TpAprovacao != TpAprovacao.Solicitante)
                    .Execute();

                db.Insert(new NerpAssinatura
                {
                    IdNerp = nerp.IdNerp,
                    TpAprovacao = TpAprovacao.Gestor,
                    IdPessoa = chefe.Pessoa.IdPessoa,
                    CdUnidade = chefe.Unidade.CdUnidade
                });

                db.Insert(new NerpAssinatura
                {
                    IdNerp = nerp.IdNerp,
                    TpAprovacao = TpAprovacao.Superintendente,
                    IdPessoa = superintendente.Pessoa.IdPessoa,
                    CdUnidade = superintendente.Unidade.CdUnidade
                });

                trans.Complete();
            }
        }
    }
}