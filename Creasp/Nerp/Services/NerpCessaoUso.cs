﻿using NBox.Services;
using NBox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using NPoco;
using Creasp.Core;
using NPoco.Linq;
using Creasp.Siplen;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Text;

namespace Creasp.Nerp
{
    public partial class NerpService : DataService
    {
        /// <summary>
        /// Lista todos os contratos de cessão e uso das entidades
        /// </summary>
        public List<ContratoEntidadeDTO> ListaContratosEntidade(int ano, int mes, StringBuilder erros)
        {
            var contratos = Factory.Get<ICreanet>().BuscaContratosEntidade(ano, mes);
            var pessoas = new PessoaService(context);
            var funcionarios = new FuncionarioService(context);
            var result = new List<ContratoEntidadeDTO>();

            foreach (var contrato in contratos)
            {
                var entidade = db.Query<Entidade>()
                    .Include(x => x.Unidade)
                    .Include(x => x.Unidade.CentroCusto)
                    .Where(x => x.TpEntidade == TpEntidade.EntidadeClasse)
                    .Where(x => x.CdExterno == contrato.CodEntidade.ToString())
                    .Where(x => x.Unidade.CentroCusto.NrAno == nrAnoBase)
                    .Where(x => x.FgAtivo)
                    .FirstOrDefault();
            
                if (entidade == null) continue;
            
                try
                {
                    var gestor = pessoas.BuscaOuIncluiPessoa(contrato.CpfGestor);
                    var unidadeGestor = funcionarios.BuscaUnidade(gestor);
                    var entpessoa = pessoas.BuscaOuIncluiPessoa(entidade.NrCnpj);
            
                    var ce = new ContratoEntidadeDTO
                    {
                        IdEntidade = entidade.IdEntidade,
                        IdPessoaEntidade = entpessoa.IdPessoa,
                        NoEntidade = entidade.NoEntidade,
            
                        IdPessoaGestor = gestor?.IdPessoa ?? 0,
                        NoGestor = gestor?.NoPessoa ?? "GESTOR NÃO INFORMADO",
                        CdUnidade = unidadeGestor.CdUnidade,

                        NrProcesso = contrato.NroProcesso,
                        NrContrato = contrato.NroContrato,

                        VrContratoMes = contrato.ValorContratoMes
                    };

                    // pesquisa se já houve algum pagamento neste contrato no mes
                    var nerpItem = db.Query<NerpItem>()
                        .Include(x => x.Nerp)
                        .Where(x => x.Nerp.TpNerp == TpNerp.CessaoUso)
                        .Where(x => x.Nerp.NrMesRef == mes)
                        .Where(x => x.Nerp.NrAno == ano)
                        .Where(x => x.IdPessoaCredor == ce.IdPessoaEntidade)
                        .FirstOrDefault();

                    if(nerpItem != null)
                    {
                        // altera o valor de pagamento para apenas a diferença do que já foi pago
                        ce.VrContratoMes = ce.VrContratoMes - nerpItem.VrTotal;
                    }

                    if (ce.VrContratoMes > 0)
                    {
                        result.Add(ce);
                    }
                    else
                    {
                        throw new Exception("O contrato " + ce.NrContrato + " já foi pago este mês");
                    }
                }
                catch(Exception ex)
                {
                    erros.AppendLine(entidade.NoEntidade + " - " + ex.Message);
                }
            }

            return result.OrderBy(x => x.NoGestor).ThenBy(x => x.NoEntidade).ToList();
        }

        /// <summary>
        /// Inclui multiplas NERPs de contratos
        /// </summary>
        public void IncluiNerpCessaoUso(IEnumerable<ContratoEntidadeDTO> contratos, int ano, int mes, string noJustificativa)
        {
            var grupos = contratos.GroupBy(x => x.IdPessoaGestor);
            var mesano = string.Format("{0:00}/{1:0000}", mes, ano);
            var pessoas = new PessoaService(context);
            var funcionarios = new FuncionarioService(context);
            var func = funcionarios.BuscaFuncionarioHierarquia(user.Login);
             
             var centrocusto = new CentroCustoService(context)
                 .BuscaCentroCusto(ConfigurationManager.AppSettings["nerp.cessaouso.centrousto"].UnMask());
            
            var contacontabil = db.Query<ContaContabil>()
                .Where(x => x.NrContaContabil == ConfigurationManager.AppSettings["nerp.cessaouso.contacontabil"].UnMask())
                .Where(x => x.NrAno == nrAnoBase)
                .Where(x => x.FgAtivo)
                .SingleOrDefault();
            
            using (var trans = db.GetTransaction())
            {
                foreach (var grupo in grupos)
                {
                    // cria a nerp com os dados básicos
                    var nerp = new Nerp
                    {
                        DtEmissao = Nerp.DataEmissao(nrAnoBase),
                        TpNerp = TpNerp.CessaoUso,
                        NrNerp = db.ExecuteScalar<int>("SELECT ISNULL(MAX(NrNerp), 0) + 1 FROM Nerp WHERE NrAno = @0", nrAnoBase),
                        NrAno = nrAnoBase,
                        NoNerp = "CONTRATO CESSAO USO - " + mesano + " - " + grupo.First().NoGestor,
                        NoJustificativa = noJustificativa,
                        IdPessoaResp = func.Pessoa.IdPessoa,
                        FgAdiantamento = false,
                        NrMesRef = mes,
                        TpSituacao = NerpSituacao.Cadastrada
                    };
            
                    db.Insert(nerp);
            
                    // aponta as pessoas que vão assinar a nerp
                    db.Insert(new NerpAssinatura
                    {
                        IdNerp = nerp.IdNerp,
                        TpAprovacao = TpAprovacao.Solicitante,
                        IdPessoa = func.Pessoa.IdPessoa,
                        CdUnidade = func.Unidade.CdUnidade
                    });
            
                    db.Insert(new NerpAssinatura
                    {
                        IdNerp = nerp.IdNerp,
                        TpAprovacao = TpAprovacao.Gestor,
                        IdPessoa = grupo.First().IdPessoaGestor,
                        CdUnidade = grupo.First().CdUnidade
                    });
            
                    db.Insert(new NerpAssinatura
                    {
                        IdNerp = nerp.IdNerp,
                        TpAprovacao = TpAprovacao.Superintendente,
                        IdPessoa = func.Superintendente.Pessoa.IdPessoa,
                        CdUnidade = func.Superintendente.Unidade.CdUnidade
                    });
            
                    foreach (var convenio in grupo)
                    {
                        var item = new NerpItem
                        {
                            IdNerp = nerp.IdNerp,
                            IdPessoaCredor = convenio.IdPessoaEntidade,
                            IdCentroCusto = centrocusto.IdCentroCusto,
                            IdContaContabil = contacontabil.IdContaContabil,
                            QtItem = 1,
                            VrUnit = convenio.VrContratoMes,
                            VrTotal = convenio.VrContratoMes
                        };
            
                        nerp.Items.Add(item);
                        db.Insert(item);
                    }
            
                    // atualizando o valor total da nerp
                    nerp.VrBruto = nerp.VrLiquido = nerp.Items.Sum(x => x.VrTotal);
                    db.Update(nerp);
                }
            
                trans.Complete();
            }
        }
   }
}