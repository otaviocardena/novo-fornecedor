﻿using NBox.Services;
using NBox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using NPoco;
using Creasp.Core;
using NPoco.Linq;
using Creasp.Siplen;
using System.Configuration;
using System.Text.RegularExpressions;
using System.IO;
using Newtonsoft.Json;

namespace Creasp.Nerp
{
    public partial class NerpService : DataService
    {
        protected readonly ISiscont siscont = Factory.Get<ISiscont>();
        protected readonly int nrAnoBase;
        static object lockSolicEmp = new object();

        public NerpService(ServiceContext context) : base(context)
        {
            nrAnoBase = (context as CreaspContext).NrAnoBase;
        }

        public void AlteraNerpResponsavel(int idNerp, string tpSituacao, int codUnidade)
        {
            var nerp = db.SingleById<Nerp>(idNerp);

            nerp.TpSituacao = tpSituacao;
            nerp.CdUnidadeResp = codUnidade;

            db.AuditUpdate(nerp, typeof(Nerp), nerp.IdNerp);

        }

        public void ReabrirNerp(int idNerp, string tpSituacao, int codUnidade)
        {
            var nerp = db.SingleById<Nerp>(idNerp);

            nerp.TpSituacao = tpSituacao;
            nerp.CdUnidadeResp = codUnidade;
            nerp.FgAtivo = true;

            db.AuditUpdate(nerp, typeof(Nerp), nerp.IdNerp);

        }

        /// <summary>
        /// Valida se ainda está dentro do limite de emissão da NERP
        /// </summary>
        public static void ValidaLimiteDtEmissao(int nrAno)
        {
            // busca o parametro no web.config conforme o ano
            var dtLimite = ConfigurationManager.AppSettings["nerp.bloqueio." + nrAno];

            if (string.IsNullOrWhiteSpace(dtLimite)) return;

            if (DateTime.Now > dtLimite.To<DateTime>()) throw new NBoxException("A data limite para emissão de NERP em {0} é {1}", nrAno, dtLimite);
        }

        #region Query NERP e NerpItem

        /// <summary>
        /// Retorna as NERPs conforme o filtro selecionado
        /// </summary>
        public QueryPage<NerpResultadoDTO> ListaNerps(Pager p, int? nrNerp, int? nrEmpenho, Periodo emissao, Periodo vencto, bool? fgAtivo, int? cdUnidade, string tpNerp, string tpSituacao, bool? fgAdiantamento, int? idFavorecido, int? idAutorizador, bool minhas, string busca = "", bool isFornecedor = false, int? fornecedor = null, string numNF = "")
        {
            var sql = Sql.Builder.Append(
              @"SELECT n.IdNerp,
                       n.NrNerp,
	                   n.TpNerp,
	                   ISNULL(p.NoPessoa, u.NoUnidade) NoResponsavel,
	                   n.DtEmissao,
	                   n.DtVencto,
	                   n.NoNerp,
	                   n.TpSituacao,
	                   n.VrBruto,
	                   (SELECT SUM(CASE WHEN i.NrSolicPagto IS NULL AND i.NrSolicAdiantamento IS NULL THEN 0 ELSE i.VrTotal END) FROM NerpItem i WHERE i.IdNerp = n.IdNerp) VrSolic,
	                   (SELECT SUM(CASE WHEN i.NrPagto IS NULL AND i.NrAdiantamento IS NULL THEN 0 ELSE i.VrTotal END) FROM NerpItem i WHERE i.IdNerp = n.IdNerp) VrPagto,
	                   n.FgAtivo
                  FROM Nerp n
                  LEFT JOIN Pessoa p ON n.IdPessoaResp = p.IdPessoa
                  LEFT JOIN Unidade u ON n.CdUnidadeResp = u.CdUnidade");

            sql.Where("n.NrAno = @0", nrAnoBase);

            if (minhas)
            {
                if (isFornecedor && fornecedor != null)
                {
                    sql
                        .Where(tpNerp != null, "n.TpNerp = @0", tpNerp)
                        .Where(tpSituacao != null, "n.TpSituacao = @0", tpSituacao)
                        .Where(nrNerp.HasValue, "n.NrNerp = @0", nrNerp)
                        .Where(!emissao.IsVazio, "n.DtEmissao BETWEEN @0 AND @1", emissao.Inicio, emissao.Fim)
                        .Where(!vencto.IsVazio, "n.DtVencto BETWEEN @0 AND @1", vencto.Inicio, vencto.Fim)
                        .Where(fgAdiantamento != null, "n.FgAdiantamento = @0", fgAdiantamento)
                        .Where(fgAtivo.HasValue, "n.FgAtivo = @0", fgAtivo)
                        .Where(nrEmpenho != null, "n.IdNerp IN (SELECT IdNerp FROM NerpEmpenho WHERE NrEmpenho = @0 AND NrAnoEmpenho = @1)", nrEmpenho, nrAnoBase)
                        .Where(cdUnidade != null, "n.IdNerp IN (SELECT IdNerp FROM NerpAssinatura WHERE CdUnidade = @0 AND TpAprovacao = 'S')", cdUnidade)
                        .Where(idAutorizador != null, "n.IdNerp IN (SELECT IdNerp FROM NerpAssinatura WHERE (IdPessoa = @0 OR IdPessoaAssina = @0) AND TpAprovacao != 'S')", idAutorizador)
                        .Where(idFavorecido != null, "n.IdNerp IN (SELECT IdNerp FROM NerpItem WHERE IdPessoaCredor = @0)", idFavorecido)
                        .Where(!string.IsNullOrEmpty(busca), "(n.NoNerp LIKE @0)", "%" + busca + "%");

                    sql.Where("(n.IdFornecedorRelacionado = @0)", fornecedor);

                    return db.Page<NerpResultadoDTO>(sql, p.DefaultSort("NrNerp"));
                }

                // Lista somente as NERPs que estão no meu nome ou em minha unidade (somente ativas)
                var func = new FuncionarioService(context).BuscaFuncionarioHierarquia(user.Login);
                var resp = new PessoaService(context).ListaDelegacoes(func.Pessoa.IdPessoa).ToList();

                sql.Where("(n.IdPessoaResp IN (" +
                    string.Join(", ", resp) +
                    ") OR CdUnidadeResp = @0)", func.Unidade.CdUnidade);

                return db.Page<NerpResultadoDTO>(sql, p.DefaultSort("NrNerp"));
            }

            sql
                .Where(tpNerp != null, "n.TpNerp = @0", tpNerp)
                .Where(tpSituacao != null, "n.TpSituacao = @0", tpSituacao)
                .Where(nrNerp.HasValue, "n.NrNerp = @0", nrNerp)
                .Where(!emissao.IsVazio, "n.DtEmissao BETWEEN @0 AND @1", emissao.Inicio, emissao.Fim)
                .Where(!vencto.IsVazio, "n.DtVencto BETWEEN @0 AND @1", vencto.Inicio, vencto.Fim)
                .Where(fgAdiantamento != null, "n.FgAdiantamento = @0", fgAdiantamento)
                .Where(fgAtivo.HasValue, "n.FgAtivo = @0", fgAtivo)
                .Where(nrEmpenho != null, "n.IdNerp IN (SELECT IdNerp FROM NerpEmpenho WHERE NrEmpenho = @0 AND NrAnoEmpenho = @1)", nrEmpenho, nrAnoBase)
                .Where(cdUnidade != null, "n.IdNerp IN (SELECT IdNerp FROM NerpAssinatura WHERE CdUnidade = @0 AND TpAprovacao = 'S')", cdUnidade)
                .Where(idAutorizador != null, "n.IdNerp IN (SELECT IdNerp FROM NerpAssinatura WHERE (IdPessoa = @0 OR IdPessoaAssina = @0) AND TpAprovacao != 'S')", idAutorizador)
                .Where(idFavorecido != null, "n.IdNerp IN (SELECT IdNerp FROM NerpItem WHERE IdPessoaCredor = @0)", idFavorecido)
                .Where(!string.IsNullOrEmpty(busca), "(n.NoNerp LIKE @0)", "%" + busca + "%");

            return db.Page<NerpResultadoDTO>(sql, p.DefaultSort("NrNerp"));
        }

        /// <summary>
        /// Busca uma NERP montando as referencias
        /// </summary>
        public Nerp BuscaNerp(int idNerp, bool incluiAssinaturas = false)
        {
            var nerp = db.Query<Nerp>()
                .Include(x => x.PessoaResp)
                .Include(x => x.UnidadeResp)
                .Include(x => x.Evento, JoinType.Left)
                .IncludeMany(x => x.Items)
                .Where(x => x.NrAno == nrAnoBase) // so abre uma NERP se o ano estiver igual (evita de alterar NERP com ano errado)
                .Single(x => x.IdNerp == idNerp);

            // inclui a coleção de assinaturas
            if (incluiAssinaturas)
            {
                nerp.Assinaturas = db.Query<NerpAssinatura>()
                    .Include(x => x.Pessoa)
                    .Include(x => x.Unidade, JoinType.Left)
                    .Include(x => x.PessoaAssina, JoinType.Left)
                    .Where(x => x.IdNerp == nerp.IdNerp)
                    .ToList()
                    .OrderBy(x => x.TpAprovacao == TpAprovacao.Solicitante ? 0 : x.TpAprovacao == TpAprovacao.Gestor ? 1 : 2)
                    .ToList();
            }

            return nerp;
        }

        /// <summary>
        /// Lista os itens de uma NERP em formato paginado (pode ser utilizada na tela de Itens e Pagamentos)
        /// </summary>
        public QueryPage<NerpItem> ListNerpItem(Pager p, int idNerp, string noPessoa)
        {
            // carrega os itens da nerp
            var result = db.Query<NerpItem>()
                .Include(x => x.PessoaCredor)
                .Include(x => x.ContaContabil)
                .Include(x => x.CentroCusto)
                .Include(x => x.TipoDespesa, JoinType.Left)
                .Include(x => x.Empenho, JoinType.Left)
                .Where(x => x.IdNerp == idNerp)
                .Where(noPessoa != null, x => x.PessoaCredor.NoPessoa.StartsWith(noPessoa.ToLike()))
                .ToPage(p.DefaultSort<NerpItem>(x => x.PessoaCredor.NoPessoa));

            return result;
        }

        /// <summary>
        /// Lista os itens de uma nerp de forma agrupada e sumarizada, incluindo total
        /// </summary>
        public IEnumerable<NerpItemAgrupadoDTO> ListaNerpItemAgrupado(int idNerp)
        {
            var nerp = db.SingleById<Nerp>(idNerp);

            var items = db.Query<NerpItem>()
                .Include(x => x.CentroCusto)
                .Include(x => x.ContaContabil)
                .Include(x => x.TipoDespesa, JoinType.Left)
                .Where(x => x.IdNerp == idNerp)
                .ToList();

            var sum = items
                .GroupBy(x => new { x.IdContaContabil, x.IdCentroCusto })
                .Select(x => new NerpItemAgrupadoDTO
                {
                    QtItem = x.Sum(s => s.QtItem),
                    VrTotal = x.Sum(s => s.VrTotal),
                    NrCentroCustoFmt = x.First().CentroCusto.NrCentroCustoFmt,
                    NoItem = x.First().ContaContabil.NoContaContabilFmt
                })
                .OrderBy(x => x.NrCentroCustoFmt).ThenBy(x => x.NoItem);

            // retorna a lista sumarizada
            foreach (var s in sum) yield return s;

            // retorna o totalizador
            yield return new NerpItemAgrupadoDTO { NoItem = "TOTAL", VrTotal = nerp.VrBruto };
        }

        /// <summary>
        /// Inclui um novo item em uma NERP já existente (somente NERP de evento)
        /// </summary>
        public void IncluiNerpItem(int idNerp, string cdTipoDespesa, int idPessoaCredor, int idCentroCusto, decimal qtItem, decimal vrUnit)
        {
            var nerp = db.SingleById<Nerp>(idNerp);

            var despesa = db.Query<TipoDespesa>()
                .Include(x => x.ContaContabil)
                .Where(x => x.ContaContabil.NrAno == nerp.NrAno)
                .Single(x => x.CdTipoDespesa == cdTipoDespesa);

            var item = new NerpItem
            {
                IdNerp = idNerp,
                IdPessoaCredor = idPessoaCredor,
                CdTipoDespesa = cdTipoDespesa,
                IdCentroCusto = idCentroCusto,
                IdContaContabil = despesa.ContaContabil.IdContaContabil,
                QtItem = qtItem,
                VrUnit = vrUnit,
                VrRateio = 0,
                VrTotal = qtItem * vrUnit
            };

            db.Insert(item);

            this.AtualizaTotalizadorNerp(item.IdNerp);
        }

        /// <summary>
        /// Altera um item de uma NERP (somente NERP de evento)
        /// </summary>
        public void AlteraNerpItem(int idNerpItem, int idCentroCusto, decimal qtItem, decimal vrUnit)
        {
            var item = db.SingleById<NerpItem>(idNerpItem);

            item.IdCentroCusto = idCentroCusto;
            item.QtItem = qtItem;
            item.VrUnit = vrUnit;
            item.VrTotal = qtItem * vrUnit;

            db.AuditUpdate(item, typeof(Nerp), item.IdNerp);

            this.AtualizaTotalizadorNerp(item.IdNerp);
        }

        /// <summary>
        /// Exclui um item de uma NERP excluindo tambem possivel documentação (somente NERP de evento)
        /// </summary>
        public void ExcluiNerpItem(int idNerpItem)
        {
            var docs = new DocumentoService(context);

            docs.ExcluiDocumento(TpRefDoc.Nerp, idNerpItem);

            var item = db.SingleById<NerpItem>(idNerpItem);

            db.AuditDelete<NerpItem>(idNerpItem, typeof(Nerp), item.IdNerp);

            this.AtualizaTotalizadorNerp(item.IdNerp);
        }

        /// <summary>
        /// Faz o cancelamento de uma NERP (pode ser feito a qualquer momento)
        /// </summary>
        public void CancelaNerp(int idNerp, string noMotivo)
        {
            var nerp = db.SingleById<Nerp>(idNerp);
            nerp.IdPessoaResp = null;
            nerp.CdUnidadeResp = null;
            nerp.TpSituacao = NerpSituacao.Cancelada;
            nerp.FgAtivo = false;
            nerp.NoHistorico = (nerp.NoHistorico == null ? "" : nerp.NoHistorico + "\n") + noMotivo;

            db.Update(nerp);
        }

        public NerpPermissao PermissaoNerp(int idNerp)
        {
            var nerp = db.SingleById<Nerp>(idNerp);
            var usuario = db.SingleById<Pessoa>((user as CreaspUser).IdPessoa);
            var resp = new PessoaService(context).ListaDelegacoes(usuario.IdPessoa);
            var minha = false; // verifica se a nerp está sob a resposabilidade do usuario
            var ufi = new UnidadeService(context).UFI;

            var unidUser = new FuncionarioService(context).BuscaUnidade(usuario);

            // verifica se o responsavel pela NERP atualmente se é o usuario (ou subdelegacao)
            if (resp.Contains(nerp.IdPessoaResp))
            {
                minha = true;
            }
            // ou está na unidade do usuario
            else if (nerp.CdUnidadeResp != null)
            {

                if (nerp.CdUnidadeResp == unidUser.CdUnidade)
                {
                    minha = true;
                }
            }

            // permissão somente pro admin
            if (user.IsInRole("*")) minha = true;

            var perm = new NerpPermissao
            {
                FgAcao = minha,
                FgDocumentar = minha && (nerp.TpSituacao == NerpSituacao.Cadastrada || nerp.TpSituacao == NerpSituacao.AgPrestacaoContas || nerp.TpSituacao == NerpSituacao.AgAprovacaoGestor),
                FgEditarItem = minha && nerp.TpSituacao == NerpSituacao.Cadastrada,
                FgSolicEmpenho = minha && nerp.TpSituacao == NerpSituacao.AgSolicEmpenho,
                FgRetencao = minha && nerp.TpSituacao == NerpSituacao.AgRetencao,
                FgSolicPagto = minha && nerp.TpSituacao == NerpSituacao.AgSolicPagto,
                FgSolicAdiantamento = minha && nerp.TpSituacao == NerpSituacao.AgSolicAdiantamento,
                FgAnexarComprovAdiant = (unidUser?.CdUnidade == ufi?.CdUnidade) && (nerp.TpSituacao == NerpSituacao.AgSolicAdiantamento || nerp.TpSituacao == NerpSituacao.AgPrestacaoContas || nerp.TpSituacao == NerpSituacao.AgAprovacaoPrestacaoContas)
            };

            // permite alterar documentação a qualquer momento
            if (user.IsInRole("nerp.editdoc")) perm.FgDocumentar = true;

            return perm;
        }

        /// <summary>
        /// Atualiza os totalizadores de uma NERP
        /// </summary>
        private void AtualizaTotalizadorNerp(int idNerp)
        {
            // atualiza total da NERP
            var nerp = db.SingleById<Nerp>(idNerp);

            nerp.VrBruto = db.Query<NerpItem>()
                .Where(x => x.IdNerp == idNerp)
                .ToEnumerable()
                .Sum(x => x.VrTotal);

            nerp.VrRetencoes = db.Query<NerpRetencao>()
                .Include(x => x.Item)
                .Where(x => x.Item.IdNerp == idNerp)
                .ToEnumerable()
                .Sum(x => x.VrRetencao);

            if (nerp.VrRetencoes > nerp.VrBruto) throw new NBoxException("O valor total da retenção não pode ser maior que o valor bruto");

            nerp.VrLiquido = nerp.VrBruto - nerp.VrRetencoes;

            db.Update(nerp);
        }

        /// <summary>
        /// Valida se pessoa credora tem mais de uma diária para a mesma data passada por parâmetro
        /// </summary>
        /// <param name="idCredor">ID da pessoa credora</param>
        /// <param name="dataEvento">Data do evento para validar</param>
        /// <returns>Verdadeiro se usuário tem mais de uma diária na data</returns>
        public bool CredorTemDuasDiarias(int idCredor, DateTime dataEvento)
        {
            var sql = Sql.Builder.Append(
              @"SELECT COUNT(*)
                  FROM nerp n
                 INNER JOIN evento    e ON e.IdEvento = n.IdEvento
                 INNER JOIN NerpItem ni ON ni.IdNerp  = n.IdNerp");

            sql.Where("ni.CdTipoDespesa LIKE 'DIARIA-%'")
               .Where("n.TpSituacao <> 'CANCELADA'")
               .Where("e.FgPermiteDiariaDup = 0")
               .Where("ni.IdPessoaCredor = @0", idCredor)
               .Where("CONVERT(DATE, e.DtEvento) = @0", dataEvento.Date);

            var result = sql.SingleOrDefault<int>(db);

            return result > 1;
        }

        #endregion

        #region Favorecidos

        /// <summary>
        /// Retorna a lista de favorecidos de uma NERP para validação no SISCONT
        /// </summary>
        public List<Pessoa> ListaFavorecidos(int idNerp, bool validaSiscont, bool validaAnuidade)
        {
            var pessoas = db.Query<NerpItem>()
                .Include(x => x.PessoaCredor)
                .Where(x => x.IdNerp == idNerp)
                .OrderBy(x => x.PessoaCredor.NoPessoa)
                .ToList()
                .GroupBy(x => x.IdPessoaCredor)
                .Select(x => x.First().PessoaCredor)
                .ToList();

            if (validaSiscont || validaAnuidade)
            {
                pessoas.ForEach((p, idx) => this.ValidaPessoaSituacao(p, validaSiscont, validaAnuidade));
            }

            return pessoas;
        }

        /// <summary>
        /// Valida se a pessoa esta com aunidade/cadastrada no siscont. Retorna dentro do campo "Mensagem" da pessoa.
        /// </summary>
        public void ValidaPessoaSituacao(Pessoa p, bool validaSiscont, bool validaAnuidade)
        {
            if (p == null) return;

            var srv = new PessoaService(context);

            if (validaSiscont)
            {
                try
                {
                    srv.BuscaNomeSiscont(p);
                    p.Mensagem = "OK";
                }
                catch (NBoxException ex)
                {
                    p.Mensagem = ex.Message;
                }
            }

            // só valida anuidade se o siscont estiver OK
            if (validaAnuidade && (p.Mensagem == null || p.Mensagem == "OK"))
            {
                if (p.NrCreasp != null)
                {
                    p.Mensagem = srv.ConsultaAnuidade(p.NrCreasp) == AnuidadeSituacao.EmDebito ? "Em Débito" : "OK";
                }
            }
        }

        #endregion

        #region Documentação

        /// <summary>
        /// Lista os items da nerp com os documentos (caso exista)
        /// </summary>
        public QueryPage<NerpDocumentoDTO> ListaDocumentosItem(Pager p, int idNerp, string noPessoa, string noDespesa)
        {
            var sql = Sql.Builder.Append(
                @"SELECT ni.IdNerpItem, 
                         p.IdPessoa,
                         p.NoPessoa, 
                         CASE WHEN td.CdTipoDespesa IS NULL THEN cc.NoContaContabil ELSE td.NoTipoDespesa END NoDespesa,
                         ni.VrTotal,
                         d.NoArquivo,
                         d.IdDocumento
                    FROM NerpItem ni
                   INNER JOIN Pessoa p ON ni.IdPessoaCredor = p.IdPessoa
                   INNER JOIN ContaContabil cc ON ni.IdContaContabil = cc.IdContaContabil
                    LEFT JOIN TipoDespesa td ON ni.CdTipoDespesa = td.CdTipoDespesa
                    LEFT JOIN Documento d ON d.IdRef = ni.IdNerpItem AND d.TpRef = @0
                   WHERE ni.IdNerp = @1", TpRefDoc.Nerp, idNerp);

            if (noPessoa != null) sql.Append(" AND p.NoPessoa LIKE @0", noPessoa.ToLike() + "%");
            if (noDespesa != null) sql.Append(" AND (CASE WHEN td.CdTipoDespesa IS NULL THEN cc.NoContaContabil ELSE td.NoTipoDespesa END) LIKE @0", noDespesa.ToLike() + "%");

            return db.Page<NerpDocumentoDTO>(sql, p.DefaultSort("NoPessoa"));
        }

        /// <summary>
        /// Inclui um novo documento anexado a um item de nerp
        /// </summary>
        public void IncluiDocumentoItem(int idNerpItem, HttpPostedFile file)
        {
            var item = db.SingleById<NerpItem>(idNerpItem);
            var docs = new DocumentoService(context);

            docs.EnviaDocumento(TpRefDoc.Nerp, idNerpItem, item.IdPessoaCredor, null, file);
        }

        /// <summary>
        /// Inclui apenas 1 arquivo e inclui N referencias
        /// </summary>
        public void IncluiDocumentoMultiplos(IEnumerable<int> idNerpItems, HttpPostedFile file)
        {
            var storage = Factory.Get<IStorage>();
            var fileId = storage.Upload("creasp" + DateTime.Now.ToString("-yyyyMM") + ":", file);

            using (var trans = db.GetTransaction())
            {
                foreach (var idNerpItem in idNerpItems)
                {
                    var item = db.SingleById<NerpItem>(idNerpItem);

                    var doc = new Documento
                    {
                        TpRef = TpRefDoc.Nerp,
                        IdRef = idNerpItem,
                        IdPessoa = item.IdPessoaCredor,
                        NoArquivo = Path.GetFileName(file.FileName),
                        DtCadastro = DateTime.Now,
                        FileId = fileId
                    };

                    db.Insert(doc);
                }

                trans.Complete();
            }
        }

        #endregion

        #region Consulta Saldo Orçamentario

        /// <summary>
        /// Verifica se a NERP possui saldo orçamentário suficiente no SISCONT. Gera exception caso não tenha
        /// </summary>
        public void ValidaSaldoOrcamentario(int idNerp)
        {
            var siscont = Factory.Get<ISiscont>();
            var centrocustos = new CentroCustoService(context);
            var emps = new NerpService(context).ListaEmpenhos(idNerp);
            var nerp = db.SingleById<Nerp>(idNerp);

            // busca os valores que deverão ser empenhados (mas ainda não foram)
            foreach (var emp in emps.Where(x => x.IdNerpEmpenho == 0))
            {
                // valida por centro de custo
                foreach (var nrCentroCusto in emp.VrCentroCusto.Keys)
                {
                    var valor = emp.VrCentroCusto[nrCentroCusto];
                    var centrocusto = centrocustos.BuscaCentroCusto(nrCentroCusto);
                    var centroCustoOrçado = centrocustos.BuscaCentroCustoOrçado(centrocusto); // todo: renomear variável para orçado
                    var dtRef = Nerp.DataEmissao(nerp.NrAno); // usa a data de hoje ou o ultimo dia do ano

                    var saldo = siscont.ConsultaSaldoOrcamentario(dtRef, emp.ContaContabil.NrContaContabilFmt, centroCustoOrçado.NrCentroCustoFmt);

                    if (saldo == null)
                    {
                        throw new NBoxException("Não foi encontrado orçamento para a conta {0} no centro de custo {1}",
                            emp.ContaContabil.NrContaContabilFmt, centroCustoOrçado.NrCentroCustoFmt);
                    }
                    else if (saldo.SaldoOrcamentarioDesbloqueado < valor)
                    {
                        throw new NBoxException("Não existe saldo orçamentário na conta {0} para o centro de custo {1}. O saldo disponível é de {2:c} (sua solicitação é de {3:c})",
                            emp.ContaContabil.NrContaContabilFmt, centroCustoOrçado.NrCentroCustoFmt, saldo.SaldoOrcamentarioDesbloqueado, valor);
                    }
                }
            }
        }

        #endregion

        #region Desfazer Empenho/Pagto/Adianta

        /// <summary>
        /// Operação para desfazer as solicitações de empenho que ainda não estejam vinculadas
        /// </summary>
        public void DesfazSolicEmp(int idNerp)
        {
            this.VerificaSiscontEmpenho(db.SingleById<Nerp>(idNerp));

            using (var trans = db.GetTransaction())
            {
                // busca os empenhos a serem excluidos (com solicitação mas sem numero de empenho)
                var idEmpenhos = db.Query<NerpEmpenho>()
                    .Where(x => x.IdNerp == idNerp)
                    .Where(x => x.NrSolicEmpenho != null)
                    .Where(x => x.NrEmpenho == null)
                    .ToList()
                    .Select(x => (int?)x.IdNerpEmpenho)
                    .ToList();

                if (idEmpenhos.Count == 0) throw new NBoxException("Não existe nenhuma solicitação de empenho para ser desfeita");

                // limpa todos os relacionamentos destes empenhos
                db.UpdateMany<NerpItem>()
                    .Where(x => x.IdNerp == idNerp)
                    .Where(x => idEmpenhos.Contains(x.IdNerpEmpenho))
                    .OnlyFields(x => x.IdNerpEmpenho)
                    .Execute(new NerpItem { IdNerpEmpenho = null });

                // exclui os empenhos
                foreach (var idEmpenho in idEmpenhos)
                {
                    db.Delete<NerpEmpenho>(idEmpenho);
                }

                log.Table("Nerp", idNerp, "Exclui as solicitações de empenho");

                trans.Complete();
            }
        }

        /// <summary>
        /// Desfaz solicitações de pagamento que ainda não tenham sido atendidas no SISCONT
        /// </summary>
        public void DesfazSolicPagto(int idNerp, IEnumerable<int> idNerpItems)
        {
            this.VerificaSiscontPagto(db.SingleById<Nerp>(idNerp));

            using (var trans = db.GetTransaction())
            {
                foreach (var idNerpItem in idNerpItems)
                {
                    var item = db.SingleById<NerpItem>(idNerpItem);

                    if (item.NrSolicPagto == null) throw new NBoxException("Não existe solicitação de pagamento no item " + item.IdNerpItem);
                    if (item.NrPagto != null) throw new NBoxException("Não é possivel desfazer um pagamento já confirmado no SISCONT");

                    item.NrSolicPagto = null;

                    db.AuditUpdate(item, typeof(Nerp), idNerp);
                }

                trans.Complete();
            }
        }

        /// <summary>
        /// Estorna pagamentos que já tenham sido estornados no SISCONT
        /// </summary>
        public void EstornaPagto(int idNerp, IEnumerable<int> idNerpItems)
        {
            this.VerificaSiscontPagto(db.SingleById<Nerp>(idNerp));

            using (var trans = db.GetTransaction())
            {
                foreach (var idNerpItem in idNerpItems)
                {
                    var item = db.SingleById<NerpItem>(idNerpItem);

                    if (item.NrSolicPagto == null) throw new NBoxException("Não existe solicitação de pagamento no item " + item.IdNerpItem);
                    if (item.NrPagto == null) throw new NBoxException("Não é possível estornar um pagamento que não tenha sido confirmado no SISCONT.");

                    item.NrSolicPagto = null;
                    item.NrPagto = null;
                    item.NrAnoPagto = null;
                    item.DtPagto = null;

                    db.AuditUpdate(item, typeof(Nerp), idNerp);
                }

                trans.Complete();
            }
        }

        /// <summary>
        /// Desfaz solicitações de adiantamento que ainda não tenham sido atendidas no SISCONT
        /// </summary>
        public void DesfazSolicAdiantamento(int idNerp, IEnumerable<int> idNerpItems)
        {
            // esta comentado abaixo pq não existe esta implementação no siscont
            //this.VerificaSiscontAdiantamento(db.SingleById<Nerp>(idNerp));

            using (var trans = db.GetTransaction())
            {
                foreach (var idNerpItem in idNerpItems)
                {
                    var item = db.SingleById<NerpItem>(idNerpItem);

                    if (item.NrSolicAdiantamento == null) throw new NBoxException("Não existe solicitação de adiantamento no item " + item.IdNerpItem);
                    if (item.NrAdiantamento != null) throw new NBoxException("Não é possivel desfazer um adiantamento já confirmado no SISCONT");

                    item.NrSolicAdiantamento = null;

                    db.AuditUpdate(item, typeof(Nerp), idNerp);
                }

                trans.Complete();
            }
        }

        #endregion

        #region Empenho

        /// <summary>
        /// Retorna a lista dos empenhos de uma nerp incluindo empenhos que ainda não foram nem solicitados (projeção)
        /// </summary>
        public IEnumerable<NerpEmpenho> ListaEmpenhos(int idNerp)
        {
            // primeiro lista os empenhos que existem
            var nerp = db.SingleById<Nerp>(idNerp);
            var emps = db.Query<NerpEmpenho>()
                .Include(x => x.Pessoa)
                .Include(x => x.ContaContabil)
                .IncludeMany(x => x.Items)
                .Where(x => x.IdNerp == idNerp)
                .ToList();

            var centrocustos = new CentroCustoService(context);

            // preenchendo os empenhos com agrupador por centro de custo
            foreach (var emp in emps)
            {
                var cc = emp.Items.GroupBy(x => x.IdCentroCusto);
                emp.VrCentroCusto = new Dictionary<string, decimal>();

                foreach (var c in cc)
                {
                    var subarea = db.SingleById<CentroCusto>(c.Key);
                    emp.VrCentroCusto[subarea.NrCentroCusto] = c.Sum(x => x.VrTotal);
                }

                yield return emp;
            }

            // pega todos os items da nerp que ainda não tem empenho
            var items = db.Query<NerpItem>()
                .Include(x => x.CentroCusto)
                .Include(x => x.ContaContabil)
                .Include(x => x.PessoaCredor)
                .Include(x => x.TipoDespesa, JoinType.Left)
                .Where(x => x.IdNerp == idNerp)
                .Where(x => x.IdNerpEmpenho == null)
                .ToList();

            // se não tiver nenhum item a ser empenhado, finaliza
            if (items.Count == 0) yield break;

            // atualiza os centro de custos para subir de SubArea para Analitico (quando for o caso)
            items.ForEach((n, i) =>
            {
                var centroCustoOrçado = centrocustos.BuscaCentroCustoOrçado(n.CentroCusto);
                n.IdCentroCusto = centroCustoOrçado.IdCentroCusto;
                n.CentroCusto = centroCustoOrçado;
            });

            // agrupa os items conforme Conta Contabil + Cpf Agrupador do Tipo de despesa / centro de custo
            // agrupa também os credores - se for só 1 credor, usa ele, senão seta pra -1
            var empSum = items
                .GroupBy(x => x.IdContaContabil.ToString() + "-" + x.TipoDespesa?.NrCpfAgrupado)
                .Select(x => new
                {
                    ContaContabil = x.First().ContaContabil,
                    VrTotal = x.Sum(s => s.VrTotal),
                    Pessoa = x.First().PessoaCredor,
                    IdPessoa = x.Min(s => s.IdPessoaCredor) == x.Max(s => s.IdPessoaCredor) ?
                        x.Min(s => s.IdPessoaCredor) : -1,
                    NrCpfAgrupado = x.First().TipoDespesa?.NrCpfAgrupado,
                    VrCentroCusto = x.GroupBy(c => c.IdCentroCusto)
                        .Select(c => new { NrCentroCusto = c.First().CentroCusto.NrCentroCusto, VrTotal = c.Sum(t => t.VrTotal) })
                });

            // cria os emepnhos
            foreach (var emp in empSum)
            {
                // se tiver mais de um credor, usa um credor padrão
                var credor = emp.IdPessoa == -1 ?
                    new PessoaService(context).BuscaCredorPadrao(emp.NrCpfAgrupado, (nerp.TpNerp == TpNerp.ART || nerp.TpNerp == TpNerp.CessaoUso)) :
                    emp.Pessoa;

                var empenho = new NerpEmpenho
                {
                    IdNerp = idNerp,
                    IdContaContabil = emp.ContaContabil.IdContaContabil,
                    ContaContabil = emp.ContaContabil,
                    IdPessoa = credor.IdPessoa,
                    Pessoa = credor,
                    VrEmpenho = emp.VrTotal,
                    VrCentroCusto = emp.VrCentroCusto.ToDictionary((k) => k.NrCentroCusto, (v) => v.VrTotal)
                };

                yield return empenho;
            }
        }

        /// <summary>
        /// Agrupa os items por conta contabil, insere na NerpEmpenho, atualiza os apontamentoda NerpEmpenho e chama o WS do siscont. Retorna a quantidade de empenhos gerados
        /// </summary>
        public int SolicEmp(int idNerp)
        {
            lock (lockSolicEmp)
            {
                var count = 0;
                var nerp = db.SingleById<Nerp>(idNerp);

                // pega somente os que não tem Id (são projeção - não existem no banco de dados)
                foreach (var emp in ListaEmpenhos(idNerp).Where(x => x.IdNerpEmpenho == 0))
                {
                    // chama o webservice
                    var guid = siscont.IncluiSolicEmp(
                        nerp.FgAdiantamento ? Siscont.Despesa.EmpenhoTipo.Estimativo : Siscont.Despesa.EmpenhoTipo.Ordinario,
                        emp.ContaContabil.NrContaContabilFmt,
                        Nerp.DataEmissao(nerp.NrAno),
                        emp.VrEmpenho,
                        emp.Pessoa,
                        "NERP " + nerp.NrNerp + "/" + nerp.NrAno + " - " + nerp.NoJustificativa,
                        emp.VrCentroCusto);

                    emp.NrSolicEmpenho = guid;

                    db.Insert(emp);

                    // atualiza os itens para apontar para o empenho criado
                    db.UpdateMany<NerpItem>()
                        .Where(x => x.IdNerp == idNerp)
                        .Where(x => x.IdContaContabil == emp.IdContaContabil)
                        .Where(x => x.IdNerpEmpenho == null)
                        .OnlyFields(x => x.IdNerpEmpenho)
                        .Execute(new NerpItem { IdNerpEmpenho = emp.IdNerpEmpenho });

                    count++;
                }

                if (count > 0)
                {
                    log.Table("Nerp", idNerp, "Solicitou empenho");
                }

                return count;
            }

        }

        /// <summary>
        /// Verifica se houve algum retorno do siscont em relação a empenhos. Se concluido o empenho, passa para a proxima etapa da NERP
        /// </summary>
        public bool VerificaSiscontEmpenho(Nerp nerp)
        {
            var alterou = false;

            // verifica se existe algum empenho a ser pesquisado
            var empenhos = db.Query<NerpEmpenho>()
                .Where(x => x.IdNerp == nerp.IdNerp)
                .Where(x => x.NrSolicEmpenho != null && x.NrEmpenho == null) // tem solicitação mas nao tem empenho
                .ToList();

            // consulta os empenhos
            foreach (var emp in empenhos)
            {
                var sisemp = siscont.ConsultaEmpenho(nerp.NrAno, emp.NrSolicEmpenho)?.FirstOrDefault();

                if (sisemp == null) continue;

                emp.NrEmpenho = sisemp.Numero;
                emp.NrAnoEmpenho = sisemp.Exercicio;

                db.Update(emp);

                alterou = true;
            }

            return alterou;
        }

        #endregion

        #region Pagamentos

        /// <summary>
        /// Faz solicitação de pagamento para as nerpItens selecionadas
        /// </summary>
        public int SolicPagtos(IEnumerable<int> idNerpItems, DateTime dtPagto, string tpDoc, string nrDoc, DateTime dtEmissaoDoc)
        {
            var centrocustos = new CentroCustoService(context);
            var pessoas = new PessoaService(context);
            var pagtos = new List<Siscont.Despesa.PagamentosAutorizadosEntity>();
            var items = new List<NerpItem>();
            Nerp nerp = null;

            // verifica se existe algum dos item que tenha solicitação
            var count = db.Query<NerpItem>()
                .Where(x => x.NrSolicPagto != null)
                .Where(x => idNerpItems.Contains(x.IdNerpItem))
                .Count();

            if (count > 0) throw new NBoxException("Existem items que já possuem solicitação de pagamento");

            foreach (var idNerpItem in idNerpItems)
            {
                var item = db.Query<NerpItem>()
                    .Include(x => x.Empenho, JoinType.Left)
                    .Include(x => x.PessoaCredor)
                    .Include(x => x.CentroCusto)
                    .Include(x => x.ContaContabil)
                    .Where(x => x.IdNerpItem == idNerpItem)
                    .Single();

                if (nerp == null)
                {
                    // busca a nerp com dados do evento (caso seja de evento)
                    nerp = db.Query<Nerp>()
                        .Include(x => x.Evento, JoinType.Left)
                        .Where(x => x.IdNerp == item.IdNerp)
                        .Single();
                }

                // evita buscas se a nerp for de evento
                if (nerp.TpNerp != TpNerp.Evento)
                {
                    // busca as retenções para o item
                    item.Retencoes = db.Query<NerpRetencao>()
                        .Include(x => x.Favorecido)
                        .Where(x => x.IdNerpItem == idNerpItem)
                        .ToList();

                    //TODO: busca os repasses a associações
                }

                // monta histórico
                var historico = "PAGAMENTO REFERENTE A ";

                if (nerp.Evento != null)
                {
                    historico += nerp.Evento.NoEvento + " - " + nerp.Evento.NoLocal + " - " + nerp.Evento.DtEvento.ToString("dd/MM/yyyy");
                }

                historico += " " + nerp.NrNerp + "/" + nerp.NrAno;
                historico += " - " + item.PessoaCredor.NoPessoa;

                // sem emepnho, sem pagamento
                if (item.Empenho == null || item.Empenho.NrEmpenho == null) throw new NBoxException("{0} sem empenho", item.PessoaCredor.NoPessoa);

                CentroCusto ccOrçado;

                //Quando adiado para o ano seguinte o pagamento, deve ser conferido se foi criado o empenho do tipo resto a pagar no SISCONT
                if (dtPagto.Year > nerp.NrAno)
                {
                    ccOrçado = centrocustos.BuscaCentroCustoOrçadoRP(item.CentroCusto);

                    var empenho = siscont.ConsultaEmpenho(dtPagto.Year, null, null, item.Empenho.NrEmpenho, null, true).FirstOrDefault();
                    if (empenho == null) throw new NBoxException("Empenho {0} cadastrado em {1} sem resto a pagar cadastrado em {2} no SISCONT", item.Empenho.NrEmpenho, item.Empenho.NrAnoEmpenho, dtPagto.Year);

                    // atualiza o empenho
                    item.Empenho.FgRestoAPagar = empenho.RestoAPagar;
                    item.Empenho.NrAnoEmpenho = empenho.Exercicio;

                    db.Update(item.Empenho);
                }
                else
                {
                    // busca o centro de custo orçado
                    ccOrçado = centrocustos.BuscaCentroCustoOrçado(item.CentroCusto);
                }

                //TODO: deveria ter uma classe propria minha e não do siscont. A questão de formatação ta ruim assim
                var pg = new Siscont.Despesa.PagamentosAutorizadosEntity
                {
                    // IdPagamentoExterno = item.IdNerpItem,
                    CodigoIdentificador = nerp.NrNerp.ToString(),
                    AnoEmpenho = item.Empenho.NrAnoEmpenho.Value,
                    NumeroEmpenho = item.Empenho.NrEmpenho,
                    DataPagamento = dtPagto,
                    CodigoConta = item.ContaContabil.NrContaContabilFmt,
                    CPFCNPJFavorecido = item.PessoaCredor.NrCpfFmt,
                    NomeFavorecido = pessoas.BuscaNomeSiscont(item.PessoaCredor),
                    TipoPessoaFisicaFavorecido = item.PessoaCredor.NrCpf.Length == 11,
                    ValorPagamento = item.VrTotal,
                    NumeroDocumento = nrDoc,
                    RestoAPagar = item.Empenho.FgRestoAPagar,
                    Processado = item.Empenho.FgProcessado,
                    TipoDocumento = tpDoc,
                    DataEmissaoDocumento = dtEmissaoDoc,
                    Historico = historico,
                    CentroCustos = new Siscont.Despesa.PagamentosAutorizadosCentroCustosEntity[] {
                        new Siscont.Despesa.PagamentosAutorizadosCentroCustosEntity
                        {
                            CodigoCentroCustos = ccOrçado.NrCentroCustoFmt,
                            NomeCentroCustos = ccOrçado.NoCentroCusto,
                            Valor = item.VrTotal,
                            CodigoSubArea = item.CentroCusto.CdSubArea,
                            NomeSubArea = item.CentroCusto.NoCentroCusto
                        } },
                    Retencoes = item.Retencoes.Select(x => new Siscont.Despesa.PagamentosAutorizadosRetencoesEntity
                    {
                        NomeTributo = x.NoTributo,
                        ValorBaseCalculo = x.VrBase,
                        ValorTributado = x.VrRetencao,
                        CPFCNPJFavorecido = x.Favorecido.NrCpfFmt,
                        DataVencimento = x.DtVencto,
                    }).ToArray()
                };

                // em caso de ART ou Cessão de Uso deve enviar dados de Repasse Associacao
                if (nerp.TpNerp == TpNerp.ART || nerp.TpNerp == TpNerp.CessaoUso)
                {
                    pg.RepasseAssociacao = new Siscont.Despesa.PagamentosAutorizadosRepassesAssociacoesEntity
                    {
                        TipoPagamentoAssociacao = nerp.TpNerp == TpNerp.ART ? "16% ART" : "CESSÃO DE USO E ESPAÇO",
                        MesReferenciaInicial = nerp.NrMesRef.Value,
                        MesReferenciaFinal = nerp.NrMesRef.Value
                    };

                    if (nerp.TpNerp == TpNerp.CessaoUso)
                    {
                        var faeasp = pessoas.BuscaOuIncluiPessoa(ConfigurationManager.AppSettings["cessaouso.rateiocnpj"]);

                        pg.RepasseAssociacao.Rateios = new Siscont.Despesa.PagamentosAutorizadosRepassesAssociacoesRateiosEntity[]
                        {
                            new Siscont.Despesa.PagamentosAutorizadosRepassesAssociacoesRateiosEntity
                            {
                                CPFCNPJPessoaRateio = faeasp.NrCpfFmt,
                                NomePessoaRateio = faeasp.NoPessoa,
                                Valor = item.VrRateio
                            }
                        };
                    }
                }

                items.Add(item);
                pagtos.Add(pg);
            }

            if (pagtos.Count == 0) return 0;

            // O retorno deste webservice é o mesmo array (mesma instancia) porem preenchido os IdPagamnetoAutorizado
            siscont.IncluiSolicPagtos(pagtos.ToArray());

            using (var trans = db.GetTransaction())
            {
                //foreach(var pagto in pagtos)
                //{
                //    var item = items.Single(x => x.IdNerpItem == pagto.IdPagamentoExterno);
                //
                //    item.NrSolicPagto = pagto.IdPagamentoAutorizado;
                //
                //    db.Update(item);
                //}
                var index = 0;
                foreach (var idNerpItem in idNerpItems)
                {
                    var item = items.Single(x => x.IdNerpItem == idNerpItem);
                    item.NrSolicPagto = pagtos[index++].IdPagamentoAutorizado;
                    db.Update(item);
                }

                trans.Complete();
            }

            log.Table("Nerp", nerp.IdNerp, string.Format("Solicitou {0} pagamento(s): {1:n}", pagtos.Count, pagtos.Sum(x => x.ValorPagamento)));

            return pagtos.Count;
        }

        /// <summary>
        /// Verifica se houve algum retorno do siscont em relação a pagamentos. Se concluido os pagamentos, passa para a proxima etapa da NERP
        /// </summary>
        public bool VerificaSiscontPagto(Nerp nerp)
        {
            var alterou = false;

            // verifica os pagamentos que ainda precisam de confiramação
            var pagtos = db.Query<NerpItem>()
                .Include(x => x.Empenho)
                .Where(x => x.IdNerp == nerp.IdNerp)
                .Where(x => x.Empenho.NrEmpenho != null) // só os q tem empenho
                .Where(x => x.NrSolicPagto != null && x.NrPagto == null) // tem solic mas nao tem confirmação
                .ToList();

            // consulta os pagamentos por nro da solicitação
            foreach (var pag in pagtos)
            {
                // verifica se existe pagamento no ano do empenho ou no ano seguinte. 
                int exercicio = nerp.NrAno;

                var sispagto = siscont.ConsultaPagto(exercicio, pag.NrSolicPagto).FirstOrDefault();

                if (sispagto == null)
                {
                    sispagto = siscont.ConsultaPagto(exercicio + 1, pag.NrSolicPagto).FirstOrDefault();

                    if (sispagto == null) continue;
                }

                pag.NrPagto = sispagto.NumeroPagamento;
                pag.NrAnoPagto = exercicio; // o siscont não retorna o ano do pagamento, 
                pag.DtPagto = sispagto.DataPagamento;

                db.Update(pag);

                alterou = true;
            }

            return alterou;
        }

        #endregion

        #region Adiantamentos

        /// <summary>
        /// Faz solicitação de adiantamento para as nerpItens selecionadas
        /// </summary>
        public int SolicAdiantamento(IEnumerable<int> idNerpItems, DateTime dtAdiantamento, Periodo perido, int idContaBanco, int idContaResp, int? idContaPassivo, string tpDoc, string nrDoc, DateTime dtEmissaoDoc)
        {
            var centrocustos = new CentroCustoService(context);
            var pessoas = new PessoaService(context);
            var adiantamentos = new List<Siscont.Despesa.SolicitacoesAdiantamentosEntity>();
            var items = new List<NerpItem>();
            Nerp nerp = null;

            // verifica se existe algum dos item que tenha solicitação
            var count = db.Query<NerpItem>()
                .Where(x => x.NrSolicPagto != null)
                .Where(x => idNerpItems.Contains(x.IdNerpItem))
                .Count();

            if (count > 0) throw new NBoxException("Existem items que já possuem solicitação de adiantamento");


            foreach (var idNerpItem in idNerpItems)
            {
                var item = db.Query<NerpItem>()
                    .Include(x => x.Empenho, JoinType.Left)
                    .Include(x => x.PessoaCredor)
                    .Include(x => x.CentroCusto)
                    .Include(x => x.ContaContabil)
                    .Where(x => x.IdNerpItem == idNerpItem)
                    .Single();

                if (nerp == null)
                {
                    // busca a nerp com dados do evento (caso seja de evento)
                    nerp = db.Query<Nerp>()
                        .Include(x => x.Evento, JoinType.Left)
                        .Where(x => x.IdNerp == item.IdNerp)
                        .Single();
                }

                // monta histórico
                var historico = $"SUPRIMENRO DO FUNDO {perido.Inicio.ToString("d")} ATÉ {perido.Fim.ToString("d")} EM FAVOR DE {item.PessoaCredor.NoPessoa} - NERP {nerp.NrNerp}/{nerp.NrAno}";

                // sem emepnho, sem pagamento
                if (item.Empenho == null || item.Empenho.NrEmpenho == null) throw new NBoxException("{0} sem empenho", item.PessoaCredor.NoPessoa);

                // busca o centro de custo orçado
                var ccOrçado = centrocustos.BuscaCentroCustoOrçado(item.CentroCusto);

                // busca as contas contabeis
                var contaBanco = db.SingleById<ContaContabil>(idContaBanco);
                var contaResp = db.SingleById<ContaContabil>(idContaResp);
                var contaPassivo = idContaPassivo == null ? null : db.SingleById<ContaContabil>(idContaPassivo);

                var ad = new Siscont.Despesa.SolicitacoesAdiantamentosEntity
                {
                    AnoEmpenho = item.Empenho.NrAnoEmpenho.Value,
                    NumeroEmpenho = item.Empenho.NrEmpenho,
                    DataAdiantamento = dtAdiantamento,
                    DataInicioAdiantamento = perido.Inicio,
                    DataFimAdiantamento = perido.Fim,
                    ValorAdiantamento = item.VrTotal,
                    CodigoContaResponsavelSuprimento = contaResp.NrContaContabilFmt,
                    NomeContaResponsavelSuprimento = contaResp.NoContaContabil,
                    CodigoCentroCusto = ccOrçado.NrCentroCustoFmt,
                    NomeCentroCusto = ccOrçado.NoCentroCusto,
                    CodigoSubArea = item.CentroCusto.CdSubArea,
                    NomeSubArea = item.CentroCusto.NoCentroCusto,
                    // CodigoContaDespesa = , - sempre é via empenho
                    // NomeContaDespesa = , - sempre é via empenho
                    CPFCNPJFavorecido = item.PessoaCredor.NrCpfFmt,
                    NomeFavorecido = pessoas.BuscaNomeSiscont(item.PessoaCredor), //item.PessoaCredor.NoPessoa --> Usar para Mock
                    // NumeroProcesso = "",
                    NomeFormaPagamento = "Arquivo Banco",
                    NumeroFormaPagamento = "",
                    CodigoContaBanco = contaBanco.NrContaContabilFmt,
                    NomeContaBanco = contaBanco.NoContaContabil,
                    NomeTipoDocumento = tpDoc,
                    NumeroDocumento = nrDoc,
                    CodigoContaPassivo = contaPassivo?.NrContaContabilFmt,
                    NomeContaPassivo = contaPassivo?.NoContaContabil,
                    DataEmissaoDocumentoFiscal = dtEmissaoDoc,
                    HistoricoAdiantamento = historico
                };

                items.Add(item);
                adiantamentos.Add(ad);
            }

            if (adiantamentos.Count == 0) return 0;

            // O retorno deste webservice é o mesmo array (mesma instancia) porem preenchido os IdSolicitacaoAdiantamento
            siscont.IncluiSolicAdiantamento(adiantamentos.ToArray());

            using (var trans = db.GetTransaction())
            {
                //foreach (var adiantamento in adiantamentos)
                //{
                //    var item = items.Single(x => x.IdNerpItem == adiantamentos.IdPagamentoExterno);
                //
                //    item.NrSolicPagto = adiantamento.IdSolicitacaoAdiantamento;
                //
                //    db.Update(item);
                //}

                // provisório - pois o webserivce ainda nao possui o CodigoExteno
                var index = 0;
                foreach (var idNerpItem in idNerpItems)
                {
                    var item = items.Single(x => x.IdNerpItem == idNerpItem);
                    item.NrSolicAdiantamento = adiantamentos[index++].IdSolicitacaoAdiantamento;
                    db.Update(item);
                }

                trans.Complete();
            }

            log.Table("Nerp", nerp.IdNerp, string.Format("Solicitou {0} adiantamentos(s): {1:n}", adiantamentos.Count, adiantamentos.Sum(x => x.ValorAdiantamento)));

            return adiantamentos.Count;
        }

        /// <summary>
        /// Verifica se houve algum retorno do siscont em relação a adiantamentos.
        /// </summary>
        public bool VerificaSiscontAdiantamento(Nerp nerp)
        {
            var alterou = false;

            // verifica os pagamentos que ainda precisam de confiramação
            var items = db.Query<NerpItem>()
                .Include(x => x.Empenho)
                .Where(x => x.IdNerp == nerp.IdNerp)
                .Where(x => x.Empenho.NrEmpenho != null) // só os q tem empenho
                .Where(x => x.NrSolicAdiantamento != null && x.NrAdiantamento == null) // tem solic mas nao tem confirmação
                .ToList();

            // consulta os adiantamentos por nro da solicitação
            foreach (var item in items)
            {
                var adiantamento = siscont.ConsultaAdiantamento(nerp.NrAno, item.NrSolicAdiantamento.Value);

                if (adiantamento == null) continue;

                item.NrAdiantamento = adiantamento.NrAdiantamento;
                item.NrAnoAdiantamento = adiantamento.NrAno;
                item.DtAdiantamento = adiantamento.DtAdiantamento;

                db.Update(item);

                alterou = true;
            }

            return alterou;
        }

        #endregion

        #region Retenção

        /// <summary>
        /// Lista todas as retenções de uma NERP
        /// </summary>
        public IEnumerable<NerpRetencao> ListaRetencoes(int idNerp)
        {
            return db.Query<NerpRetencao>()
                .Include(x => x.Favorecido)
                .Include(x => x.Item)
                .Where(x => x.Item.IdNerp == idNerp)
                .OrderBy(x => x.NoTributo)
                .ToList();
        }

        /// <summary>
        /// Inclui uma nova retenção vinculada a um item de NERP
        /// </summary>
        public void IncluiRetencao(int idNerpItem, string cdTributo, string noTributo, decimal vrBase, decimal pcRetencao, decimal vrRetencao, DateTime? dtVencto, int idPessoaFav)
        {
            // validações basicas
            if (dtVencto < DateTime.Today) throw new NBoxException("A data de vencimento não pode ser menor que hoje");

            var item = db.SingleById<NerpItem>(idNerpItem);

            if (item.NrSolicPagto != null) throw new NBoxException("Já existe uma solicitação de pagamento para este item");
            if (vrBase > item.VrTotal) throw new NBoxException("O valor base não pode ser maior que {0:n}", item.VrTotal);
            if (vrRetencao > item.VrTotal) throw new NBoxException("O valor do tributo não pode ser maior que {0:n}", item.VrTotal);
            if (vrRetencao <= 0) throw new NBoxException("O valor do tributo deve ser maior que zero");

            // verifica se o tributo ja não está cadastrado
            if (db.Query<NerpRetencao>().Count(x => x.IdNerpItem == idNerpItem && x.NoTributo == noTributo && x.IdPessoaFav == idPessoaFav) > 0)
            {
                throw new NBoxException("Este retenção já foi lançada para este favorecido nesta NERP");
            }

            var ret = new NerpRetencao
            {
                IdNerpItem = idNerpItem,
                CdTributo = cdTributo,
                NoTributo = noTributo,
                VrBase = vrBase,
                PcRetencao = pcRetencao,
                VrRetencao = vrRetencao,
                IdPessoaFav = idPessoaFav,
                DtVencto = dtVencto
            };

            db.Insert(ret);

            this.AtualizaTotalizadorNerp(item.IdNerp);
        }

        /// <summary>
        /// Altera uma retenção
        /// </summary>
        public void AlteraRetencao(int idRetencao, int idNerpItem, decimal vrBase, decimal pcRetencao, decimal vrRetencao, DateTime? dtVencto, int idPessoaFav)
        {
            var item = db.SingleById<NerpItem>(idNerpItem);

            if (item.NrSolicPagto != null) throw new NBoxException("Já existe uma solicitação de pagamento para este item");
            if (vrBase > item.VrTotal) throw new NBoxException("O valor base não pode ser maior que {0:n}", item.VrTotal);
            if (vrRetencao > item.VrTotal) throw new NBoxException("O valor do tributo não pode ser maior que {0:n}", item.VrTotal);
            if (vrRetencao <= 0) throw new NBoxException("O valor do tributo deve ser maior que zero");

            var ret = db.SingleById<NerpRetencao>(idRetencao);

            ret.VrBase = vrBase;
            ret.PcRetencao = pcRetencao;
            ret.VrRetencao = vrRetencao;
            ret.IdPessoaFav = idPessoaFav;
            ret.DtVencto = dtVencto;

            db.AuditUpdate(ret, typeof(Nerp), item.IdNerp);

            AtualizaTotalizadorNerp(item.IdNerp);
        }
        /// <summary>
        /// Exclui uma retenção
        /// </summary>
        public void ExcluiRetencao(int idNerpRetencao)
        {
            var idNerp = db.Query<NerpRetencao>()
                .Include(x => x.Item)
                .Where(x => x.IdNerpRetencao == idNerpRetencao)
                .Single().Item.IdNerp;

            db.Delete<NerpRetencao>(idNerpRetencao);

            AtualizaTotalizadorNerp(idNerp);
        }

        #endregion

        #region Workflow

        public NerpWorkFlow GetWorkflow(int idNerp)
        {
            var nerp = BuscaNerp(idNerp, true);

            if (nerp.TpNerp == TpNerp.Evento) return new NerpWorkFlowEvento(nerp, context, db);
            if (nerp.TpNerp == TpNerp.ART) return new NerpWorkFlowART(nerp, context, db);
            if (nerp.TpNerp == TpNerp.CessaoUso) return new NerpWorkFlowCessaoUso(nerp, context, db);
            if (nerp.TpNerp == TpNerp.ComEmpenho) return new NerpWorkFlowComEmpenho(nerp, context, db);
            if (nerp.TpNerp == TpNerp.SemEmpenho) return new NerpWorkFlowSemEmpenho(nerp, context, db);

            throw new NotImplementedException("Workflow não implementado");
        }

        #endregion
    }
}