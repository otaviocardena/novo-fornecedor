﻿using NBox.Services;
using NBox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using NPoco;
using Creasp.Core;
using NPoco.Linq;
using Creasp.Siplen;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Text;

namespace Creasp.Nerp
{
    public partial class NerpService : DataService
    {
        /// <summary>
        /// Lista os eventos que vão emitir uma nerp
        /// </summary>
        public QueryPage<Evento> ListaEvento(Pager p, DateTime? dtEvento, string noEvento, bool adiantamento)
        {
            if (dtEvento != null)
            {
                if (adiantamento  && dtEvento < DateTime.Today) throw new NBoxException("Para adiantamento, a data do evento deve ser anterior ou igual a hoje");
                if (!adiantamento && dtEvento > DateTime.Today) throw new NBoxException("Para ressarcimento, a data do evento deve ser igual ou posterior a hoje");
            }

            return db.Query<Evento>()
                .Where(dtEvento != null, x => dtEvento.Value < x.DtEventoFim && dtEvento.Value.AddDays(1) > x.DtEventoFim)
                .Where(adiantamento, x => x.DtEventoFim >= DateTime.Now)
                .Where(!adiantamento, x => x.DtEventoFim <= DateTime.Now)
                .Where(noEvento != null, x => x.NoEvento.StartsWith(noEvento.ToLike()))
                .Where(x => x.NrAno == nrAnoBase)
                .ToPage(p.DefaultSort("DtEvento" + (adiantamento ? " ASC" : " DESC")));
        }

        /// <summary>
        /// Lista os participantes de um evento. Se evento for no futuro, é adiantamento
        /// </summary>
        public List<NerpParticipanteDTO> ListaParticipantesNerp(int idEvento, StringBuilder erros)
        {
            var eventos = new EventoService(context);
            var diretoriasrv = new DiretoriaService(context);
            var despesasrv = new DespesaService(context);

            var participantes = new List<NerpParticipanteDTO>();

            var evento = eventos.BuscaEvento(idEvento, true);
            var adiantamento = evento.DtEvento > DateTime.Now;
            var presidenteExercicio = diretoriasrv.BuscaPresidenteExercicio(evento.DtEvento);

            foreach (var part in evento.Participantes)
            {
                Pessoa pessoa = null;

                #region Valida quem deve pago: Titular ou Suplente?

                if(adiantamento)
                {
                    if (part.IdPessoaTit == presidenteExercicio?.Pessoa?.IdPessoa)
                    {
                        pessoa = part.Titular;
                    }
                    else
                    {
                        // se já sabemos que o titular não estará presente, assume o suplente
                        if (part.TpPresencaTit == TpPresenca.Licenciado)
                        {
                            // se não tiver suplente ou ele estiver licenciado também, pula pro proximo (nao paga ninguem)
                            if (part.Suplente == null || part.TpPresencaSup == TpPresenca.Licenciado) continue;

                            pessoa = part.Suplente;
                        }
                        else
                        {
                            // neste caso, o adiantamento deve ser apenas do titular
                            pessoa = part.Titular;
                        }
                    }
                }
                else
                {
                    // se o titular esteve presente, deve ser ele a ser pago
                    if (part.TpPresencaTit == TpPresenca.Presente)
                    {
                        pessoa = part.Titular;
                    }
                    // se o titular não estava, verifica se o suplente estava (e se tem suplente para isso)
                    else if(part.Suplente != null && part.TpPresencaSup == TpPresenca.Presente)
                    {
                        pessoa = part.Suplente;
                    }
                    // se nenhum deles estava, não paga ninguem (ou não foi feita a lista de presença)
                    else
                    {
                        continue;
                    }
                }

                #endregion

                // cria um novo participante
                var p = new NerpParticipanteDTO
                {
                    IdParticipante = part.IdParticipante,
                    IdCentroCusto = part.IdCentroCusto,
                    NoCentroCusto = part.CentroCusto.NoCentroCustoFmt,
                    IdPessoa = pessoa.IdPessoa,
                    NoPessoa = pessoa.NoPessoa
                };

                try
                {
                    p.Despesas.AddRange(despesasrv.CalculaKmDiaria(pessoa, evento));
                }
                catch (Exception ex)
                {
                    erros.AppendLine(pessoa.NoPessoa + " - " + ex.Message);
                }

                participantes.Add(p);
            }

            return participantes.OrderBy(x => x.NoCentroCusto).ThenBy(x => x.NoPessoa).ToList();
        }

        /// <summary>
        /// Preenche uma NERP com os dados de um evento
        /// </summary>
        public int IncluiNerpEvento(int idEvento, string noNerp, string noJustificativa, int? idChefe, int? idSuperintendente, List<NerpParticipanteDTO> participantes)
        {
            ValidaLimiteDtEmissao(nrAnoBase);

            using (var trans = db.GetTransaction())
            {
                var eventos = new EventoService(context);
                var pessoas = new PessoaService(context);
                var despesas = new DespesaService(context);
                var funcionarios = new FuncionarioService(context);

                // busca o evento
                var evento = eventos.BuscaEvento(idEvento, false);

                // busca o funcionario com a hierarquia dele (baseado no login do usuario)
                var func = funcionarios.BuscaFuncionarioHierarquia(user.Login);
                var chefe = func.Chefe;
                var superintendente = func.Superintendente;

                // se tiver chefe/superintender por parametro, usa eles
                if (idChefe != null)
                {
                    chefe = funcionarios.BuscaFuncionario(db.SingleById<Pessoa>(idChefe).NrMatricula, null);
                }
                if (idSuperintendente != null)
                {
                    superintendente = funcionarios.BuscaFuncionario(db.SingleById<Pessoa>(idSuperintendente).NrMatricula, null);
                }

                // não permite que o chefe superior seja o mesmo que solicitou
                if (func.Pessoa.IdPessoa == chefe.Pessoa.IdPessoa) throw new NBoxException("Não é permitido que o autorizador da NERP seja a mesma pessoa que solicita a NERP");

                // cria a nerp com os dados básicos
                var nerp = new Nerp
                {
                    DtEmissao = Nerp.DataEmissao(nrAnoBase),
                    TpNerp = TpNerp.Evento,
                    NrNerp = db.ExecuteScalar<int>("SELECT ISNULL(MAX(NrNerp), 0) + 1 FROM Nerp WHERE NrAno = @0", nrAnoBase),
                    NrAno = nrAnoBase,
                    IdEvento = idEvento,
                    NoNerp = noNerp,
                    NoJustificativa = noJustificativa,
                    TpSituacao = NerpSituacao.Cadastrada,
                    IdPessoaResp = func.Pessoa.IdPessoa,
                    FgAdiantamento = (evento.DtEventoFim > DateTime.Now), // o evento ainda não finalizou, então é adiantamento
                    VrRetencoes = 0
                };

                // se a nerp for de adiantamento, o final do evento deve estar a pelo menos 7 dias uteis
                if (nerp.FgAdiantamento)
                {
                    //var maxData = Periodo.AddWorkDays(evento.DtEventoFim, -ConfigurationManager.AppSettings["nerp.adiantamento.dias"].To<int>(3));
                    //if (DateTime.Now > maxData)  throw new NBoxException("A data limite para adiantamento desde evento é {0:d}", maxData);
                }

                db.Insert(nerp);

                // aponta as pessoas que vão assinar a nerp
                db.Insert(new NerpAssinatura
                {
                    IdNerp = nerp.IdNerp,
                    TpAprovacao = TpAprovacao.Solicitante,
                    IdPessoa = func.Pessoa.IdPessoa,
                    CdUnidade = func.Unidade.CdUnidade
                });

                db.Insert(new NerpAssinatura
                {
                    IdNerp = nerp.IdNerp,
                    TpAprovacao = TpAprovacao.Gestor,
                    IdPessoa = chefe.Pessoa.IdPessoa,
                    CdUnidade = chefe.Unidade.CdUnidade
                });

                db.Insert(new NerpAssinatura
                {
                    IdNerp = nerp.IdNerp,
                    TpAprovacao = TpAprovacao.Superintendente,
                    IdPessoa = superintendente.Pessoa.IdPessoa,
                    CdUnidade = superintendente.Unidade.CdUnidade
                });

                // adiciona os itens a nerp
                foreach (var participante in participantes)
                {
                    // varre todas as despesas
                    foreach (var despesa in participante.Despesas)
                    {
                        // se a despesa for zero, não adiciona
                        if (despesa.VrTotal == 0) continue;

                        var item = new NerpItem
                        {
                            IdNerp = nerp.IdNerp,
                            IdPessoaCredor = participante.IdPessoa,
                            IdParticipante = participante.IdParticipante,
                            IdCentroCusto = participante.IdCentroCusto,
                            IdContaContabil = despesas.BuscaTipoDespesa(despesa.CdTipoDespesa).ContaContabil.IdContaContabil,
                            CdTipoDespesa = despesa.CdTipoDespesa,
                            QtItem = despesa.QtUnit,
                            VrUnit = despesa.VrUnit,
                            VrTotal = despesa.VrTotal
                        };

                        // valida se a pessoa está autorizando não está na lista
                        if (item.IdPessoaCredor == chefe.Pessoa.IdPessoa || item.IdPessoaCredor == superintendente.Pessoa.IdPessoa)
                        {
                            throw new NBoxException("Os autorizadores da NERP não podem estar entre os credores da NERP");
                        }

                        nerp.Items.Add(item);
                        db.Insert(item);
                    }
                }

                // atualiza valor total
                nerp.VrBruto = nerp.VrLiquido = nerp.Items.Sum(x => x.VrTotal);

                db.Update(nerp);

                // não emite nerp com valor zerado
                if (nerp.VrBruto == 0) throw new NBoxException("Não é permitido emitir NERP com valor zerado.");

                // antes de comitar, verifica se a NERP montada tem saldo suficiente
                this.ValidaSaldoOrcamentario(nerp.IdNerp);

                trans.Complete();

                return nerp.IdNerp;
            }
        }
    }
}