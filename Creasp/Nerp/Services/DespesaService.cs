﻿using NBox.Services;
using NBox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using NPoco;
using Creasp.Core;
using NPoco.Linq;
using Creasp.Siplen;

namespace Creasp.Nerp
{
    public class DespesaService : DataService
    {
        protected readonly int nrAnoBase;

        public DespesaService(ServiceContext context) : base(context)
        {
            nrAnoBase = (context as CreaspContext).NrAnoBase;
        }

        /// <summary>
        /// Lista os tipos de despesas disponiveis
        /// </summary>
        public IEnumerable<TipoDespesa> ListaTipoDespesa(string noTipoDespesa = null, bool? ativo = true)
        {
            return db.Query<TipoDespesa>()
                .Include(x => x.ContaContabil)
                .IncludeMany(x => x.Valores)
                .Where(x => x.ContaContabil.NrAno == nrAnoBase)
                .Where(noTipoDespesa != null, x => x.NoTipoDespesa.StartsWith(noTipoDespesa.ToLike()))
                .Where(ativo != null, x => x.FgAtivo == ativo)
                .OrderBy(x => x.NoTipoDespesa)
                .ToList();
        }

        /// <summary>
        /// Calcula os Km e Diarias que uma pessoa tem a receber (por padrão). Utiliza o campo info para informações de como foi calculado
        /// </summary>
        public IEnumerable<NerpDespesaDTO> CalculaKmDiaria(Pessoa pessoa, Evento evento)
        {
            // se for evento sem CEP, retorna cai fora
            if (evento.Cep == null) yield break;

            // verifica o tipo de pessoa na data do evento
            var tpPessoa = new PessoaService(context).BuscaTipoPessoa(pessoa, evento.DtEvento);

            // o cep de origem é o CEP da pessoa
            var cepOrigem = pessoa.Cep;

            // se o tpPessoa for Funcionario, o CEP de origem é da UNIDADE dele (e não do domicilio)
            if (tpPessoa == TpPessoa.Funcionario)
            {
                var unidade = new FuncionarioService(context).BuscaUnidade(pessoa);

                cepOrigem = unidade.Cep ?? pessoa.Cep;
            }

            // km entre a origem e o destino APENAS calculo de DIARIA
            var kmOrigem = new CepService(context).CalculaRota(cepOrigem, evento.Cep, true);

            // verifica se já existe um evento hoje
            var kmHoje = BuscaDespesaDia(evento.DtEvento, pessoa.IdPessoa, "KM-");

            // se tiver evento no dia atual, muda o cep de origem para o CEP do evento
            if (kmHoje != null)
            {
                cepOrigem = kmHoje.Nerp.Evento.Cep;
            }
            else
            {
                // verifica se existe um evento do dia anterior ou no dia posterior
                var kmOntem = BuscaDespesaDia(evento.DtEvento.Date.AddDays(-1), pessoa.IdPessoa, "KM-");
                var kmAmanha = BuscaDespesaDia(evento.DtEvento.Date.AddDays(+1), pessoa.IdPessoa, "KM-");

                // se a distancia de ontem for maior que 250km, substitui a origem
                if (kmOntem != null && kmOntem.QtItem >= 250)
                {
                    cepOrigem = kmOntem.Nerp.Evento.Cep;
                }
                // caso não encontre no dia anterior mas encontre no dia posterior, substitui o destino
                else if (kmAmanha != null && kmAmanha.QtItem >= 250)
                {
                    cepOrigem = kmAmanha.Nerp.Evento.Cep;
                }
            }

            // calcula as distancias entre o endereço da pessoa (ou do ultimo evento que ela foi) e o evento
            var rota = new CepService(context).CalculaRota(cepOrigem, evento.Cep, true);

            // busca o tipo de despesa KM por tipo de pessoa
            var km = BuscaDespesaKm(tpPessoa);

            // retorna a despesa de KM (se for maior que zero)
            if (rota.QtKmTotal > 0)
            {
                yield return new NerpDespesaDTO { CdTipoDespesa = km.CdTipoDespesa, NoTipoDespesa = km.NoTipoDespesa, QtUnit = rota.QtKmTotal, VrUnit = km.VrUnit(evento.DtEvento), NoInfo = rota.NoRota };
            }

            // verifica se tem direito a diaria
            var qtDiaria = 0m;

            // REGRAS DE QUANTIDADE DE DIARIAS: 
            // Para funcionários:
            // >= 130km = 1 diaria
            // < 130 fora do horario comercial = meia diaria
            // < 130 dentro do horario comercial = 0
            // Conselheiros e demais:
            // 1 diária (mas varia o tipo de diaria 1 [até 250km], 2 [acima de 250km] ou 3 [fora de SP])

            if (tpPessoa == TpPessoa.Funcionario)
            {
                qtDiaria = 
                    rota.QtKmTotal > 130 ? 1m :
                    evento.IsForaHorarioComercial ? .5m : 0m;
            }
            else
            {
                // calcula conforme a quantidade de dias
                qtDiaria = Convert.ToInt32(evento.DtEventoFim.Date.Subtract(evento.DtEvento.Date).TotalDays) + 1;
            }

            string diariaInfo = null;

            // verifica se existe já uma diaria paga no dia de hoje
            var diariaHoje = BuscaDespesaDia(evento.DtEvento, pessoa.IdPessoa, "DIARIA-");

            if (diariaHoje != null)
            {
                diariaInfo = "Já existe um evento neste dia [NERP: " + diariaHoje.Nerp.NrNerp + " - " + diariaHoje.Nerp.NoNerp + "]";
                qtDiaria = 0;
            }

            // se tiver direito a diaria, verifica qual das diarias
            var diaria = BuscaDespesaDiaria(tpPessoa, evento.Cep.CdUf, kmOrigem.QtKmTotal);

            if(qtDiaria > 0)
            {
                yield return new NerpDespesaDTO { CdTipoDespesa = diaria.CdTipoDespesa, NoTipoDespesa = diaria.NoTipoDespesa, QtUnit = qtDiaria, VrUnit = diaria.VrUnit(evento.DtEvento), NoInfo = diariaInfo ?? diaria.NoTipoDespesa };
            }
        }

        /// <summary>
        /// Busca em uma data se existe NERP de despesa de uma pessoa
        /// </summary>
        private NerpItem BuscaDespesaDia(DateTime data, int idPessoa, string cdTipoDespesa)
        {
            // verifica se já não tem nerp dentro do mesmo dia
            var inicio = data.Date;
            var fim = inicio.Date.AddDays(1).AddSeconds(-1);

            // guarda em memoria todas as nerp-item do dia em cache e faz busca em memoria
            var nerpItems = cache.Get("nerp-item-" + data.Ticks, 5, () =>
            {
                return db.Query<NerpItem>()
                    .Include(x => x.Nerp)
                    .Include(x => x.Nerp.Evento)
                    .Include(x => x.Nerp.Evento.Cep, JoinType.Left)
                    .Include(x => x.TipoDespesa)
                    .Where(x => x.Nerp.TpNerp == TpNerp.Evento)
                    .Where(x => x.Nerp.TpSituacao != NerpSituacao.Cancelada)
                    .Where(x => x.Nerp.Evento.DtEventoFim >= inicio && x.Nerp.Evento.DtEventoFim <= fim)
                    .ToList();
            });

            return nerpItems.FirstOrDefault(x => x.IdPessoaCredor == idPessoa && x.CdTipoDespesa.StartsWith(cdTipoDespesa));
        }


        /// <summary>
        /// Busca o tipo de despesa de KM conforme o tipo de pessoa
        /// </summary>
        private TipoDespesa BuscaDespesaKm(TpPessoa tpPessoa)
        {
            var cdTipoDespesa =
                tpPessoa == TpPessoa.Funcionario ? "KM-FUNC" :
                tpPessoa == TpPessoa.Conselheiro ? "KM-CONS" :
                tpPessoa == TpPessoa.Inspetor ? "KM-INSP" :
                tpPessoa == TpPessoa.Outros ? "KM-COLAB" : "-";

            return BuscaTipoDespesa(cdTipoDespesa);
        }

        /// <summary>
        /// Busca o tipo de despesa de DIARIA conforme o tipo de pessoa
        /// </summary>
        private TipoDespesa BuscaDespesaDiaria(TpPessoa tpPessoa, string ufDestino, int kmTotal)
        {
            var cdTipoDespesa = "DIARIA-FUNC";

            if (tpPessoa != TpPessoa.Funcionario)
            {
                cdTipoDespesa =
                    tpPessoa == TpPessoa.Conselheiro ? "DIARIA-CONS-" :
                    tpPessoa == TpPessoa.Inspetor ? "DIARIA-INSP-" :
                    tpPessoa == TpPessoa.Outros ? "DIARIA-COLAB-" : "-";

                // Se fora de SP       = Diaria 3
                // Se em SP e >= 250km = Diaria 2
                // Se em SP e  < 250km = Diaria 1
                cdTipoDespesa += 
                    ufDestino != "SP" ? "3" :
                    kmTotal >= 250 ? "2" : "1";
            }

            return BuscaTipoDespesa(cdTipoDespesa);
        }

        /// <summary>
        /// Busca o tipo de despesa mantendo em cache por 1 minuto
        /// </summary>
        public TipoDespesa BuscaTipoDespesa(string cdTipoDespesa)
        {
            return cache.Get("despesa-" + cdTipoDespesa + "-" + nrAnoBase, () =>
            {
                return db.Query<TipoDespesa>()
                    .Include(x => x.ContaContabil)
                    .IncludeMany(x => x.Valores)
                    .Where(x => x.ContaContabil.NrAno == nrAnoBase)
                    .Where(x => x.CdTipoDespesa == cdTipoDespesa)
                    .Single();
            });
        }

        /// <summary>
        /// Salva o tipo de despesa no banco
        /// </summary>
        public string SalvaTipoDespesa(TipoDespesa despesa)
        {
            if (despesa.CdTipoDespesa == null)
            {
                despesa.CdTipoDespesa = Guid.NewGuid().ToString("n").ToLower().Substring(0, 6);
                db.Insert(despesa);
            }
            else
            {
                // TipoDespesa tem PK como string que não aceita na auditoria
                log.Log("Alterou TipoDespesa: ID: " + despesa.CdTipoDespesa);
                db.AuditUpdate(despesa, typeof(TipoDespesa), 0);
            }

            return despesa.CdTipoDespesa;
        }

        /// <summary>
        /// Exclui um tipo de despesa
        /// </summary>
        public void ExcluiTipoDespesa(string cdTipoDespesa)
        {
            var despesa = db.SingleById<TipoDespesa>(cdTipoDespesa);

            if (despesa.FgInterno) throw new NBoxException("Não é possivel excluir esta despesa pois está marcada como interna");

            var items = db.Query<NerpItem>()
                .Where(x => x.CdTipoDespesa == cdTipoDespesa)
                .Count();

            if (items > 0) throw new NBoxException("Este despesa já está associada a NERPs. Utilize a opção de desativar");
                 
            db.Delete(despesa);
        }

        public List<TipoDespesaValor> ListaValores(string cdTipoDespesa)
        {
            return db.Query<TipoDespesaValor>()
                .Where(x => x.CdTipoDespesa == cdTipoDespesa)
                .OrderBy(x => x.DtIni)
                .ToList();
        }

        /// <summary>
        /// Salva uma lista de valores de tipo de despesa, limpando no banco valores anteriores
        /// </summary>
        public void SalvaValores(List<TipoDespesaValor> valores)
        {
            using (var trans = db.GetTransaction())
            {
                db.DeleteMany<TipoDespesaValor>()
                    .Where(x => x.CdTipoDespesa == valores.First().CdTipoDespesa)
                    .Execute();

                var inc = valores.Where(x => x.GetPeriodo().IsVazio == false).ToList();

                if (inc.Count == 0) throw new NBoxException("Os valores da despesa não podem ficar vazios");

                foreach (var valor in inc)
                {
                    db.Insert(valor);
                }

                trans.Complete();
            }
        }
    }
}