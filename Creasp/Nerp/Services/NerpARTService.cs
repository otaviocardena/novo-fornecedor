﻿using NBox.Services;
using NBox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using NPoco;
using Creasp.Core;
using NPoco.Linq;
using Creasp.Siplen;
using System.Configuration;
using System.Text.RegularExpressions;
using System.Text;

namespace Creasp.Nerp
{
    public partial class NerpService : DataService
    {
        /// <summary>
        /// Lista todas os convenios de ARTs de um determinado mes vindo do CREANET
        /// </summary>
        public List<ConvenioArtDTO> ListaConvenioART(int ano, int mes, StringBuilder erros)
        {
            var convenios = Factory.Get<ICreanet>().BuscaValorConvenios(ano, mes);
            var pessoas = new PessoaService(context);
            var funcionarios = new FuncionarioService(context);
            var result = new List<ConvenioArtDTO>();

            foreach (var convenio in convenios)
            {
                var entidade = db.Query<Entidade>()
                    .Include(x => x.Unidade)
                    .Include(x => x.Unidade.CentroCusto)
                    .Where(x => x.TpEntidade == TpEntidade.EntidadeClasse)
                    .Where(x => x.CdExterno == convenio.CodEntidade.ToString())
                    .Where(x => x.Unidade.CentroCusto.NrAno == nrAnoBase)
                    .Where(x => x.FgAtivo)
                    .FirstOrDefault();

                if (entidade == null) continue;

                try
                {
                    if (Pessoa.ValidaCpfZerado(convenio.CpfGestor) == false) throw new NBoxException("Gestor com CPF inválido: " + convenio.CpfGestor);

                    var gestor = pessoas.BuscaOuIncluiPessoa(convenio.CpfGestor);
                    var unidadeGestor = funcionarios.BuscaUnidade(gestor);
                    var entpessoa = pessoas.BuscaOuIncluiPessoa(entidade.NrCnpj);

                    result.Add(new ConvenioArtDTO
                    {
                        IdEntidade = entidade.IdEntidade,
                        IdPessoaEntidade = entpessoa.IdPessoa,
                        NoEntidade = entidade.NoEntidade,

                        IdPessoaGestor = gestor?.IdPessoa ?? 0,
                        NoGestor = gestor?.NoPessoa ?? "GESTOR NÃO INFORMADO",
                        CdUnidade = unidadeGestor.CdUnidade,

                        QtIndicados = convenio.QtIndicados,
                        VrIndicado = convenio.ValorIndicado,
                        VrRateio = convenio.ValorRateio,
                        VrTotal = convenio.ValorTotal
                    });
                }
                catch(Exception ex)
                {
                    erros.AppendLine(entidade.NoEntidade + " - " + ex.Message);
                }
            }

            return result.OrderBy(x => x.NoGestor).ThenBy(x => x.NoEntidade).ToList();
        }

        /// <summary>
        /// Inclui multiplas NERPs de ART baseadas no CPF do gestor
        /// </summary>
        public void IncluiNerpART(IEnumerable<ConvenioArtDTO> convenios, int ano, int mes, string noJustificativa, DateTime dtVencto)
        {
            var grupos = convenios.GroupBy(x => x.IdPessoaGestor);
            var mesano = string.Format("{0:00}/{1:0000}", mes, ano);
            var pessoas = new PessoaService(context);
            var funcionarios = new FuncionarioService(context);
            var func = funcionarios.BuscaFuncionarioHierarquia(user.Login);

            var centrocusto = new CentroCustoService(context)
                .BuscaCentroCusto(ConfigurationManager.AppSettings["nerp.art.centrousto"].UnMask());

            var contacontabil = db.Query<ContaContabil>()
                .Where(x => x.NrContaContabil == ConfigurationManager.AppSettings["nerp.art.contacontabil"].UnMask())
                .Where(x => x.NrAno == nrAnoBase)
                .Where(x => x.FgAtivo)
                .SingleOrDefault();

            using (var trans = db.GetTransaction())
            {
                foreach (var grupo in grupos)
                {
                    // cria a nerp com os dados básicos
                    var nerp = new Nerp
                    {
                        DtEmissao = Nerp.DataEmissao(nrAnoBase),
                        TpNerp = TpNerp.ART,
                        NrNerp = db.ExecuteScalar<int>("SELECT ISNULL(MAX(NrNerp), 0) + 1 FROM Nerp WHERE NrAno = @0", nrAnoBase),
                        NrAno = nrAnoBase,
                        NoNerp = "CONVENIO ART - " + mesano + " - " + grupo.First().NoGestor,
                        NoJustificativa = noJustificativa,
                        IdPessoaResp = func.Pessoa.IdPessoa,
                        FgAdiantamento = false,
                        DtVencto = dtVencto,
                        NrMesRef = mes,
                        TpSituacao = NerpSituacao.Cadastrada
                    };

                    db.Insert(nerp);

                    // aponta as pessoas que vão assinar a nerp
                    db.Insert(new NerpAssinatura
                    {
                        IdNerp = nerp.IdNerp,
                        TpAprovacao = TpAprovacao.Solicitante,
                        IdPessoa = func.Pessoa.IdPessoa,
                        CdUnidade = func.Unidade.CdUnidade
                    });

                    db.Insert(new NerpAssinatura
                    {
                        IdNerp = nerp.IdNerp,
                        TpAprovacao = TpAprovacao.Gestor,
                        IdPessoa = grupo.First().IdPessoaGestor,
                        CdUnidade = grupo.First().CdUnidade
                    });

                    db.Insert(new NerpAssinatura
                    {
                        IdNerp = nerp.IdNerp,
                        TpAprovacao = TpAprovacao.Superintendente,
                        IdPessoa = func.Superintendente.Pessoa.IdPessoa,
                        CdUnidade = func.Superintendente.Unidade.CdUnidade
                    });

                    foreach (var convenio in grupo)
                    {
                        var item = new NerpItem
                        {
                            IdNerp = nerp.IdNerp,
                            IdPessoaCredor = convenio.IdPessoaEntidade,
                            IdCentroCusto = centrocusto.IdCentroCusto,
                            IdContaContabil = contacontabil.IdContaContabil,
                            QtItem = 1,
                            VrUnit = convenio.VrIndicado,
                            VrRateio = convenio.VrRateio,
                            VrTotal = convenio.VrTotal
                        };

                        nerp.Items.Add(item);
                        db.Insert(item);
                    }

                    // atualizando o valor total da nerp
                    nerp.VrBruto = nerp.VrLiquido = nerp.Items.Sum(x => x.VrTotal);
                    db.Update(nerp);

                    // verifica se vai existir saldo para emissão da NERP
                    this.ValidaSaldoOrcamentario(nerp.IdNerp);
                }

                trans.Complete();
            }
        }
    }
}