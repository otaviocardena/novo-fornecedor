﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Creasp.Core;
using NBox.Core;
using NBox.Web.Controls;

namespace Creasp.Nerp
{
    /// <summary>
    /// Relatorio DW para NERP
    /// </summary>
    public class DWNerpDataSource : DWDataSource<Nerp>
    {
        public override string Nome => "Nerp";
        public override string Descricao => "Dados de NERP (cada linha representa uma NERP, não aberto por pessoa)";
        public override bool PorPeriodo => false;

        public override void InicializaCampos()
        {
            Add("Ano")
                .Coluna((c) => c.NrAno);

            Add("Numero")
                .Coluna((c) => c.NrNerp);

            Add("Emissão")
                .Coluna((c) => c.DtEmissao);

            Add("Vencimento")
                .Coluna((c) => c.DtVencto);

            Add("Tipo de NERP")
                .Coluna((c) => c.NoTpNerp)
                .Filtro((c, v) => c.TpNerp == v.To<string>())
                .Combo((db, p) => new ListItem[]
                {
                    new ListItem { Text = "Evento", Value = "EVENTO" },
                    new ListItem { Text = "Convênio ART", Value = "ART" },
                    new ListItem { Text = "Cessão de Uso", Value = "CESSAO-USO" },
                    new ListItem { Text = "Contrato", Value = "COM-EMPENHO" },
                    new ListItem { Text = "Pagamentos diversos", Value = "SEM-EMPENHO" }
                });

            Add("Adiantamento?")
                .Coluna((c) => c.FgAdiantamento ? "Sim" : "Não")
                .Filtro((c, v) => c.FgAdiantamento == v.To<bool>())
                .Combo((db, p) => new ListItem[]
                {
                    new ListItem { Text = "Sim", Value = "True" },
                    new ListItem { Text = "Não", Value = "False" }
                });

            Add("Justificativa")
                .Coluna((c) => c.NoJustificativa);

            Add("Descrição")
                .Coluna((c) => c.NoNerp);

            Add("Situação")
                .Coluna((c) => NerpSituacao.NoSituacao(c.TpSituacao));

            Add("Responsável")
                .Coluna((c) => c.NoResponsavel)
                .Filtro((c, v) => c.NoResponsavel.StartsWith(v));

            Add("Solicitante")
                .Coluna((c) => c.Solicitante?.Pessoa.NoPessoa)
                .Filtro((c, v) => c.Solicitante?.Pessoa.NoPessoa.StartsWith(v) ?? true);
            
            Add("Chefe Imediato/Gestor")
                .Coluna((c) => c.Gestor?.Pessoa.NoPessoa)
                .Filtro((c, v) => c.Gestor?.Pessoa.NoPessoa.StartsWith(v) ?? true);
            
            Add("Superintendente")
                .Coluna((c) => c.Superintendente?.Pessoa.NoPessoa)
                .Filtro((c, v) => c.Superintendente?.Pessoa.NoPessoa.StartsWith(v) ?? true);

            Add("Empenhos")
                .Coluna((c) => string.Join("\n", c.Items.Select(z => z.Empenho?.NrEmpenho).Distinct()))
                .Filtro((c, v) => c.Items.Any(z => z.Empenho?.NrEmpenho.To<string>() == v));

            Add("Centro de custo")
                .Coluna((c) => string.Join("\n", c.Items.Select(z => z.CentroCusto.NoCentroCustoFmt).Distinct()))
                .Filtro((c, v) => c.Items.Any(z => z.CentroCusto.NrCentroCusto == v));

            Add("Favorecidos").Desc("Favorecidos (máx 3)")
                .Coluna((c) => string.Join("\n", c.Items.Select(z => z.PessoaCredor.NoPessoa).Distinct().Take(3)))
                .Filtro((c, v) => c.Items.Any(z => z.PessoaCredor.NoPessoa.StartsWith(v)));

            Add("Valor Bruto")
                .Coluna((c) => c.VrBruto);

            Add("Retenções")
                .Coluna((c) => c.VrRetencoes);

            Add("Valor Liquido")
                .Coluna((c) => c.VrLiquido);

        }

        public override IEnumerable<Nerp> GetDados(CreaspContext db, Periodo periodo)
        {
            return db.Query<Nerp>()
                .Include(x => x.PessoaResp, NPoco.Linq.JoinType.Left)
                .Include(x => x.UnidadeResp, NPoco.Linq.JoinType.Left)
                .Include(x => x.Evento, NPoco.Linq.JoinType.Left)
                .Include(x => x.Items[0].CentroCusto)
                .Include(x => x.Items[0].ContaContabil)
                .Include(x => x.Items[0].PessoaCredor)
                .Include(x => x.Items[0].Empenho)
                .IncludeMany(x => x.Items)
                .Where(x => x.NrAno == db.NrAnoBase)
                .ToList()
                .ForEach((n, i) =>
                {
                    n.Assinaturas = db.Query<NerpAssinatura>()
                        .Include(x => x.Pessoa, NPoco.Linq.JoinType.Left)
                        .Include(x => x.Unidade, NPoco.Linq.JoinType.Left)
                        .Where(x => x.IdNerp == n.IdNerp)
                        .ToList();
                });
        }
    }
}