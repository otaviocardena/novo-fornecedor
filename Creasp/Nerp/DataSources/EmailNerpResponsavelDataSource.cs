﻿using Creasp.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Nerp
{
    public class EmailNerpResponsavelDataSource : IEmailDataSource
    {
        protected CreaspContext db = new CreaspContext();
        protected Nerp nerp;
        protected Pessoa destinatario;
        protected EmailTemplate template;

        public string CdTemplate => "NERP-RESPONSAVEL";
        public string[] ListaCampos { get; private set; }

        public EmailNerpResponsavelDataSource(int idPessoa, int idNerp)
        {
            nerp = db.Nerps.BuscaNerp(idNerp);
            destinatario = db.Pessoas.BuscaPessoa(idPessoa, false);
            template = db.SingleById<EmailTemplate>(CdTemplate);
        }

        public EmailNerpResponsavelDataSource(int idPessoa) {
            destinatario = db.Pessoas.BuscaPessoa(idPessoa, false);
        }

        public object GetDados(int idPessoa)
        {
            var dados = new
            {
                NoPessoa = destinatario.NoPessoa,
                NrNerp = nerp.NrNerp,
                NrAno = nerp.NrAno,
                DtVencto = nerp.DtVencto,
                NoHistorico = nerp.NoHistorico,
                IdNerp = nerp.IdNerp
            };

            return dados;
        }

        public IEnumerable<Pessoa> GetDestinatarios()
        {
            var pessoas = new List<Pessoa>();
            pessoas.Add(destinatario);
            return pessoas;
        }

        public string GetMensagem()
        {
            return template.NoMensagem;
        }

        public string GetTitulo()
        {
            return template.NoTitulo;
        }
    }
}
