﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Creasp.Core;
using NBox.Core;
using NBox.Web.Controls;
using NPoco.Linq;

namespace Creasp.Nerp
{
    /// <summary>
    /// Relatorio DW para NERP
    /// </summary>
    public class DWNerpRetencaoDataSource : DWDataSource<NerpRetencao>
    {
        public override string Nome => "NerpRetencao";
        public override string Descricao => "Dados de retenção de tributos de NERP (cada linha representa uma retenção feita em NERP)";
        public override bool PorPeriodo => true;

        public override void InicializaCampos()
        {
            Add("Ano")
                .Coluna((c) => c.Item.Nerp.NrAno);

            Add("Numero")
                .Coluna((c) => c.Item.Nerp.NrNerp);

            //Add("Situação")
            //    .Coluna((c) => NerpSituacao.NoSituacao(c.Item.Nerp.TpSituacao));

            //Add("Justificativa")
            //    .Coluna((c) => c.Item.Nerp.NoJustificativa);

            Add("Emissão")
                .Coluna((c) => c.Item.Nerp.DtEmissao);

            //Add("CPF Favorecido")
            //    .Coluna((c) => c.Item.PessoaCredor.NrCpf);

            Add("Favorecido")
                .Coluna((c) => c.Item.PessoaCredor.NoPessoa)
                .Filtro((c, v) => c.Item.PessoaCredor.NoPessoa.StartsWith(v));

            Add("Vencimento")
                .Coluna((c) => c.Item.Nerp.DtVencto);

            //Add("Centro Custo")
            //    .Coluna((c) => c.Item.CentroCusto.NoCentroCusto);

            //Add("Conta contábil")
            //    .Coluna((c) => c.Item.ContaContabil.NoContaContabil);

            //Add("Empenho")
            //    .Coluna((c) => c.Item.Empenho?.VrEmpenho)
            //    .Filtro((c, v) => c.Item.Empenho?.NrEmpenho == v.To<int>());

            Add("Valor despesa")
                .Coluna((c) => c.Item.VrTotal);

            Add("Tributo")
                .Coluna((c) => c.NoTributo);

            Add("Valor tributo")
                .Coluna((c) => c.VrRetencao);

            Add("Percentual retenção")
                .Coluna((c) => c.PcRetencao);

            Add("Valor base")
                .Coluna((c) => c.VrBase);

            Add("Vencimento tributo")
                .Coluna((c) => c.DtVencto);

            Add("Retenção Favorecido")
                .Coluna((c) => c.Favorecido.NoPessoa);
        }

        public override IEnumerable<NerpRetencao> GetDados(CreaspContext db, Periodo periodo)
        {
            return db.Query<NerpRetencao>()
                .Include(x => x.Item)
                .Include(x => x.Item.Empenho, JoinType.Left)
                .Include(x => x.Item.Nerp)
                .Include(x => x.Item.PessoaCredor)
                .Include(x => x.Favorecido)
                .Where(x => x.Item.Nerp.NrAno == db.NrAnoBase)
                .Where(x => x.Item.Nerp.TpNerp == TpNerp.ComEmpenho)
                .Where(x => periodo.Inicio <= x.Item.Nerp.DtVencto && periodo.Fim >= x.Item.Nerp.DtVencto)
                .OrderBy(x => x.Item.Nerp.DtEmissao)
                .ThenBy(x => x.Item.Nerp.NrNerp)
                .ToList();
        }
    }
}