﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Creasp.Core;
using NBox.Core;
using NBox.Web.Controls;
using NPoco.Linq;

namespace Creasp.Nerp
{
    /// <summary>
    /// Relatorio DW para NERP
    /// </summary>
    public class DWNerpItemDataSource : DWDataSource<NerpItem>
    {
        public override string Nome => "NerpItem";
        public override string Descricao => "Dados de Itens de NERP (cada linha representa uma despesa da NERP)";
        public override bool PorPeriodo => false;

        public override void InicializaCampos()
        {
            Add("Ano")
                .Coluna((c) => c.Nerp.NrAno);

            Add("Numero")
                .Coluna((c) => c.Nerp.NrNerp);

            Add("Mes Referencia").Desc("Valido apenas para NERPs do tipo ART/Cessão de Uso")
                .Coluna((c) => c.Nerp.NrMesRef)
                .Filtro((c, v) => c.Nerp.NrMesRef == v.To<int>())
                .Combo((db, p) => new ListItem[]
                {
                    new ListItem { Text = "Janeiro", Value = "1" },
                    new ListItem { Text = "Fevereiro", Value = "2" },
                    new ListItem { Text = "Março", Value = "3" },
                    new ListItem { Text = "Abril", Value = "4" },
                    new ListItem { Text = "Maio", Value = "5" },
                    new ListItem { Text = "Junho", Value = "6" },
                    new ListItem { Text = "Julho", Value = "7" },
                    new ListItem { Text = "Agosto", Value = "8" },
                    new ListItem { Text = "Setembro", Value = "9" },
                    new ListItem { Text = "Outubro", Value = "10" },
                    new ListItem { Text = "Novembro", Value = "11" },
                    new ListItem { Text = "Dezembro", Value = "12" }
                });

            Add("Tipo de NERP")
                .Coluna((c) => c.Nerp.NoTpNerp)
                .Filtro((c, v) => c.Nerp.TpNerp == v.To<string>())
                .Combo((db, p) => new ListItem[]
                {
                    new ListItem { Text = "Evento", Value = "EVENTO" },
                    new ListItem { Text = "Convênio ART", Value = "ART" },
                    new ListItem { Text = "Cessão de Uso", Value = "CESSAO-USO" },
                    new ListItem { Text = "Contrato", Value = "COM-EMPENHO" },
                    new ListItem { Text = "Pagamentos diversos", Value = "SEM-EMPENHO" }
                });

            Add("Adiantamento?")
                .Coluna((c) => c.Nerp.FgAdiantamento ? "Sim" : "Não")
                .Filtro((c, v) => c.Nerp.FgAdiantamento == v.To<bool>())
                .Combo((db, p) => new ListItem[]
                {
                    new ListItem { Text = "Sim", Value = "True" },
                    new ListItem { Text = "Não", Value = "False" }
                });

            Add("Justificativa")
                .Coluna((c) => c.Nerp.NoJustificativa);

            Add("Situação")
                .Coluna((c) => NerpSituacao.NoSituacao(c.Nerp.TpSituacao));

            Add("Responsável")
                .Coluna((c) => c.Nerp.NoResponsavel);

            // Add("Solicitante")
            //     .Coluna((c, db) => db.Pessoas.BuscaPessoa(c.Nerp.Solicitante.IdPessoa, false).NoPessoa);
            // 
            // Add("Gestor")
            //     .Coluna((c, db) => db.Pessoas.BuscaPessoa(c.Nerp.Gestor.IdPessoa, false).NoPessoa);
            // 
            // Add("Superintendente")
            //     .Coluna((c, db) => c.Nerp.Superintendente == null ? "" : db.Pessoas.BuscaPessoa(c.Nerp.Superintendente.IdPessoa, false).NoPessoa);

            Add("CPF Favorecido")
                .Coluna((c) => c.PessoaCredor.NrCpf);

            Add("Favorecido")
                .Coluna((c) => c.PessoaCredor.NoPessoa);

            Add("Despesa")
                .Coluna((c) => c.TipoDespesa?.NoTipoDespesa);

            Add("Centro Custo")
                .Coluna((c) => c.CentroCusto.NoCentroCusto);

            Add("Conta contábil")
                .Coluna((c) => c.ContaContabil.NoContaContabil);

            Add("Empenho")
                .Coluna((c) => c.Empenho?.VrEmpenho)
                .Filtro((c, v) => c.Empenho?.NrEmpenho == v.To<int>());

            Add("Data pagamento")
                .Coluna((c) => c.DtPagto);

            Add("Número pagamento")
                .Coluna((c) => c.NrPagto);

            Add("Data adiantamento")
                .Coluna((c) => c.DtAdiantamento);

            Add("Número adiantamento")
                .Coluna((c) => c.NrAdiantamento);

            Add("Valor Unitário")
                .Coluna((c) => c.VrUnit);

            Add("Quantidade")
                .Coluna((c) => c.QtItem);

            Add("Total")
                .Coluna((c) => c.VrTotal);
        }

        public override IEnumerable<NerpItem> GetDados(CreaspContext db, Periodo periodo)
        {
            return db.Query<NerpItem>()
                .Include(x => x.Nerp)
                .Include(x => x.PessoaCredor)
                .Include(x => x.ContaContabil)
                .Include(x => x.CentroCusto)
                .Include(x => x.Empenho, JoinType.Left)
                .Include(x => x.TipoDespesa, JoinType.Left)
                .Where(x => x.Nerp.NrAno == db.NrAnoBase)
                .ToList();
        }
    }
}