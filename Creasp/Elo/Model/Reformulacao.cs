﻿using Creasp.Core;
using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Elo
{
    [PrimaryKey("IdReformulacao", AutoIncrement = true), Serializable]
    public class Reformulacao : IAuditoria
    {
        [Column("IdReformulacao")]
        public int IdReformulacao { get; set; }

        [Column("NoReformulacao")]
        public string NoReformulacao { get; set; }

        [Column("NrAno")]
        public int NrAno { get; set; }

        [Column("PrAplicadoReceita")]
        public decimal? PrAplicadoReceita { get; set; }

        [Column("PrCotaParteConfea")]
        public decimal? PrCotaParteConfea { get; set; }

        [Column("PrCotaParteMutua")]
        public decimal? PrCotaParteMutua { get; set; }

        [Column("NoObservacaoReceitaPadrao")]
        public string NoObservacaoReceitaPadrao { get; set; }

        [Column("DtCadastro")]
        public DateTime DtCadastro { get; set; }

        [Column("DtAlteracao")]
        public DateTime? DtAlteracao { get; set; }

        [Column("LoginCadastro")]
        public string LoginCadastro { get; set; }

        [Column("LoginAlteracao")]
        public string LoginAlteracao { get; set; }


    }

}
