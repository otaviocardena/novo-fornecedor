﻿using Creasp.Core;
using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Elo
{
    [PrimaryKey("IdDisponibilidadeAtualOrcamento"), TableName("DisponibilidadeAtualOrcamento"), Serializable]
    public class DisponibilidadeAtualOrcamento : IFgAtivo, IEqualityComparer<DisponibilidadeAtualOrcamento>
    {
        [Column("IdDisponibilidadeAtualOrcamento")]
        public int IdDisponibilidadeAtualOrcamento { get; set; }

        [Column("NrAno")]
        public int NrAno { get; set; }

        [Column("DtInicio")]
        public DateTime DtInicio { get; set; }

        [Column("DtFim")]
        public DateTime DtFim { get; set; }

        [Column("NrContaContabil6")]
        public string NrContaContabil6 { get; set; }

        [Column("NrContaContabil5")]
        public string NrContaContabil5 { get; set; }

        [Column("NoContaContabil")]
        public string NoContaContabil { get; set; }

        [Column("VrArrecadadoExercicio")]
        public decimal VrArrecadadoExercicio { get; set; }

        [Column("VrArrecadadoPeriodo")]
        public decimal VrArrecadadoPeriodo { get; set; }

        [Column("VrOrcado")]
        public decimal VrOrcado { get; set; }

        [Column("VrDiferenca")]
        public decimal VrDiferenca { get; set; }

        [Column("FgAtivo")]
        public bool FgAtivo { get; set; }

        public bool Equals(DisponibilidadeAtualOrcamento x, DisponibilidadeAtualOrcamento y)
        {
            var igualdade = x.IdDisponibilidadeAtualOrcamento == y.IdDisponibilidadeAtualOrcamento &&
                x.NrAno == y.NrAno &&
                x.NrContaContabil5 == y.NrContaContabil5 &&
                x.NrContaContabil6 == y.NrContaContabil6 &&
                ((string.IsNullOrEmpty(x.NoContaContabil) && string.IsNullOrEmpty(y.NoContaContabil)) || x.NoContaContabil == y.NoContaContabil) &&
                x.VrArrecadadoExercicio == y.VrArrecadadoExercicio &&
                x.VrArrecadadoPeriodo == y.VrArrecadadoPeriodo &&
                x.VrOrcado == y.VrOrcado &&
                x.VrDiferenca == y.VrDiferenca &&
                x.FgAtivo == y.FgAtivo;

            return igualdade;
        }

        public int GetHashCode(DisponibilidadeAtualOrcamento obj)
        {
            return obj.NrAno.GetHashCode() + obj.NrContaContabil5.GetHashCode();
        }

        public override string ToString()
        {
            return $"{NrContaContabil5} - {NoContaContabil} (Diferença: {VrDiferenca.ToString("n2")} - Ativo: {(FgAtivo ? "S" : "N")})";
        }
    }
}
