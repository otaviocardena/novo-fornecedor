﻿using Creasp.Core;
using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Creasp.Elo
{
    [PrimaryKey("IdPropostaOrcamentariaDespesaItem", AutoIncrement = true), Serializable]
    public class PropostaOrcamentariaDespesaItem : IAuditoria
    {
        [Column("IdPropostaOrcamentariaDespesaItem")]
        public int IdPropostaOrcamentariaDespesaItem { get; set; }

        [Column("IdPropostaOrcamentariaDespesa")]
        public int IdPropostaOrcamentariaDespesa { get; set; }

        [Column("IdContaContabil")]
        public int IdContaContabil { get; set; }

        [Column("NrVersao")]
        public int NrVersao { get; set; }

        [Column("NrProcessoL")]
        public string NrProcessoL { get; set; }

        [Column("NoEspecificacao")]
        public string NoEspecificacao { get; set; }

        [Column("NoJustificativa")]
        public string NoJustificativa { get; set; }

        [Column("VrAprovadoAnoAnterior")]
        public decimal? VrAprovadoAnoAnterior { get; set; }

        [Column("VrExecutadoAnoAnterior")]
        public decimal? VrExecutadoAnoAnterior { get; set; }

        [Column("DtRefVrExecutado")]
        public DateTime? DtRefVrExecutado { get; set; }

        [Column("VrOrcado")]
        public decimal? VrOrcado { get; set; }

        [Column("VrOrcadoAntesFinalizar")]
        public decimal? VrOrcadoAntesFinalizar { get; set; }

        [Column("FgVrOrcadoOrigemSenior")]
        public bool? FgVrOrcadoOrigemSenior { get; set; }

        [Ignore]
        public bool FgEdicaoDesabilitada => FgVrOrcadoOrigemSenior == true && PropostaOrcamentariaDespesa.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.Contab;

        #region IAuditoria
        [Column("DtCadastro")]
        public DateTime DtCadastro { get; set; }

        [Column("DtAlteracao")]
        public DateTime? DtAlteracao { get; set; }

        [Column("LoginCadastro")]
        public string LoginCadastro { get; set; }

        [Column("LoginAlteracao")]
        public string LoginAlteracao { get; set; }
        #endregion

        //[Ignore]
        //public decimal? VrOrcado
        //{
        //    get
        //    {
        //        return VrOrcadoContabilidade.HasValue ? VrOrcadoContabilidade :
        //                VrOrcadoAprovador2.HasValue ? VrOrcadoAprovador2 :
        //               VrOrcadoAprovador1.HasValue ? VrOrcadoAprovador1 :
        //               VrOrcadoExecutor;
        //    }
        //    set
        //    {
        //        switch (PropostaOrcamentariaDespesa?.CdStatusPropostaOrcamentaria)
        //        {
        //            case StPropostaOrcamentaria.Novo:
        //            case StPropostaOrcamentaria.Executor:
        //                this.VrOrcadoExecutor = value;
        //                break;
        //            case StPropostaOrcamentaria.Aprovador1:
        //                this.VrOrcadoAprovador1 = value;
        //                break;
        //            case StPropostaOrcamentaria.Aprovador2:
        //                this.VrOrcadoAprovador2 = value;
        //                break;
        //            case StPropostaOrcamentaria.Contab:
        //                this.VrOrcadoContabilidade = value;
        //                break;
        //            default:
        //                this.VrOrcadoExecutor = value;
        //                break;
        //        }
        //    }
        //}

        [Ignore]
        public string VrHistorico
        {
            get
            {
                string historico = string.Empty;

                if (PropostaOrcamentariaHistoricoItens != null)
                {
                    foreach (var hist in PropostaOrcamentariaHistoricoItens)
                    {
                        //TODO: Como forçar a quebra de linha? \r\n não funcionou, nem "<br>".
                        historico += string.Format("{0}: {1}\r\n", hist.CdStatusPropostaOrcamentaria, hist.VrOrcado.ToString("n2"));
                    }

                    historico = historico.Trim();
                }

                return historico;
            }
        }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdPropostaOrcamentariaDespesa", ReferenceMemberName = "IdPropostaOrcamentariaDespesa")]
        public PropostaOrcamentariaDespesa PropostaOrcamentariaDespesa { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdContaContabil", ReferenceMemberName = "IdContaContabil")]
        public ContaContabil ContaContabil { get; set; }

        [Reference(ReferenceType.Many, ColumnName = "IdPropostaOrcamentariaDespesaItem", ReferenceMemberName = "IdPropostaOrcamentariaDespesaItem")]
        public List<PropostaOrcamentariaHistorico> PropostaOrcamentariaHistoricoItens { get; set; }

    }

}
