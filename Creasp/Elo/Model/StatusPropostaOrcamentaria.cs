﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Creasp.Elo
{
    [PrimaryKey("CdStatusPropostaOrcamentaria", AutoIncrement = true), Serializable]
    public class StatusPropostaOrcamentaria
    {
        [Column("CdStatusPropostaOrcamentaria")]
        public string CdStatusPropostaOrcamentaria { get; set; }

        [Column("NoDescricao")]
        public string NoDescricao { get; set; }

        [Column("FgAtivo")]
        public bool? FgAtivo { get; set; }

        [Column("NrOrdem")]
        public int NrOrdem { get; set; }
    }
}
