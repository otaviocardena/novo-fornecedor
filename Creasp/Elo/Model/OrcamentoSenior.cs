﻿using Creasp.Core;
using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Elo
{
    [PrimaryKey("IdOrcamentoSenior", AutoIncrement = true), Serializable]
    public class OrcamentoSenior : IFgAtivo, IEqualityComparer<OrcamentoSenior>
    {
        [Column("IdOrcamentoSenior")]
        public int IdOrcamentoSenior { get; set; }

        [Column("NrAno")]
        public int NrAno { get; set; }

        [Column("NrCentroCusto")]
        public string NrCentroCusto { get; set; }

        [Column("CdContaContabilResumido")]
        public int CdContaContabilResumido { get; set; }

        [Column("NrContaContabil6")]
        public string NrContaContabil6 { get; set; }

        [Column("NrContaContabil5")]
        public string NrContaContabil5 { get; set; }

        [Column("VrOrcamento")]
        public decimal VrOrcamento{ get; set; }

        [Column("FgAtivo")]
        public bool FgAtivo { get; set; }

        public bool Equals(OrcamentoSenior x, OrcamentoSenior y)
        {
            return
                x.IdOrcamentoSenior == y.IdOrcamentoSenior &&
                x.NrAno == y.NrAno &&
                x.NrCentroCusto == y.NrCentroCusto &&
                x.CdContaContabilResumido == y.CdContaContabilResumido &&
                x.NrContaContabil6 == y.NrContaContabil6 &&
                x.NrContaContabil5 == y.NrContaContabil5 &&
                x.VrOrcamento == y.VrOrcamento &&
                x.FgAtivo == y.FgAtivo;
        }

        public int GetHashCode(OrcamentoSenior obj)
        {
            return obj.NrAno.GetHashCode() + obj.NrCentroCusto.GetHashCode() + obj.CdContaContabilResumido.GetHashCode();
        }

        public override string ToString()
        {
            return $"Centro: {NrCentroCusto} - Conta:{NrContaContabil5} (Orçamento: {VrOrcamento.ToString("n2")} - Ativo: {(FgAtivo ? "S" : "N")})";
        }
    }
}
