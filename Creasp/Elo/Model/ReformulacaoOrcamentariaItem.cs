﻿using Creasp.Core;
using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Elo
{
    [PrimaryKey("IdReformulacaoOrcamentariaItem", AutoIncrement = true), Serializable]
    public class ReformulacaoOrcamentariaItem : IAuditoria
    {
        [Column("IdReformulacaoOrcamentariaItem")]
        public int IdReformulacaoOrcamentariaItem { get; set; }

        [Column("IdReformulacaoOrcamentaria")]
        public int IdReformulacaoOrcamentaria { get; set; }

        [Column("IdContaContabil")]
        public int IdContaContabil { get; set; }

        [Column("IdPropostaOrcamentariaDespesaItem")]
        public int? IdPropostaOrcamentariaDespesaItem { get; set; }

        [Column("NrProcessoL")]
        public string NrProcessoL { get; set; }

        [Column("NoJustificativa")]
        public string NoJustificativa { get; set; }

        [Column("VrOrcadoInicialmente")]
        public decimal? VrOrcadoInicialmente { get; set; }

        [Column("VrExecutado")]
        public decimal? VrExecutado { get; set; }

        [Column("DtRefVrExecutado")]
        public DateTime? DtRefVrExecutado { get; set; }

        [Column("VrDotacaoAtual")]
        public decimal? VrDotacaoAtual { get; set; }

        [Column("VrSuplementacao")]
        public decimal? VrSuplementacao { get; set; }

        [Column("VrReducao")]
        public decimal? VrReducao { get; set; }

        [Column("VrOrcamentoReformulado")]
        public decimal? VrOrcamentoReformulado => VrDotacaoAtual + VrSuplementacao - VrReducao;

        [Column("DtCadastro")]
        public DateTime DtCadastro { get; set; } = DateTime.Now;

        [Column("DtAlteracao")]
        public DateTime? DtAlteracao { get; set; }

        [Column("LoginCadastro")]
        public string LoginCadastro { get; set; }

        [Column("LoginAlteracao")]
        public string LoginAlteracao { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdReformulacaoOrcamentaria", ReferenceMemberName = "IdReformulacaoOrcamentaria")]
        public ReformulacaoOrcamentaria ReformulacaoOrcamentaria { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdContaContabil", ReferenceMemberName = "IdContaContabil")]
        public ContaContabil ContaContabil { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdPropostaOrcamentariaDespesa", ReferenceMemberName = "IdPropostaOrcamentariaDespesa")]
        public PropostaOrcamentariaDespesaItem PropostaOrcamentariaItem { get; set; }
    }

}
