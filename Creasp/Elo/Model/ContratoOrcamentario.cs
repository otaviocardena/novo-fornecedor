﻿using Creasp.Core;
using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Elo
{
    [PrimaryKey("IdContratoOrcamentario", AutoIncrement = true), Serializable]
    public class ContratoOrcamentario : IFgAtivo, IEqualityComparer<ContratoOrcamentario>
    {
        [Column("IdContratoOrcamentario")]
        public int IdContratoOrcamentario { get; set; }

        [Column("NrCpfCnpjContratado")]
        public string NrCpfCnpjContratado { get; set; }

        [Column("NoRazaoSocialContratado")]
        public string NoRazaoSocialContratado { get; set; }

        [Column("DtVigenciaInicio")]
        public DateTime? DtVigenciaInicio { get; set; }

        [Column("DtVigenciaFim")]
        public DateTime? DtVigenciaFim { get; set; }

        [Column("NrCentroCusto")]
        public string NrCentroCusto { get; set; }

        [Column("NrContaContabil")]
        public string NrContaContabil { get; set; }

        [Column("NrGestorCpf")]
        public string NrGestorCpf { get; set; }

        [Column("NrContrato")]
        public string NrContrato { get; set; }

        [Column("NrAditivo")]
        public string NrAditivo { get; set; }

        [Column("NrProcesso")]
        public string NrProcesso { get; set; }

        [Column("NoObjeto")]
        public string NoObjeto { get; set; }

        [Column("VrIndiceReajuste")]
        public string VrIndiceReajuste { get; set; }

        [Column("VrContrato")]
        public decimal? VrContrato { get; set; }

        [Column("VrAnulado")]
        public decimal? VrAnulado { get; set; }

        [Column("VrEmpenhado")]
        public decimal? VrEmpenhado { get; set; }

        [Column("VrLiquidado")]
        public decimal? VrLiquidado { get; set; }

        [Column("VrPago")]
        public decimal? VrPago { get; set; }

        [Column("FgAtivo")]
        public bool FgAtivo { get; set; }

        public bool Equals(ContratoOrcamentario x, ContratoOrcamentario y)
        {
            return
                x.IdContratoOrcamentario == y.IdContratoOrcamentario;
        }

        public int GetHashCode(ContratoOrcamentario obj)
        {
            return obj.IdContratoOrcamentario.GetHashCode();
        }

    }
}
