﻿namespace Creasp.Elo
{
    public class CdTipoConta
    {
        public const int Analitico = 1;
        public const int Sintetico = 2;
    }

    public class StPropostaOrcamentaria
    {
        public const string Novo = "Novo";
        public const string Executor = "Executor";
        public const string Aprovador1 = "Aprovador1";
        public const string Aprovador2 = "Aprovador2";
        public const string Aprovador3 = "Aprovador3";
        public const string Aprovador4 = "Aprovador4";
        public const string Contab = "Contab";
        public const string ContabValidacao = "ContabValidacao";
        public const string Plenario = "Plenario";
        public const string PlenarioOK = "PlenarioOK";

        public const string Informatica = "Informatica";
        public const string Almoxarifado = "Almoxarifado";
        public const string DAS = "DAS";
    }

    public class CdTpFormulario
    {
        public const string Informatica = "Informatica";
        public const string Almoxarifado = "Almoxarifado";
        public const string DAS = "DAS";
    }

    public class TpPerfilCentroCusto
    {
        public const string Almoxarifado = "ALMOXARIFADO";
        public const string Contabilidade = "CONTABILIDADE";
        public const string DAS = "DAS";
        public const string DCF = "DCF";
        public const string Informatica = "INFORMATICA";
    }
}
