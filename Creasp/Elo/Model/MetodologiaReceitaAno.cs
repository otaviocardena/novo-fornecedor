﻿using Creasp.Core;
using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Elo
{
    [PrimaryKey("IdMetodologiaReceitaAno", AutoIncrement = true), Serializable]
    public class MetodologiaReceitaAno : IAuditoria
    {
        [Column("IdMetodologiaReceitaAno")]
        public int IdMetodologiaReceitaAno { get; set; }

        [Column("IdContaContabil")]
        public int IdContaContabil { get; set; }

        [Column("NrAno")]
        public int NrAno { get; set; }

        [Column("CdStatusPropostaOrcamentaria")]
        public string CdStatusPropostaOrcamentaria { get; set; }

        [Column("IdPessoaResp")]
        public int? IdPessoaResp { get; set; }

        [Column("PrAplicado")]
        public decimal? PrAplicado { get; set; }

        [Column("VrAprovadoAnoAnterior")]
        public decimal? VrAprovadoAnoAnterior { get; set; }

        [Column("VrOrcado")]
        public decimal? VrOrcado { get; set; }

        [Column("PrCotaParteConfea")]
        public decimal? PrCotaParteConfea { get; set; }

        [Column("PrCotaParteMutua")]
        public decimal? PrCotaParteMutua { get; set; }

        [Column("PrSaoPaulo")]
        public decimal? PrSaoPaulo { get; set; }

        [Column("PrBrasilia")]
        public decimal? PrBrasilia { get; set; }

        [Column("NoObservacoes")]
        public string NoObservacoes { get; set; }

        [Column("FgReformulacao")]
        public bool FgReformulacao { get; set; }

        [Column("IdReformulacao")]
        public int? IdReformulacao { get; set; }

        [Column("DtCadastro")]
        public DateTime DtCadastro { get; set; }

        [Column("DtAlteracao")]
        public DateTime? DtAlteracao { get; set; }

        [Column("LoginCadastro")]
        public string LoginCadastro { get; set; }

        [Column("LoginAlteracao")]
        public string LoginAlteracao { get; set; }

        [Ignore]
        public decimal? VrCotaParteConfea => VrOrcado * (PrCotaParteConfea / 100);
        [Ignore]
        public decimal? VrCotaParteMutua => VrOrcado * (PrCotaParteMutua / 100);
        [Ignore]
        public decimal? VrCotaSaoPaulo => VrCotaParteMutua * (PrSaoPaulo / 100);
        [Ignore]
        public decimal? VrCotaBrasilia => VrCotaParteMutua * (PrBrasilia / 100);
        [Ignore]
        public decimal? VrSuplementacao { get; set; }
        [Ignore]
        public decimal? VrReducao { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdPessoaResp", ReferenceMemberName = "IdPessoa")]
        public Pessoa Responsavel { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "CdStatusPropostaOrcamentaria", ReferenceMemberName = "CdStatusPropostaOrcamentaria")]
        public StatusPropostaOrcamentaria StatusPropostaOrcamentaria { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdReformulacao", ReferenceMemberName = "IdReformulacao")]
        public Reformulacao Reformulacao { get; set; }

        [Reference(ReferenceType.Many, ColumnName = "IdMetodologiaReceitaAno", ReferenceMemberName = "IdMetodologiaReceitaAno")]
        public List<MetodologiaReceitaMes> MetodologiasReceitaMes { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdContaContabil", ReferenceMemberName = "IdContaContabil")]
        public ContaContabil ContaContabil { get; set; }

        public void CalculaVrOrcado(decimal? prAplicadoReceita)
        {
            this.VrOrcado = this.VrAprovadoAnoAnterior * (1 + prAplicadoReceita / 100);
        }
    }
}
