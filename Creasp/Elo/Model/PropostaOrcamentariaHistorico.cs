﻿using Creasp.Core;
using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Elo
{
    [PrimaryKey("IdPropostaOrcamentariaHistorico", AutoIncrement = true), Serializable]
    public class PropostaOrcamentariaHistorico : IAuditoria
    {
        [Column("IdPropostaOrcamentariaHistorico")]
        public int IdPropostaOrcamentariaHistorico { get; set; }

        [Column("IdPropostaOrcamentariaDespesaItem")]
        public int IdPropostaOrcamentariaDespesaItem { get; set; }

        [Column("CdStatusPropostaOrcamentaria")]
        public string CdStatusPropostaOrcamentaria { get; set; }

        [Column("VrOrcado")]
        public decimal VrOrcado { get; set; }

        [Column("DtCadastro")]
        public DateTime DtCadastro { get; set; }

        [Column("DtAlteracao")]
        public DateTime? DtAlteracao { get; set; }

        [Column("LoginCadastro")]
        public string LoginCadastro { get; set; }

        [Column("LoginAlteracao")]
        public string LoginAlteracao { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdPropostaOrcamentariaDespesaItem", ReferenceMemberName = "IdPropostaOrcamentariaDespesaItem")]
        public PropostaOrcamentariaDespesaItem PropostaOrcamentariaItem { get; set; }
    }

}
