﻿using Creasp.Core;
using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Elo
{
    [PrimaryKey("IdMetodologiaReceitaMes", AutoIncrement = true), Serializable]
    public class MetodologiaReceitaMes : IAuditoria
    {
        [Column("IdMetodologiaReceitaMes")]
        public int IdMetodologiaReceitaMes { get; set; }

        [Column("IdMetodologiaReceitaAno")]
        public int IdMetodologiaReceitaAno { get; set; }

        [Column("NoMes")]
        public string NoMes { get; set; }

        [Column("PrAplicado")]
        public decimal? PrAplicado { get; set; }

        [Column("VrReceitaBruta")]
        public decimal? VrReceitaBruta { get; set; }

        [Column("VrReceitaLiquida")]
        public decimal? VrReceitaLiquida { get; set; }

        [Column("DtCadastro")]
        public DateTime DtCadastro { get; set; }

        [Column("DtAlteracao")]
        public DateTime? DtAlteracao { get; set; }

        [Column("LoginCadastro")]
        public string LoginCadastro { get; set; }

        [Column("LoginAlteracao")]
        public string LoginAlteracao { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdMetodologiaReceitaAno", ReferenceMemberName = "IdMetodologiaReceitaAno")]
        public MetodologiaReceitaAno MetodologiaReceitaAno { get; set; }

        public void CalculaVrReceitaBruta(decimal? vrOrcadoAnual)
        {

            VrReceitaBruta = vrOrcadoAnual * (PrAplicado / 100);
        }

        public void CalculaVrReceitaLiquida(decimal? vrOrcadoAnual, decimal? prCotaParteConfea, decimal? prCotaParteMutua)
        {
            //Usa a mesma fórmula que receita bruta
            VrReceitaLiquida = vrOrcadoAnual * (PrAplicado / 100);

            //Calcula o % da parte Confea e parte Mutua
            var prADescontar = prCotaParteConfea + prCotaParteMutua;

            //Desconta % da parte confea e mutua
            VrReceitaLiquida = VrReceitaLiquida * (1 - prADescontar / 100);
        }
    }
}
