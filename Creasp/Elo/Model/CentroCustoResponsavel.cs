﻿using Creasp.Core;
using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Elo
{
    [PrimaryKey("IdCentroCustoResponsavel", AutoIncrement = true), Serializable]
    public class CentroCustoResponsavel
    {
        [Column("IdCentroCustoResponsavel")]
        public int IdCentroCustoResponsavel { get; set; }

        [Column("IdCentroCusto")]
        public int IdCentroCusto { get; set; }

        [Column("FgOrcavel")]
        public bool FgOrcavel { get; set; }

        [Column("IdPessoaExecutor")]
        public int? IdPessoaExecutor { get; set; }

        [Column("IdPessoaAprovador1")]
        public int? IdPessoaAprovador1 { get; set; }

        [Column("IdPessoaAprovador2")]
        public int? IdPessoaAprovador2 { get; set; }

        [Column("IdPessoaAprovador3")]
        public int? IdPessoaAprovador3 { get; set; }

        [Column("IdPessoaAprovador4")]
        public int? IdPessoaAprovador4 { get; set; }

        [Ignore]
        public string NoPerfis => CentroCustoResponsaveisPerfis != null ? string.Join(", ", CentroCustoResponsaveisPerfis.Select(x => x.CdCentroCustoPerfil)) : "";

        [Reference(ReferenceType.OneToOne, ColumnName = "IdCentroCusto", ReferenceMemberName = "IdCentroCusto")]
        public CentroCusto CentroCusto { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdPessoaExecutor", ReferenceMemberName = "IdPessoa")]
        public Pessoa ResponsavelExecucao { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdPessoaAprovador1", ReferenceMemberName = "IdPessoa")]
        public Pessoa ResponsavelAprova { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdPessoaAprovador2", ReferenceMemberName = "IdPessoa")]
        public Pessoa ResponsavelAprova2 { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdPessoaAprovador3", ReferenceMemberName = "IdPessoa")]
        public Pessoa ResponsavelAprova3 { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdPessoaAprovador4", ReferenceMemberName = "IdPessoa")]
        public Pessoa ResponsavelAprova4 { get; set; }

        [Reference(ReferenceType.Many, ColumnName = "IdCentroCustoResponsavel", ReferenceMemberName = "IdCentroCustoResponsavel")]
        public List<CentroCustoResponsavelPerfil> CentroCustoResponsaveisPerfis { get; set; }

    }
}
