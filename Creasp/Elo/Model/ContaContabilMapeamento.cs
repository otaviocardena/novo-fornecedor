﻿using Creasp.Core;
using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Elo
{
    [PrimaryKey("IdContaContabil6", AutoIncrement =false), Serializable]
    public class ContaContabilMapeamento : IAuditoria
    {
        [Column("IdContaContabil5")]
        public int? IdContaContabil5 { get; set; }

        [Column("IdContaContabil6")]
        public int IdContaContabil6 { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdContaContabil5", ReferenceMemberName = "IdContaContabil")]
        public ContaContabil ContaContabil5 { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdContaContabil6", ReferenceMemberName = "IdContaContabil")]
        public ContaContabil ContaContabil6 { get; set; }

        [Column("FgMapeadoPeloUsuario")]
        public bool FgMapeadoPeloUsuario { get; set; }

        #region IAuditoria
        public DateTime DtCadastro { get; set; }

        [Column("DtAlteracao")]
        public DateTime? DtAlteracao { get; set; }

        [Column("LoginCadastro")]
        public string LoginCadastro { get; set; }

        [Column("LoginAlteracao")]
        public string LoginAlteracao { get; set; }
        #endregion
    }
}
