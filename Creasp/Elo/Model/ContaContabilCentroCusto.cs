﻿using Creasp.Core;
using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Elo
{
    [TableName("ContaContabil_CentroCusto"), Serializable]
    public class ContaContabil_CentroCusto
    {
        [Column("IdContaContabil")]
        public int IdContaContabil { get; set; }

        [Column("IdCentroCusto")]
        public int IdCentroCusto { get; set; }

        //[Column("NrAno")]
        //public int NrAno { get; set; }

        [Column("FgInseridoPorUsuario")]
        public bool FgInseridoPorUsuario { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdContaContabil", ReferenceMemberName = "IdContaContabil")]
        public ContaContabil ContaContabil { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdCentroCusto", ReferenceMemberName = "IdCentroCusto")]
        public CentroCusto CentroCusto { get; set; }

    }
}
