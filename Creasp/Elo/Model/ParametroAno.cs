﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Elo
{
    [PrimaryKey("NrAno", AutoIncrement = false)]
    public class ParametroAno
    {
        [Column("NrAno")]
        public int NrAno { get; set; }

        [Column("PrAplicadoReceita")]
        public decimal? PrAplicadoReceita { get; set; }

        [Column("PrAplicadoDespesa")]
        public decimal? PrAplicadoDespesa { get; set; }

        [Column("PrCotaParteConfea")]
        public decimal? PrCotaParteConfea { get; set; }

        [Column("PrCotaParteMutua")]
        public decimal? PrCotaParteMutua { get; set; }

        [Column("NoObservacoesReceitaPadrao")]
        public string NoObservacoesReceitaPadrao { get; set; }

        [Column("NoDiretrizes")]
        public string NoDiretrizes { get; set; }

        [Column("DtDisponibilizacao")]
        public DateTime? DtDisponibilizacao { get; set; }
        
        [Ignore]
        public bool FgDisponibilizavel => DtDisponibilizacao == null;
    }
}
