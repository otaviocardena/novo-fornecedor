﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Elo
{
    [PrimaryKey("CdCentroCustoPerfil", AutoIncrement = false)]
    public class CentroCustoPerfil
    {
        [Column("CdCentroCustoPerfil")]
        public string CdCentroCustoPerfil { get; set; }

        [Column("NoCentroCustoPerfil")]
        public string NoCentroCustoPerfil { get; set; }
    }
}
