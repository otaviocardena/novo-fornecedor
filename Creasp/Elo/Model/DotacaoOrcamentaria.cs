﻿using Creasp.Core;
using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Elo
{
    [PrimaryKey("IdDotacaoOrcamentaria", AutoIncrement = true), Serializable]
    public class DotacaoOrcamentaria : IFgAtivo, IEqualityComparer<DotacaoOrcamentaria>
    {
        [Column("IdDotacaoOrcamentaria")]
        public int IdDotacaoOrcamentaria { get; set; }

        [Column("NrAno")]
        public int NrAno { get; set; }

        [Column("NrCentroCusto")]
        public string NrCentroCusto { get; set; }

        [Column("NrContaContabil6")]
        public string NrContaContabil6 { get; set; }

        [Column("NrContaContabil5")]
        public string NrContaContabil5 { get; set; }

        [Column("FgReceita")]
        public bool FgReceita { get; set; }

        [Column("VrAprovado")]
        public decimal? VrAprovado { get; set; }

        [Column("FgAtivo")]
        public bool FgAtivo { get; set; }

        public bool Equals(DotacaoOrcamentaria x, DotacaoOrcamentaria y)
        {
            var igualdade = x.IdDotacaoOrcamentaria == y.IdDotacaoOrcamentaria &&
                x.NrAno == y.NrAno &&
                x.NrCentroCusto == y.NrCentroCusto &&
                x.NrContaContabil5 == y.NrContaContabil5 &&
                x.NrContaContabil6 == y.NrContaContabil6 &&
                x.FgReceita == y.FgReceita &&
                x.VrAprovado == y.VrAprovado &&
                x.FgAtivo == y.FgAtivo;

            return igualdade;
        }

        public int GetHashCode(DotacaoOrcamentaria obj)
        {
            return obj.NrAno.GetHashCode() + obj.NrCentroCusto.GetHashCode() + obj.NrContaContabil5.GetHashCode() + obj.FgReceita.GetHashCode();
        }

        public override string ToString()
        {
            string tipo = FgReceita ? "Receita" : "Despesa";
            string valor = VrAprovado.HasValue ? VrAprovado.Value.ToString("n2") : "0,00";
            return $"Centro: {NrCentroCusto} - Conta: {NrContaContabil5} ({tipo}): {valor} - Ativo: {(FgAtivo ? "S" : "N")})";
        }
    }
}
