﻿using Creasp.Core;
using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Creasp.Elo
{
    [PrimaryKey("IdPropostaOrcamentariaDespesa", AutoIncrement = true), Serializable]
    public class PropostaOrcamentariaDespesa : IAuditoria
    {
        [Column("IdPropostaOrcamentariaDespesa")]
        public int IdPropostaOrcamentariaDespesa { get; set; }

        [Column("IdCentroCusto")]
        public int IdCentroCusto { get; set; }

        [Column("NrAno")]
        public int NrAno { get; set; }

        [Column("CdStatusPropostaOrcamentaria")]
        public string CdStatusPropostaOrcamentaria { get; set; }

        [Column("CdTpFormulario")]
        public string CdTpFormulario { get; set; }

        [Column("IdPessoaResp")]
        public int? IdPessoaResp { get; set; }

        [Column("FgEstaReprovado")]
        public bool FgEstaReprovado { get; set; } = false;

        [Column("IdCentroCustoOriginal")]
        public int IdCentroCustoOriginal { get; set; }

        #region IAuditoria
        [Column("DtCadastro")]
        public DateTime DtCadastro { get; set; } = DateTime.Now;

        [Column("DtAlteracao")]
        public DateTime? DtAlteracao { get; set; }

        [Column("LoginCadastro")]
        public string LoginCadastro { get; set; }

        [Column("LoginAlteracao")]
        public string LoginAlteracao { get; set; }
        #endregion

        //[Ignore]
        //public decimal? VrOrcadoAnoAnteriorTotal => PropostaOrcamentariaItens?.Sum(x => x.VrOrcadoAnoAnterior);

        [Ignore]
        public string NoMotivoRejeicao { get; set; }

        [Ignore]
        public decimal? VrAprovadoAnoAnteriorTotal => PropostaOrcamentariaItens?.Sum(x => x.VrAprovadoAnoAnterior);

        [Ignore]
        public decimal? VrExecutadoAnoAnteriorTotal => PropostaOrcamentariaItens?.Sum(x => x.VrExecutadoAnoAnterior);

        [Ignore]
        public decimal? VrOrcadoTotal => PropostaOrcamentariaItens?.Sum(x => x.VrOrcadoAntesFinalizar ?? 0);

        [Ignore]
        public int? QtdItens => PropostaOrcamentariaItens?.Count();

        [Ignore]
        public string Status => CdTpFormulario != null && CdStatusPropostaOrcamentaria == StPropostaOrcamentaria.Aprovador1 ? "Ag. " + CdTpFormulario : StatusPropostaOrcamentaria.NoDescricao;

        [Ignore]
        public bool PodeEnviar => CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.ContabValidacao 
            && CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.Plenario
            && CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.PlenarioOK;

        [Ignore]
        public bool PodeRetornar => CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.Executor 
            && CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.ContabValidacao
            && CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.Plenario
            && CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.PlenarioOK;


        //[Ignore]
        //public bool FgFormulario => CdTpFormulario != null && CdStatusPropostaOrcamentaria == StPropostaOrcamentaria.Executor;

        //[Ignore]
        //public string TextoTooltipDisponibilizar => FgFormulario ? "O formulário será disponibilizado junto com a proposta" : "Disponibilizar proposta";

        //Só pode excluir um item de formulário que não tenha sido aprovado ainda.
        [Ignore]
        public bool PodeExcluir => CdTpFormulario != null && CdStatusPropostaOrcamentaria == StPropostaOrcamentaria.Executor;

        [Reference(ReferenceType.OneToOne, ColumnName = "IdPessoaResp", ReferenceMemberName = "IdPessoa")]
        public Pessoa Responsavel { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdCentroCusto", ReferenceMemberName = "IdCentroCusto")]
        public CentroCusto CentroCusto { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdCentroCustoOriginal", ReferenceMemberName = "IdCentroCusto")]
        public CentroCusto CentroCustoOriginal { get; set; }

        [Reference(ReferenceType.Many, ColumnName = "IdPropostaOrcamentariaDespesa", ReferenceMemberName = "IdPropostaOrcamentariaDespesa")]
        public List<PropostaOrcamentariaDespesaItem> PropostaOrcamentariaItens { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "CdStatusPropostaOrcamentaria", ReferenceMemberName = "CdStatusPropostaOrcamentaria")]
        public StatusPropostaOrcamentaria StatusPropostaOrcamentaria { get; set; }

        
    }
}
