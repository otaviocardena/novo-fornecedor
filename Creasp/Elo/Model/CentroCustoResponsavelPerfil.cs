﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Elo
{
    [PrimaryKey("IdCentroCustoResponsavel,CdCentroCustoPerfil")]
    public class CentroCustoResponsavelPerfil
    {
        [Column("IdCentroCustoResponsavel")]
        public int IdCentroCustoResponsavel { get; set; }

        [Column("CdCentroCustoPerfil")]
        public string CdCentroCustoPerfil { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdCentroCustoResponsavel", ReferenceMemberName = "IdCentroCustoResponsavel")]
        public CentroCustoResponsavel CentroCustoResponsavel { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "CdCentroCustoPerfil", ReferenceMemberName = "CdCentroCustoPerfil")]
        public CentroCustoPerfil CentroCustoPerfil { get; set; }
    }
}
