﻿using Creasp.Core;
using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Elo
{
    [PrimaryKey("IdPosicaoAtualOrcamento", AutoIncrement = true), Serializable]
    public class PosicaoAtualOrcamento : IFgAtivo, IEqualityComparer<PosicaoAtualOrcamento>
    {
        [Column("IdPosicaoAtualOrcamento")]
        public int IdPosicaoAtualOrcamento { get; set; }

        [Column("NrAno")]
        public int? NrAno { get; set; }

        [Column("NrCentroCusto")]
        public string NrCentroCusto { get; set; }

        [Column("NrContaContabil6")]
        public string NrContaContabil6 { get; set; }

        [Column("NrContaContabil5")]
        public string NrContaContabil5 { get; set; }

        [Column("FgReceita")]
        public bool FgReceita { get; set; }

        [Column("VrExecutado")]
        public decimal? VrExecutado { get; set; }

        [Column("DtRefVrExecutado")]
        public DateTime? DtRefVrExecutado { get; set; }

        [Column("VrPreEmpExercicio")]
        public decimal? VrPreEmpExercicio { get; set; }

        [Column("VrPreEmpDataRef")]
        public decimal? VrPreEmpDataRef { get; set; }

        [Column("VrEmpExercicio")]
        public decimal? VrEmpExercicio { get; set; }

        [Column("VrEmpDataRef")]
        public decimal? VrEmpDataRef { get; set; }

        [Column("VrLiqExercicio")]
        public decimal? VrLiqExercicio { get; set; }

        [Column("VrLiqDataRef")]
        public decimal? VrLiqDataRef { get; set; }

        [Column("VrPagoExercicio")]
        public decimal? VrPagoExercicio { get; set; }

        [Column("VrPagoDataRef")]
        public decimal? VrPagoDataRef { get; set; }

        [Column("VrSaldoOrcamentarioExercicio")]
        public decimal? VrSaldoOrcamentarioExercicio { get; set; }

        [Column("VrSaldoOrcamentarioDataRef")]
        public decimal? VrSaldoOrcamentarioDataRef { get; set; }

        [Column("VrSaldoALiqExercicio")]
        public decimal? VrSaldoALiqExercicio { get; set; }

        [Column("VrSaldoALiqDataRef")]
        public decimal? VrSaldoALiqDataRef { get; set; }

        [Column("VrSaldoAPagarExercicio")]
        public decimal? VrSaldoAPagarExercicio { get; set; }

        [Column("VrSaldoAPagarDataRef")]
        public decimal? VrSaldoAPagarDataRef { get; set; }

        [Column("FgAtivo")]
        public bool FgAtivo { get; set; }


        public bool Equals(PosicaoAtualOrcamento x, PosicaoAtualOrcamento y)
        {
            return
                x.IdPosicaoAtualOrcamento == y.IdPosicaoAtualOrcamento &&
                x.NrAno == y.NrAno &&
                x.NrCentroCusto == y.NrCentroCusto &&
                x.NrContaContabil5 == y.NrContaContabil5 &&
                x.NrContaContabil6 == y.NrContaContabil6 &&
                x.FgReceita == y.FgReceita &&
                x.VrExecutado == y.VrExecutado &&
                x.VrPreEmpExercicio == y.VrPreEmpExercicio &&
                x.VrPreEmpDataRef == y.VrPreEmpDataRef &&
                x.VrEmpExercicio == y.VrEmpExercicio &&
                x.VrEmpDataRef == y.VrEmpDataRef &&
                x.VrLiqExercicio == y.VrLiqExercicio &&
                x.VrLiqDataRef == y.VrLiqDataRef &&
                x.VrPagoExercicio == y.VrPagoExercicio &&
                x.VrPagoDataRef == y.VrPagoDataRef &&
                x.VrSaldoOrcamentarioExercicio == y.VrSaldoOrcamentarioExercicio &&
                x.VrSaldoOrcamentarioDataRef == y.VrSaldoOrcamentarioDataRef &&
                x.VrSaldoALiqExercicio == y.VrSaldoALiqExercicio &&
                x.VrSaldoALiqDataRef == y.VrSaldoALiqDataRef &&
                x.VrSaldoAPagarExercicio == y.VrSaldoAPagarExercicio &&
                x.VrSaldoAPagarDataRef == y.VrSaldoAPagarDataRef &&
                x.FgAtivo == y.FgAtivo;
        }

        public int GetHashCode(PosicaoAtualOrcamento obj)
        {
            return obj.NrAno.GetHashCode() + obj.NrCentroCusto.GetHashCode() + obj.NrContaContabil5.GetHashCode() + obj.FgReceita.GetHashCode();
        }

        public override string ToString()
        {
            string tipo = FgReceita ? "Receita" : "Despesa";
            string valor = VrExecutado.HasValue ? VrExecutado.Value.ToString("n2") : "0,00";
            return $"Centro: {NrCentroCusto} - Conta: {NrContaContabil5} ({tipo}): {valor} - Ativo: {(FgAtivo ? "S" : "N")})";
        }
    }
}
