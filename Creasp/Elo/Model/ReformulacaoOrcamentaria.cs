﻿using Creasp.Core;
using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Creasp.Elo
{
    [PrimaryKey("IdReformulacaoOrcamentaria", AutoIncrement = true), Serializable]
    public class ReformulacaoOrcamentaria : IAuditoria
    {
        [Column("IdReformulacaoOrcamentaria")]
        public int IdReformulacaoOrcamentaria { get; set; }

        [Column("IdReformulacao")]
        public int IdReformulacao { get; set; }

        [Column("IdPropostaOrcamentariaDespesa")]
        public int? IdPropostaOrcamentariaDespesa { get; set; }

        [Column("IdCentroCusto")]
        public int IdCentroCusto { get; set; }

        [Column("CdStatusPropostaOrcamentaria")]
        public string CdStatusPropostaOrcamentaria { get; set; }

        [Column("CdTpFormulario")]
        public string CdTpFormulario { get; set; }

        [Column("IdPessoaResp")]
        public int? IdPessoaResp { get; set; }

        [Column("FgEstaReprovado")]
        public bool FgEstaReprovado { get; set; } = false;

        [Column("IdCentroCustoOriginal")]
        public int IdCentroCustoOriginal { get; set; }

        [Column("DtCadastro")]
        public DateTime DtCadastro { get; set; } = DateTime.Now;

        [Column("DtAlteracao")]
        public DateTime? DtAlteracao { get; set; }

        [Column("LoginCadastro")]
        public string LoginCadastro { get; set; }

        [Column("LoginAlteracao")]
        public string LoginAlteracao { get; set; }

        [Ignore]
        public string NoMotivoRejeicao { get; set; }

        [Ignore]
        public string Status => CdTpFormulario != null && CdStatusPropostaOrcamentaria == StPropostaOrcamentaria.Aprovador1 ? "Ag. " + CdTpFormulario : StatusPropostaOrcamentaria.NoDescricao;

        [Ignore]
        public decimal? VrOrcadoTotal => ReformulacaoOrcamentariaItens?.Sum(x => x.VrOrcamentoReformulado ?? 0);

        [Ignore]
        public int? QtdItens => ReformulacaoOrcamentariaItens?.Count();

        [Ignore]
        public bool PodeEnviar => CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.ContabValidacao
            && CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.Plenario
            && CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.PlenarioOK;

        [Ignore]
        public bool PodeRetornar => CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.Executor 
            && CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.PlenarioOK;

        [Reference(ReferenceType.OneToOne, ColumnName = "IdReformulacao", ReferenceMemberName = "IdReformulacao")]
        public Reformulacao Reformulacao { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdPessoaResp", ReferenceMemberName = "IdPessoa")]
        public Pessoa Responsavel { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdCentroCusto", ReferenceMemberName = "IdCentroCusto")]
        public CentroCusto CentroCusto { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdCentroCustoOriginal", ReferenceMemberName = "IdCentroCusto")]
        public CentroCusto CentroCustoOriginal { get; set; }

        [Reference(ReferenceType.Many, ColumnName = "IdReformulacaoOrcamentaria", ReferenceMemberName = "IdReformulacaoOrcamentaria")]
        public List<ReformulacaoOrcamentariaItem> ReformulacaoOrcamentariaItens { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "CdStatusPropostaOrcamentaria", ReferenceMemberName = "CdStatusPropostaOrcamentaria")]
        public StatusPropostaOrcamentaria StatusPropostaOrcamentaria { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdPropostaOrcamentariaDespesa", ReferenceMemberName = "IdPropostaOrcamentariaDespesa")]
        public PropostaOrcamentariaDespesa PropostaOrcamentariaDespesa { get; set; }


        //public PropostaReformulacaoDTO ToDTO()
        //{
        //    var dto = new PropostaReformulacaoDTO()
        //    {
        //        IdCentroCusto = this.IdCentroCusto,
        //        CdTpFormulario = this.CdTpFormulario,
        //        CdStatusPropostaOrcamentaria = StPropostaOrcamentaria.Executor,
        //        IdPessoaResp = this.CentroCusto.CentroCustoResponsavel.IdPessoaExecutor,
        //        IdPropostaOrcamentariaDespesa = this.IdPropostaOrcamentariaDespesa,
        //        PropostaReformulacaoItensDTO = this.ReformulacaoOrcamentariaItens.Select(x => new PropostaReformulacaoItensDTO()
        //        {
        //            IdContaContabil = x.IdContaContabil,
        //            IdPropostaOrcamentariaDespesaItem = x.IdPropostaOrcamentariaDespesaItem.Value,
        //            NoJustificativa = x.NoJustificativa,
        //            VrOrcadoInicialmente = x.VrOrcadoInicialmente,
        //            VrReducao = x.VrReducao,
        //            VrSuplementacao = x.VrSuplementacao
        //        }).ToList()
        //    };

        //    return dto;
        //}
    }
}
