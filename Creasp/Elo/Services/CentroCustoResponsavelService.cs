﻿using Creasp.Core;
using Creasp.Elo;
using NBox.Core;
using NBox.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Creasp.Elo
{
    public class CentroCustoResponsavelService : DataService
    {
        protected readonly int nrAnoBase;

        public CentroCustoResponsavelService(ServiceContext context) : base(context)
        {
            nrAnoBase = (context as CreaspContext).NrAnoBase;
        }

        #region Public

        /// <summary>
        /// Busca os responsável de um centro de custo pelo ID do CentroCustoResponsável
        /// </summary>
        public CentroCustoResponsavel BuscaCentroCustoResponsavel(int idCentroCusto, bool? fgOrcavel)
        {
            var ccr = db.Query<CentroCustoResponsavel>()
                .Include(x => x.CentroCusto)
                .Include(x => x.ResponsavelExecucao)
                .Include(x => x.ResponsavelAprova)
                .Include(x => x.ResponsavelAprova2)
                .Include(x => x.ResponsavelAprova3)
                .Include(x => x.ResponsavelAprova4)
                .IncludeMany(x => x.CentroCustoResponsaveisPerfis)
                .Where(x => x.IdCentroCusto == idCentroCusto)
                .Where(fgOrcavel.HasValue, x => x.FgOrcavel == fgOrcavel.Value)
                .SingleOrDefault();

            return ccr;
        }

        /// <summary>
        /// Busca os responsável de um centro de custo pelo ID do CentroCustoResponsável
        /// </summary>
        public CentroCustoResponsavel BuscaCentroCustoResponsavel(int idCentroCustoResponsavel)
        {
            return db.Query<CentroCustoResponsavel>()
                .Include(x => x.CentroCusto)
                .Include(x => x.ResponsavelExecucao)
                .Include(x => x.ResponsavelAprova)
                .Include(x => x.ResponsavelAprova2)
                .Include(x => x.ResponsavelAprova3)
                .Include(x => x.ResponsavelAprova4)
                .Where(x => x.IdCentroCustoResponsavel == idCentroCustoResponsavel)
                .SingleOrDefault();
        }

        /// <summary>
        /// Busca os responsável de um centro de custo pelo número do centro de custo
        /// </summary>
        public CentroCustoResponsavel BuscaCentroCustoResponsavel(string nrCentroCusto)
        {
            return db.Query<CentroCustoResponsavel>()
                .Include(x => x.CentroCusto)
                .Include(x => x.ResponsavelExecucao)
                .Include(x => x.ResponsavelAprova)
                .Include(x => x.ResponsavelAprova2)
                .Include(x => x.ResponsavelAprova3)
                .Include(x => x.ResponsavelAprova4)
                .Where(x => x.CentroCusto.NrCentroCusto == nrCentroCusto)
                .Where(x => x.CentroCusto.NrAno == nrAnoBase)
                .SingleOrDefault();
        }

        /// <summary>
        /// Busca os responsável de um centro de custo pelo ID do CentroCusto
        /// </summary>
        public CentroCustoResponsavel BuscaCentroCustoResponsavelPorCentroCusto(int idCentroCusto)
        {
            return db.Query<CentroCustoResponsavel>()
                .Include(x => x.CentroCusto)
                .Include(x => x.ResponsavelExecucao)
                .Include(x => x.ResponsavelAprova)
                .Include(x => x.ResponsavelAprova2)
                .Include(x => x.ResponsavelAprova3)
                .Include(x => x.ResponsavelAprova4)
                .Where(x => x.IdCentroCusto == idCentroCusto)
                .Where(x => x.CentroCusto.FgAtivo)
                .SingleOrDefault();
        }

        /// <summary>
        /// Lista todos os centros de custo no grid. Permite filtros por nome/número/tipo de centro de custo e nome de responsável
        /// </summary>
        public QueryPage<CentroCustoResponsavel> ListaCentrosCustosResponsaveis(Pager p,
            bool apenasOrcaveis = false,
            //int? nrAno = null,
            string tpCentroCusto = null,
            string nrCentroCusto = null,
            string noCentroCusto = null,
            string noResponsavel = null
            )
        {
            return db.Query<CentroCustoResponsavel>()
                .Include(x => x.CentroCusto)
                .Include(x => x.ResponsavelExecucao)
                .Include(x => x.ResponsavelAprova)
                .Include(x => x.ResponsavelAprova2)
                .Include(x => x.ResponsavelAprova3)
                .Include(x => x.ResponsavelAprova4)
                .IncludeMany(x => x.CentroCustoResponsaveisPerfis)
                .Where(x => x.CentroCusto.FgAtivo == true)
                .Where(x => x.CentroCusto.FgRestringirFaseOrcamentaria == false)
                //.Where(x => x.CentroCusto.NrAno == nrAno.Value)
                .Where(x => x.CentroCusto.NrAno == nrAnoBase)
                .Where(apenasOrcaveis != false, x => x.FgOrcavel)
                .Where(nrCentroCusto != null, x => x.CentroCusto.NrCentroCusto.StartsWith(nrCentroCusto.Replace(".", "").ToLike()))
                .Where(noCentroCusto != null, x => x.CentroCusto.NoCentroCusto.Contains(noCentroCusto.ToLike()))
                .Where(!string.IsNullOrEmpty(tpCentroCusto), x => x.CentroCusto.TpCentroCusto == tpCentroCusto)
                .Where(noResponsavel != null,
                        x => x.ResponsavelExecucao.NoPessoa.StartsWith(noResponsavel.ToLike())
                        || x.ResponsavelAprova.NoPessoa.StartsWith(noResponsavel.ToLike())
                        || x.ResponsavelAprova2.NoPessoa.StartsWith(noResponsavel.ToLike())
                        || x.ResponsavelAprova3.NoPessoa.StartsWith(noResponsavel.ToLike())
                        || x.ResponsavelAprova4.NoPessoa.StartsWith(noResponsavel.ToLike()))
                .ToPage(p.DefaultSort<CentroCustoResponsavel>(x => x.CentroCusto.NrCentroCusto));
        }

        /// <summary>
        /// Lista todos os centros de custos responsáves ativos.
        /// </summary>
        public List<CentroCustoResponsavel> ListaCentrosCustosResponsaveis()
        {
            return db.Query<CentroCustoResponsavel>()
                .Include(x => x.CentroCusto)
                .Include(x => x.ResponsavelExecucao)
                .Include(x => x.ResponsavelAprova)
                .Include(x => x.ResponsavelAprova2)
                .Include(x => x.ResponsavelAprova3)
                .Include(x => x.ResponsavelAprova4)
                .IncludeMany(x => x.CentroCustoResponsaveisPerfis)
                .Where(x => x.CentroCusto.FgAtivo == true)

                //.Where(x => x.CentroCusto.NrAno == nrAno.Value)
                .ToList();
        }

        /// <summary>
        /// Lista os centros de custo orçáveis, que poderão ser utilizados em propostas orçamentárias
        /// </summary>
        public List<CentroCustoResponsavel> ListaCentrosCustosOrcaveis()
        {
            return db.Query<CentroCustoResponsavel>()
                .Include(x => x.CentroCusto)
                .Include(x => x.ResponsavelExecucao)
                .Where(x => x.CentroCusto.NrAno == nrAnoBase)
                .Where(x => x.CentroCusto.FgAtivo == true)
                .Where(x => x.FgOrcavel == true)
                .ToList();
        }

        /// <summary>
        /// Lista os centros de custos orçáveis que estejam vinculados ao usuário logado ordenados pelo número do centro de custo.
        /// </summary>
        /// <returns></returns>
        public IEnumerable<CentroCustoResponsavel> ListaCentrosCustosOrcaveisDoUsuarioLogado()
        {
            int idUsuarioLogado = (user as CreaspUser).IdPessoa;

            List<int?> subDelegacoes = new PessoaService(context).ListaDelegacoes(idUsuarioLogado).ToList();

            return db.Query<CentroCustoResponsavel>()
                .Include(x => x.CentroCusto)
                .Include(x => x.ResponsavelExecucao)
                .Include(x => x.ResponsavelAprova)
                .Include(x => x.ResponsavelAprova2)
                .Include(x => x.ResponsavelAprova3)
                .Include(x => x.ResponsavelAprova4)
                .Where(x => x.FgOrcavel)
                .Where(x => x.CentroCusto.NrAno == nrAnoBase)
                .Where(x =>
                        subDelegacoes.Contains(x.ResponsavelExecucao.IdPessoa)
                     || subDelegacoes.Contains(x.ResponsavelAprova.IdPessoa)
                     || subDelegacoes.Contains(x.ResponsavelAprova2.IdPessoa)
                     || subDelegacoes.Contains(x.ResponsavelAprova3.IdPessoa)
                     || subDelegacoes.Contains(x.ResponsavelAprova4.IdPessoa))
                .OrderBy(x => x.CentroCusto.NrCentroCusto)
                .ToArray();
        }

        /// <summary>
        /// Efetua uma validação nos dados antes de salvar os responsáveis por um centro de custo.
        /// </summary>
        public void ValidaEAlteraResponsaveis(int idCentroCustoResponsavel, int idPessoaExecuta, int idPessoaAprova1, int idPessoaAprova2, int idPessoaAprova3, int idPessoaAprova4, List<string> cdsCentroCustoPerfis)
        {
            if (ValidaResponsaveis(idCentroCustoResponsavel, idPessoaExecuta, idPessoaAprova1, idPessoaAprova2, idPessoaAprova3, idPessoaAprova4))
            {
                using (var trans = db.GetTransaction())
                {
                    var ccPerfilService = new CentroCustoPerfilService(context);
                    ccPerfilService.SalvaCentroCustoResponsavelPerfil(idCentroCustoResponsavel, cdsCentroCustoPerfis);

                    AlteraResponsaveis(idCentroCustoResponsavel, idPessoaExecuta, idPessoaAprova1, idPessoaAprova2, idPessoaAprova3, idPessoaAprova4);
                    var centroCustoResponsavel = BuscaCentroCustoResponsavel(idCentroCustoResponsavel);
                    AtualizaResponsaveisPropostasReformulacoes(centroCustoResponsavel.IdCentroCusto, idPessoaExecuta, idPessoaAprova1, idPessoaAprova2, idPessoaAprova3, idPessoaAprova4);

                    trans.Complete();
                }
            }
            else
            {
                throw new NBoxException("Você não pode ter um aprovador de nível hierárquico alto sem ter todos abaixo.");
            }
        }

        /// <summary>
        /// Valida e salva os centros de custos orçáveis.
        /// </summary>
        /// <param name="idsTodosCentrosDeCustosDaPagina">IDs de todos os centros de custos da PÁGINA da Grid (não são todos os IDs da Grid)</param>
        /// <param name="idsTodosCentrosDeCustosDaPaginaSelecionados">IDs de todos os centros de custos selecionados como orçáveis da PÁGINA da Grid (não são todos os IDs selecionados como orçáveis da Grid)</param>
        /// <returns>TRUE se todas as alterações solicitadas pelo usuário foram confirmadas, FALSE se pelo menos uma foi invalidada por possuir centro de custo pai ou filho já orçável</returns>
        public bool ValidaESalvaCentrosOrcaveis(int[] idsTodosCentrosDeCustosDaPagina, int[] idsTodosCentrosDeCustosDaPaginaSelecionados)
        {
            bool todasAlteracoesEfetuadas = true;

            var todosCentrosDeCustosDaPagina = ListaCentrosCustosResponsaveis(idsTodosCentrosDeCustosDaPagina)
                                                .OrderByDescending(x => x.CentroCusto.NrCentroCusto.Length).ToList();

            var todosCentrosDeCustosOrcaveis = ListaCentrosCustosOrcaveis();

            using (var trans = db.GetTransaction())
            {
                foreach (var c in todosCentrosDeCustosDaPagina)
                {
                    var isOrcavel = idsTodosCentrosDeCustosDaPaginaSelecionados.Contains(c.IdCentroCustoResponsavel);
                    if (isOrcavel)
                    {
                        if (PossuiFilhoOrcavel(todosCentrosDeCustosOrcaveis, c.CentroCusto.NrCentroCusto) || PossuiPaiOrcavel(todosCentrosDeCustosOrcaveis, c))
                        {
                            isOrcavel = false;
                            todasAlteracoesEfetuadas = false;
                        }
                        else
                        {
                            todosCentrosDeCustosOrcaveis.Add(c);
                        }
                    }

                    c.FgOrcavel = isOrcavel;
                    db.Update(c);
                }

                trans.Complete();
            }
            return todasAlteracoesEfetuadas;
        }

        /// <summary>
        /// Insere os responsáveis para os centros de custos enviados por parâmetro - que ainda não exitem na base.
        /// </summary>
        public void InsereCentrosCustosResponsaveis(int[] idsCentrosCustos)
        {
            //Verifica quais IdsCentroCustos já existem
            var idsJaExistentes = db.Query<CentroCustoResponsavel>()
                .Where(x => idsCentrosCustos.Contains(x.IdCentroCusto))
                .ToList()
                .Select(x => x.IdCentroCusto)
                .ToList();

            //List<CentroCustoResponsavel> apenasNovos = new List<CentroCustoResponsavel>();

            //Remove do array os idsCentroCustos já existentes na base.
            idsCentrosCustos = idsCentrosCustos.Except(idsJaExistentes).ToArray();

            //Adiciona numa lista todos os idsCentroCustosNovos
            idsCentrosCustos.ToList().ForEach(x => db.Insert(new CentroCustoResponsavel() { IdCentroCusto = x, FgOrcavel = false }));
        }

        /// <summary>
        /// Lista os centros de custos que podem virar novas propostas orçamentárias.
        /// São centros de custos que ainda não são propostas orçamentárias, mas estão ativos, são orçáveis e possuem executor cadastrado.
        /// </summary>
        public List<CentroCusto> ListaCentrosCustosPassiveisDeViraremPropostas()
        {
            var centrosQueJaSaoPropostas = db.Query<PropostaOrcamentariaDespesa>()
                .Include(x => x.CentroCusto)
                .Where(x => x.NrAno == nrAnoBase)
                .ToList().Select(x => x.CentroCusto.IdCentroCusto).ToList();

            return db.Query<CentroCustoResponsavel>()
                .Include(x => x.CentroCusto)
                .Where(x => x.CentroCusto.FgAtivo == true)
                .Where(x => x.CentroCusto.NrAno == nrAnoBase)
                .Where(x => !centrosQueJaSaoPropostas.Contains(x.IdCentroCusto))
                .Where(x => x.FgOrcavel == true)
                .Where(x => x.IdPessoaExecutor != null)
                .ToList().Select(x => x.CentroCusto).Distinct().ToList();
        }

        /// <summary>
        /// Lista os centros de custos que podem virar novas propostas orçamentárias.
        /// São centros de custos que ainda não são propostas de reformulação, mas estão ativos, são orçáveis e possuem executor cadastrado.
        /// </summary>
        public List<CentroCusto> ListaCentrosCustosPassiveisDeViraremPropostasDeReformulacao(int nrAno, int idReformulacao)
        {
            var centrosQueJaSaoPropostas = db.Query<ReformulacaoOrcamentaria>()
                .Include(x => x.CentroCusto)
                .Where(x => x.IdReformulacao == idReformulacao)
                .ToList().Select(x => x.CentroCusto.IdCentroCusto).ToList();

            return db.Query<CentroCustoResponsavel>()
                .Include(x => x.CentroCusto)
                .Where(x => x.CentroCusto.FgAtivo == true)
                .Where(x => x.CentroCusto.NrAno == nrAno)
                .Where(x => !centrosQueJaSaoPropostas.Contains(x.IdCentroCusto))
                .Where(x => x.FgOrcavel == true)
                .Where(x => x.IdPessoaExecutor != null)
                .ToList().Select(x => x.CentroCusto).Distinct().ToList();
        }

        /// <summary>
        /// Valida se existem centros de custos orçáveis sem executor determinado.
        /// </summary>
        /// <returns></returns>
        public bool ValidaCentroCustosOrcaveisComExecutor(int nrAno)
        {
            var temCentrosOrcaveisSemExecutor = db.Query<CentroCustoResponsavel>()
                .Include(x => x.CentroCusto)
                .Where(x => x.CentroCusto.NrAno == nrAno)
                .Where(x => x.FgOrcavel == true)
                .Where(x => x.IdPessoaExecutor == null)
                .Any();

            return !temCentrosOrcaveisSemExecutor;
        }

        /// <summary>
        /// Atualiza os responsáveis atuais das propostas orçamentárias e reformulações.
        /// </summary>
        public void AtualizaResponsaveisPropostasReformulacoes(int idCentroCusto, int idExecutor, int idAprovador1, int idAprovador2, int idAprovador3, int idAprovador4)
        {
            var propostas = db.Query<PropostaOrcamentariaDespesa>()
                .Where(x => x.NrAno == nrAnoBase)
                .Where(x => x.IdCentroCusto == idCentroCusto)
                .Where(x => x.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.ContabValidacao)
                .Where(x => x.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.Plenario)
                .Where(x => x.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.PlenarioOK)
                .ToList();

            if (propostas != null)
            {
                foreach (var p in propostas)
                {
                    int? idNovoResponsavel = DefineNovoResponsavelPeloStatus(p.CdStatusPropostaOrcamentaria, idExecutor, idAprovador1, idAprovador2, idAprovador3, idAprovador4);

                    if (idNovoResponsavel.HasValue) //Se não possuir valor, não deve atualizar nada
                    {
                        p.IdPessoaResp = idNovoResponsavel.Value;
                        db.Update(p);
                    }
                }
            }

            var refOrcamentarias = db.Query<ReformulacaoOrcamentaria>()
                .Include(x => x.Reformulacao)
                .Where(x => x.Reformulacao.NrAno == nrAnoBase)
                .Where(x => x.IdCentroCusto == idCentroCusto)
                .Where(x => x.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.ContabValidacao)
                .Where(x => x.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.Plenario)
                .Where(x => x.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.PlenarioOK)
                .ToList();

            if (refOrcamentarias != null)
            {
                using (var trans = db.GetTransaction())
                {
                    foreach (var r in refOrcamentarias)
                    {
                        int? idNovoResponsavel = DefineNovoResponsavelPeloStatus(r.CdStatusPropostaOrcamentaria, idExecutor, idAprovador1, idAprovador2, idAprovador3, idAprovador4);

                        if (idNovoResponsavel.HasValue) //Se não possuir valor, não deve atualizar nada
                        {
                            r.IdPessoaResp = idNovoResponsavel.Value;
                            db.Update(r);
                        }
                    }

                    trans.Complete();
                }
            }
        }
        #endregion

        #region Private

        /// <summary>
        /// Valida se os responsáveis de um centro de custo foram cadastrados na ordem correta. 
        /// Impede que seja cadastrado um "Aprovador 4" sem ter um "Aprovador 2", por exemplo.
        /// </summary>
        private bool ValidaResponsaveis(int idCentroCustoResponsavel, int idPessoaExecuta, int idPessoaAprova1, int idPessoaAprova2, int idPessoaAprova3, int idPessoaAprova4)
        {
            //TODO: Código de teste, funciona, mas não tem boa legibilidade
            return !((idPessoaAprova4 != 0 && (idPessoaAprova3 == 0 || idPessoaAprova2 == 0 || idPessoaAprova1 == 0 || idPessoaExecuta == 0))
                || (idPessoaAprova3 != 0 && (idPessoaAprova2 == 0 || idPessoaAprova1 == 0 || idPessoaExecuta == 0))
                || (idPessoaAprova2 != 0 && (idPessoaAprova1 == 0 || idPessoaExecuta == 0))
                || (idPessoaAprova1 != 0 && (idPessoaExecuta == 0)));
        }

        /// <summary>
        /// Altera as pessoas responsáveis pelo centro de custo.
        /// </summary>
        private void AlteraResponsaveis(int idCentroCustoResponsavel, int idPessoaExecuta, int idPessoaAprova1, int idPessoaAprova2, int idPessoaAprova3, int idPessoaAprova4)
        {
            var c = db.SingleById<CentroCustoResponsavel>(idCentroCustoResponsavel);
            c.IdPessoaExecutor = idPessoaExecuta != 0 ? idPessoaExecuta : (int?)null;
            c.IdPessoaAprovador1 = idPessoaAprova1 != 0 ? idPessoaAprova1 : (int?)null;
            c.IdPessoaAprovador2 = idPessoaAprova2 != 0 ? idPessoaAprova2 : (int?)null;
            c.IdPessoaAprovador3 = idPessoaAprova3 != 0 ? idPessoaAprova3 : (int?)null;
            c.IdPessoaAprovador4 = idPessoaAprova4 != 0 ? idPessoaAprova4 : (int?)null;
            db.AuditUpdate(c);
        }

        /// <summary>
        /// Busca o centro de custo responsável baseado no valor que estiver no WebConfig para chave do parâmetro.
        /// </summary>
        /// <param name="chaveWebConfig">Nome da chave como está no Web Config</param>
        /// <returns></returns>
        private CentroCustoResponsavel BuscaCentroCustoResponsavelPelaConfiguracao(string chaveWebConfig)
        {
            if (string.IsNullOrEmpty(chaveWebConfig)) throw new ArgumentNullException("chaveWebConfig");

            string nrCentroCusto = ConfigurationManager.AppSettings[chaveWebConfig].UnMask();
            return BuscaCentroCustoResponsavel(nrCentroCusto);
        }

        /// <summary>
        /// Valida se um centro de custo possui algum "centro de custo filho" já marcado como orçável, baseado numa lista pré definida.
        /// </summary>
        /// <param name="todosCentrosDeCustosDoAno">Lista de todos os centros de custos de um ano em que a validação precisa ser feita.</param>
        /// <param name="nrCentroCusto">Número do centro de custo que será verificado</param>
        /// <returns>TRUE se houver algum centro de custo filho orçável, FALSE se não tiver</returns>
        private bool PossuiFilhoOrcavel(List<CentroCustoResponsavel> todosCentrosDeCustosDoAno, string nrCentroCusto)
        {
            return todosCentrosDeCustosDoAno
                    .Where(x => x.FgOrcavel)
                    .Where(x => x.CentroCusto.NrCentroCusto != nrCentroCusto)
                    .Where(x => x.CentroCusto.NrCentroCusto.StartsWith(nrCentroCusto))
                    .Any();
        }

        /// <summary>
        /// Valida se um centro de custo possui o "centro de custo pai" já marcado como orçável, baseado numa lista pré definida.
        /// </summary>
        /// <param name="todosCentrosDeCustosDoAno">Lista de todos os centros de custos de um ano em que a validação precisa ser feita.</param>
        /// <param name="nrCentroCusto">Número do centro de custo que será verificado</param>
        /// <returns>TRUE se o centro de custo pai orçável, FALSE se não for</returns>
        private bool PossuiPaiOrcavel(List<CentroCustoResponsavel> todosCentrosDeCustosDoAno, CentroCustoResponsavel centroCustoResp)
        {
            int qtdTentativasEncontrarPaiOrcavel = 0;
            CentroCusto centroTemp = centroCustoResp.CentroCusto;

            while (!string.IsNullOrWhiteSpace(centroTemp?.NrCentroCusto) && qtdTentativasEncontrarPaiOrcavel < 5)
            {
                var jaExistePaiOrcavel = todosCentrosDeCustosDoAno
                        .Where(x => x.CentroCusto.FgAtivo)
                        .Where(x => x.CentroCusto.NrCentroCusto == centroTemp.NrCentroCustoPai)
                        .Where(x => x.FgOrcavel)
                        .Any();

                if (jaExistePaiOrcavel)
                {
                    return true;
                }

                string nrNovoCentroCusto = centroTemp.NrCentroCustoPai;

                centroTemp = new CentroCusto() { NrCentroCusto = nrNovoCentroCusto };
                qtdTentativasEncontrarPaiOrcavel++;
            }

            return false;
        }

        /// <summary>
        /// Lista os centros de custos responsáveis quem possuam os IDs informados
        /// </summary>
        /// <param name="ids">IDs dos centros de custos responsáveis</param>
        /// <returns></returns>
        private List<CentroCustoResponsavel> ListaCentrosCustosResponsaveis(int[] ids)
        {
            return db.Query<CentroCustoResponsavel>()
                .Include(x => x.CentroCusto)
                //.Where(x => x.CentroCusto.FgAtivo)
                .Where(x => ids.Contains(x.IdCentroCustoResponsavel))
                .ToList();
        }

        private int? DefineNovoResponsavelPeloStatus(string cdStatusPropostaOrcamentaria, int idExecutor, int idAprovador1, int idAprovador2, int idAprovador3, int idAprovador4)
        {
            switch (cdStatusPropostaOrcamentaria)
            {
                case StPropostaOrcamentaria.Executor:
                    if (idExecutor > 0)
                        return idExecutor;
                    break;
                case StPropostaOrcamentaria.Aprovador1:
                    if (idAprovador1 > 0)
                        return idAprovador1;
                    break;
                case StPropostaOrcamentaria.Aprovador2:
                    if (idAprovador2 > 0)
                        return idAprovador2;
                    break;
                case StPropostaOrcamentaria.Aprovador3:
                    if (idAprovador3 > 0)
                        return idAprovador3;
                    break;
                case StPropostaOrcamentaria.Aprovador4:
                    if (idAprovador4 > 0)
                        return idAprovador4;
                    break;
                default:
                    break;
            }

            return null;
        }

        #endregion
    }
}
