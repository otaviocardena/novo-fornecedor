﻿using Creasp.Core;

using Creasp.Elo;
using NBox.Core;
using NBox.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Creasp.Elo
{
    public class MetodologiaReceitaService : DataService
    {
        protected readonly int nrAnoBase;

        public MetodologiaReceitaService(ServiceContext context) : base(context)
        {
            nrAnoBase = (context as CreaspContext).NrAnoBase;
        }

        #region Public

        /// <summary>
        /// Lista as contas contábeis de receita na grid view
        /// </summary>
        public QueryPage<MetodologiaReceitaAno> ListaMetodologias(Pager p,
            int? idReformulacao,
            int cdTipoContaContabil,
            string nrContaContabil = null,
            string noContaContabil = null)
        {
            var metodologias = db.Query<MetodologiaReceitaAno>()
                .Include(x => x.ContaContabil)
                .Include(x => x.Responsavel)
                .Include(x => x.StatusPropostaOrcamentaria)
                .Where(x => x.ContaContabil.FgAtivo == true)
                .Where(x => x.NrAno == nrAnoBase)
                .Where(x => x.ContaContabil.NrContaContabil.StartsWith(CdPrefixoContaContabil.ReceitaInicial))
                .Where(x => x.FgReformulacao == idReformulacao.HasValue)
                .Where(idReformulacao.HasValue, x => x.IdReformulacao == idReformulacao.Value)
                .Where(nrContaContabil != null, x => x.ContaContabil.NrContaContabil.StartsWith(nrContaContabil.UnMask().ToLike()))
                .Where(noContaContabil != null, x => x.ContaContabil.NoContaContabil.Contains(noContaContabil.ToLike()))
                .Where(cdTipoContaContabil != 0, x => x.ContaContabil.FgAnalitico == (cdTipoContaContabil == CdTipoConta.Analitico))
                .ToPage(p.DefaultSort<MetodologiaReceitaAno>(x => x.ContaContabil.NrContaContabil));

            return metodologias;
        }

        /// <summary>
        /// Busca uma única metodologia de receita pela conta contábil, com os seus meses. Se for de uma reformulação, passar o ID da reformulação também por parâmetro.
        /// </summary>
        public MetodologiaReceitaAno BuscaMetodologiaReceita(int idMetodologia)
        {
            return db.Query<MetodologiaReceitaAno>()
                .Include(x => x.ContaContabil)
                //.Include(x => x.Responsavel) //Se descomentar essa linha, dá erro. 
                //.Include(x => x.StatusPropostaOrcamentaria)
                .IncludeMany(x => x.MetodologiasReceitaMes)
                .Where(x => x.IdMetodologiaReceitaAno == idMetodologia)
                //.Where(idReformulacao.HasValue, x => x.IdReformulacao == idReformulacao.Value)
                //.Where(x => x.FgReformulacao == idReformulacao.HasValue)
                .SingleOrDefault();
        }

        /// <summary>
        /// Lista as metodologias de receita, com os respectivos meses, de um ano específico ou de uma reformulação.
        /// </summary>
        public List<MetodologiaReceitaAno> BuscaMetodologias(int nrAno, int? idReformulacao = null)
        {
            var dados = db.Query<MetodologiaReceitaAno>()
                .Include(x => x.ContaContabil)
                .Include(x => x.Responsavel)
                .Include(x => x.Reformulacao)
                .IncludeMany(x => x.MetodologiasReceitaMes)
                .Where(x => x.NrAno == nrAno)
                .Where(idReformulacao.HasValue == false, x => x.FgReformulacao == false)
                .Where(idReformulacao.HasValue, x => x.IdReformulacao == idReformulacao.Value)
                .ToList();

            return dados;
        }

        /// <summary>
        /// Busca o valor total orçado para um ano (previsão inicial) ou de uma reformulação específica.
        /// </summary>
        public decimal BuscaTotalOrcadoAno(int? idReformulacao = null)
        {
            var soma = db.Query<MetodologiaReceitaAno>()
                .Where(x => x.NrAno == nrAnoBase)
                .Where(x => x.IdReformulacao == idReformulacao)
                .ToList()?
                .Sum(x => x.VrOrcado);

            return soma.HasValue ? soma.Value : 0;

        }

        /// <summary>
        /// Cria completamente as metodologias de receita para um determinado ano.
        /// </summary>
        /// <param name="nrAno">Número do ano em que serão criadas as metodologias.</param>
        /// <param name="prAplicadoReceita">Percentual que será aplicado (multiplicado) no valor do ano anterior.</param>
        /// <param name="prCotaParteConfea">Percentual que será igual em todas as metodologias.</param>
        /// <param name="prCotaParteMutua">Percentual que será igual em todas as metodologias.</param>
        /// <param name="noObservacoesReceitaPadrao">Observações que serão iguais em todas as metodologias de receita.</param>
        public void CriaMetodologiaReceita(int nrAno, decimal? prAplicadoReceita, decimal? prCotaParteConfea, decimal? prCotaParteMutua, string noObservacoesReceitaPadrao)
        {
            string[] meses = new string[] { "Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro" };

            var contaContabilService = new ContaContabilService(context);
            var dotacaoOrcamentariaService = new OrcamentoService(context);
            var centroPerfilService = new CentroCustoPerfilService(context);

            var centroCustoContabilidade = centroPerfilService.BuscaSeHouverCentroCustoResponsavel(TpPerfilCentroCusto.Contabilidade);

            var contasReceitaAnalitica = contaContabilService.ListaContasContabeis(nrAno, CdPrefixoContaContabil.ReceitaInicial, CdTipoConta.Analitico);
            var disponibilidadeOrcamento = dotacaoOrcamentariaService.ListaDisponibilidadeAtualOrcamento(nrAno - 1, nrAno - 1);
            var metodologiasAnoAnterior = BuscaMetodologias(nrAno - 1);

            //Troca o número da conta contábil de reformulação pra previsão inicial, para que possa ser feito o match depois.
            disponibilidadeOrcamento.ForEach(x => x.NrContaContabil5 = x.NrContaContabil5.Replace(CdPrefixoContaContabil.ReceitaReformulacao, CdPrefixoContaContabil.ReceitaInicial));

            var lstNovasMetodologias = new List<MetodologiaReceitaAno>();
            using (var trans = db.GetTransaction())
            {
                foreach (var conta in contasReceitaAnalitica)
                {
                    var metodologia = new MetodologiaReceitaAno();

                    metodologia.IdContaContabil = conta.IdContaContabil;
                    metodologia.NrAno = nrAno;
                    metodologia.PrAplicado = prAplicadoReceita;
                    metodologia.FgReformulacao = false;
                    metodologia.IdPessoaResp = centroCustoContabilidade.IdPessoaExecutor;
                    metodologia.CdStatusPropostaOrcamentaria = StPropostaOrcamentaria.Executor;

                    var metodologiaAnoAnterior = metodologiasAnoAnterior
                                            .Where(x => x.ContaContabil.NrContaContabil == conta.NrContaContabil)
                                            .SingleOrDefault();

                    if (metodologiaAnoAnterior != null)
                    {
                        metodologia.PrCotaParteConfea = metodologiaAnoAnterior.PrCotaParteConfea;
                        metodologia.PrCotaParteMutua = metodologiaAnoAnterior.PrCotaParteMutua;
                        metodologia.PrSaoPaulo = metodologiaAnoAnterior.PrSaoPaulo;
                        metodologia.PrBrasilia = metodologiaAnoAnterior.PrBrasilia;
                        metodologia.NoObservacoes = metodologiaAnoAnterior.NoObservacoes;
                        metodologia.VrAprovadoAnoAnterior = metodologiaAnoAnterior.VrOrcado;
                        metodologia.VrOrcado = metodologia.VrAprovadoAnoAnterior * (1 + metodologia.PrAplicado / 100);
                    }
                    else
                    {
                        metodologia.PrCotaParteConfea = prCotaParteConfea;
                        metodologia.PrCotaParteMutua = prCotaParteMutua;
                        metodologia.NoObservacoes = noObservacoesReceitaPadrao;
                        
                        //Apesar de ser o valor aprovado do ano anterior, já é pra buscar o valor com as devidas reformulações, ou seja, do conjunto "disponibilidade atual do orçamento".
                        metodologia.VrAprovadoAnoAnterior = disponibilidadeOrcamento
                            .Where(x => x.NrContaContabil5 == conta.NrContaContabil)
                            .Sum(x => x.VrOrcado);

                        if (metodologia.VrAprovadoAnoAnterior == null || metodologia.VrAprovadoAnoAnterior == 0)
                        {
                            metodologia.VrOrcado = 0;
                        }
                        else if (metodologia.PrAplicado == null || metodologia.PrAplicado == 0)
                        {
                            metodologia.VrOrcado = metodologia.VrAprovadoAnoAnterior;
                        }
                        else
                        {
                            metodologia.VrOrcado = metodologia.VrAprovadoAnoAnterior * (1 + metodologia.PrAplicado / 100);
                        }
                    }

                    InsereAno(metodologia);
                    lstNovasMetodologias.Add(metodologia);
                    
                    InsereMeses(metodologia, metodologiaAnoAnterior, meses);
                };

                trans.Complete();
            }

            NotificaNovoResponsavel(lstNovasMetodologias, false, (user as CreaspUser).IdPessoa);
        }

        /// <summary>
        /// Atualiza os valores de todas as metodologias existentes do ano.
        /// </summary>
        /// <param name="nrAno">Número do ano em que as metodologias serão atualizadas.</param>
        /// <param name="prAplicadoReceita">Percentual que será aplicado (multiplicado) no valor do ano anterior.</param>
        /// <param name="prCotaParteConfea">Percentual que será igual em todas as metodologias.</param>
        /// <param name="prCotaParteMutua">Percentual que será igual em todas as metodologias.</param>
        /// <param name="noObservacoesReceitaPadrao">Observações que serão iguais em todas as metodologias de receita.</param>
        public void AtualizaMetodologias(int nrAno, decimal prAplicadoReceita, decimal prCotaParteConfea, decimal prCotaParteMutua, string noObservacoesPadraoReceita, int? idReformulacao)
        {
            using (var trans = db.GetTransaction())
            {
                var metodologias = db.Query<MetodologiaReceitaAno>()
                    .IncludeMany(x => x.MetodologiasReceitaMes)
                    .Where(x => x.IdReformulacao == idReformulacao)
                    .Where(x => x.NrAno == nrAno)
                    .ToList();

                if (idReformulacao.HasValue)
                {
                    var reformulacao = new ReformulacaoService(context).BuscaReformulacao(idReformulacao.Value);

                    reformulacao.PrAplicadoReceita = prAplicadoReceita;
                    reformulacao.PrCotaParteConfea = prCotaParteConfea;
                    reformulacao.PrCotaParteMutua = prCotaParteMutua;
                    reformulacao.NoObservacaoReceitaPadrao = noObservacoesPadraoReceita;

                    db.AuditUpdate(reformulacao);
                }

                foreach (var m in metodologias)
                {
                    m.PrAplicado = prAplicadoReceita;
                    m.PrCotaParteConfea = prCotaParteConfea;
                    m.PrCotaParteMutua = prCotaParteMutua;
                    m.NoObservacoes = noObservacoesPadraoReceita;
                    m.CalculaVrOrcado(prAplicadoReceita);

                    foreach (var mes in m.MetodologiasReceitaMes)
                    {
                        mes.CalculaVrReceitaBruta(m.VrOrcado);
                        mes.CalculaVrReceitaLiquida(m.VrOrcado, m.PrCotaParteConfea, m.PrCotaParteMutua);
                    }

                    SalvaAnoEMeses(m);
                }

                trans.Complete();
            }
        }

        /// <summary>
        /// Atualiza o campo observações de receitas de todas as metodologias existentes do ano.
        /// </summary>
        public void AtualizaObservacoesMetodologias(int nrAno, string noObservacoesPadraoReceita)
        {
            using (var trans = db.GetTransaction())
            {
                var metodologias = db.Query<MetodologiaReceitaAno>()
                    .IncludeMany(x => x.MetodologiasReceitaMes)
                    .Where(x => x.NrAno == nrAno)
                    .ToList();

                foreach (var m in metodologias)
                {
                    m.NoObservacoes = noObservacoesPadraoReceita;

                    foreach (var mes in m.MetodologiasReceitaMes)
                    {
                        mes.CalculaVrReceitaBruta(m.VrOrcado);
                        mes.CalculaVrReceitaLiquida(m.VrOrcado, m.PrCotaParteConfea, m.PrCotaParteMutua);
                    }

                    db.AuditUpdate(m);
                }

                trans.Complete();
            }
        }

        /// <summary>
        /// Valida e salva uma metodologia com seus respectivos meses.
        /// </summary>
        /// <param name="metodologiaAno"></param>
        public void ValidaESalva(MetodologiaReceitaAno metodologiaAno)
        {
            Valida(metodologiaAno);
            SalvaAnoEMeses(metodologiaAno);
        }

        /// <summary>
        /// Insere um ano como metodologia no banco de dados.
        /// </summary>
        /// <param name="ano"></param>
        public void InsereAno(MetodologiaReceitaAno ano)
        {
            db.Insert(ano);
        }

        /// <summary>
        /// Insere uma lista de meses no banco de dados.
        /// </summary>
        /// <param name="meses"></param>
        public void InsereMeses(MetodologiaReceitaAno metodologia, MetodologiaReceitaAno metodologiaAnoAnterior, string[] meses)
        {
            MetodologiaReceitaMes mesAnterior;

            foreach (var mes in meses)
            {
                var metodologiaMes = new MetodologiaReceitaMes();

                metodologiaMes.IdMetodologiaReceitaAno = metodologia.IdMetodologiaReceitaAno;
                metodologiaMes.NoMes = mes;

                if ((mesAnterior = metodologiaAnoAnterior?.MetodologiasReceitaMes.Where(x => x.NoMes == mes).SingleOrDefault()) != null)
                {
                    metodologiaMes.PrAplicado = mesAnterior.PrAplicado;
                    metodologiaMes.CalculaVrReceitaBruta(metodologia.VrOrcado);
                    metodologiaMes.CalculaVrReceitaLiquida(metodologia.VrOrcado, metodologia.PrCotaParteConfea, metodologia.PrCotaParteMutua);
                }
                else
                {
                    metodologiaMes.PrAplicado = 0;
                    metodologiaMes.VrReceitaBruta = 0;
                    metodologiaMes.VrReceitaLiquida = 0;
                }

                db.Insert(metodologiaMes);
            }
        }

        /// <summary>
        /// Disponibiliza o conjunto de metodologias de receita a um próximo responsável.
        /// </summary>
        public void DisponibilizaMetodologiasReceitaAoProximoResponsavel(int nrAno, int? idReformulacao = null)
        {
            var metodologias = BuscaMetodologias(nrAno, idReformulacao);
            

            if (metodologias.Count == 0)
            {
                throw new NBoxException("Nenhuma metodologia de receita encontrada.");
            }

            if (metodologias[0].CdStatusPropostaOrcamentaria == StPropostaOrcamentaria.ContabValidacao)
            {
                var vrOrcadoAnoReceita = Math.Round(BuscaTotalOrcadoAno(), 2);
                var vrOrcadoAnoDespesa = Math.Round(new PropostaOrcamentariaDespesaService(context).BuscaTotalOrcadoAno(), 2);

                if (vrOrcadoAnoDespesa != vrOrcadoAnoReceita) throw new NBoxException($"Os valores de despesa não são iguais a receita. <br> Despesa: { vrOrcadoAnoDespesa.ToString("n2") } <br> Receita: { vrOrcadoAnoReceita.ToString("n2") }");
            }

            Pessoa pessoaRemetente = null;

            if (metodologias[0].IdPessoaResp.HasValue)
            {
                pessoaRemetente = new PessoaService(context).BuscaPessoa(metodologias[0].IdPessoaResp.Value, false);
            }

            var statusService = new StatusPropostaOrcamentariaService(context);
            using (var trans = db.GetTransaction())
            {
                foreach (var m in metodologias)
                {
                    //SalvaHistorico(m);
                    statusService.ProximoStatus(m);
                }

                SalvaApenasAnos(metodologias);

                trans.Complete();
            }

            NotificaNovoResponsavel(metodologias, false, pessoaRemetente?.IdPessoa);

            //}
        }

        /// <summary>
        /// Disponibiliza o conjunto de metodologias de receita a um responsável anterior.
        /// </summary>
        public void DisponibilizaMetodologiasReceitaAoResponsavelAnterior(int nrAno, int? idReformulacao = null)
        {
            //if (!VerificaSeUsuarioPodeAlterarProposta(idPropostaOrcamentaria))
            //{
            //    throw new NBoxException("Você não tem permissão para fazer isto.");
            //}

            //var propostas = BuscaPropostaOrcamentariaESeusFormularios(idPropostaOrcamentaria);
            var metodologias = BuscaMetodologias(nrAno, idReformulacao);

            if (metodologias.Count == 0)
            {
                throw new NBoxException("Nenhuma metodologia de receita encontrada.");
            }

            Pessoa pessoaRemetente = null;

            if (metodologias[0].IdPessoaResp.HasValue)
            {
                pessoaRemetente = new PessoaService(context).BuscaPessoa(metodologias[0].IdPessoaResp.Value, false);
            }

            var statusService = new StatusPropostaOrcamentariaService(context);

            using (var trans = db.GetTransaction())
            {
                foreach (var m in metodologias)
                {
                    //SalvaHistorico(m);

                    //proposta.PropostaOrcamentariaItens?.ForEach(x => x.VrOrcado = x.VrOrcadoAntesFinalizar);

                    statusService.StatusAnterior(m);

                    //Salva(m);
                }

                SalvaApenasAnos(metodologias);

                trans.Complete();
            }

            NotificaNovoResponsavel(metodologias, true, pessoaRemetente?.IdPessoa);

            //NotificaNovoResponsavel(proposta, true);
            //}
        }

        /// <summary>
        /// Notifica o novo responsável pelas metodologias via e-mail.
        /// </summary>
        public void NotificaNovoResponsavel(List<MetodologiaReceitaAno> metodologias, bool reprovacao, int? idPessoaRespAnterior)
        {
            if (metodologias[0].CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.PlenarioOK)
            {
                var pessoaService = new PessoaService(context);
                var pDestinataria = pessoaService.BuscaPessoa(metodologias[0].IdPessoaResp.Value, false);

                if (pDestinataria == null)
                {
                    throw new NBoxException("Destinatário não encontrado.");
                }

                //Se não houver e-mail cadastrado ou a pessoa não aceitar receber e-mail (conforme cadastro), não envia o e-mail.
                if (string.IsNullOrEmpty(pDestinataria.NoEmail) || !pDestinataria.FgAceitaEmail)
                {
                    return;
                    //throw new NBoxException("Destinatário sem e-mail cadastrado. A proposta foi alterada, mas ele não foi informado.");
                }

                var pRemetente = pessoaService.BuscaPessoa(idPessoaRespAnterior.Value, false);

                if (string.IsNullOrEmpty(pRemetente.NoEmail))
                {
                    return;
                    //throw new NBoxException("Remetente sem e-mail cadastrado. A proposta foi alterada, mas o novo responsável não foi informado.");
                }

                string assunto = string.Format("CreaSP (ELO): As metodologias de receita do ano de {0} estão com você!", metodologias[0].NrAno);
                string msg = string.Format("{0} {1} as metodologias de receita para você.",
                    pRemetente != null ? pRemetente.NoPessoa : "Seu colega",
                    reprovacao ? "retornou" : "enviou");

                NBox.Services.Email.New()
                                    .To(pDestinataria.NoEmail, pDestinataria.NoPessoa)
                                    .ReplyTo(pRemetente?.NoEmail ?? "")
                                    .Subject(assunto)
                                    .Body(msg)
                                    .Send();

                new EmailService(context).RegistraEnvioDeEmail(pDestinataria.IdPessoa, pDestinataria.NoEmail, assunto, msg);
            }
        }

        /// <summary>
        /// Verifica se usuário logado pode alterar a metodologia
        /// </summary>
        public bool VerificaSeUsuarioPodeAlterarMetodologia(int idMetodologiaReceitaAno)
        {
            var metodologia = BuscaMetodologiaReceita(idMetodologiaReceitaAno);

            if (metodologia.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.ContabValidacao
                && metodologia.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.Plenario
                && metodologia.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.PlenarioOK)
            {

                if (metodologia.IdPessoaResp.HasValue && user != null)
                {
                    List<int?> subDelegacoes = new PessoaService(context).ListaDelegacoes((user as CreaspUser).IdPessoa).ToList();
                    return subDelegacoes.Contains(metodologia.IdPessoaResp);
                }
            }

            return false;
        }

        /// <summary>
        /// Verifica se usuário logado pode alterar a metodologia de reformulação
        /// </summary>
        public bool VerificaSeUsuarioPodeAprovarMetodologia(int? idReformulacao)
        {
            var metodologias = BuscaMetodologias(nrAnoBase, idReformulacao);

            List<int?> subDelegacoes = new PessoaService(context).ListaDelegacoes((user as CreaspUser).IdPessoa).ToList();

            return metodologias.All(x => subDelegacoes.Contains(x.IdPessoaResp)) && metodologias.All(x => x.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.PlenarioOK);
        }

        /// <summary>
        /// Verifica se usuároi pode reprovar a metodologia, seja ela de reformulação ou não.
        /// </summary>
        public bool VerificaSeUsuarioPodeReprovarMetodologia(int? idReformulacao)
        {
            var metodologias = BuscaMetodologias(nrAnoBase, idReformulacao);

            List<int?> subDelegacoes = new PessoaService(context).ListaDelegacoes((user as CreaspUser).IdPessoa).ToList();

            return metodologias.All(x => subDelegacoes.Contains(x.IdPessoaResp)) && metodologias.All(x => x.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.Executor);
        }

        /// <summary>
        /// Modifica o número da conta contábil de todas metodologias bucando a conta 6 referentes às contas 5.
        /// </summary>
        /// <returns>Retorna a lista de erros, contas contábeis iniciadas por 5 se contas de referência iniciadas por 6.</returns>
        public List<string> DeParaTodasContas5para6(List<MetodologiaReceitaAno> metodologias)
        //private void DeParaTodasContas5para6(List<MetodologiaReceitaAno> metodologias)
        {
            var contaCoMapeamentoService = new ContaContabilMapeamentoService(context);
            var contasContabeis = contaCoMapeamentoService.BuscaContasContabeisMapeadas();

            var lstContas5sem6 = new List<string>();

            string nrContaContabil6 = string.Empty;

            for (int i = 0; i < metodologias.Count; i++)
            {
                nrContaContabil6 = contaCoMapeamentoService.DePara5para6(contasContabeis, metodologias[i].ContaContabil.NrContaContabil);
                if (!string.IsNullOrEmpty(nrContaContabil6))
                {
                    metodologias[i].ContaContabil.NrContaContabil = contaCoMapeamentoService.DePara5para6(contasContabeis, metodologias[i].ContaContabil.NrContaContabil);
                }
                else
                {
                    lstContas5sem6.Add(metodologias[i].ContaContabil.NrContaContabilFmt);
                }
            }

            return lstContas5sem6.Distinct().ToList();
        }

        #endregion

        #region Private

        /// <summary>
        /// Valida se o ano pode ser salvo.
        /// </summary>
        private void Valida(MetodologiaReceitaAno metodologiaAno)
        {
            if (metodologiaAno.PrCotaParteMutua.To<decimal>() != 0 && metodologiaAno.PrSaoPaulo.To<decimal>() + metodologiaAno.PrBrasilia.To<decimal>() != 100)
            {
                throw new NBoxException("O % total das cotas parte mútuas (São Paulo e Brasília) deve ser 100%.");
            }

            if (metodologiaAno.VrOrcado != 0 && metodologiaAno.MetodologiasReceitaMes.Sum(x => x.PrAplicado) != 100)
            {
                throw new NBoxException("O % total dos meses deve ser 100%.");
            }
        }

        /// <summary>
        /// Salva o anos e os meses das metodologias, além de atualizar os valores das propostas orçamentárias referente as "Cota-parte" Mútua e Confea.
        /// </summary>
        /// <param name="metodologiaAno"></param>
        private void SalvaAnoEMeses(MetodologiaReceitaAno metodologiaAno)
        {
            using (var trans = db.GetTransaction())
            {
                db.AuditUpdate(metodologiaAno);

                foreach (var mes in metodologiaAno.MetodologiasReceitaMes)
                {
                    db.AuditUpdate(mes);
                }

                new PropostaOrcamentariaDespesaService(context).AtualizaTransposicaoCotaParteMutuaConfea();

                trans.Complete();
            }
        }

        /// <summary>
        /// Salva apenas os anos das metodologias.
        /// </summary>
        /// <param name="metodologiasAno"></param>
        private void SalvaApenasAnos(List<MetodologiaReceitaAno> metodologiasAno)
        {
            using (var trans = db.GetTransaction())
            {
                foreach (var ano in metodologiasAno)
                {
                    db.AuditUpdate(ano);
                }

                trans.Complete();
            }
        }

        #endregion
    }
}
