﻿using Creasp.Core;
using NBox.Core;
using NBox.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Creasp.Elo
{
    public class PropostaOrcamentariaDespesaService : DataService
    {
        protected readonly int nrAnoBase;

        public PropostaOrcamentariaDespesaService(ServiceContext context) : base(context)
        {
            nrAnoBase = (context as CreaspContext).NrAnoBase;
        }

        //private List<DotacaoOrcamentaria> _dotacoesAnteriores;
        private List<PosicaoAtualOrcamento> _posicaoAtualOrcamento;
        private List<OrcamentoSenior> _orcamentosSenior;
        private List<ContaContabil_CentroCusto> _relacaoContaCentro;

        /// <summary>
        /// Lista todos as propostas orçamentárias no grid. Permite filtros por nome de centro de custo e nome de responsável.
        /// </summary>
        public QueryPage<PropostaOrcamentariaDespesa> ListaPropostaOrcamentariaAnoAtual(Pager p,
            string nrCentroCusto = null,
            string noCentroCusto = null,
            string noResponsavel = null,
            string cdStatusProposta = null,
            string cdTipoFormulario = null)
        {
            var pessoaService = new PessoaService(context);

            bool isContabilidade = new CentroCustoPerfilService(context).ValidaUsuarioLogadoPossuiPerfil(TpPerfilCentroCusto.Contabilidade);
            int idUsuarioLogado = user != null ? (user as CreaspUser).IdPessoa : 0;

            List<int?> subDelegacoes = pessoaService.ListaDelegacoes(idUsuarioLogado).ToList();

            var propostas = db.Query<PropostaOrcamentariaDespesa>()
                .Include(x => x.CentroCustoOriginal)
                .Include(x => x.CentroCusto)
                //.Include(x => x.CentroCusto.CentrosCustosResponsaveis)
                .Include(x => x.Responsavel)
                .Include(x => x.StatusPropostaOrcamentaria)
                //.Include(x => x.Status)
                //.IncludeMany(x => x.PropostaOrcamentariaItens)
                .Where(x => x.NrAno == nrAnoBase)
                .Where(nrCentroCusto != null, x => x.CentroCusto.NrCentroCusto.StartsWith(nrCentroCusto.UnMask()))
                .Where(noCentroCusto != null, x => x.CentroCusto.NoCentroCusto.Contains(noCentroCusto.ToLike()))
                .Where(noResponsavel != null, x => x.Responsavel.NoPessoa.StartsWith(noResponsavel.ToLike()))
                .Where(cdStatusProposta != null, x => x.StatusPropostaOrcamentaria.CdStatusPropostaOrcamentaria == cdStatusProposta)
                .Where(cdTipoFormulario != null, x => x.CdTpFormulario == cdTipoFormulario)
                .ToList();
            
            var ccrService = new CentroCustoResponsavelService(context);
            propostas.ForEach(x => x.CentroCusto.CentroCustoResponsavel = ccrService.BuscaCentroCustoResponsavel(x.IdCentroCusto, null));

            var pagedPropostas = propostas.Where(x => isContabilidade
                || subDelegacoes.Contains(x.CentroCusto.CentroCustoResponsavel.IdPessoaExecutor)
                || subDelegacoes.Contains(x.CentroCusto.CentroCustoResponsavel.IdPessoaAprovador1)
                || subDelegacoes.Contains(x.CentroCusto.CentroCustoResponsavel.IdPessoaAprovador2)
                || subDelegacoes.Contains(x.CentroCusto.CentroCustoResponsavel.IdPessoaAprovador3)
                || subDelegacoes.Contains(x.CentroCusto.CentroCustoResponsavel.IdPessoaAprovador4)
                || subDelegacoes.Contains(x.Responsavel.IdPessoa))
                .ToPage(p.DefaultSort<PropostaOrcamentariaDespesa>(x => x.CentroCusto.NrCentroCusto));

            var itens = db.Query<PropostaOrcamentariaDespesaItem>()
                .Include(x => x.PropostaOrcamentariaDespesa)
                .Where(x => x.PropostaOrcamentariaDespesa.NrAno == nrAnoBase)
                .Where(x => x.NrVersao == 0)
                .ToList();

            pagedPropostas.Items.ForEach(x => x.PropostaOrcamentariaItens = itens.Where(y => y.IdPropostaOrcamentariaDespesa == x.IdPropostaOrcamentariaDespesa).ToList());

            return pagedPropostas;
        }

        /// <summary>
        /// Busca uma proposta orçamentária pelo seu ID.
        /// </summary>
        public PropostaOrcamentariaDespesa BuscaPropostaOrcamentaria(int idPropostaOrcamentaria)
        {
            var proposta = db.Query<PropostaOrcamentariaDespesa>()
                .Include(x => x.CentroCusto)
                .Include(x => x.CentroCustoOriginal)
                //.Include(x => x.CentroCusto.CentroCustoResponsavel)
                //.Include(x => x.CentroCusto.CentroCustoResponsavel.ResponsavelExecucao)
                //.Include(x => x.CentroCusto.CentroCustoResponsavel.ResponsavelAprova)
                //.Include(x => x.CentroCusto.CentroCustoResponsavel.ResponsavelAprova2)
                //.Include(x => x.CentroCusto.CentroCustoResponsavel.ResponsavelAprova3)
                //.Include(x => x.CentroCusto.CentroCustoResponsavel.ResponsavelAprova4)
                .Include(x => x.Responsavel)
                .Include(x => x.StatusPropostaOrcamentaria)
                .IncludeMany(x => x.PropostaOrcamentariaItens)
                .Where(x => x.IdPropostaOrcamentariaDespesa == idPropostaOrcamentaria)
                .Single();

            var ccrService = new CentroCustoResponsavelService(context);
            proposta.CentroCusto.CentroCustoResponsavel = ccrService.BuscaCentroCustoResponsavel(proposta.IdCentroCusto, null);

            return proposta;
        }

        /// <summary>
        /// Busca uma proposta orçamentária pelo seu número de centro de custo, desconsiderando formulários.
        /// </summary>
        public PropostaOrcamentariaDespesa BuscaPropostaOrcamentaria(string nrCentroCusto)
        {
            var proposta = db.Query<PropostaOrcamentariaDespesa>()
                .Include(x => x.CentroCusto)
                //.Include(x => x.CentroCusto.CentroCustoResponsavel)
                //.Include(x => x.CentroCusto.CentroCustoResponsavel.ResponsavelExecucao)
                //.Include(x => x.CentroCusto.CentroCustoResponsavel.ResponsavelAprova)
                //.Include(x => x.CentroCusto.CentroCustoResponsavel.ResponsavelAprova2)
                //.Include(x => x.CentroCusto.CentroCustoResponsavel.ResponsavelAprova3)
                //.Include(x => x.CentroCusto.CentroCustoResponsavel.ResponsavelAprova4)
                .Include(x => x.Responsavel)
                .Include(x => x.StatusPropostaOrcamentaria)
                .IncludeMany(x => x.PropostaOrcamentariaItens)
                .Where(x => x.CentroCusto.NrCentroCusto == nrCentroCusto)
                .Where(x => x.NrAno == nrAnoBase)
                //.Where(x => string.IsNullOrEmpty(x.CdTpFormulario))
                .SingleOrDefault();

            var ccrService = new CentroCustoResponsavelService(context);
            proposta.CentroCusto.CentroCustoResponsavel = ccrService.BuscaCentroCustoResponsavel(proposta.IdCentroCusto, null);

            if (proposta == null)
            {
                throw new NBoxException($"Nenhuma proposta orçamentária para o centro de custo { nrCentroCusto }.");
            }

            return proposta;
        }

        /// <summary>
        /// Valida e inclui um formulário para um centro de custo.
        /// </summary>
        public void ValidaEIncluiFormulario(string cdTpFormulario, string nrCentroCusto)
        {
            if (string.IsNullOrWhiteSpace(cdTpFormulario)) throw new ArgumentNullException("cdTpFormulario");
            if (string.IsNullOrWhiteSpace(nrCentroCusto)) throw new ArgumentNullException("nrCentroCusto");

            if (VerificaSeJaExisteFormulario(cdTpFormulario, nrCentroCusto))
            {
                throw new NBoxException("Formulário já existente");
            }

            var p = new PropostaOrcamentariaDespesa();

            var centroCustoService = new CentroCustoService(context);
            var pessoaService = new PessoaService(context);

            p.IdCentroCusto = centroCustoService.BuscaCentroCusto(nrCentroCusto).IdCentroCusto;
            p.IdCentroCustoOriginal = p.IdCentroCusto;
            p.IdPessoaResp = (user as CreaspUser).IdPessoa;
            p.NrAno = nrAnoBase;
            p.CdStatusPropostaOrcamentaria = StPropostaOrcamentaria.Executor;
            p.CdTpFormulario = cdTpFormulario;

            db.Insert(p);
        }

        /// <summary>
        /// Verifica se já existe um formulário de um determinado tipo para um centro de custo específico.
        /// </summary>
        private bool VerificaSeJaExisteFormulario(string cdTpFormulario, string nrCentroCusto)
        {
            return db.Query<PropostaOrcamentariaDespesa>()
                .Include(x => x.CentroCustoOriginal)
                .Where(x => x.NrAno == nrAnoBase)
                .Where(x => x.CentroCustoOriginal.NrCentroCusto == nrCentroCusto)
                .Where(x => x.CdTpFormulario == cdTpFormulario)
                .Any();
        }

        /// <summary>
        /// Criar as propostas orçamentárias, buscando dados de dotações orçamentárias (dados importados) 
        /// e da relação entre conta contábil e centro de custo.
        /// </summary>
        public void CriaPropostasOrcamentarias(ParametroAno ano)
        {
            var dotacaoService = new OrcamentoService(context);
            var relacaoContaCentroService = new ContaContabilCentroCustoService(context);
            var centroCustoResponsavelService = new CentroCustoResponsavelService(context);

            var relacaoContaCentro = relacaoContaCentroService.ListaRelacaoContaContabilComCentroCustoOrcavel().OrderBy(x => x.IdCentroCusto).ToList();
            var centrosDeCustoResponsaveis = centroCustoResponsavelService.ListaCentrosCustosOrcaveis().OrderBy(x => x.IdCentroCusto).ToList();

            using (var trans = db.GetTransaction())
            {
                foreach (var centro in centrosDeCustoResponsaveis)
                {
                    if (centro.IdPessoaExecutor.HasValue)
                    {
                        IncluiPropostaOrcamentaria(ano, centro, true);
                    }
                }

                trans.Complete();
            }

            var propostas = db.Query<PropostaOrcamentariaDespesa>()
                    .Include(x => x.CentroCusto)
                    //.IncludeMany(x => x.PropostaOrcamentariaItens)
                    .Where(x => x.NrAno == ano.NrAno)
                    .ToList();

            int idUsuarioLogado = (user as CreaspUser).IdPessoa;

            propostas.ForEach(x => NotificaNovoResponsavel(x, false, idUsuarioLogado));
        }

        /// <summary>
        /// Valida e inclui proposta orçamentária para um centro de custo específico.
        /// </summary>
        public void ValidaEIncluiPropostaOrcamentaria(int nrAno, string nrCentroCusto)
        {
            var parametroAnoService = new ParametroAnoService(context);
            var centroCustoResponsavelService = new CentroCustoResponsavelService(context);

            var parametroAno = parametroAnoService.BuscaParametroAno(nrAno);
            var centroCustoResponsavel = centroCustoResponsavelService.BuscaCentroCustoResponsavel(nrCentroCusto);

            IncluiPropostaOrcamentaria(parametroAno, centroCustoResponsavel, true);
        }

        /// <summary>
        /// Inclui uma proposta orçamentária, definindo responsável e preenchendo valores informativos (se houver).
        /// </summary>
        public void IncluiPropostaOrcamentaria(ParametroAno ano, CentroCustoResponsavel centroDeCustoResponsavel, bool notificaResponsavel)
        {
            PropostaOrcamentariaDespesa proposta = new PropostaOrcamentariaDespesa();

            using (var trans = db.GetTransaction())
            {
                var dotacaoService = new OrcamentoService(context);
                var relacaoContaCentroService = new ContaContabilCentroCustoService(context);
                var centroCustoResponsavelService = new CentroCustoResponsavelService(context);

                //if (_dotacoesAnteriores == null) _dotacoesAnteriores = dotacaoService.ListaDotacoes(ano.NrAno - 1, ano.NrAno - 1, false);
                if (_posicaoAtualOrcamento == null) _posicaoAtualOrcamento = dotacaoService.ListaPosicaoAtualOrcamento(ano.NrAno - 1, ano.NrAno - 1);
                if (_orcamentosSenior == null) _orcamentosSenior = dotacaoService.ListaOrcamentoSenior(ano.NrAno);
                if (_relacaoContaCentro == null) _relacaoContaCentro = relacaoContaCentroService.ListaRelacaoContaContabilComCentroCustoOrcavel().OrderBy(x => x.IdCentroCusto).ToList();
                //if (_centrosDeCustoResponsaveis == null) _centrosDeCustoResponsaveis = centroCustoResponsavelService.ListaCentroCustoOrcaveis().OrderBy(x => x.IdCentroCusto).ToList();

                proposta.NrAno = ano.NrAno;
                proposta.IdCentroCusto = centroDeCustoResponsavel.IdCentroCusto;
                proposta.IdCentroCustoOriginal = centroDeCustoResponsavel.IdCentroCusto;
                proposta.CdStatusPropostaOrcamentaria = StPropostaOrcamentaria.Executor;
                proposta.IdPessoaResp = centroDeCustoResponsavel.ResponsavelExecucao?.IdPessoa;

                db.Insert(proposta);

                foreach (var contaContabilCentroCusto in _relacaoContaCentro.Where(x => x.IdCentroCusto == centroDeCustoResponsavel.IdCentroCusto))
                {
                    var orcamentoSenior = _orcamentosSenior
                        .Where(x => x.NrAno == ano.NrAno)
                        .Where(x => ("0" + x.NrCentroCusto).StartsWith(centroDeCustoResponsavel.CentroCusto.NrCentroCusto))
                        .Where(x => x.NrContaContabil5 == contaContabilCentroCusto.ContaContabil.NrContaContabil)
                        .Sum(x => x.VrOrcamento);

                    //var dotacaoAnoAnterior = _dotacoesAnteriores
                    //    .Where(x => x.NrAno == ano.NrAno - 1)
                    //    .Where(x => x.NrCentroCusto == centroDeCustoResponsavel.CentroCusto.NrCentroCusto)
                    //    .Where(x => x.NrContaContabil == contaContabilCentroCusto.ContaContabil.NrContaContabil)
                    //    .SingleOrDefault();

                    var posicaoOrcamento = _posicaoAtualOrcamento
                        .Where(x => x.NrAno == ano.NrAno - 1)
                        .Where(x => x.NrCentroCusto == centroDeCustoResponsavel.CentroCusto.NrCentroCusto)
                        .Where(x => x.NrContaContabil5 == contaContabilCentroCusto.ContaContabil.NrContaContabil)
                        .SingleOrDefault();

                    PropostaOrcamentariaDespesaItem propostaItem = new PropostaOrcamentariaDespesaItem();

                    propostaItem.IdPropostaOrcamentariaDespesa = proposta.IdPropostaOrcamentariaDespesa;
                    propostaItem.PropostaOrcamentariaDespesa = proposta;
                    propostaItem.IdContaContabil = contaContabilCentroCusto.IdContaContabil;
                    propostaItem.VrAprovadoAnoAnterior = posicaoOrcamento?.VrEmpExercicio + posicaoOrcamento?.VrSaldoOrcamentarioExercicio;
                    propostaItem.VrExecutadoAnoAnterior = posicaoOrcamento?.VrExecutado;
                    propostaItem.DtRefVrExecutado = posicaoOrcamento?.DtRefVrExecutado;
                    propostaItem.NrVersao = 0;

                    //Tenta pegar os valores nessa ordem: orçado pela senior; aprovado para o último ano multiplicado pelo percentual de despesa; apenas aprovado para o último ano; 0.
                    if (orcamentoSenior != 0)
                    {
                        propostaItem.VrOrcado = orcamentoSenior;
                        propostaItem.FgVrOrcadoOrigemSenior = true;
                        propostaItem.NoEspecificacao = "Folha pagamento";
                        propostaItem.NoJustificativa = "Folha pagamento";
                    }
                    else
                    {
                        //Valor orçado é genérico ao "VrAprovadoAnoAnterior", neste caso é baseado no valor DOTAÇÃO ATUAL (valor do empenho + saldo orçamentário)
                        propostaItem.VrOrcado = propostaItem.VrAprovadoAnoAnterior.HasValue && ano.PrAplicadoDespesa.HasValue ? propostaItem.VrAprovadoAnoAnterior * (1 + ano.PrAplicadoDespesa / 100) :
                                                propostaItem.VrAprovadoAnoAnterior.HasValue ? propostaItem.VrAprovadoAnoAnterior : 0;

                        propostaItem.FgVrOrcadoOrigemSenior = false;
                    }

                    propostaItem.VrOrcadoAntesFinalizar = propostaItem.VrOrcado;

                    db.Insert(propostaItem);
                }

                trans.Complete();
            }

            if (notificaResponsavel)
            {
                proposta.CentroCusto = new CentroCustoService(context).BuscaCentroCusto(proposta.IdCentroCusto);
                NotificaNovoResponsavel(proposta, false, (user as CreaspUser).IdPessoa);
            }
        }

        /// <summary>
        /// Lista todas as propostas orçamentárias de um ano específico.
        /// </summary>
        public List<PropostaOrcamentariaDespesa> BuscaPropostasPorAno(int nrAno, bool apenasFormulario = false)
        {
            return db.Query<PropostaOrcamentariaDespesa>()
                .Include(x => x.CentroCusto)
                //.IncludeMany(x => x.PropostaOrcamentariaItens)
                .Where(x => x.NrAno == nrAno)
                .Where(apenasFormulario == true, x => x.CdTpFormulario == null)
                .ToList();
        }

        /// <summary>
        /// Envia todas as propostas orçamentárias de um ano específico para o próximo status.
        /// </summary>
        /// <param name="nrAno">Ano que as propostas estão.</param>
        public void DisponibilizaTodasPropostasParaProximoStatus(int nrAno)
        {
            using (var trans = db.GetTransaction())
            {
                var pessoaService = new PessoaService(context);
                var centroCustoPerfilService = new CentroCustoPerfilService(context);

                if (!centroCustoPerfilService.ValidaUsuarioLogadoPossuiPerfil(TpPerfilCentroCusto.Contabilidade))
                {
                    throw new NBoxException("Apenas a contabilidade pode fazer isto");
                }

                var propostas = BuscaPropostasPorAno(nrAno);

                if (propostas == null || propostas.Count == 0)
                {
                    throw new NBoxException("Nenhuma proposta orçamentária encontrada.");
                }

                if (!VerificaSeTodasPropostasTemMesmoStatus(nrAno, propostas.FirstOrDefault().CdStatusPropostaOrcamentaria))
                {
                    throw new NBoxException("Todas propostas precisam ter o mesmo status");
                }

                foreach (var p in propostas)
                {
                    DisponibilizaPropostaAoProximoResponsavel(p.IdPropostaOrcamentariaDespesa, false);
                }

                trans.Complete();
            }
        }

        /// <summary>
        /// Envia todas as propostas orçamentárias de um ano específico para o status anterior.
        /// </summary>
        /// <param name="nrAno">Ano que as propostas estão.</param>
        public void DisponibilizaTodasPropostasParaStatusAnterior(int nrAno)
        {
            using (var trans = db.GetTransaction())
            {
                var pessoaService = new PessoaService(context);
                var centroCustoPerfilService = new CentroCustoPerfilService(context);

                if (!centroCustoPerfilService.ValidaUsuarioLogadoPossuiPerfil(TpPerfilCentroCusto.Contabilidade))
                {
                    throw new NBoxException("Apenas a contabilidade pode fazer isto.");
                }

                var propostas = BuscaPropostasPorAno(nrAno);

                if (propostas == null || propostas.Count == 0)
                {
                    throw new NBoxException("Nenhuma proposta orçamentária encontrada.");
                }

                //Pega o status da primeira proposta...
                string cdStatusAtual = propostas.FirstOrDefault().CdStatusPropostaOrcamentaria;

                //... que deverá ser igual ao status de todas outras propostas.
                if (!VerificaSeTodasPropostasTemMesmoStatus(nrAno, cdStatusAtual))
                {
                    throw new NBoxException("Todas propostas precisam ter o mesmo status.");
                }

                //Se o status "antigo" for "Ag. Validação Contabilidade"
                if (cdStatusAtual == StPropostaOrcamentaria.ContabValidacao)
                {
                    //Salva uma nova versão da proposta orçamentária.
                    SalvaNovaVersao(nrAno);
                }

                //Por fim, pra cada proposta, passa ela para o responsável anterior.
                foreach (var p in propostas)
                {
                    DisponibilizaPropostaAoResponsavelAnterior(p.IdPropostaOrcamentariaDespesa, false, string.Empty);
                }

                trans.Complete();
            }
        }

        /// <summary>
        /// Verifica se todas as propostas orçamentárias de um mesmo ano possuem o mesmo status. 
        /// </summary>
        public bool VerificaSeTodasPropostasTemMesmoStatus(int nrAno, string cdPropostaOrcamentaria)
        {
            var propostas = BuscaPropostasPorAno(nrAno);

            return propostas.All(x => x.CdStatusPropostaOrcamentaria == cdPropostaOrcamentaria);
        }

        /// <summary>
        /// Lista todos os status das propostas orçamentárias do ano corrente. Sem repetições na lista.
        /// </summary>
        public List<string> ListaStatusDasPropostas(int nrAno)
        {
            var propostas = BuscaPropostasPorAno(nrAno);
            return propostas.Select(x => x.CdStatusPropostaOrcamentaria).Distinct().ToList();
        }

        /// <summary>
        /// Valida a proposta orçamentária, salva os valores alterados como históricos, altera o status / responsável e notifica via e-mail o novo responsável.
        /// </summary>
        public void DisponibilizaPropostaAoProximoResponsavel(int idPropostaOrcamentaria, bool notificaResponsavel)
        {
            if (!PodeMoverProposta(idPropostaOrcamentaria))
            {
                throw new NBoxException("Você não tem permissão para fazer isto.");
            }

            var proposta = BuscaPropostaOrcamentaria(idPropostaOrcamentaria);
            int idPessoaRespAnterior = proposta.IdPessoaResp.HasValue ? proposta.IdPessoaResp.Value : 0;

            using (var trans = db.GetTransaction())
            {
                proposta.PropostaOrcamentariaItens = new PropostaOrcamentariaDespesaService(context).ListaPropostaOrcamentariaItens(proposta.IdPropostaOrcamentariaDespesa);

                if (proposta.PropostaOrcamentariaItens.Count > 0)
                {
                    if (!ValidaEspecificacoes(proposta.PropostaOrcamentariaItens))
                    {
                        throw new NBoxException("Todas as especificações precisam estar preenchidas.");
                    }

                    if (!ValidaJustificativas(proposta.PropostaOrcamentariaItens))
                    {
                        throw new NBoxException("Todas as justificativas precisam estar preenchidas.");
                    }

                    SalvaHistorico(proposta);

                    proposta.PropostaOrcamentariaItens?.ForEach(x => x.VrOrcado = x.VrOrcadoAntesFinalizar);
                }

                //Se o status anterior for "Ag. Validação Contabilidade"
                if (proposta.CdStatusPropostaOrcamentaria == StPropostaOrcamentaria.ContabValidacao && !string.IsNullOrEmpty(proposta.CdTpFormulario))
                {
                    //Preenche o centro de custo de acordo com o formulário.
                    //proposta.IdCentroCusto = new CentroCustoService(context).BuscaCentroCustoPelaConfiguracao(proposta.CdTpFormulario).IdCentroCusto;
                    proposta.IdCentroCusto = new CentroCustoPerfilService(context).BuscaSeHouverCentroCustoResponsavel(proposta.CdTpFormulario.ToUpper()).IdCentroCusto;
                }

                AtualizaResponsavelProximoStatus(proposta);

                SalvaProposta(proposta);

                trans.Complete();
            }

            if (notificaResponsavel)
            {
                NotificaNovoResponsavel(proposta, false, idPessoaRespAnterior);
            }
        }

        /// <summary>
        /// Valida a proposta orçamentária, salva os valores alterados como históricos, altera o status / responsável e notifica via e-mail o responsável anterior.
        /// </summary>
        public void DisponibilizaPropostaAoResponsavelAnterior(int idPropostaOrcamentaria, bool notificaResponsavel, string noMotivoRejeicao)
        {
            //TODO: (importante) Refatorar para melhorar performance deste método, pois agora é chamado dentro de um foreach em alguns casos.

            if (!PodeMoverProposta(idPropostaOrcamentaria))
            {
                throw new NBoxException("Você não tem permissão para fazer isto.");
            }

            var proposta = BuscaPropostaOrcamentaria(idPropostaOrcamentaria);
            int idPessoaRespAnterior = proposta.IdPessoaResp.HasValue ? proposta.IdPessoaResp.Value : 0;

            using (var trans = db.GetTransaction())
            {
                proposta.NoMotivoRejeicao = noMotivoRejeicao;

                if (proposta.PropostaOrcamentariaItens?.Count > 0)
                {
                    if (!ValidaEspecificacoes(proposta.PropostaOrcamentariaItens))
                    {
                        throw new NBoxException("Todas as especificações precisam estar preenchidas.");
                    }

                    if (!ValidaJustificativas(proposta.PropostaOrcamentariaItens))
                    {
                        throw new NBoxException("Todas as justificativas precisam estar preenchidas.");
                    }

                    SalvaHistorico(proposta);

                    proposta.PropostaOrcamentariaItens?.ForEach(x => x.VrOrcado = x.VrOrcadoAntesFinalizar);
                }

                //Se o status anterior for "Ag. Aprov. Plenáro"
                if (proposta.CdStatusPropostaOrcamentaria == StPropostaOrcamentaria.Plenario && !string.IsNullOrEmpty(proposta.CdTpFormulario))
                {
                    //Preenche o centro de custo com o ID do centro de custo que solicitou o formulário.
                    proposta.IdCentroCusto = proposta.IdCentroCustoOriginal;
                }

                AtualizaResponsavelStatusAnterior(proposta);

                SalvaProposta(proposta);

                trans.Complete();
            }

            if (notificaResponsavel)
            {
                NotificaNovoResponsavel(proposta, true, idPessoaRespAnterior);
            }
        }

        /// <summary>
        /// Exclui uma proposta orçamentária (em cascata com valores históricos e itens da proposta).
        /// </summary>
        /// <param name="idPropostaOrcamentaria"></param>
        public void ExcluiProposta(int idPropostaOrcamentaria)
        {
            using (var trans = db.GetTransaction())
            {
                var proposta = BuscaPropostaOrcamentaria(idPropostaOrcamentaria);

                if (proposta.PropostaOrcamentariaItens?.Count > 0)
                {
                    foreach (var item in proposta.PropostaOrcamentariaItens)
                    {
                        db.DeleteMany<PropostaOrcamentariaHistorico>()
                        .Where(x => x.IdPropostaOrcamentariaDespesaItem == item.IdPropostaOrcamentariaDespesaItem)
                        .Execute();
                    }
                }

                db.DeleteMany<PropostaOrcamentariaDespesaItem>()
                    .Where(x => x.IdPropostaOrcamentariaDespesa == idPropostaOrcamentaria)
                    //.Where(x => x.PropostaOrcamentariaDespesa.CdTpFormulario != null);
                    .Execute();

                db.Delete<PropostaOrcamentariaDespesa>(idPropostaOrcamentaria);

                trans.Complete();
            }
        }

        /// <summary>
        /// Busca o somatório do valor orçado de todas as propostas orçamentárias do ano.
        /// </summary>
        public decimal BuscaTotalOrcadoAno()
        {
            var soma = db.Query<PropostaOrcamentariaDespesaItem>()
                .Include(x => x.PropostaOrcamentariaDespesa)
                .Where(x => x.PropostaOrcamentariaDespesa.NrAno == nrAnoBase)
                .Where(x => x.NrVersao == 0)
                .ToList()?
                .Sum(x => x.VrOrcadoAntesFinalizar);

            return soma.HasValue ? soma.Value : 0;
        }

        /// <summary>
        /// Salva uma nova versão 
        /// </summary>
        private void SalvaNovaVersao(int nrAno)
        {
            using (var trans = db.GetTransaction())
            {
                var propostas = BuscaPropostasPorAno(nrAno);
                propostas.ForEach(x => x.PropostaOrcamentariaItens = ListaPropostaOrcamentariaItens(x.IdPropostaOrcamentariaDespesa, false));

                int? nrProximaVersao = propostas.Max(p => p.PropostaOrcamentariaItens.Max(y => (int?)y.NrVersao)) + 1;

                foreach (var p in propostas)
                {
                    foreach (var item in p.PropostaOrcamentariaItens.Where(x => x.NrVersao == 0))
                    {
                        item.NrVersao = nrProximaVersao.Value;
                        db.Insert(item);
                    }
                }

                trans.Complete();
            }
        }

        /// <summary>
        /// Salva a proposta orçamentária e seus itens.
        /// </summary>
        /// <param name="proposta"></param>
        private void SalvaProposta(PropostaOrcamentariaDespesa proposta)
        {
            using (var trans = db.GetTransaction())
            {
                if (proposta?.PropostaOrcamentariaItens?.Count > 0)
                {
                    foreach (var item in proposta.PropostaOrcamentariaItens)
                    {
                        db.Update(item);
                    }
                }

                if (proposta != null)
                {
                    db.Update(proposta);
                }

                trans.Complete();
            }
        }

        /// <summary>
        /// Salva os itens de uma proposta orçamentária como históricos.
        /// </summary>
        private void SalvaHistorico(PropostaOrcamentariaDespesa proposta)
        {
            using (var trans = db.GetTransaction())
            {
                if (proposta == null || proposta.PropostaOrcamentariaItens == null)
                {
                    throw new ArgumentNullException("Proposta, Proposta.PropostaOrcamentariaItens");
                }

                var historicoItens = proposta.PropostaOrcamentariaItens
                    //.Where(x => x.VrOrcadoAntesFinalizar != x.VrOrcado || proposta.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.ContabValidacao)
                    .Where(x => x.VrOrcadoAntesFinalizar != x.VrOrcado)
                    .Select(x => new PropostaOrcamentariaHistorico()
                    {
                        IdPropostaOrcamentariaDespesaItem = x.IdPropostaOrcamentariaDespesaItem,
                        CdStatusPropostaOrcamentaria = proposta.CdStatusPropostaOrcamentaria,
                        VrOrcado = x.VrOrcadoAntesFinalizar.HasValue ? x.VrOrcadoAntesFinalizar.Value : 0,
                        DtCadastro = DateTime.Now,
                        LoginCadastro = user.Login
                    });

                foreach (var hItem in historicoItens)
                {
                    db.Insert(hItem);
                }

                trans.Complete();
            }
        }

        /// <summary>
        /// Atualiza a proposta para o próximo status.
        /// </summary>
        private void AtualizaResponsavelProximoStatus(PropostaOrcamentariaDespesa proposta)
        {
            var statusService = new StatusPropostaOrcamentariaService(context);
            statusService.ProximoStatus(proposta);
        }

        /// <summary>
        /// Atualiza a proposta para o status anterior.
        /// </summary>
        private void AtualizaResponsavelStatusAnterior(PropostaOrcamentariaDespesa proposta)
        {
            var statusService = new StatusPropostaOrcamentariaService(context);
            statusService.StatusAnterior(proposta);
        }

        /// <summary>
        /// Notifica via e-mail o novo responsável pela proposta orçamentária.
        /// Se o responsável pela proposta não tiver e-mail cadastrado, não retorna erro nem nada, apenas não envia o e-mail.
        /// </summary>
        private void NotificaNovoResponsavel(PropostaOrcamentariaDespesa proposta, bool reprovacao, int idPessoaRespAnterior)
        {
            if (proposta.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.PlenarioOK)
            {
                if (!(proposta.IdPessoaResp > 0))
                {
                    throw new NBoxException($"Proposta { proposta.CentroCusto.NrCentroCustoFmt } não tem um novo responsável.");
                }

                if (proposta.CentroCusto == null)
                {
                    throw new NBoxException($"Proposta { proposta.IdPropostaOrcamentariaDespesa } sem centro de custo atribuído.");
                }

                var pessoaService = new PessoaService(context);
                var pDestinataria = pessoaService.BuscaPessoa(proposta.IdPessoaResp.Value, false);

                if (pDestinataria == null)
                {
                    throw new NBoxException("Destinatário não encontrado.");
                }

                //Se não houver e-mail cadastrado ou a pessoa não aceitar receber e-mail (conforme cadastro), não envia o e-mail.
                if (!string.IsNullOrEmpty(pDestinataria.NoEmail) && pDestinataria.FgAceitaEmail)
                {
                    var pRemetente = pessoaService.BuscaPessoa(idPessoaRespAnterior, false);

                    string assunto = string.Format("CreaSP (ELO): A proposta {0} está com você!", proposta.CentroCusto.NrCentroCustoFmt);
                    string msg = string.Format("{0} {1} a proposta do centro de custo {2} {3} para você.",
                        pRemetente != null ? pRemetente.NoPessoa : "Seu colega",
                        reprovacao ? "retornou" : "enviou",
                        proposta.CentroCusto.NoCentroCustoFmt,
                        proposta.CdTpFormulario != null ? "(" + proposta.CdTpFormulario + ")" : "");

                    if (reprovacao && !string.IsNullOrEmpty(proposta.NoMotivoRejeicao))
                    {
                        msg = string.Format("{0} \r\nMotivo: {1}", msg, proposta.NoMotivoRejeicao);
                    }
                    else if (reprovacao == false && proposta.CdStatusPropostaOrcamentaria == StPropostaOrcamentaria.Executor)
                    {
                        string deDiretrizes = new ParametroAnoService(context).BuscaParametroAno(proposta.NrAno).NoDiretrizes;

                        if (!string.IsNullOrEmpty(deDiretrizes))
                            msg = string.Format("{0} \r\n\r\nDiretrizes: {1}", msg, deDiretrizes);
                    }

                    NBox.Services.Email.New()
                        .To(pDestinataria.NoEmail, pDestinataria.NoPessoa)
                        .ReplyTo(pRemetente?.NoEmail ?? "lucas.aguiar@numeria.com.br")
                        .Subject(assunto)
                        .Body(msg)
                        .Send();

                    new EmailService(context).RegistraEnvioDeEmail(pDestinataria.IdPessoa, pDestinataria.NoEmail, assunto, msg);
                }
            }
        }

        /// <summary>
        /// Verifica se o usuário logado pode alterar (editar textos e valores) uma proposta orçamentária.
        /// </summary>
        public bool PodeAlterarProposta(int idPropostaOrcamentaria)
        {
            var proposta = BuscaPropostaOrcamentaria(idPropostaOrcamentaria);

            if (proposta.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.ContabValidacao
                && proposta.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.Plenario
                && proposta.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.PlenarioOK)
            {
                if (user != null)
                {
                    int idUsuarioLogado = (user as CreaspUser).IdPessoa;
                    List<int?> subDelegacoes = new PessoaService(context).ListaDelegacoes(idUsuarioLogado).ToList();
                    return subDelegacoes.Contains(proposta.IdPessoaResp);
                }
            }

            return false;
        }

        /// <summary>
        /// Verifica se o usuário logado pode mover (aprovar ou recusar) uma proposta orçamentária.
        /// </summary>
        /// <param name="idPropostaOrcamentaria"></param>
        /// <returns></returns>
        public bool PodeMoverProposta(int idPropostaOrcamentaria)
        {
            var proposta = BuscaPropostaOrcamentaria(idPropostaOrcamentaria);
            if (user != null)
            {
                int idUsuarioLogado = (user as CreaspUser).IdPessoa;
                List<int?> subDelegacoes = new PessoaService(context).ListaDelegacoes(idUsuarioLogado).ToList();
                return subDelegacoes.Contains(proposta.IdPessoaResp);
            }

            return false;
        }

        //private void DeParaTodasContas5para6(List<PropostaOrcamentariaDespesa> propostas)
        //private void DeParaTodasContas5para6(List<PropostaOrcamentariaDespesa> propostas)
        public List<string> DeParaTodasContas5para6(List<PropostaOrcamentariaDespesa> propostas)
        {
            var contaMapeamentoService = new ContaContabilMapeamentoService(context);
            var contasContabeis = contaMapeamentoService.BuscaContasContabeisMapeadas();

            var lstContas5sem6 = new List<string>();

            string nrContaContabil6 = string.Empty;

            for (int i = 0; i < propostas.Count; i++)
            {
                //propostas[i].PropostaOrcamentariaItens.ForEach(x => x.ContaContabil.NrContaContabil = contaContabilService.DePara5para6(contasContabeis, x.ContaContabil.NrContaContabil));
                if (propostas[i].PropostaOrcamentariaItens != null)
                {
                    for (int j = 0; j < propostas[i].PropostaOrcamentariaItens.Count; j++)
                    {
                        nrContaContabil6 = contaMapeamentoService.DePara5para6(contasContabeis, propostas[i].PropostaOrcamentariaItens[j].ContaContabil.NrContaContabil);
                        if (!string.IsNullOrEmpty(nrContaContabil6))
                        {
                            propostas[i].PropostaOrcamentariaItens[j].ContaContabil.NrContaContabil = contaMapeamentoService.DePara5para6(contasContabeis, propostas[i].PropostaOrcamentariaItens[j].ContaContabil.NrContaContabil);
                        }
                        else
                        {
                            lstContas5sem6.Add(propostas[i].PropostaOrcamentariaItens[j].ContaContabil.NrContaContabilFmt);
                        }
                    }
                }
                //propostas[i].PropostaOrcamentariaItens.ForEach(x => x.ContaContabil.NrContaContabil = contaContabilService.DePara5para6(contasContabeis, x.ContaContabil.NrContaContabil));
            }

            return lstContas5sem6;
        }

        /// <summary>
        /// Atualiza 3 itens da proposta orçamentária do centro de custo "Contabilidade".
        /// Esta atualização oriunda do valor gasto com as "Cota-partes" Mútua (SP e BSB) e Confea - são valores registrados nas metodologias de receita.
        /// </summary>
        public void AtualizaTransposicaoCotaParteMutuaConfea()
        {
            using (var trans = db.GetTransaction())
            {
                //var centroResponsavelContabilidade = new CentroCustoResponsavelService(context).BuscaCentroCustoResponsavelContabilidade();
                //var propostaOrcamentaria = new PropostaOrcamentariaDespesaService(context).BuscaPropostaOrcamentaria(centroResponsavelContabilidade.CentroCusto.NrCentroCusto);

                var centroCustoDCF = new CentroCustoPerfilService(context).BuscaSeHouverCentroCustoResponsavel(TpPerfilCentroCusto.DCF).CentroCusto;

                var propostaOrcamentaria = db.Query<PropostaOrcamentariaDespesa>()
                    .Include(x => x.CentroCusto)
                    .Where(x => x.CentroCusto.NrCentroCusto == centroCustoDCF.NrCentroCusto)
                    .Where(x => x.NrAno == nrAnoBase)
                    .Where(x => x.CdTpFormulario == null)
                    .SingleOrDefault();

                if (propostaOrcamentaria == null)
                {
                    throw new NBoxException("Antes de alterar as metodologias, é preciso ter uma proposta orçamentária para o centro de custos de finanças.");
                }

                if (propostaOrcamentaria.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.Plenario &&
                    propostaOrcamentaria.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.PlenarioOK)
                {
                    var metodologias = new MetodologiaReceitaService(context).BuscaMetodologias(nrAnoBase);

                    //O novo valor dos itens da proposta orçamentária são 
                    var vrCotaParteConfea = metodologias.Sum(x => x.VrCotaParteConfea);
                    var vrCotaParteMutuaSP = metodologias.Sum(x => x.VrCotaSaoPaulo);
                    var vrCotaParteMutuaBSB = metodologias.Sum(x => x.VrCotaBrasilia);

                    var propostasItens = new PropostaOrcamentariaDespesaService(context)
                                        .ListaPropostaOrcamentariaItens(propostaOrcamentaria.IdPropostaOrcamentariaDespesa);

                    var pCotaParteMutuaSP = propostasItens.Where(x => x.ContaContabil.NrContaContabil == "52211050201").SingleOrDefault();
                    var pCotaParteConfea = propostasItens.Where(x => x.ContaContabil.NrContaContabil == "52211050202").SingleOrDefault();
                    var pCotaParteMutuaBSB = propostasItens.Where(x => x.ContaContabil.NrContaContabil == "52211050203").SingleOrDefault();

                    if (pCotaParteConfea == null || pCotaParteMutuaBSB == null || pCotaParteMutuaSP == null)
                    {
                        throw new NBoxException("Antes de alterar as metodologias, é preciso cadastrar as contas contábeis de 'Cota Parte' no centro de custos de finanças.");
                    }

                    pCotaParteConfea.VrOrcado = vrCotaParteConfea;
                    pCotaParteConfea.VrOrcadoAntesFinalizar = vrCotaParteConfea;

                    db.Save(pCotaParteConfea);

                    pCotaParteMutuaSP.VrOrcado = vrCotaParteMutuaSP;
                    pCotaParteMutuaSP.VrOrcadoAntesFinalizar = vrCotaParteMutuaSP;

                    db.Save(pCotaParteMutuaSP);

                    pCotaParteMutuaBSB.VrOrcado = vrCotaParteMutuaBSB;
                    pCotaParteMutuaBSB.VrOrcadoAntesFinalizar = vrCotaParteMutuaBSB;

                    db.Save(pCotaParteMutuaBSB);
                }

                trans.Complete();
            }
        }

        /// <summary>
        /// Busca todas os itens da proposta orçamentária de acordo com o ano seleconado (nrAnoBase).
        /// </summary>
        public List<PropostaOrcamentariaDespesaItem> ListaPropostaOrcamentariaItens(int? idPropostaOrcamentaria = null, bool apenasVersaoAtual = true)
        {
            var propostaItens = db.Query<PropostaOrcamentariaDespesaItem>()
                .Include(x => x.ContaContabil)
                .Include(x => x.PropostaOrcamentariaDespesa)
                .Include(x => x.PropostaOrcamentariaDespesa.CentroCusto)
                //.IncludeMany(x => x.PropostaOrcamentariaHistoricoItens)
                .Where(idPropostaOrcamentaria != null, x => x.IdPropostaOrcamentariaDespesa == idPropostaOrcamentaria)
                .Where(x => x.PropostaOrcamentariaDespesa.NrAno == nrAnoBase)
                .Where(apenasVersaoAtual, x => x.NrVersao == 0)
                .OrderBy(x => x.ContaContabil.NrContaContabil)
                .ToList();

            return propostaItens;
        }

        /// <summary>
        /// Busca todas os itens, incluindo a proposta e formularios.
        /// </summary>
        public List<PropostaOrcamentariaDespesaItem> ListaItensPorCentroCusto (string nrCentroCusto, bool apenasVersaoAtual = true)
        {
            var propostaItens = db.Query<PropostaOrcamentariaDespesaItem>()
                .Include(x => x.ContaContabil)
                .Include(x => x.PropostaOrcamentariaDespesa)
                .Include(x => x.PropostaOrcamentariaDespesa.CentroCusto)
                //.IncludeMany(x => x.PropostaOrcamentariaHistoricoItens)
                .Where(x => x.PropostaOrcamentariaDespesa.CentroCusto.NrCentroCusto == nrCentroCusto)
                .Where(x => x.PropostaOrcamentariaDespesa.NrAno == nrAnoBase)
                .Where(apenasVersaoAtual, x => x.NrVersao == 0)
                .OrderBy(x => x.ContaContabil.NrContaContabil)
                .ToList();

            return propostaItens;
        }





        /// <summary>
        /// Agrupa itens de acordo com conta contábil para formatar a lista de acordo com o esperado pelo SisCont.
        /// </summary>
        public List<PropostaOrcamentariaDespesaItem> ListaPropostaOrcamentariaItensAgrupados(List<PropostaOrcamentariaDespesaItem> propostaItens)
        {
            return propostaItens
                        .GroupBy(l => l.ContaContabil.NrContaContabil)
                        .Select(cl => new PropostaOrcamentariaDespesaItem
                        {
                            ContaContabil = cl.First().ContaContabil,
                            IdContaContabil = cl.First().IdContaContabil,
                            IdPropostaOrcamentariaDespesa = cl.First().IdPropostaOrcamentariaDespesa,
                            IdPropostaOrcamentariaDespesaItem = cl.First().IdPropostaOrcamentariaDespesaItem,
                            NrProcessoL = cl.First().NrProcessoL,
                            NrVersao = cl.First().NrVersao,
                            NoEspecificacao = cl.First().NoEspecificacao,
                            NoJustificativa = cl.First().NoJustificativa,
                            VrAprovadoAnoAnterior = cl.First().VrAprovadoAnoAnterior,
                            VrExecutadoAnoAnterior = cl.First().VrExecutadoAnoAnterior,
                            VrOrcado = cl.Sum(c => c.VrOrcado),
                            VrOrcadoAntesFinalizar = cl.Sum(d => d.VrOrcadoAntesFinalizar),
                            DtAlteracao = cl.First().DtAlteracao,
                            DtCadastro = cl.First().DtCadastro,
                            DtRefVrExecutado = cl.First().DtRefVrExecutado,
                            FgVrOrcadoOrigemSenior = cl.First().FgVrOrcadoOrigemSenior,
                            LoginAlteracao = cl.First().LoginAlteracao,
                            LoginCadastro = cl.First().LoginCadastro,
                            PropostaOrcamentariaDespesa = cl.First().PropostaOrcamentariaDespesa,
                            PropostaOrcamentariaHistoricoItens = cl.First().PropostaOrcamentariaHistoricoItens
                        })
                        .ToList();
        }

        /// <summary>
        /// Salva os itens da proposta orçamentária 
        /// </summary>
        public void Salva(List<PropostaOrcamentariaDespesaItem> itensProposta)
        {
            using (var trans = db.GetTransaction())
            {
                //Poderia ter uma flag informando que o objeto foi alterado, 
                //pois normalmente o usuário não vai alterar todos registros, 
                //então não há necessidade de fazer update em todos.

                if (!ValidaEspecificacoes(itensProposta))
                {
                    throw new NBoxException("Todos os itens com valores orçados precisam ter uma especificação preenchida");
                }

                if (!ValidaJustificativas(itensProposta))
                {
                    throw new NBoxException("Todos os itens com valores orçados precisam ter uma justificativa preenchida");
                }

                foreach (var item in itensProposta)
                {
                    db.AuditUpdate(item);
                }

                trans.Complete();
            }
        }

        private bool ValidaJustificativas(List<PropostaOrcamentariaDespesaItem> itensProposta)
        {
            return !itensProposta.Where(x => x.VrOrcadoAntesFinalizar > 0).Any(x => string.IsNullOrWhiteSpace(x.NoJustificativa));
        }

        private bool ValidaEspecificacoes(List<PropostaOrcamentariaDespesaItem> itensProposta)
        {
            return !itensProposta.Where(x => x.VrOrcadoAntesFinalizar > 0).Any(x => string.IsNullOrWhiteSpace(x.NoEspecificacao));
        }

        /// <summary>
        /// Valida e inclui uma conta contábil dentro de uma proposta orçamentária.
        /// </summary>
        public void ValidaEIncluiContaContabil(int idPropostaOrcamentaria, int idContaContabil, string nrProcessoL)
        {
            if (idPropostaOrcamentaria == 0) throw new ArgumentNullException("idPropostaOrcamentaria");
            if (idContaContabil == 0) throw new ArgumentNullException("idContaContabil");

            if (!ValidaInclusaoContaContabil(idPropostaOrcamentaria, idContaContabil, nrProcessoL))
            {
                throw new NBoxException("Conta contábil já existe nesta proposta.");
            }

            IncluiContaContabil(idPropostaOrcamentaria, idContaContabil, nrProcessoL);
        }

        /// <summary>
        /// Valida se conta contábil pode ser inserida na proposta orçamentária.
        /// </summary>
        private bool ValidaInclusaoContaContabil(int idPropostaOrcamentaria, int idContaContabil, string nrProcessoL)
        {
            return !db.Query<PropostaOrcamentariaDespesaItem>()
                .Where(x => x.IdPropostaOrcamentariaDespesa == idPropostaOrcamentaria)
                .Where(x => x.IdContaContabil == idContaContabil)
                .Where(x => !string.IsNullOrEmpty(nrProcessoL) && x.NrProcessoL == nrProcessoL) //Assim permite incluir a mesma conta sem número de processo.
                //.Where(!string.IsNullOrEmpty(nrProcessoL), x => x.NrProcessoL == nrProcessoL) //Assim permite apenas uma conta sem número de processo.
                .Any();
        }

        /// <summary>
        /// Inclui uma conta contábil na proposta orçamentária.
        /// Este pocesso busca dados sincronizados com sistemas externos para, havendo vínculo para centro de custo e conta contábil, 
        /// preencher valores executados, dotação atual e valor orçado (em caso de conta de RH).
        /// </summary>
        private void IncluiContaContabil(int idPropostaOrcamentaria, int idContaContabil, string nrProcessoL)
        {
            var propostaService = new PropostaOrcamentariaDespesaService(context);
            var contaContabilService = new ContaContabilService(context);

            var p = propostaService.BuscaPropostaOrcamentaria(idPropostaOrcamentaria);
            var conta = contaContabilService.BuscaContaContabil(idContaContabil);

            var pi = new PropostaOrcamentariaDespesaItem();
            pi.IdPropostaOrcamentariaDespesa = p.IdPropostaOrcamentariaDespesa;
            pi.IdContaContabil = conta.IdContaContabil;

            //Se tiver número de processo ou for um formulário, não é pra preencher nenhum outro valor da conta contábil.
            if (!string.IsNullOrEmpty(nrProcessoL) || !string.IsNullOrEmpty(p.CdTpFormulario))
            {
                pi.NrProcessoL = nrProcessoL;
                pi.VrOrcado = 0;
                pi.VrOrcadoAntesFinalizar = 0;
            }
            else
            {
                var orcamentoService = new OrcamentoService(context);

                pi.VrAprovadoAnoAnterior = orcamentoService.ListaDotacoes(p.NrAno - 1, p.NrAno - 1, false)
                    .Where(x => x.NrCentroCusto == p.CentroCusto.NrCentroCusto)
                    .Where(x => x.NrContaContabil5 == conta.NrContaContabil)
                    .SingleOrDefault()?.VrAprovado;

                var orcamentoSenior = orcamentoService.ListaOrcamentoSenior(p.NrAno);

                if (orcamentoSenior?.Count > 0)
                {
                    pi.VrOrcado = orcamentoSenior
                        .Where(x => "0" + x.NrCentroCusto == p.CentroCusto.NrCentroCusto)
                        .Where(x => x.NrContaContabil5.StartsWith(conta.NrContaContabil))
                        .SingleOrDefault()?.VrOrcamento;

                    if (pi.VrOrcado != null) pi.FgVrOrcadoOrigemSenior = true;
                }

                var posicaoAtualOrcamento = orcamentoService.ListaPosicaoAtualOrcamento(p.NrAno - 1, p.NrAno - 1)
                    .Where(x => x.NrCentroCusto == p.CentroCusto.NrCentroCusto)
                    .Where(x => x.NrContaContabil5 == conta.NrContaContabil)
                    .SingleOrDefault();

                if (posicaoAtualOrcamento != null)
                {
                    pi.VrExecutadoAnoAnterior = posicaoAtualOrcamento.VrExecutado;
                    pi.DtRefVrExecutado = posicaoAtualOrcamento.DtRefVrExecutado;
                }
            }

            db.Insert(pi);
        }

        /// <summary>
        /// Verifica se uma proposta orçamentária já possui uma conta contábil específica.
        /// </summary>
        public bool VerificaSePropostaPossuiConta(int idPropostaOrcamentaria, int idContaContabil)
        {
            return db.Query<PropostaOrcamentariaDespesaItem>()
                .Where(x => x.IdPropostaOrcamentariaDespesa == idPropostaOrcamentaria)
                .Where(x => x.IdContaContabil == idContaContabil)
                .Any();
        }

        /// <summary>
        /// Exclui uma conta contábil de formulário e seus registros na tabela histórico. 
        /// </summary>
        public void ExcluiFormularioItem(int idPropostaOrcamentariaItem)
        {
            using (var trans = db.GetTransaction())
            {
                db.DeleteMany<PropostaOrcamentariaHistorico>().Where(x => x.IdPropostaOrcamentariaDespesaItem == idPropostaOrcamentariaItem).Execute();
                db.Delete<PropostaOrcamentariaDespesaItem>(idPropostaOrcamentariaItem);

                trans.Complete();
            }
        }
    }
}
