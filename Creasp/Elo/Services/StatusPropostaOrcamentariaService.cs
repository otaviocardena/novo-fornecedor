﻿using Creasp.Core;
using Creasp.Elo;
using NBox.Services;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Creasp.Elo
{
    public class StatusPropostaOrcamentariaService : DataService
    {
        //TODO: Refatorar toda esta classe para ela não ter tanto código duplicado. Mudanças de status devem ser otimizadas. Criar classe abstrata, DTO, algo nessa linha.

        public StatusPropostaOrcamentariaService(ServiceContext context) : base(context)
        {
            var lstStatus = ListaStatusPropostas();

            Status = lstStatus.Select(x => x.CdStatusPropostaOrcamentaria).ToArray();
        }

        private string[] Status;

        //public string ProximoStatus(PropostaOrcamentariaDespesa proposta)
        //{
        //    int posicaoAtual = Array.IndexOf(Status, proposta.CdStatusPropostaOrcamentaria);
        //    proposta.CdStatusPropostaOrcamentaria = Status[posicaoAtual + 1];

        //    string templateLog = AlteraStatus(proposta);

        //    return string.Format(templateLog, "enviada");
        //}

        //public string StatusAnterior(PropostaOrcamentariaDespesa proposta)
        //{
        //    int posicaoAtual = Array.IndexOf(Status, proposta.CdStatusPropostaOrcamentaria);
        //    proposta.CdStatusPropostaOrcamentaria = posicaoAtual == 0 ? "" : Status[posicaoAtual - 1];

        //    string templateLog = AlteraStatus(proposta);

        //    return string.Format(templateLog, "retornada");
        //}

        //private string AlteraStatus(PropostaOrcamentariaDespesa proposta)
        //{
        //    string templateLog = string.Empty;

        //    switch (proposta.CdStatusPropostaOrcamentaria)
        //    {
        //        case StPropostaOrcamentaria.Executor:
        //            proposta.IdPessoaResp = proposta.CentroCusto.CentroCustoResponsavel.IdPessoaExecutor;
        //            templateLog = "Reformução {0} para o executor.";
        //            break;
        //        case StPropostaOrcamentaria.Aprovador1:
        //            proposta.IdPessoaResp = proposta.CentroCusto.CentroCustoResponsavel.IdPessoaAprovador1;
        //            templateLog = "Reformução {0} para o 1º aprovador.";
        //            break;
        //        case StPropostaOrcamentaria.Aprovador2:
        //            proposta.IdPessoaResp = proposta.CentroCusto.CentroCustoResponsavel.IdPessoaAprovador2;
        //            templateLog = "Reformução {0} para o 2º aprovador.";
        //            break;
        //        case StPropostaOrcamentaria.Aprovador3:
        //            proposta.IdPessoaResp = proposta.CentroCusto.CentroCustoResponsavel.IdPessoaAprovador3;
        //            templateLog = "Reformução {0} para o 3º aprovador.";
        //            break;
        //        case StPropostaOrcamentaria.Aprovador4:
        //            proposta.IdPessoaResp = proposta.CentroCusto.CentroCustoResponsavel.IdPessoaAprovador4;
        //            templateLog = "Reformução {0} para o 4º aprovador.";
        //            break;
        //        case StPropostaOrcamentaria.Contab:
        //            var centroResponsavelService = new CentroCustoResponsavelService(context);
        //            proposta.IdPessoaResp = centroResponsavelService.BuscaPessoaResponsavelContabilidade()?.IdPessoa;
        //            templateLog = "Reformução {0} para a contabilidade.";
        //            break;
        //        case StPropostaOrcamentaria.Plenario:
        //            //proposta.IdPessoaResp = idPessoaContabilidade;
        //            proposta.IdPessoaResp = null;
        //            templateLog = "Reformução {0} para o plenário.";
        //            break;
        //        case StPropostaOrcamentaria.PlenarioOK:
        //            //proposta.IdPessoaResp = idPessoaContabilidade;
        //            proposta.IdPessoaResp = null;
        //            templateLog = "Reformução {0} para a contabilidade.";
        //            break;
        //        default:
        //            break;
        //    }

        //    return templateLog;
        //}

        /// <summary>
        /// Lista todos os status
        /// </summary>
        public IEnumerable<StatusPropostaOrcamentaria> ListaStatusPropostas()
        {
            return db.Query<StatusPropostaOrcamentaria>()
                        .Where(x => x.FgAtivo == true)
                        .OrderBy(x => x.NrOrdem)
                        .ToList();
        }

        public void ProximoStatus(PropostaOrcamentariaDespesa proposta)
        {
            proposta.CdStatusPropostaOrcamentaria = BuscaProximoStatus(proposta.CdStatusPropostaOrcamentaria, proposta.CdTpFormulario, proposta.CentroCusto.CentroCustoResponsavel);
            proposta.IdPessoaResp = BuscaIdPessoaNovoResponsavel(proposta.CdStatusPropostaOrcamentaria, proposta.CdTpFormulario, proposta.CentroCusto.CentroCustoResponsavel);
            proposta.FgEstaReprovado = false;
            LogaAlteracaoStatus(proposta.CdStatusPropostaOrcamentaria, "PropostaOrcamentariaDespesa", proposta.IdPropostaOrcamentariaDespesa, proposta.CdTpFormulario, false);
        }

        public void StatusAnterior(PropostaOrcamentariaDespesa proposta)
        {
            proposta.CdStatusPropostaOrcamentaria = BuscaStatusAnterior(proposta.CdStatusPropostaOrcamentaria, proposta.CdTpFormulario, proposta.CentroCusto.CentroCustoResponsavel);
            proposta.IdPessoaResp = BuscaIdPessoaNovoResponsavel(proposta.CdStatusPropostaOrcamentaria, proposta.CdTpFormulario, proposta.CentroCusto.CentroCustoResponsavel);
            proposta.FgEstaReprovado = true;
            LogaAlteracaoStatus(proposta.CdStatusPropostaOrcamentaria, "PropostaOrcamentariaDespesa", proposta.IdPropostaOrcamentariaDespesa, proposta.CdTpFormulario, true, proposta.NoMotivoRejeicao);
        }

        public void ProximoStatus(ReformulacaoOrcamentaria reformulacao)
        {
            reformulacao.CdStatusPropostaOrcamentaria = BuscaProximoStatus(reformulacao.CdStatusPropostaOrcamentaria, reformulacao.CdTpFormulario, reformulacao.CentroCusto.CentroCustoResponsavel);
            reformulacao.IdPessoaResp = BuscaIdPessoaNovoResponsavel(reformulacao.CdStatusPropostaOrcamentaria, reformulacao.CdTpFormulario, reformulacao.CentroCusto.CentroCustoResponsavel);
            reformulacao.FgEstaReprovado = false;
            LogaAlteracaoStatus(reformulacao.CdStatusPropostaOrcamentaria, "ReformulacaoOrcamentaria", reformulacao.IdReformulacaoOrcamentaria, reformulacao.CdTpFormulario, false);
        }

        public void StatusAnterior(ReformulacaoOrcamentaria reformulacao)
        {
            reformulacao.CdStatusPropostaOrcamentaria = BuscaStatusAnterior(reformulacao.CdStatusPropostaOrcamentaria, reformulacao.CdTpFormulario, reformulacao.CentroCusto.CentroCustoResponsavel);
            reformulacao.IdPessoaResp = BuscaIdPessoaNovoResponsavel(reformulacao.CdStatusPropostaOrcamentaria, reformulacao.CdTpFormulario, reformulacao.CentroCusto.CentroCustoResponsavel);
            reformulacao.FgEstaReprovado = true;
            LogaAlteracaoStatus(reformulacao.CdStatusPropostaOrcamentaria, "ReformulacaoOrcamentaria", reformulacao.IdReformulacaoOrcamentaria, reformulacao.CdTpFormulario, true, reformulacao.NoMotivoRejeicao);
        }

        public void ProximoStatus(MetodologiaReceitaAno metodologia)
        {
            var centroPerfilService = new CentroCustoPerfilService(context);
            var centroResponsavel = centroPerfilService.BuscaSeHouverCentroCustoResponsavel(TpPerfilCentroCusto.Contabilidade);
            
            metodologia.CdStatusPropostaOrcamentaria = BuscaProximoStatus(metodologia.CdStatusPropostaOrcamentaria, "", centroResponsavel);
            metodologia.IdPessoaResp = BuscaIdPessoaNovoResponsavel(metodologia.CdStatusPropostaOrcamentaria, "", centroResponsavel);
            LogaAlteracaoStatus(metodologia.CdStatusPropostaOrcamentaria, "ReformulacaoOrcamentaria", metodologia.IdMetodologiaReceitaAno, "", false);
        }

        public void StatusAnterior(MetodologiaReceitaAno metodologia)
        {
            var centroPerfilService = new CentroCustoPerfilService(context);
            var centroResponsavel = centroPerfilService.BuscaSeHouverCentroCustoResponsavel(TpPerfilCentroCusto.Contabilidade);

            metodologia.CdStatusPropostaOrcamentaria = BuscaStatusAnterior(metodologia.CdStatusPropostaOrcamentaria, "", centroResponsavel);
            metodologia.IdPessoaResp = BuscaIdPessoaNovoResponsavel(metodologia.CdStatusPropostaOrcamentaria, "", centroResponsavel);
            LogaAlteracaoStatus(metodologia.CdStatusPropostaOrcamentaria, "ReformulacaoOrcamentaria", metodologia.IdMetodologiaReceitaAno, "", true);
        }

        private string BuscaProximoStatus(string cdStatusAntigo, string cdTpFormulario, CentroCustoResponsavel centroCustoResponsavel)
        {
            //NOTE: Código mais feio que já escrevi na vida. Resolve o problema, mas é simplesmente ilegível, incompreensível
            //string novoStatus = cdStatusAntigo;

            //do
            //{
            //    int posicaoAtual = Array.IndexOf(Status, novoStatus);
            //    if (posicaoAtual == Status.Length) throw new NBoxException("Não foi possível ir para o próximo status.");

            //    novoStatus = Status[posicaoAtual + 1];

            //    switch (novoStatus)
            //    {
            //        case StPropostaOrcamentaria.Executor:
            //            if (centroCustoResponsavel.IdPessoaExecutor.HasValue) return novoStatus; break;
            //        case StPropostaOrcamentaria.Aprovador1:
            //            if (!string.IsNullOrEmpty(cdTpFormulario))
            //            {
            //                novoStatus = cdTpFormulario == CdTpFormulario.Almoxarifado ? StPropostaOrcamentaria.Almoxarifado :
            //                    cdTpFormulario == CdTpFormulario.Informatica ? StPropostaOrcamentaria.Informatica :
            //                    cdTpFormulario == CdTpFormulario.DAS ? StPropostaOrcamentaria.DAS : "";

            //                if (string.IsNullOrEmpty(novoStatus)) throw new NBoxException("Não foi possível ir para o próximo status. Formulário indefinido.");
            //            }

            //            if (centroCustoResponsavel.IdPessoaAprovador1.HasValue) return novoStatus; break;
            //        case StPropostaOrcamentaria.Aprovador2:
            //            if (centroCustoResponsavel.IdPessoaAprovador2.HasValue) return novoStatus; break;
            //        case StPropostaOrcamentaria.Aprovador3:
            //            if (centroCustoResponsavel.IdPessoaAprovador3.HasValue) return novoStatus; break;
            //        case StPropostaOrcamentaria.Aprovador4:
            //            if (centroCustoResponsavel.IdPessoaAprovador4.HasValue) return novoStatus; break;
            //        case StPropostaOrcamentaria.Contab:
            //        case StPropostaOrcamentaria.Plenario:
            //        case StPropostaOrcamentaria.PlenarioOK:
            //            return novoStatus;
            //        default:
            //            break;
            //    }

            //} while (true);


            //Se for do tipo formulário...
            if (!string.IsNullOrEmpty(cdTpFormulario))
            {
                //Se o novo status for de 1º aprovador, o status deve ser respectivo ao tipo de formulário
                if (cdStatusAntigo == StPropostaOrcamentaria.Executor)
                {
                    return StPropostaOrcamentaria.Aprovador1;

                    //return cdTpFormulario == CdTpFormulario.Almoxarifado ? StPropostaOrcamentaria.Almoxarifado :
                    //    cdTpFormulario == CdTpFormulario.Informatica ? StPropostaOrcamentaria.Informatica :
                    //    cdTpFormulario == CdTpFormulario.DAS ? StPropostaOrcamentaria.DAS : "";
                }
                else if (cdStatusAntigo == StPropostaOrcamentaria.Aprovador1)
                {
                    return StPropostaOrcamentaria.Contab;
                }

                //if (cdStatusAntigo == StPropostaOrcamentaria.Almoxarifado
                //    || cdStatusAntigo == StPropostaOrcamentaria.Informatica
                //    || cdStatusAntigo == StPropostaOrcamentaria.DAS)
                //{
                //    return StPropostaOrcamentaria.Contab;
                //}

                //if (cdStatusAntigo == StPropostaOrcamentaria.Contab)
                //{
                //    return StPropostaOrcamentaria.Plenario;
                //}

                //if (cdStatusAntigo == StPropostaOrcamentaria.Plenario)
                //{
                //    return StPropostaOrcamentaria.PlenarioOK;
                //}
            }

            int posicaoAtual = Array.IndexOf(Status, cdStatusAntigo);
            string novoStatus = Status[posicaoAtual + 1];

            //Se o novo status for de um aprovador, mas o centro de custo não possuir um responsável para este nível, vai direto pra contabilidade.
            if ((novoStatus == StPropostaOrcamentaria.Aprovador1 && !(centroCustoResponsavel.IdPessoaAprovador1 > 0))
                || (novoStatus == StPropostaOrcamentaria.Aprovador2 && !(centroCustoResponsavel.IdPessoaAprovador2 > 0))
                || (novoStatus == StPropostaOrcamentaria.Aprovador3 && !(centroCustoResponsavel.IdPessoaAprovador3 > 0))
                || (novoStatus == StPropostaOrcamentaria.Aprovador4 && !(centroCustoResponsavel.IdPessoaAprovador4 > 0)))
            {
                return StPropostaOrcamentaria.Contab;
            }

            return novoStatus;
        }

        private string BuscaStatusAnterior(string cdStatusAntigo, string cdTpFormulario, CentroCustoResponsavel centroCustoResponsavel)
        {

            if (!string.IsNullOrEmpty(cdTpFormulario))
            {
                if (cdStatusAntigo == StPropostaOrcamentaria.Contab)
                {
                    return StPropostaOrcamentaria.Aprovador1;

                    //return cdTpFormulario == CdTpFormulario.Almoxarifado ? StPropostaOrcamentaria.Almoxarifado :
                    //    cdTpFormulario == CdTpFormulario.Informatica ? StPropostaOrcamentaria.Informatica :
                    //    cdTpFormulario == CdTpFormulario.DAS ? StPropostaOrcamentaria.DAS : "";
                }

                ////Sendo formulário, deve-se desconsiderar os níveis hierárquicos do centro de custo.
                //centroCustoResponsavel.IdPessoaAprovador1 = null;
                //centroCustoResponsavel.IdPessoaAprovador2 = null;
                //centroCustoResponsavel.IdPessoaAprovador3 = null;
                //centroCustoResponsavel.IdPessoaAprovador4 = null;
            }

            int qtdTentativas = 0;

            string novoStatus = cdStatusAntigo;

            do
            {
                int posicaoAtual = Array.IndexOf(Status, novoStatus);
                novoStatus = posicaoAtual == 0 ? "" : Status[posicaoAtual - 1];

                if (novoStatus == StPropostaOrcamentaria.ContabValidacao) return StPropostaOrcamentaria.ContabValidacao;
                if (novoStatus == StPropostaOrcamentaria.Contab) return StPropostaOrcamentaria.Contab;
                if (novoStatus == StPropostaOrcamentaria.Aprovador4 && centroCustoResponsavel.IdPessoaAprovador4.HasValue) return StPropostaOrcamentaria.Aprovador4;
                if (novoStatus == StPropostaOrcamentaria.Aprovador3 && centroCustoResponsavel.IdPessoaAprovador3.HasValue) return StPropostaOrcamentaria.Aprovador3;
                if (novoStatus == StPropostaOrcamentaria.Aprovador2 && centroCustoResponsavel.IdPessoaAprovador2.HasValue) return StPropostaOrcamentaria.Aprovador2;
                if (novoStatus == StPropostaOrcamentaria.Aprovador1 && centroCustoResponsavel.IdPessoaAprovador1.HasValue) return StPropostaOrcamentaria.Aprovador1;
                if (novoStatus == StPropostaOrcamentaria.Executor) return StPropostaOrcamentaria.Executor;
                //if (novoStatus == StPropostaOrcamentaria.Novo) return StPropostaOrcamentaria.Novo;

                qtdTentativas++;
            } while (qtdTentativas <= 5);
            //if (cdStatusAntigo == StPropostaOrcamentaria.Contab && !string.IsNullOrEmpty(cdTpFormulario))
            //{
            //    return StPropostaOrcamentaria.Aprovador1;
            //}
            //else
            //{
            //    int posicaoAtual = Array.IndexOf(Status, cdStatusAntigo);
            //    return posicaoAtual == 0 ? "" : Status[posicaoAtual - 1];
            //}

            return string.Empty;
        }

        private int? BuscaIdPessoaNovoResponsavel(string cdStatusPropostaorcamentaria, string cdTipoFormulario, CentroCustoResponsavel centroResponsavel)
        {
            var centroResponsavelService = new CentroCustoResponsavelService(context);
            var pessoaService = new PessoaService(context);
            var centroCustoPerfilService = new CentroCustoPerfilService(context);

            //Se for um formulário e o status atual for aprovador1...
            if (!string.IsNullOrEmpty(cdTipoFormulario) && cdStatusPropostaorcamentaria == StPropostaOrcamentaria.Aprovador1)
            {
                switch (cdTipoFormulario)
                {
                    case CdTpFormulario.Informatica: return centroCustoPerfilService.BuscaSeHouverCentroCustoResponsavel(TpPerfilCentroCusto.Informatica).IdPessoaExecutor;
                    case CdTpFormulario.Almoxarifado: return centroCustoPerfilService.BuscaSeHouverCentroCustoResponsavel(TpPerfilCentroCusto.Almoxarifado).IdPessoaExecutor;
                    case CdTpFormulario.DAS: return centroCustoPerfilService.BuscaSeHouverCentroCustoResponsavel(TpPerfilCentroCusto.DAS).IdPessoaExecutor;
                    default: return null;
                }
            }
            else
            {
                int? idNovoResp;

                switch (cdStatusPropostaorcamentaria)
                {
                    case StPropostaOrcamentaria.Executor: idNovoResp = centroResponsavel.IdPessoaExecutor; break;

                    case StPropostaOrcamentaria.Informatica:
                        idNovoResp = centroCustoPerfilService.BuscaSeHouverCentroCustoResponsavel(TpPerfilCentroCusto.Informatica).IdPessoaExecutor;
                        break;
                    case StPropostaOrcamentaria.Almoxarifado:
                        idNovoResp = centroCustoPerfilService.BuscaSeHouverCentroCustoResponsavel(TpPerfilCentroCusto.Almoxarifado).IdPessoaExecutor;
                        break;
                    case StPropostaOrcamentaria.DAS:
                        idNovoResp = centroCustoPerfilService.BuscaSeHouverCentroCustoResponsavel(TpPerfilCentroCusto.DAS).IdPessoaExecutor;
                        break;

                    case StPropostaOrcamentaria.Aprovador1: idNovoResp = centroResponsavel.IdPessoaAprovador1; break;
                    case StPropostaOrcamentaria.Aprovador2: idNovoResp = centroResponsavel.IdPessoaAprovador2; break;
                    case StPropostaOrcamentaria.Aprovador3: idNovoResp = centroResponsavel.IdPessoaAprovador3; break;
                    case StPropostaOrcamentaria.Aprovador4: idNovoResp = centroResponsavel.IdPessoaAprovador4; break;
                    case StPropostaOrcamentaria.Contab:
                    case StPropostaOrcamentaria.ContabValidacao:
                        idNovoResp = centroCustoPerfilService.BuscaSeHouverCentroCustoResponsavel(TpPerfilCentroCusto.Contabilidade).IdPessoaExecutor;
                        break;
                    case StPropostaOrcamentaria.Plenario: idNovoResp = null; break;
                    case StPropostaOrcamentaria.PlenarioOK: idNovoResp = null; break;
                    default: idNovoResp = null; break;
                }

                if (idNovoResp == null) //TODO: Não vai funcionar para reprovação, código temporário até termos a definição de usuários pra Plenario e PlenarioOK.
                {
                    idNovoResp = centroCustoPerfilService.BuscaSeHouverCentroCustoResponsavel(TpPerfilCentroCusto.Contabilidade).IdPessoaExecutor;
                }

                return idNovoResp;
            }
        }

        private void LogaAlteracaoStatus(string cdStatus, string nomeTabela, int chavePrimaria, string cdTipoFormulario, bool reprovacao = false, string noMotivoRejeicao = "")
        {
            string templateLog = string.Empty;

            var centroResponsavelService = new CentroCustoResponsavelService(context);

            if (!string.IsNullOrEmpty(cdTipoFormulario) && cdStatus == StPropostaOrcamentaria.Aprovador1)
            {
                switch (cdTipoFormulario)
                {
                    case CdTpFormulario.Informatica: templateLog = "{0} para a informática."; break;
                    case CdTpFormulario.Almoxarifado: templateLog = "{0} para o almoxarifado."; break;
                    case CdTpFormulario.DAS: templateLog = "{0} para o DAS."; break;
                    default: break;
                }
            }
            else
            {
                switch (cdStatus)
                {
                    case StPropostaOrcamentaria.Executor: templateLog = "{0} para o executor."; break;
                    case StPropostaOrcamentaria.Aprovador1: templateLog = "{0} para o aprovador 1."; break;
                    case StPropostaOrcamentaria.Aprovador2: templateLog = "{0} para o aprovador 2."; break;
                    case StPropostaOrcamentaria.Aprovador3: templateLog = "{0} para o aprovador 3."; break;
                    case StPropostaOrcamentaria.Aprovador4: templateLog = "{0} para o aprovador 4."; break;
                    case StPropostaOrcamentaria.Contab: templateLog = "{0} para a contabilidade."; break;
                    case StPropostaOrcamentaria.ContabValidacao: templateLog = "{0} para a validação da contabilidade."; break;
                    case StPropostaOrcamentaria.Plenario: templateLog = "{0} para o plenário."; break;
                    case StPropostaOrcamentaria.PlenarioOK: templateLog = "Aprovada pelo plenário."; break;
                    default: break;
                }
            }

            string mensagemLog = reprovacao ? string.Format(templateLog, "Reprovada") : string.Format(templateLog, "Enviada");
            if (!string.IsNullOrEmpty(noMotivoRejeicao))
            {
                mensagemLog = string.Format("{0} Motivo: {1}", mensagemLog, noMotivoRejeicao);
            }

            new AuditLog().Table(nomeTabela, chavePrimaria, mensagemLog);
        }

        //public void ProximoStatus(ReformulacaoOrcamentaria reformulacao)
        //{
        //    int posicaoAtual = Array.IndexOf(Status, reformulacao.CdStatusPropostaOrcamentaria);
        //    reformulacao.CdStatusPropostaOrcamentaria = Status[posicaoAtual + 1];

        //    string templateLog = AlteraStatus(reformulacao);

        //    new AuditLog().Table("ReformulacaoOrcamentaria", reformulacao.IdReformulacaoOrcamentaria, string.Format(templateLog, "enviada"));
        //}

        //public void StatusAnterior(ReformulacaoOrcamentaria reformulacao)
        //{
        //    int posicaoAtual = Array.IndexOf(Status, reformulacao.CdStatusPropostaOrcamentaria);
        //    reformulacao.CdStatusPropostaOrcamentaria = posicaoAtual == 0 ? "" : Status[posicaoAtual - 1];

        //    string templateLog = AlteraStatus(reformulacao);

        //    new AuditLog().Table("ReformulacaoOrcamentaria", reformulacao.IdReformulacaoOrcamentaria, string.Format(templateLog, "retornada"));
        //}

        //private string AlteraStatus(ReformulacaoOrcamentaria reformulacao)
        //{
        //    string templateLog = string.Empty;

        //    switch (reformulacao.CdStatusPropostaOrcamentaria)
        //    {
        //        case StPropostaOrcamentaria.Executor:
        //            reformulacao.IdPessoaResp = reformulacao.CentroCusto.CentroCustoResponsavel.IdPessoaExecutor;
        //            templateLog = "Reformução {0} para o executor.";
        //            break;
        //        case StPropostaOrcamentaria.Aprovador1:
        //            reformulacao.IdPessoaResp = reformulacao.CentroCusto.CentroCustoResponsavel.IdPessoaAprovador1;
        //            templateLog = "Reformução {0} para o 1º aprovador.";
        //            break;
        //        case StPropostaOrcamentaria.Aprovador2:
        //            reformulacao.IdPessoaResp = reformulacao.CentroCusto.CentroCustoResponsavel.IdPessoaAprovador2;
        //            templateLog = "Reformução {0} para o 2º aprovador.";
        //            break;
        //        case StPropostaOrcamentaria.Aprovador3:
        //            reformulacao.IdPessoaResp = reformulacao.CentroCusto.CentroCustoResponsavel.IdPessoaAprovador3;
        //            templateLog = "Reformução {0} para o 3º aprovador.";
        //            break;
        //        case StPropostaOrcamentaria.Aprovador4:
        //            reformulacao.IdPessoaResp = reformulacao.CentroCusto.CentroCustoResponsavel.IdPessoaAprovador4;
        //            templateLog = "Reformução {0} para o 4º aprovador.";
        //            break;
        //        case StPropostaOrcamentaria.Contab:
        //            var centroResponsavelService = new CentroCustoResponsavelService(context);
        //            reformulacao.IdPessoaResp = centroResponsavelService.BuscaPessoaResponsavelContabilidade()?.IdPessoa;
        //            templateLog = "Reformução {0} para a contabilidade.";
        //            break;
        //        case StPropostaOrcamentaria.Plenario:
        //            //proposta.IdPessoaResp = idPessoaContabilidade;
        //            reformulacao.IdPessoaResp = null;
        //            templateLog = "Reformução {0} para o plenário.";
        //            break;
        //        case StPropostaOrcamentaria.PlenarioOK:
        //            //proposta.IdPessoaResp = idPessoaContabilidade;
        //            reformulacao.IdPessoaResp = null;
        //            templateLog = "Reformução {0} para a contabilidade.";
        //            break;
        //        default:
        //            break;
        //    }

        //    return templateLog;
        //}
    }
}
