﻿using Creasp.Core;
using NBox.Core;
using NBox.Services;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Creasp.Elo
{
    public class RelatoriosService : DataService
    {
        protected readonly int nrAnoBase;

        private ParametroAno _parametroAno;
        private List<ContaContabil> _contas;
        private List<PosicaoAtualOrcamento> _posicaoAtualOrcamento;
        private List<DisponibilidadeAtualOrcamento> _disponibilidadeAtualOrcamento;
        private string _dtRefVrExecutadoTexto;
        private string _dtRefVrArrecadadoTexto;
        private const int QTD_CARACTERES_PARA_NEGRITO = 7; //Apenas para sintético

        public RelatoriosService(ServiceContext context) : base(context)
        {
            nrAnoBase = (context as CreaspContext).NrAnoBase;
        }

        /// <summary>
        /// Carrega valores iniciais para gerar os relatórios.
        /// </summary>
        /// <param name="fgReceita">Define se os dados serão de receita OU despesa</param>
        /// <param name="trazerAnaliticasTambem">Define se as contas contábeis serão apenas sintáticas ou sináticas e analíticas também.</param>
        private void CarregaValoresIniciais(bool fgReceita, bool trazerAnaliticasTambem)
        {
            _parametroAno = new ParametroAnoService(context).BuscaParametroAno(nrAnoBase);
            if (fgReceita)
            {
                _contas = new ContaContabilService(context).BuscaContasContabeisPorPrefixo(CdPrefixoContaContabil.ReceitaInicial, trazerAnaliticasTambem);
            }
            else
            {
                _contas = new ContaContabilService(context).BuscaContasContabeisPorPrefixo(CdPrefixoContaContabil.DespesaInicial, trazerAnaliticasTambem);
            }

            _contas = _contas.Select(x => new ContaContabil()
            {
                NrContaContabil = x.NrContaContabil,
                NoContaContabil = x.NoContaContabil,
                FgAnalitico = x.FgAnalitico,
                NrAno = _parametroAno.NrAno
            })
            //.Where(x => x.NrContaContabil.Length != 4)
            .Distinct()
            .OrderBy(x => x.NrContaContabil)
            .ToList();

            var orcamentoService = new OrcamentoService(context);

            _posicaoAtualOrcamento = orcamentoService.ListaPosicaoAtualOrcamento(_parametroAno.NrAno - 2, _parametroAno.NrAno);
            _disponibilidadeAtualOrcamento = orcamentoService.ListaDisponibilidadeAtualOrcamento(_parametroAno.NrAno - 2, _parametroAno.NrAno);

            var dtRefVrExecutado = _posicaoAtualOrcamento
                .Where(x => x.NrAno == _parametroAno.NrAno - 1) //TODO: Testar com relatórios de reformulação.
                .Select(x => x.DtRefVrExecutado)?.Max();

            _dtRefVrExecutadoTexto = dtRefVrExecutado.HasValue ? dtRefVrExecutado.Value.ToString("dd/MM/yyyy") : "01/01/0001";

            var dtRefVrArrecadado = _disponibilidadeAtualOrcamento
                .Where(x => x.NrAno == _parametroAno.NrAno - 1)
                .Select(x => x.DtFim)?.Max();

            _dtRefVrArrecadadoTexto = dtRefVrArrecadado.HasValue ? dtRefVrArrecadado.Value.ToString("dd/MM/yyyy") : "01/01/0001";
        }

        /// <summary>
        /// Gera o relatório de despesa detalhado (geral ou limitado a uma proposta orçamentária.
        /// </summary>
        public List<RelatorioDespesaDetalhadoRegistroDTO> MontarGridDespesaDetalhado(bool fgIncluirValoresZerados, int? idPropostaOrcamentaria = null)
        {
            //Lista todas as propostas orçamentárias e agrupa as que possuirem mesma "quebra" de informações.
            var valoresPropostasItensDTO = new PropostaOrcamentariaDespesaService(context)
                .ListaPropostaOrcamentariaItens(idPropostaOrcamentaria, false)
                .GroupBy(x => new
                {
                    x.NrVersao,
                    x.IdContaContabil,
                    x.ContaContabil.NrContaContabil,
                    x.ContaContabil.NoContaContabil,
                    x.ContaContabil.NrContaContabilFmt,
                    x.ContaContabil.NrContaContabilPai,
                    x.NoEspecificacao,
                    x.NoJustificativa,
                    x.PropostaOrcamentariaDespesa.IdCentroCusto,
                    x.PropostaOrcamentariaDespesa.CentroCusto.NrCentroCusto,
                    x.PropostaOrcamentariaDespesa.CentroCusto.NoCentroCustoFmt,
                    x.NrProcessoL
                })
                .Select(g => new RelatorioDespesaDetalhadoDTO()
                {
                    NrVersao = g.Key.NrVersao,
                    IdContaContabil = g.Key.IdContaContabil,
                    NrContaContabil = g.Key.NrContaContabil,
                    NoContaContabil = g.Key.NoContaContabil,
                    NrContaContabilFmt = g.Key.NrContaContabilFmt,
                    NrContaContabilPai = g.Key.NrContaContabilPai,
                    NoEspecificacao = g.Key.NoEspecificacao,
                    NoJustificativa = g.Key.NoJustificativa,
                    IdCentroCusto = g.Key.IdCentroCusto,
                    NrCentroCusto = g.Key.NrCentroCusto,
                    NoCentroCustoFmt = g.Key.NoCentroCustoFmt,
                    VrOrcadoAntesFinalizar = g.Sum(y => y.VrOrcadoAntesFinalizar),
                    Nrprocesso = g.Key.NrProcessoL                                                            
                })
                .ToList();

            List<RelatorioDespesaDetalhadoRegistroDTO> registrosDoRelatorio = new List<RelatorioDespesaDetalhadoRegistroDTO>();

            //Lista todos os números de versões (exceto a versão atual)
            List<int> versoes = valoresPropostasItensDTO
                .Select(x => x.NrVersao).Where(x => x != 0)
                .Distinct()
                .OrderBy(x => x)
                .ToList();

            versoes.Add(0); //Adiciona a versão atual no final da lista.

            //Lista apenas as informações das DTOs que contemplam as propostas itens. Isto é necessário para gerar todas DTOs para todas versões.
            var infoDTOs = valoresPropostasItensDTO
                .Select(x => new
                {
                    x.IdContaContabil,
                    x.NrContaContabil,
                    x.NrContaContabilFmt,
                    x.NrContaContabilPai,
                    x.NoContaContabil,
                    x.IdCentroCusto,
                    x.NrCentroCusto,
                    x.NoCentroCustoFmt,
                    x.NoEspecificacao,
                    x.NoJustificativa,
                    x.Nrprocesso
                })
                .Distinct()
                .OrderBy(x => x.NrContaContabil)
                .ThenBy(x => x.NrCentroCusto)
                .Distinct()
                .ToList();

            //Para cada versão de proposta orçamentária (sendo a atual sempre a última da lista)...
            foreach (var versao in versoes)
            {
                string ultimaContaPai = string.Empty;
                string ultimaConta = string.Empty;
                string ultimoTipoDespesa = string.Empty;
                decimal somaContaAnalitica = 0;
                decimal somaContaSintetica = 0;
                decimal somaTipoDespesa = 0;
                decimal somaTotal = 0;

                ContaContabil conta;
                ContaContabil contaPai;

                //Itera sobre todas as DTOs, mesmo que algumas não possuam dados para determinadas versões (é necessário preencher com 0,00).
                for (int i = 0; i < infoDTOs.Count; i++)
                {
                    //Busca os valores (já agrupado se houver mesmo centro, conta, especificação e versão) das informações das DTOs.
                    var pItem = valoresPropostasItensDTO
                        .Where(x => x.IdCentroCusto == infoDTOs[i].IdCentroCusto)
                        .Where(x => x.IdContaContabil == infoDTOs[i].IdContaContabil)
                        .Where(x => x.NoEspecificacao == infoDTOs[i].NoEspecificacao)
                        .Where(x => x.NoJustificativa == infoDTOs[i].NoJustificativa)
                        .Where(x => x.Nrprocesso == infoDTOs[i].Nrprocesso)
                        .Where(x => x.NrVersao == versao)
                        .SingleOrDefault();

                    //Totalizador de cada conta contábil analítica
                    if (!idPropostaOrcamentaria.HasValue && !string.IsNullOrEmpty(ultimaConta) &&
                        ((pItem == null && i + 1 < infoDTOs.Count && infoDTOs[i + 1].NrContaContabil != ultimaConta) || (ultimaConta != infoDTOs[i].NrContaContabil)))
                    {
                        conta = db.Query<ContaContabil>()
                           .Where(x => x.NrAno == nrAnoBase)
                           .Where(x => x.NrContaContabil == ultimaConta.UnMask())
                           .SingleOrDefault();

                        CriaNovaLinhaRelatorioDetalhado(registrosDoRelatorio, true, "VERDE", conta.NrContaContabilFmt, "", conta.NoContaContabil, versao, "", "", somaContaAnalitica,"");
                        somaContaAnalitica = 0;
                    }

                    //Totalizador de cada conta sintética
                    if (!idPropostaOrcamentaria.HasValue && !string.IsNullOrEmpty(ultimaContaPai) &&
                        ((pItem == null && i + 1 < infoDTOs.Count && infoDTOs[i + 1].NrContaContabilPai != ultimaContaPai) || (ultimaContaPai != infoDTOs[i].NrContaContabilPai)))
                    {
                        contaPai = db.Query<ContaContabil>()
                            .Where(x => x.NrAno == nrAnoBase)
                            .Where(x => x.NrContaContabil == ultimaContaPai.UnMask())
                            .SingleOrDefault();

                        CriaNovaLinhaRelatorioDetalhado(registrosDoRelatorio, true, "AMARELO", contaPai.NoContaContabil.ToUpper(), "", "", versao, "", "", somaContaSintetica,"");
                        somaContaSintetica = 0;
                    }

                    //Totalizador de tipo de despesa: CORRENTE ou CAPITAL
                    if (!idPropostaOrcamentaria.HasValue && !string.IsNullOrEmpty(ultimoTipoDespesa) &&
                        ((pItem == null && i + 1 < infoDTOs.Count && infoDTOs[i + 1].NrContaContabil?.Substring(0, 5) != ultimoTipoDespesa) || (ultimoTipoDespesa != infoDTOs[i].NrContaContabil.Substring(0, 5))))
                    {
                        CriaNovaLinhaRelatorioDetalhado(registrosDoRelatorio, true, "VERMELHO", ultimoTipoDespesa == "52211" ? "TOTAL DESPESA CORRENTE" : "TOTAL DESPESA DE CAPITAL", "", "", versao, "", "", somaTipoDespesa, "");
                        somaTipoDespesa = 0;
                    }

                    //Cria pelo menos uma linha para cada DTO, mesmo se não possuir valor.
                    if (fgIncluirValoresZerados || 
                        PossuiValorEmAlgumaVersao(valoresPropostasItensDTO, infoDTOs[i].IdCentroCusto, infoDTOs[i].IdContaContabil, infoDTOs[i].NoEspecificacao, infoDTOs[i].NoJustificativa))
                    {
                        CriaNovaLinhaRelatorioDetalhado(registrosDoRelatorio, false, "",
                        infoDTOs[i].NoCentroCustoFmt, infoDTOs[i].NrContaContabilFmt, infoDTOs[i].NoContaContabil, versao,
                        infoDTOs[i].NoEspecificacao, infoDTOs[i].NoJustificativa, pItem?.VrOrcadoAntesFinalizar, infoDTOs[i].Nrprocesso);
                    }

                    if (pItem != null)
                    {
                        //Se uma proposta orçamentária item for encontrada, soma nos totalizadores.
                        somaContaAnalitica += pItem.VrOrcadoAntesFinalizar.HasValue ? pItem.VrOrcadoAntesFinalizar.Value : 0;
                        somaContaSintetica += pItem.VrOrcadoAntesFinalizar.HasValue ? pItem.VrOrcadoAntesFinalizar.Value : 0;
                        somaTipoDespesa += pItem.VrOrcadoAntesFinalizar.HasValue ? pItem.VrOrcadoAntesFinalizar.Value : 0;
                        somaTotal += pItem.VrOrcadoAntesFinalizar.HasValue ? pItem.VrOrcadoAntesFinalizar.Value : 0;
                    }

                    //Sempre atualiza a última conta, última conta pai e último tipo de despesa.
                    ultimaConta = infoDTOs[i].NrContaContabil;
                    ultimaContaPai = infoDTOs[i].NrContaContabilPai;
                    ultimoTipoDespesa = infoDTOs[i].NrContaContabil.Substring(0, 5);
                }

                //Se não for de um centro de custo específico, mostrar todos os totalizadores.
                if (!idPropostaOrcamentaria.HasValue)
                {
                    //Totalizador de cada conta contábil analítica
                    conta = db.Query<ContaContabil>()
                       .Where(x => x.NrAno == nrAnoBase)
                       .Where(x => x.NrContaContabil == ultimaConta.UnMask())
                       .SingleOrDefault();

                    if (conta != null)
                    {
                        CriaNovaLinhaRelatorioDetalhado(registrosDoRelatorio, true, "VERDE", conta.NrContaContabilFmt, "", conta.NoContaContabil, versao, "", "", somaContaAnalitica, "");
                        somaContaAnalitica = 0;

                        //Totalizador de cada conta sintética
                        contaPai = db.Query<ContaContabil>()
                            .Where(x => x.NrAno == nrAnoBase)
                            .Where(x => x.NrContaContabil == ultimaContaPai.UnMask())
                            .SingleOrDefault();

                        CriaNovaLinhaRelatorioDetalhado(registrosDoRelatorio, true, "AMARELO", contaPai.NoContaContabil.ToUpper(), "", "", versao, "", "", somaContaSintetica, "");
                        somaContaSintetica = 0;

                        //Totalizador de tipo de despesa: CORRENTE ou CAPITAL
                        CriaNovaLinhaRelatorioDetalhado(registrosDoRelatorio, true, "VERMELHO", ultimoTipoDespesa == "52211" ? "TOTAL DESPESA CORRENTE" : "TOTAL DESPESA DE CAPITAL", "", "", versao, "", "", somaTipoDespesa, "");
                        somaTipoDespesa = 0;
                    }
                }

                //Totalizador geral da planilha
                if (registrosDoRelatorio.Count > 0)
                {
                    CriaNovaLinhaRelatorioDetalhado(registrosDoRelatorio, true, "ROXO", "TOTAL GERAL", "", "", versao, "", "", somaTotal, "");
                    somaTipoDespesa = 0;
                }
            }

            return registrosDoRelatorio;
        }

        /// <summary>
        /// Retorna um datatable com os dados referentes ao ano de uma metodologia de receita.
        /// </summary>
        /// <param name="idContaContabil">Define de qual metodologia os dados serão trazidos.</param>
        /// <returns></returns>
        public List<RelatorioMetodologiaDTO> MontarGridMetodologiaReceitaAno(bool fgIncluirValoresZerados, int? idContaContabil = null) //Demonstrativo Metodologia da Receita
        {
            CarregaValoresIniciais(true, true);

            List<RelatorioMetodologiaDTO> registrosDoRelatorio = new List<RelatorioMetodologiaDTO>();

            var metodologias = new MetodologiaReceitaService(context).BuscaMetodologias(_parametroAno.NrAno);

            if (idContaContabil.HasValue)
            {
                metodologias = metodologias.Where(x => x.IdContaContabil == idContaContabil.Value).ToList();
            }                                                                                                                                           

            foreach (var m in metodologias )
            {
                if (m.VrOrcado > 0 || fgIncluirValoresZerados)
                {
                    RelatorioMetodologiaDTO registroRelatorio = new RelatorioMetodologiaDTO();
                    registroRelatorio.NrAno = _parametroAno.NrAno;
                    registroRelatorio.NrContaContabil = m.ContaContabil.NrContaContabilFmt;
                    registroRelatorio.NoContaContabil = m.ContaContabil.NoContaContabil;
                    registroRelatorio.VrOrcado = m.VrOrcado.HasValue ? m.VrOrcado.Value : 0;
                    registroRelatorio.NoObservacoes = m.NoObservacoes;
             
                    registroRelatorio.VrReceitaBrutaJan = RetornaReceitaBruta(m, "Janeiro");
                    registroRelatorio.VrReceitaLiquidaJan = RetornaReceitaLiquida(m, "Janeiro");

                    registroRelatorio.VrReceitaBrutaFev = RetornaReceitaBruta(m, "Fevereiro");
                    registroRelatorio.VrReceitaLiquidaFev = RetornaReceitaLiquida(m, "Fevereiro");

                    registroRelatorio.VrReceitaBrutaMar = RetornaReceitaBruta(m, "Março");
                    registroRelatorio.VrReceitaLiquidaMar = RetornaReceitaLiquida(m, "Março");

                    registroRelatorio.VrReceitaBrutaAbr = RetornaReceitaBruta(m, "Abril");
                    registroRelatorio.VrReceitaLiquidaAbr = RetornaReceitaLiquida(m, "Abril");

                    registroRelatorio.VrReceitaBrutaMai = RetornaReceitaBruta(m, "Maio");
                    registroRelatorio.VrReceitaLiquidaMai = RetornaReceitaLiquida(m, "Maio");

                    registroRelatorio.VrReceitaBrutaJun = RetornaReceitaBruta(m, "Junho");
                    registroRelatorio.VrReceitaLiquidaJun = RetornaReceitaLiquida(m, "Junho");

                    registroRelatorio.VrReceitaBrutaJul = RetornaReceitaBruta(m, "Julho");
                    registroRelatorio.VrReceitaLiquidaJul = RetornaReceitaLiquida(m, "Julho");

                    registroRelatorio.VrReceitaBrutaAgo = RetornaReceitaBruta(m, "Agosto");
                    registroRelatorio.VrReceitaLiquidaAgo = RetornaReceitaLiquida(m, "Agosto");

                    registroRelatorio.VrReceitaBrutaSet = RetornaReceitaBruta(m, "Setembro");
                    registroRelatorio.VrReceitaLiquidaSet = RetornaReceitaLiquida(m, "Setembro");

                    registroRelatorio.VrReceitaBrutaOut = RetornaReceitaBruta(m, "Outubro");
                    registroRelatorio.VrReceitaLiquidaOut = RetornaReceitaLiquida(m, "Outubro");

                    registroRelatorio.VrReceitaBrutaNov = RetornaReceitaBruta(m, "Novembro");
                    registroRelatorio.VrReceitaLiquidaNov = RetornaReceitaLiquida(m, "Novembro");

                    registroRelatorio.VrReceitaBrutaDez = RetornaReceitaBruta(m, "Dezembro");
                    registroRelatorio.VrReceitaLiquidaDez = RetornaReceitaLiquida(m, "Dezembro");
                                      
                    registroRelatorio.PrCotaParteConfea = m.PrCotaParteConfea.HasValue ? m.PrCotaParteConfea.Value : 0;
                    registroRelatorio.PrCotaParteMutua = m.PrCotaParteMutua.HasValue ? m.PrCotaParteMutua.Value : 0;
                    registroRelatorio.PrCotaParteMutuaSP = m.PrSaoPaulo.HasValue ? m.PrSaoPaulo.Value : 0;
                    registroRelatorio.PrCotaParteMutuaBSB = m.PrBrasilia.HasValue ? m.PrBrasilia.Value : 0;

                    registroRelatorio.VrCotaParteConfea = m.VrCotaParteConfea.HasValue ? m.VrCotaParteConfea.Value : 0;
                    registroRelatorio.VrCotaParteMutua = m.VrCotaParteMutua.HasValue ? m.VrCotaParteMutua.Value : 0;
                    registroRelatorio.VrCotaParteMutuaSP = m.VrCotaSaoPaulo.HasValue ? m.VrCotaSaoPaulo.Value : 0;
                    registroRelatorio.VrCotaParteMutuaBSB = m.VrCotaBrasilia.HasValue ? m.VrCotaBrasilia.Value : 0;

                    registrosDoRelatorio.Add(registroRelatorio);
                }
            }

            return registrosDoRelatorio;
        }

        private decimal RetornaReceitaBruta(MetodologiaReceitaAno metodologia, string noMes)
        {
            var metodologiaMes = metodologia.MetodologiasReceitaMes.Where(x => x.NoMes == noMes).Single();
            metodologiaMes.CalculaVrReceitaBruta(metodologia.VrOrcado);
            return metodologiaMes.VrReceitaBruta.HasValue ? metodologiaMes.VrReceitaBruta.Value : 0;
        }

        private decimal RetornaReceitaLiquida(MetodologiaReceitaAno metodologia, string noMes)
        {
            var metodologiaMes = metodologia.MetodologiasReceitaMes.Where(x => x.NoMes == noMes).Single();
            metodologiaMes.CalculaVrReceitaLiquida(metodologia.VrOrcado, metodologia.PrCotaParteConfea, metodologia.PrCotaParteMutua);
            return metodologiaMes.VrReceitaLiquida.HasValue ? metodologiaMes.VrReceitaLiquida.Value : 0;
        }

        /// <summary>
        /// Cria uma nova linha especificamente no relatório detalhado de despesa.
        /// </summary>
        /// <param name="dt">DataTable em que será inserido a linha.</param>
        /// <param name="fgBold">Linha em negrito</param>
        /// <param name="cor">Background Color da linha</param>
        /// <param name="noCentroCustoFmt">Nome do centro de custo (formatado para aparecer o número também)</param>
        /// <param name="nrContaContabil">Número da conta contábil</param>
        /// <param name="noContaContabil">Nome da conta contábil</param>
        /// <param name="noEspecificacao">Texto de especificação.</param>
        /// <param name="vrOrcado">Valor orçado</param>
        private void CriaNovaLinhaRelatorioDetalhado(List<RelatorioDespesaDetalhadoRegistroDTO> dtos, bool fgBold, string cor, string noCentroCustoFmt, string nrContaContabil, string noContaContabil, int nrVersao, string noEspecificacao, string noJustificativa, decimal? vrOrcado, string NrProcesso)
        {
            RelatorioDespesaDetalhadoRegistroDTO registroRelatorio = new RelatorioDespesaDetalhadoRegistroDTO();
            registroRelatorio.NrAno = nrAnoBase;
            registroRelatorio.Bold = fgBold;
            registroRelatorio.Cor = cor;
            registroRelatorio.NoCentroCustoFmt = noCentroCustoFmt;
            registroRelatorio.NrContaContabil = nrContaContabil;
            registroRelatorio.NoContaContabil = noContaContabil;

            if (nrVersao == 0)
                registroRelatorio.NrVersao = "Atual";
            else
                registroRelatorio.NrVersao = nrVersao + "ª proposta";

            registroRelatorio.NoEspecificacao = noEspecificacao;
            registroRelatorio.NoJustificativa = noJustificativa;
            registroRelatorio.VrOrcado = vrOrcado.HasValue ? vrOrcado.Value : 0;
            registroRelatorio.AgrupadorLinhas = string.Concat(noCentroCustoFmt, nrContaContabil, noContaContabil, noEspecificacao, noJustificativa);
            registroRelatorio.NrProcesso = NrProcesso;

            dtos.Add(registroRelatorio);
        }

        private bool PossuiValorEmAlgumaVersao(List<RelatorioDespesaDetalhadoDTO> valoresPropostasItensDTO, int idCentroCusto, int idContaContabil, string noEspecificacao, string noJutificativa)
        {
            var possuiAlgumValorEmAlgumaVersao = valoresPropostasItensDTO
                        .Where(x => x.IdCentroCusto == idCentroCusto)
                        .Where(x => x.IdContaContabil == idContaContabil)
                        .Where(x => x.NoEspecificacao == noEspecificacao)
                        .Where(x => x.NoJustificativa == noJutificativa)
                        .Where(x => x.VrOrcadoAntesFinalizar > 0)
                        .Any();

            return possuiAlgumValorEmAlgumaVersao;
        }
    }
}
