﻿using Creasp.Core;
using Creasp.Elo;
using NBox.Core;
using NBox.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Elo
{
    public class ParametroAnoService : DataService
    {
        public ParametroAnoService(ServiceContext context) : base(context) { }

        /// <summary>
        /// Lista os parâmetros anos
        /// </summary>
        public List<ParametroAno> ListaParametrosAnos()
        {
            return db.Query<ParametroAno>()
                .ToList();
        }

        /// <summary>
        /// Busca o parâmetro ano pelo ano.
        /// </summary>
        public ParametroAno BuscaParametroAno(int nrAno)
        {
            var parametroAno = db.Query<ParametroAno>().SingleOrDefault(x => x.NrAno == (nrAno));

            if (parametroAno == null)
            {
                throw new NBoxException("Ano não encontrado.");
            }

            return parametroAno;
        }

        /// <summary>
        /// Valida se o parâmetro já existe e, caso não exista, insere na base de dados.
        /// </summary>
        public void Salvar(int nrAno, decimal? prAplicadoReceita, decimal? prAplicadoDespesa,
            decimal? prCotaParteConfea, decimal? prCotaParteMutua, string noObservacoes, string deDiretrizesAosExecutores)
        {
            if (db.Query<ParametroAno>().Where(x => x.NrAno == nrAno).Any())
            {
                throw new NBoxException("Parâmetro de ano já existente.");
            }

            var ano = new ParametroAno();

            ano.NrAno = nrAno;
            ano.PrAplicadoDespesa = prAplicadoDespesa;
            ano.PrAplicadoReceita = prAplicadoReceita;
            ano.PrCotaParteConfea = prCotaParteConfea;
            ano.PrCotaParteMutua = prCotaParteMutua;
            ano.NoObservacoesReceitaPadrao = noObservacoes;
            ano.DtDisponibilizacao = null;
            ano.NoDiretrizes = deDiretrizesAosExecutores;

            db.Insert(ano);
        }

        /// <summary>
        /// Altera o parâmetro ano, atualizando os dados de metodologia da receita baseado nos novos parâmetros.
        /// </summary>
        public void AtualizaParametroAno(int nrAno, decimal prAplicadoReceita, decimal prCotaParteConfea, decimal prCotaParteMutua, string noObservacoesPadraoReceita)
        {
            using (var trans = db.GetTransaction())
            {
                var metodologiaReceitaService = new MetodologiaReceitaService(context);
                metodologiaReceitaService.AtualizaMetodologias(nrAno, prAplicadoReceita, prCotaParteConfea, prCotaParteMutua, noObservacoesPadraoReceita, null);

                var parametroAno = BuscaParametroAno(nrAno);

                parametroAno.PrAplicadoReceita = prAplicadoReceita;
                parametroAno.PrCotaParteConfea = prCotaParteConfea;
                parametroAno.PrCotaParteMutua = prCotaParteMutua;
                parametroAno.NoObservacoesReceitaPadrao = noObservacoesPadraoReceita;

                db.Update(parametroAno);

                trans.Complete();
            }
        }

        /// <summary>
        /// Disponibiliza um parâmetro ano que ainda não tenha sido disponibilizado. Esta ação cria todas propostas orçamentárias de despesa e as metodologias de receita do ano.
        /// </summary>
        public void Disponibilizar(int nrAno)
        {
            using (var trans = db.GetTransaction())
            {
                var ano = BuscaParametroAno(nrAno);

                if (ano.DtDisponibilizacao != null)
                {
                    throw new NBoxException("Ano já disponibilizado. Recarregue a página se necessário.");
                }

                if (!(new CentroCustoPerfilService(context).TodosPerfisEstaoVinculados(ano.NrAno)))
                {
                    throw new NBoxException("Todos os perfis de centro de custo devem estar vinculados a um centro de custo.");
                }

                if (!(new CentroCustoResponsavelService(context).ValidaCentroCustosOrcaveisComExecutor(ano.NrAno)))
                {
                    throw new NBoxException("Todos centros de custos orçáveis devem ter executores cadastrados.");
                }

                var metodologiaReceitaService = new MetodologiaReceitaService(context);
                var propostaOrcamentariaDespesaService = new PropostaOrcamentariaDespesaService(context);

                metodologiaReceitaService.CriaMetodologiaReceita(ano.NrAno, ano.PrAplicadoReceita, ano.PrCotaParteConfea, ano.PrCotaParteMutua, ano.NoObservacoesReceitaPadrao);
                propostaOrcamentariaDespesaService.CriaPropostasOrcamentarias(ano);

                if (!PropostasEMetodologiasCriadas(ano.NrAno))
                {
                    throw new NBoxException("Não foi possível disponibilizar o ano, alguma configuração impediu que as propostas ou metodologias fossem criadas corretamente.");
                }

                ano.DtDisponibilizacao = DateTime.Now;
                db.AuditUpdate(ano);

                trans.Complete();
            }
        }

        /// <summary>
        /// Verifica se o parâmetro ano pode ser editado.
        /// </summary>
        public bool PodeEditarParametroAno(int nrAno)
        {
            var parametroAno = BuscaParametroAno(nrAno);
            return !db.Query<Reformulacao>().Any(x => x.NrAno == parametroAno.NrAno);
        }

        /// <summary>
        /// Valida se foram existem propostas orçamentárias e metodologias de receita.
        /// </summary>
        private bool PropostasEMetodologiasCriadas(int nrAno)
        {
            var temPropostasItens = db.Query<PropostaOrcamentariaDespesaItem>()
                .Include(x => x.PropostaOrcamentariaDespesa)
                .Where(x => x.PropostaOrcamentariaDespesa.NrAno == nrAno)
                .Any();

            var temMetodologiasMes = db.Query<MetodologiaReceitaMes>()
                .Include(x => x.MetodologiaReceitaAno)
                .Where(x => x.MetodologiaReceitaAno.NrAno == nrAno)
                .Any();

            return temMetodologiasMes && temPropostasItens;
        }

        //TODO: Aproveitar este código da forma certa ou jogar fora (os 4 métodos abaixo). Cuidado com os "parâmetros fixos".
        public void GeraRelacaoCentroConta()
        {
            var orcamentoService = new OrcamentoService(context);
            var dotacoes = orcamentoService.ListaDotacoes(DateTime.Now.Year - 2, DateTime.Now.Year, null).Select(x => new { CentroCusto = x.NrCentroCusto, ContaContabil = x.NrContaContabil5 }).ToList();
            var posicaoAtual = orcamentoService.ListaPosicaoAtualOrcamento(DateTime.Now.Year - 2, DateTime.Now.Year).Select(x => new { CentroCusto = x.NrCentroCusto, ContaContabil = x.NrContaContabil5 }).ToList();

            var relacaoCentroConta = dotacoes.Union(posicaoAtual).Where(x => x.CentroCusto != null && x.ContaContabil != null).Distinct().ToList();

            using (var trans = db.GetTransaction())
            {
                foreach (var r in relacaoCentroConta)
                {
                    var conta = db.Query<ContaContabil>().Where(x => x.NrAno == 2017 && x.NrContaContabil == r.ContaContabil && x.FgAtivo).SingleOrDefault();
                    var centro = db.Query<CentroCusto>().Where(x => x.NrAno == 2017 && x.NrCentroCusto == r.CentroCusto && x.FgAtivo).SingleOrDefault();
                    if (!db.Query<ContaContabil_CentroCusto>().Any(x => x.IdCentroCusto == centro.IdCentroCusto && x.IdContaContabil == conta.IdContaContabil))
                    {
                        db.Insert(new ContaContabil_CentroCusto() { IdCentroCusto = centro.IdCentroCusto, IdContaContabil = conta.IdContaContabil, FgInseridoPorUsuario = false });
                    }
                }

                trans.Complete();
            }
        }

        public void DuplicaCentrosOrcaveis()
        {
            var centros = db.Query<CentroCusto>()
                //.Include(x => x.CentrosCustosResponsaveis)
                .Where(x => x.NrAno == 2017)
                .ToList();

            //TODO: TESTAR PERFORMANCE
            var ccrService = new CentroCustoResponsavelService(context);
            centros.ForEach(x => x.CentroCustoResponsavel = ccrService.BuscaCentroCustoResponsavel(x.IdCentroCusto, null));

            var centrosOrcados = db.Query<CentroCusto>()
                            //.Include(x => x.CentrosCustosResponsaveis)
                            //.Where(x => x.CentroCustoResponsavel.FgOrcavel)
                            .ToList();

            //TODO: TESTAR PERFORMANCE
            centrosOrcados.ForEach(x => x.CentroCustoResponsavel = ccrService.BuscaCentroCustoResponsavel(x.IdCentroCusto, true));

            foreach (var c in centros)
            {
                if (!db.Query<CentroCustoResponsavel>().Any(x => x.IdCentroCusto == c.IdCentroCusto))
                {
                    db.Insert(new CentroCustoResponsavel()
                    {
                        IdCentroCusto = c.IdCentroCusto,
                        IdPessoaExecutor = 1,
                        FgOrcavel = centrosOrcados.Where(x => x.NrCentroCusto == c.NrCentroCusto).Any()
                    });
                }
                //var centroParaTornarOrcavel = db.Query<CentroCustoResponsavel>()
                //    .Include(x => x.CentroCusto)
                //    .Where(x => x.CentroCusto.NrCentroCusto == c.NrCentroCusto && x.CentroCusto.NrAno == 2017)
                //    .SingleOrDefault();

                //centroParaTornarOrcavel.FgOrcavel = true;
                //db.Update(centroParaTornarOrcavel);
            }
        }
    }
}
