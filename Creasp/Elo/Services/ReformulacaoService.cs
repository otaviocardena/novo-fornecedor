﻿using Creasp.Core;
using Creasp.Elo.DTO;
using NBox.Core;
using NBox.Services;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Creasp.Elo
{
    public class ReformulacaoService : DataService
    {
        protected readonly int nrAnoBase;

        public ReformulacaoService(ServiceContext context) : base(context)
        {
            nrAnoBase = (context as CreaspContext).NrAnoBase;
        }

        List<DotacaoOrcamentaria> _dotacoesAnteriores;
        List<PosicaoAtualOrcamento> _posicaoAtualOrcamento;
        List<OrcamentoSenior> _orcamentosSenior;
        List<ContaContabil_CentroCusto> _relacaoContaCentro;

        /// <summary>
        /// Lista todas as refomrulações
        /// </summary>
        public QueryPage<Reformulacao> ListaReformulacoes(Pager p)
        {
            var reformulacoes = db.Query<Reformulacao>()
                .Where(x => x.NrAno == nrAnoBase)
                .ToPage(p.DefaultSort<Reformulacao>(x => x.IdReformulacao));

            return reformulacoes;
        }

        public List<Reformulacao> ListaReformulacoes()
        {
            var reformulacoes = db.Query<Reformulacao>()
                .Where(x => x.NrAno == nrAnoBase)
                .OrderByDescending(x => x.IdReformulacao)
                .ToList();

            return reformulacoes;
        }

        public QueryPage<ReformulacaoOrcamentaria> ListaReformulacaoOrcamentaria(Pager p, int idReformulacao,
            string nrCentroCusto = null,
            string noCentroCusto = null,
            string noResponsavel = null,
            string cdStatusProposta = null,
            string cdTipoFormulario = null)
        {
            var pessoaService = new PessoaService(context);

            bool isContabilidade = new CentroCustoPerfilService(context).ValidaUsuarioLogadoPossuiPerfil(TpPerfilCentroCusto.Contabilidade);
            int idUsuarioLogado = user != null ? (user as CreaspUser).IdPessoa : 0;

            List<int?> subDelegacoes = pessoaService.ListaDelegacoes(idUsuarioLogado).ToList();

            var reformulacoesOrcamentarias = db.Query<ReformulacaoOrcamentaria>()
                .Include(x => x.Reformulacao)
                .Include(x => x.CentroCustoOriginal)
                .Include(x => x.CentroCusto)
                //.Include(x => x.CentroCusto.CentroCustoResponsavel)
                //.Include(x => x.CentroCusto.CentroCustoResponsavel.ResponsavelExecucao)
                //.Include(x => x.CentroCusto.CentroCustoResponsavel.ResponsavelAprova)
                //.Include(x => x.CentroCusto.CentroCustoResponsavel.ResponsavelAprova2)
                //.Include(x => x.CentroCusto.CentroCustoResponsavel.ResponsavelAprova3)
                //.Include(x => x.CentroCusto.CentroCustoResponsavel.ResponsavelAprova4)
                .Include(x => x.Responsavel)
                .Include(x => x.StatusPropostaOrcamentaria)
                //.IncludeMany(x => x.ReformulacaoOrcamentariaItens)
                .Where(nrCentroCusto != null, x => x.CentroCusto.NrCentroCusto.StartsWith(nrCentroCusto.UnMask()))
                .Where(noCentroCusto != null, x => x.CentroCusto.NoCentroCusto.Contains(noCentroCusto.ToLike()))
                .Where(noResponsavel != null, x => x.Responsavel.NoPessoa.StartsWith(noResponsavel.ToLike()))
                .Where(cdStatusProposta != null, x => x.CdStatusPropostaOrcamentaria == cdStatusProposta)
                .Where(cdTipoFormulario != null, x => x.CdTpFormulario == cdTipoFormulario)
                .Where(x => x.IdReformulacao == idReformulacao)
                .ToList();

            var ccrService = new CentroCustoResponsavelService(context);
            reformulacoesOrcamentarias.ForEach(x => x.CentroCusto.CentroCustoResponsavel = ccrService.BuscaCentroCustoResponsavel(x.IdCentroCusto, null));

            var pagedRefsOrcamentarias = reformulacoesOrcamentarias.Where(x => isContabilidade
                        || subDelegacoes.Contains(x.CentroCusto.CentroCustoResponsavel.IdPessoaExecutor)
                        || subDelegacoes.Contains(x.CentroCusto.CentroCustoResponsavel.IdPessoaAprovador1)
                        || subDelegacoes.Contains(x.CentroCusto.CentroCustoResponsavel.IdPessoaAprovador2)
                        || subDelegacoes.Contains(x.CentroCusto.CentroCustoResponsavel.IdPessoaAprovador3)
                        || subDelegacoes.Contains(x.CentroCusto.CentroCustoResponsavel.IdPessoaAprovador4)
                        || subDelegacoes.Contains(x.Responsavel.IdPessoa))
                .ToPage(p.DefaultSort<ReformulacaoOrcamentaria>(x => x.CentroCusto.NrCentroCusto));

            var itens = db.Query<ReformulacaoOrcamentariaItem>()
                //.Include(x => x.ReformulacaoOrcamentaria)
                //.Include(x => x.ReformulacaoOrcamentaria.Reformulacao)
                //.Where(x => x.ReformulacaoOrcamentaria.Reformulacao.NrAno == nrAnoBase)
                .ToList();

            pagedRefsOrcamentarias.Items.ForEach(x => x.ReformulacaoOrcamentariaItens = itens.Where(y => y.IdReformulacaoOrcamentaria == x.IdReformulacaoOrcamentaria).ToList());

            return pagedRefsOrcamentarias;
        }

        public List<ReformulacaoOrcamentariaItem> BuscaReformulacaoOrcamentariaItens(int idReformulacaoOrcamentaria)
        {
            var reformulacaoItens = db.Query<ReformulacaoOrcamentariaItem>()
                .Include(x => x.ContaContabil)
                .Include(x => x.ReformulacaoOrcamentaria)
                .Include(x => x.ReformulacaoOrcamentaria.Reformulacao)
                .Where(x => x.IdReformulacaoOrcamentaria == idReformulacaoOrcamentaria)
                //.Where(x => x.ReformulacaoOrcamentaria.Reformulacao.NrAno == nrAnoBase)
                .OrderBy(x => x.ContaContabil.NrContaContabil)
                .ToList();

            return reformulacaoItens;
        }

        public void IncluiReformulacaoOrcamentaria(int idReformulacao, string nrCentroCusto)
        {
            using (var trans = db.GetTransaction())
            {
                int nrAno = BuscaReformulacao(idReformulacao).NrAno;

                var dotacaoService = new OrcamentoService(context);
                var relacaoContaCentroService = new ContaContabilCentroCustoService(context);
                var centroCustoResponsavelService = new CentroCustoResponsavelService(context);
                var centroDeCustoResponsavel = centroCustoResponsavelService.BuscaCentroCustoResponsavel(nrCentroCusto);

                if (_dotacoesAnteriores == null) _dotacoesAnteriores = dotacaoService.ListaDotacoes(nrAno - 1, nrAno - 1, false);
                if (_posicaoAtualOrcamento == null) _posicaoAtualOrcamento = dotacaoService.ListaPosicaoAtualOrcamento(nrAno - 1, nrAno - 1);
                if (_orcamentosSenior == null) _orcamentosSenior = dotacaoService.ListaOrcamentoSenior(nrAno);
                if (_relacaoContaCentro == null) _relacaoContaCentro = relacaoContaCentroService.ListaRelacaoContaContabilComCentroCustoOrcavel().OrderBy(x => x.IdCentroCusto).ToList();

                ReformulacaoOrcamentaria refOrcamentaria = new ReformulacaoOrcamentaria();

                refOrcamentaria.IdReformulacao = idReformulacao;
                refOrcamentaria.IdCentroCusto = centroDeCustoResponsavel.IdCentroCusto;
                refOrcamentaria.CdStatusPropostaOrcamentaria = StPropostaOrcamentaria.Executor;
                refOrcamentaria.IdPessoaResp = centroDeCustoResponsavel.ResponsavelExecucao?.IdPessoa;

                db.Insert(refOrcamentaria);

                foreach (var contaContabilCentroCusto in _relacaoContaCentro.Where(x => x.IdCentroCusto == centroDeCustoResponsavel.IdCentroCusto))
                {
                    var orcamentoSenior = _orcamentosSenior
                        .Where(x => x.NrAno == nrAno)
                        .Where(x => ("0" + x.NrCentroCusto).StartsWith(nrCentroCusto))
                        .Where(x => x.NrContaContabil5 == contaContabilCentroCusto.ContaContabil.NrContaContabil)
                        .Sum(x => x.VrOrcamento);

                    var dotacaoAnoAnterior = _dotacoesAnteriores
                        .Where(x => x.NrAno == nrAno - 1)
                        .Where(x => x.NrCentroCusto == nrCentroCusto)
                        .Where(x => x.NrContaContabil5 == contaContabilCentroCusto.ContaContabil.NrContaContabil)
                        .SingleOrDefault();

                    var posicaoOrcamento = _posicaoAtualOrcamento
                        .Where(x => x.NrAno == nrAno - 1)
                        .Where(x => x.NrCentroCusto == nrCentroCusto)
                        .Where(x => x.NrContaContabil5 == contaContabilCentroCusto.ContaContabil.NrContaContabil)
                        .SingleOrDefault();

                    ReformulacaoOrcamentariaItem refItem = new ReformulacaoOrcamentariaItem();

                    refItem.IdReformulacaoOrcamentaria = refOrcamentaria.IdReformulacaoOrcamentaria;
                    refItem.IdContaContabil = contaContabilCentroCusto.IdContaContabil;
                    refItem.VrOrcadoInicialmente = 0;
                    refItem.VrDotacaoAtual = posicaoOrcamento?.VrEmpExercicio + posicaoOrcamento?.VrSaldoOrcamentarioExercicio;
                    refItem.VrExecutado = posicaoOrcamento?.VrExecutado;
                    refItem.DtRefVrExecutado = posicaoOrcamento?.DtRefVrExecutado;

                    db.Insert(refItem);
                }

                trans.Complete();
            }
            //NotificaNovoResponsavel(proposta, false, (user as CreaspUser).IdPessoa);
        }

        public decimal BuscaTotalOrcadoAno()
        {
            var soma = db.Query<ReformulacaoOrcamentariaItem>()
                .Include(x => x.ReformulacaoOrcamentaria)
                .Include(x => x.ReformulacaoOrcamentaria.Reformulacao)
                .Where(x => x.ReformulacaoOrcamentaria.Reformulacao.NrAno == nrAnoBase)
                .ToList()?
                .Sum(x => x.VrOrcamentoReformulado);

            return soma.HasValue ? soma.Value : 0;
        }

        public List<ReformulacaoOrcamentariaItem> ListaReformulacaoOrcamentariaItensMaisRecenteDoAno()
        {
            var idReformulacaoMaisRecente = BuscaIdReformulacaoMaisRecente();

            var reformulacaoItens = db.Query<ReformulacaoOrcamentariaItem>()
                .Include(x => x.ContaContabil)
                .Include(x => x.ReformulacaoOrcamentaria)
                .Include(x => x.ReformulacaoOrcamentaria.Reformulacao)
                .Where(x => x.ReformulacaoOrcamentaria.Reformulacao.NrAno == nrAnoBase)
                .Where(x => x.ReformulacaoOrcamentaria.IdReformulacao == idReformulacaoMaisRecente)
                .ToList();

            return reformulacaoItens;
        }

        public List<ReformulacaoOrcamentariaItem> ListaReformulacaoOrcamentariaItens(int idReformulacao)
        {
            //var idReformulacaoMaisRecente = BuscaIdReformulacaoMaisRecente();

            var reformulacaoItens = db.Query<ReformulacaoOrcamentariaItem>()
                .Include(x => x.ContaContabil)
                .Include(x => x.ReformulacaoOrcamentaria)
                .Include(x => x.ReformulacaoOrcamentaria.Reformulacao)
                .Where(x => x.ReformulacaoOrcamentaria.Reformulacao.NrAno == nrAnoBase)
                .Where(x => x.ReformulacaoOrcamentaria.IdReformulacao == idReformulacao)
                .ToList();

            return reformulacaoItens;
        }

        public List<MetodologiaReceitaAno> ListaMetodologias(int idReformulacao)
        {
            //var idReformulacaoMaisRecente = BuscaIdReformulacaoMaisRecente();

            var metodologias = db.Query<MetodologiaReceitaAno>()
                .Include(x => x.ContaContabil)
                .Include(x => x.Reformulacao)
                .Where(x => x.Reformulacao.NrAno == nrAnoBase)
                .Where(x => x.IdReformulacao == idReformulacao)
                .ToList();

            return metodologias;
        }

        public Reformulacao BuscaReformulacao(int idReformulacao)
        {
            return db.SingleById<Reformulacao>(idReformulacao);
        }

        public List<ReformulacaoOrcamentaria> BuscaReformulacoesOrcamentarias(int idReformulacao)
        {
            return db.Query<ReformulacaoOrcamentaria>()
                .Include(x => x.Reformulacao)
                .Include(x => x.CentroCusto)
                .Where(x => x.IdReformulacao == idReformulacao)
                .ToList();
        }

        public ReformulacaoOrcamentaria BuscaReformulacaoOrcamentaria(int idReformulacaoOrcamentaria)
        {
            var refOrcamentaria = db.Query<ReformulacaoOrcamentaria>()
                .Include(x => x.CentroCustoOriginal)
                .Include(x => x.CentroCusto)
                //.Include(x => x.CentroCusto.CentroCustoResponsavel)
                .Include(x => x.Reformulacao)
                .Include(x => x.Responsavel)
                .Where(x => x.IdReformulacaoOrcamentaria == idReformulacaoOrcamentaria)
                .SingleOrDefault();

            if (refOrcamentaria != null)
            {
                var ccrService = new CentroCustoResponsavelService(context);
                refOrcamentaria.CentroCusto.CentroCustoResponsavel = ccrService.BuscaCentroCustoResponsavel(refOrcamentaria.IdCentroCusto, null);
            }

            return refOrcamentaria;
        }

        /// <summary>
        /// Lista todos os status das reformulações orçamentárias da reformulação indicada. Sem repetições na lista.
        /// </summary>
        public List<string> ListaStatusDasReformulacoes(int idReformulacao)
        {
            var reformulacoesOrcamentarias = BuscaReformulacoesOrcamentarias(idReformulacao);
            return reformulacoesOrcamentarias.Select(x => x.CdStatusPropostaOrcamentaria).Distinct().ToList();
        }

        /// <summary>
        /// Verifica se usuário logado pode mover (enviar ou retornar) uma reformulação orçamentária.
        /// </summary>
        public bool PodeMoverReformulacaoOrcamentaria(int idReformulacaoOrcamentaria)
        {
            var reformulacao = BuscaReformulacaoOrcamentaria(idReformulacaoOrcamentaria);

            if (user != null)
            {
                int idUsuarioLogado = (user as CreaspUser).IdPessoa;
                List<int?> subDelegacoes = new PessoaService(context).ListaDelegacoes(idUsuarioLogado).ToList();
                return subDelegacoes.Contains(reformulacao.IdPessoaResp);
            }

            return false;
        }

        /// <summary>
        /// Verifica se usuário logado pode alterar os valores ou textos de uma reformulação orçamentária.
        /// </summary>
        public bool PodeAlterarReformulacaoOrcamentaria(int idReformulacaoOrcamentaria)
        {
            var reformulacao = BuscaReformulacaoOrcamentaria(idReformulacaoOrcamentaria);

            if (reformulacao.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.ContabValidacao
                && reformulacao.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.Plenario
                && reformulacao.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.PlenarioOK)
            {
                if (user != null)
                {
                    int idUsuarioLogado = (user as CreaspUser).IdPessoa;
                    List<int?> subDelegacoes = new PessoaService(context).ListaDelegacoes(idUsuarioLogado).ToList();
                    return subDelegacoes.Contains(reformulacao.IdPessoaResp);
                }
            }

            return false;
        }

        /// <summary>
        /// Envia todas as reformulações orçamentárias de uma reformulação específica para o próximo status. O segundo parâmetro (cdStatusAtual) NÃO refere-se ao próximo status. 
        /// </summary>
        /// <param name="idReformulacao">Reformulação que os orçamentos estão.</param>
        /// <param name="cdStatusAtual">Status que todas as propostas devem estar antes de irem para o próximo status.</param>
        public List<string> DisponibilizaTodasReformulacoesDespesaParaProximoStatus(int idReformulacao, string cdStatusAtual)
        {
            using (var trans = db.GetTransaction())
            {
                var lstErros = new List<string>();

                var pessoaService = new PessoaService(context);
                var centroCustoPerfilService = new CentroCustoPerfilService(context);

                if (!centroCustoPerfilService.ValidaUsuarioLogadoPossuiPerfil(TpPerfilCentroCusto.Contabilidade))
                {
                    throw new NBoxException("Apenas a contabilidade pode fazer isto");
                }

                if (!VerificaSeTodasReformulacoesTemMesmoStatus(idReformulacao, cdStatusAtual))
                {
                    throw new NBoxException("Todas reformulãções precisam ter o mesmo status");
                }

                var reformulacoesOrcamentarias = BuscaReformulacoesOrcamentarias(idReformulacao);
                //reformulacoesOrcamentarias.ForEach(x => x.ReformulacaoOrcamentariaItens = BuscaReformulacaoOrcamentariaItens(x.IdReformulacaoOrcamentaria));

                //var reformulacoesParaEnviarImplanta = FiltraApenasReformulacoesAlteradas(reformulacoesOrcamentarias);

                //DeParaTodasContas5para6(reformulacoesParaEnviarImplanta);

                //var siscontService = new SiscontService();
                //lstErros = siscontService.EnviaReformulacaoDespesa(reformulacoesParaEnviarImplanta);

                if (lstErros.Count == 0)
                {
                    foreach (var r in reformulacoesOrcamentarias)
                    {
                        DisponibilizaReformulacaoAoProximoResponsavel(r.IdReformulacaoOrcamentaria);
                    }
                }

                trans.Complete();

                return lstErros;
            }
        }

        /// <summary>
        /// Envia todas as propostas orçamentárias de um ano específico para o status anterior.
        /// </summary>
        /// <param name="nrAno">Ano que as propostas estão.</param>
        public void DisponibilizaTodasReformulacoesDespesaParaStatusAnterior(int idReformulacao)
        {
            using (var trans = db.GetTransaction())
            {
                var pessoaService = new PessoaService(context);
                var centroCustoPerfilService = new CentroCustoPerfilService(context);

                if (!centroCustoPerfilService.ValidaUsuarioLogadoPossuiPerfil(TpPerfilCentroCusto.Contabilidade))
                {
                    throw new NBoxException("Apenas a contabilidade pode fazer isto.");
                }

                var refs = BuscaReformulacoesOrcamentarias(idReformulacao);

                if (refs == null || refs.Count == 0)
                {
                    throw new NBoxException("Nenhuma reformulação orçamentária encontrada.");
                }

                //Pega o status da primeira proposta...
                string cdStatusAtual = refs.FirstOrDefault().CdStatusPropostaOrcamentaria;

                //... que deverá ser igual ao status de todas outras propostas.
                if (!VerificaSeTodasReformulacoesTemMesmoStatus(idReformulacao, cdStatusAtual))
                {
                    throw new NBoxException("Todas propostas precisam ter o mesmo status.");
                }

                //Por fim, pra cada proposta, passa ela para o responsável anterior.
                foreach (var r in refs)
                {
                    DisponibilizaReformulacaoAoResponsavelAnterior(r.IdReformulacaoOrcamentaria, string.Empty);
                }

                trans.Complete();
            }
        }

        public bool VerificaSeTodasReformulacoesTemMesmoStatus(int idReformulacao, string cdPropostaOrcamentaria)
        {
            var reformulacoes = BuscaReformulacoesOrcamentarias(idReformulacao);

            if (reformulacoes.Count == 0) throw new NBoxException("Nenhuma reformulação encontrada.");

            return reformulacoes.All(x => x.CdStatusPropostaOrcamentaria == cdPropostaOrcamentaria);
        }

        public int BuscaIdReformulacaoMaisRecente(int? idReformulacaoNova = null)
        {
            var reformulacoes = db.Query<Reformulacao>()
                .Where(x => x.NrAno == nrAnoBase)
                .Where(idReformulacaoNova.HasValue, x => x.IdReformulacao != idReformulacaoNova)
                .ToList();

            if (reformulacoes?.Count > 0)
            {
                return reformulacoes.Max(x => x.IdReformulacao);
            }

            return 0;
        }


        private List<ReformulacaoOrcamentaria> FiltraApenasReformulacoesAlteradas(List<ReformulacaoOrcamentaria> reformulacoesOrcamentarias)
        {
            var reformulacoesFiltradas = new List<ReformulacaoOrcamentaria>(reformulacoesOrcamentarias);

            for (int i = 0; i < reformulacoesFiltradas.Count; i++)
            {
                reformulacoesFiltradas[i].ReformulacaoOrcamentariaItens =
                     reformulacoesFiltradas[i].ReformulacaoOrcamentariaItens
                    .Where(x => x.VrSuplementacao != x.VrReducao)
                    .ToList();
            }

            reformulacoesFiltradas = reformulacoesFiltradas.Where(x => x.ReformulacaoOrcamentariaItens.Count > 0).ToList();

            return reformulacoesFiltradas;
        }

        //private void DeParaTodasContas5para6(List<ReformulacaoOrcamentaria> reformulacoesOrcamentarias)
        public List<string> DeParaTodasContas5para6(List<ReformulacaoOrcamentaria> reformulacoesOrcamentarias)
        {
            var contaMapeamentoService = new ContaContabilMapeamentoService(context);
            var contasContabeis = contaMapeamentoService.BuscaContasContabeisMapeadas();

            var lstContas5sem6 = new List<string>();

            string nrContaContabil6 = string.Empty;

            for (int i = 0; i < reformulacoesOrcamentarias.Count; i++)
            {
                if (reformulacoesOrcamentarias[i].ReformulacaoOrcamentariaItens != null)
                {
                    for (int j = 0; j < reformulacoesOrcamentarias[i].ReformulacaoOrcamentariaItens.Count; j++)
                    {
                        nrContaContabil6 = contaMapeamentoService.DePara5para6(contasContabeis, reformulacoesOrcamentarias[i].ReformulacaoOrcamentariaItens[j].ContaContabil.NrContaContabil);
                        if (!string.IsNullOrEmpty(nrContaContabil6))
                        {
                            reformulacoesOrcamentarias[i].ReformulacaoOrcamentariaItens[j].ContaContabil.NrContaContabil = contaMapeamentoService.DePara5para6(contasContabeis, reformulacoesOrcamentarias[i].ReformulacaoOrcamentariaItens[j].ContaContabil.NrContaContabil);
                        }
                        else
                        {
                            lstContas5sem6.Add(reformulacoesOrcamentarias[i].ReformulacaoOrcamentariaItens[j].ContaContabil.NrContaContabilFmt);
                        }
                    }
                }
                //reformulacoesOrcamentarias[i].ReformulacaoOrcamentariaItens.ForEach(x => x.ContaContabil.NrContaContabil = contaContabilService.DePara5para6(contasContabeis, x.ContaContabil.NrContaContabil));
            }

            return lstContas5sem6;
        }

        public void ValidaEIncluiReformulacao(int nrAno, string noReformulacao, int[] idsSelecionados)
        {
            using (var trans = db.GetTransaction())
            {
                if (!ValidaReformulacao(nrAno, noReformulacao))
                {
                    throw new NBoxException("Reformulação inválida");
                }

                if (ExisteReformulacao(nrAno, noReformulacao))
                {
                    throw new NBoxException("Reformulação já existente");
                }

                if (ExistePropostaOuReformulacaoAberta())
                {
                    throw new NBoxException("Existem propostas ou reformulações não finalizadas");
                }

                int idReformulacao = IncluiReformulacao(nrAno, noReformulacao);
                PreencheReformulacoesOrcamentarias(idReformulacao, idsSelecionados);
                PreencheReformulacoesMetodologia(idReformulacao);

                trans.Complete();
            }
        }

        public void DisponibilizaReformulacaoAoProximoResponsavel(int idReformulacaoOrcamentaria)
        {

            if (!PodeMoverReformulacaoOrcamentaria(idReformulacaoOrcamentaria))
            {
                throw new NBoxException("Você não tem permissão para fazer isto.");
            }

            var reformulacaoOrcamentaria = BuscaReformulacaoOrcamentaria(idReformulacaoOrcamentaria);
            int? idPessoaRespAnterior = reformulacaoOrcamentaria.IdPessoaResp;

            using (var trans = db.GetTransaction())
            {
                reformulacaoOrcamentaria.ReformulacaoOrcamentariaItens = BuscaReformulacaoOrcamentariaItens(idReformulacaoOrcamentaria);

                if (!ValidaJustificativas(reformulacaoOrcamentaria.ReformulacaoOrcamentariaItens))
                {
                    throw new NBoxException("Todas as justificativas com valores de suplementção ou redução precisam estar preenchidas.");
                }

                AtualizaResponsavelProximoStatus(reformulacaoOrcamentaria);
                SalvaReformulacaoOrcamentaria(reformulacaoOrcamentaria);

                //SalvaHistorico(reformulacao);
                //reformulacaoOrcamentaria.ReformulacaoOrcamentariaItens.ForEach(x => x.VrReducao = x.VrReducaoAntesDeFinalizar);
                //reformulacaoOrcamentaria.ReformulacaoOrcamentariaItens.ForEach(x => x.VrSuplementacao = x.VrSuplementacaoAntesDeFinalizar);

                trans.Complete();
            }

            NotificaNovoResponsavel(reformulacaoOrcamentaria, false, idPessoaRespAnterior);
        }

        public void DisponibilizaReformulacaoAoResponsavelAnterior(int idReformulacaoOrcamentaria, string NoMotivoRejeicao)
        {

            if (!PodeMoverReformulacaoOrcamentaria(idReformulacaoOrcamentaria))
            {
                throw new NBoxException("Você não tem permissão para fazer isto.");
            }

            var reformulacaoOrcamentaria = BuscaReformulacaoOrcamentaria(idReformulacaoOrcamentaria);
            int? idPessoaRespAnterior = reformulacaoOrcamentaria.IdPessoaResp;

            using (var trans = db.GetTransaction())
            {
                if (!ValidaJustificativas(reformulacaoOrcamentaria.ReformulacaoOrcamentariaItens))
                {
                    throw new NBoxException("Todas as justificativas com valores de suplementção ou redução precisam estar preenchidas.");
                }

                reformulacaoOrcamentaria.NoMotivoRejeicao = NoMotivoRejeicao;
                AtualizaResponsavelStatusAnterior(reformulacaoOrcamentaria);
                SalvaReformulacaoOrcamentaria(reformulacaoOrcamentaria);

                //SalvaHistorico(reformulacao);

                //reformulacao.ReformulacaoOrcamentariaItens.ForEach(x => x.VrOrcado = x.VrOrcadoAntesFinalizar);

                trans.Complete();
            }

            NotificaNovoResponsavel(reformulacaoOrcamentaria, true, idPessoaRespAnterior);
        }

        private bool ValidaJustificativas(List<ReformulacaoOrcamentariaItem> itensReformulacao)
        {
            if (itensReformulacao != null &&
                itensReformulacao.Count > 0 &&
                itensReformulacao
                .Where(x => x.VrReducao > 0 || x.VrSuplementacao > 0)
                .Any(x => string.IsNullOrEmpty(x.NoJustificativa)))
            {
                return false;
            }

            return true;
        }

        private void NotificaNovoResponsavel(ReformulacaoOrcamentaria reformulacaoOrcamentaria, bool reprovacao, int? idPessoaRespAnterior)
        {
            if (reformulacaoOrcamentaria.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.PlenarioOK)
            {
                var pessoaService = new PessoaService(context);
                var pDestinataria = pessoaService.BuscaPessoa(reformulacaoOrcamentaria.IdPessoaResp.Value, false);

                if (pDestinataria == null)
                {
                    throw new NBoxException("Destinatário não encontrado.");
                }

                //Se não houver e-mail cadastrado ou a pessoa não aceitar receber e-mail (conforme cadastro), não envia o e-mail.
                if (string.IsNullOrEmpty(pDestinataria.NoEmail) || !pDestinataria.FgAceitaEmail)
                {
                    return;
                    //throw new NBoxException("Destinatário sem e-mail cadastrado. A proposta foi alterada, mas ele não foi informado.");
                }

                var pRemetente = pessoaService.BuscaPessoa(idPessoaRespAnterior.Value, false);

                string assunto = string.Format("CreaSP (ELO): A reformulação {0} está com você!", reformulacaoOrcamentaria.CentroCusto.NrCentroCustoFmt);
                string msg = string.Format("{0} {1} a proposta do centro de custo {2} {3} para você.",
                    pRemetente != null ? pRemetente.NoPessoa : "Seu colega",
                    reprovacao ? "retornou" : "enviou",
                    reformulacaoOrcamentaria.CentroCusto.NoCentroCustoFmt,
                    reformulacaoOrcamentaria.CdTpFormulario != null ? "(" + reformulacaoOrcamentaria.CdTpFormulario + ")" : "");

                if (reprovacao && !string.IsNullOrEmpty(reformulacaoOrcamentaria.NoMotivoRejeicao))
                {
                    msg = string.Format("{0} \r\nMotivo: {1}", msg, reformulacaoOrcamentaria.NoMotivoRejeicao);
                }

                NBox.Services.Email.New()
                        .To(pDestinataria.NoEmail, pDestinataria.NoPessoa)
                        .ReplyTo(pRemetente?.NoEmail ?? "")
                        .Subject(assunto)
                        .Body(msg)
                        .Send();

                new EmailService(context).RegistraEnvioDeEmail(pDestinataria.IdPessoa, pDestinataria.NoEmail, assunto, msg);
            }
        }

        private bool ValidaReformulacao(int nrAno, string noReformulacao)
        {
            return nrAno > 0 && !string.IsNullOrWhiteSpace(noReformulacao);
        }

        private bool ExisteReformulacao(int nrAno, string noReformulacao)
        {
            return db.Query<Reformulacao>()
                       .Where(x => x.NrAno == nrAno)
                       .Where(x => x.NoReformulacao == noReformulacao.Trim())
                       .Any();
        }

        private int IncluiReformulacao(int nrAno, string noReformulacao)
        {
            var reformulacao = new Reformulacao()
            {
                NrAno = nrAno,
                NoReformulacao = noReformulacao.Trim()
            };

            db.Insert<Reformulacao>(reformulacao);

            return reformulacao.IdReformulacao;
        }

        private bool ExistePropostaOuReformulacaoAberta()
        {
            bool existePropostaAberta = db.Query<PropostaOrcamentariaDespesa>()
                                        .Where(x => x.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.PlenarioOK)
                                        .Any();

            if (existePropostaAberta) return true;

            bool existeReformulacaoDespesaAberta = db.Query<ReformulacaoOrcamentaria>()
                                        .Where(x => x.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.PlenarioOK)
                                        .Any();

            if (existeReformulacaoDespesaAberta) return true;

            bool existeReformulacaoReceitaAberta = db.Query<MetodologiaReceitaAno>()
                                        .Where(x => x.FgReformulacao == true)
                                        .Where(x => x.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.PlenarioOK)
                                        .Any();

            return existeReformulacaoReceitaAberta;
        }

        public void ValidaEIncluiContaContabil(int idReformulacaoOrcamentaria, int idContaContabil, string nrProcessoL)
        {
            if (idReformulacaoOrcamentaria == 0) throw new ArgumentNullException("idReformulacaoOrcamentaria");
            if (idContaContabil == 0) throw new ArgumentNullException("idContaContabil");

            if (!ValidaInclusaoContaContabil(idReformulacaoOrcamentaria, idContaContabil, nrProcessoL))
            {
                throw new NBoxException("Conta contábil já existe nesta reformulão de proposta.");
            }

            IncluiContaContabil(idReformulacaoOrcamentaria, idContaContabil, nrProcessoL);

        }

        private bool ValidaInclusaoContaContabil(int idReformulacaoOrcamentaria, int idContaContabil, string nrProcessoL)
        {
            return !db.Query<ReformulacaoOrcamentariaItem>()
                .Where(x => x.IdReformulacaoOrcamentaria == idReformulacaoOrcamentaria)
                .Where(x => x.IdContaContabil == idContaContabil)
                .Where(x => x.NrProcessoL == nrProcessoL && !string.IsNullOrEmpty(nrProcessoL))
                .Any();
        }

        private void IncluiContaContabil(int idReformulacaoOrcamentaria, int idContaContabil, string nrProcessoL)
        {
            var ri = new ReformulacaoOrcamentariaItem();

            var contaContabilService = new ContaContabilService(context);

            var r = BuscaReformulacaoOrcamentaria(idReformulacaoOrcamentaria);
            var conta = contaContabilService.BuscaContaContabil(idContaContabil);

            ri.IdReformulacaoOrcamentaria = r.IdReformulacaoOrcamentaria;
            ri.IdContaContabil = conta.IdContaContabil;

            //Se tiver número de processo ou for um formulário, não é pra preencher nenhum outro valor da conta contábil.
            if (!string.IsNullOrEmpty(nrProcessoL) || !string.IsNullOrEmpty(r.CdTpFormulario))
            {
                ri.VrOrcadoInicialmente = 0;
                ri.NrProcessoL = nrProcessoL;
                ri.VrDotacaoAtual = 0;
                ri.VrSuplementacao = 0;
                ri.VrReducao = 0;
            }
            else
            {
                var orcamentoService = new OrcamentoService(context);

                var posicaoAtualOrcamento = orcamentoService.ListaPosicaoAtualOrcamento(r.Reformulacao.NrAno, r.Reformulacao.NrAno) //TODO: O filtro correto é sem o "- 1" dos dois parâmetros.
                .Where(x => x.NrCentroCusto == r.CentroCusto.NrCentroCusto)
                .Where(x => x.NrContaContabil5 == conta.NrContaContabil)
                .SingleOrDefault();

                if (posicaoAtualOrcamento != null)
                {
                    ri.VrExecutado = posicaoAtualOrcamento.VrExecutado;
                    ri.DtRefVrExecutado = posicaoAtualOrcamento.DtRefVrExecutado;
                    ri.VrDotacaoAtual = posicaoAtualOrcamento.VrEmpExercicio + posicaoAtualOrcamento.VrSaldoOrcamentarioExercicio;
                }
                else
                {
                    ri.VrDotacaoAtual = 0;
                }

                ri.VrOrcadoInicialmente = 0;
                ri.VrSuplementacao = 0;
                ri.VrReducao = 0;
            }

            db.Insert(ri);
        }

        /// <summary>
        /// Lista os centros de custo de por reformulação ou proposta orçamentaria.
        /// </summary>
        public QueryPage<CentroCustoDTO> ListarCentrosDeCustoParaNovaReformulacao(Pager p, int? idReformulacaoAnterior = null)
        {
            ICollection<CentroCustoDTO> centroCustoDTO = new List<CentroCustoDTO>();
            if (idReformulacaoAnterior.HasValue && idReformulacaoAnterior.Value > 0)
            {
                centroCustoDTO = db.Query<ReformulacaoOrcamentaria>()
                        .Include(x => x.CentroCusto)
                        .IncludeMany(x => x.ReformulacaoOrcamentariaItens)
                        .Where(x => x.IdReformulacao == idReformulacaoAnterior)
                        .ToList()
                        .Select(r => new CentroCustoDTO
                        {
                            NoCentroCustoFmt = r.CentroCusto.NoCentroCustoFmt,
                            IdCentroCusto = r.CentroCusto.IdCentroCusto,
                            NrCentroCustoFmt = r.CentroCusto.NrCentroCustoFmt,
                            TpCentroCusto = r.CentroCusto.TpCentroCusto,
                            Valor = r.VrOrcadoTotal.HasValue ? r.VrOrcadoTotal.Value : 0m
                        }).ToList();

            }
            else
            {
                centroCustoDTO = db.Query<PropostaOrcamentariaDespesa>()
                       .Include(x => x.CentroCusto)
                       .IncludeMany(x => x.PropostaOrcamentariaItens)
                       .Where(x => x.NrAno == nrAnoBase)
                       .ToList()
                       .Select(r => new CentroCustoDTO
                       {
                           NoCentroCustoFmt = r.CentroCusto.NoCentroCustoFmt,
                           IdCentroCusto = r.CentroCusto.IdCentroCusto,
                           NrCentroCustoFmt = r.CentroCusto.NrCentroCustoFmt,
                           TpCentroCusto = r.CentroCusto.TpCentroCusto,
                           Valor = r.VrOrcadoTotal.HasValue ? r.VrOrcadoTotal.Value : 0m
                       }).ToList();
            }
            return centroCustoDTO.ToPage(p);
        }


        private void PreencheReformulacoesOrcamentarias(int idReformulacao, int[] idsSelecionados)
        {
            using (var trans = db.GetTransaction())
            {
                var reformulacao = BuscaReformulacao(idReformulacao);

                var propostasReformulacoesDTO = new List<PropostaReformulacaoDTO>();

                var idReformulacaoAnterior = BuscaIdReformulacaoMaisRecente(idReformulacao);
                var orcamentoService = new OrcamentoService(context);

                var posicaoAtualOrcamento = orcamentoService.ListaPosicaoAtualOrcamento(nrAnoBase - 1, nrAnoBase);

                if (idReformulacaoAnterior > 0)
                {
                    var reformulacoesAnteriores = db.Query<ReformulacaoOrcamentaria>()
                        .Include(x => x.CentroCustoOriginal)
                        //.Include(x => x.CentroCustoOriginal.CentroCustoResponsavel)
                        //.IncludeMany(x => x.ReformulacaoOrcamentariaItens)
                        .Where(x => x.IdReformulacao == idReformulacaoAnterior)
                        .ToList();

                    //TODO: TESTAR PERFORMANCE
                    var ccrService = new CentroCustoResponsavelService(context);
                    //var ccrService = new CentroCustoPerfilService(context);
                    foreach (var reformulacaoOrcamentaria in reformulacoesAnteriores)
                    {
                        reformulacaoOrcamentaria.CentroCustoOriginal.CentroCustoResponsavel = ccrService.BuscaCentroCustoResponsavel(reformulacaoOrcamentaria.IdCentroCusto, null);
                        reformulacaoOrcamentaria.ReformulacaoOrcamentariaItens = BuscaReformulacaoOrcamentariaItens(reformulacaoOrcamentaria.IdReformulacaoOrcamentaria);
                        propostasReformulacoesDTO.Add((PropostaReformulacaoDTO)reformulacaoOrcamentaria);
                    }
                }
                else
                {
                    var propostas = db.Query<PropostaOrcamentariaDespesa>()
                        .Include(x => x.CentroCustoOriginal)
                        //.Include(x => x.CentroCustoOriginal.CentroCustoResponsavel)
                        //.IncludeMany(x => x.PropostaOrcamentariaItens)
                        .Where(x => x.NrAno == reformulacao.NrAno)
                        .ToList();

                    //TODO: TESTAR PERFORMANCE
                    var ccrService = new CentroCustoResponsavelService(context);
                    var propostaService = new PropostaOrcamentariaDespesaService(context);
                    foreach (var proposta in propostas)
                    {
                        proposta.CentroCustoOriginal.CentroCustoResponsavel = ccrService.BuscaCentroCustoResponsavel(proposta.IdCentroCusto, null);
                        proposta.PropostaOrcamentariaItens = propostaService.ListaPropostaOrcamentariaItens(proposta.IdPropostaOrcamentariaDespesa);
                        propostasReformulacoesDTO.Add((PropostaReformulacaoDTO)proposta);
                    }
                }

                //var relacaoCentroContaJaInserida = new List<ContaContabil_CentroCusto>();

                decimal? vrDotacaoAtual;
                decimal? vrExecutado;
                DateTime? dtRefExecutado;

                var centroPerfilService = new CentroCustoPerfilService(context);
                var idExecutorContab = centroPerfilService.BuscaSeHouverCentroCustoResponsavel(TpPerfilCentroCusto.Contabilidade).IdPessoaExecutor;

                foreach (var dto in propostasReformulacoesDTO)
                {
                    string status = "";
                    if (!idsSelecionados.Contains(dto.IdCentroCustoOriginal))
                    {
                        status = StPropostaOrcamentaria.Executor;
                    }
                    else
                    {
                        dto.IdPessoaResp = idExecutorContab;
                        status = StPropostaOrcamentaria.Contab;
                    }

                    var refOrcamentaria = new ReformulacaoOrcamentaria()
                    {
                        IdCentroCusto = dto.IdCentroCustoOriginal,
                        IdCentroCustoOriginal = dto.IdCentroCustoOriginal,
                        CdTpFormulario = dto.CdTpFormulario,
                        CdStatusPropostaOrcamentaria = status,
                        IdPessoaResp = dto.IdPessoaResp,
                        IdReformulacao = reformulacao.IdReformulacao,
                        IdPropostaOrcamentariaDespesa = dto.IdPropostaOrcamentariaDespesa,
                        LoginCadastro = user.Login
                    };

                    db.Insert<ReformulacaoOrcamentaria>(refOrcamentaria);

                    if (dto.PropostaReformulacaoItensDTO?.Count > 0)
                    {
                        foreach (var dtoItem in dto.PropostaReformulacaoItensDTO)
                        {
                            vrDotacaoAtual = 0;
                            vrExecutado = 0;
                            dtRefExecutado = null;

                            var posicao = posicaoAtualOrcamento
                                .Where(x => x.NrAno == nrAnoBase)
                                .Where(x => x.NrCentroCusto == dto.NrCentroCustoOriginal)
                                .Where(x => x.NrContaContabil5 == dtoItem.NrContaContabil)
                                .SingleOrDefault();

                            if (posicao != null)
                            {
                                vrDotacaoAtual = posicao.VrEmpExercicio + posicao.VrSaldoOrcamentarioExercicio;
                                vrExecutado = posicao.VrExecutado;
                                dtRefExecutado = posicao.DtRefVrExecutado;
                            }

                            var refOrcamentariaItem = new ReformulacaoOrcamentariaItem()
                            {
                                IdContaContabil = dtoItem.IdContaContabil,
                                IdPropostaOrcamentariaDespesaItem = dtoItem.IdPropostaOrcamentariaDespesaItem,
                                IdReformulacaoOrcamentaria = refOrcamentaria.IdReformulacaoOrcamentaria,
                                NoJustificativa = dtoItem.NoJustificativa,
                                VrOrcadoInicialmente = dtoItem.VrOrcadoInicialmente,
                                VrExecutado = vrExecutado,
                                DtRefVrExecutado = dtRefExecutado,
                                VrDotacaoAtual = vrDotacaoAtual,
                                VrReducao = dtoItem.VrReducao,
                                VrSuplementacao = dtoItem.VrSuplementacao,
                                LoginCadastro = user.Login
                            };

                            db.Insert<ReformulacaoOrcamentariaItem>(refOrcamentariaItem);

                            //relacaoCentroContaJaInserida.Add(
                            //    new ContaContabil_CentroCusto()
                            //    {
                            //        IdCentroCusto = dto.IdCentroCustoOriginal,
                            //        IdContaContabil = dtoItem.IdContaContabil
                            //    });
                        }
                    }
                }

                trans.Complete();
            }

            int idUsuarioLogado = (user as CreaspUser).IdPessoa;
            var lstNovasReformulacoes = BuscaReformulacoesOrcamentarias(idReformulacao);
            lstNovasReformulacoes.ForEach(x => NotificaNovoResponsavel(x, false, idUsuarioLogado));
        }

        private void PreencheReformulacoesMetodologia(int idReformulacao)
        {
            List<MetodologiaReceitaAno> reformulacoesMetodologia = new List<MetodologiaReceitaAno>();

            using (var trans = db.GetTransaction())
            {
                var reformulacao = BuscaReformulacao(idReformulacao);

                //var contaContabilService = new ContaContabilService(context);
                var metodologiaReceitaService = new MetodologiaReceitaService(context);
                var centroPerfilService = new CentroCustoPerfilService(context);

                var idReformulacaoAnterior = BuscaIdReformulacaoMaisRecente(idReformulacao);

                int? parametroIdReformulacaoAnterior = null;

                if (idReformulacaoAnterior > 0)
                {
                    parametroIdReformulacaoAnterior = idReformulacaoAnterior;
                }

                var metodologiasReceita = metodologiaReceitaService.BuscaMetodologias(reformulacao.NrAno, parametroIdReformulacaoAnterior);

                var centroCustoResponsavel = centroPerfilService.BuscaSeHouverCentroCustoResponsavel(TpPerfilCentroCusto.Contabilidade);

                foreach (var metodologia in metodologiasReceita)
                {
                    var reformulacaoMetodologia = new MetodologiaReceitaAno();

                    reformulacaoMetodologia.IdContaContabil = metodologia.IdContaContabil;
                    reformulacaoMetodologia.NrAno = reformulacao.NrAno;
                    reformulacaoMetodologia.PrAplicado = 0; //metodologia.PrAplicado;
                    reformulacaoMetodologia.PrCotaParteConfea = metodologia.PrCotaParteConfea;
                    reformulacaoMetodologia.PrCotaParteMutua = metodologia.PrCotaParteMutua;
                    reformulacaoMetodologia.PrBrasilia = metodologia.PrBrasilia;
                    reformulacaoMetodologia.PrSaoPaulo = metodologia.PrSaoPaulo;
                    reformulacaoMetodologia.FgReformulacao = true;
                    reformulacaoMetodologia.IdReformulacao = reformulacao.IdReformulacao;
                    reformulacaoMetodologia.VrAprovadoAnoAnterior = metodologia.VrOrcado;
                    reformulacaoMetodologia.VrOrcado = metodologia.VrOrcado;
                    reformulacaoMetodologia.NoObservacoes = metodologia.NoObservacoes;
                    reformulacaoMetodologia.IdPessoaResp = centroCustoResponsavel.IdPessoaExecutor;
                    reformulacaoMetodologia.CdStatusPropostaOrcamentaria = StPropostaOrcamentaria.Executor;

                    metodologiaReceitaService.InsereAno(reformulacaoMetodologia);

                    List<MetodologiaReceitaMes> metodologiasMeses = new List<MetodologiaReceitaMes>();

                    foreach (var mes in metodologia.MetodologiasReceitaMes)
                    {
                        var metodologiaMes = new MetodologiaReceitaMes();

                        metodologiaMes.IdMetodologiaReceitaAno = reformulacaoMetodologia.IdMetodologiaReceitaAno;
                        metodologiaMes.NoMes = mes.NoMes;
                        metodologiaMes.PrAplicado = mes.PrAplicado;
                        metodologiaMes.VrReceitaBruta = mes.VrReceitaBruta;
                        metodologiaMes.VrReceitaLiquida = mes.VrReceitaLiquida;

                        db.Insert(metodologiaMes);
                    }

                    reformulacoesMetodologia.Add(reformulacaoMetodologia);
                };

                trans.Complete();
            }

            new MetodologiaReceitaService(context).NotificaNovoResponsavel(reformulacoesMetodologia, false, (user as CreaspUser).IdPessoa);
        }

        private void AtualizaResponsavelProximoStatus(ReformulacaoOrcamentaria reformulacaoOrcamentaria)
        {
            var statusService = new StatusPropostaOrcamentariaService(context);
            statusService.ProximoStatus(reformulacaoOrcamentaria);
        }

        private void AtualizaResponsavelStatusAnterior(ReformulacaoOrcamentaria reformulacaoOrcamentaria)
        {
            var statusService = new StatusPropostaOrcamentariaService(context);
            statusService.StatusAnterior(reformulacaoOrcamentaria);
        }

        public void SalvaReformulacaoOrcamentaria(ReformulacaoOrcamentaria reformulacaoOrcamentaria)
        {
            db.Update(reformulacaoOrcamentaria);
        }

        /// <summary>
        /// Salva os itens da proposta orçamentária 
        /// </summary>
        public void SalvaRefomulcaoItens(List<ReformulacaoOrcamentariaItem> itensReformulacao)
        {
            using (var trans = db.GetTransaction())
            {
                //Poderia ter uma flag informando que o objeto foi alterado, 
                //pois normalmente o usuário não vai alterar todos registros, 
                //então não há necessidade de fazer update em todos.

                if (!ValidaJustificativas(itensReformulacao))
                {
                    throw new NBoxException("Todos os itens com valores de suplementção ou redução precisam ter uma justificativa preenchida");
                }

                foreach (var item in itensReformulacao)
                {
                    db.AuditUpdate(item);
                }

                trans.Complete();
            }
        }

        public void ValidaEIncluiFormulario(int idReformulacao, string cdTpFormulario, string nrCentroCusto)
        {
            if (string.IsNullOrWhiteSpace(cdTpFormulario)) throw new ArgumentNullException("cdTpFormulario");
            if (string.IsNullOrWhiteSpace(nrCentroCusto)) throw new ArgumentNullException("nrCentroCusto");

            if (VerificaSeJaExisteFormulario(idReformulacao, cdTpFormulario, nrCentroCusto))
            {
                throw new NBoxException("Formulário já existente");
            }

            var r = new ReformulacaoOrcamentaria();

            var centroCustoService = new CentroCustoService(context);
            var pessoaService = new PessoaService(context);

            r.IdCentroCusto = centroCustoService.BuscaCentroCusto(nrCentroCusto).IdCentroCusto;
            r.IdCentroCustoOriginal = r.IdCentroCusto;
            r.IdPessoaResp = (user as CreaspUser).IdPessoa;
            r.IdReformulacao = idReformulacao;
            r.CdStatusPropostaOrcamentaria = StPropostaOrcamentaria.Executor;
            r.CdTpFormulario = cdTpFormulario;

            db.Insert(r);
        }

        private bool VerificaSeJaExisteFormulario(int idReformulacao, string cdTpFormulario, string nrCentroCusto)
        {
            return db.Query<ReformulacaoOrcamentaria>()
                .Include(x => x.CentroCusto)
                .Where(x => x.IdReformulacao == idReformulacao)
                .Where(x => x.CentroCusto.NrCentroCusto == nrCentroCusto)
                .Where(x => x.CdTpFormulario == cdTpFormulario)
                .Any();
        }

        public bool VerificaSeReformulacaoOrcamentariaPossuiConta(int idReformulacaoOrcamenaria, int idContaContabil)
        {
            return db.Query<ReformulacaoOrcamentariaItem>()
                .Where(x => x.IdReformulacaoOrcamentaria == idReformulacaoOrcamenaria)
                .Where(x => x.IdContaContabil == idContaContabil)
                .Any();
        }
    }
}
