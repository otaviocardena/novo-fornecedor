﻿using Creasp.Core;
using Creasp.Elo;
using NBox.Core;
using NBox.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Elo
{
    public class CentroCustoPerfilService : DataService
    {
        protected readonly int nrAnoBase;

        public CentroCustoPerfilService(ServiceContext context) : base(context)
        {
            nrAnoBase = (context as CreaspContext).NrAnoBase;
        }

        /// <summary>
        /// Lista todos os perfis dos centros de custos.
        /// </summary>
        /// <returns></returns>
        public List<CentroCustoPerfil> ListaCentroCustosPerfis()
        {
            var l = db.Query<CentroCustoPerfil>().ToList();
            return l;
        }

        /// <summary>
        /// Lista todos os relacionamentos entre centro de custo responsável e perfil a partir de um ID de centro de custo responsável
        /// </summary>
        public List<CentroCustoResponsavelPerfil> ListaCentroCustoResponsavelPerfil(int idCentroCustoResponsavel)
        {
            return db.Query<CentroCustoResponsavelPerfil>()
                .Include(x => x.CentroCustoResponsavel)
                .Include(x => x.CentroCustoResponsavel.CentroCusto)
                .Include(x => x.CentroCustoResponsavel.ResponsavelExecucao)
                .Where(x => x.IdCentroCustoResponsavel == idCentroCustoResponsavel)
                .ToList();
        }

        /// <summary>
        /// Lista todos perfis de centro de custo que já estão vinculados com responsáveis que não sejam os do parâmetro.
        /// </summary>
        public List<CentroCustoPerfil> ListaCentroCustoPerfilJaUtilizadosPorOutrosCentroCusto(int idCentroCustoResponsavel)
        {
            var l = db.Query<CentroCustoResponsavelPerfil>()
                .Include(x => x.CentroCustoPerfil)
                .Include(x => x.CentroCustoResponsavel)
                .Include(x => x.CentroCustoResponsavel.CentroCusto)
                .Where(x => x.IdCentroCustoResponsavel != idCentroCustoResponsavel)
                .Where(x => x.CentroCustoResponsavel.CentroCusto.NrAno == nrAnoBase)
                .ToList()
                .Select(x => x.CentroCustoPerfil)
                .ToList();

            return l;
        }

        public bool ValidaUsuarioLogadoPossuiPerfil(string cdCentroCustoPerfil)
        {
            var ccr = BuscaCentroCustoResponsavel(cdCentroCustoPerfil);

            int idUsuarioLogado = (user as CreaspUser).IdPessoa;

            List<int?> subDelegacoes = new PessoaService(context).ListaDelegacoes(idUsuarioLogado).ToList();

            if (ccr != null)
            {   
                return subDelegacoes.Contains(ccr.IdPessoaExecutor);
            }

            return false;
        }

        /// <summary>
        /// Busca um centro de custo responsável a partir de um código de perfil de centro de custo utilizando o ano base também como filtro.
        /// </summary>
        public CentroCustoResponsavel BuscaSeHouverCentroCustoResponsavel(string cdCentroCustoPerfil)
        {
            var c = BuscaCentroCustoResponsavel(cdCentroCustoPerfil);

            if (c == null) throw new NBoxException($"Nenhum centro de custo cadastrado com o perfil { cdCentroCustoPerfil }.");

            return c;
        }

        /// <summary>
        /// Busca um centro de custo responsável a partir de um código de perfil de centro de custo utilizando o ano base também como filtro.
        /// </summary>
        private CentroCustoResponsavel BuscaCentroCustoResponsavel(string cdCentroCustoPerfil)
        {
            var c = db.Query<CentroCustoResponsavelPerfil>()
                .Include(x => x.CentroCustoResponsavel)
                .Include(x => x.CentroCustoResponsavel.CentroCusto)
                .Where(x => x.CentroCustoResponsavel.CentroCusto.NrAno == nrAnoBase)
                .Where(x => x.CdCentroCustoPerfil == cdCentroCustoPerfil)
                .SingleOrDefault();

            if (c != null) return c.CentroCustoResponsavel;

            return null;
        }

        /// <summary>
        /// Valida e salva os relacionamentos entre centro de custo responsável e perfil. Exclui todos perfis existentes depois inclui todos enviados.
        /// </summary>
        public void SalvaCentroCustoResponsavelPerfil(int idCentroCustoResponsavel, List<string> cdsCentroCustoPerfis)
        {
            ValidaCetroCustoResponsavelPerfil(idCentroCustoResponsavel, cdsCentroCustoPerfis);
            using (var trans = db.GetTransaction())
            {
                ExcluiCentroCustoResponsavelPerfil(idCentroCustoResponsavel);
                IncluiCentroCustoResponsavelPerfil(idCentroCustoResponsavel, cdsCentroCustoPerfis);

                trans.Complete();
            }

        }

        /// <summary>
        /// Valida se existe algum outro centro de custo responsável (diferente do enviado por parâmetro) com estes códigos de perfis de centro de custo.
        /// </summary>
        private void ValidaCetroCustoResponsavelPerfil(int idCentroCustoResponsavel, List<string> cdsCentroCustoPerfis)
        {
            //Para cada perfil que precisa ser validado
            foreach (var cdCentroCustoPerfil in cdsCentroCustoPerfis)
            {
                //Verifica se o perfil já existe para outro centro de custo responsável
                var outroCCResponsavelComEstePerfil = db.Query<CentroCustoResponsavelPerfil>()
                    .Include(x => x.CentroCustoResponsavel)
                    .Include(x => x.CentroCustoResponsavel.CentroCusto)
                    .Where(x => x.CentroCustoResponsavel.CentroCusto.NrAno == nrAnoBase)
                    .Where(x => x.IdCentroCustoResponsavel != idCentroCustoResponsavel)
                    .Where(x => x.CdCentroCustoPerfil == cdCentroCustoPerfil)
                    .SingleOrDefault();

                //Se existir um outro centro de custo com aquele perfil...
                if (outroCCResponsavelComEstePerfil != null)
                {
                    //Lança uma mensagem de erro para o usuário.
                    throw new NBoxException($"O centro de custo { outroCCResponsavelComEstePerfil.CentroCustoResponsavel.CentroCusto.NrCentroCustoFmt } já possui o perfil { cdCentroCustoPerfil }");
                }
            }
        }

        /// <summary>
        /// Exclui todos os relacionamentos de um centro de custo responsável com os perfis de centro de custo.
        /// </summary>
        private void ExcluiCentroCustoResponsavelPerfil(int idCentroCustoResponsavel)
        {
            var lstCCRP = ListaCentroCustoResponsavelPerfil(idCentroCustoResponsavel);
            foreach (var item in lstCCRP)
            {
                db.Delete(item);
            }
        }

        /// <summary>
        /// Insere todos os centros de custo perfis para um centro de custo responsável.
        /// </summary>
        private void IncluiCentroCustoResponsavelPerfil(int idCentroCustoResponsavel, List<string> cdsCentroCustoPerfis)
        {
            foreach (var item in cdsCentroCustoPerfis)
            {
                db.Insert(new CentroCustoResponsavelPerfil() { IdCentroCustoResponsavel = idCentroCustoResponsavel, CdCentroCustoPerfil = item });
            }
        }

        public bool TodosPerfisEstaoVinculados(int nrAno)
        {
            var perfisExistentes = ListaCentroCustosPerfis();

            var perfisUtilizados = db.Query<CentroCustoResponsavelPerfil>()
                .Include(x => x.CentroCustoResponsavel)
                .Include(x => x.CentroCustoResponsavel.CentroCusto)
                .Where(x => x.CentroCustoResponsavel.CentroCusto.NrAno == nrAno)
                .ToList();

            return perfisExistentes.Count == perfisUtilizados.Count;
        }
    }
}
