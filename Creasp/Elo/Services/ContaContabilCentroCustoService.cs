﻿using Creasp.Core;
using Creasp.Elo;
using NBox.Core;
using NBox.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Elo
{
    public class ContaContabilCentroCustoService : DataService
    {
        protected readonly int nrAnoBase;

        public ContaContabilCentroCustoService(ServiceContext context) : base(context)
        {
            nrAnoBase = (context as CreaspContext).NrAnoBase;
        }

        #region Public

        /// <summary>
        /// Lista relação entre contas contábeis e centros de custos que sejam orçáveis.
        /// </summary>
        public List<ContaContabil_CentroCusto> ListaRelacaoContaContabilComCentroCustoOrcavel()
        {
            var relacaoContaCentro = db.Query<ContaContabil_CentroCusto>()
                .Include(x => x.ContaContabil)
                .Include(x => x.CentroCusto)
                //.Include(x => x.CentroCusto.CentroCustoResponsavel)
                //.Where(x => x.CentroCusto.CentroCustoResponsavel.FgOrcavel)
                .ToList();

            var ccrService = new CentroCustoResponsavelService(context);
            relacaoContaCentro.ForEach(x => x.CentroCusto.CentroCustoResponsavel = ccrService.BuscaCentroCustoResponsavel(x.IdCentroCusto, true));

            //Lista todos os centros de custo do ano (para facilitar a pesquisa no fim do método).
            var centrosDeCusto = db.Query<CentroCusto>()
                .Where(x => x.NrAno == nrAnoBase)
                .ToList();

            //Lista todas contas contábeis do ano (para facilitar a pesquisa no fim do método).
            var contasContabeis = db.Query<ContaContabil>()
                .Where(x => x.NrAno == nrAnoBase)
                .ToList();

            //Lista o vínculo das contas contábeis com centros de custo a partir de dados importados do SISCONT no ano anterior.
            var posicoesAtuaisOrcamento = db.Query<PosicaoAtualOrcamento>()
                .Where(x => x.NrAno == nrAnoBase - 1)
                .ToList()
                .Select(x => new
                {
                    x.NrContaContabil5,
                    x.NrCentroCusto
                }
                )
                .Distinct()
                .ToList();

            if (posicoesAtuaisOrcamento.Count > 0)
            {
                CentroCusto ce;
                ContaContabil co;

                foreach (var item in posicoesAtuaisOrcamento)
                {
                    if ((ce = centrosDeCusto.Where(x => x.NrCentroCusto == item.NrCentroCusto).SingleOrDefault()) != null)
                    {
                        if ((co = contasContabeis.Where(x => x.NrContaContabil == item.NrContaContabil5).SingleOrDefault()) != null)
                        {
                            relacaoContaCentro.Add(new ContaContabil_CentroCusto()
                            {
                                IdCentroCusto = ce.IdCentroCusto,
                                CentroCusto = ce,
                                IdContaContabil = co.IdContaContabil,
                                ContaContabil = co
                            });
                        }
                    }
                }
            }

            return relacaoContaCentro.Distinct().ToList();
        }

        /// <summary>
        /// Altera toda uma lista de relacionamento entre conta contábil e centro de custo de uma só vez.
        /// </summary>
        public void Altera(List<ContaContabil_CentroCusto> relacaoContaCentro)
        {
            db.AuditUpdate(relacaoContaCentro);
        }

        #endregion
    }
}
