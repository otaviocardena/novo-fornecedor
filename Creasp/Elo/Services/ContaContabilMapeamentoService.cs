﻿using Creasp.Core;
using NBox.Core;
using NBox.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Creasp.Elo
{
    public class ContaContabilMapeamentoService : DataService
    {
        protected readonly int nrAnoBase;

        public ContaContabilMapeamentoService(ServiceContext context) : base(context)
        {
            nrAnoBase = (context as CreaspContext).NrAnoBase;
        }

        /// <summary>
        /// Lista os mapeamentos de contas contábeis na grid view
        /// </summary>
        public QueryPage<ContaContabilMapeamento> ListaContaContabilMapeamento(Pager p,
            string nrContaContabil = null,
            string noContaContabil = null,
            string tpContaContabil = null)
        {
            var contasContabeisMapeamento = db.Query<ContaContabilMapeamento>()
                .Include(x => x.ContaContabil5)
                .Include(x => x.ContaContabil6)
                .Where(x => x.ContaContabil6.NrAno == nrAnoBase)
                .Where(tpContaContabil != null, x =>
                        x.ContaContabil6.NrContaContabil.StartsWith(tpContaContabil.To<string>()))
                .Where(nrContaContabil != null, x =>
                        x.ContaContabil6.NrContaContabil.StartsWith(nrContaContabil.UnMask().ToLike()))
                .Where(noContaContabil != null, x =>
                        x.ContaContabil6.NoContaContabil.StartsWith(noContaContabil.ToLike()))
                .ToPage(p.DefaultSort<ContaContabilMapeamento>(x => x.ContaContabil6.NrContaContabil));

            return contasContabeisMapeamento;
        }

        /// <summary>
        /// Busca os mapeamentos entre contas contábeis 5 e 6
        /// </summary>
        public List<ContaContabilMapeamento> BuscaContasContabeisMapeadas()
        {
            return db.Query<ContaContabilMapeamento>()
                                        .Include(x => x.ContaContabil5)
                                        .Include(x => x.ContaContabil6)
                                        .Where(x => x.ContaContabil5.NrAno == nrAnoBase)
                                        .Where(x => x.ContaContabil6.NrAno == nrAnoBase)
                                        .ToList();
        }

        /// <summary>
        /// Verifica se existe mapeamento para uma conta contábil 6
        /// </summary>
        public bool ExisteMapeamento(int idContaContabil6)
        {
            return db.Query<ContaContabilMapeamento>()
                       .Where(x => x.IdContaContabil6 == idContaContabil6)
                       .Any();
        }

        /// <summary>
        /// Busca um mapeamento de uma conta contábil 6 específica
        /// </summary>
        public ContaContabilMapeamento BuscaContaMapeamento(int idContaContabil6)
        {
            return db.Query<ContaContabilMapeamento>()
                .Where(x => x.IdContaContabil6 == idContaContabil6)
                .SingleOrDefault();
        }

        /// <summary>
        /// Busca primeiro mapeamento, quando houver, de uma conta contábil 5 específica
        /// </summary>
        public List<ContaContabilMapeamento> BuscaConta5Mapeamentos(int idContaContabil5)
        {
            return db.Query<ContaContabilMapeamento>()
                .Where(x => x.IdContaContabil5 == idContaContabil5)?.ToList();
        }

        /// <summary>
        /// Valida e verifica se mapeamento já existe, antes de incluir
        /// </summary>
        public void ValidaEIncluiMapeamento(int idContaContabil6, int? idContaContabil5)
        {
            if (!ValidaMapeamento(idContaContabil6, idContaContabil5))
            {
                throw new NBoxException("Mapeamento inválido");
            }

            if (!ExisteMapeamento(idContaContabil6))
            {
                IncluiMapeamento(idContaContabil6, idContaContabil5);
            }
        }

        /// <summary>
        /// Valida antes de alterar o mapeamento
        /// </summary>
        public void ValidaEAlteraMapeamento(int idContaContabil6, int? idContaContabil5)
        {
            if (!ValidaMapeamento(idContaContabil6, idContaContabil5))
            {
                throw new NBoxException("Mapeamento inválido");
            }

            AlteraMapeamento(idContaContabil6, idContaContabil5);
        }

        /// <summary>
        /// Busca o número de uma contas contábil 6 a partir de um número de conta contábil 5 no ano base.
        /// Armazena a informação em cache.
        /// </summary>
        public string De5para6(string nrContaContabil5)
        {
            //Verifica se a conta passada já não é uma conta 6.
            if (nrContaContabil5.StartsWith(CdPrefixoContaContabil.Executado))
            {
                //Se for, retorna ela mesma.
                return nrContaContabil5;
            }

            //Verifica se a conta contábil é uma conta 5 (de previsão).
            if (nrContaContabil5.StartsWith(CdPrefixoContaContabil.Previsao))
            {
                return cache.Get<string>("conta-contabil-" + nrContaContabil5, 5, () =>
                {
                    //Busca uma conta 6 entre as contas mapeadas do ano base.
                    string nrContaContabil6 = db.Query<ContaContabilMapeamento>()
                                .Include(x => x.ContaContabil5)
                                .Include(x => x.ContaContabil6)
                                .Where(x => x.ContaContabil5.NrAno == nrAnoBase)
                                .Where(x => x.ContaContabil6.NrAno == nrAnoBase)
                                .Where(x => x.ContaContabil6.NrContaContabil.StartsWith("62211"))
                                .Where(x => x.ContaContabil5.NrContaContabil == nrContaContabil5)
                                .SingleOrDefault()?.ContaContabil6.NrContaContabil;

                    //Se encontrou, já retorna o número da conta 6.
                    if (!string.IsNullOrEmpty(nrContaContabil6)) return nrContaContabil6;

                    //Monta o regex para substituir o primeiro dígito da conta 5 (e tornar conta 6).
                    var regex = new Regex(Regex.Escape(CdPrefixoContaContabil.Previsao));
                    nrContaContabil6 = regex.Replace(nrContaContabil5, CdPrefixoContaContabil.Executado, 1);

                    //Verifica se essa conta 6 "montada" existe.
                    var contaContabilService = new ContaContabilService(context);
                    if (contaContabilService.ExisteConta(nrContaContabil6))
                    {
                        //Se existir, retorna ela.
                        return nrContaContabil6;
                    }

                    return string.Empty;
                });
            }

            return string.Empty;
        }

        /// <summary>
        /// Busca o número de uma contas contábil 5 a partir de um número de conta contábil 6 no ano base.
        /// Armazena a informação em cache.
        /// </summary>
        public string De6para5(string nrContaContabil6)
        {
            //Verifica se a conta passada já não é uma conta 5.
            if (nrContaContabil6.StartsWith(CdPrefixoContaContabil.Previsao))
            {
                //Se for, retorna ela mesma.
                return nrContaContabil6;
            }

            //Verifica se a conta contábil é uma conta 6 (de execução).
            if (nrContaContabil6.StartsWith(CdPrefixoContaContabil.Executado))
            {
                return cache.Get<string>("conta-contabil-" + nrContaContabil6, 5, () =>
                {
                    //Busca uma conta 5 entre as contas mapeadas do ano base.
                    string nrContaContabil5 = db.Query<ContaContabilMapeamento>()
                                .Include(x => x.ContaContabil5)
                                .Include(x => x.ContaContabil6)
                                .Where(x => x.ContaContabil5.NrAno == nrAnoBase)
                                .Where(x => x.ContaContabil6.NrAno == nrAnoBase)
                                .Where(x => x.ContaContabil6.NrContaContabil == nrContaContabil6)
                                .Where(x => x.IdContaContabil5 != null)
                                .SingleOrDefault()?.ContaContabil6.NrContaContabil;

                    //Se encontrou, já retorna o número da conta 5.
                    if (!string.IsNullOrEmpty(nrContaContabil5)) return nrContaContabil5;

                    //Monta o regex para substituir o primeiro dígito da conta 6 (e tornar conta 5).
                    var regex = new Regex(Regex.Escape(CdPrefixoContaContabil.Executado));
                    nrContaContabil5 = regex.Replace(nrContaContabil6, CdPrefixoContaContabil.Previsao, 1);

                    //Verifica se essa conta 5 "montada" existe.
                    var contaContabilService = new ContaContabilService(context);
                    if (contaContabilService.ExisteConta(nrContaContabil5))
                    {
                        //Se existir, retorna ela.
                        return nrContaContabil5;
                    }

                    return string.Empty;
                });
            }

            return string.Empty;
        }

        /// <summary>
        /// Busca uma conta contábil 6 oriunda de uma conta contábil 5. 
        /// </summary>
        /// <param name="lstContasMapeadas">Lista de todos os mapeamentos entre contas contábeil 5 e 6</param>
        /// <param name="nrContaContabil">Número da conta contábil 5</param>
        /// <returns>Número da conta contábil 6</returns>
        public string DePara5para6(List<ContaContabilMapeamento> lstContasMapeadas, string nrContaContabil)
        {
            if (nrContaContabil.StartsWith(CdPrefixoContaContabil.Executado))
            {
                return nrContaContabil;
            }

            if (nrContaContabil.StartsWith(CdPrefixoContaContabil.Previsao))
            {
                string nrContaContabil6 = BuscaContasContabeisMapeadas()
                                        .OrderBy(x => x.ContaContabil6.NrContaContabil)
                                        .Where(x => x.ContaContabil6.NrContaContabil.StartsWith("62211"))
                                        .Where(x => x.ContaContabil5.NrContaContabil == nrContaContabil)
                                        .SingleOrDefault()?.ContaContabil6.NrContaContabil;

                if (!string.IsNullOrEmpty(nrContaContabil6)) return nrContaContabil6;

                var regex = new Regex(Regex.Escape(CdPrefixoContaContabil.Previsao));
                nrContaContabil6 = regex.Replace(nrContaContabil, CdPrefixoContaContabil.Executado, 1);
				
				//Fixo até que se implemente o código agrupador.
                if (nrContaContabil == "52111060503005")
                {
                    nrContaContabil6 = "62111080203";
                }

                var contaContabilService = new ContaContabilService(context);

                if (contaContabilService.ExisteConta(nrContaContabil6))
                {
                    return nrContaContabil6;
                }
            }

            return string.Empty;
        }

        /// <summary>
        /// Busca uma conta contábil 5 oriunda de uma conta contábil 6. 
        /// </summary>
        /// <param name="lstContasMapeadas">Lista de todos os mapeamentos entre contas contábeil 5 e 6</param>
        /// <param name="nrContaContabil">Número da conta contábil 6</param>
        /// <returns>Número da conta contábil 5</returns>
        public string DePara6para5(List<ContaContabilMapeamento> lstContasMapeadas, string nrContaContabil)
        {
            if (nrContaContabil.StartsWith(CdPrefixoContaContabil.Previsao))
            {
                return nrContaContabil;
            }

            if (nrContaContabil.StartsWith(CdPrefixoContaContabil.Executado))
            {
                string nrContaContabil5 = lstContasMapeadas
                                        .Where(x => x.ContaContabil6.NrContaContabil == nrContaContabil)
                                        .Where(x => x.IdContaContabil5 != null)
                                        .SingleOrDefault()?.ContaContabil5.NrContaContabil;

                if (!string.IsNullOrEmpty(nrContaContabil5)) return nrContaContabil5;

                var regex = new Regex(Regex.Escape(CdPrefixoContaContabil.Executado));
                nrContaContabil5 = regex.Replace(nrContaContabil, CdPrefixoContaContabil.Previsao, 1);

                var contaContabilService = new ContaContabilService(context);

                if (contaContabilService.ExisteConta(nrContaContabil5))
                {
                    return nrContaContabil5;
                }
            }

            return string.Empty;
        }

        /// <summary>
        ///     Exclui um mapeamento entre contas 6 e 5
        /// </summary>
        public void ExcluiMapeamento(int idContaContabil5, int idContaContabil6)
        {
            var mapeamento = new ContaContabilMapeamento() { IdContaContabil5 = idContaContabil5, IdContaContabil6 = idContaContabil6 };

            db.Delete<ContaContabilMapeamento>(mapeamento);
        }

        /// <summary>
        /// Mapeia algumas contas contábeis (de todos os anos) pendentes de acordo com as regras:
        /// 1. A conta 6 e a 5 referente devem ter exatamente o mesmo nome.
        /// 2. O prefixo (composto, PELO MENOS, pelos primeiros 4 dígitos) devem ter apenas o primeiro dígito diferente (6 e 5).
        /// 3. Deve haver APENAS UMA conta 5 referente à conta 6 com as regras acima. Se houver 0 ou mais que 1, o mapeamento não é realizado.
        /// </summary>
        public void MapeiaContasContabeisPendentes()
        {
            //Lista todos os mapeamentos de contas contábeis que estão pendentes (de todos os anos).
            var mapeamentosPendentes = db.Query<ContaContabilMapeamento>()
                .Include(x => x.ContaContabil6)
                .Where(x => !x.IdContaContabil5.HasValue)
                .ToList();

            //Busca todas as contas contábeis iniciadas em 5 já existentes no banco de dados (de todos os anos).
            var contas5existentes = db.Query<ContaContabil>()
                .Where(x => x.FgAtivo)
                .Where(x => x.NrContaContabil.StartsWith(CdPrefixoContaContabil.Previsao))
                .ToList();

            string prefixoConta = string.Empty;

            foreach (var mp in mapeamentosPendentes)
            {
                try
                {
                    //Pega os 5 primeiros dígitos da conta contábil pendente, já substituindo o primeiro "6" por "5". 
                    prefixoConta = "5" + mp.ContaContabil6.NrContaContabil.Substring(1, 4);

                    //Se o 5º dígito do prefixo for "3", é porque está é uma conta de execução oriunda do RH Senior...
                    if (prefixoConta.EndsWith("3"))
                    {
                        //Então tem que substituir o 5º dígito por "1".
                        prefixoConta = prefixoConta.Substring(0, 4) + "1";
                    }

                    //Busca uma conta contábil referente a pendente
                    var contas5 = contas5existentes
                        .Where(x => x.NrAno == mp.ContaContabil6.NrAno)
                        .Where(x => x.NoContaContabil == mp.ContaContabil6.NoContaContabil)
                        .Where(x => x.NrContaContabil.StartsWith(prefixoConta))
                        .ToList();

                    if (contas5 == null || contas5.Count == 0)
                    {
                        //Se não encontrar nenhuma referente, tira o último dígito do prefixo para abranger mais resultados.
                        contas5 = contas5existentes
                            .Where(x => x.NrAno == mp.ContaContabil6.NrAno)
                            .Where(x => x.NoContaContabil == mp.ContaContabil6.NoContaContabil)
                            .Where(x => x.NrContaContabil.StartsWith(prefixoConta.Substring(0, 4)))
                            .ToList();
                    }

                    if (contas5 != null)
                    {
                        //Se houver mais que um item, é porque existe mais que uma possibilidade, então não deve fazer nada.
                        if (contas5.Count == 1)
                        {
                            //Atualiza o mapeamento pendente.
                            mp.IdContaContabil5 = contas5.Single().IdContaContabil;
                            mp.FgMapeadoPeloUsuario = false;
                            db.Update(mp);
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new NBoxException(ex.Message);
                }
            }
        }

        /// <summary>
        /// Replica o mapeamento de contas contábeis de um ano para outro, baseado no número das cnotas contábeis.
        /// </summary>
        public void ReplicaMapeamentoContasContabeis(int nrAnoOrigem, int nrAnoDestino)
        {
            //Lista os mapeamentos existentes no ano de origem.
            var mapeamentoAtual = db.Query<ContaContabilMapeamento>()
                .Include(x => x.ContaContabil5)
                .Include(x => x.ContaContabil6)
                .Where(x => x.ContaContabil5.NrAno == nrAnoOrigem)
                .Where(x => x.ContaContabil6.NrAno == nrAnoOrigem)
                .ToList();

            //Lista as contas contábeis ativas existentesno ano de destino.
            var contas = db.Query<ContaContabil>()
                .Where(x => x.NrAno == nrAnoDestino)
                .Where(x => x.FgAtivo)
                .ToList();

            //Para cada mapeamento existente da origem...
            foreach (var map in mapeamentoAtual)
            {
                //Busca as contas contábeis de destino com mesmo número de conta contábil.
                var c5 = contas.Where(x => x.NrContaContabil == map.ContaContabil5.NrContaContabil).SingleOrDefault();
                var c6 = contas.Where(x => x.NrContaContabil == map.ContaContabil6.NrContaContabil).SingleOrDefault();

                //Se uma das contas não for encontrada entre as contas ativas de origem, o mapeamento não será feito.
                if (c5 == null || c6 == null) continue;

                //Verifica se o mapeamento já existe na base de dados. Se ainda não existir...
                if (!db.Query<ContaContabilMapeamento>().Any(x => x.IdContaContabil5 == c5.IdContaContabil && x.IdContaContabil6 == c6.IdContaContabil))
                {
                    try
                    {
                        //Insere o mapeamento.
                        db.Insert(new ContaContabilMapeamento()
                        {
                            IdContaContabil5 = c5.IdContaContabil,
                            IdContaContabil6 = c6.IdContaContabil,
                            FgMapeadoPeloUsuario = false
                        });
                    }
                    catch (Exception ex)
                    {
                        throw new NBoxException(ex.Message);
                    }
                }
            }
        }

        /// <summary>
        /// Valida se as duas contas existem e se as contas contábeis começam com o prefixo correto
        /// </summary>
        private bool ValidaMapeamento(int idContaContabil6, int? idContaContabil5)
        {
            var contaContabilService = new ContaContabilService(context);

            var conta6 = contaContabilService.BuscaContaContabil(idContaContabil6);

            if (conta6 == null)
            {
                return false;
            }

            if (!conta6.NrContaContabil.StartsWith(CdPrefixoContaContabil.Executado))
            {
                return false;
            }

            if (idContaContabil5.HasValue)
            {
                var conta5 = contaContabilService.BuscaContaContabil(idContaContabil5.Value);
                if (conta5 != null && !conta5.NrContaContabil.StartsWith(CdPrefixoContaContabil.Previsao))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Efetua a inclusão do mapeamento
        /// </summary>
        private void IncluiMapeamento(int idContaContabil6, int? idContaContabil5)
        {
            var mapeamento = new ContaContabilMapeamento()
            {
                IdContaContabil6 = idContaContabil6,
                IdContaContabil5 = idContaContabil5,
                FgMapeadoPeloUsuario = false
            };

            db.Insert(mapeamento);
        }

        /// <summary>
        /// Efetua a alteração no mapeamento
        /// </summary>
        private void AlteraMapeamento(int idContaContabil6, int? idContaContabil5)
        {
            var contaMapeamento = BuscaContaMapeamento(idContaContabil6);
            contaMapeamento.IdContaContabil5 = idContaContabil5;
            db.Update(contaMapeamento);
        }
    }
}
