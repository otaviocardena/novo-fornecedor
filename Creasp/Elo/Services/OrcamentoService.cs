﻿using Creasp.Core;
using NBox.Services;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Creasp.Elo
{
    public class OrcamentoService : DataService
    {
        public OrcamentoService(ServiceContext context) : base(context) { }

        #region Public

        /// <summary>
        /// Lista as dotações orçamentárias do período indicado.
        /// </summary>
        /// <param name="anoInicial">Ano inicial da busca.</param>
        /// <param name="anoFinal">Ano final da busca.</param>
        /// <param name="fgReceita">Inicador de receita (TRUE) ou despesa (FALSE). Se for (NULL), traz receita e despesa.</param>
        public List<DotacaoOrcamentaria> ListaDotacoes(int anoInicial, int anoFinal, bool? fgReceita)
        {
            return db.Query<DotacaoOrcamentaria>()
                .Where(x => x.NrAno >= anoInicial)
                .Where(x => x.NrAno <= anoFinal)
                .Where(fgReceita.HasValue, x => x.FgReceita == fgReceita.Value)
                .Where(x => x.FgAtivo)
                .ToList();
        }

        /// <summary>
        /// Lista a "posição atual do orçamento" do período indicado.
        /// </summary>
        public List<PosicaoAtualOrcamento> ListaPosicaoAtualOrcamento(int anoInicial, int anoFinal)
        {
            return db.Query<PosicaoAtualOrcamento>()
                .Where(x => x.NrAno >= anoInicial)
                .Where(x => x.NrAno <= anoFinal)
                .Where(x => !x.FgReceita)
                .Where(x => x.FgAtivo)
                .ToList();
        }

        /// <summary>
        /// Lista a disponibilidade atual do orçamento" do período indicado.
        /// </summary>
        public List<DisponibilidadeAtualOrcamento> ListaDisponibilidadeAtualOrcamento(int anoInicial, int anoFinal)
        {
            return db.Query<DisponibilidadeAtualOrcamento>()
                .Where(x => x.NrAno >= anoInicial)
                .Where(x => x.NrAno <= anoFinal)
                .Where(x => x.FgAtivo)
                .ToList();
        }

        /// <summary>
        /// Atualiza o valor executado (com sua data de referência) e o valor de dotação atual dos itens das propostas orçamentárias do ano base.
        /// </summary>
        public void AtualizaValoresNasPropostasEReformulacoes(int nrAno)
        {
            var posicaoAtualOrcamento = db.Query<PosicaoAtualOrcamento>()
                .Where(x => x.NrAno == nrAno)
                .ToList();

            if (posicaoAtualOrcamento.Count > 0)
            {
                var propostasOrcamentariasItens = db.Query<PropostaOrcamentariaDespesaItem>()
                    .Include(x => x.PropostaOrcamentariaDespesa)
                    .Include(x => x.PropostaOrcamentariaDespesa.CentroCusto)
                    .Include(x => x.ContaContabil)
                    .Where(x => x.PropostaOrcamentariaDespesa.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.ContabValidacao)
                    .Where(x => x.PropostaOrcamentariaDespesa.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.Plenario)
                    .Where(x => x.PropostaOrcamentariaDespesa.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.PlenarioOK)
                    .Where(x => x.PropostaOrcamentariaDespesa.NrAno == nrAno + 1)
                    .Where(x => x.NrVersao == 0)
                    .Where(x => x.PropostaOrcamentariaDespesa.CdTpFormulario == null)
                    .ToList();

                var reformulacoesOrcamentariasItens = db.Query<ReformulacaoOrcamentariaItem>()
                    .Include(x => x.ReformulacaoOrcamentaria)
                    .Include(x => x.ReformulacaoOrcamentaria.Reformulacao)
                    .Include(x => x.ReformulacaoOrcamentaria.CentroCusto)
                    .Include(x => x.ContaContabil)
                    .Where(x => x.ReformulacaoOrcamentaria.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.ContabValidacao)
                    .Where(x => x.ReformulacaoOrcamentaria.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.Plenario)
                    .Where(x => x.ReformulacaoOrcamentaria.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.PlenarioOK)
                    .Where(x => x.ReformulacaoOrcamentaria.Reformulacao.NrAno == nrAno)
                    .Where(x => x.ReformulacaoOrcamentaria.CdTpFormulario == null)
                    .ToList();

                if (propostasOrcamentariasItens.Count > 0 || reformulacoesOrcamentariasItens.Count > 0)
                {
                    using (var trans = db.GetTransaction())
                    {
                        foreach (var p in posicaoAtualOrcamento)
                        {
                            var itensParaAtualizar = propostasOrcamentariasItens
                                .Where(x => x.PropostaOrcamentariaDespesa.CentroCusto.NrCentroCusto == p.NrCentroCusto)
                                .Where(x => x.ContaContabil.NrContaContabil == p.NrContaContabil5)
                                .ToList();

                            foreach (var item in itensParaAtualizar)
                            {
                                item.DtRefVrExecutado = p.DtRefVrExecutado;
                                item.VrExecutadoAnoAnterior = p.VrExecutado;
                                item.VrAprovadoAnoAnterior = p.VrEmpExercicio + p.VrSaldoOrcamentarioExercicio;

                                db.Update(item);
                            }

                            var refItensParaAtualizar = reformulacoesOrcamentariasItens
                                .Where(x => x.ReformulacaoOrcamentaria.CentroCusto.NrCentroCusto == p.NrCentroCusto)
                                .Where(x => x.ContaContabil.NrContaContabil == p.NrContaContabil5)
                                .ToList();

                            foreach (var item in refItensParaAtualizar)
                            {
                                item.DtRefVrExecutado = p.DtRefVrExecutado;
                                item.VrExecutado = p.VrExecutado;
                                item.VrDotacaoAtual = p.VrEmpExercicio + p.VrSaldoOrcamentarioExercicio;

                                db.Update(item);
                            }
                        }

                        trans.Complete();
                    }
                }
            }
        }

        /// <summary>
        /// Atualiza o valor orçado (oriundo da Senior) nas contas de RH das propostas orçamentárias.
        /// </summary>
        public void AtualizaVrOrcadoPropostasOrcamentarias(int nrAno)
        {
            var orcamentoSenior = db.Query<OrcamentoSenior>()
                .Where(x => x.NrAno == nrAno)
                .ToList();

            var propostasOrcamentariasItens = db.Query<PropostaOrcamentariaDespesaItem>()
                .Include(x => x.PropostaOrcamentariaDespesa)
                .Include(x => x.PropostaOrcamentariaDespesa.CentroCusto)
                .Include(x => x.ContaContabil)
                .Where(x => x.PropostaOrcamentariaDespesa.NrAno == nrAno)
                .Where(x => x.PropostaOrcamentariaDespesa.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.ContabValidacao)
                .Where(x => x.PropostaOrcamentariaDespesa.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.Plenario)
                .Where(x => x.PropostaOrcamentariaDespesa.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.PlenarioOK)
                .ToList();

            using (var trans = db.GetTransaction())
            {
                foreach (var p in propostasOrcamentariasItens)
                {
                    var valoresOrcadosSenior = orcamentoSenior
                        .Where(x => ("0" + x.NrCentroCusto).StartsWith(p.PropostaOrcamentariaDespesa.CentroCusto.NrCentroCusto))
                        .Where(x => x.NrContaContabil5 == p.ContaContabil.NrContaContabil)
                        .Select(x => x.VrOrcamento)
                        .ToList();

                    if (valoresOrcadosSenior.Count > 0)
                    {
                        p.VrOrcado = valoresOrcadosSenior.Sum();
                        p.VrOrcadoAntesFinalizar = p.VrOrcado;
                        p.NoEspecificacao = "Folha pagamento";
                        p.NoJustificativa = "Folha pagamento";
                        p.FgVrOrcadoOrigemSenior = true;
                        p.DtAlteracao = DateTime.Now;
                        p.LoginAlteracao = "sincronismo";
                        db.Update(p);
                    }

                    //var reformulacaoItem = reformulacoesOrcamentariasItens
                    //    .Where(x => x.ReformulacaoOrcamentaria.CentroCusto.NrCentroCusto == p.NrCentroCusto)
                    //    .Where(x => x.ContaContabil.NrContaContabil == p.NrContaContabil)
                    //    .SingleOrDefault();

                    //if (reformulacaoItem != null)
                    //{
                    //    reformulacaoItem.DtRefVrExecutado = p.DtRefVrExecutado;
                    //    reformulacaoItem.VrExecutado = p.VrExecutado;
                    //    reformulacaoItem.VrDotacaoAtual = p.VrEmpExercicio + p.VrSaldoOrcamentarioExercicio;

                    //    db.Update(reformulacaoItem);
                    //}
                }

                trans.Complete();
            }
        }

        /// <summary>
        /// Lista o orçamento de RH da Senior do ano indicado, preenchendo a coluna de "NrContaContabil".
        /// </summary>
        /// <param name="nrAno"></param>
        /// <returns></returns>
        public List<OrcamentoSenior> ListaOrcamentoSenior(int nrAno)
        {
            var orcamentoSenior = db.Query<OrcamentoSenior>()
                .Where(x => x.NrAno == nrAno)
                .Where(x => x.FgAtivo)
                .ToList();

            //Preenche o número da conta contábil
            var contaContabilService = new ContaContabilService(context);
            var contaMapeamentoService = new ContaContabilMapeamentoService(context);
            var contasContabeis = contaContabilService.ListaContasContabeis(nrAno, CdPrefixoContaContabil.Executado);
            var mapeamentosContas = contaMapeamentoService.BuscaContasContabeisMapeadas();


            for (int i = 0; i < orcamentoSenior.Count; i++)
            {
                //TODO: evitar de chamar esse DePara para cada registro, poderia ser feito no momento do sincronismo. Problema: dependência de sincronizar os dados depois do mapeamento de contas.
                orcamentoSenior[i].NrContaContabil6 = contasContabeis.SingleOrDefault(x => x.CdResumido == orcamentoSenior[i].CdContaContabilResumido)?.NrContaContabil;
                orcamentoSenior[i].NrContaContabil5 = contaMapeamentoService.DePara6para5(mapeamentosContas, orcamentoSenior[i].NrContaContabil6);
            }

            return orcamentoSenior;
        }

        #endregion
    }
}
