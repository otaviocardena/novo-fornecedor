﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Elo
{
    public class RelatorioDemonstrativoReceitaDespesaSinteticoDTO
    {
        public int NrAno { get; set; }
        public bool BoldReceita { get; set; }
        public bool BoldDespesa { get; set; }
        public string NrContaContabilReceita { get; set; }
        public string NoContaContabilReceita { get; set; }
        public decimal? VrOrcadoParcialReceita { get; set; }
        public decimal? VrOrcadoTotalReceita { get; set; }
        public string NrContaContabilDespesa { get; set; }
        public string NoContaContabilDespesa { get; set; }
        public decimal? VrOrcadoParcialDespesa { get; set; }
        public decimal? VrOrcadoTotalDespesa { get; set; }
    }
}
