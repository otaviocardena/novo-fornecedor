﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Elo
{
    public class RelatorioDemonstrativoReceitaDespesaSinteticoResumoDTO
    {
        public string NoItem { get; set; }
        public decimal VrReceita { get; set; }
        public decimal VrDespesa { get; set; }
    }
}
