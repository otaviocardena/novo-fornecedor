﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Elo
{
    public class RelatorioDespesaDetalhadoRegistroDTO
    {
        public int NrAno { get; set; }
        public bool Bold { get; set; }
        public string Cor { get; set; }
        public string AgrupadorLinhas { get; set; }
        public string NoCentroCustoFmt { get; set; }
        public string NrContaContabil { get; set; }
        public string NoContaContabil { get; set; }
        public string NrVersao { get; set; }
        public string NoEspecificacao { get; set; }
        public string NoJustificativa { get; set; }
        public decimal VrOrcado { get; set; }
        public string NrProcesso { get; set; }
    }
}
