﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Elo
{
    public class RelatorioReformulacaoAnaliticoReceitaDTO
    {
        public int NrAno { get; set; }
        public DateTime DtRefVrArrecadado { get; set; }
        public bool Bold { get; set; }
        public string NrContaContabil { get; set; }
        public string NoContaContabil { get; set; }
        public decimal VrOrcadoInicialmente { get; set; }
        public decimal VrReformulacaoAnterior { get; set; }
        public decimal VrExecutado { get; set; }
        public decimal PrExecutado { get; set; }
        public decimal VrSuplementacao { get; set; }
        public decimal PrSuplementacao { get; set; }
        public decimal VrReducao { get; set; }
        public decimal PrReducao { get; set; }
        public decimal VrOrcamentoReformulado { get; set; }
    }
}
