﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Elo.DTO
{
    public class CentroCustoDTO
    {
        public int IdCentroCusto { get; set; }
        public string NrCentroCustoFmt { get; set; }
        public string NoCentroCustoFmt { get; set; }
        public string TpCentroCusto { get; set; }
        public decimal Valor { get; set; }
    }
}
