﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Elo
{
    public class RelatorioMetodologiaDTO
    {
        public int NrAno { get; set; }
        public string NrContaContabil { get; set; }
        public string NoContaContabil { get; set; }
        public decimal VrOrcado { get; set; }
        public string NoObservacoes { get; set; }
        public decimal PrCotaParteConfea { get; set; }
        public decimal PrCotaParteMutua { get; set; }
        public decimal PrCotaParteMutuaSP { get; set; }
        public decimal PrCotaParteMutuaBSB { get; set; }
        public decimal VrCotaParteConfea { get; set; }
        public decimal VrCotaParteMutua { get; set; }
        public decimal VrCotaParteMutuaSP { get; set; }
        public decimal VrCotaParteMutuaBSB { get; set; }
        public decimal VrReceitaBrutaJan { get; set; }
        public decimal VrReceitaBrutaFev { get; set; }
        public decimal VrReceitaBrutaMar { get; set; }
        public decimal VrReceitaBrutaAbr { get; set; }
        public decimal VrReceitaBrutaMai { get; set; }
        public decimal VrReceitaBrutaJun { get; set; }
        public decimal VrReceitaBrutaJul { get; set; }
        public decimal VrReceitaBrutaAgo { get; set; }
        public decimal VrReceitaBrutaSet { get; set; }
        public decimal VrReceitaBrutaOut { get; set; }
        public decimal VrReceitaBrutaNov { get; set; }
        public decimal VrReceitaBrutaDez { get; set; }        
        public decimal VrReceitaLiquidaJan { get; set; }
        public decimal VrReceitaLiquidaFev { get; set; }
        public decimal VrReceitaLiquidaMar { get; set; }
        public decimal VrReceitaLiquidaAbr { get; set; }
        public decimal VrReceitaLiquidaMai { get; set; }
        public decimal VrReceitaLiquidaJun { get; set; }
        public decimal VrReceitaLiquidaJul { get; set; }
        public decimal VrReceitaLiquidaAgo { get; set; }
        public decimal VrReceitaLiquidaSet { get; set; }
        public decimal VrReceitaLiquidaOut { get; set; }
        public decimal VrReceitaLiquidaNov { get; set; }
        public decimal VrReceitaLiquidaDez { get; set; }

        public decimal VrReceitaBrutaAno
        {
            get
            {
                return VrReceitaBrutaJan + VrReceitaBrutaFev + VrReceitaBrutaMar + VrReceitaBrutaAbr +
                       VrReceitaBrutaMai + VrReceitaBrutaJun + VrReceitaBrutaJul + VrReceitaBrutaAgo +
                       VrReceitaBrutaSet + VrReceitaBrutaOut + VrReceitaBrutaNov + VrReceitaBrutaDez ;
            }
        }

        public decimal VrReceitaLiquidaAno
        {
            get
            {
                return VrReceitaLiquidaJan + VrReceitaLiquidaFev + VrReceitaLiquidaMar + VrReceitaLiquidaAbr +
                       VrReceitaLiquidaMai + VrReceitaLiquidaJun + VrReceitaLiquidaJul + VrReceitaLiquidaAgo +
                       VrReceitaLiquidaSet + VrReceitaLiquidaOut + VrReceitaLiquidaNov + VrReceitaLiquidaDez;
            }
        }


    }
}