﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Elo
{
    public class PropostaReformulacaoItensDTO
    {
        public int IdContaContabil { get; set; }
        public string NrContaContabil { get; set; }
        public int? IdPropostaOrcamentariaDespesaItem { get; set; }
        public string NoJustificativa { get; set; }
        public decimal? VrOrcadoInicialmente { get; set; }
        public decimal? VrReducao { get; set; }
        public decimal? VrSuplementacao { get; set; }
    }
}
