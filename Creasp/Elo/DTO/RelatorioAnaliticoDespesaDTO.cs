﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Elo
{
    public class RelatorioAnaliticoDespesaDTO
    {
        public int NrAno { get; set; }
        public DateTime DtRefVrExecutado { get; set; }
        public bool Bold { get; set; }
        public string NrContaContabil { get; set; }    
        public string NoContaContabil { get; set; }
        public decimal VrAprovado { get; set; }
        public decimal VrExecutado { get; set; }
        public decimal PrExecutado { get; set; }
        public decimal VrOrcado { get; set; }
        public decimal PrOrcado { get; set; }
    }
}
