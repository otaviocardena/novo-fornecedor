﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Creasp.Elo;

namespace Creasp.Elo
{
    public class PropostaReformulacaoDTO
    {
        public PropostaReformulacaoDTO()
        {
            PropostaReformulacaoItensDTO = new List<PropostaReformulacaoItensDTO>();
        }

        public int IdCentroCustoOriginal { get; set; }
        public string NrCentroCustoOriginal { get; set; }
        public string CdTpFormulario { get; set; }
        public int? IdPessoaResp { get; set; }
        public int? IdPropostaOrcamentariaDespesa { get; set; }
        public List<PropostaReformulacaoItensDTO> PropostaReformulacaoItensDTO { get; set; }

        public static explicit operator PropostaReformulacaoDTO(PropostaOrcamentariaDespesa v)
        {
            return new PropostaReformulacaoDTO()
            {
                IdCentroCustoOriginal = v.IdCentroCustoOriginal,
                NrCentroCustoOriginal = v.CentroCustoOriginal.NrCentroCusto,
                CdTpFormulario = v.CdTpFormulario,
                IdPessoaResp = v.CentroCustoOriginal.CentroCustoResponsavel.IdPessoaExecutor,
                IdPropostaOrcamentariaDespesa = v.IdPropostaOrcamentariaDespesa,
                PropostaReformulacaoItensDTO = v.PropostaOrcamentariaItens?.Select(x => new PropostaReformulacaoItensDTO()
                {
                    IdContaContabil = x.IdContaContabil,
                    NrContaContabil = x.ContaContabil.NrContaContabil,
                    IdPropostaOrcamentariaDespesaItem = x.IdPropostaOrcamentariaDespesaItem,
                    NoJustificativa = x.NoJustificativa,
                    VrOrcadoInicialmente = x.VrOrcado,
                    VrReducao = 0,
                    VrSuplementacao = 0
                }).ToList()
            };
        }

        public static explicit operator PropostaReformulacaoDTO(ReformulacaoOrcamentaria v)
        {
            return new PropostaReformulacaoDTO()
            {
                IdCentroCustoOriginal = v.IdCentroCusto,
                NrCentroCustoOriginal = v.CentroCustoOriginal.NrCentroCusto,
                CdTpFormulario = v.CdTpFormulario,
                IdPessoaResp = v.CentroCustoOriginal.CentroCustoResponsavel.IdPessoaExecutor,
                IdPropostaOrcamentariaDespesa = v.IdPropostaOrcamentariaDespesa,
                PropostaReformulacaoItensDTO = v.ReformulacaoOrcamentariaItens?.Select(x => new PropostaReformulacaoItensDTO()
                {
                    IdContaContabil = x.IdContaContabil,
                    NrContaContabil = x.ContaContabil.NrContaContabil,
                    IdPropostaOrcamentariaDespesaItem = x.IdPropostaOrcamentariaDespesaItem ?? null,
                    NoJustificativa = x.NoJustificativa,
                    VrOrcadoInicialmente = x.VrOrcadoInicialmente,
                    VrReducao = x.VrReducao,
                    VrSuplementacao = x.VrSuplementacao
                }).ToList()
            };
        }



        //IdCentroCusto = proposta.IdCentroCusto,
        //CdTpFormulario = proposta.CdTpFormulario,
        //CdStatusPropostaOrcamentaria = StPropostaOrcamentaria.Executor,
        //IdPessoaResp = proposta.CentroCusto.CentroCustoResponsavel.IdPessoaExecutor,
        //IdReformulacao = reformulacao.IdReformulacao,
        //IdPropostaOrcamentariaDespesa = proposta.IdPropostaOrcamentariaDespesa,
        //LoginCadastro = user.Login
    }
}
