﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Elo
{
    public class RelatorioDespesaDetalhadoDTO
    {
        public int NrVersao { get; set; }
        public int IdContaContabil { get; set; }
        public string NrContaContabil { get; set; }
        public string NoContaContabil { get; set; }
        public string NrContaContabilFmt { get; set; }
        public string NrContaContabilPai { get; set; }
        public string NoEspecificacao { get; set; }
        public string NoJustificativa { get; set; }
        public int IdCentroCusto { get; set; }
        public string NrCentroCusto { get; set; }
        public string NoCentroCustoFmt { get; set; }
        public decimal? VrOrcadoAntesFinalizar { get; set; }
        public string Nrprocesso { get; set; }
    }
}
