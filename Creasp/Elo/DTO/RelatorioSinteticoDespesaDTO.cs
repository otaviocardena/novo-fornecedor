﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Elo
{
    public class RelatorioSinteticoDespesaDTO
    {
        public int NrAno { get; set; }
        public DateTime DtRefVrExecutado { get; set; }
        public bool Bold { get; set; }
        public string NrContaContabil { get; set; }
        public string NoContaContabil { get; set; }
        public decimal VrOrcado2AnoAtras { get; set; }
        public decimal VrExecutado2AnoAtras { get; set; }
        public decimal VrOrcado1AnoAtras { get; set; }
        public decimal VrExecutado1AnoAtras { get; set; }
        public decimal VrOrcado { get; set; }
    }
}
