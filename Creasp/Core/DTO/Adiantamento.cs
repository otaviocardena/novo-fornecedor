﻿using System;
using NBox.Core;

namespace Creasp.Core
{
    public class Adiantamento
    {
        public Guid IdSolicAdiantamento { get; set; }
        public int NrAno { get; set; }
        public int NrAdiantamento { get; set; }
        public DateTime DtAdiantamento { get; set; }
        public bool FgContaPrestada { get; set; }
        public decimal? VrAdiantado { get; set; }
        public decimal? VrDevolvido { get; set; }
        public decimal? VrFinal { get; set; }
    }
}