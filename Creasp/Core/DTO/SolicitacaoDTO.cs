﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Core.DTO
{
    public class SolicitacaoDTO
    {
        public int IdSolicitacao { get; set; }
        public int IdPessoa { get; set; }
        public DateTime DataVencimentoBoleto { get; set; } = Periodo.MinDate;
        public int IdCertidao { get; set; }
        public int IdRelatorio { get; set; }
        public int IdNF { get; set; }
        public string ValorBruto { get; set; }
        public string Observacoes { get; set; }
        public bool CheckAssinatura { get; set; }
        public bool AprovadoADM { get; set; }
        public bool AprovadoTEC { get; set; }
        public string ObsAprovador { get; set; }
        public string VlrSugerido { get; set; }
        public string Status { get; set; }
        public string ObsRecusa { get; set; }
        public bool Tributos { get; set; }
        public string NrEmpenho { get; set; }
        public string GestorEmail { get; set; }
        public string TecEmail { get; set; }
        public int? TecId { get; set; }
        public int? GestorId { get; set; }
        public string TecNome { get; set; }
        public string GestorNome { get; set; }
        public string Aliquota { get; set; }
        public string BaseCalc { get; set; }
        public string TributoOuRetencao { get; set; }
        public DateTime? Vencimento { get; set; }
        public int IdTributoDoc { get; set; }
        public string NumNf { get; set; }
        public string Etapa { get; set; }


        public DateTime DtCadastro { get; set; } = DateTime.Now;
        public DateTime? DtAlteracao { get; set; }
        public string LoginCadastro { get; set; }
        public string LoginAlteracao { get; set; }
    }
}
