﻿using System;
using NBox.Core;

namespace Creasp.Core
{
    public class FuncionarioHierarquia : Funcionario
    {
        public Funcionario Chefe { get; set; } = new Funcionario();
        public Funcionario Gerente { get; set; } = new Funcionario();
        public Funcionario Superintendente { get; set; } = new Funcionario();
    }
}