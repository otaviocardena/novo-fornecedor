﻿using System;
using NBox.Core;

namespace Creasp.Core
{
    public class Funcionario
    {
        public Pessoa Pessoa { get; set; }
        public Unidade Unidade { get; set; }
    }
}