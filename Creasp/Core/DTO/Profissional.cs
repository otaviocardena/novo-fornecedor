﻿using System;
using NBox.Core;

namespace Creasp.Core
{
    public class Profissional
    {
        public string NrCreasp { get; set; }
        public string NrCpf { get; set; }
        public string NoProfissional { get; set; }
        public DateTime DtNasc { get; set; }
        public string TpSexo { get; set; }
        public string NoEmail { get; set; }
        public int[] Camaras { get; set; }
        public string[] Titulos { get; set; }
        public ProfissionalEndereco[] Enderecos { get; set; }

        public string NoTitulo
        {
            get
            {
                return string.Join(", ", Titulos);
            }
        }
    }

    public class ProfissionalEndereco
    {
        public string TpEndereco { get; set; }
        public string NrCep { get; set; }
        public string NoLogradouro { get; set; }
        public string NrEndereco { get; set; }
        public string NoComplemento { get; set; }
        public string NoBairro { get; set; }
        public string NoCidade { get; set; }
        public string CdUf { get; set; }
        public bool FgCorrespondencia { get; set; }
        public string NrTelefone { get; set; }

        public string NrCepFmt => NrCep.Mask("99.999-999");

        public string NoEnderecoFmt =>
            (NoLogradouro == null ? "" : NoLogradouro + ", " + NrEndereco) + 
            (NoComplemento == null ? "" : " - " + NoComplemento) +
            (NoBairro == null ? "" : "\n" + NoBairro) +
            (NoCidade == null ? "" : "\n" + NoCidade + " / " + CdUf) +
            (NrCep == null ? "" : "\n" + NrCepFmt);
    }
}