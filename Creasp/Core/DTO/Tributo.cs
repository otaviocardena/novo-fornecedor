﻿using System;
using NBox.Core;

namespace Creasp.Core
{
    public class Tributo
    {
        public string CdTributo { get; set; }
        public string NoTributo { get; set; }
    }
}