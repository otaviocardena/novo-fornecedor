﻿using System;
using NBox.Core;

namespace Creasp.Core
{
    /// <summary>
    /// Classe DTO para exibir dados de uma ROTA
    /// </summary>
    [Serializable]
    public class Rota
    {
        public string NrCepOrigem { get; set; }
        public string NrCepDestino { get; set; }

        public int CdCidadeOrigem { get; set; }
        public int CdCidadeDestino { get; set; }

        public string CdUfOrigem { get; set; }
        public string CdUfDestino { get; set; }

        public string NoOrigem { get; set; }
        public string NoDestino { get; set; }

        public bool FgIdaVolta { get; set; }

        public int QtKmIda { get; set; }
        public int QtKmVolta { get; set; }

        public string NrCepOrigemFmt => NrCepOrigem.Mask("99.999-999");
        public string NrCepDestinoFmt => NrCepDestino.Mask("99.999-999");

        /// <summary>
        /// Calcula a distancia total (ida e volta, se for o caso). Quando faltar um valor, usa o que tiver (ex. Tenho só ida e não tenho volta, a ida/volta é 2xida)
        /// </summary>
        public int QtKmTotal => FgIdaVolta ? 
            (QtKmIda > 0 ? QtKmIda : QtKmVolta) + (QtKmVolta > 0 ? QtKmVolta : QtKmIda) : 
            (QtKmIda > 0 ? QtKmIda : QtKmVolta);

        public string NoRota => NoOrigem + (FgIdaVolta ? " <=> " : " => ") + NoDestino;

    }
}