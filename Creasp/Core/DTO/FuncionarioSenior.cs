﻿using System;
using NBox.Core;
using NPoco;

namespace Creasp.Core
{
    [PrimaryKey("NrMatricula", AutoIncrement = false), Serializable]
    public class FuncionarioSenior
    {
        public int NrMatricula { get; set; }
        public string NrCpf { get; set; }
        public string NoFuncionario { get; set; }
        public string NoCargo { get; set; }
        public string NrCentroCusto { get; set; }

        public string NrCpfChefe { get; set; }
        public string NrCpfGerente { get; set; }
        public string NrCpfSuperintendente { get; set; }

        public string NoChefe { get; set; }
        public string NoGerente { get; set; }
        public string NoSuperintendente { get; set; }
    }
}