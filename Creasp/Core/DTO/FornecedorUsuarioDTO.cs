﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Core.DTO
{
    public class FornecedorUsuarioDTO
    {
        public DateTime DataCadastro { get; set; } = DateTime.Now;
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
        public string IdSecao { get; set; }
    }
}
