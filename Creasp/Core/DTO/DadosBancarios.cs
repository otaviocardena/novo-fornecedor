﻿using System;

namespace Creasp.Core
{
    /// <summary>
    /// Dados de conta bancária para enviar para o SISCONT
    /// </summary>
    public class DadosBancarios : ICloneable
    {
        public string NrBanco { get; set; }
        public string NoBanco { get; set; }
        public string NrAgencia { get; set; }
        public string NrAgenciaDv { get; set; }
        public string NrConta { get; set; }
        public string NrContaDv { get; set; }
        public bool FgContaCorrente { get; set; }

        public object Clone()
        {
            return MemberwiseClone();
        }

        /// <summary>
        /// Entrega dados de conta bancária formatada pro usuario
        /// </summary>
        public string NoDadosBancarios => NrBanco + " - " +
            NrAgencia + (string.IsNullOrEmpty(NrAgenciaDv) ? "" : "-" + NrAgenciaDv) + " / " +
            NrConta + (string.IsNullOrEmpty(NrContaDv) ? "" : "-" + NrContaDv);
    }
}