﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;

namespace Creasp.Core
{
    public interface IFgAtivo
    {
        bool FgAtivo { get; set; }
    }
}