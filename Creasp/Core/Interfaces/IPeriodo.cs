﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;

namespace Creasp.Core
{
    public interface IPeriodo
    {
        DateTime DtIni { get; set; }
        DateTime DtFim { get; set; }
    }
}