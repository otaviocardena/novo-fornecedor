﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using Creasp.Elo;
using Creasp.Siscont.Despesa;

namespace Creasp.Core
{
    public interface ISiscont
    {
        /// <summary>
        /// Busca pessoas cadastradas no Sistcont
        /// </summary>
        IEnumerable<Pessoa> BuscaPessoas(string cpf, string nome);

        /// <summary>
        /// Inclui ou altera os dados de uma pessoa no siscont
        /// </summary>
        void IncluirOuAlteraPessoa(Pessoa pessoa);

        /// <summary>
        /// Retorna todos os cnetros de custos ativos no momento da pesquisa
        /// </summary>
        IEnumerable<CentroCusto> ListaCentroCusto(int nrAno);

        /// <summary>
        /// Retorna todos as contas contabeis do plano de contas
        /// </summary>
        IEnumerable<ContaContabil> ListaPlanoContas(int nrAno);

        /// <summary>
        /// Retorna todos as contas contabeis do plano de contas
        /// </summary>
        IEnumerable<Tributo> ListaTributos();

        /// <summary>
        /// Inclui uma solicitação de emepnho
        /// </summary>
        Guid IncluiSolicEmp(EmpenhoTipo tipo, string nrContaContabilFmt, DateTime dtEmpenho, decimal vrEmpenho, Pessoa pessoa, string historico, Dictionary<string, decimal> centroCustoValor);

        /// <summary>
        /// Retorna todas as dotações orçamentárias do ano de exercício indicado
        /// </summary>
        /// <returns></returns>
        IEnumerable<DotacaoOrcamentaria> ListaDotacaoOrcamentaria(int nrAno);

        /// <summary>
        /// Retorna a posição atual do orçamento de despesa (valores já executados)
        /// </summary>
        /// <returns></returns>
        IEnumerable<PosicaoAtualOrcamento> ListaPosicaoAtualOrcamento(List<string> contasContabeis, int nrAno);

        /// <summary>
        /// Retorna a posição atual do orçamento de receita (valores já arrecadados)
        /// </summary>
        IEnumerable<DisponibilidadeAtualOrcamento> ListaDisponibilidadeOrcamento(int nrAno);

        /// <summary>
        /// Retorna os contratos dos centros de contas
        /// </summary>
        /// <returns></returns>
        IEnumerable<ContratoOrcamentario> ListaContratos();

        //void EnviaDotacoesOrcamentarias(List<DotacaoOrcamentaria> dotacoes);
        //object BuscaSaldoOrcamentario(int ano, string nrContaContabil, string nrCentroCusto);

        /// <summary>
        /// Consulta os empenhos no sistema SISCONT
        /// </summary>
        Siscont.Despesa.IntegracaoEmpenhosEntity[] ConsultaEmpenho(int nrAno, Guid? nrSolicEmp = null, string nrCpfFmt = null, int? nrEmp = null, string nrProcesso = null, bool? fgRestoAPagar = null, bool? fgProcessado = null);

        /// <summary>
        /// Consulta o saldo de orçamento para uma conta/centro de custo
        /// </summary>
        Siscont.Orcamento.DisponibilidadeOrcamentariaEntity ConsultaSaldoOrcamentario(DateTime dtRef, string nrContaContabil, string nrCentroCusto);

        /// <summary>
        /// Lista os tipos de documento para pagamento
        /// </summary>
        IEnumerable<string> ListaTipoDocumentos();

        /// <summary>
        /// Faz a solicitação de pagamento de um conjunto de items de nerp
        /// </summary>
        void IncluiSolicPagtos(Siscont.Despesa.PagamentosAutorizadosEntity[] pagtos);

        /// <summary>
        /// Faz a solicitação de pagamento de um conjunto de items de nerp
        /// </summary>
        Siscont.Despesa.PagamentosEntity[] ConsultaPagto(int nrAno, Guid? nrSolicPagto = null, string nrCpf = null, int? nrPagto = null, int? nrEmp = null, string noFavorecido = null);

        /// <summary>
        /// Faz uma solicitação de Adiantamento e retorna no mesmo array os Id gerados
        /// </summary>
        void IncluiSolicAdiantamento(SolicitacoesAdiantamentosEntity[] adiantamentos);

        /// <summary>
        /// Consulta se um adiantamento foi acatado pelo usuario no SISCONT
        /// </summary>
        Adiantamento ConsultaAdiantamento(int nrAno, Guid nrSolicAdiantamento);

        /// <summary>
        /// Ping de verificação do serviço
        /// </summary>
        long Ping();
    }
}