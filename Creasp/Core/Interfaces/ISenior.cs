﻿using Creasp.Elo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Creasp.Core
{
    public interface ISenior
    {
        /// <summary>
        /// Busca funcionarios no sistema da Senior RH
        /// </summary>

        IEnumerable<FuncionarioSenior> BuscaFuncionarios(int? matricula, string cpf, string nome);

        /// <summary>
        /// Retorna um array com todos os CPFs que são subordinados a um funcionário
        /// </summary>
        IEnumerable<string> BuscaSubordinados(string cpf);
        
        /// <summary>
        /// Busca o orçamento da SENIOR
        /// </summary>
        IEnumerable<OrcamentoSenior> BuscaOrcamento(int nrAno);

        /// <summary>
        /// Avalia a disponibilidade do servidor da SENIOR
        /// </summary>
        long Ping();

        
    }
}