﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;

namespace Creasp.Core
{
    public interface IAuditoria
    {
        DateTime DtCadastro { get; set; }
        DateTime? DtAlteracao { get; set; }
        string LoginCadastro { get; set; }
        string LoginAlteracao { get; set; }
    }
}