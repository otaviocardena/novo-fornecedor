﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using Creasp.Creanet;

namespace Creasp.Core
{
    public interface ICreanet
    {
        /// <summary>
        /// Busca os profissionais no CREANET conforme creasp, cpf ou nome (like)
        /// </summary>
        IEnumerable<Profissional> BuscaProfissionais(string creasp, string cpf, string nome);

        /// <summary>
        /// Lista todas as entidades de classe do CREANET ativas, sem informar o IdEntidade (=0)
        /// </summary>
        IEnumerable<Entidade> ListaEntidades();

        /// <summary>
        /// Lista todas as instituições de ensino do CREANET ativas com representação no plenario, sem informar o IdEntidade (=0)
        /// </summary>
        IEnumerable<Entidade> ListaIES();

        /// <summary>
        /// Retorna todos os convenios de um mês/ano das entidades
        /// </summary>
        IEnumerable<ValorConvenio> BuscaValorConvenios(int ano, int mes);

        /// <summary>
        /// Retorna todos os contratos de cessão de uso de um mes/ano
        /// </summary>
        IEnumerable<ContratoEntidade> BuscaContratosEntidade(int ano, int mes);

        /// <summary>
        /// Retorna a lista de todas as unidades ativas no CREA
        /// </summary>
        IEnumerable<Unidade> ListaUnidades();

        /// <summary>
        /// Indica a situação de pagamento de uma anuidade
        /// </summary>
        string ConsultaAnuidade(string creasp);

        /// <summary>
        /// Testa a conectividade com serviços externos
        /// </summary>
        long Ping();
    }

    public class AnuidadeSituacao
    {
        public const string EmDia = "EM_DIA";
        public const string Quite = "QUITE";
        public const string EmDebito = "EM_DEBITO";

        public static string NoSituacao(string cdSituacao)
        {
            return cdSituacao == EmDia ? "Em dia" :
                cdSituacao == Quite ? "Quite" : "Em débito";
        }
    }
}