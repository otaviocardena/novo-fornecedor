﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;

namespace Creasp.Core
{
    public interface IEmailDataSource
    {
        /// <summary>
        /// Retorna a lista dos destinaratios do email a ser enviado
        /// </summary>
        IEnumerable<Pessoa> GetDestinatarios();

        /// <summary>
        /// Retorna a lista dos destinararios junto com a fonte de dados de cada um
        /// </summary>
        object GetDados(int idPessoa);

        /// <summary>
        /// Retorna o codigo do template, da tabela de EmailTemplate
        /// </summary>
        string CdTemplate { get; }

        /// <summary>
        /// Lista os campos que a fonte de dados oferece
        /// </summary>
        string[] ListaCampos { get; }
    }
}