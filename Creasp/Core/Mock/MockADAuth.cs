﻿using NBox.Core;
using NBox.Services;
using NPoco;
using System;
using System.Linq;

namespace Creasp.Core
{
    public class MockADAuth : IAuth
    {
        public IUser Authentication(string login, string password)
        {
            var db = Factory.Get<IDatabase>();
            var pessoa = db.FirstOrDefault<Pessoa>(x => x.NoLogin == login);

            return new CreaspUser(login, "Usuário " + login, login + "@numeria.com.br", pessoa?.IdPessoa ?? 0, new string[] { "*" }, new string[] { "*" }, login == "fornecedor");
        }

        public bool Authorization(IUser user, string resource)
        {
            return true;
        }
    }
}