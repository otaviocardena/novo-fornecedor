﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using Creasp.Creanet;
using NBox.Core;
using NBox.Services;
using NPoco;

namespace Creasp.Core
{
    public class MockCreanet : ICreanet
    {
        public IEnumerable<Profissional> BuscaProfissionais(string creasp, string cpf, string nome)
        {
            if (string.IsNullOrEmpty(creasp) && string.IsNullOrEmpty(cpf) && string.IsNullOrEmpty(nome)) throw new ArgumentNullException("creasp,cpf,nome");

            var dados = MockUtils.ReadJson<Profissional[]>("Creanet.Profissional");

            if (!string.IsNullOrEmpty(creasp)) return dados.Where(x => x.NrCreasp == creasp);
            if (!string.IsNullOrEmpty(cpf)) return dados.Where(x => x.NrCpf == cpf);
            return dados.Where(x => x.NoProfissional.StartsWith(nome ?? ""));
        }

        public string ConsultaAnuidade(string creasp)
        {
            return MockUtils.Number(1, 100) <= 80 ? AnuidadeSituacao.EmDia : AnuidadeSituacao.EmDebito;
        }

        public IEnumerable<Entidade> ListaEntidades()
        {
            return MockUtils.ReadJson<Entidade[]>("Creanet.Entidade")
                .Where(x => x.TpEntidade == TpEntidade.EntidadeClasse);
        }

        public IEnumerable<Entidade> ListaIES()
        {
            return MockUtils.ReadJson<Entidade[]>("Creanet.Entidade")
                .Where(x => x.TpEntidade == TpEntidade.InstituicaoEnsino);
        }

        public IEnumerable<Unidade> ListaUnidades()
        {
            return MockUtils.ReadJson<Unidade[]>("Creanet.Unidade");
        }

        public long Ping()
        {
            return 100;
        }

        public IEnumerable<ValorConvenio> BuscaValorConvenios(int ano, int mes)
        {
            yield break;
        }

        public IEnumerable<ContratoEntidade> BuscaContratosEntidade(int ano, int mes)
        {
            yield break;
        }
    }
}