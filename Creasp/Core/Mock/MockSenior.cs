﻿using NBox.Core;
using NBox.Services;
using System;
using System.Linq;
using System.Collections.Generic;
using Creasp.Elo;

namespace Creasp.Core
{
    public class MockSenior : ISenior
    {
        public IEnumerable<FuncionarioSenior> BuscaFuncionarios(int? matricula, string cpf, string nome)
        {
            if (matricula == -1 && string.IsNullOrEmpty(cpf) && string.IsNullOrEmpty(nome)) throw new ArgumentNullException("matricula, cpf,nome");

            var dados = MockUtils.ReadJson<FuncionarioSenior[]>("Senior.Funcionario");

            if (matricula != null) return dados.Where(x => x.NrMatricula == matricula);
            if (!string.IsNullOrEmpty(cpf)) return dados.Where(x => x.NrCpf == cpf);
            return dados.Where(x => x.NoFuncionario.StartsWith(nome ?? ""));
        }

        public IEnumerable<string> BuscaSubordinados(string cpf)
        {
            yield break;
        }

        public long Ping()
        {
            throw new NotImplementedException("Não implementado");
        }

        public IEnumerable<OrcamentoSenior> BuscaOrcamento(int nrAno)
        {
            yield break;
        }
    }
}