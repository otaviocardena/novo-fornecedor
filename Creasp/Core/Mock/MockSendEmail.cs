﻿using NBox.Core;
using NBox.Services;
using System;
using System.Linq;
using System.Net.Mail;
using System.Diagnostics;

namespace Creasp.Core
{
    public class MockSendEmail : ISendEmail
    {
        public void SendEmail(MailMessage mail)
        {
            Debug.Print("Enviando email... " + mail.To.First().Address +
                " - " + mail.Subject + " - " + mail.Body);
        }
    }
}