﻿using NBox.Core;
using NBox.Services;
using System;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Web;
using System.IO;
using Creasp;
using Creasp.Elo;
using Creasp.Siscont.Despesa;
using Creasp.Siscont.Orcamento;

namespace Creasp.Core
{
    public class MockSiscont : ISiscont
    {
        public IEnumerable<Pessoa> BuscaPessoas(string cpf, string nome)
        {
            if(string.IsNullOrEmpty(cpf) && string.IsNullOrEmpty(nome)) throw new ArgumentNullException("cpf,nome");

            var dados = MockUtils.ReadJson<Pessoa[]>("Siscont.Pessoa");

            if (!string.IsNullOrEmpty(cpf)) return dados.Where(x => x.NoPessoa == "MAURICIO DAVID");            
            return dados.Where(x => x.NoPessoa == "MAURICIO DAVID");
            //return dados.Where(x => x.NrCpf == cpf);
            //return dados.Where(x => x.NoPessoa.StartsWith(nome ?? ""));
        }

        public IntegracaoEmpenhosEntity[] ConsultaEmpenho(int nrAno, Guid? nrSolicEmp = default(Guid?), string nrCpf = null, int? nrEmp = default(int?), string nrProcesso = null, bool? fgRestoAPagar = default(bool?), bool? fgProcessado = default(bool?))
        {
            if (nrSolicEmp == null)
            {
                DespesaClient _despesa = new DespesaClient();

                return _despesa.ConsultarEmpenhos(new IntegracaoEmpenhosFilterEntity
                {
                    Exercicio = nrAno,
                    RestoAPagar = fgRestoAPagar == null ? BooleanOption.All : fgRestoAPagar.Value ? BooleanOption.True : BooleanOption.False,
                    FavorecidoCPFCNPJ = Pessoa.FormataCpf(nrCpf),
                    IdSolicitacaoReservaOrcamentaria = nrSolicEmp,
                    Numero = nrEmp,
                    NumeroProcesso = nrProcesso
                    //TODO: Numero do processo está mapeado como int e não string NumeroProcesso = nrProcesso
                });
            }
            else
            {
                Random r = new Random();

                IntegracaoEmpenhosEntity[] emp = new IntegracaoEmpenhosEntity[] { new IntegracaoEmpenhosEntity { Exercicio = nrAno, Numero = r.Next(1, int.MaxValue) } };
                return emp;
            }




        }

        public PagamentosEntity[] ConsultaPagto(int nrAno, Guid? nrSolicPagto = default(Guid?), string nrCpfFmt = null, int? nrPagto = default(int?), int? nrEmp = default(int?), string noFavorecido = null)
        {
            Random r = new Random();

            var pagto = new Siscont.Despesa.PagamentosEntity[] { new PagamentosEntity {DataPagamento = DateTime.Now, NumeroPagamento = r.Next(1, int.MaxValue) } };

            return pagto;
        }

        public DisponibilidadeOrcamentariaEntity ConsultaSaldoOrcamentario(DateTime dtRef, string nrContaContabilFmt, string nrCentroCustoAnalitico)
        {
            var dispo = new DisponibilidadeOrcamentariaEntity();
            dispo.SaldoOrcamentarioDesbloqueado = decimal.MaxValue;
            return dispo;
        }

        public Adiantamento ConsultaAdiantamento(int nrAno, Guid nrSolicAdiantamento)
        {
            Random r = new Random();
            var adiantamento = new Adiantamento();

            adiantamento.NrAdiantamento = r.Next(1, int.MaxValue);
            adiantamento.NrAno = DateTime.Now.Year;
            adiantamento.DtAdiantamento = DateTime.Now;

            return adiantamento;
        }

        public void IncluirOuAlteraPessoa(Pessoa pessoa)
        {
        }

        public void IncluiSolicAdiantamento(SolicitacoesAdiantamentosEntity[] adiantamentos)
        {
            foreach (var p in adiantamentos)
            {
                p.IdSolicitacaoAdiantamento = Guid.NewGuid();
            }
        }

        public Guid IncluiSolicEmp(EmpenhoTipo tipo, string nrContaContabilFmt, DateTime dtEmpenho, decimal vrEmpenho, Pessoa pessoa, string historico, Dictionary<string, decimal> centroCustoValor)
        {
            return Guid.NewGuid();
        }

        public void IncluiSolicPagtos(Siscont.Despesa.PagamentosAutorizadosEntity[] pagtos)
        {
            foreach(var p in pagtos)
            {
                p.IdPagamentoAutorizado = Guid.NewGuid();
            }
        }

        public IEnumerable<CentroCusto> ListaCentroCusto(int nrAno)
        {
            return MockUtils.ReadJson<CentroCusto[]>("Siscont.CentroCusto");
        }

        public IEnumerable<ContaContabil> ListaPlanoContas(int nrAno)
        {
            yield break;
        }

        public IEnumerable<string> ListaTipoDocumentos()
        {
            yield return "BOLETO";
        }

        public IEnumerable<Tributo> ListaTributos()
        {
            yield return new Core.Tributo { CdTributo = "A", NoTributo = "Tributo A" };
        }

        public IEnumerable<DotacaoOrcamentaria> ListaDotacaoOrcamentaria(int nrAno)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<PosicaoAtualOrcamento> ListaPosicaoAtualOrcamento(List<string> contasContabeis, int nrAno)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ContratoOrcamentario> ListaContratos()
        {
            throw new NotImplementedException();
        }

        public long Ping()
        {
            return 99;
        }

        public IEnumerable<DisponibilidadeAtualOrcamento> ListaDisponibilidadeOrcamento(int nrAno)
        {
            throw new NotImplementedException();
        }
    }
}