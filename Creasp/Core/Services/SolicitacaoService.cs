﻿using Creasp.Core.DTO;
using Creasp.Core.Model;
using Creasp.Nerp;
using NBox.Core;
using NBox.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Creasp.Core.Services
{
    public partial class SolicitacaoService : DataService
    {
        protected readonly int nrAnoBase;
        protected ISiscont siscont = Factory.Get<ISiscont>();
        protected string baseUrl = ConfigurationManager.AppSettings["urlBase"] ?? "http://localhost:8080";

        public SolicitacaoService(ServiceContext context) : base(context)
        {
            nrAnoBase = (context as CreaspContext).NrAnoBase;
        }

        public bool IncluiTributosFinalizaSolicitacao(HttpPostedFile fileTributo, string aliquota, string baseCalc, string vencimento, string tipoTributoRetencao, int idSolicitacao)
        {
            var solicitacao = db.FirstOrDefault<Solicitacao>(s => s.IdSolicitacao == idSolicitacao);
            if (solicitacao == null) return false;

            DateTime? dtVencimento = null;
            var isParsed = DateTime.TryParse(vencimento, out var _);
            if (isParsed) dtVencimento = DateTime.Parse(vencimento);

            var doc = new DocumentoService(context);
            var idTributoFile = doc.EnviaDocumentoERetornaId(TpRefDoc.Solicitacao, solicitacao.IdSolicitacao, solicitacao.IdPessoa, null, fileTributo);

            solicitacao.DtAlteracao = DateTime.Now;
            solicitacao.Aliquota = aliquota;
            solicitacao.BaseCalc = baseCalc;
            solicitacao.Vencimento = dtVencimento;
            solicitacao.TributoOuRetencao = tipoTributoRetencao;
            solicitacao.IdTributoDoc = idTributoFile;
            solicitacao.Status = StatusSolicitacao.Aprovada;

            try
            {
                db.Update(solicitacao);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public int IncluiSolicitacao(string obs, string vlrBruto, bool checkAssinatura, HttpPostedFile fileNF, HttpPostedFile fileCert, HttpPostedFile fileRel, int idPessoa, string numNF)
        {
            //add validations

            var solicitacao = new Solicitacao();
            solicitacao.DtAlteracao = DateTime.Now;
            solicitacao.DtCadastro = DateTime.Now;
            solicitacao.Observacoes = obs;
            solicitacao.ValorBruto = vlrBruto;
            solicitacao.CheckAssinatura = checkAssinatura;
            solicitacao.IdPessoa = idPessoa;
            solicitacao.NumNf = numNF;

            var doc = new DocumentoService(context);
            var idNFDoc = doc.EnviaDocumentoERetornaId(TpRefDoc.Solicitacao, solicitacao.IdSolicitacao, solicitacao.IdPessoa, null, fileNF);
            var idCertDoc = doc.EnviaDocumentoERetornaId(TpRefDoc.Solicitacao, solicitacao.IdSolicitacao, solicitacao.IdPessoa, null, fileCert);
            var idRelDoc = doc.EnviaDocumentoERetornaId(TpRefDoc.Solicitacao, solicitacao.IdSolicitacao, solicitacao.IdPessoa, null, fileRel);

            solicitacao.IdCertidao = idCertDoc;
            solicitacao.IdNF = idNFDoc;
            solicitacao.IdRelatorio = idRelDoc;
            solicitacao.Status = StatusSolicitacao.Cadastrada;

            //Testar isso
            try
            {                
                var fornecedorPessoa = db.FirstOrDefault<FornecedorPessoa>(s => s.IdPessoa == idPessoa);
                var cnpj = db.SingleById<Fornecedor>(fornecedorPessoa?.IdFornecedor)?.Cnpj;

                var empenhos = siscont.ConsultaEmpenho(nrAnoBase,
                null,
                cnpj,
                null,
                null,
                null).ToList();

                var empenho = empenhos.FirstOrDefault();
                solicitacao.NrEmpenho = empenho.Numero.ToString();
                var gestor = db.FirstOrDefault<Pessoa>(p => p.NrCpf == empenho.GestorContratoCPF || p.NrCpfFmt == empenho.GestorContratoCPF);

                solicitacao.GestorEmail = gestor?.NoEmail;
                solicitacao.TecEmail = gestor?.NoEmail;
                solicitacao.GestorId = gestor.IdPessoa;
                solicitacao.TecId = gestor.IdPessoa;
                solicitacao.TecNome = gestor?.NoPessoa;
                solicitacao.GestorNome = gestor?.NoPessoa;
            }
            catch
            {
                solicitacao.GestorEmail = "";
                solicitacao.TecEmail = "";
                solicitacao.NrEmpenho = string.IsNullOrEmpty(solicitacao.NrEmpenho) ? "" : solicitacao.NrEmpenho;
                solicitacao.GestorId = null;
                solicitacao.TecId = null;
                solicitacao.TecNome = "";
                solicitacao.GestorNome = "";
            }
            
            db.Insert(solicitacao);
            return solicitacao.IdSolicitacao;
        }

        #region EMAILS
        public void EnviaEmailADM(int IdSolicitacao)
        {
            var solicitacao = db.SingleById<Solicitacao>(IdSolicitacao);
            var nomeFornecedor = "";
            var fornecedorPessoa = db.FirstOrDefault<FornecedorPessoa>(s => s.IdPessoa == solicitacao.IdPessoa);

            if (fornecedorPessoa != null)
            {
                var fornecedor = db.SingleById<Fornecedor>(fornecedorPessoa.IdFornecedor);
                nomeFornecedor = fornecedor?.NomeFantasia ?? "";
            }

            var vlrBruto = "R$ " + solicitacao.ValorBruto;
            var nome = db.SingleById<Pessoa>(solicitacao.IdPessoa)?.NoPessoa ?? "";

            var bodyMessageADM = @"   <table style=""margin: 0 auto; font - family:Arial"">
                                           <tr style = ""background:#054589; height: 56px; color:white; text-align:center"" >
    
                                                <td >
                                                    CREA - SP | Nova solicitação de pagamento de Fornecedor
                                                 </td >
     
                                             </tr >
     

                                             <tr >
     
                                                 <td style = ""text-align: center; vertical-align: middle !important;"" >
      
                                                      <img src = ""https://i.ibb.co/jgr21fV/pagamento.png"" />
       
                                                   </td >
       
                                               </tr >
       

                                               <tr >
       
                                                   <td style = ""text-align: center; vertical-align: middle !important;"" >
                                                        #NOMEFORNECEDOR#
                                            </td >
        
                                                </tr >
        

                                                <tr >
        
                                                    <td style = ""text-align: center; vertical-align: middle !important;"" >
                                                         Há um novo processo de solicitação de pagamento do #NOME#, no valor #VALOR# aguardando sua validação
                                            </td >
         
                                                 </tr >
         

                                                 <tr style = ""text-align: center"" >
          
                                                      <td >
                                                          Para acesso a ferramenta de aprovação <a href = ""#URL#"" target = ""_blank"" > utilize o link</ a >
                 
                                                             </td >
                 
                                                         </tr >
                 
                                                     </table > ";
            bodyMessageADM = bodyMessageADM.Replace("#NOME#", nome);
            bodyMessageADM = bodyMessageADM.Replace("#VALOR#", vlrBruto);
            bodyMessageADM = bodyMessageADM.Replace("#NOMEFORNECEDOR#", nomeFornecedor);
            bodyMessageADM = bodyMessageADM.Replace("#URL#", $"{baseUrl}/solicitacao/Solicitacao.AprovacaoADM.aspx?idSolicitacao={IdSolicitacao}");

            EmailService email = new EmailService(context);
            EmailNerpResponsavelDataSource template = new EmailNerpResponsavelDataSource(solicitacao.IdPessoa);

            if (!string.IsNullOrEmpty(solicitacao.GestorEmail)) email.EnviaEmail(template, solicitacao.IdPessoa, "CREASP - Nova solicitação de pagamento", bodyMessageADM, solicitacao.GestorEmail, temNerp: false);

            AlterarSolicitacaoStatus(StatusSolicitacao.AgAprovacaoADM, IdSolicitacao);
        }

        public void EnviaEmailTEC(int IdSolicitacao)
        {
            var solicitacao = db.SingleById<Solicitacao>(IdSolicitacao);
            var nomeFornecedor = "";
            var fornecedorPessoa = db.FirstOrDefault<FornecedorPessoa>(s => s.IdPessoa == solicitacao.IdPessoa);

            if (fornecedorPessoa != null)
            {
                var fornecedor = db.SingleById<Fornecedor>(fornecedorPessoa.IdFornecedor);
                nomeFornecedor = fornecedor?.NomeFantasia ?? "";
            }

            var vlrBruto = "R$ " + solicitacao.ValorBruto;
            var nome = db.SingleById<Pessoa>(solicitacao.IdPessoa)?.NoPessoa ?? "";

            var bodyMessageADM = @"   <table style=""margin: 0 auto; font - family:Arial"">
                                           <tr style = ""background:#054589; height: 56px; color:white; text-align:center"" >
    
                                                <td >
                                                    CREA - SP | Nova solicitação de pagamento de Fornecedor
                                                 </td >
     
                                             </tr >
     

                                             <tr >
     
                                                 <td style = ""text-align: center; vertical-align: middle !important;"" >
      
                                                      <img src = ""https://i.ibb.co/jgr21fV/pagamento.png"" />
       
                                                   </td >
       
                                               </tr >
       

                                               <tr >
       
                                                   <td style = ""text-align: center; vertical-align: middle !important;"" >
                                                        #NOMEFORNECEDOR#
                                            </td >
        
                                                </tr >
        

                                                <tr >
        
                                                    <td style = ""text-align: center; vertical-align: middle !important;"" >
                                                         Há um novo processo de solicitação de pagamento do #NOME#, aguardando sua aprovação técnica
                                            </td >
         
                                                 </tr >
         

                                                 <tr style = ""text-align: center"" >
          
                                                      <td >
                                                          Para acesso a ferramenta de aprovação <a href = ""#URL#"" target = ""_blank"" > utilize o link</ a >
                 
                                                             </td >
                 
                                                         </tr >
                 
                                                     </table > ";
            bodyMessageADM = bodyMessageADM.Replace("#NOME#", nome);
            bodyMessageADM = bodyMessageADM.Replace("#VALOR#", vlrBruto);
            bodyMessageADM = bodyMessageADM.Replace("#NOMEFORNECEDOR#", nomeFornecedor);
            bodyMessageADM = bodyMessageADM.Replace("#URL#", $"{baseUrl}/solicitacao/Solicitacao.AprovacaoTEC.aspx?idSolicitacao={IdSolicitacao}");

            EmailService email = new EmailService(context);
            EmailNerpResponsavelDataSource template = new EmailNerpResponsavelDataSource(solicitacao.IdPessoa);

            if (!string.IsNullOrEmpty(solicitacao.TecEmail))  email.EnviaEmail(template, solicitacao.IdPessoa, "CREASP - Nova solicitação de pagamento", bodyMessageADM, solicitacao.TecEmail, temNerp: false);            
        }

        public void EnviaEmailGESTOR(int IdSolicitacao)
        {
            //mapear email do gestor/fornecedor e admin e tecnico relacionados ha solicitação

            var solicitacao = db.SingleById<Solicitacao>(IdSolicitacao);
            var nomeFornecedor = "";
            var emailFornecedor = "";
            var fornecedorPessoa = db.FirstOrDefault<FornecedorPessoa>(s => s.IdPessoa == solicitacao.IdPessoa);

            if (fornecedorPessoa != null)
            {
                var fornecedor = db.SingleById<Fornecedor>(fornecedorPessoa.IdFornecedor);
                nomeFornecedor = fornecedor?.NomeFantasia ?? "";
                emailFornecedor = fornecedor?.Email ?? "";
            }

            var vlrBruto = "R$ " + solicitacao.ValorBruto;
            var nome = db.SingleById<Pessoa>(solicitacao.IdPessoa)?.NoPessoa ?? "";

            var bodyMessageADM = @"   <table style=""margin: 0 auto; font - family:Arial"">
                                           <tr style = ""background:#054589; height: 56px; color:white; text-align:center"" >
    
                                                <td >
                                                    CREA - SP | Nova solicitação de pagamento de Fornecedor
                                                 </td >
     
                                             </tr >
     

                                             <tr >
     
                                                 <td style = ""text-align: center; vertical-align: middle !important;"" >
      
                                                      <img src = ""https://i.ibb.co/jgr21fV/pagamento.png"" />
       
                                                   </td >
       
                                               </tr >
       

                                               <tr >
       
                                                   <td style = ""text-align: center; vertical-align: middle !important;"" >
                                                        #NOMEFORNECEDOR#
                                            </td >
        
                                                </tr >
        

                                                <tr >
        
                                                    <td style = ""text-align: center; vertical-align: middle !important;"" >
                                                         Há um novo processo de solicitação de pagamento do #NOME# aguardando aprovação 
                                                            Administrativa pelo Fiscal #ADMIN# e técnica pelo fiscal #TEC#
                                            </td >
         
                                                 </tr >
         

                                                 <tr style = ""text-align: center"" >
          
                                                      <td >
                                                          Para acompanhar o processo de aprovação acesse a plataforma de fornecedores.
                 
                                                             </td >
                 
                                                         </tr >
                 
                                                     </table > ";
            bodyMessageADM = bodyMessageADM.Replace("#NOME#", nome);
            bodyMessageADM = bodyMessageADM.Replace("#VALOR#", vlrBruto);
            bodyMessageADM = bodyMessageADM.Replace("#NOMEFORNECEDOR#", nomeFornecedor);
            bodyMessageADM = bodyMessageADM.Replace("#TEC#", solicitacao.TecNome);
            bodyMessageADM = bodyMessageADM.Replace("#ADMIN#", solicitacao.GestorNome);

            EmailService email = new EmailService(context);
            EmailNerpResponsavelDataSource template = new EmailNerpResponsavelDataSource(solicitacao.IdPessoa);

            email.EnviaEmail(template, solicitacao.IdPessoa, "CREASP - Nova solicitação de pagamento", bodyMessageADM, emailFornecedor, temNerp: false);
        }

        public void EnviaEmailRecusaGESTOR(int IdSolicitacao, string obsRecusa)
        {
            //mapear email do gestor/fornecedor e admin e tecnico relacionados ha solicitação

            var solicitacao = db.SingleById<Solicitacao>(IdSolicitacao);
            var nomeFornecedor = "";
            var emailFornecedor = "";
            var fornecedorPessoa = db.FirstOrDefault<FornecedorPessoa>(s => s.IdPessoa == solicitacao.IdPessoa);

            if (fornecedorPessoa != null)
            {
                var fornecedor = db.SingleById<Fornecedor>(fornecedorPessoa.IdFornecedor);
                nomeFornecedor = fornecedor?.NomeFantasia ?? "";
                emailFornecedor = fornecedor?.Email ?? "";
            }

            var bodyMessageADM = @"   <table style=""margin: 0 auto; font - family:Arial"">
                                           <tr style = ""background:#054589; height: 56px; color:white; text-align:center"" >
    
                                                <td >
                                                    CREA - SP | Nova solicitação de pagamento de Fornecedor
                                                 </td >
     
                                             </tr >
     

                                             <tr >
     
                                                 <td style = ""text-align: center; vertical-align: middle !important;"" >
      
                                                      <img src = ""https://i.ibb.co/K2yRgDc/Group-374.png"" />
       
                                                   </td >
       
                                               </tr >
       

                                               <tr >
       
                                                   <td style = ""text-align: center; vertical-align: middle !important;"" >
                                                        #TEC#
                                            </td >
        
                                                </tr >
        

                                                <tr >
        
                                                    <td style = ""text-align: center; vertical-align: middle !important;"" >
                                                         A Solicitação de pagamento do #NOMEFORNECEDOR# foi recusada, abaixo o motivo descrito pelo aprovador:
                                            </td >
         
                                                 </tr >
         

                                                 <tr style = ""text-align: center"" >
          
                                                      <td >
                                                          #OBSRECUSA#
                 
                                                             </td >
                 
                                                         </tr >
                 
                                                     </table > ";
            bodyMessageADM = bodyMessageADM.Replace("#TEC#", solicitacao.TecNome);
            bodyMessageADM = bodyMessageADM.Replace("#OBSRECUSA#", obsRecusa);
            bodyMessageADM = bodyMessageADM.Replace("#NOMEFORNECEDOR#", nomeFornecedor);

            EmailService email = new EmailService(context);
            EmailNerpResponsavelDataSource template = new EmailNerpResponsavelDataSource(solicitacao.IdPessoa);

            if (!string.IsNullOrEmpty(solicitacao.GestorEmail))  email.EnviaEmail(template, solicitacao.IdPessoa, "CREASP - Nova recusa de solicitação de pagamento", bodyMessageADM, solicitacao.GestorEmail, temNerp: false);
        }

        public void EnviaEmailRecusaFORNECEDOR(int IdSolicitacao, string obsRecusa)
        {
            //mapear email do gestor/fornecedor e admin e tecnico relacionados ha solicitação

            var solicitacao = db.SingleById<Solicitacao>(IdSolicitacao);
            var emailFornecedor = "";
            var fornecedorPessoa = db.FirstOrDefault<FornecedorPessoa>(s => s.IdPessoa == solicitacao.IdPessoa);

            if (fornecedorPessoa != null)
            {
                var fornecedor = db.SingleById<Fornecedor>(fornecedorPessoa.IdFornecedor);
                emailFornecedor = fornecedor?.Email ?? "";
            }

            var bodyMessageADM = @"   <table style=""margin: 0 auto; font - family:Arial"">
                                           <tr style = ""background:#054589; height: 56px; color:white; text-align:center"" >
    
                                                <td >
                                                    CREA - SP | Nova solicitação de pagamento de Fornecedor
                                                 </td >
     
                                             </tr >
     

                                             <tr >
     
                                                 <td style = ""text-align: center; vertical-align: middle !important;"" >
      
                                                      <img src = ""https://i.ibb.co/K2yRgDc/Group-374.png"" />
       
                                                   </td >
       
                                               </tr >
       

                                               <tr >
       
                                                   <td style = ""text-align: center; vertical-align: middle !important;"" >
                                                        #TEC#
                                            </td >
        
                                                </tr >
        

                                                <tr >
        
                                                    <td style = ""text-align: center; vertical-align: middle !important;"" >
                                                        Sua Solicitação de pagamento foi recusada, abaixo o motivo descrito pelo aprovador:
                                            </td >
         
                                                 </tr >
         

                                                 <tr style = ""text-align: center"" >
          
                                                      <td >
                                                          #OBSRECUSA#
                 
                                                             </td >
                 
                                                         </tr >

                                                    <tr style = ""text-align: center"" >
          
                                                      <td >
                                                          Para mais esclarecimentos entre em contato com seu Gestor de Contrato do CREA SP.
                 
                                                             </td >
                 
                                                         </tr >
                 
                                                     </table > ";
            bodyMessageADM = bodyMessageADM.Replace("#TEC#", solicitacao.TecNome);
            bodyMessageADM = bodyMessageADM.Replace("#OBSRECUSA#", obsRecusa);

            EmailService email = new EmailService(context);
            EmailNerpResponsavelDataSource template = new EmailNerpResponsavelDataSource(solicitacao.IdPessoa);

            email.EnviaEmail(template, solicitacao.IdPessoa, "CREASP - Nova recusa de solicitação de pagamento", bodyMessageADM, emailFornecedor, temNerp: false);
        }

        public void EnviaEmailMaisInformacao(Solicitacao solicitacao)
        {
            //mapear email do gestor/fornecedor e admin e tecnico relacionados ha solicitação

            var emailPessoa = db.FirstOrDefault<Pessoa>(s => s.IdPessoa == solicitacao.IdPessoa)?.NoEmail;
            var vlorSugerido = !string.IsNullOrEmpty(solicitacao.VlrSugerido) ? $" Sugerindo o valor de R${solicitacao.VlrSugerido}. " : "";
            var obsSugerido = !string.IsNullOrEmpty(solicitacao.ObsAprovador) ? $" A observação do aprovador é: {solicitacao.ObsAprovador}. " : "";

            var bodyMessageADM = @"   <table style=""margin: 0 auto; font - family:Arial"">
                                           <tr style = ""background:#054589; height: 56px; color:white; text-align:center"" >
    
                                                <td >
                                                    CREA - SP | Nova solicitação de pagamento de Fornecedor
                                                 </td >
     
                                             </tr >
     

                                             <tr >
     
                                                 <td style = ""text-align: center; vertical-align: middle !important;"" >
      
                                                      <img src = ""https://i.ibb.co/jgr21fV/pagamento.png"" />
       
                                                   </td >
       
                                               </tr >

                                                <tr >
        
                                                    <td style = ""text-align: center; vertical-align: middle !important;"" >
                                                         Foi pedido mais informações sobre a solicitação : #ID#. #VALORSUGERIDO# #OBSSUGERIDA#
                                            </td >
         
                                                 </tr >
         

                                                 <tr style = ""text-align: center"" >
          
                                                      <td >
                                                          Para acompanhar o processo de aprovação acesse a plataforma de fornecedores.
                 
                                                             </td >
                 
                                                         </tr >
                 
                                                     </table > ";
            bodyMessageADM = bodyMessageADM.Replace("#ID#", solicitacao.IdSolicitacao.ToString());
            bodyMessageADM = bodyMessageADM.Replace("#VALORSUGERIDO#", vlorSugerido);
            bodyMessageADM = bodyMessageADM.Replace("#OBSSUGERIDA#", obsSugerido);

            EmailService email = new EmailService(context);
            EmailNerpResponsavelDataSource template = new EmailNerpResponsavelDataSource(solicitacao.IdPessoa);

            email.EnviaEmail(template, solicitacao.IdPessoa, "CREASP - Mais informções sobre solicitação de pagamento", bodyMessageADM, emailPessoa, temNerp: false);
        }
        #endregion

        public void AprovaSolicitacaoADM(int IdSolicitacao)
        {
            EnviaEmailTEC(IdSolicitacao);
            AlterarSolicitacaoStatus(StatusSolicitacao.AgAprovacaoTEC, IdSolicitacao);
            var solicitacao = db.FirstOrDefault<Solicitacao>(s => s.IdSolicitacao == IdSolicitacao);

            solicitacao.AprovadoADM = true;
            db.Update(solicitacao);
        }

        public void AprovaSolicitacaoTEC(int IdSolicitacao)
        {
            AlterarSolicitacaoStatus(StatusSolicitacao.AgCalcImposto, IdSolicitacao);
            var solicitacao = db.FirstOrDefault<Solicitacao>(s => s.IdSolicitacao == IdSolicitacao);

            solicitacao.AprovadoTEC = true;
            db.Update(solicitacao);
        }

        public void RecusaSolicitacao(int IdSolicitacao, string obsRecusa)
        {
            AlterarSolicitacaoStatus(StatusSolicitacao.Recusada, IdSolicitacao);
            var solicitacao = db.FirstOrDefault<Solicitacao>(s => s.IdSolicitacao == IdSolicitacao);

            solicitacao.ObsRecusa = obsRecusa;
            db.Update(solicitacao);

            EnviaEmailRecusaGESTOR(IdSolicitacao, obsRecusa);
            EnviaEmailRecusaFORNECEDOR(IdSolicitacao, obsRecusa);
        }

        public bool AlterarSolicitacaoStatus(string status, int IdSolicitacao)
        {
            if (!StatusSolicitacao.validaStatusSolicitacao(status)) return false;
            var solicitacao = db.FirstOrDefault<Solicitacao>(s => s.IdSolicitacao == IdSolicitacao);

            solicitacao.Status = status;
            db.Update(solicitacao);
            return true;
        }

        public Solicitacao ObtemPorId(int idSolicitacao)
        {
            return db.FirstOrDefault<Solicitacao>(s => s.IdSolicitacao == idSolicitacao);
        }

        public List<SolicitacaoDTO> ObtemPorAprovador(int idAprovador)
        {
            var finalResult = new List<SolicitacaoDTO>();

            var result = db.Query<Solicitacao>()
                .Where(s => (s.TecId == idAprovador && s.Status == StatusSolicitacao.AgAprovacaoTEC) || 
                    (s.GestorId == idAprovador && s.Status == StatusSolicitacao.AgAprovacaoADM))
                .ToList();

            foreach (var solicitacao in result)
            {
                var solicitacaoMerge = new SolicitacaoDTO {
                    Status = ObtemDescricaoStatus(solicitacao.Status),
                    LoginCadastro = solicitacao.LoginAlteracao,
                    DtCadastro = solicitacao.DtCadastro,
                    ValorBruto = solicitacao.ValorBruto,
                    NumNf = solicitacao.NumNf,
                    IdSolicitacao = solicitacao.IdSolicitacao,
                    Etapa = solicitacao.Status == StatusSolicitacao.AgAprovacaoTEC ? "TEC" : solicitacao.Status == StatusSolicitacao.AgAprovacaoADM ? "ADM" : "ADM"
                };
                finalResult.Add(solicitacaoMerge);
            }

            return finalResult;
        }

        public List<Solicitacao> ObtemPorIdPessoa(int idPessoa, string numNF, string etapa, Periodo dtSolicitacao)
        {
            var idFornecedor = db.Query<FornecedorPessoa>()
                    .FirstOrDefault(u => u.IdPessoa == idPessoa).IdFornecedor;

            var fornecedor = db.Query<Fornecedor>()
                    .FirstOrDefault(u => u.IdFornecedor == idFornecedor);

            var idPessoas = db.Query<FornecedorPessoa>()                   
                    .Where(u => u.IdFornecedor == idFornecedor)
                    .ToList()
                    .Select(f => f.IdPessoa);

            var result = new List<Solicitacao>();

            foreach (var id in idPessoas)
            {
                var solicitacoes = db.Query<Solicitacao>().Where(s => s.IdPessoa == id).ToList();
                result.AddRange(solicitacoes);
            }

            foreach (var solicitacao in result)
            {
                result.FirstOrDefault(r => r.IdSolicitacao == solicitacao.IdSolicitacao).Status = ObtemDescricaoStatus(solicitacao.Status);
                result.FirstOrDefault(r => r.IdSolicitacao == solicitacao.IdSolicitacao).LoginCadastro = solicitacao.LoginCadastro + $" - {fornecedor.NomeFantasia}";
            }

            if (!string.IsNullOrEmpty(numNF))
            {
                result = result.Where(r => r.NumNf.Contains(numNF)).ToList();
            }

            if (!string.IsNullOrEmpty(etapa))
            {
                result = result.Where(r => r.Status.Contains(etapa)).ToList();
            }

            if (!dtSolicitacao.IsVazio)
            {
                result = result.Where(r => r.DtCadastro >= dtSolicitacao.Inicio && r.DtCadastro <= dtSolicitacao.Fim).ToList();
            }

            return result;
        }

        public void PedidoMaisInformacaoADM(int idSolicitacao, string vlrSugerido, string obsSugerida)
        {
            var solicitacao = db.FirstOrDefault<Solicitacao>(s => s.IdSolicitacao == idSolicitacao);

            solicitacao.VlrSugerido = vlrSugerido;
            solicitacao.ObsAprovador = obsSugerida;
            solicitacao.Status = StatusSolicitacao.AgMaisInfosADM;
            db.Update(solicitacao);

            EnviaEmailMaisInformacao(solicitacao);
        }

        public void PedidoMaisInformacaoTEC(int idSolicitacao, string vlrSugerido, string obsSugerida)
        {
            var solicitacao = db.FirstOrDefault<Solicitacao>(s => s.IdSolicitacao == idSolicitacao);

            solicitacao.VlrSugerido = vlrSugerido;
            solicitacao.ObsAprovador = obsSugerida;
            solicitacao.Status = StatusSolicitacao.AgMaisInfosTEC;
            db.Update(solicitacao);

            EnviaEmailMaisInformacao(solicitacao);
        }

        private string ObtemDescricaoStatus(string status)
        {
            if(!StatusSolicitacao.validaStatusSolicitacao(status)) return status;

            if (status == "C")
            {
                return "Cadastrada";
            }
            if (status == "ADM")
            {
                return "Aguaradando aprovação administrativa";
            }
            if (status == "TEC")
            {
                return "Aguaradando aprovação técnica";
            }
            if (status == "AI" || status == "TI")
            {
                return "Aguardando mais informações do requerente";
            }
            if (status == "A")
            {
                return "Aprovada";
            }
            if (status == "IMP")
            {
                return "Aguardando definição dos impostos";
            }
            if (status == "R")
            {
                return "Recusada";
            }

            return status;

        }
    }
}
