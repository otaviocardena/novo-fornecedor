﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using Creasp.Core;
using NBox.Core;
using NBox.Services;
using NPoco;

namespace Creasp.Core
{
    public class CamaraService : DataService
    {
        public CamaraService(ServiceContext context) : base(context) { }

        /// <summary>
        /// Lista apenas as camaras de uma entidae no periodo informado
        /// </summary>
        public List<Camara> BuscaPorEntidade(int idEntidade, Periodo periodo)
        {
            var e = db.SingleById<Entidade>(idEntidade);

            // 21/12/2016 - Se for entidade de classe, retorna todas as camaras
            if (e.TpEntidade == TpEntidade.EntidadeClasse)
            {
                return db.Query<Camara>()
                    .WherePeriodo(periodo)
                    .OrderBy(x => x.NoCamara)
                    .ToList();
            }

            var ids = db.Query<EntidadeCamaras>()
                .Where(x => x.IdEntidade == idEntidade)
                .ProjectTo(x => x.CdCamara);

            return db.Query<Camara>()
                .WherePeriodo(periodo)
                .Where(x => ids.Contains(x.CdCamara))
                .OrderBy(x => x.NoCamara)
                .ToList();
        }

        /// <summary>
        /// Lista todas as camaras conforme o periodo informado
        /// </summary>
        public IEnumerable<Camara> ListaCamaras(Periodo periodo)
        {
            return db.Query<Camara>()
                .WherePeriodo(periodo)
                .OrderBy(x => x.NoCamara)
                .ToEnumerable();
        }
    }
}