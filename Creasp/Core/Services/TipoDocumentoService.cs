﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using Creasp.Core;
using NBox.Core;
using NBox.Services;
using NPoco;
using NPoco.Linq;
using Nustache.Core;

namespace Creasp.Core
{
    public class TipoDocumentoService : DataService
    {
        public TipoDocumentoService(ServiceContext context) : base(context) { }

        /// <summary>
        /// Inclui um novo tipo de documento
        /// </summary>
        public void Inclui(string TpRef, string noTipoDoc, int nrOrdem, bool fgObrigatorio, bool fgAtivo)
        {
            var cdTipoDoc = db.ExecuteScalar<int?>("SELECT MAX(CdTipoDoc) + 1 FROM TipoDocumento") ?? 1;

            var td = new TipoDocumento
            {
                CdTipoDoc = cdTipoDoc,
                TpRef = TpRef,
                NoTipoDoc = noTipoDoc,
                NrOrdem = nrOrdem,
                FgObrigatorio = fgObrigatorio,
                FgAtivo = fgAtivo
            };

            db.Insert(td);
        }

        /// <summary>
        /// Altera os dados do tipo de documento
        /// </summary>
        public void Altera(int cdTipoDoc, string noTipoDoc, int nrOrdem, bool fgObrigatorio, bool fgAtivo)
        {
            var td = db.SingleById<TipoDocumento>(cdTipoDoc);

            td.NoTipoDoc = noTipoDoc;
            td.NrOrdem = nrOrdem;
            td.FgObrigatorio = fgObrigatorio;
            td.FgAtivo = fgAtivo;

            db.Update(td);
        }

        public void Exclui(int cdTipoDoc)
        {
            try
            {
                db.Delete<TipoDocumento>(cdTipoDoc);
            }
            catch (SqlException ex) when (ex.Number == 547)
            {
                throw new NBoxException("Não é possivel excluir este tipo de documento pois está já está sendo utilizado");
            }
        }
    }
}