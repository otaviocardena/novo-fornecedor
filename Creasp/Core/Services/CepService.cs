﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using NBox.Core;
using NBox.Services;
using NPoco;

namespace Creasp.Core
{
    public class CepService : DataService
    {
        public CepService(ServiceContext context) : base(context) { }

        /// <summary>
        /// Busca um cep incluindo dados da cidade. Retorna NULL caso nao exista
        /// </summary>
        public Cep BuscaCep(string nrCep, bool usaCache = true)
        {
            if (usaCache)
            {
                return cache.Get("cep-" + nrCep, () =>
                {
                    return db.Query<Cep>()
                        .FirstOrDefault(x => x.NrCep == nrCep);
                });
            }
            else
                return db.Query<Cep>()
                    .FirstOrDefault(x => x.NrCep == nrCep);
        }

        /// <summary>
        /// Inclui um CEP manualmente no sistema quando não existir
        /// </summary>
        public Cep IncluiCepManual(string nrCep, string noLogradouro, string noBairro, string noCidade, string cdUf, int? cdCidadeManual = null)
        {

            int cdCidade;

            // procura pelo codigo da cidade usando o mesmo nome
            var cidade = db.Query<Cep>()
                            .Where(x => x.NoCidade == noCidade).FirstOrDefault();

            if (cidade == null)
            {
                cdCidade = db.ExecuteScalar<int>("SELECT MAX(CdCidade) + 1 FROM Cep");
            }
            else
            {
                cdCidade = cidade.CdCidade;
            }

            var cep = new Cep
            {
                NrCep = nrCep,
                NoLogradouro = noLogradouro?.MaxLength(100) ?? "IMPORTAÇÃO",
                NoBairro = noBairro,
                CdCidade = cdCidade, 
                NoCidade = noCidade?.MaxLength(100) ?? "IMPORTAÇÃO",
                CdUf = cdUf?.MaxLength(2) ?? "SP"
            };

            if (db.Query<Cep>().Any(c => c.NrCep == nrCep))
                throw new NBoxException("Esse cep já existe no sistema.");

            db.Insert(cep);

            return cep;
        }

        /// <summary>
        /// Atualiza um CEP no sistema
        /// </summary>
        public void AtualizaCep(string nrCep, string noLogradouro, string noBairro, string noCidade, string cdUf, int? cdCidadeManual = null)
        {
            // procura pelo codigo da cidade usando o mesmo nome ou o código informado
            var cep = BuscaCep(nrCep, false);

            cep.NrCep = nrCep;
            cep.NoLogradouro = noLogradouro?.MaxLength(100) ?? "IMPORTAÇÃO";
            cep.NoBairro = noBairro;
            cep.CdCidade = cep?.CdCidade ?? 3550308; // São Paulo
            cep.NoCidade = noCidade?.MaxLength(100) ?? "IMPORTAÇÃO";
            cep.CdUf = cdUf?.MaxLength(2) ?? "SP";

            if (!db.Query<Cep>().Any(c => c.NrCep == nrCep))
                throw new NBoxException("Impossível editar CEP que não existe no sistema.");

            db.Update(cep);
        }

        /// <summary>
        /// Remove CEP de acordo com número informado
        /// </summary>
        public void RemoverCep(string nrCep)
        {
            db.Delete(BuscaCep(nrCep, false));
        }

        /// <summary>
        /// Lista CEP de acordo com filtros informados
        /// </summary>
        public QueryPage<Cep> ListaCeps(Pager p, string nrCep, string noCidade, string cdUf)
        {
            var sql = Sql.Builder.Append(
              @"SELECT *
                  FROM CEP");

            if (!string.IsNullOrEmpty(nrCep))
                sql.Where("NrCep = @0", nrCep);

            if (!string.IsNullOrEmpty(noCidade))
                sql.Where("NoCidade LIKE '%' + @0 + '%'", noCidade.Trim().ToLike());

            if (!string.IsNullOrEmpty(cdUf))
                sql.Where("CdUf = @0", cdUf);

            return db.Page<Cep>(sql, p.DefaultSort("NoCidade, NoBairro, NoLogradouro"));
        }

        /// <summary>
        /// Lista os estados brasileiros (sigla)
        /// </summary>
        public IEnumerable<string> ListaUFs()
        {
            return db.Fetch<string>(@"SELECT DISTINCT CdUf FROM Cep CdUf ORDER BY CdUf");
        }

        /// <summary>
        /// Retorna somente as cidades de um estado
        /// </summary>
        public IEnumerable<Cep> ListaCidades(string cdUf)
        {
            return db.Fetch<Cep>(@"SELECT DISTINCT CdCidade, NoCidade, CdUf FROM Cep WHERE CdUf = @0 ORDER BY NoCidade", cdUf);
        }

        /// <summary>
        /// Lista todas as rotas (ida e volta) entre uma cidade de origem a um estado inteiro (todas as cidades)
        /// </summary>
        public IEnumerable<Rota> ListaRotas(string cdUfOrigem, int cdCidadeDestino)
        {
            // busca todas as rotas que saem ou chegam no destino
            var dist = db.Query<DistanciaCidades>()
                .Where(x => x.CdCidadeOrigem == cdCidadeDestino || x.CdCidadeDestino == cdCidadeDestino)
                .ToList();

            return ListaCidades(cdUfOrigem)
                .Where(x => x.CdCidade != cdCidadeDestino && x.CdCidade != 0)
                .DistinctBy(x => x.CdCidade)
                .Select(x => new Rota
                {
                    CdCidadeOrigem = x.CdCidade,
                    CdCidadeDestino = cdCidadeDestino,
                    NoOrigem = x.NoCidade,
                    QtKmIda = dist.FirstOrDefault(c => c.CdCidadeOrigem == x.CdCidade && c.CdCidadeDestino == cdCidadeDestino)?.VrKm ?? 0,
                    QtKmVolta = dist.FirstOrDefault(c => c.CdCidadeOrigem == cdCidadeDestino && c.CdCidadeDestino == x.CdCidade)?.VrKm ?? 0
                })
                .ToList();
        }

        /// <summary>
        /// Atualiza rotas
        /// </summary>
        public void AtualizaRotas(List<Rota> rotas)
        {
            using (var trans = db.GetTransaction())
            {
                // limpa os registros
                db.DeleteMany<DistanciaCidades>()
                    .Where(x => x.CdCidadeOrigem == rotas.First().CdCidadeDestino || x.CdCidadeDestino == rotas.First().CdCidadeDestino)
                    .Execute();

                foreach (var rota in rotas)
                {
                    // inclui se existir dado
                    if (rota.QtKmIda > 0)
                    {
                        db.Insert(new DistanciaCidades { CdCidadeOrigem = rota.CdCidadeOrigem, CdCidadeDestino = rota.CdCidadeDestino, VrKm = rota.QtKmIda });
                    }
                    if (rota.QtKmVolta > 0)
                    {
                        db.Insert(new DistanciaCidades { CdCidadeOrigem = rota.CdCidadeDestino, CdCidadeDestino = rota.CdCidadeOrigem, VrKm = rota.QtKmVolta });
                    }
                }

                trans.Complete();
            }
        }

        /// <summary>
        /// Busca a distancia, em KM, entre duas cidades. Gera uma exception caso não exista
        /// </summary>
        public Rota CalculaRota(Cep cepOrigem, Cep cepDestino, bool idaVolta)
        {
            var rota = new Rota { NrCepOrigem = cepOrigem?.NrCep, NrCepDestino = cepDestino?.NrCep, FgIdaVolta = idaVolta };

            rota.NoOrigem = cepOrigem?.NoCidade ?? $"CEP não encontrado";
            rota.NoDestino = cepDestino?.NoCidade ?? $"CEP não encontrado";
            rota.CdCidadeOrigem = cepOrigem?.CdCidade ?? 0;
            rota.CdCidadeDestino = cepDestino?.CdCidade ?? 0;

            rota.CdUfOrigem = cepOrigem?.CdUf ?? "SP";
            rota.CdUfDestino = cepDestino?.CdUf ?? "SP";

            // caso não encontre o codigo da cidade retorna 0
            // se o codigo da cidade for zero, retorna zero (Codigo zero pode ser cadastro antigo de CEP importado)
            //   é preciso corrigir manulamente no banco. Poderia ter uma interface para ajuste?
            if (cepOrigem == null || cepDestino == null || cepOrigem.CdCidade == 0 || cepDestino.CdCidade == 0) return rota;

            // origem == destino retorna 0
            if (cepOrigem.CdCidade == cepDestino.CdCidade) return rota;

            var r = BuscaRotaDados(cepOrigem.CdCidade, cepDestino.CdCidade, idaVolta);

            rota.QtKmIda = r.QtKmIda;
            rota.QtKmVolta = r.QtKmVolta;

            return rota;
        }

        /// <summary>
        /// Busca uma rota no banco de dados com cache
        /// </summary>
        private Rota BuscaRotaDados(int cdCidadeOrigem, int cdCidadeDestino, bool idaVolta)
        {
            return cache.Get("rota-" + cdCidadeOrigem + cdCidadeDestino + idaVolta, () =>
            {
                var result = new Rota();

                // busca trecho de ida
                var ida = db.Query<DistanciaCidades>()
                    .Where(x => x.CdCidadeOrigem == cdCidadeOrigem && x.CdCidadeDestino == cdCidadeDestino)
                    .SingleOrDefault();

                result.QtKmIda = ida?.VrKm ?? 0;

                // se estiver precisando so da ida e tiver no banco esse trecho, ja retorna ele e poupa um select
                if (idaVolta == false && ida != null) return result;

                // busca trecho de volta
                var volta = db.Query<DistanciaCidades>()
                    .Where(x => x.CdCidadeOrigem == cdCidadeDestino && x.CdCidadeDestino == cdCidadeOrigem)
                    .SingleOrDefault();

                result.QtKmVolta = volta?.VrKm ?? 0;

                return result;
            });
        }

        /// <summary>
        /// Faz a importação de rotas pelo arquivo gerado no DER
        /// </summary>
        public void ImportaRota(Stream stream)
        {
            var nrlinha = 1;
            // CodOrigem ; Nome Origem ; CodDestino ; Nome Destino ; Km

            using (var reader = new StreamReader(stream, Encoding.Default))
            {
                var line = reader.ReadLine(); // header

                while ((line = reader.ReadLine()) != null && ++nrlinha > 0)
                {
                    var campos = line.Split(';');
                    if (line.StartsWith("#") || campos.Length != 5) continue;

                    var codOrigem = campos[0].To<int>();
                    var codDestino = campos[2].To<int>();
                    var km = campos[4].To<int>();

                    var dist = db.SingleOrDefault<DistanciaCidades>(x => x.CdCidadeOrigem == codOrigem && x.CdCidadeDestino == codDestino);

                    if (dist == null)
                    {
                        db.Insert(new DistanciaCidades { CdCidadeOrigem = codOrigem, CdCidadeDestino = codDestino, VrKm = km });
                    }
                    else
                    {
                        dist.VrKm = km;
                        db.Update(dist);
                    }
                }
            }
        }

        /// <summary>
        /// Importa um arquivo de CEPs em formato CSV
        /// </summary>
        public void ImportaDadosCep(Stream stream)
        {
            db.InsertBulk(LeArquivoCep(stream));
        }

        private IEnumerable<Cep> LeArquivoCep(Stream stream)
        {
            //CEP|Tipo_Logradouro|Logradouro|Complemento|Local|Bairro|Cidade|cod_cidade|UF|Estado|cod_estado
            //0         1           2            3         4     5      6       7       8    9       10     
            var nrlinha = 1;

            using (var reader = new StreamReader(stream, Encoding.Default))
            {
                var line = reader.ReadLine(); // header

                while ((line = reader.ReadLine()) != null && ++nrlinha > 0)
                {
                    var campos = line.Split('|');
                    if (campos.Length != 11) continue;

                    var cep = new Cep
                    {
                        NrCep = campos[0],
                        NoLogradouro = (campos[1] + " " + campos[2]).ToUpper().Trim().MaxLength(80, ""),
                        NoBairro = campos[5].ToUpper(),
                        CdCidade = campos[7].To<int>(),
                        NoCidade = campos[6].ToUpper().MaxLength(100, ""),
                        CdUf = campos[8]
                    };

                    yield return cep;
                }
            }
        }

        
    }
}