﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using NBox.Core;
using NBox.Services;
using NPoco;

namespace Creasp.Core
{
    public class GrupoAcessoService : DataService
    {
        public GrupoAcessoService(ServiceContext context) : base(context) { }

        /// <summary>
        /// Salva os dados de um grupo de acesso na tabela GrupoAcesso
        /// </summary>
        public void Salvar(string cdGrupo, IEnumerable<string> recursos)
        {
            using (var trans = db.GetTransaction())
            {
                db.DeleteMany<GrupoAcesso>().Where(x => x.CdGrupo == cdGrupo).Execute();

                foreach (var recurso in recursos)
                {
                    // valida se existe um subrecurso sem o recurso pai
                    var parts = recurso.Split('.');

                    if (parts.Length == 3)
                    {
                        var recursoPai = parts[0] + "." + parts[1];

                        if (recursos.FirstOrDefault(x => x == recursoPai) == null)
                        {
                            throw new NBoxException("O recurso [{0}] foi selecionado sem selecionar o recurso [{1}]",
                                recurso, recursoPai);
                        }
                    }

                    db.Insert(new GrupoAcesso { CdGrupo = cdGrupo, CdRecurso = recurso });
                }

                trans.Complete();
            }
        }
    }
}