﻿using Creasp.Elo;
using NBox.Core;
using NBox.Services;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;


namespace Creasp.Core
{
    public class ContaContabilService : DataService
    {
        protected readonly int nrAnoBase;

        public ContaContabilService(ServiceContext context) : base(context)
        {
            nrAnoBase = (context as CreaspContext).NrAnoBase;
        }
        
        /// <summary>
        /// Busca todas as contas contábeis passando um prefixo e um tipo de conta.
        /// </summary>
        public List<ContaContabil> ListaContasContabeis(int nrAno, string cdPrefixoContaContabil, int? cdTipoContaContabil = null)
        {
            var contasContabeis = db.Query<ContaContabil>()
                .Where(x => x.FgAtivo)
                .Where(x => x.NrAno == nrAno)
                .Where(!string.IsNullOrEmpty(cdPrefixoContaContabil), x => x.NrContaContabil.StartsWith(cdPrefixoContaContabil)) 
                .Where(cdTipoContaContabil != null, x => x.FgAnalitico == (cdTipoContaContabil == CdTipoConta.Analitico))
                .ToList();

            return contasContabeis;
        }

        /// <summary>
        /// Busca uma conta contábil pelo seu ID
        /// </summary>
        /// <returns></returns>
        public ContaContabil BuscaContaContabil(int idContaContabil)
        {
            return db.Query<ContaContabil>()
                .Where(x => x.IdContaContabil == idContaContabil)
                .SingleOrDefault();
        }

        /// <summary>
        /// Busca uma conta contábil pelo seu ID
        /// </summary>
        /// <returns></returns>
        public ContaContabil BuscaContaContabil(string nrContaContabil)
        {
            return db.Query<ContaContabil>()
                .Where(x => x.NrContaContabil == nrContaContabil)
                .Where(x => x.NrAno == nrAnoBase)
                .SingleOrDefault();
        }
        
        /// <summary>
        /// Busca todas as contas contábeis que iniciam pelo prefixo enviado por parâmetro
        /// </summary>
        /// <param name="cdPrefixoConta"></param>
        /// <returns></returns>
        public List<ContaContabil> BuscaContasContabeisPorPrefixo(string cdPrefixoConta, bool trazerAnaliticasTambem)
        {
            var dados = db.Query<ContaContabil>()
                    .Where(x => x.NrAno == nrAnoBase)
                    .Where(x => x.FgAtivo)
                    .Where(x => x.NrContaContabil.StartsWith(cdPrefixoConta))
                    .Where(trazerAnaliticasTambem == false, x => x.FgAnalitico == false)
                    //.Where(NrContaFiltro != null, x => x.NrContaContabil.StartsWith(NrContaFiltro))
                    .OrderBy(x => x.NrContaContabil)
                    .ToList();

            return dados;
        }
        
        /// <summary>
        /// Verifica se já existe a conta contábil pelo número
        /// </summary>
        public bool ExisteConta(string nrContaContabil)
        {
            return db.Query<ContaContabil>()
                .Where(x => x.NrAno == nrAnoBase)
                .Where(x => x.NrContaContabil == nrContaContabil)
                .Where(x => x.FgAtivo)
                .Any();
        }
    }
}
