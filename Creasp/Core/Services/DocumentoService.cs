﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp.Core;
using NBox.Core;
using NBox.Services;
using NPoco;
using NPoco.Linq;
using Creasp.Siplen;
using NBox.Web;

namespace Creasp.Core
{
    public class DocumentoService : DataService
    {
        public DocumentoService(ServiceContext context) : base(context) { }

        protected IStorage storage = Factory.Get<IStorage>();
        //protected AzureBlobStorage azureStorage = new AzureBlobStorage();

        /// <summary>
        /// Migra documento para o AzureBlobStorage
        /// </summary>
        public void MigrarDocumento(DocumentoMigracao doc)
        {
            //var documento = storage.Download(doc.FileId);

            //storage.Upload("", doc.FileId, documento);

            //doc.FgImportado = true;

            //db.Update(doc);

            throw new NotImplementedException();
        }

        /// <summary>
        /// Busca todos os documentos relacionados ao IdRef.
        /// </summary>
        public List<Documento> ListaDocumentos(string TpRef, int idRef)
        {
            return db.Query<Documento>()
                .Include(x => x.Tipo, JoinType.Left)
                .Where(x => x.TpRef == TpRef)
                .Where(x => x.IdRef == idRef)
                .OrderBy(x => x.Tipo.NrOrdem)
                .ToList();
        }

        /// <summary>
        /// Adiciona na list os tipos de documentos que faltam para o tipo informado (ordena os resultados pelo NrOrdem)
        /// </summary>
        public void AdicionaDocumentosFaltantes(List<Documento> docs, string TpRef)
        {
            var tipos = db.Query<TipoDocumento>()
                .Where(x => x.TpRef == TpRef)
                .Where(x => x.FgAtivo)
                .ToList();

            foreach (var tipo in tipos)
            {
                if (!docs.Exists(x => x.CdTipoDoc == tipo.CdTipoDoc))
                {
                    docs.Add(new Documento { CdTipoDoc = tipo.CdTipoDoc, Tipo = tipo });
                }
            }

            docs.Sort(new Comparison<Documento>((l, r) => l.Tipo.NrOrdem.CompareTo(r.Tipo.NrOrdem)));
        }

        /// <summary>
        /// Lista todos os documentos referentes a uma pessoa
        /// </summary>
        public IEnumerable<Documento> ListaDocumentosPessoa(int idPessoa)
        {
            return db.Query<Documento>()
                .Include(x => x.Tipo, JoinType.Left)
                .Where(x => x.IdPessoa == idPessoa)
                .OrderByDescending(x => x.DtCadastro)
                .ToList();
        }

        /// <summary>
        /// Envia um aquivo para o servidor
        /// </summary>
        public void EnviaDocumento(string TpRef, int idRef, int? idPessoa, int? cdTipoDoc, HttpPostedFile file)
        {           

            var id = storage.Upload("creasp" + DateTime.Now.ToString("-yyyyMM") + ":", file);

            var doc = new Documento
            {
                TpRef = TpRef,
                IdRef = idRef,
                IdPessoa = idPessoa,
                CdTipoDoc = cdTipoDoc,
                NoArquivo = file.FileName,
                DtCadastro = DateTime.Now,
                FileId = id
            };

            db.Insert(doc);
        }

        /// <summary>
        /// Envia um aquivo para o servidor e retornar id do registro
        /// </summary>
        public int EnviaDocumentoERetornaId(string TpRef, int idRef, int? idPessoa, int? cdTipoDoc, HttpPostedFile file)
        {

            var id = storage.Upload("creasp" + DateTime.Now.ToString("-yyyyMM"), file);

            var doc = new Documento
            {
                TpRef = TpRef,
                IdRef = idRef,
                IdPessoa = idPessoa,
                CdTipoDoc = cdTipoDoc,
                NoArquivo = file.FileName,
                DtCadastro = DateTime.Now,
                FileId = id
            };

            db.Insert(doc);

            return doc.IdDocumento;
        }

        /// <summary>
        /// Exclui o documento do banco e do repositorio de arquivos
        /// </summary>
        public void ExcluiDocumento(int idDoc)
        {
            ExcluiDocumento(db.SingleById<Documento>(idDoc));
        }

        /// <summary>
        /// Exclui todos os documentos vinculados a uma referencia
        /// </summary>
        public void ExcluiDocumento(string TpRef, int idRef)
        {
            var docs = db.Query<Documento>()
                .Where(x => x.TpRef == TpRef && x.IdRef == idRef)
                .ToList();

            docs.ForEach((d, i) => ExcluiDocumento(d));
        }

        private void ExcluiDocumento(Documento doc)
        {
            // só exclui o arquivo fisicamente se ninguem mais estiver usando o mesmo arquivo
            var count = db.Query<Documento>()
                .Where(x => x.FileId == doc.FileId)
                .Where(x => x.IdDocumento != doc.IdDocumento)
                .Count();

            if (count == 0)
            {
                storage.Delete(doc.FileId);
            }

            db.Delete<Documento>(doc.IdDocumento);
        }

        /// <summary>
        /// Efetua o download do arquivo
        /// </summary>
        public void Download(NPage page, int idDoc)
        {
            var doc = db.SingleById<Documento>(idDoc);

            if (doc == null) throw new NBoxException("Documento anexo não encontrado");

            Download(page, doc);
        }

        /// <summary>
        /// Efetua o download do arquivo
        /// </summary>
        public void Download(NPage page, Documento doc)
        {
            var filename = storage.Filename(doc.FileId)?.ToLower();

            if (filename == null) throw new NBoxException("O arquivo '{0}' não foi encontrado no sistema de armazenado (IdDoc:{1})", doc.NoArquivo, doc.IdDocumento);

            var inline = filename.EndsWith(".pdf") || filename.EndsWith(".jpg") || filename.EndsWith(".png") || filename.EndsWith(".gif");

            page.RunScript("download", "var j = window.open('{0}'); setTimeout(function() {{ if(j == null) showMessage('Você precisa permitir pop-ups para exibir o documento em uma nova aba.', 'n-message-error', null); }}, 200);", page.DownloadLink(doc.FileId, doc.NoArquivo, inline));

            //page.Download(doc.FileId, inline);
        }
    }
}