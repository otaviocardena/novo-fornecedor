﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NBox.Core;
using NBox.Services;
using Creasp.Elo;


namespace Creasp.Core
{
    /// <summary>
    /// Classe que sincroniza dados da base local com o CREANET.
    /// O sincronismo é feito da seguinte maneira: Busca todos os dados remotos (que são os mais recentes, atuais - desativados não devem vir) e todos os dados do banco local.
    /// Compara-se todos os remotos com os locais em busca de novos (remoto que não existe no local) ou diferentes (remoto que é diferente do local, mas tem mesmo "ID"). Inclui ou altera. Se o local estiver desativado, ativa.
    /// Dentre os demais locais, verifica se não consta na lista do remoto. Se não constar, deve ser desativado.
    /// </summary>
    public class SyncService : DataService
    {
        protected readonly int nrAnoBase;

        public SyncService(ServiceContext context) : base(context)
        {
            nrAnoBase = (context as CreaspContext).NrAnoBase;
        }

        protected ICreanet creanet = Factory.Get<ICreanet>();
        protected ISiscont siscont = Factory.Get<ISiscont>();
        protected ISenior senior = Factory.Get<ISenior>();

        /// <summary>
        /// Executa os sincronismos entre os dados remotos e o banco local
        /// </summary>
        public void Run(StringBuilder sb)
        {
            using (var trans = db.GetTransaction())
            {
                PlanoContas(sb);
                CentroCusto(sb);
                Unidade(sb);
                EntidadeClasse(sb);
                InstituicaoEnsino(sb);
                
                var audit = new AuditLog();
                audit.Log("Executou sincronismo de dados:\n" + sb.ToString());

                trans.Complete();
            }
        }

        /// <summary>
        /// Executa os sincronismos entre os dados remotos da Implanta e o banco local
        /// </summary>
        public List<string> RunOrcamentoSiscont(StringBuilder sb)
        {
            var lstContasNaoMapeadas = new List<string>();

            using (var trans = db.GetTransaction())
            {
                lstContasNaoMapeadas.AddRange(DotacaoOrcamentaria(sb));
                lstContasNaoMapeadas.AddRange(PosicaoAtualOrcamento(sb));
                lstContasNaoMapeadas.AddRange(DiponibilidadeAtualOrcamento(sb));

                //Só deve-se commitar a importação de dados e atualizar propostas e reformulações se não houver contas para mapear.
                if (lstContasNaoMapeadas.Count == 0)
                {
                    var orcamentoService = new OrcamentoService(context);
                    orcamentoService.AtualizaValoresNasPropostasEReformulacoes(nrAnoBase);

                    trans.Complete();

                    var audit = new AuditLog();
                    audit.Log("Executou sincronismo de dados de orçamento (SISCONT):\n" + sb.ToString());
                }
            }

            AdicionaContas6paraMapear(lstContasNaoMapeadas.Distinct().ToList());

            return lstContasNaoMapeadas;
        }

        /// <summary>
        /// Executa os sincronismos entre os dados remotos da Senior e o banco local
        /// </summary>
        public List<string> RunOrcamentoSenior(StringBuilder sb)
        {
            var lstContasNaoMapeadas = new List<string>();

            using (var trans = db.GetTransaction())
            {
                lstContasNaoMapeadas.AddRange(OrcamentoRH(sb));
                
                //Só deve-se commitar a importação de dados e atualizar propostas e reformulações se não houver contas para mapear.
                if (lstContasNaoMapeadas.Count == 0)
                {
                    var orcamentoService = new OrcamentoService(context);
                    orcamentoService.AtualizaVrOrcadoPropostasOrcamentarias(nrAnoBase);

                    trans.Complete();

                    var audit = new AuditLog();
                    audit.Log("Executou sincronismo de dados de orçamento (SENIOR):\n" + sb.ToString());
                }
            }

            AdicionaContas6paraMapear(lstContasNaoMapeadas.Distinct().ToList());
            
            return lstContasNaoMapeadas;
        }

        private void PlanoContas(StringBuilder sb)
        {
            var remote = siscont.ListaPlanoContas(nrAnoBase).ToList();
            var local = db.Query<ContaContabil>().Where(x => x.NrAno == nrAnoBase).ToList();
            var merge = Merge(sb, remote, local,
                (r, l) => r.IdContaContabil = l.IdContaContabil,
                (r, l) => r.NrContaContabil == l.NrContaContabil);

            foreach (var i in merge)
            {
                db.Save(i.IsNew, i.Item);
            }
        }

        private void CentroCusto(StringBuilder sb)
        {
            var remote = siscont.ListaCentroCusto(nrAnoBase).ToList();
            var local = db.Query<CentroCusto>().Where(x => x.NrAno == nrAnoBase).ToList();
            var merge = Merge(sb, remote, local, 
                (r, l) => r.IdCentroCusto = l.IdCentroCusto,
                (r, l) => r.NrCentroCusto == l.NrCentroCusto && r.IdSiscont == l.IdSiscont);

            foreach (var i in merge)
            {
                db.Save(i.IsNew, i.Item);
            }

            new CentroCustoResponsavelService(context).InsereCentrosCustosResponsaveis(local.Select(x => x.IdCentroCusto).ToArray());
        }

        private void Unidade(StringBuilder sb)
        {
            var remote = creanet.ListaUnidades().ToList();
            var local = db.Fetch<Unidade>();
            var merge = Merge(sb, remote, local, 
                (r, l) => r.CdUnidade = l.CdUnidade,
                (r, l) => r.CdUnidade == l.CdUnidade);

            foreach (var i in merge)
            {
                // valida se existe o CEP
                if (db.Query<Cep>().FirstOrDefault(x => x.NrCep == i.Item.NrCep) == null)
                {
                    throw new NBoxException("CEP " + i.Item.NrCep + " não encontrado");
                }

                // se houve mudança no centro de custo, mantem o antigo
                if (!i.IsNew)
                {
                    var novo = i.Item.NrCentroCusto;
                    var antigo = local.Single(x => x.CdUnidade == i.Item.CdUnidade).NrCentroCusto;

                    if (novo != antigo)
                    {
                        i.Item.NrCentroCustoAnterior = antigo;
                    }
                }

                db.Save(i.IsNew, i.Item);
            }
        }

        public void EntidadeClasse(StringBuilder sb)
        {
            var service = new EntidadeService(context);
            var remote = creanet.ListaEntidades().ToList();
            var local = db.Query<Entidade>()
                .IncludeMany(x => x.Camaras)
                .Where(x => x.TpEntidade == TpEntidade.EntidadeClasse)
                .ToList();

            var merge = Merge(sb, remote, local, 
                (r, l) => r.IdEntidade = l.IdEntidade,
                (r, l) => r.CdExterno == l.CdExterno && r.TpEntidade == l.TpEntidade);

            foreach (var i in merge)
            {
                // caso a unidade, pula
                //TODO: estas unidade devem faltar provavelmente no pagamento ART/Cessão de uso
                if (db.Query<Unidade>().FirstOrDefault(x => x.CdUnidade == i.Item.CdUnidade) == null)
                {
                    sb.AppendLine($"[ERRO]: Unidade {i.Item.CdUnidade} não encontrada: a entidade {i.Item.NoEntidade} não será gravada");
                    continue;
                }

                service.GravaEntidade(i.IsNew, i.Item);
            }
        }

        public void InstituicaoEnsino(StringBuilder sb)
        {
            var service = new EntidadeService(context);
            var remote = creanet.ListaIES().ToList();
            var local = db.Query<Entidade>()
                .IncludeMany(x => x.Camaras)
                .Where(x => x.TpEntidade == TpEntidade.InstituicaoEnsino)
                .ToList();

            var merge = Merge(sb, remote, local, 
                (r, l) => r.IdEntidade = l.IdEntidade,
                (r, l) => r.CdExterno == l.CdExterno && r.TpEntidade == l.TpEntidade);

            foreach (var i in merge)
            {
                service.GravaEntidade(i.IsNew, i.Item);
            }
        }

        private List<string> OrcamentoRH(StringBuilder sb)
        {
            var remote = senior.BuscaOrcamento(nrAnoBase).ToList();

            #region ValidaConta6para5
            var lstContas6semConta5 = new List<string>();

            var contaContabilService = new ContaContabilService(context);
            var contaMapeamentoService = new ContaContabilMapeamentoService(context);
            var lstContasMapeadas = contaMapeamentoService.BuscaContasContabeisMapeadas();

            //Preenche o número da conta contábil
            var contasContabeis = contaContabilService.ListaContasContabeis(nrAnoBase, CdPrefixoContaContabil.Executado);
            var mapeamentosContas = contaMapeamentoService.BuscaContasContabeisMapeadas();

            for (int i = 0; i < remote.Count; i++)
            {
                remote[i].NrContaContabil6 = contasContabeis.SingleOrDefault(x => x.CdResumido == remote[i].CdContaContabilResumido)?.NrContaContabil;
            }

            remote = remote.Where(x => !string.IsNullOrEmpty(x.NrContaContabil6)).ToList();

            //Inicia a validação de cada uma das contas (elas precisam ter uma conta 5 de referência)
            for (int i = 0; i < remote.Count; i++) //TODO: Não iterar toda esta lista, fazer parecido com o método "PosicaoAtualOrcamento" e iterar contas distintas.
            {
                string nrContaContabil5 = contaMapeamentoService.DePara6para5(lstContasMapeadas, remote[i].NrContaContabil6);

                if (!string.IsNullOrEmpty(nrContaContabil5))
                {
                    remote[i].NrContaContabil5 = nrContaContabil5; //Atualiza a conta contábil para uma conta 5.
                }
                else
                {
                    if (!lstContas6semConta5.Contains(remote[i].NrContaContabil6))
                    {
                        lstContas6semConta5.Add(remote[i].NrContaContabil6); //Adiciona na lista de contas contábeis 6 sem referente 5
                    }

                    remote[i].NrContaContabil5 = null; //"Invalida" uma conta contábil
                }
            }

            #endregion

            //Se houver contas para mapear, nem continua o sincronismo.
            if (lstContas6semConta5.Count > 0)
                return lstContas6semConta5;

            var local = db.Query<OrcamentoSenior>()
                .Where(x => x.NrAno == nrAnoBase)
                .Where(x => x.FgAtivo)
                .ToList();

            var merge = Merge(sb, remote, local,
                (r, l) => r.IdOrcamentoSenior = l.IdOrcamentoSenior,
                (r, l) => r.NrCentroCusto == l.NrCentroCusto
                && r.CdContaContabilResumido == l.CdContaContabilResumido
                //&& r.NrContaContabil == l.NrContaContabil
                && r.NrAno == l.NrAno);

            foreach (var i in merge)
            {
                db.Save(i.IsNew, i.Item);
            }
            
            return lstContas6semConta5;
        }

        private List<string> DotacaoOrcamentaria(StringBuilder sb)
        {
            var remote = siscont.ListaDotacaoOrcamentaria(nrAnoBase).ToList();

            #region ValidaConta6para5
            var lstContas6semConta5 = new List<string>();
            
            var contaMapeamentoService = new ContaContabilMapeamentoService(context);
            var lstContasMapeadas = contaMapeamentoService.BuscaContasContabeisMapeadas();

            //Inicia a validação de cada uma das contas (elas precisam ter uma conta 5 de referência)
            for (int i = 0; i < remote.Count; i++) //TODO: Não iterar toda esta lista, fazer parecido com o método "PosicaoAtualOrcamento" e iterar contas distintas.
            {
                string nrContaContabil5 = contaMapeamentoService.DePara6para5(lstContasMapeadas, remote[i].NrContaContabil6);

                if (!string.IsNullOrEmpty(nrContaContabil5))
                {
                    remote[i].NrContaContabil5 = nrContaContabil5; //Atualiza a conta contábil para uma conta 5.
                }
                else
                {
                    lstContas6semConta5.Add(remote[i].NrContaContabil6); //Adiciona na lista de contas contábeis 6 sem referente 5
                    remote[i].NrContaContabil5 = null; //"Invalida" uma conta contábil
                }
            }
            #endregion

            //Se houver contas para mapear, nem continua o sincronismo.
            if (lstContas6semConta5.Count > 0)
                return lstContas6semConta5;

            var orcamentoService = new OrcamentoService(context);
            var local = orcamentoService.ListaDotacoes(nrAnoBase, nrAnoBase, null);

            var merge = Merge(sb, remote, local,
                (r, l) => r.IdDotacaoOrcamentaria = l.IdDotacaoOrcamentaria,
                (r, l) => r.NrAno == l.NrAno
                && r.NrCentroCusto == l.NrCentroCusto
                && r.NrContaContabil5 == l.NrContaContabil5
                && r.FgReceita == l.FgReceita);

            foreach (var i in merge)
            {
                db.Save(i.IsNew, i.Item);
            }

            return lstContas6semConta5;
        }

        private List<string> PosicaoAtualOrcamento(StringBuilder sb)
        {
            #region ValidaConta6para5
            var lstContas6semConta5 = new List<string>();

            var contaContabilService = new ContaContabilService(context);
            var contaMapeamentoService = new ContaContabilMapeamentoService(context);
            var contaCentroService = new ContaContabilCentroCustoService(context);

            //Busca todas as contas com mapeamento 6 para 5.
            var lstContasMapeadas = contaMapeamentoService.BuscaContasContabeisMapeadas();

            //Busca apenas contas que estão relacionadas a centros de custos orçáveis para utilizar como parâmetro no WebService
            var contasContabeis = contaContabilService.ListaContasContabeis(nrAnoBase, CdPrefixoContaContabil.DespesaInicialExec, CdTipoConta.Analitico).Select(x => x.NrContaContabil).Distinct().ToList();

            //Transforma as contas contábeis de 5 para 6, para poder enviar como parâmetro ao WebService.
            for (int i = 0; i < contasContabeis.Count; i++)
            {
                contasContabeis[i] = contaMapeamentoService.DePara5para6(lstContasMapeadas, contasContabeis[i]);
            }

            var remote = siscont.ListaPosicaoAtualOrcamento(contasContabeis, nrAnoBase).ToList();

            var contasRemote = remote.Select(x => x.NrContaContabil6).Distinct().ToList(); //Busca apenas as contas contábeis diferentes para fazer a validação

            //Inicia a validação de cada uma das contas (elas precisam ter uma conta 5 de referência)
            for (int i = 0; i < contasRemote.Count; i++)
            {
                string nrContaContabil5 = contaMapeamentoService.DePara6para5(lstContasMapeadas, contasRemote[i]);

                if (!string.IsNullOrEmpty(nrContaContabil5))
                {
                    remote.Where(x => x.NrContaContabil6 == contasRemote[i]).ToList().ForEach(x => x.NrContaContabil5 = nrContaContabil5); //Atualiza a conta contábil para uma conta 5.
                }
                else
                {
                    lstContas6semConta5.Add(contasRemote[i]); //Adiciona na lista de contas contábeis 6 sem referente 5
                    remote.Where(x => x.NrContaContabil6 == contasRemote[i]).ToList().ForEach(x => x.NrContaContabil5 = null); //"Invalida" uma conta contábil
                }
            }

            #endregion

            //Se houver contas para mapear, nem continua o sincronismo.
            if (lstContas6semConta5.Count > 0)
                return lstContas6semConta5;

            var orcamentoService = new OrcamentoService(context);
            var local = orcamentoService.ListaPosicaoAtualOrcamento(nrAnoBase, nrAnoBase);

            var merge = Merge(sb, remote, local,
                (r, l) => r.IdPosicaoAtualOrcamento = l.IdPosicaoAtualOrcamento,
                (r, l) => r.NrAno == l.NrAno
                && r.NrCentroCusto == l.NrCentroCusto
                && r.NrContaContabil5 == l.NrContaContabil5
                && r.FgReceita == l.FgReceita);

            foreach (var i in merge)
            {
                db.Save(i.IsNew, i.Item);
            }

            return lstContas6semConta5;
        }

        private List<string> DiponibilidadeAtualOrcamento(StringBuilder sb)
        {
            var lstContas6semConta5 = new List<string>();

            var contaMapeamentoService = new ContaContabilMapeamentoService(context);

            //Busca todas as contas com mapeamento 6 para 5.
            var lstContasMapeadas = contaMapeamentoService.BuscaContasContabeisMapeadas();

            var remote = siscont.ListaDisponibilidadeOrcamento(nrAnoBase).ToList();

            var contasRemote = remote.Select(x => x.NrContaContabil6).Distinct().ToList(); //Busca apenas as contas contábeis diferentes para fazer a validação

            //Inicia a validação de cada uma das contas (elas precisam ter uma conta 5 de referência)
            for (int i = 0; i < contasRemote.Count; i++)
            {
                string nrContaContabil5 = contaMapeamentoService.DePara6para5(lstContasMapeadas, contasRemote[i]);

                if (!string.IsNullOrEmpty(nrContaContabil5))
                {
                    remote.Where(x => x.NrContaContabil6 == contasRemote[i]).ToList().ForEach(x => x.NrContaContabil5 = nrContaContabil5); //Atualiza a conta contábil para uma conta 5.
                }
                else
                {
                    lstContas6semConta5.Add(contasRemote[i]); //Adiciona na lista de contas contábeis 6 sem referente 5
                    remote.Where(x => x.NrContaContabil6 == contasRemote[i]).ToList().ForEach(x => x.NrContaContabil5 = null); //"Invalida" uma conta contábil
                }
            }

            //Se houver contas para mapear, nem continua o sincronismo.
            if (lstContas6semConta5.Count > 0)
                return lstContas6semConta5;

            var orcamentoService = new OrcamentoService(context);
            var local = orcamentoService.ListaDisponibilidadeAtualOrcamento(nrAnoBase, nrAnoBase);

            var merge = Merge(sb, remote, local,
                (r, l) => r.IdDisponibilidadeAtualOrcamento = l.IdDisponibilidadeAtualOrcamento,
                (r, l) => r.NrAno == l.NrAno
                && r.NrContaContabil5 == l.NrContaContabil5);

            foreach (var i in merge)
            {
                db.Save(i.IsNew, i.Item);
            }

            return lstContas6semConta5;
        }

        private void Contratos(StringBuilder sb)
        {
            var remote = siscont.ListaContratos().ToList();
            var local = db.Query<ContratoOrcamentario>().ToList();

            var merge = Merge(sb, remote, local,
                (r, l) => r.IdContratoOrcamentario = l.IdContratoOrcamentario,
                (r, l) => r.IdContratoOrcamentario == l.IdContratoOrcamentario);

            foreach (var i in merge)
            {
                db.Save(i.IsNew, i.Item);
            }
        }

        private void AdicionaContas6paraMapear(List<string> contas6)
        {
            var contaContabilService = new ContaContabilService(context);
            var contaMapeamentoService = new ContaContabilMapeamentoService(context);

            foreach (var nrConta6 in contas6)
            {
                var conta6existente = contaContabilService.BuscaContaContabil(nrConta6);
                if (conta6existente == null)
                {
                    throw new NBoxException("Você precisa sincronizar as contas contábeis antes de sincronizar o orçamento.");
                }

                if (!contaMapeamentoService.ExisteMapeamento(conta6existente.IdContaContabil))
                {
                    contaMapeamentoService.ValidaEIncluiMapeamento(conta6existente.IdContaContabil, null);
                }
            }
        }

        public List<string> ValidaEEnviaReformulacaoAtual(int idReformulacao)
        {
            var lstContas5semContas6 = new List<string>();

            var reformulacaoService = new ReformulacaoService(context);
            var reformulacao = reformulacaoService.BuscaReformulacao(idReformulacao);

            var metodologiaService = new MetodologiaReceitaService(context);
            var metodologiasDeReformulacao = metodologiaService.BuscaMetodologias(reformulacao.NrAno, idReformulacao);

            if (metodologiasDeReformulacao.Any(x => x.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.PlenarioOK))
            {
                throw new NBoxException("Todas metodologias de reformulação devem estar aprovadas pelo plenário.");
            }

            var propostasDeReformulacao = reformulacaoService.BuscaReformulacoesOrcamentarias(reformulacao.IdReformulacao);

            if (propostasDeReformulacao.Any(x => x.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.PlenarioOK))
            {
                throw new NBoxException("Todas propostas orçamentárias de reformulação devem estar aprovadas pelo plenário.");
            }

            propostasDeReformulacao.ForEach(x => x.ReformulacaoOrcamentariaItens = reformulacaoService.BuscaReformulacaoOrcamentariaItens(x.IdReformulacaoOrcamentaria));

            lstContas5semContas6.AddRange(metodologiaService.DeParaTodasContas5para6(metodologiasDeReformulacao));
            lstContas5semContas6.AddRange(reformulacaoService.DeParaTodasContas5para6(propostasDeReformulacao));

            if (lstContas5semContas6.Count == 0)
            {
                return EnviaReformulacaoSiscont(metodologiasDeReformulacao, propostasDeReformulacao);
            }
            else
            {
                lstContas5semContas6.ForEach(x => x = string.Format("A conta {0} não possui uma conta 6 referente.", x));
                return lstContas5semContas6;
            }
        }

        public List<string> ValidaEEnviaOrcamentoAtual()
        {
            var lstContas5semContas6 = new List<string>();

            var metodologiaService = new MetodologiaReceitaService(context);
            var metodologias = metodologiaService.BuscaMetodologias(nrAnoBase, null);

            if (metodologias.Count == 0)
            {
                throw new NBoxException("Nenhuma metodologia encontrada.");
            }

            if (metodologias.Any(x => x.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.PlenarioOK))
            {
                throw new NBoxException("Todas metodologias devem estar aprovadas pelo plenário.");
            }

            var propostaService = new PropostaOrcamentariaDespesaService(context);
            var propostas = propostaService.BuscaPropostasPorAno(nrAnoBase);

            if (propostas.Count == 0)
            {
                throw new NBoxException("Nenhuma proposta encontrada.");
            }

            if (propostas.Any(x => x.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.PlenarioOK))
            {
                throw new NBoxException("Todas propostas orçamentárias devem estar aprovadas pelo plenário.");
            }

            propostas = propostaService.BuscaPropostasPorAno(nrAnoBase, true);

            var propostaItensService = new PropostaOrcamentariaDespesaService(context);
            propostas.ForEach(x => x.PropostaOrcamentariaItens = propostaItensService.ListaPropostaOrcamentariaItensAgrupados(propostaItensService.ListaItensPorCentroCusto(x.CentroCusto.NrCentroCusto)));

            lstContas5semContas6.AddRange(metodologiaService.DeParaTodasContas5para6(metodologias));
            lstContas5semContas6.AddRange(propostaService.DeParaTodasContas5para6(propostas));

            if (lstContas5semContas6.Count == 0)
            {
                return EnviaOrcamentoSiscont(metodologias, propostas);
            }
            else
            {
                List<string> erros = new List<string>();
                lstContas5semContas6.ForEach(x => erros.Add(string.Format("A conta {0} não possui uma conta 6 referente.", x)));
                return erros;
            }
        }
        
        private List<string> EnviaOrcamentoSiscont(List<MetodologiaReceitaAno> metodologias, List<PropostaOrcamentariaDespesa> propostas)
        {
            propostas.ForEach(x => x.PropostaOrcamentariaItens = x.PropostaOrcamentariaItens
                            .Where(y => y.VrOrcado > 0)
                            .ToList());

            metodologias = metodologias
                            .Where(x => x.VrOrcado > 0)
                            .ToList();

            var siscontService = new SiscontService();
            return siscontService.EnviaDotacoesOrcamentarias(metodologias, propostas);
        }

        private List<string> EnviaReformulacaoSiscont(List<MetodologiaReceitaAno> metodologiasReformulacao, List<ReformulacaoOrcamentaria> propostasReformulacao)
        {
            propostasReformulacao.ForEach(x => x.ReformulacaoOrcamentariaItens = x.ReformulacaoOrcamentariaItens
                            .Where(y => y.VrReducao != y.VrSuplementacao)
                            .ToList());

            propostasReformulacao = propostasReformulacao.Where(x => x.ReformulacaoOrcamentariaItens.Count > 0).ToList();

            metodologiasReformulacao = metodologiasReformulacao
                            .Where(x => x.VrOrcado != x.VrAprovadoAnoAnterior)
                            .ToList();

            var siscontService = new SiscontService();
            return siscontService.EnviaReformulacao(metodologiasReformulacao, propostasReformulacao);
        }

        /// <summary>
        /// Função que faz o merge de 2 listas: uma local e outra remota. Retorna uma nova lista com elementos a adicionar/alterar
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="log">Log para dizer o que está sendo alterado</param>
        /// <param name="remote">Lista de itens remota</param>
        /// <param name="local">Lista de itens local</param>
        /// <param name="copyId">Função para copiar o ID do objeto remoto para o objeto local</param>
        /// <param name="compareID">Funcao para comparar o codigo compartilhado entre Local/Remoto</param>
        /// <returns></returns>
        private List<MergeItem<T>> Merge<T>(StringBuilder log, List<T> remote, List<T> local, Action<T, T> copyId, Func<T, T, bool> compareID)
            where T : IFgAtivo, IEqualityComparer<T>
        {
            var result = new List<MergeItem<T>>();

            // procura no local cada um dos itens do remote
            foreach (var r in remote)
            {
                var isNew = true;
                var add = true;
                T old = default(T);
                // procura no local para seta o mesmo ID
                foreach (var l in local)
                {
                    if (compareID(l, r)) // verifica se tem o mesmo codigo compartilhado
                    {
                        // se for, seta o ID do remoto com o ID do local
                        copyId(r, l);
                        r.FgAtivo = true;
                        isNew = false;
                        old = l; // captura o valor local (que será atualizado)
                        if (l.Equals(r, l)) add = false;
                        break;
                    }
                }

                // so adiciona se o remoto tiver alguma diferença com o local
                if (add)
                {
                    if (isNew) r.FgAtivo = true;
                    result.Add(new MergeItem<T> { Item = r, IsNew = isNew });

                    log.AppendLine(string.Format("{0} [{1}] - {2}{3}",
                        typeof(T).Name,
                        isNew ? "Inc" : "Alt",
                        isNew ? "" : old.ToString() + " => ",
                        r.ToString()));
                }
            }

            // procura por locais orfãos (para desativar)
            foreach (var l in local.Where(x => x.FgAtivo))
            {
                var found = false;

                foreach (var r in remote)
                {
                    if (compareID(l, r))
                    {
                        found = true;
                        break;
                    }
                }

                if (found == false)
                {
                    l.FgAtivo = false;
                    result.Add(new MergeItem<T> { Item = l, IsNew = false });

                    log.AppendLine(string.Format("{0} [Del] - {1}",
                        typeof(T).Name,
                        l.ToString()));
                }
            }

            return result;
        }

        class MergeItem<T>
        {
            public T Item;
            public bool IsNew;
        }
    }
}