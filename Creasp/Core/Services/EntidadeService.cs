﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using NBox.Core;
using NBox.Services;
using NPoco;

namespace Creasp.Core
{
    public class EntidadeService : DataService
    {
        public EntidadeService(ServiceContext context) : base(context) { }

        /// <summary>
        /// Lista as entidades ativas
        /// </summary>
        public IEnumerable<Entidade> ListaEntidades()
        {
            return db.Query<Entidade>()
                .Where(x => x.FgAtivo)
                .OrderBy(x => x.TpEntidade).ThenBy(x => x.CdRepasse).ThenBy(x => x.NoEntidade)
                .ToEnumerable();
        }

        /// <summary>
        /// Inclui ou altera uma entidade
        /// </summary>
        public void GravaEntidade(bool isNew, Entidade entidade)
        {
            using (var trans = db.GetTransaction())
            {
                db.Save(isNew, entidade);

                db.DeleteMany<EntidadeCamaras>()
                    .Where(x => x.IdEntidade == entidade.IdEntidade)
                    .Execute();

                entidade.Camaras?.ForEach((c, i) =>
                {
                    c.IdEntidade = entidade.IdEntidade;
                    db.Insert(c);
                });

                trans.Complete();
            }
        }
    }
}