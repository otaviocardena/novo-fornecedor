﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Creasp.Nerp;
using Creasp.Siplen;
using NBox.Core;
using NBox.Services;
using NPoco.Linq;
using System.IO;
using System.Text;
using Creasp.Core.Model;

namespace Creasp.Core
{
    public class PessoaService : DataService
    {
        protected ICreanet creanet = Factory.Get<ICreanet>();
        protected ISiscont siscont = Factory.Get<ISiscont>();
        protected ISenior senior = Factory.Get<ISenior>();

        public PessoaService(ServiceContext context) : base(context) { }

        /// <summary>
        /// Busca uma pessoa nos sistemas externos e interno, para evitar duplicidade. Deve ser enviado apenas 1 dos 3 parametros de identificação. Tenta buscar em todos os sistemas externos e interno.
        /// </summary>
        public Pessoa BuscaPessoaExterna(string cpf, string creasp, int? matricula)
        {
            // conforme o parametro passado, faz a busca no respectivo sistema e preenche os parametros
            FuncionarioSenior funcionario = null;
            Profissional profissional = null;
            Pessoa pessoa = null; // siscont

            if (cpf != null)
            {
                pessoa = siscont.BuscaPessoas(cpf, null).FirstOrDefault();

                if (cpf.Length == 11) // somente pessoa fisica
                {
                    profissional = creanet.BuscaProfissionais(null, cpf, null).FirstOrDefault();
                    funcionario = senior.BuscaFuncionarios(null, cpf, null).FirstOrDefault();
                }
            }
            else if (creasp != null)
            {
                profissional = creanet.BuscaProfissionais(creasp, null, null).FirstOrDefault();
                if (profissional == null) throw new NBoxException("Profissional não encontrado");
                cpf = profissional.NrCpf;
                pessoa = siscont.BuscaPessoas(cpf, null).FirstOrDefault();
                funcionario = senior.BuscaFuncionarios(null, cpf, null).FirstOrDefault();
            }
            else if (matricula != null)
            {
                funcionario = senior.BuscaFuncionarios(matricula, null, null).FirstOrDefault();
                if (funcionario == null) throw new NBoxException("Matricula não encontrada");
                cpf = funcionario.NrCpf;
                profissional = creanet.BuscaProfissionais(null, cpf, null).FirstOrDefault();
                pessoa = siscont.BuscaPessoas(cpf, null).FirstOrDefault();
            }

            // Depois verifica se existe no banco a pessoa pelo CPF
            var p = db.Query<Pessoa>()
                .Include(x => x.Cep, JoinType.Left)
                .FirstOrDefault(x => x.NrCpf == cpf);

            // Se não encontrar, cria um novo objeto (IdPessoa = 0)
            if (p == null) p = new Pessoa { NrCpf = cpf };

            // Se existir profissional, copia os dados de profissional
            if (profissional != null)
            {
                p.NoPessoa = profissional.NoProfissional;
                p.NrCreasp = profissional.NrCreasp;
                p.NrCpf = profissional.NrCpf;
                p.NoTitulo = profissional.NoTitulo;
                p.DtNasc = profissional.DtNasc;
                p.TpSexo = profissional.TpSexo;

                // Email e endereço só copia se for novo (dá preferencia pelo endereço de correspondencia)
                p.NoEmail = profissional.NoEmail;

                var end = profissional.Enderecos.FirstOrDefault(x => x.FgCorrespondencia)
                    ?? profissional.Enderecos.FirstOrDefault();

                p.NoEndCorresp = end != null && end.FgCorrespondencia ? end.NoEnderecoFmt : null;

                if (end != null && p.NrCep == null)
                {
                    p.NrCep = end.NrCep;
                    p.NoLogradouro = end.NoLogradouro;
                    p.NrEndereco = end.NrEndereco;
                    p.NoComplemento = end.NoComplemento;
                    p.Cep = db.FirstOrDefault<Cep>(x => x.NrCep == end.NrCep);

                    // se por um acaso não exista o CEP, cadastra na base
                    if (p.Cep == null)
                    {
                        var srv = new CepService(context);
                        p.Cep = srv.IncluiCepManual(end.NrCep, end.NoLogradouro, end.NoBairro, end.NoCidade, end.CdUf);
                    }
                }
            }

            // Se existir pessoa no siscont, copia os dados
            if (pessoa != null)
            {
                if (p.NoPessoa == null) p.NoPessoa = pessoa.NoPessoa;
                if (p.NrCpf == null) p.NrCpf = pessoa.NrCpf;
            }

            // Se existir funciona, copia dados de funcionario
            if (funcionario != null)
            {
                if (p.NoPessoa == null) p.NoPessoa = funcionario.NoFuncionario;
                if (p.NrCpf == null) p.NrCpf = funcionario.NrCpf;
                p.NrMatricula = funcionario.NrMatricula;
            }

            return p;
        }

        /// <summary>
        /// Busca a pessoa pelo CPF somente no nosso banco. Se não existir, busca em todos os sistemas e inclui ela no nosso sistema
        /// </summary>
        public Pessoa BuscaOuIncluiPessoa(string nrCpf)
        {
            if (string.IsNullOrEmpty(nrCpf)) return null;

            var p = db.Query<Pessoa>()
                .Where(x => x.NrCpf == nrCpf)
                .SingleOrDefault();

            if (p != null) return p;

            p = BuscaPessoaExterna(nrCpf, null, null);

            if (p.NoPessoa == null) throw new NBoxException("Não foi encontrado no SISCONT o CPF: " + nrCpf);

            // se pessoa não existir no banco, inclui
            if (p.IdPessoa == 0)
            {
                db.Insert(p);
            }

            return p;
        }

        /// <summary>
        /// Retorna uma pessoa do banco com possibilidade de buscar dados bancarios no Siscont
        /// </summary>
        public Pessoa BuscaPessoa(int idPessoa, bool incluirSiscont)
        {
            var p = db.Query<Pessoa>()
                .Include(x => x.Cep, JoinType.Left)
                .Single(x => x.IdPessoa == idPessoa);

            if (incluirSiscont)
            {
                var s = siscont.BuscaPessoas(p.NrCpf, null).FirstOrDefault();

                if (s != null && s.DadosBancarios != null)
                {
                    p.DadosBancarios = s.DadosBancarios.Clone() as DadosBancarios;
                }
            }

            return p;
        }

        /// <summary>
        /// Busca, nos sistemas externos, os possiveis nomes que uma pessoa tem
        /// </summary>
        public List<KeyValuePair<string, string>> ListaNomesDisponiveis(string cpf)
        {
            var s = siscont.BuscaPessoas(cpf, null).Select(x => new KeyValuePair<string, string>(x.NoPessoa, "SISCONT"));
            var p = creanet.BuscaProfissionais(null, cpf, null).Select(x => new KeyValuePair<string, string>(x.NoProfissional, "CREANET"));
            var f = senior.BuscaFuncionarios(null, cpf, null).Select(x => new KeyValuePair<string, string>(x.NoFuncionario, "SENIOR"));

            var lista = new List<KeyValuePair<string, string>>();

            lista.AddRange(s);
            lista.AddRange(p);
            lista.AddRange(f);

            return lista.DistinctBy(x => x.Key).ToList();

        }

        /// <summary>
        /// Busca o nome da pessoa utilizado no SISCONT. Faz validações para evitar envio em nome de pessoa errada (CPF duplicado no SISCONT). Gera erros
        /// </summary>
        public string BuscaNomeSiscont(Pessoa p)
        {
            var pessoas = siscont.BuscaPessoas(p.NrCpfFmt, null);

            if (pessoas.Count() == 0)
            {
                throw new NBoxException("Pessoa {0} ({1}) não cadastrada no SISCONT", p.NoPessoa, p.NrCpfFmt);
            }
            else if (pessoas.Count() == 1)
            {
                var pessoa = pessoas.First();

                // if (pessoa.DadosBancarios == null)
                // {
                //     throw new NBoxException("Pessoa {0} ({1}) sem dados bancários no SISCONT", p.NoPessoa, p.NrCpfFmt);
                // }

                return pessoas.First().NoPessoa;
            }
            else
            {
                // caso tenhas CPF duplicado no SISCONT, confere se o Nome é o mesmo que temos na base
                var pessoa = pessoas
                    .Where(x => x.NrCpfFmt == p.NrCpfFmt)
                    .Where(x => x.NoPessoa == p.NoPessoa)
                    .FirstOrDefault();

                if (pessoa == null)
                {
                    throw new NBoxException("CPF {0} duplicado no SISCONT", p.NrCpfFmt);
                }
                //else if(pessoa.DadosBancarios == null)
                //{
                //    throw new NBoxException("Pessoa {0} ({1}) sem dados bancários no SISCONT", p.NoPessoa, p.NrCpfFmt);
                //}

                return pessoa.NoPessoa;
            }
        }

        /// <summary>
        /// Utiliza o webservice para fazer consulta a anuidade
        /// </summary>
        public string ConsultaAnuidade(string creasp)
        {
            return creanet.ConsultaAnuidade(creasp);
        }

        /// <summary>
        /// Inclui/Altera uma pessoa. Sincroniza dados bancários com o SISCONT
        /// </summary>
        public void Salva(bool isNew, Pessoa pessoa, StringBuilder erros)
        {
            // Busca algumas informações no creanet, se necessário
            if (!string.IsNullOrEmpty(pessoa.NrCreasp) && pessoa.DtNasc == null)
            {
                var profissional = creanet.BuscaProfissionais(pessoa.NrCreasp, null, null).FirstOrDefault();
                if (profissional == null) throw new NBoxException("Profissional não encontrado");
                pessoa.DtNasc = profissional.DtNasc;
                pessoa.TpSexo = profissional.TpSexo;
                pessoa.NoTitulo = profissional.NoTitulo.MaxLength(250);
            }

            // procura por duplicidade matricula/creasp na hora de incluir (não tem indice unico como cpf)
            if (isNew)
            {
                if (pessoa.NrMatricula != null && db.FirstOrDefault<Pessoa>(x => x.NrMatricula == pessoa.NrMatricula) != null)
                    throw new NBoxException("Já existe algum com a matricula {0}", pessoa.NrMatricula);

                if (!string.IsNullOrEmpty(pessoa.NrCreasp) && db.FirstOrDefault<Pessoa>(x => x.NrCreasp == pessoa.NrCreasp) != null)
                    throw new NBoxException("Já existe algum com a matricula {0}", pessoa.NrMatricula);
            }

            db.Save(isNew, pessoa);

            // só salva no siscont se tiver dados da conta bancaria
            if (pessoa.DadosBancarios?.NrConta != null)
            {
                try
                {
                    siscont.IncluirOuAlteraPessoa(pessoa);
                }
                catch (Exception ex)
                {
                    erros.Append(ex.Message);
                }
            }
        }

        /// <summary>
        /// Atualiza os dados do cadastro vindos do CREANET (nome, telefone, email, titulos, endereço de correspondencia, ....). Retorna false se nenhuma atualização foi feita
        /// </summary>
        public bool AtualizaCadastro(Pessoa p)
        {
            StringBuilder sb = new StringBuilder();

            var c = creanet.BuscaProfissionais(p.NrCreasp, null, null).FirstOrDefault();

            if (c == null)
            {
                throw new NBoxException("Profissional {0} ({1}) não encontrado no CREANET", p.NoPessoa, p.NrCreasp);
            }

            var atualiza = false;

            if (p.NoPessoa != c.NoProfissional)
            {
                p.NoPessoa = c.NoProfissional;
                atualiza = true;
                sb.AppendLine($"Nome alterado de \"{ p.NoPessoa }\" para \"{ c.NoProfissional }\".");
            }

            if (p.TpSexo != c.TpSexo)
            {
                p.TpSexo = c.TpSexo;
                atualiza = true;
                sb.AppendLine($"Sexo alterado de \"{ p.TpSexo }\" para \"{ c.TpSexo }\".");
            }

            if (p.NoTitulo != c.NoTitulo)
            {
                p.NoTitulo = c.NoTitulo;
                atualiza = true;
                sb.AppendLine($"Título alterado de \"{ p.NoTitulo }\" para \"{ c.NoTitulo }\".");
            }

            // só atualiza email se não for funcionário.
            if (p.NoEmail != c.NoEmail && p.NrMatricula == null)
            {
                p.NoEmail = c.NoEmail;
                atualiza = true;
                sb.AppendLine($"E-mail alterado de \"{ p.NoEmail }\" para \"{ c.NoEmail }\".");
            }

            if (p.DtNasc != c.DtNasc)
            {
                p.DtNasc = c.DtNasc;
                atualiza = true;
                sb.AppendLine(string.Format("Data de nascimento alterada de {0} para {1}.",
                    p.DtNasc.HasValue ? p.DtNasc.Value.ToString("dd/MM/yyyy") : string.Empty,
                    c.DtNasc.ToString("dd/MM/yyyy")));
            }

            if (p.NoEndCorresp != c.Enderecos?.FirstOrDefault(x => x.FgCorrespondencia)?.NoEnderecoFmt)
            {
                string novoEndereco = c.Enderecos?.FirstOrDefault(x => x.FgCorrespondencia)?.NoEnderecoFmt;
                p.NoEndCorresp = novoEndereco;
                atualiza = true;
                sb.AppendLine($"Endereço alterado de \"{ p.NoEndCorresp }\" para \"{ novoEndereco }\".");
            }

            var telefones = string.Join(" / ", c.Enderecos?.OrderBy(x => x.FgCorrespondencia ? 1 : 2).Select(x => x.NrTelefone).ToArray());

            if (p.NrTelefone != telefones)
            {
                p.NrTelefone = telefones.MaxLength(100);
                atualiza = true;
                sb.AppendLine($"Telefone alterado de \"{ p.NrTelefone }\" para \"{ telefones }\".");
            }

            if (atualiza)
            {
                db.Update(p);

                var audit = new AuditLog();
                audit.Log("Atualizou dados da pessoa com CREASP [" + p.NrCreasp + "]:\n" + sb.ToString());
            }


            return atualiza;
        }

        /// <summary>
        /// Identifica o tipo de pessoa em uma determinada data (Conselheiro, Indicado, Inspetor, Funcionario ou Outros)
        /// </summary>
        public TpPessoa BuscaTipoPessoa(Pessoa pessoa, DateTime dt)
        {
            return cache.Get<TpPessoa>("tppessoa_" + pessoa.IdPessoa + dt.Ticks, () =>
            {
                // busca se a pessoa tem mandato de conselheiro/inspetor na data
                var mandato = db.Query<Mandato>()
                    .Where(x => x.IdPessoa == pessoa.IdPessoa)
                    .Where(x => x.TpMandato == TpMandato.Conselheiro || x.TpMandato == TpMandato.Inspetor)
                    .WherePeriodo(Periodo.UmDia(dt))
                    .FirstOrDefault();

                if (mandato?.TpMandato == TpMandato.Conselheiro)
                {
                    return TpPessoa.Conselheiro;
                }
                else if (mandato?.TpMandato == TpMandato.Inspetor)
                {
                    return TpPessoa.Inspetor;
                }
                else if (pessoa.NrMatricula != null)
                {
                    return TpPessoa.Funcionario;
                }
                else
                {
                    // verifica se ele não está indicado a conselheiro titular/suplente
                    var ind = db.Query<Indicacao>()
                        .Where(x => x.IdPessoaTit == pessoa.IdPessoa || x.IdPessoaSup == pessoa.IdPessoa)
                        .Where(x => x.AnoIni == dt.Year)
                        .FirstOrDefault();

                    if (ind != null) return TpPessoa.Conselheiro;
                }

                return TpPessoa.Outros;

            });
        }

        /// <summary>
        /// Vincula um funcionario do CREA a um registro de PESSOA. Faz buscas no RH Senior e Implanta pra validações de dados
        /// </summary>
        public void VinculaLogin(string nrCpf, int nrMatricula, string noPessoa, DateTime dtNasc)
        {
            var pessoa = BuscaPessoaExterna(nrCpf, null, null);

            // se IdPessoa for zero, não está cadastrada em nenhum lugar, gera erro
            if (pessoa.NoPessoa == null) throw new NBoxException("CPF não cadastrado no SISCONT/Senior");

            // testa a matricula
            if (pessoa.NrMatricula != null && pessoa.NrMatricula != nrMatricula) throw new NBoxException("O número de matricula não confere para o CPF informado");

            // não utilizada a data de nascimento para validação
            pessoa.DtNasc = dtNasc;

            // verifica se a pessoa já não tem vinculo com outra login. Se tiver, só avisa ao usuario para contactar do administrador
            if (pessoa.NoLogin != null && pessoa.NoLogin != user.Login)
            {
                throw new NBoxException("Seu perfil já está vinculado com outra pessoa (PessoaID: {0}, Login: {1}). Contacte o administrador do sistema", pessoa.IdPessoa, pessoa.NoLogin);
            }

            // vincula o login
            pessoa.NoLogin = user.Login;

            // atualiza (ou cria) a pessoa no sistema com o vinculo do seu login
            this.Salva(pessoa.IdPessoa == 0, pessoa, new StringBuilder());
        }

        /// <summary>
        /// Busca o credor padrão na emissão de NERPs com multiplos credores. Se não existir no nosso banco, busca nos sistemas externos
        /// </summary>
        public Pessoa BuscaCredorPadrao(string nrCpf, bool associacao)
        {
            var cpf = nrCpf ?? ConfigurationManager.AppSettings[associacao ? "empenho.associacao.cnpj" : "empenho.credor.cpf"];
            var pessoa = db.SingleOrDefault<Pessoa>(x => x.NrCpf == cpf.UnMask());

            if (pessoa == null)
            {
                var p = BuscaPessoaExterna(cpf.UnMask(), null, null);
                if (p.NoPessoa == null) throw new NBoxException("Não foi encontrado o CPF {0} no SISCONT", cpf);
                db.Save(p.IdPessoa == 0, p);
                return p;
            }

            return pessoa;
        }

        public void ImportaDadosFunc(Stream stream, StringBuilder erros)
        {
            var matriculas = LeArquivoFunc(stream).Distinct().ToList();
            InsereFuncionariosNovos(matriculas, erros);
        }

        private IEnumerable<int> LeArquivoFunc(Stream stream)
        {
            using (var reader = new StreamReader(stream, Encoding.Default))
            {
                var line = string.Empty;

                while ((line = reader.ReadLine()) != null)
                {
                    var m = Converter.To<int?>(line, null);

                    if (m == null) continue;

                    yield return m.Value;
                }
            }
        }

        private void InsereFuncionariosNovos(List<int> matriculas, StringBuilder erros)
        {
            foreach (var m in matriculas)
            {
                try
                {
                    var pessoa = BuscaPessoaExterna(null, null, m);

                    if (pessoa.IdPessoa == 0)
                    {
                        db.Insert(pessoa);
                    }
                    else
                    {
                        db.Update(pessoa);
                    }
                }
                catch (Exception ex)
                {
                    erros.AppendLine("Matricula: " + m + " - " + ex.Message + "<br/>");
                }
            }
        }

        #region Delegação de acesso

        /// <summary>
        /// Delega acesso ao sistema do seu usuário ou de um subordinado seu
        /// </summary>
        public void DelegarAcesso(int idPessoa, int idPessoaCedido, Periodo periodo)
        {
            var pessoa = db.SingleById<Pessoa>(idPessoa);

            // se a pessoa que está delegando o acesso para outra, verifica se ela tem permissão disto
            if (idPessoa != (user as CreaspUser).IdPessoa)
            {
                if ((user as CreaspUser).IdPessoa == 1 || (user as CreaspUser).IdPessoa == 6577 ||  (user as CreaspUser).IdPessoa == 5170)
                {
                    // permitir que o admin/janaina/andreia possam delegar qualquer acesso
                    // #2037
                }
                else
                {
                    var usuario = db.SingleById<Pessoa>((user as CreaspUser).IdPessoa);
                    var subordinados = senior.BuscaSubordinados(usuario.NrCpf);

                    if (subordinados.Contains(pessoa.NrCpf) == false)
                    {
                        throw new NBoxException("Seu usuário não tem permissão de delegar acesso para {0} pois ele não é seu subordinado", pessoa.NoPessoa);
                    }
                }
            }

            var s = new SubDelegacao
            {
                IdPessoa = idPessoa,
                IdPessoaCedido = idPessoaCedido,
                DtIni = periodo.Inicio,
                DtFim = periodo.Fim
            };

            db.Insert(s);
        }

        public void ExcluirDelegacao(int idPessoa, DateTime dtIni)
        {
            if (idPessoa != (user as CreaspUser).IdPessoa) throw new NBoxException("Não é permitido excluir delegação de acesso de um usuário diferente de você");

            db.DeleteMany<SubDelegacao>().Where(x => x.IdPessoa == idPessoa && x.DtIni == dtIni).Execute();
        }

        /// <summary>
        /// Retorna o proprio usuário (idPessoa) e todos os usuarios ao qual o usuario (idPessoa) assine em nome de
        /// </summary>
        public IEnumerable<int?> ListaDelegacoes(int idPessoa)
        {
            yield return idPessoa;

            var subdelegacoes = db.Query<SubDelegacao>()
                .Where(x => x.IdPessoaCedido == idPessoa)
                .WherePeriodo(Periodo.Hoje)
                .ProjectTo(x => x.IdPessoa);

            foreach (var s in subdelegacoes)
            {
                yield return s;
            }
        }

        #endregion

        public int? ObtemIdFornecedor(int? idPessoa)
        {
            if (idPessoa == null) return null;

            var fornecedor = db.Query<FornecedorPessoa>()
                                .FirstOrDefault(u => u.IdPessoa == idPessoa);
            return fornecedor?.IdFornecedor ?? null;
        }
    }
}