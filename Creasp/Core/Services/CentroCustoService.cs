﻿using System.Collections.Generic;
using System.Linq;
using NBox.Core;
using NBox.Services;
using Creasp.Elo;
using System;

namespace Creasp.Core
{
    public class CentroCustoService : DataService
    {
        protected readonly int nrAnoBase;

        public CentroCustoService(ServiceContext context) : base(context)
        {
            nrAnoBase = (context as CreaspContext).NrAnoBase;
        }

        /// <summary>
        ///Lista os centros de custo conforme o login do usuario
        /// </summary>
        public QueryPage<CentroCusto> ListaCentroCusto(Pager p, string tpCentroCusto = null)
        {
            return db.Query<CentroCusto>()
                 .Where(x => x.NrAno == nrAnoBase)
                 .Where(x => x.FgAtivo == true)
                 .Where(tpCentroCusto != null, x => x.TpCentroCusto == tpCentroCusto)
                 .OrderBy(x => x.NrCentroCusto)
                 .ToPage(p.DefaultSort<CentroCusto>(x => x.IdCentroCusto));
        }

        // <summary>
        // Lista os centros de custo conforme o login do usuario
        // </summary>
        public List<CentroCusto> ListaCentroCusto(string query, string tpCentroCusto = null)
        {
            return db.Query<CentroCusto>()
                .Where(x => x.NrAno == nrAnoBase)
                .Where(x => x.FgAtivo == true)
                .Where(x => x.NrCentroCusto.StartsWith(query.Replace(".", "")) || x.NoCentroCusto.StartsWith(query.ToLike()))
                .Where(tpCentroCusto != null, x => x.TpCentroCusto == tpCentroCusto)
                .OrderBy(x => x.NrCentroCusto)
                .ToEnumerable()
                .Take(10)
                .ToList();
        }

        /// <summary>
        /// Retorna o centro de custo conforme o ID informado (leva em consideração o ano)
        /// </summary>
        public CentroCusto BuscaCentroCusto(int idCentroCusto)
        {
            return db.Query<CentroCusto>()
                .Where(x => x.NrAno == nrAnoBase)
                .Where(x => x.FgAtivo == true)
                .Where(x => x.IdCentroCusto == idCentroCusto)
                .FirstOrDefault();
        }

        /// <summary>
        /// Retorna o centro de custo conforme o numero informado (leva em consideração o ano)
        /// </summary>
        public CentroCusto BuscaCentroCusto(string nrCentroCusto)
        {
            
            if (nrAnoBase < System.DateTime.Today.Year)
            {
                return db.Query<CentroCusto>()
                        .Where(x => x.NrAno == nrAnoBase)                        
                        .Where(x => x.NrCentroCusto == nrCentroCusto)
                        .FirstOrDefault();
            }
            else
            {
                return db.Query<CentroCusto>()
                        .Where(x => x.NrAno == nrAnoBase)
                        .Where(x => x.NrCentroCusto == nrCentroCusto)
                        .Where(x => x.FgAtivo == true)
                        .FirstOrDefault();
            }


        }

        /// <summary>
        /// Retorna todos as subareas dos centros de custo informados
        /// </summary>
        public IEnumerable<CentroCusto> ListaSubAreas(IEnumerable<string> nrCentrosCustoAnalitico, bool verificaFgAtivo = true)
        {
            foreach (var analitico in nrCentrosCustoAnalitico)
            {
                IEnumerable<CentroCusto> subareas;

                if (verificaFgAtivo)
                {
                    subareas = db.Query<CentroCusto>()
                                .Where(x => x.NrAno == nrAnoBase)
                                .Where(x => x.FgAtivo)
                                .Where(x => x.TpCentroCusto == TpCentroCusto.SubArea)
                                .Where(x => x.NrCentroCusto.StartsWith(analitico))
                                .OrderBy(x => x.NrCentroCusto)
                                .ToEnumerable();
                }
                else
                {
                    subareas = db.Query<CentroCusto>()
                                .Where(x => x.NrAno == nrAnoBase)
                                .Where(x => x.TpCentroCusto == TpCentroCusto.SubArea)
                                .Where(x => x.NrCentroCusto.StartsWith(analitico))
                                .OrderBy(x => x.NrCentroCusto)
                                .ToEnumerable();
                }


                foreach (var subarea in subareas)
                {
                    yield return subarea;
                }
            }
        }

        /// <summary>
        /// Retorna todos as subareas dos centros de custo informados
        /// </summary>
        public IEnumerable<CentroCusto> ListaSubAreas(IEnumerable<Guid> identificadorCentrosCustoAnalitico, bool verificaFgAtivo = true)
        {
            foreach (var analitico in identificadorCentrosCustoAnalitico)
            {
                IEnumerable<CentroCusto> subareas;

                if (verificaFgAtivo)
                {
                    subareas = db.Query<CentroCusto>()
                                .Where(x => x.NrAno == nrAnoBase)
                                .Where(x => x.FgAtivo)
                                .Where(x => x.TpCentroCusto == TpCentroCusto.SubArea)
                                .Where(x => x.IdSiscont == analitico)
                                .OrderBy(x => x.NrCentroCusto)
                                .ToEnumerable();
                }
                else
                {
                    subareas = db.Query<CentroCusto>()
                                .Where(x => x.NrAno == nrAnoBase)
                                .Where(x => x.TpCentroCusto == TpCentroCusto.SubArea)
                                .Where(x => x.IdSiscont == analitico)
                                .OrderBy(x => x.NrCentroCusto)
                                .ToEnumerable();
                }


                foreach (var subarea in subareas)
                {
                    yield return subarea;
                }
            }
        }

        /// <summary>
        /// Busca o centro de custo referente ao analito conforme a subarea
        /// </summary>
        public CentroCusto BuscaCentroCustoOrçado(int idCentroCusto)
        {
            return BuscaCentroCustoOrçado(db.SingleById<CentroCusto>(idCentroCusto));
        }

        /// <summary>
        /// Busca o centro de custo referente ao analito conforme a subarea
        /// </summary>
        public CentroCusto BuscaCentroCustoOrçado(string nrCentroCusto)
        {
            return BuscaCentroCustoOrçado(BuscaCentroCusto(nrCentroCusto));
        }

        /// <summary>
        /// Busca o centro de custo referente ao analítico, ou orçado, conforme a subarea
        /// </summary>
        public CentroCusto BuscaCentroCustoOrçado(CentroCusto subArea) 
        {
            return cache.Get("cca_" + subArea.NrCentroCusto + "_" + subArea.NrAno, () =>
            {
                // caso a subarea seja, na verdade, um centro de custo analico, retorna ele mesmo
                if (subArea.TpCentroCusto == TpCentroCusto.Analitico) return subArea;

                if (subArea.TpCentroCusto == TpCentroCusto.Sintetico) throw new NBoxException("Não é possivel determinar o centro de custo analítico deste centro de custo " + subArea.NrCentroCustoFmt);

                // busca o centro de custo removendo o ultimo .XX
                var subAreaSuperior = CentroCusto.Superior(subArea.NrCentroCusto);

                // classifica como centro de custo orçado
                var subAreaOrçado = subAreaSuperior;

                // verifica se subArea está marcada como orçável
                var centroCustoResponsaveisService = new CentroCustoResponsavelService(context);
                var centroCustoOrcaveis = centroCustoResponsaveisService.ListaCentrosCustosOrcaveis();
                if (centroCustoOrcaveis.Exists(x => x.CentroCusto.NrCentroCusto == subArea.NrCentroCusto))
                    subAreaOrçado = subArea.NrCentroCusto;

                // o centro de custo orçado deve estar ativo
                if (nrAnoBase < System.DateTime.Today.Year)
                {
                    return db.Query<CentroCusto>()
                            .Where(x => x.NrCentroCusto == subAreaOrçado.UnMask())
                            .Where(x => x.NrAno == subArea.NrAno)
                            .Where(x => x.TpCentroCusto == TpCentroCusto.SubArea)
                            .Single($"Centro de custo orçado {subAreaOrçado} ativo não encontrado");
                }
                else
                {
                    return db.Query<CentroCusto>()
                            .Where(x => x.NrCentroCusto == subAreaOrçado.UnMask())
                            .Where(x => x.NrAno == subArea.NrAno)
                            .Where(x => x.TpCentroCusto == TpCentroCusto.SubArea)
                            .Where(x => x.FgAtivo)
                            .Single($"Centro de custo orçado {subAreaOrçado} ativo não encontrado");
                }

            });
        }

        /// <summary>
        /// Busca o centro de custo referente ao analitico, ou orçado, conforme a subarea para os casos de RP, onde o centro de custo não precisa estar ativo.
        /// </summary>
        public CentroCusto BuscaCentroCustoOrçadoRP(CentroCusto subArea)
        {
            return cache.Get("cca_" + subArea.NrCentroCusto + "_" + subArea.NrAno, () =>
            {
                // caso a subarea seja, na verdade, um centro de custo analico, retorna ele mesmo
                if (subArea.TpCentroCusto == TpCentroCusto.Analitico) return subArea;

                if (subArea.TpCentroCusto == TpCentroCusto.Sintetico) throw new NBoxException("Não é possivel determinar o centro de custo analítico deste centro de custo " + subArea.NrCentroCustoFmt);

                // busca o centro de custo removendo o ultimo .XX
                var subAreaSuperior = CentroCusto.Superior(subArea.NrCentroCusto);

                // classifica como centro de custo orçado
                var subAreaOrçado = subAreaSuperior;

                // verifica se subArea não está marcada como orçável
                var centroCustoResponsaveisService = new CentroCustoResponsavelService(context);
                var centroCustoOrcaveis = centroCustoResponsaveisService.ListaCentrosCustosOrcaveis();
                if (centroCustoOrcaveis.Exists(x => x.CentroCusto.NrCentroCusto == subArea.NrCentroCusto))
                    subAreaOrçado = subArea.NrCentroCusto;

                // o centro de custo analitico deve estar ativo
                return db.Query<CentroCusto>()
                    .Where(x => x.NrCentroCusto == subAreaOrçado.UnMask())
                    .Where(x => x.NrAno == subArea.NrAno)
                    .Where(x => x.TpCentroCusto == TpCentroCusto.Analitico)
                    .Single($"Centro de custo analítico {subAreaOrçado} ativo não encontrado");
            });
        }

        /// <summary>
        /// Lista as contas contábeis de um centro de custo específico, passando também o prefixo das contas.
        /// </summary>
        public List<ContaContabil> ListaContasContabeis(int idCentroCusto, string cdPrefixoConta)
        {
            var contasContabeis = db.Query<ContaContabil>()
                .Include(x => x.ContaContabilCentroCusto)
                .Include(x => x.ContaContabilCentroCusto.CentroCusto)
                //.Where(x => x.NrAno == nrAnoBase)
                .Where(x => x.ContaContabilCentroCusto.CentroCusto.IdCentroCusto == idCentroCusto)
                .Where(x => x.NrContaContabil.StartsWith(cdPrefixoConta))
                .ToList();

            return contasContabeis;
        }

        /// <summary>
        /// Salva as contas contábeis relacionadas a um centro de custo. É necessário passar um único centro de custo, 
        /// a lista de contas contábeis selecionadas para um determinado filtro (prefixo)
        /// </summary>
        public void SalvaContas(int idCentroCusto, int[] idsContasContabeisSelecionadas, string cdPrefixoConta)
        {
            //TODO: remover parâmetro cdPrefixoConta e deixar fixo: CdPrefixoContaContabil.Despesa

            var idsContasContabeisJaExistentes = ListaContasContabeis(idCentroCusto, cdPrefixoConta).Select(x => x.IdContaContabil);

            var idsNovasContas = idsContasContabeisSelecionadas.Except(idsContasContabeisJaExistentes);

            foreach (var idConta in idsNovasContas)
            {
                var relacaoContaCentro = new ContaContabil_CentroCusto();

                relacaoContaCentro.IdCentroCusto = idCentroCusto;
                relacaoContaCentro.IdContaContabil = idConta;
                relacaoContaCentro.FgInseridoPorUsuario = true;


                //Verifica se existe proposta para este centro de custo.
                var proposta = db.Query<PropostaOrcamentariaDespesa>()
                    .Where(x => x.IdCentroCusto == idCentroCusto)
                    .Where(x => x.CdTpFormulario == null || x.CdTpFormulario == "")
                    .Where(x => x.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.PlenarioOK)
                    .SingleOrDefault();

                if (proposta != null)
                {
                    var propostaService = new PropostaOrcamentariaDespesaService(context);
                    if (!propostaService.VerificaSePropostaPossuiConta(proposta.IdPropostaOrcamentariaDespesa, idConta))
                    {
                        propostaService.ValidaEIncluiContaContabil(proposta.IdPropostaOrcamentariaDespesa, idConta, null);
                    }
                }

                //Verifica se existe reformulação para este centro de custo.
                var reformulacaoOrcamentaria = db.Query<ReformulacaoOrcamentaria>()
                    .Where(x => x.IdCentroCusto == idCentroCusto)
                    .Where(x => x.CdTpFormulario == null || x.CdTpFormulario == "")
                    .Where(x => x.CdStatusPropostaOrcamentaria != StPropostaOrcamentaria.PlenarioOK)
                    .OrderByDescending(x => x.IdReformulacao)
                    .FirstOrDefault();

                if (reformulacaoOrcamentaria != null)
                {
                    var reformulacaoService = new ReformulacaoService(context);
                    if (!reformulacaoService.VerificaSeReformulacaoOrcamentariaPossuiConta(reformulacaoOrcamentaria.IdReformulacaoOrcamentaria, idConta))
                    {
                        reformulacaoService.ValidaEIncluiContaContabil(reformulacaoOrcamentaria.IdReformulacaoOrcamentaria, idConta, null);
                    }
                }

                db.Insert(relacaoContaCentro);
            }

            var idsContasExcluidas = idsContasContabeisJaExistentes.Except(idsContasContabeisSelecionadas);
            if (idsContasExcluidas?.Count() > 0)
            {
                db.DeleteMany<ContaContabil_CentroCusto>()
                    .Where(x => x.IdCentroCusto == idCentroCusto)
                    .Where(x => idsContasExcluidas.Contains(x.IdContaContabil))
                    .Execute();
            }
        }
    }
}