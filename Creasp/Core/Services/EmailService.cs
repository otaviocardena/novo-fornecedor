﻿using System;
using System.Collections.Generic;
using System.Linq;
using NBox.Core;
using NBox.Services;
using Nustache.Core;

namespace Creasp.Core
{
    public class EmailService : DataService
    {
        public EmailService(ServiceContext context) : base(context) { }

        public void EnviaEmail(IEmailDataSource ds, int idPessoa, string noAssunto, string noMensagem, string responderPara, bool atualizaCadastro = false, bool temNerp = true)
        {
            var p = ds.GetDestinatarios().Where(x => idPessoa == x.IdPessoa).FirstOrDefault();
            var msg = "";
            var dados = new object();

            if (temNerp) dados = ds.GetDados(p.IdPessoa);

            try
            {
                msg = Render.StringToString(noMensagem, dados, new RenderContextBehaviour { HtmlEncoder = (s) => s });
            }
            catch (Exception ex)
            {
                throw new NBoxException(ex.Message);
            }

            if (atualizaCadastro)
            {
                new PessoaService(context).AtualizaCadastro(p);
            }

            NBox.Services.Email.New()
                .To(p.NoEmail, p.NoPessoa)
                .ReplyTo(responderPara)
                .Subject(noAssunto)
                .Body("<div>" + msg + "</div>")
                .Send();

            RegistraEnvioDeEmail(idPessoa, p.NoEmail, noAssunto, noMensagem);
        }

        /// <summary>
        /// Envia os emails conforme templates
        /// </summary>
        public void GeraEmails(IEmailDataSource ds, List<int> idPessoas, string assunto, string template, string replyTo)
        {
            foreach (var p in ds.GetDestinatarios().Where(x => idPessoas.Contains(x.IdPessoa)))
            {
                EnviaEmail(ds, p.IdPessoa, assunto, template, replyTo, true);
            }
        }

        /// <summary>
        /// Atualiza o template atual com um novo texto
        /// </summary>
        public void AtualizaTemplate(string cdTemplate, string titulo, string mensagem, string replyTo)
        {
            db.Update(new EmailTemplate { CdTemplate = cdTemplate, NoTitulo = titulo, NoMensagem = mensagem, NoEmailResposta = replyTo });
        }

        /// <summary>
        /// Reenvia um email, fazendo a atualização de email caso necessário
        /// </summary>
        public void ReenviaEmail(int idEmail)
        {
            var old = db.Query<Email>()
                .Include(x => x.Pessoa)
                .Single(x => x.IdEmail == idEmail);

            NBox.Services.Email.New()
                .To(old.Pessoa.NoEmail, old.Pessoa.NoPessoa)
                .Subject(old.NoAssunto)
                .Body(old.NoMensagem)
                .Send();

            new PessoaService(context).AtualizaCadastro(old.Pessoa);

            var e = new Email
            {
                IdPessoa = old.IdPessoa,
                NoEmail = old.NoEmail,
                NoAssunto = old.NoAssunto,
                NoMensagem = old.NoMensagem,
                DtEnvio = DateTime.Now
            };

            db.Insert(e);
        }

        public void RegistraEnvioDeEmail(int idPessoaDest, string noEmail, string noAssunto, string noMensagem)
        {
            var email = new Email()
            {
                IdPessoa = idPessoaDest,
                NoEmail = noEmail,
                NoAssunto = noAssunto,
                NoMensagem = noMensagem?.Length >= 8000 ? noMensagem?.Substring(0, 8000) : noMensagem,
                DtEnvio = DateTime.Now
            };

            db.Insert(email);
        }
    }
}