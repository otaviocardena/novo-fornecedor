﻿using Creasp.Core.DTO;
using Creasp.Core.Model;
using NBox.Core;
using NBox.Services;
using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Creasp.Core.Services
{
    public class FornecedorService : DataService
    {
        protected readonly int nrAnoBase;
        public FornecedorService(ServiceContext context) : base(context)
        {
            nrAnoBase = (context as CreaspContext).NrAnoBase;
        }

        #region FornecedorUsuarioTemp
        public void InseririUsuarioTemporario(FornecedorUsuariosTemp usuario)
        {
            //TODO: Validar se não existe mesmo nome na tabela de login
            usuario.Senha = Converter.MD5(usuario.Senha);
            db.Insert(usuario);
        }

        public QueryPage<FornecedorUsuariosTemp> ListaUsuariosFornecedorTemp(Pager p, string idSecao)
        {
            var sql = Sql.Builder.Append(
              $@"SELECT IdSecao, Nome, Email, Senha, DataCadastro
                FROM CREASP.dbo.FornecedorUsuariosTemp WHERE IdSecao = '{idSecao}'");

            return db.Page<FornecedorUsuariosTemp>(sql, p.DefaultSort("Nome"));
        }

        public bool ExisteUsuarioTempPorEmail(string email, string idSecao)
        {
            return db.Query<FornecedorUsuariosTemp>()
                .FirstOrDefault(u => u.Email == email && u.IdSecao == idSecao) != null;
        }

        public bool ExisteUsuarioPorEmail(string email)//new
        {
            return db.Query<FornecedorUsuariosTemp>()
                .FirstOrDefault(u => u.Email == email) != null;
        }

        public FornecedorUsuariosTemp BuscaUsuarioTempPorEmail(string email, string idSecao)
        {
            return db.Query<FornecedorUsuariosTemp>()
                .FirstOrDefault(u => u.Email == email && u.IdSecao == idSecao);
        }

        public List<FornecedorUsuariosTemp> BuscaUsuarioTempSecao(string idSecao)
        {
            return db.Query<FornecedorUsuariosTemp>()
                .Where(u => u.IdSecao == idSecao).ToList();
        }

        public void ExcluirUsarioTemp(string email, string idSecao)
        {
            var usuarioTemp = db.Query<FornecedorUsuariosTemp>()
                .FirstOrDefault(u => u.Email == email && u.IdSecao == idSecao);

            if (usuarioTemp == null)
            {
                return;
            }

            db.DeleteMany<FornecedorUsuariosTemp>().Where(u => u.Email == email && u.IdSecao == idSecao).Execute();
        }

        public void ExcluirUsarioDoFornecedor(int idPessoa, int? idFornecedor)//new
        {
            var pessoa = db.Query<Pessoa>().FirstOrDefault( p => p.IdPessoa == idPessoa);
            var fornecedorPessoa = db.Query<FornecedorPessoa>().FirstOrDefault(fp => fp.IdPessoa == idPessoa && fp.IdFornecedor == idFornecedor);
            var loginFornecedorPessoa = db.Query<LoginFornecedor>().FirstOrDefault(lf => lf.IdFornecedorPessoa == fornecedorPessoa.IdFornecedorPessoa);

            if (pessoa == null || fornecedorPessoa != null || loginFornecedorPessoa != null)
            {
                return;
            }

            //db.Delete(fornecedorPessoa);
            //db.Delete(loginFornecedorPessoa);
            //db.Delete(pessoa);
        }

        public void EditarUsarioTemp(string oldEmail, FornecedorUsuariosTemp usuario)
        {
            var usuarioTempOld = db.Query<FornecedorUsuariosTemp>()
                .FirstOrDefault(u => u.Email == oldEmail && u.IdSecao == usuario.IdSecao);

            if (usuarioTempOld == null)
            {
                return;
            }

            if (string.IsNullOrEmpty(usuario.Senha)) usuario.Senha = usuarioTempOld.Senha;
            else usuario.Senha = Converter.MD5(usuario.Senha);

            db.DeleteMany<FornecedorUsuariosTemp>().Where(u => u.Email == oldEmail && u.IdSecao == usuario.IdSecao).Execute();
            db.Insert(usuario);
        }

        public LoginFornecedor BuscaLoginFornecedor(string oldEmail)//new
        {
            var loginFornecedor = db.Query<LoginFornecedor>().FirstOrDefault(l => l.Login == oldEmail);

            if (loginFornecedor == null)
            {
                return null;
            }

            return loginFornecedor;
        }

        public void EditarUsarioFornecedor(string oldEmail, string email, string nome, string senha)//new
        {
            var loginFornecedor = db.Query<LoginFornecedor>().FirstOrDefault( l => l.Login == oldEmail);
            var pessoa = db.Query<Pessoa>().FirstOrDefault(p => p.NoEmail == oldEmail);

            if (loginFornecedor == null || pessoa == null)
            {
                return;
            }

            if (senha != null && senha != "") {
                loginFornecedor.Senha = Converter.MD5(senha);
            }

            loginFornecedor.Login = email;
            pessoa.NoEmail = email;
            pessoa.NoLogin = email;
            pessoa.NoPessoa = nome;

            db.Update(loginFornecedor);
            db.Update(pessoa);
        }

        public string InserirUsuarioFornecedor(Fornecedor fornecedor, string email, string nome, string senha) //new
        {

            var pessoa =  new Pessoa
            {
                NrCpf = fornecedor.Cnpj.UnMask(),
                NoPessoa = nome,
                NoLogin = email,
                NoEmail = email,
                FgAceitaEmail = true,
                NoTitulo = "FORNECEDOR"
            };

            db.Insert(pessoa);
            var pessoaCadastrada = db.Query<Pessoa>()
                .FirstOrDefault(u => u.NoLogin == pessoa.NoLogin);

            if (pessoaCadastrada == null)
            {
                return $"Não foi possivel adicionar o usuário {nome}";
            }

            var fornecedorPessoa = new FornecedorPessoa
            {
                IdPessoa = pessoaCadastrada.IdPessoa,
                IdFornecedor = fornecedor.IdFornecedor
            };

            db.Insert(fornecedorPessoa);
            var fornecedorPessoaCad = db.Query<FornecedorPessoa>()
                .FirstOrDefault(u => u.IdFornecedor == fornecedorPessoa.IdFornecedor && u.IdPessoa == fornecedorPessoa.IdPessoa);

            if (fornecedorPessoaCad == null)
            {
                return $"Não foi possivel adicionar o usuário {nome}";
            }

            var loginFornecedor = new LoginFornecedor
            {
                IdFornecedorPessoa = fornecedorPessoaCad.IdFornecedorPessoa,
                Login = pessoaCadastrada.NoLogin,
                Senha = Converter.MD5(senha)
            };
            db.Insert(loginFornecedor);

            return null;
        }

        public void AtualizaDadosFornecedor(int? idfornecedor, Fornecedor f) //new
        {
            var fornecedorOld = db.Query<Fornecedor>().FirstOrDefault(fn => fn.IdFornecedor == idfornecedor);

            if (fornecedorOld == null) {
                return;
            }

            fornecedorOld.Cnpj = f.Cnpj;
            fornecedorOld.Email = f.Email;
            fornecedorOld.RazaoSocial = f.RazaoSocial;
            fornecedorOld.NomeFantasia = f.NomeFantasia;
            fornecedorOld.Logradouro = f.Logradouro;
            fornecedorOld.CEP = f.CEP;
            fornecedorOld.Complemento = f.Complemento;
            fornecedorOld.Numero = f.Numero;

            db.Update(fornecedorOld);
        }

        public List<Pessoa> ObtemPessoaPorFornecedor(int? idFornecedor){

            var fornecedorPessoa = db.Query<FornecedorPessoa>().FirstOrDefault(f => f.IdFornecedor == idFornecedor);

            return db.Query<Pessoa>().Where(f => f.IdPessoa == fornecedorPessoa.IdPessoa).ToList();

        }

        public List<Pessoa> ObtemPessoaPorFornecedorNew(int? idFornecedor)//new
        {

            var fornecedorPessoa = db.Query<FornecedorPessoa>().Where(f => f.IdFornecedor == idFornecedor).ToList();
            var pessoas = new List<Pessoa>();

            foreach (var fp in fornecedorPessoa) {
                pessoas.Add(db.Query<Pessoa>().FirstOrDefault(f => f.IdPessoa == fp.IdPessoa));
            }

            return pessoas;

        }

        public LoginFornecedor ObtemFornecedorPessoa(int? idPessoa)
        {

            var fornecedorPessoa = db.Query<FornecedorPessoa>().FirstOrDefault(f => f.IdPessoa == idPessoa);
            var loginFornecedor = db.Query<LoginFornecedor>().FirstOrDefault(f => f.IdFornecedorPessoa == fornecedorPessoa.IdFornecedorPessoa);

            loginFornecedor.Senha = "";

            return loginFornecedor;

        }
        #endregion

        #region Fornecedor
        public Tuple<bool, List<string>> NovoFornecedor(Fornecedor fornecedor, List<FornecedorUsuariosTemp> pessoas)
        {
            var erros = new List<string>();
            var fornecedorExiste = db.Query<Fornecedor>()
                    .FirstOrDefault(u => u.Cnpj == fornecedor.Cnpj) != null;

            if (fornecedorExiste)
            {
                erros.Add($"Cnpj {fornecedor.Cnpj} já existe na nossa base de dados, por favor tente realizar o login com um de seus usuários cadastrados");
                return new Tuple<bool, List<string>>(false, erros);
            }

            db.Insert(fornecedor);

            var fornecedorCadastrada = db.Query<Fornecedor>()
                    .FirstOrDefault(u => u.Cnpj == fornecedor.Cnpj);

            if (fornecedorCadastrada == null)
            {
                erros.Add($"Erro ao adicionar o fornecedor {fornecedor.NomeFantasia}, tente alterar as informações e tente novamente");
                return new Tuple<bool, List<string>>(false, erros);
            }

            foreach (var pessoa in pessoas)
            {
                var pessoaMergiada = MontarPessoa(pessoa, fornecedor.Cnpj);

                db.Insert(pessoaMergiada);
                var pessoaCadastrada = db.Query<Pessoa>()
                    .FirstOrDefault(u => u.NoLogin == pessoaMergiada.NoLogin);

                if (pessoaCadastrada == null)
                {
                    erros.Add($"Não foi possivel adicionar o usuário {pessoa.Nome}");
                    db.Delete<Fornecedor>($"DELETE Fornecedor WHERE IdFornecedor = {fornecedorCadastrada.IdFornecedor}");
                    break;
                }

                var fornecedorPessoa = new FornecedorPessoa {
                    IdPessoa = pessoaCadastrada.IdPessoa,
                    IdFornecedor = fornecedor.IdFornecedor
                };

                db.Insert(fornecedorPessoa);
                var fornecedorPessoaCad = db.Query<FornecedorPessoa>()
                    .FirstOrDefault(u => u.IdFornecedor == fornecedorPessoa.IdFornecedor && u.IdPessoa == fornecedorPessoa.IdPessoa);

                if (fornecedorPessoaCad == null)
                {
                    erros.Add($"Não foi possivel adicionar o usuário {pessoa.Nome}");
                    break;
                }

                var loginFornecedor = new LoginFornecedor
                {
                    IdFornecedorPessoa = fornecedorPessoaCad.IdFornecedorPessoa,
                    Login = pessoaCadastrada.NoLogin,
                    Senha = pessoa.Senha
                };
                db.Insert(loginFornecedor);
            }

            if (erros?.Any() ?? false) return new Tuple<bool, List<string>>(false, erros);

            return new Tuple<bool, List<string>>(true, null);
        }

        private Pessoa MontarPessoa(FornecedorUsuariosTemp pessoa, string cnpj)
        {
            return new Pessoa
            {
                NrCpf = cnpj.UnMask(),
                NoPessoa = pessoa.Nome,
                NoLogin = pessoa.Email,
                NoEmail = pessoa.Email,
                FgAceitaEmail = true,
                NoTitulo = "FORNECEDOR"
            };
        }

        public Fornecedor ObtemFornecedor(int? idFornecedor) {

            return db.FirstOrDefault<Fornecedor>(f => f.IdFornecedor == idFornecedor);

        }

        public QueryPage<Fornecedor> listaFornecedores(Pager p)
        {
            var sql = Sql.Builder.Append(
              $@"SELECT * FROM CREASP.dbo.Fornecedor");

            return db.Page<Fornecedor>(sql, p.DefaultSort("IdFornecedor"));
        }

        #endregion
    }
}
