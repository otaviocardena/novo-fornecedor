﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Creasp.Core;
using NBox.Core;
using NBox.Services;
using NPoco;
using NPoco.Linq;

namespace Creasp.Core
{
    public class UnidadeService : DataService
    {
        public UnidadeService(ServiceContext context) : base(context) { }

        public Unidade BuscaUnidadeCentroCusto(string nrCentroCusto)
        {
            return cache.Get<Unidade>("unid-cc-" + nrCentroCusto, () =>
            {
                return db.Query<Unidade>()
                    .FirstOrDefault(x => x.NrCentroCusto == nrCentroCusto.UnMask());
            });
        }

        public IEnumerable<Unidade> ListaUnidades(string tpUnidade, string noUnidade)
        {
            return db.Query<Unidade>()
                .Where(tpUnidade != null, x => x.TpUnidade == tpUnidade)
                .Where(noUnidade != null, x => x.NoUnidade.Contains(noUnidade))
                .ToEnumerable()
                .OrderBy(x => Regex.Replace(x.NoUnidade, @"\s(\d)\D", " 0$1")); // para ordenar considerando que na string está o nro (e tem mais de 10)
        }

        /// <summary>
        /// Lista as unidades filhas de uma unidade pai (FAZ CACHE)
        /// </summary>
        public IEnumerable<Unidade> ListaUnidadesFilhas(int cdUnidadePai)
        {
            return cache.Get("unidades-filha-" + cdUnidadePai, () =>
            {
                return db.Fetch<Unidade>(@";WITH x AS
                    (
                        -- anchor:
                        SELECT CdUnidade, CdUnidadePai, NoUnidade, TpUnidade, NrCentroCusto, Nivel = 0
                        FROM Unidade WHERE CdUnidadePai = @0
                        UNION ALL
                        -- recursive:
                        SELECT u.CdUnidade, u.CdUnidadePai, u.NoUnidade, u.TpUnidade, u.NrCentroCusto, Nivel = x.Nivel + 1
                        FROM x INNER JOIN Unidade AS u
                        ON u.CdUnidadePai = x.CdUnidade
                    )
                    SELECT CdUnidade, CdUnidadePai, NoUnidade, TpUnidade, NrCentroCusto, Nivel FROM x
                    ORDER BY Nivel, NoUnidade
                    OPTION (MAXRECURSION 32)
                    ", cdUnidadePai);
            });
        }

        /// <summary>
        /// Busca uma unidade pai conforme a unidade filha passada (FAZ CACHE)
        /// </summary>
        public Unidade BuscaUnidadePai(int cdUnidadeFilha, string tpUnidadePai)
        {
            return cache.Get<Unidade>("unidade-f-" + cdUnidadeFilha + "-pai-" + tpUnidadePai,  () =>
            {
                var u = db.FirstOrDefault<Unidade>(x => x.CdUnidade == cdUnidadeFilha);

                while(u != null && u.CdUnidadePai != null)
                {
                    if(u.TpUnidade == tpUnidadePai)
                    {
                        return u;
                    }

                    u = db.FirstOrDefault<Unidade>(x => x.CdUnidade == u.CdUnidadePai);
                }

                return null;
            });
        }

        /// <summary>
        /// Retorna a unidade UCO fixa pelo centro de custo no web.config
        /// </summary>
        public Unidade UCO
        {
            get { return BuscaUnidadeCentroCusto(ConfigurationManager.AppSettings["centrocusto.uco"].UnMask()); }
        }


        /// <summary>
        /// Retorna a unidade UFI fixa pelo centro de custo no web.config
        /// </summary>
        public Unidade UFI
        {
            get { return BuscaUnidadeCentroCusto(ConfigurationManager.AppSettings["centrocusto.ufi"].UnMask()); }
        }

        /// <summary>
        /// Retorna o departamento CONT fixa pelo centro de custo no web.config
        /// </summary>
        public Unidade CONT
        {
            get { return BuscaUnidadeCentroCusto(ConfigurationManager.AppSettings["centrocusto.cont"].UnMask()); }
        }

    }
}