﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using Creasp.Core;
using Creasp.Nerp;
using Creasp.Siplen;
using NBox.Core;
using NBox.Services;
using NPoco;
using NPoco.Linq;

namespace Creasp.Core
{
    public class FuncionarioService : DataService
    {
        protected ISenior senior = Factory.Get<ISenior>();

        public FuncionarioService(ServiceContext context) : base(context) { }

        /// <summary>
        /// Monta todos os dados de functionario baseado no login da pessoa. Neste momento a pessoa já deve ter o vinculo com Pessoa. Os superiores podem não ter cadastro pois serão incluidos automaticamente
        /// </summary>
        public FuncionarioHierarquia BuscaFuncionarioHierarquia(string noLogin)
        {
            var pessoa = db.Query<Pessoa>()
                .Where(x => x.NoLogin == noLogin)
                .Single("Não foi encontrada o vinculo de pessoa do login " + noLogin);

            var pessoas = new PessoaService(context);

            var f = new FuncionarioHierarquia { Pessoa = pessoa };

            FuncionarioSenior func = null;

            // caso a pessoa não tenha matricula, busca pelo CPF na Senior e atualiza meu cadastro
            if (pessoa.NrMatricula == null)
            {
                // busca o funcionario no RH pelo CPF e atualiza nosso sistema
                func = senior.BuscaFuncionarios(null, pessoa.NrCpf, null).FirstOrDefault();

                if (func == null) throw new NBoxException("A pessoa {0} não possui cadastro no sistema Senior/RH", pessoa.NoPessoa);

                pessoa.NrMatricula = func.NrMatricula;

                db.Update(pessoa);
            }
            else
            {
                // busca o funcionario no RH pela matricula
                func = senior.BuscaFuncionarios(pessoa.NrMatricula.Value, null, null).FirstOrDefault();
            }

            // se não encontrar o funcionario no RH, ja gera o erro
            if (func == null) throw new NBoxException("Não foi encontrado no sistema SENIOR/RH o funcionário de matricula " + pessoa.NrMatricula);

            // busca, na senior, os dados de funcionario (por causa do centro de custo)
            var chefe = senior.BuscaFuncionarios(null, func.NrCpfChefe, null).FirstOrDefault();
            var gerente = senior.BuscaFuncionarios(null, func.NrCpfGerente, null).FirstOrDefault();
            var superintendente = senior.BuscaFuncionarios(null, func.NrCpfSuperintendente, null).FirstOrDefault();

            // busca as pessoas
            f.Chefe.Pessoa = pessoas.BuscaOuIncluiPessoa(chefe.NrCpf);
            f.Gerente.Pessoa = pessoas.BuscaOuIncluiPessoa(gerente.NrCpf);
            f.Superintendente.Pessoa = pessoas.BuscaOuIncluiPessoa(superintendente.NrCpf);

            // monta as unidades conforme os centro de custos (busca pelo anteior também)
            f.Unidade = BuscaUnidadeCentroDeCusto(func.NrCentroCusto);

            // valida possiveis inconsistencias
            if (f.Unidade == null) throw new NBoxException("Não foi encontrada a unidade do funcionário {0} no centro de custo {1}", func.NoFuncionario, func.NrCentroCusto);

            f.Chefe.Unidade = BuscaUnidadeCentroDeCusto(chefe.NrCentroCusto) ??
                f.Unidade;

            f.Gerente.Unidade = BuscaUnidadeCentroDeCusto(gerente.NrCentroCusto) ??
                f.Unidade;

            f.Superintendente.Unidade = BuscaUnidadeCentroDeCusto(superintendente.NrCentroCusto) ??
                f.Unidade;

            return f;
        }

        /// <summary>
        /// Busca um funcionario - pode ser necessário ter que cadastrar a pessoa no sistema
        /// </summary>
        public Funcionario BuscaFuncionario(int? nrMatricula, string nrCpf)
        {
            var func = senior.BuscaFuncionarios(nrMatricula, nrCpf, null).FirstOrDefault();

            if (func == null) throw new NBoxException("Não foi encontrado o funcionário (Matricula: {0}, CPF: {1}", nrMatricula, nrCpf);

            var pessoas = new PessoaService(context);

            var f = new Funcionario
            {
                Pessoa = pessoas.BuscaOuIncluiPessoa(func.NrCpf),
                Unidade = BuscaUnidadeCentroDeCusto(func.NrCentroCusto)
            };

            if (f.Unidade == null) throw new NBoxException("Não foi encontrada a unidade do funcionário {0} no centro de custo {1}", func.NoFuncionario, func.NrCentroCusto);

            return f;
        }

        /// <summary>
        /// Busca a unidade aonde um funcionario está alocado (consulta SENIOR para pegar o centro de custo)
        /// </summary>
        public Unidade BuscaUnidade(Pessoa pessoa)
        {
            if (pessoa.NrMatricula == null) throw new NBoxException("{0} não tem número de matricula", pessoa.NoPessoa);

            var func = senior.BuscaFuncionarios(pessoa.NrMatricula.Value, null, null).FirstOrDefault();

            if (func == null) throw new NBoxException("{0}-{1} não foi encontrado no sistema Senior/RH", pessoa.NrMatricula, pessoa.NoPessoa);

            var unidade = BuscaUnidadeCentroDeCusto(func.NrCentroCusto);

            if (unidade == null) throw new NBoxException("Não foi encontrada a unidade para o funcionário {0} no centro de custo {1}",func.NoFuncionario ,func.NrCentroCusto);

            return unidade;
        }

        /// <summary>
        /// Busca a unidade por centro de custo
        /// </summary>
        public Unidade BuscaUnidadeCentroDeCusto(string nrCentroCusto)
        {
            // Buscar a unidade na seguinte ordem: primeiro a unidade ativa pelo centro de custo atual
            // quando não encontrar, começa a buscar pelas outras regras
            return 
                db.FirstOrDefault<Unidade>(x => x.NrCentroCusto == nrCentroCusto && x.FgAtivo == true) ??
                db.FirstOrDefault<Unidade>(x => x.NrCentroCustoAnterior == nrCentroCusto && x.FgAtivo == true) ??
                db.FirstOrDefault<Unidade>(x => x.NrCentroCusto == nrCentroCusto && x.FgAtivo == false) ??
                db.FirstOrDefault<Unidade>(x => x.NrCentroCustoAnterior == nrCentroCusto && x.FgAtivo == false);
        }
    }
}