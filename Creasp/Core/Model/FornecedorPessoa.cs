﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Core.Model
{
    [PrimaryKey("IdFornecedorPessoa", AutoIncrement = true), Serializable]
    public class FornecedorPessoa
    {
        public int IdFornecedorPessoa { get; set; }
        public int IdFornecedor { get; set; }
        public int IdPessoa { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdPessoa", ReferenceMemberName = "IdPessoa")]
        public Pessoa Pessoa { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdFornecedor", ReferenceMemberName = "IdFornecedor")]
        public Fornecedor Fornecedor { get; set; }
    }
}
