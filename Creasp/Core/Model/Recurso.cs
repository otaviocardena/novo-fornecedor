﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using NPoco;

namespace Creasp.Core
{
    public class Recurso
    {
        public static List<Recurso> Recursos = new List<Recurso>();

        static Recurso()
        {
            var filename = "~/Content/Sitemap/recursos.ini";
            var file = HttpContext.Current.Server.MapPath(filename);

            foreach (var line in File.ReadAllLines(file))
            {
                if (line.Trim().Length == 0 || line.StartsWith("#")) continue;
                var arr = line.Split('=');
                var parts = Regex.Match(arr[0], @"^([\w-\.]*)\[?([\w]+\|?)*\]?");
                var cdRecurso = parts.Groups[1].Value;
                var subrecursos = parts.Groups[2].Captures.Cast<Capture>().Select(x => x.Value.Replace("|", "")).ToList();
                var noRecurso = arr.Length == 1 ? cdRecurso : arr[1].Trim();

                var recurso = new Recurso { CdRecurso = cdRecurso, NoRecurso = noRecurso };

                Recursos.Add(recurso);

                foreach(var subrecurso in subrecursos)
                {
                    Recursos.Add(new Recurso
                    {
                        CdRecurso = cdRecurso + "." + subrecurso,
                        NoRecurso = noRecurso + " <b>[" + subrecurso.ToUpper() + "]</b>"
                    });
                }
            }
        }

        public string CdRecurso { get; set; }
        public string NoRecurso { get; set; }
        //public List<string> Subrecursos { get; set; }
    }
}