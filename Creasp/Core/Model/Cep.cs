﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NBox.Core;
using NPoco;

namespace Creasp.Core
{
    [PrimaryKey("NrCep", AutoIncrement = false), Serializable]
    public class Cep
    {
        public string NrCep { get; set; }
        public string NoLogradouro { get; set; }
        public string NoBairro { get; set; }
        public int CdCidade { get; set; }
        public string NoCidade { get; set; }
        public string CdUf { get; set; }

        [Ignore]
        public string NrCepFmt => NrCep.Mask("99.999-999");
    }
}