﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp.Core;
using NBox.Core;
using NPoco;

namespace Creasp.Core
{
    [PrimaryKey("IdEntidade", AutoIncrement = true)]
    public class Entidade : IFgAtivo, IEqualityComparer<Entidade>
    {
        public int IdEntidade { get; set; }
        public string CdExterno { get; set; }
        public int? CdRepasse { get; set; }
        public string NoEntidade { get; set; }
        public string NrCnpj { get; set; }
        public string TpEntidade { get; set; }
        public int? CdUnidade { get; set; }
        public bool FgRepresentante { get; set; }
        public bool FgAtivo { get; set; } = true;

        [Reference(ReferenceType.Many, ColumnName = "IdEntidade", ReferenceMemberName = "IdEntidade")]
        public List<EntidadeCamaras> Camaras { get; set; } = new List<EntidadeCamaras>();

        [Reference(ReferenceType.OneToOne, ColumnName = "CdUnidade", ReferenceMemberName = "CdUnidade")]
        public Unidade Unidade { get; set; }

        #region Equals

        public bool Equals(Entidade x, Entidade y)
        {
            var cx = x.Camaras == null ? new int[0] : x.Camaras.Select(c => c.CdCamara).OrderBy(c => c).ToArray();
            var cy = y.Camaras == null ? new int[0] : y.Camaras.Select(c => c.CdCamara).OrderBy(c => c).ToArray();

            return
                x.IdEntidade == y.IdEntidade &&
                x.CdExterno == y.CdExterno &&
                x.CdRepasse == y.CdRepasse &&
                x.NoEntidade.Trim() == y.NoEntidade.Trim() &&
                x.NrCnpj == y.NrCnpj &&
                x.TpEntidade == y.TpEntidade &&
                x.CdUnidade == y.CdUnidade &&
                x.FgRepresentante == y.FgRepresentante &&
                x.FgAtivo == y.FgAtivo &&
                cx.SequenceEqual(cy);
        }

        public int GetHashCode(Entidade obj)
        {
            return obj.CdExterno.GetHashCode() + obj.TpEntidade.GetHashCode();
        }

        #endregion

        public override string ToString()
        {
            return $"{CdExterno} - {NoEntidade} - {NrCnpj} - Unidade: {CdUnidade} - (Ativo: {(FgAtivo ? "S" : "N")})";
        }
    }
}