﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp.Core;
using NPoco;

namespace Creasp.Core
{
    [PrimaryKey("IdDocumentoMigracao", AutoIncrement = true), Serializable]
    public class DocumentoMigracao
    {
        public int IdDocumentoMigracao { get; set; }
        public string FileId { get; set; }
        public bool FgImportado { get; set; }

    }
}