﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NPoco;

namespace Creasp.Core
{
    [PrimaryKey("CdGrupo,CdRecurso", AutoIncrement = false), Serializable]
    public class GrupoAcesso
    {
        public string CdGrupo { get; set; }
        public string CdRecurso { get; set; }
    }
}