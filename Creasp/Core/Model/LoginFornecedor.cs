﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Core.Model
{
    [PrimaryKey("IdLogin", AutoIncrement = true), Serializable]
    public class LoginFornecedor
    {
        public int IdLogin { get; set; }
        public string Login { get; set; }
        public int IdFornecedorPessoa { get; set; }
        public string Senha { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdFornecedorPessoa", ReferenceMemberName = "IdFornecedorPessoa")]
        public FornecedorPessoa FornecedorPessoa { get; set; }
    }
}
