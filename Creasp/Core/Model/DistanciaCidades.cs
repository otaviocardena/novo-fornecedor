﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NBox.Core;
using NPoco;

namespace Creasp.Core
{
    [PrimaryKey("CdCidadeOrigem,CdCidadeDestino", AutoIncrement = false), Serializable]
    public class DistanciaCidades
    {
        public int CdCidadeOrigem { get; set; }
        public int CdCidadeDestino { get; set; }
        public int VrKm { get; set; }
    }
}