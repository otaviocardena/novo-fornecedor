﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NBox.Core;
using NPoco;

namespace Creasp.Core
{
    [PrimaryKey("CdTemplate", AutoIncrement = true), Serializable]
    public class EmailTemplate
    {
        public string CdTemplate { get; set; }
        public string NoTitulo { get; set; }
        public string NoMensagem { get; set; }
        public string NoEmailResposta { get; set; }
    }
}