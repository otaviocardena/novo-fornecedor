﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp.Core;
using NPoco;

namespace Creasp.Core
{
    [PrimaryKey("IdDocumento", AutoIncrement = true), Serializable]
    public class Documento : IAuditoria
    {
        public int IdDocumento { get; set; }

        public string TpRef { get; set; }
        public int IdRef { get; set; }
        public int? IdPessoa { get; set; }
        public int? CdTipoDoc { get; set; }
        public string NoArquivo { get; set; }
        public DateTime DtValidade { get; set; } = Periodo.MaxDate;
        public string FileId { get; set; }

        #region IAuditoria
        public DateTime DtCadastro { get; set; } = DateTime.Now;
        public DateTime? DtAlteracao { get; set; }
        public string LoginCadastro { get; set; }
        public string LoginAlteracao { get; set; }
        #endregion

        [Reference(ReferenceType.OneToOne, ColumnName = "CdTipoDoc", ReferenceMemberName = "CdTipoDoc")]
        public TipoDocumento Tipo { get; set; }
    }
}