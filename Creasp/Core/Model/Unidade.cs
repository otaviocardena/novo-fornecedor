﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp.Core;
using NPoco;

namespace Creasp.Core
{
    [PrimaryKey("CdUnidade", AutoIncrement = false), Serializable]
    public class Unidade : IFgAtivo, IEqualityComparer<Unidade>
    {
        public int CdUnidade { get; set; }
        public int? CdUnidadePai { get; set; }
        public string NoUnidade { get; set; }
        public string TpUnidade { get; set; }
        public string NrCep { get; set; }
        public string NrCentroCusto { get; set; }
        public string NrCentroCustoAnterior { get; set; }
        public string NrCpfGestor { get; set; }
        public bool FgAtivo { get; set; } = true;

        [Reference(ReferenceType.OneToOne, ColumnName = "NrCep", ReferenceMemberName = "NrCep")]
        public Cep Cep { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "NrCentroCusto", ReferenceMemberName = "NrCentroCusto")]
        public CentroCusto CentroCusto { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "NrCentroCustoAnterior", ReferenceMemberName = "NrCentroCustoAnterior")]
        public CentroCusto CentroCustoAnterior { get; set; }

        /// <summary>
        /// Formata o centro de custo utilizando as regras do CentroCusto
        /// </summary>
        [Ignore]
        public string NrCentroCustoFmt => new CentroCusto { NrCentroCusto = NrCentroCusto ?? "-" }.NrCentroCustoFmt;

        [Ignore]
        public string NrCentroCustoAnteriorFmt => new CentroCusto { NrCentroCusto = NrCentroCustoAnterior ?? "-" }.NrCentroCustoFmt;

        public bool Equals(Unidade x, Unidade y)
        {
            return 
                x.CdUnidade == y.CdUnidade &&
                x.CdUnidadePai == y.CdUnidadePai &&
                x.NoUnidade == y.NoUnidade &&
                x.TpUnidade == y.TpUnidade &&
                x.NrCep == y.NrCep &&
                x.NrCentroCusto == y.NrCentroCusto &&
                x.FgAtivo == y.FgAtivo;
        }

        public int GetHashCode(Unidade obj)
        {
            return CdUnidade.GetHashCode();
        }

        public override string ToString()
        {
            return $"{CdUnidade} - {CdUnidadePai} - {NoUnidade} - {NrCentroCustoFmt} : Gestor: {NrCpfGestor} (Ativo: {(FgAtivo ? "S" : "N")} )";
        }
    }
}