﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp.Core;
using NBox.Core;
using NPoco;
using Creasp.Elo;

namespace Creasp.Core
{
    [PrimaryKey("IdContaContabil", AutoIncrement = true), Serializable]
    public class ContaContabil : IFgAtivo, IEqualityComparer<ContaContabil>
    {
        public int IdContaContabil { get; set; }

        public int NrAno { get; set; }
        public string NrContaContabil { get; set; }
        public string NoContaContabil { get; set; }
        public int? CdResumido { get; set; }
        public bool FgAnalitico { get; set; } = false;
        public bool FgAtivo { get; set; } = true;

        [Ignore]
        public string NrContaContabilFmt => Formata(NrContaContabil);

        [Ignore]
        public string NoContaContabilFmt => $"{NrContaContabilFmt} - {NoContaContabil}";

        [Ignore]
        public string TpContaContabil => FgAnalitico ? "Analítico" : "Sintético";

        [Ignore]
        public string NrContaContabilPai => this.NrContaContabilFmt.Contains(".") ? this.NrContaContabilFmt.Substring(0, NrContaContabilFmt.LastIndexOf('.')).Replace(".", "") : null;


        [Reference(ReferenceType.OneToOne, ColumnName = "IdContaContabil", ReferenceMemberName = "IdContaContabil")]
        public ContaContabil_CentroCusto ContaContabilCentroCusto { get; set; }
        
        #region Equals

        public bool Equals(ContaContabil x, ContaContabil y)
        {
            return
                x.IdContaContabil == y.IdContaContabil &&
                x.NrAno == y.NrAno &&
                x.NrContaContabil == y.NrContaContabil &&
                x.CdResumido == y.CdResumido &&
                x.FgAnalitico == y.FgAnalitico &&
                x.NoContaContabil.Trim() == y.NoContaContabil.Trim() &&
                x.FgAtivo == y.FgAtivo;
        }

        public int GetHashCode(ContaContabil obj)
        {
            return obj.NrContaContabil.GetHashCode();
        }

        #endregion

        public static string Formata(string str)
        {
            var value = str.UnMask();

            return
                value.Length == 1 ? NBox.Core.Converter.ApplyMask(value, "9") :
                value.Length == 2 ? NBox.Core.Converter.ApplyMask(value, "9.9") :
                value.Length == 3 ? NBox.Core.Converter.ApplyMask(value, "9.9.9") :
                value.Length == 4 ? NBox.Core.Converter.ApplyMask(value, "9.9.9.9") :
                value.Length == 5 ? NBox.Core.Converter.ApplyMask(value, "9.9.9.9.9") :
                value.Length == 7 ? NBox.Core.Converter.ApplyMask(value, "9.9.9.9.9.99") :
                value.Length == 9 ? NBox.Core.Converter.ApplyMask(value, "9.9.9.9.9.99.99") :
                value.Length == 11 ? NBox.Core.Converter.ApplyMask(value, "9.9.9.9.9.99.99.99") :
                value.Length == 14 ? NBox.Core.Converter.ApplyMask(value, "9.9.9.9.9.99.99.99.999") :
                value.Length == 17 ? NBox.Core.Converter.ApplyMask(value, "9.9.9.9.9.99.99.99.999.999") :
                value.Length == 20 ? NBox.Core.Converter.ApplyMask(value, "9.9.9.9.9.99.99.99.999.999.999") :
                NBox.Core.Converter.ApplyMask(value, "9.9.9.9.9.99.99.99.999.999.999.999");
        }

        public override string ToString()
        {
            return $"{NoContaContabilFmt} - {CdResumido} (Ativo: {(FgAtivo ? "S" : "N")})";
        }
    }
}