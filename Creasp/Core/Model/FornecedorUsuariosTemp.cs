﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Core.Model
{
    public class FornecedorUsuariosTemp
    {
        public string DataCadastro { get; set; } = DateTime.Now.ToString();
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
        public string IdSecao { get; set; }
    }
}
