﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NPoco;

namespace Creasp.Core
{
    [PrimaryKey("CdTipoDoc", AutoIncrement = false), Serializable]
    public class TipoDocumento
    {
        public int CdTipoDoc { get; set; }

        public string TpRef { get; set; }
        public string NoTipoDoc { get; set; }
        public int NrOrdem { get; set; }
        public bool FgObrigatorio { get; set; }
        public bool FgAtivo { get; set; } = true;
    }
}