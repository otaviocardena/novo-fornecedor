﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp.Core;
using NPoco;

namespace Creasp.Core
{
    [PrimaryKey("CdCamara", AutoIncrement = false), Serializable]
    public class Camara : IPeriodo, IEqualityComparer<Camara>
    {
        public int CdCamara { get; set; }
        public string NoCamara { get; set; }
        public string CdSigla { get; set; }
        public DateTime DtIni { get; set; }
        public DateTime DtFim { get; set; }

        [Ignore]
        public List<Siplen.Mandato> Membros { get; set; } = new List<Siplen.Mandato>();

        /// <summary>
        /// Utilizado para exibir mensagens pro usuario de validação de uma comissão
        /// </summary>
        [Ignore]
        public string Mensagem { get; set; }

        #region Equals

        public bool Equals(Camara x, Camara y)
        {
            return
                x.CdCamara == y.CdCamara &&
                x.NoCamara == y.NoCamara;
        }

        public int GetHashCode(Camara obj)
        {
            return obj.CdCamara.GetHashCode();
        }

        #endregion
    }
}