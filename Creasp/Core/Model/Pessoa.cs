﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp.Core;
using NBox.Core;
using NPoco;

namespace Creasp.Core
{
    [PrimaryKey("IdPessoa", AutoIncrement = true), Serializable]
    public class Pessoa : IAuditoria
    {
        public int IdPessoa { get; set; }

        public string NrCpf { get; set; }
        public string NrCreasp { get; set; }
        public int? NrMatricula { get; set; }
        public string NoLogin { get; set; } // vincula um usuario a uma pessoa

        public string NoPessoa { get; set; }
        public string NoTitulo { get; set; }
        public DateTime? DtNasc { get; set; }
        public string TpSexo { get; set; }
        public string NoEmail { get; set; }
        public string NrTelefone { get; set; }

        public string NrCep { get; set; }
        public string NoLogradouro { get; set; }
        public string NrEndereco { get; set; }
        public string NoComplemento { get; set; }
        public string NoBairro { get; set; }
        public string NoEndCorresp { get; set; }
        public bool FgAceitaEmail { get; set; }
        public string NoHistorico { get; set; }
        public string CdTokenRedmine { get; set; }

        #region IAuditoria
        public DateTime DtCadastro { get; set; } = DateTime.Now;
        public DateTime? DtAlteracao { get; set; }
        public string LoginCadastro { get; set; }
        public string LoginAlteracao { get; set; }
        #endregion

        [Reference(ReferenceType.OneToOne, ColumnName = "NrCep", ReferenceMemberName = "NrCep")]
        public Cep Cep { get; set; }

        /// <summary>
        /// Retorna o CPF ou CNPJ formatado com pontuação
        /// </summary>
        [Ignore]
        public string NrCpfFmt => FormataCpf(NrCpf);

        [Ignore]
        public string EnderecoFmt => (
            (NoLogradouro == null ? "" : NoLogradouro + ", " + NrEndereco) +
            (NoComplemento == null ? "" : " - " + NoComplemento) +
            (NoBairro == null ? "" : "\n" + NoBairro) +
            (Cep == null ? "" : "\n" + Cep.NoCidade + " / " + Cep.CdUf + "\n" + Cep.NrCepFmt)
            ).Trim();

        /// <summary>
        /// Abrevia o titulo para melhor exibir nas consultas/relatórios
        /// </summary>
        [Ignore]
        public string NoTituloAbr => TituloAbr.Abreviar(NoTitulo);

        /// <summary>
        /// Dados de conta bancária. Não armazenado. Somente utilizado quando via SISCONT
        /// </summary>
        [Ignore]
        public DadosBancarios DadosBancarios { get; set; }

        /// <summary>
        /// Campo de mensagem para o usuario
        /// </summary>
        [Ignore]
        public string Mensagem { get; set; }

        public static string FormataCpf(string cpf)
        {
            return cpf.Mask(cpf.UnMask().Length == 11 ? "999.999.999-99" : "99.999.999/9999-99");
        }

        public static bool ValidaCpfZerado(string cpf)
        {
            if (cpf == null || cpf.Length < 11) cpf = (cpf ?? "").PadLeft(11, '0');

            if (cpf == "00000000000") return false;

            // valida apenas se o cpf é um numero repetido. Pode futuramente validar o digito tambem. Valida apenas CPF
            return true;
        }
    }
}