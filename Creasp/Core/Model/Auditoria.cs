﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NPoco;

namespace Creasp.Core
{
    [PrimaryKey("IdAuditoria", AutoIncrement = true), Serializable]
    public class Auditoria
    {
        public int IdAuditoria { get; set; }
        public DateTime DtAuditoria { get; set; }
        public string NoLogin { get; set; }
        public string NoPagina { get; set; }
        public string NoTabela { get; set; }
        public int? IdChave { get; set; }
        public string NoConteudo { get; set; }
    }
}