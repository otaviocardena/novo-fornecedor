﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp.Core;
using NBox.Core;
using NPoco;

namespace Creasp.Core
{
    [PrimaryKey("IdEntidade,CdCamara", AutoIncrement = false)]
    public class EntidadeCamaras
    {
        public int IdEntidade { get; set; }
        public int CdCamara { get; set; }
    }
}