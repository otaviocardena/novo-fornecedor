﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NBox.Core;
using NPoco;

namespace Creasp.Core
{
    [PrimaryKey("IdEmail", AutoIncrement = true), Serializable]
    public class Email
    {
        public int IdEmail { get; set; }
        public int IdPessoa { get; set; }
        public string NoEmail { get; set; }
        public string NoAssunto { get; set; }
        public string NoMensagem { get; set; }
        public DateTime DtEnvio { get; set; } = DateTime.Now;

        [Reference(ReferenceType.OneToOne, ColumnName = "IdPessoa", ReferenceMemberName = "IdPessoa")]
        public Pessoa Pessoa { get; set; }
    }
}