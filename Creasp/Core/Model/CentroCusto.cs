﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp.Core;
using NBox.Core;
using NPoco;
using Creasp.Elo;

namespace Creasp.Core
{
    [PrimaryKey("IdCentroCusto", AutoIncrement = true), Serializable]
    public class CentroCusto : IFgAtivo, IEqualityComparer<CentroCusto>
    {
        public int IdCentroCusto { get; set; }
        public Guid IdSiscont { get; set; }
        public int NrAno { get; set; }
        public int? NrAnoTerminoRestringir { get; set; }
        public string NrCentroCusto { get; set; }
        public string NoCentroCusto { get; set; }
        public string TpCentroCusto { get; set; }
        public bool FgAtivo { get; set; } = true;
        public bool FgRestringirFaseOrcamentaria { get; set; } = false;

        [Ignore]
        public string NrCentroCustoFmt => Formata(NrCentroCusto);

        [Ignore]
        public string NoCentroCustoFmt => $"{NrCentroCustoFmt} - {NoCentroCusto}";

        [Ignore]
        public string TpCentroCustoFmt
        {
            get
            {
                switch (TpCentroCusto)
                {
                    case Core.TpCentroCusto.Analitico:
                        return "Analitico";
                    case Core.TpCentroCusto.Sintetico:
                        return "Sintético";
                    case Core.TpCentroCusto.SubArea:
                        return "Sub-área";
                    default:
                        break;
                }

                return string.Empty;
            }
        }

        /// <summary>
        /// Retorna o codigo da subarea (caso seja uma subarea)
        /// </summary>
        [Ignore]
        public string CdSubArea => TpCentroCusto == "X" ? NrCentroCustoFmt.Substring(NrCentroCustoFmt.LastIndexOf(".") + 1) : "";

        [Ignore]
        public string NrCentroCustoPai => this.NrCentroCustoFmt.Contains(".") ? this.NrCentroCustoFmt.Substring(0, NrCentroCustoFmt.LastIndexOf('.')).Replace(".", "") : null;

        //[Reference(ReferenceType.Many, ColumnName = "IdCentroCusto", ReferenceMemberName = "IdCentroCusto")]
        //public List<CentroCustoResponsavel> CentrosCustosResponsaveis { get; set; }

        [Ignore]
        public CentroCustoResponsavel CentroCustoResponsavel { get; set; }

        #region Equals

        public bool Equals(CentroCusto x, CentroCusto y)
        {
            return
                x.IdCentroCusto == y.IdCentroCusto &&
                x.IdSiscont == y.IdSiscont &&
                x.NrAno == y.NrAno &&
                x.NrAnoTerminoRestringir == y.NrAnoTerminoRestringir &&
                x.NrCentroCusto == y.NrCentroCusto &&
                x.NoCentroCusto.Trim() == y.NoCentroCusto.Trim() &&
                x.TpCentroCusto == y.TpCentroCusto &&
                x.FgAtivo == y.FgAtivo &&
                x.FgRestringirFaseOrcamentaria == y.FgRestringirFaseOrcamentaria;
        }

        public int GetHashCode(CentroCusto obj)
        {
            return obj.NrCentroCusto.GetHashCode();
        }

        #endregion

        /// <summary>
        /// Retorna o NrCentroCusto formatado
        /// </summary>
        public static string Superior(string nrCentroCustoSubArea)
        {
            var nrCentroCustoFmt = Formata(nrCentroCustoSubArea);

            var analitico = nrCentroCustoFmt.Substring(0, nrCentroCustoFmt.LastIndexOf("."));

            return analitico;
        }

        /// <summary>
        /// Retorna o codigo de subarea de um centro de custo
        /// </summary>
        public static string SubArea(string nrCentroCustoSubArea)
        {
            var nrCentroCustoFmt = Formata(nrCentroCustoSubArea);

            return nrCentroCustoFmt.Substring(nrCentroCustoFmt.LastIndexOf(".") + 1);
        }

        /// <summary>
        /// Formata o centro de custo
        /// </summary>
        public static string Formata(string str)
        {
            var value = str.UnMask() ?? "";

            return
                value.Length == 2 ? Converter.ApplyMask(value, "99") :
                value.Length == 4 ? Converter.ApplyMask(value, "99.99") :
                value.Length == 6 ? Converter.ApplyMask(value, "99.99.99") :
                value.Length == 8 ? Converter.ApplyMask(value, "99.99.99.99") :
                value.Length == 9 ? Converter.ApplyMask(value, "99.99.99.999") :
                value.Length == 10 ? Converter.ApplyMask(value, "99.99.99.99.99") : value;
        }

        public override string ToString()
        {
            return $"{NrCentroCustoFmt} - {NoCentroCusto} (Tipo: {TpCentroCusto} - Ativo: {(FgAtivo ? "S" : "N")})";
        }
    }
}