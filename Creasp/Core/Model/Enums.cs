﻿namespace Creasp.Core
{
    public enum TpPessoa
    {
        Funcionario,
        Conselheiro,
        Inspetor,
        Outros
    }

    public enum TpParticipante
    {
        Funcionario,
        Diretor,
        Conselheiro,
        Inspetor,
        GrupoDeTrabalho,
        Outros
    }

    public class TpSexo
    {
        public const string Masculino = "M";
        public const string Feminino = "F";
    }

    public class TpEntidade
    {
        public const string EntidadeClasse = "E";
        public const string InstituicaoEnsino = "I";
    }

    public class TpCentroCusto
    {
        public const string Sintetico = "S";
        public const string Analitico = "A";
        public const string SubArea = "X";
    }

    /// <summary>
    /// Referencia ao tipo de documento (Tabela Documento)
    /// </summary>
    public class TpRefDoc
    {
        public const string Indicacao = "INDICACAO";

        public const string Inspetor = "INSPETOR";
        public const string Conselheiro = "CONSELHEIRO";
        public const string Nerp = "NERP";
        public const string NerpPagto = "NERP-PAGTO";
        public const string NerpAdiantamento = "NERP-ADIANTA";
        public const string Retencao = "RETENCAO";
        public const string Licenca = "LICENCA";
        public const string Evento = "EVENTO";
        public const string Solicitacao = "SOLICITACAO";
    }

    public class TpUnidade
    {
        public const string Assessoria = "ASSESSORIA";
        public const string Camara = "CÂMARA";
        public const string Comissao = "COMISSÃO";
        public const string Departamento = "DEPARTAMENTO";
        public const string DeptoRegional = "DEPARTAMENTO REGIONAL";
        public const string Divisao = "DIVISÃO";
        public const string Externo = "EXTERNO(A)";
        public const string GrupoTrabalho = "GRUPO DE TRABALHO";
        public const string NaoDefinido = "NÃO DEFINIDO";
        public const string Plenario = "PLENÁRIO";
        public const string Posto = "POSTO";
        public const string UGI = "UGI";
        public const string UGIeUOP = "UGI E UOP";
        public const string Unidade = "UNIDADE";
        public const string UOP = "UOP";
    }

    public class CdPrefixoContaContabil
    {
        public const string Previsao = "5";
        public const string Receita = "521";
        public const string ReceitaInicial = "5211";
        public const string ReceitaReformulacao = "5212";
        public const string Despesa = "522";
        public const string DespesaInicial = "5221";
        public const string DespesaReformulacao = "5222";
        public const string Executado = "6";
        public const string ReceitaExec = "621";
        public const string ReceitaInicialExec = "6211";
        public const string ReceitaReformulacaoExec = "6212";
        public const string DespesaExec = "622";
        public const string DespesaInicialExec = "6221";
        public const string DespesaReformulacaoExec = "6222";
    }

    public class StatusSolicitacao
    {
        public const string Cadastrada = "C";
        public const string AgAprovacaoADM = "ADM";
        public const string AgAprovacaoTEC = "TEC";
        public const string AgMaisInfosADM = "AI";
        public const string AgMaisInfosTEC = "TI";
        public const string AgCalcImposto = "IMP";
        public const string Aprovada = "A";
        public const string Recusada = "R";

        public static bool validaStatusSolicitacao(string status)
        {
            if (status == StatusSolicitacao.Cadastrada || status == StatusSolicitacao.AgAprovacaoADM ||
                status == StatusSolicitacao.AgAprovacaoTEC || status == StatusSolicitacao.AgMaisInfosADM || status == StatusSolicitacao.AgMaisInfosTEC ||
                status == StatusSolicitacao.AgCalcImposto || status == StatusSolicitacao.Aprovada || status == StatusSolicitacao.Recusada)
            {
                return true;
            }

            return false;
        }
    }    
}