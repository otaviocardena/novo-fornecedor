﻿using System;
using System.Web;
using Creasp.Nerp;
using Creasp.Siplen;
using NBox.Core;
using NBox.Services;
using Creasp.Elo;
using Creasp.Core.Services;

namespace Creasp.Core
{
    /// <summary>
    /// Implementa o acesso aos serviços relacionados ao banco de dados CREASP
    /// </summary>
    public class CreaspContext : ServiceContext
    {
        public int NrAnoBase { get; private set; }

        public CreaspContext() : base("creasp")
        {
            NrAnoBase = HttpContext.Current?.Request.Cookies["ano"]?.Value.To<int?>() ?? DateTime.Now.Year;
        }

        public CreaspContext(int nrAno) : base("creasp")
        {
            NrAnoBase = nrAno;
        }

        #region Usuario/Pessoa

        private Pessoa _usuario = null;

        public Pessoa Usuario
        {
            get
            {
                if (_usuario == null)
                {
                    var login = user?.Identity?.Name;

                    if (string.IsNullOrWhiteSpace(login)) return new Pessoa { NoPessoa = "Usuário não logado" };

                    _usuario = db.Query<Pessoa>()
                        .Where(x => x.NoLogin == login)
                        .FirstOrDefault();
                }

                return _usuario;
            }
        }

        #endregion

        #region CORE

        private CamaraService _camaras = null;
        private CentroCustoService _centroCustos = null;
        private CepService _ceps = null;
        private DocumentoService _documentos = null;
        private EmailService _emails = null;
        private EntidadeService _entidades = null;
        private GrupoAcessoService _grupoAcesso = null;
        private PessoaService _pessoas = null;
        private SyncService _sync = null;
        private TipoDocumentoService _tipoDocumentos = null;
        private UnidadeService _unidades = null;
        private ContaContabilService _contasContabeis = null;

        public CamaraService Camaras => _camaras ?? (_camaras = new CamaraService(this));
        public CentroCustoService CentroCustos => _centroCustos ?? (_centroCustos = new CentroCustoService(this));
        public CepService Ceps => _ceps ?? (_ceps = new CepService(this));
        public DocumentoService Documentos => _documentos ?? (_documentos = new DocumentoService(this));
        public EmailService Emails => _emails ?? (_emails = new EmailService(this));
        public EntidadeService Entidades => _entidades ?? (_entidades = new EntidadeService(this));
        public GrupoAcessoService GrupoAcesso => _grupoAcesso ?? (_grupoAcesso = new GrupoAcessoService(this));
        public PessoaService Pessoas => _pessoas ?? (_pessoas = new PessoaService(this));
        public SyncService Sync => _sync ?? (_sync = new SyncService(this));
        public TipoDocumentoService TipoDocumentos => _tipoDocumentos ?? (_tipoDocumentos = new TipoDocumentoService(this));
        public UnidadeService Unidades => _unidades ?? (_unidades = new UnidadeService(this));
        public ContaContabilService ContasContabeis => _contasContabeis ?? (_contasContabeis = new ContaContabilService(this));

        #endregion

        #region SIPLEN

        private CamaraRepService _camaraRep = null;
        private ComissaoService _comissoes = null;
        private ConselheiroService _conselheiros = null;
        private DiretoriaService _diretoria = null;
        private EventoService _eventos = null;
        private GrupoService _grupos = null;
        private IndicacaoService _indicacoes = null;
        private InspetoriaService _inspetorias = null;
        private InspetorService _inspetores = null;
        private LicencaService _licencas = null;
        private MandatoService _mandatos = null;
        private RelatorioService _relatorios = null;

        public CamaraRepService CamaraRep => _camaraRep ?? (_camaraRep = new CamaraRepService(this));
        public ComissaoService Comissoes => _comissoes ?? (_comissoes = new ComissaoService(this));
        public ConselheiroService Conselheiros => _conselheiros ?? (_conselheiros = new ConselheiroService(this));
        public DiretoriaService Diretoria => _diretoria ?? (_diretoria = new DiretoriaService(this));
        public EventoService Eventos => _eventos ?? (_eventos = new EventoService(this));
        public GrupoService Grupos => _grupos ?? (_grupos = new GrupoService(this));
        public IndicacaoService Indicacoes => _indicacoes ?? (_indicacoes = new IndicacaoService(this));
        public InspetoriaService Inspetorias => _inspetorias ?? (_inspetorias = new InspetoriaService(this));
        public InspetorService Inspetores => _inspetores ?? (_inspetores = new InspetorService(this));
        public LicencaService Licencas => _licencas ?? (_licencas = new LicencaService(this));
        public MandatoService Mandatos => _mandatos ?? (_mandatos = new MandatoService(this));
        public RelatorioService Relatorios => _relatorios ?? (_relatorios = new RelatorioService(this));

        #endregion

        #region NERP

        private NerpService _nerps = null;
        private DespesaService _despesas = null;

        public NerpService Nerps => _nerps ?? (_nerps = new NerpService(this));
        public DespesaService Despesas => _despesas ?? (_despesas = new DespesaService(this));

        #endregion

        #region ELO

        private PropostaOrcamentariaDespesaService _propostasOrcamentarias = null;
        private OrcamentoService _dotacoesOrcamentarias = null;
        private CentroCustoResponsavelService _centrosCustosResponsaveis = null;
        private StatusPropostaOrcamentariaService _statusPropostasOrcamentarias = null;
        private MetodologiaReceitaService _metodologiasReceita = null;
        private ParametroAnoService _parametrosAnos = null;
        private ReformulacaoService _reformulacoesAnos = null;
        private RelatoriosService _relatoriosElo = null;
        private CentroCustoPerfilService _centrosCustosPerfis = null;
        private ContaContabilMapeamentoService _contasContabeisMapeamentos = null;

        public PropostaOrcamentariaDespesaService PropostasOrcamentarias => _propostasOrcamentarias ?? (_propostasOrcamentarias = new PropostaOrcamentariaDespesaService(this));
        public OrcamentoService DotacoesOrcamentarias => _dotacoesOrcamentarias ?? (_dotacoesOrcamentarias = new OrcamentoService(this));
        public CentroCustoResponsavelService CentrosCustosResponsaveis => _centrosCustosResponsaveis ?? (_centrosCustosResponsaveis = new CentroCustoResponsavelService(this));
        public StatusPropostaOrcamentariaService StatusPropostasOrcamentarias => _statusPropostasOrcamentarias ?? (_statusPropostasOrcamentarias = new StatusPropostaOrcamentariaService(this));
        public MetodologiaReceitaService MetodologiasReceita => _metodologiasReceita ?? (_metodologiasReceita = new MetodologiaReceitaService(this));
        public ParametroAnoService ParametroAno => _parametrosAnos ?? (_parametrosAnos = new ParametroAnoService(this));
        public ReformulacaoService Reformulacoes => _reformulacoesAnos ?? (_reformulacoesAnos = new ReformulacaoService(this));
        public RelatoriosService RelatoriosElo => _relatoriosElo ?? (_relatoriosElo = new RelatoriosService(this));
        public CentroCustoPerfilService CentrosCustosPerfis => _centrosCustosPerfis ?? (_centrosCustosPerfis = new CentroCustoPerfilService(this));
        public ContaContabilMapeamentoService ContasContabeisMapeamentos => _contasContabeisMapeamentos ?? (_contasContabeisMapeamentos = new ContaContabilMapeamentoService(this));

        #endregion

        #region SOLICITACAO
        private SolicitacaoService _solicitacao = null;

        public SolicitacaoService Solicitacao => _solicitacao ?? (_solicitacao = new SolicitacaoService(this));
        #endregion

        #region FORNECEDOR
        private FornecedorService _fornecedor = null;

        public FornecedorService Fornecedor => _fornecedor ?? (_fornecedor = new FornecedorService(this));
        #endregion

    }
}