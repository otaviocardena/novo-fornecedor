﻿using NBox.Core;
using NBox.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Core
{
    public class Tabelas : BaseTask
    {
        public override int Run(CreaspContext context)
        {
            Console.WriteLine("### INICIANDO O SINCRONISMO DE TABELAS ###");

            Factory.Register<IAuditWriter>(() => new AuditWriter());

            var syncService = new SyncService(context);
            StringBuilder sb = new StringBuilder();
            syncService.Run(sb);

            Console.WriteLine(sb.ToString());
            Console.WriteLine("### SINCRONISMO DE TABELAS FINALIZADO ###");

            return 1;
        }
    }
}
