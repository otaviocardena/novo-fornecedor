﻿using NBox.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Core
{
    public class Pessoas : BaseTask
    {
        public override int Run(CreaspContext context)
        {
            Console.WriteLine("### INICIANDO O SINCRONISMO DE PESSOAS ###");

            var db = new CreaspContext();

            Console.WriteLine("Buscando todas as pessoas com número de creasp preenchido...");
            var pessoas = db.Query<Pessoa>()
                .Where(x => x.NrCreasp != null)
                .OrderBy(x => x.IdPessoa)
                .ToList();

            Console.WriteLine(string.Format("{0} pessoas encontradas.", pessoas.Count));
            if (pessoas.Count == 0) return 0;

            int pessoasAtualizadas = 0;

            foreach (var p in pessoas)
            {
                try
                {
                    if (db.Pessoas.AtualizaCadastro(p))
                    {
                        Console.WriteLine(string.Format("ATUALIZADO: {0}.", p.NoPessoa));
                        pessoasAtualizadas++;
                    }
                    else
                    {
                        Console.WriteLine(string.Format("NÃO ATUALIZADO: {0}.", p.NoPessoa));
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(string.Format("ERRO ao atualizar {0}: {1}", p.NoPessoa, ex.Message));
                }
            }

            Console.WriteLine("### SINCRONISMO DE PESSOAS FINALIZADO ###");

            return pessoasAtualizadas;
        }
    }
}
