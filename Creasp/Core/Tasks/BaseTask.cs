﻿using Creasp.Core.Utils;
using dotless.Core.Loggers;
using Microsoft.Build.Framework;
using NBox.Core;
using NBox.Services;
using NPoco;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Creasp.Core
{
    public abstract class BaseTask : ITask
    {
        public IBuildEngine BuildEngine { get; set; }
        public ITaskHost HostObject { get; set; }

        [Required]
        public string AppConfig { get; set; }

        public abstract int Run(CreaspContext context);

        public bool Execute()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("pt-BR");
            using (Core.AppConfig.Change(AppConfig))
            {
                return Start();
            }
        }

        public bool Start()
        {
            try
            {
                Console.WriteLine("### CONFIGURANDO EXECUÇÃO DE SINCRONISMOS ###");

                Factory.Register<IDatabase>(() => new CreaspDatabase());
                Factory.Register<ICache>(() => new ConsoleCache(TimeSpan.FromMinutes(1000))); 
                Factory.Register<IAuditWriter>(() => new AuditWriter()); 

                Factory.Register<ICreanet>(() => new CreanetService());
                Factory.Register<ISiscont>(() => new SiscontService());
                Factory.Register<ISenior>(() => new SeniorService());

                int? ano = null; //Deveria ser um parâmetro.

                //Se não houver ano por parâmetro, coloca o ano atual.
                if (!ano.HasValue)
                {
                    ano = DateTime.Now.Year;
                }

                CreaspContext context = new CreaspContext(ano.Value);

                this.Run(context);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
