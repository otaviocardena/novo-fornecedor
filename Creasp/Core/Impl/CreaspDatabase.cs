﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using NBox.Services;
using System.Configuration;
using NPoco;

namespace Creasp.Core
{
    /// <summary>
    /// Classe que faz a gestão da organização dos arquivos de armazenamento de arquivos
    /// </summary>
    public class CreaspDatabase : SqlServer
    {
        public CreaspDatabase() : base("creasp")
        {
#if DEBUG
            this.CommandDebug += (s, sql) => System.Diagnostics.Debug.Print("---------------------------------------------\n" + sql);
#endif
        }

        protected override bool OnInserting(InsertContext insertContext)
        {
            var entity = insertContext.Poco as IAuditoria;

            if (entity != null)
            {
                entity.DtCadastro = DateTime.Now;
                entity.LoginCadastro = Auth.Current<CreaspUser>()?.Login ?? "sistema";
            }

            return base.OnInserting(insertContext);
        }

        protected override bool OnUpdating(UpdateContext updateContext)
        {
            var entity = updateContext.Poco as IAuditoria;

            if (entity != null)
            {
                entity.DtAlteracao = DateTime.Now;
                entity.LoginAlteracao = Auth.Current<CreaspUser>()?.Login ?? "sistema";
            }

            return base.OnUpdating(updateContext);
        }
    }
}