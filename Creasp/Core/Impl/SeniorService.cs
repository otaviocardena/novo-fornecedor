﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Xml;
using Creasp;
using Creasp.Nerp;
using Creasp.Siscont.Despesa;
using NBox.Core;
using NBox.Services;
using NPoco;
using Creasp.Elo;

namespace Creasp.Core
{
    /// <summary>
    /// Implementa as chamadas de WebSrvice na SENIOR. Como a senior usa Java?Ruby? a classe proxy não funciona. Precisa enviar SOAP manual
    /// </summary>
    public class SeniorService : ISenior
    {
        private static ICache _cache = Factory.Get<ICache>();

        private static string _urlBuscarPessoas;
        private static string _urlBuscarSubordinados;
        private static string _urlBuscarOrcamento;
        private static string _username;
        private static string _password;
        private static int _tipoHierarquia;

        static SeniorService()
        {
            _username = ConfigurationManager.AppSettings["senior.username"];
            _password = ConfigurationManager.AppSettings["senior.password"];
            _tipoHierarquia = ConfigurationManager.AppSettings["senior.tipohierarquia"].To<int>();

            _urlBuscarPessoas = ConfigurationManager.AppSettings["senior.BuscarPessoas"];
            _urlBuscarSubordinados = ConfigurationManager.AppSettings["senior.BuscarSubordinados"];
            _urlBuscarOrcamento = ConfigurationManager.AppSettings["senior.BuscarOrcamento"];
        }

        /// <summary>
        /// Faz a busca por funcionarios no sistema da SENIOR
        /// </summary>
        public IEnumerable<FuncionarioSenior> BuscaFuncionarios(int? matricula, string cpf, string nome)
        {
            // Permite utilizar arquivo de MOCK junto com dados de produção (PRESIDENTE)
            if (matricula != null || cpf != null)
            {
                var db = Factory.Get<IDatabase>();

                var func = db.Query<FuncionarioSenior>()
                    .Where(matricula != null, x => x.NrMatricula == matricula)
                    .Where(cpf != null, x => x.NrCpf == cpf)
                    .FirstOrDefault();

                if (func != null) return new FuncionarioSenior[] { func };
            }

            return _cache.Get("func-senior-" + matricula + "-" + cpf + "-" + nome, () =>
            {
                var pars = new StringBuilder();

                if (matricula != null) pars.AppendFormat("<matricula>{0}</matricula>", matricula);
                if (cpf != null) pars.AppendFormat("<CPF>{0}</CPF>", cpf);
                if (nome != null) pars.AppendFormat("<nome>{0}</nome>", nome);
                pars.AppendFormat("<tipoHierarquia>{0}</tipoHierarquia>", _tipoHierarquia);

                var res = Execute(_urlBuscarPessoas, "BuscarPessoas", pars.ToString());

                var result = new List<FuncionarioSenior>();
                var pessoas = res.SelectNodes("pessoas");

                foreach (XmlNode pessoa in pessoas)
                {
                    var CPF = pessoa.SelectSingleNode("CPF").InnerText;
                    var centroCusto = pessoa.SelectSingleNode("centroCusto").InnerText.To<string>() ?? "";
                    var cpfChefeImediato = pessoa.SelectSingleNode("cpfChefeImediato").InnerText.To<string>();
                    var cpfGerente = pessoa.SelectSingleNode("cpfGerente").InnerText.To<string>();
                    var cpfSuperIntendente = pessoa.SelectSingleNode("cpfSuperIntendente").InnerText.To<string>();
                    var descricaoCargo = pessoa.SelectSingleNode("descricaoCargo").InnerText;
                    //var endereco = pessoa.SelectSingleNode("endereco").InnerText;
                    var matricul = pessoa.SelectSingleNode("matricula").InnerText;
                    var nom = pessoa.SelectSingleNode("nome").InnerText;
                    var nomeChefeImediato = pessoa.SelectSingleNode("nomeChefeImediato").InnerText.To<string>();
                    var nomeGerente = pessoa.SelectSingleNode("nomeGerente").InnerText.To<string>();
                    var nomeSuperIntendente = pessoa.SelectSingleNode("nomeSuperIntendente").InnerText.To<string>();

                    // pode vir cpfXXXXXX = "0" ou nome em branco, limpa o cpf
                    if (cpfChefeImediato == "0" || nomeChefeImediato == null) cpfChefeImediato = null;
                    if (cpfGerente == "0" || nomeGerente == null) cpfGerente = null;
                    if (cpfSuperIntendente == "0" || nomeSuperIntendente == null) cpfSuperIntendente = null;

                    result.Add(new FuncionarioSenior
                    {
                        NrMatricula = matricul.To<int>(),
                        NoCargo = descricaoCargo,
                        NrCpf = CPF.PadLeft(11, '0').UnMask(),
                        NoFuncionario = nom,
                        NrCentroCusto = string.Join("", centroCusto.Split('.').Select(c => c.PadLeft(2, '0'))),
                        NrCpfChefe = (cpfChefeImediato ?? CPF).PadLeft(11, '0').UnMask(),
                        NrCpfGerente = (cpfGerente ?? CPF).PadLeft(11, '0').UnMask(),
                        NrCpfSuperintendente = (cpfSuperIntendente ?? cpfChefeImediato ?? CPF).PadLeft(11, '0').UnMask(),
                        NoChefe = nomeChefeImediato ?? nom,
                        NoGerente = nomeGerente ?? nom,
                        NoSuperintendente = nomeSuperIntendente ?? nomeChefeImediato ?? nom
                    });
                }

                // garante que retorna somente 1 CPF/Matricula, pegando a matricula mais alta
                var matriculas = result.GroupBy(x => x.NrCpf).Select(x => x.Max(m => m.NrMatricula));

                return result.Where(x => matriculas.Contains(x.NrMatricula)).ToList();
            });
        }

        /// <summary>
        /// Retorna todos os CPF subordinados a uma pessoa
        /// </summary>
        public IEnumerable<string> BuscaSubordinados(string cpf)
        {
            var pars = new StringBuilder();

            pars.AppendFormat("<numeroCpf>{0}</numeroCpf>", cpf);
            pars.AppendFormat("<tipoHierarquia>{0}</tipoHierarquia>", _tipoHierarquia);

            var res = Execute(_urlBuscarSubordinados, "BuscarSubordinados", pars.ToString());

            var result = new List<FuncionarioSenior>();
            var subordinados = res.SelectNodes("subordinados");

            foreach (XmlNode pessoa in subordinados)
            {
                var nr = pessoa.SelectSingleNode("CPF").InnerText.UnMask().To<string>()?.PadLeft(11, '0');

                if (nr != null) yield return nr;
            }
        }

        public IEnumerable<OrcamentoSenior> BuscaOrcamento(int nrAno)
        {
            var orcamentosSenior = new List<OrcamentoSenior>();

            var pars = new StringBuilder();

            pars.AppendFormat("<ano>{0}</ano>", nrAno);

            var resposta = Execute(_urlBuscarOrcamento, "BuscarOrcamento", pars.ToString());
            var nodos = resposta.SelectNodes("retorno");

            foreach (XmlNode orcamento in nodos)
            {
                var orcamentoSenior = new OrcamentoSenior()
                {
                    NrAno = nrAno,
                    NrCentroCusto = orcamento.SelectSingleNode("codCentroCusto").InnerText.UnMask().To<string>(),
                    CdContaContabilResumido = orcamento.SelectSingleNode("codContaContabil").InnerText.UnMask().To<int>(),
                    VrOrcamento = orcamento.SelectSingleNode("vrOrcamento").InnerText.Replace(".", ",").To<decimal>() //TODO: Fazer conversão corretamente.
                };

                orcamentosSenior.Add(orcamentoSenior);
            }

            return orcamentosSenior;
        }

        public long Ping()
        {
            var stop = new Stopwatch();
            stop.Start();
            BuscaFuncionarios(new Random(DateTime.Now.Second).Next(80000, 100000), null, null);
            return stop.ElapsedMilliseconds;
        }

        #region Chamada WS manual

        private static XmlElement Execute(string endpoint, string service, string xmlParameters)
        {
            // faz cache do resultado
            return _cache.Get("senior-" + Converter.MD5(service + xmlParameters), () =>
            {
                var request = (HttpWebRequest)WebRequest.Create(endpoint);
                request.Headers.Add("SOAP:Action");
                request.ContentType = "text/xml;charset=\"utf-8\"";
                request.Accept = "text/xml";
                request.Method = "POST";

                var body = new XmlDocument();
                body.LoadXml(string.Format(@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" xmlns:ser=""http://services.senior.com.br"">
                <soapenv:Header />
                    <soapenv:Body>
                        <ser:{0}>
                            <user>{1}</user>
                            <password>{2}</password>
                            <encryption>0</encryption>
                            <parameters>
                                {3}
                            </parameters>
                        </ser:{0}>
                    </soapenv:Body>
                </soapenv:Envelope>", service, _username, _password, xmlParameters));

                using (var stream = request.GetRequestStream())
                {
                    body.Save(stream);
                }

                using (var response = request.GetResponse())
                {
                    using (var reader = new StreamReader(response.GetResponseStream()))
                    {
                        var soapResult = reader.ReadToEnd();
                        var xmlres = new XmlDocument();
                        xmlres.LoadXml(soapResult);

                        var result = xmlres.SelectSingleNode("//result") as XmlElement;
                        var erro = result.SelectSingleNode("erroExecucao").InnerText;

                        if (!string.IsNullOrEmpty(erro))
                        {
                            throw new NBoxException(erro);
                        }

                        return result;
                    }
                }
            });
        }

        #endregion
    }
}