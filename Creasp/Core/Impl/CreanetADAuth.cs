﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using Creasp.Core.Model;
using NBox.Core;
using NBox.Services;
using NPoco;

namespace Creasp.Core
{
    public class CreanetADAuth : IAuth
    {
        private Creanet.ServiceCreanetClient _client = new Creanet.ServiceCreanetClient();

        public IUser Authentication(string login, string password)
        {
            var passMD5 = Converter.MD5(password);

            var db = Factory.Get<IDatabase>();
            //var loginFornecedor = db.Query<LoginFornecedor>().FirstOrDefault(l => l.Login.ToUpper() == login.ToUpper() && l.Senha == passMD5);

            if (login == "admin")
            {
                var adminPassword = ConfigurationManager.AppSettings["admin.password"] ?? "123";

                if (password != adminPassword) throw new NBoxException("Usuário/Senha inválido");

                return new CreaspUser("admin", "Administrador", "admin@numeria.com.br", 1, new string[] { "*" }, new string[] {"*"}, false, "ALL.USUARIO");
            }
            else if (login == "tempFornecedor")
            {
                return new CreaspUser("fornecedor", "Fornecedor", "admin@numeria.com.br", 1, new string[] { "*" }, new string[] { "*" }, true, "ALL.TEMP.FORNECEDOR");
            }
            //else if (loginFornecedor != null)
            //{
            //    var pessoa = db.Query<Pessoa>().FirstOrDefault(l => l.NoLogin.ToUpper() == loginFornecedor.Login.ToUpper());
            //    var nome = pessoa.NoPessoa.Length > 20 ? pessoa.NoPessoa.Substring(0, 19) : pessoa.NoPessoa;
            //    return new CreaspUser(nome, pessoa.NoPessoa, pessoa.NoEmail, pessoa.IdPessoa, new string[] { "*" }, new string[] { "*" }, true, "ALL.FORNECEDOR");
            //}
            else
            {
                var user = _client.ValidaLogin(login, password);

                if (user == null) throw new NBoxException("Usuário/Senha inválido");

                // os grupos vem em string com '
                var grupos = user.GruposDeAcesso?.Split(',').Select(x => x.ToUpper().Replace("'", "")).ToList() ?? new List<string>();
                var recursos = new List<string>();

                if (grupos.Count == 0) throw new NBoxException("Este usuário não possui nenhum grupo");

                // busca no banco de dados os recursos associados aos grupos do usuario
                grupos.ForEach((g, i) => recursos.AddRange(db.Query<GrupoAcesso>().Where(x => x.CdGrupo == g).ToEnumerable().Select(x => x.CdRecurso)));

                // pode ser nulo (ainda não tem usuario vinculado)
                var pessoa = db.FirstOrDefault<Pessoa>(x => x.NoLogin == login);

                // atualiza o email conforme AD
                if (pessoa != null && pessoa.NoEmail != user.Email)
                {
                    pessoa.NoEmail = user.Email;
                    db.Update(pessoa);
                }

                return new CreaspUser(login, user.Nome, user.Email, pessoa?.IdPessoa ?? 0, recursos.Distinct(), grupos, false);
            }
        }

        public bool Authorization(IUser user, string resource)
        {
            throw new NotImplementedException();
        }
    }
}