﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using NBox.Services;
using System.Configuration;
using System.IO;

namespace Creasp.Core
{
    /// <summary>
    /// Classe que faz a gestão da organização dos arquivos de armazenamento de arquivos
    /// </summary>
    public class CreaspStorage : FileStorage
    {
        public CreaspStorage() : 
            base(ConfigurationManager.AppSettings["creasp.storage"], (root, container) =>
            {
                var path = Path.Combine(root, container + ".fdb");
                return path;
            })
        {
            
        }
    }
}