﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using Creasp.Nerp;
using NBox.Core;
using NBox.Services;
using Newtonsoft.Json;
using NPoco;
using Creasp.Elo;
using Creasp.Siscont.Despesa;

namespace Creasp.Core
{
    public class SiscontService : ISiscont
    {
        private Siscont.Cadastro.CadastroClient _cadastro = new Siscont.Cadastro.CadastroClient();
        private Siscont.Contabilidade.ContabilidadeClient _contabilidade = new Siscont.Contabilidade.ContabilidadeClient();
        private Siscont.Despesa.DespesaClient _despesa = new Siscont.Despesa.DespesaClient();
        private Siscont.Orcamento.OrcamentoClient _orcamento = new Siscont.Orcamento.OrcamentoClient();

        private ICache _cache = Factory.Get<ICache>();
        private AuditLog _log = new AuditLog();

        public SiscontService()
        {
            var username = ConfigurationManager.AppSettings["siscont.username"];
            var password = ConfigurationManager.AppSettings["siscont.password"];

            _cadastro.ClientCredentials.UserName.UserName = username;
            _contabilidade.ClientCredentials.UserName.UserName = username;
            _despesa.ClientCredentials.UserName.UserName = username;
            _orcamento.ClientCredentials.UserName.UserName = username;

            _cadastro.ClientCredentials.UserName.Password = password;
            _contabilidade.ClientCredentials.UserName.Password = password;
            _despesa.ClientCredentials.UserName.Password = password;
            _orcamento.ClientCredentials.UserName.Password = password;
        }

        public long Ping()
        {
            var stop = new Stopwatch();
            stop.Start();
            _cadastro.ConsultarPessoasDadosBancarios(new Siscont.Cadastro.PessoasFilterEntity { CPFCNPJ = "05.371.908-0001/25", TipoPessoa = Siscont.Cadastro.PessoaTipo.Juridica });
            return stop.ElapsedMilliseconds;
        }

        #region Pessoa

        public IEnumerable<Pessoa> BuscaPessoas(string cpf, string nome)
        {

            var f = new Siscont.Cadastro.PessoasFilterEntity();

            if (!string.IsNullOrEmpty(cpf)) f.CPFCNPJ = Pessoa.FormataCpf(cpf);
            if (!string.IsNullOrEmpty(nome)) f.NomeRazaoSocial = nome;

            return _cadastro.ConsultarPessoasDadosBancarios(f)
                .Select(x => new Pessoa
                {
                    NrCpf = x.CPFCNPJ.UnMask(),
                    NoPessoa = x.NomeRazaoSocial,
                    DadosBancarios = x.BancoCodigoCompensacao == null ? null : new DadosBancarios
                    {
                        NrBanco = x.BancoCodigoCompensacao,
                        NoBanco = x.BancoNome,
                        NrAgencia = x.Agencia,
                        NrAgenciaDv = x.AgenciaDV,
                        NrConta = x.ContaNumero,
                        NrContaDv = x.ContaDV,
                        FgContaCorrente = x.ContaCorrente
                    }
                });
        }

        public void IncluirOuAlteraPessoa(Pessoa pessoa)
        {

            var p = new Siscont.Cadastro.PessoasEntity
            {
                CPFCNPJ = pessoa.NrCpfFmt,
                NomeRazaoSocial = pessoa.NoPessoa
            };

            if (pessoa.DadosBancarios != null)
            {
                p.BancoCodigoCompensacao = pessoa.DadosBancarios.NrBanco;
                p.Agencia = pessoa.DadosBancarios.NrAgencia;
                p.AgenciaDV = pessoa.DadosBancarios.NrAgenciaDv;
                p.ContaNumero = pessoa.DadosBancarios.NrConta;
                p.ContaDV = pessoa.DadosBancarios.NrContaDv;
                p.ContaCorrente = pessoa.DadosBancarios.FgContaCorrente;
                p.TipoPessoa = p.CPFCNPJ.Length == 14? Siscont.Cadastro.PessoaTipo.Fisica : p.CPFCNPJ.Length == 18 ? Siscont.Cadastro.PessoaTipo.Juridica : Siscont.Cadastro.PessoaTipo.NaoInformado;
            }

            var ret = _cadastro.IncluirAlterarPessoasDadosBancarios(p);

            if (ret.Erro)
            {
                throw new NBoxException("Não foi possivel atualizar dados no SISCONT. " + ret.Mensagens.First()?.Texto);
            }
        }

        #endregion

        #region Tabelas

        public IEnumerable<CentroCusto> ListaCentroCusto(int nrAno)
        {

            var ccs = _despesa.ConsultarCentroCusto(new Siscont.Despesa.CentroCustoFilterEntity { Exercicio = nrAno });

            foreach (var cc in ccs)
            {
                var centrocusto = new CentroCusto
                {
                    IdSiscont = cc.Identificador,
                    NrAno = cc.Exercicio,
                    NrAnoTerminoRestringir = cc.AnoTerminoRestringir,
                    NrCentroCusto = cc.Codigo.UnMask(),
                    NoCentroCusto = cc.Nome,
                    TpCentroCusto = cc.TipoCentroCusto == Siscont.Despesa.CentroCustoTipo.Analitico ? TpCentroCusto.Analitico : TpCentroCusto.Sintetico,
                    FgAtivo = true,
                    FgRestringirFaseOrcamentaria = cc.RestringirFaseOrcamentaria
                };

                if (centrocusto.NrCentroCustoFmt != cc.Codigo) throw new NBoxException("Erro no formato do centro de custo: " + cc.Codigo);

                yield return centrocusto;

                foreach (var area in cc.SubAreas)
                {
                    yield return new CentroCusto
                    {
                        NrAno = cc.Exercicio,
                        IdSiscont = cc.Identificador,
                        NrCentroCusto = cc.Codigo.UnMask() + area.Codigo.UnMask(),
                        NoCentroCusto = area.Nome,
                        TpCentroCusto = TpCentroCusto.SubArea,
                        FgAtivo = true
                    };
                }
            }
        }

        public IEnumerable<ContaContabil> ListaPlanoContas(int nrAno)
        {

            var db = Factory.Get<IDatabase>();

            // monto a estrutura minima (q não muda) que o SISCONT não retorna (pois solicito por prefixo)
            var internas = new List<ContaContabil>
            {
                new ContaContabil { NrAno = nrAno, NrContaContabil = "1", NoContaContabil = "ATIVO" },
                new ContaContabil { NrAno = nrAno, NrContaContabil = "11", NoContaContabil = "ATIVO CIRCULANTE" },
                new ContaContabil { NrAno = nrAno, NrContaContabil = "111" },
                new ContaContabil { NrAno = nrAno, NrContaContabil = "113" },

                new ContaContabil { NrAno = nrAno, NrContaContabil = "2", NoContaContabil = "PASSIVO E PATRIMÔNIO LÍQUIDO" },
                new ContaContabil { NrAno = nrAno, NrContaContabil = "21", NoContaContabil = "PASSIVO CIRCULANTE" },
                new ContaContabil { NrAno = nrAno, NrContaContabil = "213" },

                new ContaContabil { NrAno = nrAno, NrContaContabil = "5", NoContaContabil = "CONTROLES DE APROVAÇÃO DO PLANEJAMENTO E ORÇAMENTO" },
                new ContaContabil { NrAno = nrAno, NrContaContabil = "51", NoContaContabil = "PLANEJAMENTO APROVADO" },
                new ContaContabil { NrAno = nrAno, NrContaContabil = "511" },

                new ContaContabil { NrAno = nrAno, NrContaContabil = "52", NoContaContabil = "ORÇAMENTO APROVADO" },
                new ContaContabil { NrAno = nrAno, NrContaContabil = "521" },
                new ContaContabil { NrAno = nrAno, NrContaContabil = "522" },

                new ContaContabil { NrAno = nrAno, NrContaContabil = "53", NoContaContabil = "INSCRIÇÃO DE RESTOS A PAGAR" },
                new ContaContabil { NrAno = nrAno, NrContaContabil = "531" },

                new ContaContabil { NrAno = nrAno, NrContaContabil = "6", NoContaContabil = "CONTROLES DA EXECUÇÃO DO PLANEJAMENTO E ORÇAMENTO" },
                new ContaContabil { NrAno = nrAno, NrContaContabil = "61", NoContaContabil = "EXECUÇÃO DO PLANEJAMENTO" },
                new ContaContabil { NrAno = nrAno, NrContaContabil = "611" },

                new ContaContabil { NrAno = nrAno, NrContaContabil = "62", NoContaContabil = "EXECUÇÃO DO ORÇAMENTO" },
                new ContaContabil { NrAno = nrAno, NrContaContabil = "621" },
                new ContaContabil { NrAno = nrAno, NrContaContabil = "622" },

                new ContaContabil { NrAno = nrAno, NrContaContabil = "63", NoContaContabil = "EXECUÇÃO DE RESTOS A PAGAR" },
                new ContaContabil { NrAno = nrAno, NrContaContabil = "631" },
                new ContaContabil { NrAno = nrAno, NrContaContabil = "632" }
            };

            // adicionar apenas os niveis 1 e 2
            var contas = new List<ContaContabil>(internas.Where(x => x.NrContaContabil.Length != 3));

            // demais niveis vem pelo webservice da implanta
            foreach (var interna in internas.Where(x => x.NrContaContabil.Length == 3))
            {
                contas.AddRange(ListaPlanoContas(interna.NrContaContabilFmt, nrAno));
            }

            return contas;
        }

        private IEnumerable<ContaContabil> ListaPlanoContas(string prefixo, int nrAno)
        {
            foreach (var conta in _contabilidade.ConsultarPlanoConta(new Siscont.Contabilidade.PlanoContasFilterEntity { Exercicio = nrAno, Prefixo = prefixo }))
            {
                var cc = new ContaContabil
                {
                    NrAno = conta.Exercicio,
                    NrContaContabil = conta.Codigo.UnMask(),
                    NoContaContabil = conta.Nome,
                    CdResumido = conta.CodigoResumido,
                    FgAnalitico = conta.TipoPlanoConta == Siscont.Contabilidade.PlanoContaTipo.Analitica,
                    FgAtivo = true
                };

                // faz um teste na formatação do numero da conta - gera erro aqui se for o caso
                if (cc.NrContaContabilFmt != conta.Codigo) throw new ApplicationException("Formatação da conta contabil inválida: " + conta.Codigo);

                yield return cc;
            }
        }

        public IEnumerable<Tributo> ListaTributos()
        {

            return _cache.Get("tributos", () =>
            {
                return _despesa.ConsultarTributos(new Siscont.Despesa.TributosFilterEntity { Exercicio = DateTime.Now.Year })
                    .Select(x => new Tributo
                    {
                        CdTributo = x.Codigo.To<string>() ?? NBox.Core.Converter.MD5(x.Nome).Substring(0, 10),
                        NoTributo = x.Nome
                    })
                    .ToList();
            });
        }

        #endregion

        #region Orcamento
        public IEnumerable<DotacaoOrcamentaria> ListaDotacaoOrcamentaria(int nrAno)
        {
            var dados = _orcamento.ConsultarDotacaoOrcamentaria(new Siscont.Orcamento.DotacoesOrcamentariasFilterEntity() { Exercicio = nrAno })
                .Select(x => new DotacaoOrcamentaria
                {
                    NrAno = x.Exercicio,
                    NrCentroCusto = x.CodigoCentroCusto.UnMask().To<string>(),
                    NrContaContabil6 = x.CodigoContaContabil.UnMask().To<string>(),
                    FgReceita = x.Receita,
                    VrAprovado = x.Valor,
                    FgAtivo = true
                }
                );

            return dados;
        }

        public IEnumerable<ContratoOrcamentario> ListaContratos()
        {
            var contratos = _despesa.ConsultarContratos(new Siscont.Despesa.ContratosFilterEntity() { }); //TODO: classe de filtro foi alterada, por enquanto não usamos este sincronismo.
            var contratosOrcamentarios = new List<ContratoOrcamentario>();
            ContratoOrcamentario contratoOrcamenario;

            foreach (var c in contratos)
            {
                foreach (var e in c.Empenhos)
                {
                    foreach (var centro in e.DistribuicaoCentroCusto)
                    {
                        contratoOrcamenario = new ContratoOrcamentario();

                        contratoOrcamenario.NrCpfCnpjContratado = c.CPFCPJContratado;
                        contratoOrcamenario.DtVigenciaInicio = c.DataInicioVigencia;
                        contratoOrcamenario.DtVigenciaFim = c.DataFinalVigencia;
                        contratoOrcamenario.NrGestorCpf = c.GestorCPF;
                        contratoOrcamenario.NrContrato = c.Numero.ToString(); ;
                        contratoOrcamenario.NrAditivo = c.NumeroAditivo.ToString();
                        contratoOrcamenario.NrProcesso = c.NumeroProcesso;
                        contratoOrcamenario.NoObjeto = c.Objeto;
                        contratoOrcamenario.VrContrato = c.ValorContrato;
                        contratoOrcamenario.NoRazaoSocialContratado = c.NomeRazaoSociaContratado;
                        contratoOrcamenario.VrIndiceReajuste = c.IndiceReajuste;

                        contratoOrcamenario.NrContaContabil = e.Numero.ToString();

                        contratoOrcamenario.NrCentroCusto = centro.CentroCustoCodigo;
                        contratoOrcamenario.VrAnulado = centro.ValorAnulado;
                        contratoOrcamenario.VrEmpenhado = centro.ValorEmpenhado;
                        contratoOrcamenario.VrLiquidado = centro.ValorLiquidado;
                        contratoOrcamenario.VrPago = centro.ValorPago;

                        contratosOrcamentarios.Add(contratoOrcamenario);
                    }
                }
            }

            return contratosOrcamentarios;
        }

        public IEnumerable<PosicaoAtualOrcamento> ListaPosicaoAtualOrcamento(List<string> contasContabeis, int nrAno)
        {
            var filtro = new Siscont.Orcamento.PosicaoAtualOrcamentoFilterEntity();

            var dataAtual = DateTime.Now;

            if (nrAno == dataAtual.Year)
            {
                filtro.DataReferencia = dataAtual.Date;
            }
            else if (nrAno < dataAtual.Year)
            {
                filtro.DataReferencia = new DateTime(nrAno, 12, 31);
            }
            else
            {
                return new List<PosicaoAtualOrcamento>();
            }

            var orcamentoAtual = new List<PosicaoAtualOrcamento>();

            foreach (var conta in contasContabeis)
            {
                filtro.ContaContabil = new ContaContabil() { NrContaContabil = conta }.NrContaContabilFmt;
                orcamentoAtual.AddRange(_orcamento.ConsultarPosicaoAtualOrcamento(filtro)
                    .Select(x => new PosicaoAtualOrcamento()
                    {
                        NrAno = filtro.DataReferencia.Year,
                        NrCentroCusto = x.CodigoCentroCusto?.UnMask(),
                        NrContaContabil6 = x.CodigoConta?.UnMask(),
                        FgReceita = false,
                        DtRefVrExecutado = filtro.DataReferencia,
                        VrEmpDataRef = x.ValorEmpenhadoDataReferencia,
                        VrEmpExercicio = x.ValorEmpenhadoExercicio,
                        VrExecutado = x.ValorEmpenhadoDataReferencia, //??
                        VrLiqDataRef = x.ValorLiquidadoDataReferencia,
                        VrLiqExercicio = x.ValorLiquidadoExercicio,
                        VrPagoDataRef = x.ValorPagoDataReferencia,
                        VrPagoExercicio = x.ValorPagoExercicio,
                        VrPreEmpDataRef = x.ValorPreEmpenhadoDataReferencia,
                        VrPreEmpExercicio = x.ValorPreEmpenhadoExercicio,
                        VrSaldoALiqDataRef = x.SaldoALiquidarDataReferencia,
                        VrSaldoALiqExercicio = x.SaldoALiquidarExercicio,
                        VrSaldoAPagarDataRef = x.SaldoAPagarDataReferencia,
                        VrSaldoAPagarExercicio = x.SaldoAPagarExercicio,
                        VrSaldoOrcamentarioDataRef = x.SaldoOrcamentarioDataReferencia,
                        VrSaldoOrcamentarioExercicio = x.SaldoOrcamentarioExercicio,
                    }
                ));
            }

            return orcamentoAtual;
        }

        public IEnumerable<DisponibilidadeAtualOrcamento> ListaDisponibilidadeOrcamento(int nrAno)
        {
            var dataAtual = DateTime.Now;

            var filtro = new Siscont.Contabilidade.OrcamentoAtualReceitaFilterEntity()
            {
                SomenteContasAnaliticas = true,
                SomenteContasComMovimento = false
            };

            filtro.DataInicio = new DateTime(nrAno, 1, 1);

            if (nrAno == dataAtual.Year)
            {
                filtro.DataFim = dataAtual;
            }
            else if (nrAno < dataAtual.Year)
            {
                filtro.DataFim = new DateTime(nrAno, 12, 31);
            }
            else
            {
                return new List<DisponibilidadeAtualOrcamento>();
            }

            var orcamentoAtual = new List<DisponibilidadeAtualOrcamento>();

            orcamentoAtual.AddRange(_contabilidade.ConsultarOrcamentoAtualReceita(filtro)
                .Select(x => new DisponibilidadeAtualOrcamento()
                {
                    NrAno = filtro.DataInicio.Year,
                    NrContaContabil6 = x.CodigoConta?.UnMask(),
                    NoContaContabil = x.NomeConta != null ? x.NomeConta?.UnMask() : "",
                    DtInicio = filtro.DataInicio,
                    DtFim = filtro.DataFim,
                    VrOrcado = x.ValorOrcado,
                    VrArrecadadoExercicio = x.ValorArrecadadoExercicio,
                    VrArrecadadoPeriodo = x.ValorArrecadadoPeriodo,
                    VrDiferenca = x.ValorDiferenca
                }
            ));

            return orcamentoAtual;
        }

        public List<string> EnviaDotacoesOrcamentarias(List<PropostaOrcamentariaDespesa> propostas)
        {
            int qtdDotacoes = propostas.Sum(x => x.PropostaOrcamentariaItens.Count);

            Siscont.Orcamento.DotacoesOrcamentariasEntity[] dotacoesOrcamentarias = new Siscont.Orcamento.DotacoesOrcamentariasEntity[qtdDotacoes];

            int cont = 0;

            for (int i = 0; i < propostas.Count; i++)
            {
                for (int j = 0; j < propostas[i].PropostaOrcamentariaItens.Count; j++)
                {
                    dotacoesOrcamentarias[cont] = new Siscont.Orcamento.DotacoesOrcamentariasEntity()
                    {
                        Exercicio = propostas[i].NrAno,
                        CodigoCentroCusto = propostas[i].CentroCusto.NrCentroCustoFmt,
                        NomeCentroCusto = propostas[i].CentroCusto.NoCentroCusto,
                        CodigoContaContabil = propostas[i].PropostaOrcamentariaItens[j].ContaContabil.NrContaContabilFmt,
                        NomeContaContabil = propostas[i].PropostaOrcamentariaItens[j].ContaContabil.NoContaContabil,
                        Receita = false,
                        Valor = propostas[i].PropostaOrcamentariaItens[j].VrOrcado.HasValue ? propostas[i].PropostaOrcamentariaItens[j].VrOrcado.Value : 0
                    };

                    cont++;
                }
            }

            var retorno = _orcamento.EnviarDotacaoOrcamentaria(dotacoesOrcamentarias);
            return RetornaErrosCasoTenha(retorno.Mensagens);
        }

        public List<string> EnviaDotacoesOrcamentarias(List<MetodologiaReceitaAno> metodologias, List<PropostaOrcamentariaDespesa> propostas)
        {
            int qtdDotacoes = propostas.Sum(x => x.PropostaOrcamentariaItens.Count);

            Siscont.Orcamento.DotacoesOrcamentariasEntity[] dotacoesOrcamentarias = new Siscont.Orcamento.DotacoesOrcamentariasEntity[metodologias.Count + qtdDotacoes];

            for (int i = 0; i < metodologias.Count; i++)
            {
                dotacoesOrcamentarias[i] = new Siscont.Orcamento.DotacoesOrcamentariasEntity()
                {
                    Exercicio = metodologias[i].NrAno,
                    CodigoCentroCusto = null,
                    NomeCentroCusto = null,
                    CodigoContaContabil = metodologias[i].ContaContabil.NrContaContabilFmt,
                    NomeContaContabil = metodologias[i].ContaContabil.NoContaContabil,
                    Receita = true,
                    Valor = metodologias[i].VrOrcado.HasValue ? metodologias[i].VrOrcado.Value : 0
                };
            }

            int cont = metodologias.Count;

            for (int i = 0; i < propostas.Count; i++)
            {
                for (int j = 0; j < propostas[i].PropostaOrcamentariaItens.Count; j++)
                {
                    dotacoesOrcamentarias[cont] = new Siscont.Orcamento.DotacoesOrcamentariasEntity()
                    {
                        Exercicio = propostas[i].NrAno,
                        CodigoCentroCusto = propostas[i].CentroCusto.NrCentroCustoFmt,
                        NomeCentroCusto = propostas[i].CentroCusto.NoCentroCusto,
                        CodigoContaContabil = propostas[i].PropostaOrcamentariaItens[j].ContaContabil.NrContaContabilFmt,
                        NomeContaContabil = propostas[i].PropostaOrcamentariaItens[j].ContaContabil.NoContaContabil,
                        Receita = false,
                        Valor = propostas[i].PropostaOrcamentariaItens[j].VrOrcado.HasValue ? propostas[i].PropostaOrcamentariaItens[j].VrOrcado.Value : 0
                    };

                    cont++;
                }
            }

            var retorno = _orcamento.EnviarDotacaoOrcamentaria(dotacoesOrcamentarias);

            return RetornaErrosCasoTenha(retorno.Mensagens);
        }

        public List<string> EnviaReformulacao(List<MetodologiaReceitaAno> metodologiasReformuladas, List<ReformulacaoOrcamentaria> reformulacoes)
        {
            int qtdDotacoes = reformulacoes.Sum(x => x.ReformulacaoOrcamentariaItens.Count);

            Siscont.Orcamento.ReformulacoesEntity[] reformulacoesOrcamentarias = new Siscont.Orcamento.ReformulacoesEntity[qtdDotacoes + metodologiasReformuladas.Count];

            decimal? vrReformulacao;
            bool paraMais;

            for (int i = 0; i < metodologiasReformuladas.Count; i++)
            {
                paraMais = metodologiasReformuladas[i].VrOrcado > metodologiasReformuladas[i].VrAprovadoAnoAnterior;
                vrReformulacao = paraMais ? metodologiasReformuladas[i].VrOrcado - metodologiasReformuladas[i].VrAprovadoAnoAnterior : metodologiasReformuladas[i].VrAprovadoAnoAnterior - metodologiasReformuladas[i].VrOrcado;

                reformulacoesOrcamentarias[i] = new Siscont.Orcamento.ReformulacoesEntity()
                {
                    Exercicio = metodologiasReformuladas[i].Reformulacao.NrAno,
                    CodigoContaContabil = metodologiasReformuladas[i].ContaContabil.NrContaContabilFmt,
                    CodigoCentroCusto = "",
                    NumeroProcesso = null,
                    Historico = metodologiasReformuladas[i].NoObservacoes,
                    Receita = true,
                    ParaMais = paraMais,
                    Valor = Convert.ToDecimal(vrReformulacao.HasValue ? vrReformulacao.Value : 0)
                };
            }

            int cont = metodologiasReformuladas.Count;

            for (int i = 0; i < reformulacoes.Count; i++)
            {
                for (int j = 0; j < reformulacoes[i].ReformulacaoOrcamentariaItens.Count; j++)
                {
                    var vrSuplementacao = reformulacoes[i].ReformulacaoOrcamentariaItens[j].VrSuplementacao;
                    var vrReducao = reformulacoes[i].ReformulacaoOrcamentariaItens[j].VrReducao;

                    paraMais = vrSuplementacao > vrReducao;
                    vrReformulacao = paraMais ? vrSuplementacao - vrReducao : vrReducao - vrSuplementacao;

                    reformulacoesOrcamentarias[cont] = new Siscont.Orcamento.ReformulacoesEntity()
                    {
                        Exercicio = reformulacoes[i].Reformulacao.NrAno,
                        CodigoCentroCusto = reformulacoes[i].CentroCusto.NrCentroCustoFmt,
                        CodigoContaContabil = reformulacoes[i].ReformulacaoOrcamentariaItens[j].ContaContabil.NrContaContabilFmt,
                        NumeroProcesso = null,
                        Historico = reformulacoes[i].ReformulacaoOrcamentariaItens[j].NoJustificativa,
                        Receita = false,
                        ParaMais = paraMais,
                        Valor = Convert.ToDecimal(vrReformulacao.HasValue ? vrReformulacao.Value : 0)
                    };

                    cont++;
                }
            }

            var retorno = _orcamento.CadastrarReformulacao(reformulacoesOrcamentarias);
            return RetornaErrosCasoTenha(retorno.Mensagens);
        }

        public List<string> EnviaReformulacaoReceita(List<MetodologiaReceitaAno> metodologiasReformuladas)
        {
            Siscont.Orcamento.ReformulacoesEntity[] reformulacoesOrcamentarias = new Siscont.Orcamento.ReformulacoesEntity[metodologiasReformuladas.Count];

            decimal? vrReformulacao;
            bool paraMais;

            for (int i = 0; i < metodologiasReformuladas.Count; i++)
            {
                paraMais = metodologiasReformuladas[i].VrOrcado > metodologiasReformuladas[i].VrAprovadoAnoAnterior;
                vrReformulacao = paraMais ? metodologiasReformuladas[i].VrOrcado - metodologiasReformuladas[i].VrAprovadoAnoAnterior : metodologiasReformuladas[i].VrAprovadoAnoAnterior - metodologiasReformuladas[i].VrOrcado;

                reformulacoesOrcamentarias[i] = new Siscont.Orcamento.ReformulacoesEntity()
                {
                    Exercicio = metodologiasReformuladas[i].Reformulacao.NrAno,
                    CodigoContaContabil = metodologiasReformuladas[i].ContaContabil.NrContaContabilFmt,
                    NumeroProcesso = null,
                    Historico = metodologiasReformuladas[i].NoObservacoes,
                    Receita = true,
                    ParaMais = paraMais,
                    Valor = Convert.ToDecimal(vrReformulacao.HasValue ? vrReformulacao.Value : 0)
                };
            }

            var retorno = _orcamento.CadastrarReformulacao(reformulacoesOrcamentarias);
            return RetornaErrosCasoTenha(retorno.Mensagens);
        }

        private List<string> RetornaErrosCasoTenha(Siscont.Orcamento.Mensagem[] mensagens)
        {
            var lstErros = new List<string>();

            if (mensagens != null && mensagens.Length > 0)
            {
                foreach (var m in mensagens)
                {
                    lstErros.Add(string.Format("{0}: {1}", m.Tipo, m.Texto));
                }
            }

            return lstErros.Distinct().ToList();
        }

        #endregion

        public Siscont.Orcamento.DisponibilidadeOrcamentariaEntity ConsultaSaldoOrcamentario(DateTime dtRef, string nrContaContabil, string nrCentroCusto)
        {
            return _orcamento.ConsultarDisponibilidadeOrcamentaria(new Siscont.Orcamento.DisponibilidadeOrcamentariaFilterEntity
            {
                DataReferencia = dtRef,
                ContaContabil = ContaContabil.Formata(nrContaContabil),
                CentroCustos = nrCentroCusto
            })
            .FirstOrDefault();
        }

        #region Empenho

        /// <summary>
        /// Inclui uma solicitação de empenho - retorna um Guid com a solicitação feita
        /// </summary>
        public Guid IncluiSolicEmp(Siscont.Despesa.EmpenhoTipo tipo, string nrContaContabilFmt, DateTime dtEmpenho, decimal vrEmpenho, Pessoa pessoa, string historico, Dictionary<string, decimal> centroCustoValor)
        {
            var req = new SolicitacoesReservasOrcamentariasEntity
            {
                ContaContabil = nrContaContabilFmt,
                SolicitacaoTipo = Siscont.Despesa.SolicitacaoOrcamentariaTipo.Empenho,
                EmpenhoData = dtEmpenho,
                EmpenhoTipo = tipo,
                EmpenhoValor = vrEmpenho,
                FavorecidoNome = new CreaspContext().Pessoas.BuscaNomeSiscont(pessoa),
                FavorecidoCPFCNPJ = pessoa.NrCpfFmt,
                Historico = historico,
                DistribuicoesCentroCusto = centroCustoValor
                    .Select(x => new Siscont.Despesa.SolicitacoesReservasOrcamentariasCentroCustoEntity
                    {
                        CentroCustoCodigo = CentroCusto.Formata(x.Key),
                        Valor = x.Value
                    }).ToArray()
            };

            var ret = _despesa.IncluirSolicitacaoReservaOrcamentaria(req);

            if (ret.Erro)
            {
                var mensagem = ret.Mensagens.FirstOrDefault()?.Texto ?? "Erro ao efetuar a solicitação de empenho no SISCONT";

                _log.Error(new Exception("SISCONT: IncluiSolicEmp = " + JsonConvert.SerializeObject(req) + " Erro: (" + mensagem + ")"));

                throw new NBoxException("SISCONT: " +  mensagem);
            }

            return ret.Entity;
        }

        /// <summary>
        /// Consulta empenho
        /// </summary>
        public IntegracaoEmpenhosEntity[] ConsultaEmpenho(int nrAno, Guid? nrSolicEmp = null, string nrCpf = null, int? nrEmp = null, string nrProcesso = null, bool? fgRestoAPagar = null, bool? fgProcessado = null)
        {
            return _despesa.ConsultarEmpenhos(new IntegracaoEmpenhosFilterEntity
            {
                Exercicio = nrAno,
                RestoAPagar = fgRestoAPagar == null ? BooleanOption.All : fgRestoAPagar.Value ? BooleanOption.True : BooleanOption.False,
                FavorecidoCPFCNPJ = Pessoa.FormataCpf(nrCpf),
                IdSolicitacaoReservaOrcamentaria = nrSolicEmp,
                Numero = nrEmp,
                NumeroProcesso = nrProcesso,
                RestoAPagarProcessado = fgProcessado == null ? BooleanOption.All : fgProcessado.Value ? BooleanOption.True : BooleanOption.False
                //TODO: Numero do processo está mapeado como int e não string NumeroProcesso = nrProcesso
            });
        }

        #endregion

        #region Pagamentos

        /// <summary>
        /// Inclui uma solicitação de pagamento para ser aprovada no SISCONT pelo usuário
        /// </summary>
        public void IncluiSolicPagtos(PagamentosAutorizadosEntity[] pagtos)
        {
            var ret = _despesa.CadastrarPagamentosAutorizados(pagtos);

            if (ret.Erro)
            {
                var mensagem = ret.Mensagens.FirstOrDefault()?.Texto ?? "Erro ao efetuar a solicitação de pagamento no SISCONT";

                _log.Error(new Exception("SISCONT: IncluiSolicPagtos = " + JsonConvert.SerializeObject(pagtos) + " Erro: (" + mensagem + ")"));

                throw new NBoxException("SISCONT: " + mensagem);
            }

            // testa se a quantidade enviada é igual a quantidad recebida
            if (pagtos.Count() != ret.Entity.Count()) throw new NBoxException("CadastrarPagamentosAutorizados: A quantidade de pagamento de envio não é igual de retorno");

            // copia o IdPagamentoAutoriazado conforme ordem de envio
            for (var i = 0; i < ret.Entity.Count(); i++)
            {
                pagtos[i].IdPagamentoAutorizado = ret.Entity[i].IdPagamentoAutorizado;
            }
        }

        /// <summary>
        /// Consulta um ou vários pagamentos
        /// </summary>
        public PagamentosEntity[] ConsultaPagto(int nrAno, Guid? nrSolicPagto = default(Guid?), string nrCpfFmt = null, int? nrPagto = default(int?), int? nrEmp = default(int?), string noFavorecido = null)
        {
            return _despesa.ConsultarPagamentos(new PagamentosFilterEntity
            {
                Exercicio = nrAno,
                CPFCNPJFavorecido = nrCpfFmt,
                IdPagamentoAutorizado = nrSolicPagto,
                NomeRazaoSocialFavorecido = noFavorecido,
                NumeroEmpenho = nrEmp,
                NumeroPagamento = nrPagto
            });
        }

        public IEnumerable<string> ListaTipoDocumentos()
        {
            yield return "Restos a Pagar";
            yield return "Outros tipos";
            yield return "Relação";
            yield return "Termo de Compromisso PC";
            yield return "Bilhete de Passagem e Nota de Bagagem";
            yield return "Bilhete de Passagem Ferroviário";
            yield return "Bilhete de Passagem Rodoviário";
            yield return "Boleto";
            yield return "Conta de água";
            yield return "Cupom Fiscal";
            yield return "DANFE";
            yield return "DARF";
            yield return "Despacho";
            yield return "Extrato";
            yield return "Fatura";
            yield return "GFIP";
            yield return "Lista de presença";
            yield return "Memorando";
            yield return "Nota de Atesto";
            yield return "Nota Fiscal";
            yield return "Nota Fiscal de Conta de Energia Eletrica";
            yield return "Nota Fiscal de Serviço";
            yield return "Nota Fiscal Fatura";
            yield return "Nota Fiscal Fatura de Serviço de Telecomunicações";
            yield return "Outros";
            yield return "Outros  tipos";
            yield return "Recibo";
            yield return "Requerimento";
        }

        #endregion

        #region Adiantamento

        public void IncluiSolicAdiantamento(SolicitacoesAdiantamentosEntity[] adiantamentos)
        {
            // adiantamento não tem IdExterno, vou tentar fazer via HASH (com todos os campos)
            var ret = _despesa.SolicitarAcatarAdiantamento(adiantamentos);

            if (ret.Erro)
            {
                var mensagem = ret.Mensagens.FirstOrDefault()?.Texto ?? "Erro ao efetuar a solicitação de adiantamento no SISCONT";

                _log.Error(new Exception("SISCONT: IncluiSolicAdiantamento = " + JsonConvert.SerializeObject(adiantamentos) + " Erro: (" + mensagem + ")"));

                throw new NBoxException("SISCONT: " + mensagem);
            }

            // testa se a quantidade enviada é igual a quantidad recebida
            if (adiantamentos.Count() != ret.Entity.Count()) throw new NBoxException("SolicitarAcatarAdiantamento: A quantidade de adiantamentos de envio não é igual de retorno");

            // copia o IdPagamentoAutoriazado conforme ordem de envio
            for (var i = 0; i < ret.Entity.Count(); i++)
            {
                adiantamentos[i].IdSolicitacaoAdiantamento = ret.Entity[i].IdSolicitacaoAdiantamento;
            }
        }

        public Adiantamento ConsultaAdiantamento(int nrAno, Guid idSolicAdiantamento)
        {
            //É MOCK
            var item = Factory.Get<IDatabase>().Single<NerpItem>(x => x.NrSolicAdiantamento == idSolicAdiantamento);

            return new Adiantamento
            {
                NrAno = nrAno,
                IdSolicAdiantamento = idSolicAdiantamento,
                NrAdiantamento = 0,
                DtAdiantamento = DateTime.Now,
                FgContaPrestada = true,
                VrAdiantado = item.VrTotal,
                VrDevolvido = 0,
                VrFinal = item.VrTotal
            };
        }
        #endregion
    }
}