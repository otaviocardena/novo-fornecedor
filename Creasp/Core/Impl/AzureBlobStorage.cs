﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using NBox.Core;
using NBox.Services;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Creasp.Core
{
    public class AzureBlobStorage : IStorage
    {
        private CloudBlobContainer _blobContainer;
        private AuditLog _log = new AuditLog();


        public AzureBlobStorage()
        {
            try
            {
                string storageconn = ConfigurationManager.AppSettings["StorageCS"];
                CloudStorageAccount storageacc = CloudStorageAccount.Parse(storageconn);

                CloudBlobClient cloudBlobClient = storageacc.CreateCloudBlobClient();
                _blobContainer = cloudBlobClient.GetContainerReference("creaspcontainer");

                // Create the container and set the permission  
                if (_blobContainer.CreateIfNotExists())
                {
                    _blobContainer.SetPermissions(
                        new BlobContainerPermissions
                        {
                            PublicAccess = BlobContainerPublicAccessType.Blob
                        }
                    );
                }
            }
            catch (Exception ex)
            {

                _log.Error(new Exception("AzureBlob() - Erro: " + ex.Message));

                throw new NBoxException("AzureBlob(): " + ex.Message);
            }
        }

        public void Delete(string fileId)
        {
            try
            {
                CloudBlockBlob blockBlob = _blobContainer.GetBlockBlobReference(fileId);

                blockBlob.Delete();
            }
            catch (Exception ex)
            {
                _log.Error(new Exception("AzureBlob delete - Erro: " + ex.Message));

                throw new NBoxException("Deletar documento: " + ex.Message);
            }
        }

        public void Download(string fileId, Stream output)
        {
            var cloudBlockBlob = _blobContainer.GetBlockBlobReference(fileId);

            cloudBlockBlob.FetchAttributes();

            cloudBlockBlob.DownloadToStream(output);
        }

        public byte[] Download(string fileId)
        {
            var cloudBlockBlob = _blobContainer.GetBlockBlobReference(fileId);

            cloudBlockBlob.FetchAttributes();

            byte[] byteArray = new byte[cloudBlockBlob.Properties.Length];

            cloudBlockBlob.DownloadToByteArray(byteArray, 0);

            return byteArray;
        }

        public string Filename(string fileId)
        {
            var cloudBlockBlob = _blobContainer.GetBlockBlobReference(fileId);

            cloudBlockBlob.FetchAttributes();

            return cloudBlockBlob.Name;
        }

        public IEnumerable<string> List(string container)
        {
            var result = _blobContainer.ListBlobs();

            return result.Select(x => x.Uri.AbsoluteUri);
          
        }

        /// <summary>
        /// Upload de um arquivo a partir de um HttpPostedFile. NomeBlob = container:Guid();
        /// </summary>
        /// <param name="container">Prefixo nome blob</param>
        /// <param name="file"></param>
        /// <returns>Absolute Uri</returns>
        public string Upload(string container, HttpPostedFile file)
        {
            string fileName;

            try
            {

                fileName = container + Guid.NewGuid();

                CloudBlockBlob blockBlob;

                blockBlob = _blobContainer.GetBlockBlobReference(fileName);
                blockBlob.Properties.ContentType = file.ContentType;

                blockBlob.UploadFromStream(file.InputStream);
            }
            catch (Exception ex)
            {
                _log.Error(new Exception("AzureBlob upload - Erro: " + ex.Message));

                throw new NBoxException("Incluir documento: " + ex.Message);
            }

            return fileName;
        }

        /// <summary>
        /// Upload de um arquivo a partir de um Stream
        /// </summary>
        /// <param name="container">Prefixo nome blob</param>
        /// <param name="filename">Sufixo nome blob, utilizar um guid()</param>
        /// <param name="input"></param>
        /// <returns></returns>
        public string Upload(string container, string filename, Stream input)
        {
            string name;

            try
            {
                name = container + filename;

                CloudBlockBlob blockBlob;

                blockBlob = _blobContainer.GetBlockBlobReference(name);

                blockBlob.UploadFromStream(input);

            }
            catch (Exception ex)
            {
                _log.Error(new Exception("AzureBlob upload - Erro: " + ex.Message));

                throw new NBoxException("Incluir documento: " + ex.Message);
            }

            return name;
        }

        /// <summary>
        /// Upload de um arquivo a partir de um byte[]
        /// </summary>
        /// <param name="container">Prefixo nome blob</param>
        /// <param name="filename">Sufixo nome blob, utilizar um guid()</param>
        /// <param name="input"></param>
        /// <returns></returns>
        public string Upload(string container, string filename, byte[] input)
        {
            string name;

            try
            {
                name = container + filename;

                CloudBlockBlob blockBlob;

                blockBlob = _blobContainer.GetBlockBlobReference(name);

                blockBlob.UploadFromByteArray(input, 0, input.Length);
              
            }
            catch (Exception ex)
            {
                _log.Error(new Exception("AzureBlob upload ("+ filename +") - Erro: " + ex.Message));

                throw new NBoxException("Erro ao incluir documento ("+ filename +") : " + ex.Message);
            }

            return name;
        }

        public string Url(string fileId)
        {
            var blockBlob = _blobContainer.GetBlockBlobReference(fileId);
            return blockBlob.Uri.AbsoluteUri;
        }
        
    }
}
