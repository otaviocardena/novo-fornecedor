﻿using System;
using NBox.Core;
using NBox.Services;
using NPoco;

namespace Creasp.Core
{
    /// <summary>
    /// Implementa o writer da auditoria em banco de dados
    /// </summary>
    public class AuditWriter : IAuditWriter
    {
        public void Write(IUser user, string page, string tablename, object pkey, string content)
        {
            var db = Factory.Get<IDatabase>();

            var audit = new Auditoria
            {
                DtAuditoria = DateTime.Now,
                NoLogin = user == null ? null : user.Login.MaxLength(30),
                NoPagina = page.MaxLength(200),
                NoTabela = tablename.MaxLength(40),
                IdChave = pkey.To<int?>(),
                NoConteudo = content.MaxLength(2000)
            };

            db.Insert(audit);
        }
    }
}