﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using Creasp;
using Creasp.Creanet;
using NBox.Core;
using NBox.Services;
using NPoco;

namespace Creasp.Core
{
    //TODO: neste caso acho que deve fazer foreach para melhorar possivel mensagem de erro na busca da unidade/camara
    // acho que vamos ter muitos problemas com isso
    public class CreanetService : ICreanet
    {
        private Creanet.ServiceCreanetClient _client = new Creanet.ServiceCreanetClient();
        private ICache _cache = Factory.Get<ICache>();

        public IEnumerable<Profissional> BuscaProfissionais(string creasp, string cpf, string nome)
        {
            if (string.IsNullOrEmpty(creasp) && string.IsNullOrEmpty(cpf) && string.IsNullOrEmpty(nome)) throw new ArgumentNullException("creasp,cpf,nome");

            var result = _client.BuscaProfissionais(creasp.To<long>(0), cpf, nome, 1);

            return result.Select(x => new Profissional
            {
                NrCreasp = x.Creasp.ToString("0000000000"),
                NrCpf = x.Cpf?.Trim().To<string>()?.PadLeft(11, '0'),
                NoProfissional = x.Nome,
                DtNasc = x.DataNascimento,
                TpSexo = x.Sexo.ToString(),
                NoEmail = x.Email?.Trim().To<string>(),
                Camaras = x.Titulos.Select(t => t.CodCamara.To<int>()).ToArray(),
                Titulos = x.Titulos.Select(t => t.NomeTitulo).ToArray(),
                Enderecos = x.Enderecos.Select(e => new ProfissionalEndereco
                {
                    TpEndereco = e.TipoEndereco.ToString(),
                    NrCep = e.Cep,
                    NoLogradouro = (e.TipoLogradouro + " " + e.Logradouro)?.Trim(),
                    NrEndereco = e.Numero?.Trim().To<string>(),
                    NoComplemento = e.Complemento?.Trim().To<string>(),
                    NoBairro = e.Bairro?.Trim().To<string>(),
                    NoCidade = e.Localidade?.Trim().To<string>(),
                    CdUf = e.Uf?.Trim().To<string>(),
                    FgCorrespondencia = e.IndCorrespondencia == "SIM",
                    NrTelefone = 
                        string.IsNullOrWhiteSpace(e.Fone1) && string.IsNullOrWhiteSpace(e.Fone2) ? null :
                        string.IsNullOrWhiteSpace(e.Fone2) ? e.Fone1 : 
                        string.IsNullOrWhiteSpace(e.Fone1) ? e.Fone2 :
                        e.Fone1 + " / " + e.Fone2
                }).ToArray()
            });
        }

        public string ConsultaAnuidade(string creasp)
        {
            return _cache.Get("anuidade-" + creasp, () =>
            {
                var result = _client.ConsultaAnuidade(creasp.To<long>());

                return result == "Quite" ? AnuidadeSituacao.Quite :
                    result == "EmDia" ? AnuidadeSituacao.EmDia : AnuidadeSituacao.EmDebito;
            });
        }

        public IEnumerable<ValorConvenio> BuscaValorConvenios(int ano, int mes)
        {
            return _client.BuscaValorConvenios(ano, mes)
                .ToList()
                .ForEach((x, i) => x.CpfGestor = x.CpfGestor.UnMask().PadLeft(11, '0'));
        }

        public IEnumerable<ContratoEntidade> BuscaContratosEntidade(int ano, int mes)
        {
            //MOCK para homologação
            yield return new ContratoEntidade
            {
                CodEntidade = 295,
                CpfGestor = "61159026815",
                NroContrato = "00000-1",
                NroProcesso = "99999-1",
                ValorContratoMes = 1800
            };
            yield return new ContratoEntidade
            {
                CodEntidade = 333,
                CpfGestor = "61159026815",
                NroContrato = "00000-2",
                NroProcesso = "99999-2",
                ValorContratoMes = 2000
            };
            //return _client.BuscaContratosEntidade(ano, mes);
        }

        public IEnumerable<Entidade> ListaEntidades()
        {
            var result = _client.BuscaEntidades();

            return result.Select(x => new Entidade()
            {
                CdExterno = x.CodEntidade.To<string>(),
                NoEntidade = x.Nome?.Trim().To<string>(),
                NrCnpj = (x.CNPJ ?? "").UnMask().PadLeft(14, '0'),
                TpEntidade = TpEntidade.EntidadeClasse,
                CdRepasse = x.CodRepasse.To<int?>() ?? 0,
                CdUnidade = x.CodUnidade.To<int>(0),
                Camaras = x.CodCamaras?.Select(c => new EntidadeCamaras { CdCamara = c }).ToList() ?? new List<EntidadeCamaras>(),
                FgRepresentante = x.IndPlenario == '1',
                FgAtivo = true
            });
        }

        public IEnumerable<Entidade> ListaIES()
        {
            var result = _client.BuscaRepresentacaoInstituicoesDeEnsino();

            return result.Select(x => new Entidade()
            {
                CdExterno = x.NumRegistro.ToString(),
                NoEntidade = x.NomeRepresentacao ?? x.InstituicaoEnsinos[0].Nome ?? "SEM NOME",
                TpEntidade = TpEntidade.InstituicaoEnsino,
                CdUnidade = x.InstituicaoEnsinos.Max(z => z.CodUnidade).To<int?>(),
                Camaras = x.Camaras.Select(c => new EntidadeCamaras { CdCamara = c.CodCamara.To<int>() }).ToList(),
                FgRepresentante = true,
                FgAtivo = true
            });
        }

        public IEnumerable<Unidade> ListaUnidades()
        {
            var result = _client.ListaUnidades();

            return result.Select(x => new Unidade
            {
                CdUnidade = x.CodUnidade.To<int>(),
                CdUnidadePai = x.CodUnidadePai.To<int?>(),
                NoUnidade = x.NomeUnidade?.Trim().To<string>(),
                TpUnidade = x.TipoUnidade?.Trim().To<string>(),
                // NrCpfGestor = x.
                NrCep = x.Cep?.Trim().To<string>(),
                NrCentroCusto = x.NrCentroCusto?.Trim().To<string>(),
                FgAtivo = true
            });
        }

        public long Ping()
        {
            var stop = new Stopwatch();
            stop.Start();
            _client.ConsultaAnuidade(601518779);
            return stop.ElapsedMilliseconds;
        }
    }
}