﻿using NBox.Core;
using NBox.Web;
using System;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Collections.Generic;
using System.Collections;
using NBox.Services;
using NPoco;
using System.Text;

namespace Creasp.Core
{
    public class CreaspUser : IUser
    {
        private const int HASH_SIZE = 4;

        public string Login { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public int IdPessoa { get; set; }
        public bool Fornecedor { get; set; }
        public string Claim { get; set; }
        public HashSet<string> GruposAd { get; set; }
        public HashSet<string> Recursos { get ;set; }

        public IIdentity Identity { get { return new GenericIdentity(this.Login); } }

        public bool IsInRole(string roleName)
        {
            if (roleName == "?") return true; // valida apenas se usuário logado

            return 
                Recursos.Contains("*") || // admin power
                Recursos.Contains(roleName) || // exact resource
                Recursos.FirstOrDefault(x => x.StartsWith(roleName + ".")) != null; // child resource
        }

        public CreaspUser()
        {
            // para deserialização
        }

        public CreaspUser(string login, string nome, string email, int idPessoa, IEnumerable<string> recursos, IEnumerable<string> grupos, bool fornecedor, string claim = "ALL")
        {
            Login = login;
            Nome = nome;
            Email = email;
            IdPessoa = idPessoa;
            GruposAd = new HashSet<string>(grupos);
            Recursos = new HashSet<string>(recursos);
            Fornecedor = fornecedor;
            Claim = claim;
        }

        public IDictionary<string, object> Serialize()
        {
            return new Dictionary<string, object>
            {
                { "u", Login },
                { "n", Nome },
                { "f", Email },
                { "p", IdPessoa },
                { "r", string.Join(",", Recursos.ToArray()) },
                { "g", string.Join(",", GruposAd.ToArray()) },
                { "o", Fornecedor },
                { "c", Claim },
            };
        }

        public void Deserialize(IDictionary<string, object> payload)
        {
            Login = payload["u"] as string;
            Nome = payload["n"] as string;
            Email = payload["f"] as string;
            IdPessoa = Convert.ToInt32(payload["p"]);
            Recursos = new HashSet<string>((payload["r"] as string ?? "").Split(','));
            GruposAd = new HashSet<string>((payload["g"] as string ?? "").Split(','));
            Fornecedor = (bool) payload["o"];
            Claim = payload["c"] as string;
        }
    }
}