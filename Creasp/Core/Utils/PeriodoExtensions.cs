﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using NBox.Core;
using NPoco;
using NPoco.Linq;
using Creasp.Siplen;
using Creasp.Nerp;

namespace Creasp.Core
{
    /// <summary>
    /// Algumas extensões para trabalhar com periodo
    /// </summary>
    public static class WhereExtensions
    {
        public static Periodo GetPeriodo(this IPeriodo p)
        {
            return new Periodo(p);
        }

        #region WHERE IPeriodo BETWEEN Periodo

        /// <summary>
        /// Retorna todas os periodos que cruzam pelo periodo informado
        /// </summary>
        public static IQueryProvider<Camara> WherePeriodo(this IQueryProvider<Camara> qb, Periodo periodo)
        {
            return periodo.IsVazio ? qb : qb.Where(x => periodo.Inicio <= x.DtFim && periodo.Fim >= x.DtIni);
        }

        public static IQueryProvider<Licenca> WherePeriodo(this IQueryProvider<Licenca> qb, Periodo periodo)
        {
            return periodo.IsVazio ? qb : qb.Where(x => periodo.Inicio <= x.DtFim && periodo.Fim >= x.DtIni);
        }

        public static IQueryProvider<Mandato> WherePeriodo(this IQueryProvider<Mandato> qb, Periodo periodo)
        {
            return periodo.IsVazio ? qb : qb.Where(x => periodo.Inicio <= x.DtFim && periodo.Fim >= x.DtIni);
        }

        public static IQueryProvider<Grupo> WherePeriodo(this IQueryProvider<Grupo> qb, Periodo periodo)
        {
            return periodo.IsVazio ? qb : qb.Where(x => periodo.Inicio <= x.DtFim && periodo.Fim >= x.DtIni);
        }

        public static IQueryProvider<Comissao> WherePeriodo(this IQueryProvider<Comissao> qb, Periodo periodo)
        {
            return periodo.IsVazio ? qb : qb.Where(x => periodo.Inicio <= x.DtFim && periodo.Fim >= x.DtIni);
        }

        public static IQueryProvider<Cargo> WherePeriodo(this IQueryProvider<Cargo> qb, Periodo periodo)
        {
            return periodo.IsVazio ? qb : qb.Where(x => periodo.Inicio <= x.DtFim && periodo.Fim >= x.DtIni);
        }

        public static IQueryProvider<Inspetoria> WherePeriodo(this IQueryProvider<Inspetoria> qb, Periodo periodo)
        {
            return periodo.IsVazio ? qb : qb.Where(x => periodo.Inicio <= x.DtFim && periodo.Fim >= x.DtIni);
        }

        public static IQueryProvider<SubDelegacao> WherePeriodo(this IQueryProvider<SubDelegacao> qb, Periodo periodo)
        {
            return periodo.IsVazio ? qb : qb.Where(x => periodo.Inicio <= x.DtFim && periodo.Fim >= x.DtIni);
        }

        /// <summary>
        /// Retorna apenas os registros que contem periodo em aberto (DtFim = 9999/12/31
        /// </summary>
        public static IQueryProvider<Camara> WherePeriodoEmAberto(this IQueryProvider<Camara> qb)
        {
            return qb.Where(x => x.DtFim == Periodo.MaxDate);
        }

        public static IQueryProvider<Mandato> WherePeriodoEmAberto(this IQueryProvider<Mandato> qb)
        {
            return qb.Where(x => x.DtFim == Periodo.MaxDate);
        }

        public static IQueryProvider<Licenca> WherePeriodoEmAberto(this IQueryProvider<Licenca> qb)
        {
            return qb.Where(x => x.DtFim == Periodo.MaxDate);
        }

        public static IQueryProvider<Comissao> WherePeriodoEmAberto(this IQueryProvider<Comissao> qb)
        {
            return qb.Where(x => x.DtFim == Periodo.MaxDate);
        }

        public static IQueryProvider<Cargo> WherePeriodoEmAberto(this IQueryProvider<Cargo> qb)
        {
            return qb.Where(x => x.DtFim == Periodo.MaxDate);
        }

        public static IQueryProvider<Inspetoria> WherePeriodoEmAberto(this IQueryProvider<Inspetoria> qb)
        {
            return qb.Where(x => x.DtFim == Periodo.MaxDate);
        }

        #endregion
    }
}