﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Creasp;
using NBox.Core;
using NBox.Web;
using NBox.Web.Controls;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace Creasp.Core
{
    public interface IDWDataSource : IDisposable
    {
        string Nome { get; }
        string Descricao { get; }
        bool PorPeriodo { get; }
        IEnumerable<IDWCampo> ListaCampos();
        void Executa(Periodo p, List<int> colunas, List<KeyValuePair<int, string>> filtros);
        void Download(NPage page);
    }

    /// <summary>
    /// Classe abstrata que facilita a implementação da interface de data source
    /// </summary>
    public abstract class DWDataSource<T> : IDWDataSource
    {
        public abstract string Nome { get; }
        public abstract string Descricao { get; }
        public abstract bool PorPeriodo { get; }
        public abstract void InicializaCampos();
        public abstract IEnumerable<T> GetDados(CreaspContext context, Periodo periodo);

        private List<DWCampo<T>> _campos = null;

        protected DWCampo<T> Add(string nome, bool isPadrao = true)
        {
            var campo = new DWCampo<T>(_campos.Count + 1, nome);
            campo.IsPadrao = isPadrao;
            _campos.Add(campo);
            return campo;
        }

        public IEnumerable<DWCampo<T>> GetCampos()
        {
            if (_campos == null)
            {
                _campos = new List<DWCampo<T>>();
                InicializaCampos();
            }
            return _campos;
        }

        public IEnumerable<IDWCampo> ListaCampos() => GetCampos().Select(x => x as IDWCampo);

        #region Implementação da emissão do relatorio em excel

        private ExcelPackage _pkg;
        private ExcelWorksheet _ws;

        public void InicializaExcel()
        {
            _pkg = new ExcelPackage();
            _ws = _pkg.Workbook.Worksheets.Add(Nome);

            // header padrão
            var logo = _ws.Drawings.AddPicture("logo", new FileInfo(HttpContext.Current.Server.MapPath("~/Content/Images/brasao_header.gif")));
            logo.From.Column = 0;
            logo.From.Row = 0;
            var fix = .3;
            logo.SetSize((int)(1726 * fix), (int)(197 * fix));

            _ws.PrinterSettings.RepeatRows = new ExcelAddress("$1:$5");
        }

        /// <summary>
        /// Processa todo o relatório (gerando o excel) conforme a fonte de dados, aplicando os campos e filtros inforado. 
        /// </summary>
        public void Executa(Periodo p, List<int> colunas, List<KeyValuePair<int, string>> filtros)
        {
            InicializaExcel();

            var context = new CreaspContext();

            var campos = GetCampos().ToList();
            var dados = GetDados(context, p);

            var header = 5;
            var linha = header; // começa na proxima linha apartir deste numero

            // percorre todas as linhas do datasource
            foreach (var row in dados)
            {
                var stop = false;

                // para cada linha, aplica todos os filtros
                foreach (var filtro in filtros)
                {
                    var campo = campos.First(x => x.Id == filtro.Key);
                    var resultado = campo.FiltroFn(row, filtro.Value, context);

                    if (!resultado)
                    {
                        stop = true;
                        break;
                    }
                }

                // caso não tenha passado pelo filtro, pula pro proximo registro
                if (stop) continue;

                linha++;

                colunas.ForEach((c, i) =>
                {
                    var campo = campos.First(x => x.Id == c);
                    var value = campo.ColunaFn(row, context);
                    var cell = _ws.Cells[linha, i + 1];

                    if (value is DateTime && ((DateTime)value == Periodo.MaxDate || (DateTime)value == Periodo.MinDate))
                    {
                        value = null;
                    }

                    cell.Value = value;

                    if (value is DateTime) cell.Style.Numberformat.Format = "dd/MM/yyyy";
                    if (value is decimal) cell.Style.Numberformat.Format = "0.00#,##";
                    if (value is string && value != null && value.ToString().Contains('\n')) cell.Style.WrapText = true;

                });
            }

            // verifica se não foi encontrado nenhum registro
            if (linha == header) throw new NBoxException("Nenhum registro para o filtro informado");

            // montando a header
            colunas
                .ForEach((c, i) => _ws.Cells[header, i + 1].Value = campos.First(x => x.Id == c).Nome)
                .ForEach((c, i) => _ws.Column(i + 1).AutoFit(i == 0 ? 10 : 8, 80));

            // estilo na header
            var head = _ws.Cells[header, 1, header, colunas.Count()];
            head.Style.Fill.PatternType = ExcelFillStyle.Solid;
            head.Style.Fill.BackgroundColor.SetColor(Color.Yellow);
            head.Style.Font.Bold = true;
            _ws.View.FreezePanes(header + 1, 1);
        }

        public void Download(NPage page)
        {
            using (var mem = new MemoryStream())
            {
                _pkg.SaveAs(mem);

                page.Download(_ws.Name + "-" + DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss") + ".xlsx", mem.ToArray(), false);
            }
        }

        public void Dispose()
        {
            if (_pkg != null) _pkg.Dispose();
        }

        #endregion
    }

    #region Representa um campo disponivel no relatório

    public interface IDWCampo
    {
        int Id { get; set; }
        string Nome { get; set; }
        string Descricao { get; set; }
        bool IsPadrao { get; set; }
        bool IsColuna { get; }
        bool IsFiltro { get; }

        MaskType MaskType { get; set; }
        string MaskFormat { get; set; }
        Func<CreaspContext, Periodo, IEnumerable<ListItem>> DropDownItems { get; set; }
    }

    /// <summary>
    /// Representa um campo (coluna) ou filtro de pesquisa
    /// </summary>
    public class DWCampo<T> : IDWCampo
    {
        public DWCampo(int id, string nome)
        {
            Id = id;
            Nome = nome;
            Descricao = nome;
            IsPadrao = true;
        }

        public int Id { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public bool IsPadrao { get; set; }

        public Func<T, CreaspContext, object> ColunaFn { get; set; } = null;
        public Func<T, string, CreaspContext, bool> FiltroFn { get; set; } = null;

        public bool IsColuna => ColunaFn != null;
        public bool IsFiltro => FiltroFn != null;

        public MaskType MaskType { get; set; } = MaskType.String;
        public string MaskFormat { get; set; } = "";
        public Func<CreaspContext, Periodo, IEnumerable<ListItem>> DropDownItems { get; set; }

        public DWCampo<T> Desc(string descricao)
        {
            Descricao = descricao;
            return this;
        }

        public DWCampo<T> Coluna(Func<T, object> fn)
        {
            ColunaFn = (c, db) => fn(c);
            return this;
        }

        public DWCampo<T> Coluna(Func<T, CreaspContext, object> fn)
        {
            ColunaFn = fn;
            return this;
        }

        public DWCampo<T> Filtro(Func<T, string, bool> fn)
        {
            FiltroFn = (c, v, db) => fn(c, v);
            return this;
        }
        public DWCampo<T> Filtro(Func<T, string, CreaspContext, bool> fn)
        {
            FiltroFn = fn;
            return this;
        }

        public DWCampo<T> Mask(MaskType mask, string format = "")
        {
            MaskType = mask;
            MaskFormat = format;
            return this;
        }

        public DWCampo<T> Combo(Func<CreaspContext, Periodo, IEnumerable<ListItem>> fn)
        {
            DropDownItems = fn;
            return this;
        }
    }

    #endregion
}