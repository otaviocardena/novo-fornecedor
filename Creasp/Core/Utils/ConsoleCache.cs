﻿using NBox.Services;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Core.Utils
{
    public class ConsoleCache : ICache
    {
        private TimeSpan _timeout;

        public ConsoleCache(TimeSpan timeout)
        {
            _timeout = timeout;
        }

        public ConsoleCache()
        {
            _timeout = TimeSpan.FromMinutes(60);
        }

        public void Clear()
        {
        }

        public void Clear(string prefix)
        {
        }

        public T Get<T>(string key, Func<T> create)
        {
            return Get<T>(key, _timeout, create);
        }
        public T Get<T>(string key, int timeout, Func<T> create)
        {
            return Get<T>(key, TimeSpan.FromMinutes(timeout), create);
        }

        public T Get<T>(string key, TimeSpan timeout, Func<T> create)
        {
            return create();
        }
    }
}
