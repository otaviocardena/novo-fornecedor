﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using NBox.Core;
using NPoco;

namespace Creasp.Core
{
    /// <summary>
    /// Struct para trabalhar com periodo de datas. Não considera hora - Possui limites menores que o C#;
    /// </summary>
    [Serializable]
    public struct Periodo
    {
        // Datas Minimas/Maximas prevendo uso no SQL Server
        public static readonly DateTime MinDate = new DateTime(1900, 1, 1);
        public static readonly DateTime MaxDate = new DateTime(9999, 12, 31);
        public static readonly Periodo Vazio = new Periodo(MinDate, MaxDate);
        public static readonly Periodo Hoje = new Periodo(DateTime.Today, DateTime.Today);

        public DateTime Inicio;
        public DateTime Fim;

        public Periodo(IPeriodo periodo)
        {
            Inicio = periodo.DtIni < MinDate ? MinDate : periodo.DtIni.Date;
            Fim = periodo.DtFim > MaxDate ? MaxDate : periodo.DtFim.Date;

            // no caso da instancia do objeto não possuir DtIni nem DtFim (valor default é MinValue)
            if (Fim == DateTime.MinValue) Fim = MaxDate;

            if (Fim < Inicio) throw new NBoxException("A data de fim não pode ser menor que a data de início");
        }

        public Periodo(DateTime dtIni, DateTime dtFim)
        {
            // garante que só vai trabalhar com a data (sem hora) limitando nos MinMax dates
            Inicio = dtIni.Date < MinDate ? MinDate : dtIni.Date;
            Fim = dtFim > MaxDate ? MaxDate : dtFim.Date;

            if (Fim < Inicio) throw new NBoxException("A data de fim não pode ser menor que a data de início");
        }

        /// <summary>
        /// Cria o perido conforme o nome por extenso
        /// </summary>
        public Periodo(string extenso)
        {
            Match m = null;

            if (extenso.Equals("Hoje", StringComparison.OrdinalIgnoreCase))
            {
                Inicio = DateTime.Today;
                Fim = DateTime.Today;
            }
            else if (extenso.Equals("EsteMes", StringComparison.OrdinalIgnoreCase))
            {
                Inicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                Fim = Inicio.AddMonths(1).AddDays(-1);
            }
            else if (extenso.Equals("MesPassado", StringComparison.OrdinalIgnoreCase))
            {
                Inicio = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1);
                Fim = Inicio.AddMonths(1).AddDays(-1);
            }
            else if (extenso.Equals("EsteAno", StringComparison.OrdinalIgnoreCase))
            {
                Inicio = new DateTime(DateTime.Today.Year, 1, 1);
                Fim = new DateTime(DateTime.Today.Year, 12, 31);
            }
            else if (extenso.Equals("AnoPassado", StringComparison.OrdinalIgnoreCase))
            {
                Inicio = new DateTime(DateTime.Today.Year - 1, 1, 1);
                Fim = new DateTime(DateTime.Today.Year - 1, 12, 31);
            }
            else if(extenso.Equals("Vazio", StringComparison.OrdinalIgnoreCase))
            {
                Inicio = MinDate;
                Fim = MaxDate;
            }
            else if ((m = Regex.Match(extenso, @"^\d+$")).Success)
            {
                Inicio = new DateTime(extenso.To<int>(), 1, 1);
                Fim = new DateTime(extenso.To<int>(), 12, 31);
            }
            else if ((m = Regex.Match(extenso, @"^(\d+)\s*[-a]\s*(\d+)$")).Success)
            {
                Inicio = new DateTime(m.Groups[1].Value.To<int>(), 1, 1);
                Fim = new DateTime(m.Groups[2].Value.To<int>(), 12, 31);
            }
            else
            {
                throw new ArgumentException("Período por extenso [" + extenso + "] desconhecido");
            }
        }

        /// <summary>
        /// Indica que o periodo não tem final, é permanente
        /// </summary>
        public bool EmAberto => Fim == MaxDate;

        /// <summary>
        /// Indica que o periodo é vazio
        /// </summary>
        public bool IsVazio => Inicio == MinDate && Fim == MaxDate;

        /// <summary>
        /// Pega o intervalo de tempo entre o inicio e o final
        /// </summary>
        public TimeSpan TimeSpan => Fim.Subtract(Inicio);

        /// <summary>
        /// Indica se o periodo é apenas o dia de hoje
        /// </summary>
        public bool IsHoje => Inicio == DateTime.Today && Fim == DateTime.Today;

        /// <summary>
        /// Retorna a data de final mais proxima. Se o periodo for em aberto, não considera (retorna MaxDate)
        /// </summary>
        public DateTime FimMaisProximo(DateTime dtFim1, DateTime? dtFim2 = null)
        {
            if (EmAberto) return MaxDate;

            var list = new List<DateTime>(new DateTime[] { Fim, dtFim1 });

            if (dtFim2.HasValue) list.Add(dtFim2.Value);

            return list.Min();
        }

        /// <summary>
        /// Retorna se o há colisão entre dois periodos
        /// </summary>
        public bool Intersecao(Periodo periodo)
        {
            return (periodo.Inicio >= this.Inicio && periodo.Inicio <= this.Fim) || // colisao no inicio
                (periodo.Fim >= this.Inicio && periodo.Fim <=  this.Fim) || // colisao no fim
                (periodo.Inicio < this.Inicio && periodo.Fim > this.Fim); // periodo engloba todo o this
        }

        /// <summary>
        /// Verifica se o periodo passado parametro está completamente dentro do periodo da classe
        /// </summary>
        public bool NoIntervalo(Periodo periodo)
        {
            return periodo.Inicio >= Inicio && periodo.Fim <= Fim;
        }

        /// <summary>
        /// Verifica se a data por parametro está dentro do periodo da classe
        /// </summary>
        public bool NoIntervalo(DateTime dt)
        {
            return dt.Date >= Inicio && dt.Date <= Fim;
        }

        /// <summary>
        /// Valida periodo minimo (D dias, M mes, A ano)
        /// </summary>
        public Periodo ValidaMin(char dma, int value)
        {
            if (dma == 'D' && Fim < Inicio.AddDays(value)) throw new NBoxException("O período mínimo são de {0} dia(s)", value);
            if (dma == 'M' && Fim < Inicio.AddMonths(value)) throw new NBoxException("O período mínimo são de {0} mes(es)", value);
            if (dma == 'A' && Fim < Inicio.AddYears(value)) throw new NBoxException("O período mínimo são de {0} ano(s)", value);

            return this;
        }

        /// <summary>
        /// Valida periodo máximo de dias (D dias, M mes, A ano)
        /// </summary>
        public Periodo ValidaMax(char dma, int value, bool aceitaEmAberto = true)
        {
            if (Fim == MaxDate && aceitaEmAberto) return this;

            if (dma == 'D' && Fim >= Inicio.AddDays(value)) throw new NBoxException("O período máximo são de {0} dia(s)", value);
            if (dma == 'M' && Fim >= Inicio.AddMonths(value)) throw new NBoxException("O período máximo são de {0} mes(es)", value);
            if (dma == 'A' && Fim >= Inicio.AddYears(value)) throw new NBoxException("O período máximo são de {0} ano(s)", value);

            return this;
        }

        /// <summary>
        /// Cria uma instancia de Periodo de apenas 1 dia (Inicio = Fim)
        /// </summary>
        public static Periodo UmDia(DateTime data) => new Periodo(data, data);

        public static Periodo UmAno(int ano) => new Periodo(new DateTime(ano, 1, 1), new DateTime(ano, 12, 31));

        public static Periodo Anos(int anoIni, int anoFim) => new Periodo(new DateTime(anoIni, 1, 1), new DateTime(anoFim, 12, 31));

        #region Operadores

        public override bool Equals(object obj) => obj is Periodo && this == (Periodo)obj;
        public override int GetHashCode() => Inicio.GetHashCode() ^ Fim.GetHashCode();
        public static bool operator ==(Periodo x, Periodo y) => x.Inicio == y.Inicio && x.Fim == y.Fim;
        public static bool operator !=(Periodo x, Periodo y) => !(x == y);
        public override string ToString() => ToString(true, false);

        #endregion

        public string ToString(bool incluiDuracao, bool compactado = false)
        {
            var fmt = "";
            var dias = TimeSpan.TotalDays + 1;
            var meses = Math.Round(dias / 30d).To<int>();
            var anos = Math.Round(meses / 12d).To<int>();
            var tempo = dias <= 30 ? dias + " dias" :
                meses == 1 ? "1 mês" :
                meses > 1 && meses <= 12 ? meses + " meses" :
                meses > 12 && anos == 1 ? "1 ano" : anos + " anos";

            if (Inicio == MinDate && Fim == MaxDate) return "Sem período";

            // verifica se as datas podem ser exibidas de modo compactado
            if (compactado)
            {
                // 1 dias em relação a hoje
                if (IsHoje) return "Hoje";
                if (Inicio == DateTime.Today.AddDays(-1) && Fim == DateTime.Today.AddDays(-1)) return "Ontem";
                if (Inicio == DateTime.Today.AddDays(+1) && Fim == DateTime.Today.AddDays(+1)) return "Amanhã";

                // mes inteiro
                if (Inicio.Day == 1 && 
                    Fim.Year == Inicio.Year && 
                    Fim.Month == Inicio.Month && 
                    Fim.Day == Inicio.AddMonths(1).AddDays(-1).Day)
                {
                    return Inicio.ToString("MMMM/yyyy").Captalize();
                }

                // ano inteiro
                if (Inicio.Day == 1 && Inicio.Month == 1 && Fim.Month == 12 && Fim.Day == 31 &&
                    Inicio.Year == Fim.Year)
                {
                    return Inicio.Year.ToString();
                }

                // periodo de anos
                if (Inicio.Day == 1 && Inicio.Month == 1 && Fim.Month == 12 && Fim.Day == 31)
                {
                    return Inicio.Year.ToString() + (Fim == MaxDate ? " em diante" : " a " + Fim.Year.ToString());
                }
            }

            // sem data fim
            if (Fim == MaxDate) fmt = "{0:d} em diante";

            // mesmo dia
            else if (Inicio == Fim) fmt = "{0:d}" + (incluiDuracao ? " (1 dia)" : "");

            // dentro do mesmo mês
            else if (Inicio.Year == Fim.Year) fmt = "{0:dd/MM} a {1:d}" + (incluiDuracao ? " ({2})" : "");

            // todos os outros casos
            else fmt = "{0:d} a {1:d}" + (incluiDuracao ? " ({2})" : "");

            return string.Format(fmt, Inicio, Fim, tempo);
        }

        #region Util para calculo de dia util em uma data

        public static DateTime AddWorkDays(DateTime date, int workingDays)
        {
            var direction = workingDays < 0 ? -1 : 1;
            var newDate = date;

            while (workingDays != 0)
            {
                newDate = newDate.AddDays(direction);
                if (newDate.DayOfWeek != DayOfWeek.Saturday && newDate.DayOfWeek != DayOfWeek.Sunday && !IsHoliday(newDate))
                {
                    workingDays -= direction;
                }
            }

            return newDate;
        }

        public static bool IsHoliday(DateTime date)
        {
            if (date.Month == 1 && date.Day == 1) return true; // ano novo
            if (date.Month == 4 && date.Day == 21) return true; // tiradentes
            if (date.Month == 5 && date.Day == 1) return true; // trabalho
            if (date.Month == 10 && date.Day == 12) return true; // nsa sra aparecida
            if (date.Month == 11 && date.Day == 2) return true; // finados
            if (date.Month == 11 && date.Day == 15) return true; // proclamação da republica
            if (date.Month == 12 && date.Day == 25) return true; // natal

            return false;
        }

        #endregion
    }
}