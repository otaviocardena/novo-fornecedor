﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Creasp;
using NBox.Core;

namespace Creasp.Core
{
    public class TituloAbr
    {
        // cache para titulos
        private static Dictionary<string, string> _masc = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
        private static Dictionary<string, string> _fem = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

        private static Dictionary<string, string> _titulos = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);

        #region Titulos

        static TituloAbr()
        {
            Add("Engenheiro Ambiental", "Engenheira Ambiental", "Eng. Amb.");
            Add("Engenheiro Civil", "Engenheira Civil", "Eng. Civ.");
            Add("Engenheiro de Fortificação e Construção", "Engenheira de Fortificação e Construção", "Eng. Fort. Constr.");
            Add("Engenheiro de Operação - Construção Civil", "Engenheira de Operação - Construção Civil", "Eng. Oper. Constr. Civ.");
            Add("Engenheiro de Operação - Construção de Estradas", "Engenheira de Operação - Construção de Estradas", "Eng. Oper. Constr. Estr.");
            Add("Engenheiro de Operação - Edificações", "Engenheira de Operação - Edificações", "Eng. Oper. Edif.");
            Add("Engenheiro de Operação - Estradas", "Engenheira de Operação - Estradas", "Eng. Oper. Estr.");
            Add("Engenheiro Industrial - Civil", "Engenheira Industrial - Civil", "Eng. Ind. Civ.");
            Add("Engenheiro Militar", "Engenheira Militar", "Eng. Mil.");
            Add("Engenheiro Rodoviário", "Engenheira Rodoviária", "Eng. Rodov.");
            Add("Engenheiro Sanitarista", "Engenheira Sanitarista", "Eng. Sanit.");
            Add("Engenheiro Sanitarista e Ambiental", "Engenheira Sanitarista e Ambiental", "Eng. Sanit. Amb.");
            Add("Engenheiro de Infra-Estrutura Aeronáutica", "Engenheira de Infra-Estrutura Aeronáutica", "Eng. Infra-Estrut. Aeron.");
            Add("Engenheiro de Produção - Civil", "Engenheira de Produção - Civil", "Eng. Prod. Civ.");
            Add("Engenheiro Hídrico", "Engenheira Hídrica", "Eng. Hidr.");
            Add("Urbanista", "Urbanista", "Urb.");
            Add("Tecnólogo em Construção Civil", "Tecnóloga em Construção Civil", "Tecg. Constr. Civ.");
            Add("Tecnólogo em Construção Civil - Edificações", "Tecnóloga em Construção Civil - Edificações", "Tecg. Constr. Civ. Edif.");
            Add("Tecnólogo em Construção Civil - Estrada e Topografia", "Tecnóloga em Construção Civil - Estrada e Topografia", "Tecg. Constr. Civ. Estr. Topogr.");
            Add("Tecnólogo em Construção Civil - Movimento de Terra e", "Tecnóloga em Construção Civil - Movimento de Terra e", "Tecg. Constr. Civ. Mov. Terra Pav.");
            Add("Tecnólogo em Construção Civil - Obras de Solos", "Tecnóloga em Construção Civil - Obras de Solos", "Tecg. Constr. Civ. Obr. Solos");
            Add("Tecnólogo em Construção Civil - Obras Hidráulicas", "Tecnóloga em Construção Civil - Obras Hidráulicas", "Tecg. Constr. Civ. Obr. Hidr.");
            Add("Tecnólogo em Construção Civil - Terraplenagem", "Tecnóloga em Construção Civil - Terraplenagem", "Tecg. Constr. Civ. Terrapl.");
            Add("Tecnólogo em Edificações", "Tecnóloga em Edificações", "Tecg. Edif.");
            Add("Tecnólogo em Estradas", "Tecnóloga em Estradas", "Tecg. Estr.");
            Add("Tecnólogo em Operação e Administração de Sistemas de", "Tecnóloga em Operação e Administração de Sistemas de", "Tecg. Oper. Adm. Naveg. Fluv.");
            Add("Tecnólogo em Saneamento", "Tecnóloga em Saneamento", "Tecg. Saneam.");
            Add("Tecnólogo em Saneamento Ambiental", "Tecnóloga em Saneamento Ambiental", "Tecg. Saneam. Amb.");
            Add("Tecnólogo em Saneamento Básico", "Tecnóloga em Saneamento Básico", "Tecg. Saneam. Básico");
            Add("Tecnólogo em Controle de Obras", "Tecnóloga em Controle de Obras", "Tecg. Contr. Obras");
            Add("Tecnólogo em Transporte Terrestre - Urbano", "Tecnóloga em Transporte Terrestre - Urbano", "Tecg. Transp. Terr. Urb.");
            Add("Tecnólogo em Processos Ambientais", "Tecnóloga em Processos Ambientais", "Tecg. Proc. Amb.");
            Add("Tecnólogo em Gestão Ambiental", "Tecnóloga em Gestão Ambiental", "Tecg. Gest. Amb.");
            Add("Técnico em Construção Civil", "Técnica em Construção Civil", "Tec. Constr. Civ.");
            Add("Técnico em Desenho de Construção Civil", "Técnica em Desenho de Construção Civil", "Tec. Des. Constr. Civ.");
            Add("Técnico em Desenho de Projetos", "Técnica em Desenho de Projetos", "Tec. Des. Proj.");
            Add("Técnico em Edificações", "Técnica em Edificações", "Tec. Edif.");
            Add("Técnico em Estradas", "Técnica em Estradas", "Tec. Estr.");
            Add("Técnico em Estradas e Pontes", "Técnica em Estradas e Pontes", "Tec. Estr. Pontes");
            Add("Técnico em Hidrologia", "Técnica em Hidrologia", "Tec. Hidrol.");
            Add("Técnico em Saneamento", "Técnica em Saneamento", "Tec. Saneam.");
            Add("Técnico em Transportes Rodoviários", "Técnica em Transportes Rodoviários", "Tec. Transp. Rodov.");
            Add("Técnico em Meio Ambiente", "Técnica em Meio Ambiente", "Tec. Meio Amb.");
            Add("Técnico Desenhista de Arquitetura", "Técnica Desenhista de Arquitetura", "Tec. Des. Arq.");
            Add("Técnico em Decoração", "Técnica em Decoração", "Tec. Decor.");
            Add("Técnico em Maquetaria", "Técnica em Maquetaria", "Tec. Maquet.");
            Add("Técnico em Trânsito", "Técnica em Trânsito", "Tec. Tran.");
            Add("Engenheiro de Computação", "Engenheira de Computação", "Eng. Comp.");
            Add("Engenheiro de Comunicações", "Engenheira de Comunicação", "Eng. Comunic.");
            Add("Engenheiro de Controle e Automação", "Engenheira de Controle e Automação", "Eng. Contr. Autom.");
            Add("Engenheiro de Operação - Eletrônica", "Engenheira de Operação - Eletrônica", "Eng. Oper. Eletron.");
            Add("Engenheiro de Operação - Eletrotécnica", "Engenheira de Operação - Eletrotécnica", "Eng. Oper. Eletrotec.");
            Add("Engenheiro de Operação - Telecomunicações", "Engenheira de Operação - Telecomunicações", "Eng. Oper. Telecom.");
            Add("Engenheiro de Produção - Eletricista", "Engenheira de Produção - Eletricista", "Eng. Prod. Eletr.");
            Add("Engenheiro de Telecomunicações", "Engenheira de Telecomunicações", "Eng. Telecom.");
            Add("Engenheiro de Transmissão", "Engenheira de Transmissão", "Eng. Transm.");
            Add("Engenheiro Eletricista", "Engenheira Eletricista", "Eng. Eletric.");
            Add("Engenheiro Eletricista - Eletrônica", "Engenheira Eletricista - Eletrônica", "Eng. Eletric. Eletron.");
            Add("Engenheiro Eletricista - Eletrotécnica", "Engenheira Eletricista - Eletrotécnica", "Eng. Eletric. Eletrotec.");
            Add("Engenheiro em Eletrônica", "Engenheira em Eletrônica", "Eng. Eletron.");
            Add("Engenheiro em Eletrotécnica", "Engenheira em Eletrotécnica", "Eng. Eletrotec.");
            Add("Engenheiro Industrial - Elétrica", "Engenheira Industrial - Elétrica", "Eng. Ind. Eletr.");
            Add("Engenheiro Industrial - Eletrônica", "Engenheira Industrial - Eletrônica", "Eng. Ind. Eletron.");
            Add("Engenheiro Industrial - Eletrotécnica", "Engenheira Industrial - Eletrotécnica", "Eng. Ind. Eletrotec.");
            Add("Engenheiro Industrial - Telecomunicações", "Engenheira Industrial - Telecomunicações", "Eng. Ind. Telecom.");
            Add("Engenheiro Biomédico", "Engenheira Biomédica", "Eng. Biomed.");
            Add("Tecnólogo em Automação Industrial", "Tecnóloga em Automação Industrial", "Tecg. Autom. Ind.");
            Add("Tecnólogo em Distribuição de Energia Elétrica", "Tecnóloga em Distribuição de Energia Elétrica", "Tecg. Distr. Energ. Eletr.");
            Add("Tecnólogo em Eletricidade", "Tecnóloga em Eletricidade", "Tecg. Eletricid.");
            Add("Tecnólogo em Eletrônica", "Tecnóloga em Eletrônica", "Tecg. Eletron.");
            Add("Tecnólogo em Eletrônica Industrial", "Tecnóloga em Eletrônica Industrial", "Tecg. Eletron. Ind.");
            Add("Tecnólogo em Instrumentação e Controle", "Tecnóloga em Instrumentação e Controle", "Tecg. Instr. Contr.");
            Add("Tecnólogo em Máquinas Elétricas", "Tecnóloga em Máquinas Elétricas", "Tecg. Maq. Eletr.");
            Add("Tecnólogo em Sistemas Elétricos", "Tecnóloga em Sistemas Elétricos", "Tecg. Sist. Eletr.");
            Add("Tecnólogo em Técnicas Digitais", "Tecnóloga em Técnicas Digitais", "Tecg. Tec. Dig.");
            Add("Tecnólogo em Telecomunicações", "Tecnóloga em Telecomunicações", "Tecg. Telecom.");
            Add("Tecnólogo em Telecomunicações - Telefonia e Redes Externas", "Tecnóloga em Telecomunicações - Telefonia e Redes Externas", "Tecg. Telecom. Telef. Redes Ext.");
            Add("Tecnólogo em Sistemas de Telefonia", "Tecnóloga em Sistemas de Telefonia", "Tecg. Sist. Telef.");
            Add("Tecnólogo em Transmissão e Distribuição Elétrica", "Tecnóloga em Transmissão e Distribuição Elétrica", "Tecg. Transm. Distr. Eletr.");
            Add("Tecnólogo em Redes de Computadores", "Tecnóloga em Redes de Computadores", "Tecg. Redes Comp.");
            Add("Tecnólogo em Sistemas de Comunicação sem Fio", "Tecnóloga em Sistemas de Comunicação sem Fio", "Tecg. Sist. Comunic. Sem Fio");
            Add("Tecnólogo em Eletrotécnica Industrial", "Tecnóloga em Eletrotécnica Industrial", "Tecg. Eletrotec. Ind.");
            Add("Técnico em Automação Industrial", "Técnica em Automação Industrial", "Tec. Autom. Ind.");
            Add("Técnico em Automação Industrial Eletrônica", "Técnica em Automação Industrial Eletrônica", "Tec. Autom. Ind. Eletron.");
            Add("Técnico em Eletricidade", "Técnica em Eletricidade", "Tec. Eletric.");
            Add("Técnico em Eletromecânica", "Técnica em Eletromecânica", "Tec. Eletromec.");
            Add("Técnico em Eletrônica", "Técnica em Eletrônica", "Tec. Eletron.");
            Add("Técnico em Eletrônica - Telecomunicações", "Técnica em Eletrônica - Telecomunicações", "Tec. Eletron. Telecom.");
            Add("Técnico em Eletrotécnica", "Técnica em Eletrotécnica", "Tec. Eletrotec.");
            Add("Técnico em Informática Industrial", "Técnica em Informática Industrial", "Tec. Inform. Ind.");
            Add("Técnico em Instrumentação", "Técnica em Instrumentação", "Tec. Instrum.");
            Add("Técnico em Microinformática", "Técnica em Microinformática", "Tec. Microinform.");
            Add("Técnico em Proteção Radiológica", "Técnica em Proteção Radiológica", "Tec. Prot. Radiol.");
            Add("Técnico em Telecomunicações", "Técnica em Telecomunicações", "Tec. Telecom.");
            Add("Técnico em Telefonia", "Técnica em Telefonia", "Tec. Telef.");
            Add("Técnico em Mecatrônica", "Técnica em Mecatrônica", "Tec. Mecatron.");
            Add("Técnico em Eletroeletrônica", "Técnica em Eletroeletrônica", "Tec. Eletroeletron.");
            Add("Técnico em Manutenção de Computadores", "Técnica em Manutenção de Computadores", "Tec. Manut. Computad.");
            Add("Técnico em Redes de Comunicação", "Técnica em Redes de Comunicação", "Tec. Redes Comunic.");
            Add("Engenheiro Aeronáutico", "Engenheira Aeronáutica", "Eng. Aeron.");
            Add("Engenheiro Mecânico e de Armamento", "Engenheira Mecânica e de Armamento", "Eng. Mec. Armam.");
            Add("Engenheiro Mecânico e de Automóvel", "Engenheira Mecânica e de Automóvel", "Eng. Mec. Auto.");
            Add("Engenheiro de Operação - Aeronáutica", "Engenheira de Operação - Aeronáutica", "Eng. Oper. Aeron.");
            Add("Engenheiro de Operação - Fabricação Mecânica", "Engenheira de Operação - Fabricação Mecânica", "Eng. Oper. Fabric. Mec.");
            Add("Engenheiro de Operação - Indústria da Madeira", "Engenheira de Operação - Indústria da Madeira", "Eng. Oper. Ind. Mad.");
            Add("Engenheiro de Operação - Máquinas e Motores", "Engenheira de Operação - Máquinas e Motores", "Eng. Oper. Maq. Motores");
            Add("Engenheiro de Operação - Mecânica", "Engenheira de Operação - Mecânica", "Eng. Oper. Mec.");
            Add("Engenheiro de Operação - Mecânica Automobilística", "Engenheira de Operação - Mecânica Automobilística", "Eng. Oper. Mec. Auto.");
            Add("Engenheiro de Operação - Mecânica de Manutenção", "Engenheira de Operação - Mecânica de Manutenção", "Eng. Oper. Mec. Manut.");
            Add("Engenheiro de Operação - Mecânica de Máquinas e Ferramentas", "Engenheira de Operação - Mecânica de Máquinas e Ferramentas", "Eng. Oper. Mec. Maq. Ferram.");
            Add("Engenheiro de Operação - Metalurgista", "Engenheira de Operação - Metalurgista", "Eng. Oper. Metal.");
            Add("Engenheiro de Operação - Processo de Fabricação Mecânica", "Engenheira de Operação - Processo de Fabricação Mecânica", "Eng. Oper. Proc. Fab. Mec.");
            Add("Engenheiro de Operação - Produção", "Engenheira de Operação - Produção", "Eng. Oper. Prod.");
            Add("Engenheiro de Operação - Refrigeração e Ar Condicionado", "Engenheira de Operação - Refrigeração e Ar Condicionado", "Eng. Oper. Refrig. Ar Cond.");
            Add("Engenheiro de Operação - Siderurgia", "Engenheira de Operação - Siderurgia", "Eng. Oper. Siderur.");
            Add("Engenheiro de Produção", "Engenheira de Produção", "Eng. Prod.");
            Add("Engenheiro de Produção - Mecânica", "Engenheira de Produção - Mecânica", "Eng. Prod. Mec.");
            Add("Engenheiro de Produção - Metalurgista", "Engenheira de Produção - Metalurgista", "Eng. Prod. Metal.");
            Add("Engenheiro de Produção - Agroindústria", "Engenheira de Produção - Agroindústria", "Eng. Prod. Agroind.");
            Add("Engenheiro Industrial - Madeira", "Engenheira Industrial - Madeira", "Eng. Ind. Mad.");
            Add("Engenheiro Industrial - Mecânica", "Engenheira Industrial - Mecânica", "Eng. Ind. Mec.");
            Add("Engenheiro Industrial - Metalurgia", "Engenheira Industrial - Metalurgia", "Eng. Ind. Metal.");
            Add("Engenheiro Mecânico", "Engenheira Mecânica", "Eng. Mec.");
            Add("Engenheiro Mecânico - Automação e Sistemas", "Engenheira Mecânica - Automação e Sistemas", "Eng. Mec. - Autom. Sist.");
            Add("Engenheiro Metalurgista", "Engenheira Metalurgista", "Eng. Metal.");
            Add("Engenheiro Naval", "Engenheira Naval", "Eng. Naval");
            Add("Engenheiro Mecânico Eletricista", "Engenheira Mecânica Eletricista", "Eng. Mec. Eletric.");
            Add("Tecnólogo em Aeronaves", "Tecnóloga em Aeronaves", "Tecg. Aeronav.");
            Add("Tecnólogo em Construção Naval", "Tecnóloga em Construção Naval", "Tecg. Constr. Naval");
            Add("Tecnólogo em Eletromecânica", "Tecnóloga em Eletromecânica", "Tecg. Eletromec.");
            Add("Tecnólogo em Indústria da Madeira", "Tecnóloga em Indústria da Madeira", "Tecg. Ind. Mad.");
            Add("Tecnólogo em Manutenção de Máquinas e Equipamentos", "Tecnóloga em Manutenção de Máquinas e Equipamentos", "Tecg. Manut. Maq. Equip.");
            Add("Tecnólogo em Máquinas", "Tecnóloga em Máquinas", "Tecg. Maq.");
            Add("Tecnólogo em Máquinas e Equipamentos", "Tecnóloga em Máquinas e Equipamentos", "Tecg. Maq. Equip.");
            Add("Tecnólogo em Mecânica", "Tecnóloga em Mecânica", "Tecg. Mec.");
            Add("Tecnólogo em Mecânica - Automobilismo", "Tecnóloga em Mecânica - Automobilismo", "Tecg. Mec. Auto.");
            Add("Tecnólogo em Mecânica - Desenhista Projetista", "Tecnóloga em Mecânica - Desenhista Projetista", "Tecg. Mec. Des. Proj.");
            Add("Tecnólogo em Mecânica - Oficinas", "Tecnóloga em Mecânica - Oficinas", "Tecg. Mec. Ofic.");
            Add("Tecnólogo em Mecânica - Produção Industrial de Móveis", "Tecnóloga em Mecânica - Produção Industrial de Móveis", "Tecg. Mec. Prod. Ind.");
            Add("Tecnólogo em Mecânica - Soldagem", "Tecnóloga em Mecânica - Soldagem", "Tecg. Mec. Sold.");
            Add("Tecnólogo em Mecânica - Processos Industriais", "Tecnóloga em Mecânica - Processos Industriais", "Tecg. Mec. Proc. Ind.");
            Add("Tecnólogo em Mecânica, Oficina e Manutenção", "Tecnóloga em Mecânica, Oficina e Manutenção", "Tecg. Mec. Ofic. Manut.");
            Add("Tecnólogo em Metalurgia", "Tecnóloga em Metalurgia", "Tecg. Metal.");
            Add("Tecnólogo em Processo de Produção e Usinagem", "Tecnóloga em Processo de Produção e Usinagem", "Tecg. Proc. Prod. Usinag.");
            Add("Tecnólogo em Produção de Calçados", "Tecnóloga em Produção de Calçados", "Tecg. Prod. Calçados");
            Add("Tecnólogo em Produção de Couro", "Tecnóloga em Produção de Couro", "Tecg. Prod. Couro");
            Add("Tecnólogo em Siderúrgica", "Tecnóloga em Siderúrgica", "Tecg. Siderur.");
            Add("Tecnólogo em Soldagem", "Tecnóloga em Soldagem", "Tecg. Sold.");
            Add("Tecnólogo Naval", "Tecnóloga Naval", "Tecg. Naval");
            Add("Tecnólogo em Qualidade Total", "Tecnóloga em Qualidade Total", "Tecg. Qualid. Total");
            Add("Tecnólogo em Mecatrônica Industrial", "Tecnóloga em Mecatrônica Industrial", "Tecg. Mecatron. Ind.");
            Add("Tecnólogo em Gestão da Produção Industrial", "Tecnóloga em Gestão da Produção Industrial", "Tecg. Gest. Prod. Ind.");
            Add("Tecnólogo em Fabricação Mecânica", "Tecnóloga em Fabricação Mecânica", "Tecg. Fab. Mec.");
            Add("Técnico Desenhista de Máquinas", "Técnica Desenhista de Máquinas", "Tec. Des. Maq.");
            Add("Técnico em Aeronáutica", "Técnica em Aeronáutica", "Tec. Aeron.");
            Add("Técnico em Aeronaves", "Técnica em Aeronaves", "Tec. Aeronav.");
            Add("Técnico em Calçados", "Técnica em Calçados", "Tec. Calçados");
            Add("Técnico em Construção de Máquinas e Motores", "Técnica em Construção de Máquinas e Motores", "Tec. Constr. Maq. Mot.");
            Add("Técnico em Construção Naval", "Técnica em Construção Naval", "Tec. Constr. Naval");
            Add("Técnico em Estruturas Navais", "Técnica em Estruturas Navais", "Tec. Estr. Navais");
            Add("Técnico em Fundição", "Técnica em Fundição", "Tec. Fund.");
            Add("Técnico em Manutenção de Aeronaves", "Técnica em Manutenção de Aeronaves", "Tec. Manut. Areonav.");
            Add("Técnico em Máquinas", "Técnica em Máquinas", "Tec. Maq.");
            Add("Técnico em Máquinas e Motores", "Técnica em Máquinas e Motores", "Tec. Maq. Mot.");
            Add("Técnico em Máquinas Navais", "Técnica em Máquinas Navais", "Tec. Maq. Navais");
            Add("Técnico em Mecânica", "Técnica em Mecânica", "Tec. Mec.");
            Add("Técnico em Mecânica de Precisão", "Técnica em Mecânica de Precisão", "Tec. Mec. Prec.");
            Add("Técnico em Metalurgia", "Técnica em Metalurgia", "Tec. Metal.");
            Add("Técnico em Náutica", "Técnica em Náutica", "Tec. Naut.");
            Add("Técnico em Operações de Reatores", "Técnica em Operações de Reatores", "Tec. Oper. Reat.");
            Add("Técnico em Refrigeração e Ar Condicionado", "Técnica em Refrigeração e Ar Condicionado", "Tec. Refrig. Ar Cond.");
            Add("Técnico em Siderurgia", "Técnica em Siderurgia", "Tec. Siderur.");
            Add("Técnico em Soldagem", "Técnica em Soldagem", "Tec. Sold.");
            Add("Técnico em Usinagem Mecânica", "Técnica em Usinagem Mecânica", "Tec. Usinag. Mec.");
            Add("Técnico Naval", "Técnica Naval", "Tec. Naval");
            Add("Técnico em Metrologia", "Técnica em Metrologia", "Tec. Metrol.");
            Add("Técnico em Qualidade e Produtividade", "Técnica em Qualidade e Produtividade", "Tec. Qualid. Prod.");
            Add("Técnico em Tecnologias Finais do Gás", "Técnica em Tecnologias Finais do Gás", "Tec. Tecnol. Finais do Gás");
            Add("Técnico em Desenho de Projetos - Mecânica", "Técnica em Desenho de Projetos - Mecânica", "Tec. Des. Proj. - Mec.");
            Add("Técnico em Montagem e Manut. de Sistemas de Gás", "Técnica em Montagem e Manut. de Sistemas de Gás", "Tec. Mont. Manut. Sist. Gás Comb.");
            Add("Técnico em Móveis", "Técnica em Móveis", "Tec. Móveis");
            Add("Técnico em Manutenção Automotiva", "Técnica em Manutenção Automotiva", "Tec. Manut. Automot.");
            Add("Engenheiro de Alimentos", "Engenheira de Alimentos", "Eng. Alim.");
            Add("Engenheiro de Materiais", "Engenheira de Materiais", "Eng. Mat.");
            Add("Engenheiro de Operação - Petroquímica", "Engenheira de Operação - Petroquímica", "Eng. Oper. Petroq.");
            Add("Engenheiro de Operação - Química", "Engenheira de Operação - Química", "Eng. Oper. Quim.");
            Add("Engenheiro de Operação - Têxtil", "Engenheira de Operação - Têxtil", "Eng. Oper. Têxtil");
            Add("Engenheiro de Produção - Materiais", "Engenheira de Produção - Materiais", "Eng. Prod. Mat.");
            Add("Engenheiro de Produção - Química", "Engenheira de Produção - Química", "Eng. Prod. Quim.");
            Add("Engenheiro de Produção - Têxtil", "Engenheira de Produção - Têxtil", "Eng. Prod. Têxtil");
            Add("Engenheiro Industrial - Química", "Engenheira Industrial - Química", "Eng. Ind. Quim.");
            Add("Engenheiro Químico", "Engenheira Química", "Eng. Quím.");
            Add("Engenheiro Têxtil", "Engenheira Têxtil", "Eng. Têxtil");
            Add("Engenheiro de Petróleo", "Engenheira de Petróleo", "Eng. Petrol.");
            Add("Engenheiro de Plástico", "Engenheira de Plástico", "Eng. Plast.");
            Add("Engenheiro Bioquímico", "Engenheira Bioquímica", "Eng. Bioquím.");
            Add("Tecnólogo em Alimentos", "Tecnóloga em Alimentos", "Tecg. Alim.");
            Add("Tecnólogo em Cerâmica", "Tecnóloga em Cerâmica", "Tecg. Cer.");
            Add("Tecnólogo em Indústria Têxtil", "Tecnóloga em Indústria Têxtil", "Tecg. Ind. Têxtil");
            Add("Tecnólogo em Materiais", "Tecnóloga em Materiais", "Tecg. Mat.");
            Add("Tecnólogo em Processos Petroquímicos", "Tecnóloga em Processos Petroquímicos", "Tecg. Proc. Petroq.");
            Add("Tecnólogo em Química", "Tecnóloga em Química", "Tecg. Quim.");
            Add("Tecnólogo Têxtil", "Tecnóloga Têxtil", "Tecg. Têxtil");
            Add("Tecnólogo em Petróleo e Gás", "Tecnóloga em Petróleo e Gás", "Tecg. Petrol. Gás");
            Add("Tecnólogo em Polímeros", "Tecnóloga em Polímeros", "Tecg. Polim.");
            Add("Tecnólogo em Produção de Vestuário", "Tecnóloga em Produção de Vestuário", "Tecg. Prod. Vest.");
            Add("Técnico em Alimentos", "Técnica em Alimentos", "Tec. Alim.");
            Add("Técnico em Borracha", "Técnica em Borracha", "Tec. Borrac.");
            Add("Técnico em Celulose", "Técnica em Celulose", "Tec. Celulose");
            Add("Técnico em Celulose e Papel", "Técnica em Celulose e Papel", "Tec. Celulose Papel");
            Add("Técnico em Cerâmica", "Técnica em Cerâmica", "Tec. Cer.");
            Add("Técnico em Cerveja e Refrigerantes", "Técnica em Cerveja e Refrigerantes", "Tec. Cerv. Refrig.");
            Add("Técnico em Fiação", "Técnica em Fiação", "Tec. Fiação");
            Add("Técnico em Fiação e Tecelagem", "Técnica em Fiação e Tecelagem", "Tec. Fiação Tecel.");
            Add("Técnico em Malharia", "Técnica em Malharia", "Tec. Malharia");
            Add("Técnico em Papel", "Técnica em Papel", "Tec. Papel");
            Add("Técnico em Petroquímica", "Técnica em Petroquímica", "Tec. Petroq.");
            Add("Técnico em Plástico", "Técnica em Plástico", "Tec. Plast.");
            Add("Técnico em Química", "Técnica em Química", "Tec. Quim.");
            Add("Técnico em Tecelagem", "Técnica em Tecelagem", "Tec. Tecel.");
            Add("Técnico em Vestuário", "Técnica em Vestuário", "Tec. Vest.");
            Add("Técnico Têxtil", "Técnica Têxtil", "Tec. Têxtil");
            Add("Técnico em Cervejaria", "Técnica em Cervejaria", "Tec. Cerv.");
            Add("Técnico em Controle de Qualidade de Alimentos", "Técnica em Controle de Qualidade de Alimentos", "Tec. Contrl. Qualid. Alim.");
            Add("Técnico em Processamento de Frutas e Hortaliças", "Técnica em Processamento de Frutas e Hortaliças", "Tec. Processam. Frutas Hortal.");
            Add("Técnico em Materiais", "Técnica em Materiais", "Tec. Mat.");
            Add("Técnico em Petróleo e Gás", "Técnica em Petróleo e Gás", "Tec. Petrol. Gás");
            Add("Técnico em Curtimento", "Técnica em Curtimento", "Tec. Curt.");
            Add("Técnico em Processamento de Pescado", "Técnica em Processamento de Pescado", "Tec. Processam. Pesc.");
            Add("Engenheiro de Minas", "Engenheira de Minas", "Eng. Minas");
            Add("Engenheiro Geólogo", "Engenheira Geóloga", "Eng. Geol.");
            Add("Geólogo", "Geóloga", "Geol.");
            Add("Engenheiro de Exploração e Produção de Petróleo", "Engenheira de Exploração e Produção de Petróleo", "Eng. Expl. Prod. Petrol.");
            Add("Tecnólogo de Minas", "Tecnóloga de Minas", "Tecg. Minas");
            Add("Tecnólogo em Manutenção Petroquímica", "Tecnóloga em Manutenção Petroquímica", "Tecg. Manut. Petroq.");
            Add("Tecnólogo em Rochas Ornamentais", "Tecnóloga em Rochas Ornamentais", "Tecg. Roc. Ornam.");
            Add("Técnico em Geologia", "Técnica em Geologia", "Tec. Geol.");
            Add("Técnico em Mineração", "Técnica em Mineração", "Tec. Miner.");
            Add("Técnico em Perfuração de Poços", "Técnica em Perfuração de Poços", "Tec. Perf. Poços");
            Add("Agrimensor", "Agrimensora", "Agrim.");
            Add("Engenheiro Agrimensor", "Engenheira Agrimensora", "Eng. Agrim.");
            Add("Engenheiro Cartógrafo", "Engenheira Cartógrafa", "Eng. Cartog.");
            Add("Engenheiro de Geodésia", "Engenheira de Geodésia", "Eng. Geod.");
            Add("Engenheiro em Topografia Rural", "Engenheira em Topografia Rural", "Eng. Topog. Rural");
            Add("Engenheiro Geógrafo", "Engenheira Geógrafa", "Eng. Geog.");
            Add("Engenheiro Topógrafo", "Engenheira Topógrafa", "Eng. Topog.");
            Add("Geógrafo", "Geógrafa", "Geog.");
            Add("Tecnólogo em Topografia", "Tecnóloga em Topografia", "Tecg. Topog.");
            Add("Tecnólogo em Geoprocessamento", "Tecnóloga em Geoprocessamento", "Tecg. Geoproc.");
            Add("Tecnólogo em Agrimensura", "Tecnóloga em Agrimensura", "Tecg. Agrim.");
            Add("Técnico em Agrimensura", "Técnica em Agrimensura", "Tec. Agrim.");
            Add("Técnico em Fotogrametria", "Técnica em Fotogrametria", "Tec. Fotogram.");
            Add("Técnico em Geodésia e Cartografia", "Técnica em Geodésia e Cartografia", "Tec. Geod. Cartog.");
            Add("Técnico em Topografia", "Técnica em Topografia", "Tec. Topog.");
            Add("Técnico em Geomensura", "Técnica em Geomensura", "Tec. Geom.");
            Add("Engenheiro Agrícola", "Engenheira Agrícola", "Eng. Agric.");
            Add("Engenheiro Agrônomo", "Engenheira Agrônoma", "Eng. Agr.");
            Add("Engenheiro de Pesca", "Engenheira de Pesca", "Eng. Pesca");
            Add("Engenheiro Florestal", "Engenheira Florestal", "Eng. Ftal.");
            Add("Meteorologista", "Meteorologista", "Meteorol.");
            Add("Engenheiro de Aqüicultura", "Engenheira de Aqüicultura", "Eng. Aqüicult.");
            Add("Tecnólogo em Açúcar e Álcool", "Tecnóloga em Açúcar e Álcool", "Tecg. Açúcar Alc.");
            Add("Tecnólogo em Administração Rural", "Tecnóloga em Administração Rural", "Tecg. Adm. Rural");
            Add("Tecnólogo em Agricultura", "Tecnóloga em Agricultura", "Tecg. Agric.");
            Add("Tecnólogo em Agronomia", "Tecnóloga em Agronomia", "Tecg. Agr.");
            Add("Tecnólogo em Agropecuária", "Tecnóloga em Agropecuária", "Tecg. Agropec.");
            Add("Tecnólogo em Aqüicultura", "Tecnóloga em Aqüicultura", "Tecg. Aqüicult.");
            Add("Tecnólogo em Bovinocultura", "Tecnóloga em Bovinocultura", "Tecg. Bovin.");
            Add("Tecnólogo em Ciências Agrárias", "Tecnóloga em Ciências Agrárias", "Tecg. Cienc. Agrar.");
            Add("Tecnólogo em Cooperativismo", "Tecnóloga em Cooperativismo", "Tecg. Cooperat.");
            Add("Tecnólogo em Curtumes e Tanantes", "Tecnóloga em Curtumes e Tanantes", "Tecg. Curt. Tanant.");
            Add("Tecnólogo em Fitotecnia", "Tecnóloga em Fitotecnia", "Tecg. Fitotec.");
            Add("Tecnólogo em Fruticultura", "Tecnóloga em Fruticultura", "Tecg. Fruticult.");
            Add("Tecnólogo em Fruticultura de Clima Temperado", "Tecnóloga em Fruticultura de Clima Temperado", "Tecg. Fruticult. Clima Temp.");
            Add("Tecnólogo em Heveicultura", "Tecnóloga em Heveicultura", "Tecg. Heveicult.");
            Add("Tecnólogo em Laticínios", "Tecnóloga em Laticínios", "Tecg. Latic.");
            Add("Tecnólogo em Mecanização Agrícola", "Tecnóloga em Mecanização Agrícola", "Tecg. Mec. Agric.");
            Add("Tecnólogo em Meteorologia", "Tecnóloga em Meteorologia", "Tecg. Meteorol.");
            Add("Tecnólogo em Pecuária", "Tecnóloga em Pecuária", "Tecg. Pec.");
            Add("Tecnólogo Industrial de Açúcar de Cana", "Tecnóloga Industrial de Açúcar de Cana", "Tecg. Ind. Açúcar Cana");
            Add("Tecnólogo em Recursos Hídricos e Irrigação", "Tecnóloga em Recursos Hídricos e Irrigação", "Tecg. Rec. Hidr. Irrig.");
            Add("Tecnólogo em Horticultura", "Tecnóloga em Horticultura", "Tecg. Hortic.");
            Add("Tecnólogo em Irrigação e Drenagem", "Tecnóloga em Irrigação e Drenagem", "Tecg. Irrig. Drenag.");
            Add("Tecnólogo em Agroindústria", "Tecnóloga em Agroindústria", "Tecg. Agroind.");
            Add("Tecnólogo em Agroecologia", "Tecnóloga em Agroecologia", "Tecg. Agroecol.");
            Add("Tecnólogo em Viticultura e Enologia", "Tecnóloga em Viticultura e Enologia", "Tecg. Vit. Enol.");
            Add("Tecnólogo em Cafeicultura", "Tecnóloga em Cafeicultura", "Tecg. Cafeicult.");
            Add("Tecnológo em Silvicultura", "Tecnóloga em Silvicultura", "Tecg. Silvicult.");
            Add("Tecnólogo em Paisagismo e Jardinagem", "Tecnóloga em Paisagismo e Jardinagem", "Tecg. Paisag. Jard.");
            Add("Tecnólogo em Produção de Grãos", "Tecnóloga em Produção de Grãos", "Tecg. Prod. Grãos");
            Add("Tecnólogo em Agronegócios", "Tecnóloga em Agronegócios", "Tecg. Agroneg.");
            Add("Técnico Agrícola", "Técnica Agrícola", "Tec. Agric.");
            Add("Técnico em Agroindústria", "Técnica em Agroindústria", "Tec. Agroind.");
            Add("Técnico em Açúcar e Álcool", "Técnica em Açúcar e Álcool", "Tec. Açúcar Alc.");
            Add("Técnico em Agricultura", "Técnica em Agricultura", "Tec. Agricult.");
            Add("Técnico em Agropecuária", "Técnica em Agropecuária", "Tec. Agropec.");
            Add("Técnico em Aqüicultura", "Técnica em Aqüicultura", "Tec. Aqüicult.");
            Add("Técnico em Beneficiamento de Madeira", "Técnica em Beneficiamento de Madeira", "Tec. Benef. Mad.");
            Add("Técnico em Bovinocultura", "Técnica em Bovinocultura", "Tec. Bovinocult.");
            Add("Técnico em Carnes e Derivados", "Técnica em Carnes e Derivados", "Tec. Carnes Deriv.");
            Add("Técnico em Cooperativismo", "Técnica em Cooperativismo", "Tec. Cooperat.");
            Add("Técnico em Enologia", "Técnica em Enologia", "Tec. Enol.");
            Add("Técnico em Frutas e Hortaliças", "Técnica em Frutas e Hortaliças", "Tec. Frutas Hortal.");
            Add("Técnico em Horticultura", "Técnica em Horticultura", "Tec. Hortic.");
            Add("Técnico em Irrigação e Drenagem", "Técnica em Irrigação e Drenagem", "Tec. Irrig. Drenag.");
            Add("Técnico em Laticínios", "Técnica em Laticínios", "Tec. Latic.");
            Add("Técnico em Meteorologia", "Técnica em Meteorologia", "Tec. Meteorol.");
            Add("Técnico em Pecuária", "Técnica em Pecuária", "Tec. Pec.");
            Add("Técnico em Pesca", "Técnica em Pesca", "Tec. Pesca");
            Add("Técnico em Piscicultura", "Técnica em Piscicultura", "Tec. Piscicult.");
            Add("Técnico Florestal", "Técnica Florestal", "Tec. Ftal.");
            Add("Técnico Rural", "Técnica Rural", "Tec. Rural");
            Add("Técnico em Cafeicultura", "Técnica em Cafeicultura", "Tec. Cafeicult.");
            Add("Técnico em Zootecnia", "Técnica em Zootecnia", "Tec. Zootec.");
            Add("Técnico em Jardinagem", "Técnica em Jardinagem", "Tec. Jard.");
            Add("Técnico em Infra-Estrutura Rural", "Técnica em Infra-Estrutura Rural", "Tec. Infra-Estrut. Rural");
            Add("Técnico em Paisagismo", "Técnica em Paisagismo", "Tec. Paisag.");
            Add("Técnico em Agroecologia", "Técnica em Agroecologia", "Tec. Agroecol.");
            Add("Técnico em Agronegócio", "Técnica em Agronegócio", "Tec. Agroneg.");
            Add("Técnico em Fruticultura", "Técnica em Fruticultura", "Tec. Fruticult.");
            Add("Tecnólogo de Segurança do Trabalho", "Tecnóloga de Segurança do Trabalho", "Tecg. Seg. Trab.");
            Add("Técnico de Segurança do Trabalho", "Técnica de Segurança do Trabalho", "Tec. Seg. Trab.");
            Add("Engenheiro de Segurança do Trabalho", "Engenheira de Segurança do Trabalho", "Eng. Seg. Trab.");
        }

        #endregion

        public static string Abreviar(string noTitulo)
        {
            if (string.IsNullOrWhiteSpace(noTitulo)) return null;
            string abr = "";

            if (_titulos.TryGetValue(noTitulo, out abr))
            {
                return abr;
            }

            abr = string.Join(", ", noTitulo
                .Split(',')
                .Select(x => x.Trim())
                .Select(x => Regex.Replace(x, @"\s{2,3}", " ")) // espaços duplos/triplos
                .Select(x => Converter.RemoveAccent(x))
                .Select(x => BuscaAbreviacao(x))
                .ToArray());

            var e = abr.LastIndexOf(',');

            if (e > 0) abr = abr.Substring(0, e - 1) + " e " + abr.Substring(e + 1);

            abr = abr.ToUpper();

            lock (_titulos) { _titulos[noTitulo] = abr; };

            return abr;
        }

        private static void Add(string masc, string fem, string abr)
        {
            _masc[Converter.RemoveAccent(masc)] = abr;
            _fem[Converter.RemoveAccent(fem)] = abr;
        }

        private static string BuscaAbreviacao(string titulo)
        {
            string abr;

            if (_masc.TryGetValue(titulo, out abr))
            {
                return abr;
            }
            if (_fem.TryGetValue(titulo, out abr))
            {
                return abr;
            }

            return titulo;
        }
    }
}