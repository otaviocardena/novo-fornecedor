﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using NBox.Web.Controls;
using System.Web.UI;
using NBox.Web;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using NBox.Core;

namespace Creasp.Core
{
    public static class MasterPageExtensions
    {
        /// <summary>
        /// Informa URL do icone de voltar na master page
        /// </summary>
        public static void BackTo(this NPage page, string url, bool restore = true)
        {
            var lnkBack = page.Master.FindControl("lnkBack") as NHyperLink;
            if (lnkBack == null) return;
            lnkBack.Visible = true;
            lnkBack.NavigateUrl = page.ResolveUrl(url) + (restore ? "#restore" : "");
        }

        /// <summary>
        /// Volta para a pagina que está definida no BackTo()
        /// </summary>
        public static void Back(this NPage page)
        {
            var lnkBack = page.Master.FindControl("lnkBack") as NHyperLink;
            if (lnkBack == null) return;
            page.Redirect(lnkBack.NavigateUrl);
        }

        public static void Audit(this NPage page, IAuditoria entity, object pkey)
        {
            var btnAudit = page.Master.FindControl("btnAudit") as NLinkButton;
            if (btnAudit == null) return;
            var tabAuditoria = page.Master.FindControl("tabAuditoria") as NTab;
            var ltrCadastro = tabAuditoria.FindControl("ltrCadastro") as Literal;
            var ltrAlterado = tabAuditoria.FindControl("ltrAlterado") as Literal;

            tabAuditoria.Text = entity.GetType().Name;
            ltrCadastro.Text = "\"" + entity.LoginCadastro + "\" em " + entity.DtCadastro.ToString("dd/MM/yyyy HH:mm");
            ltrAlterado.Text = entity.DtAlteracao.HasValue == false ? "" :
                "\"" + entity.LoginAlteracao + "\" em " + entity.DtAlteracao.Value.ToString("dd/MM/yyyy HH:mm");

            btnAudit.Visible = true;
            btnAudit.CommandName = entity.GetType().Name;
            btnAudit.CommandArgument = pkey.ToString();
        }

        /// <summary>
        /// Altera o titulo da pagina permitindo coloca um subtitle
        /// </summary>
        public static void SetPageTitle(this NPage page, string title, string subtitle = null)
        {
            page.PageTitle = title + 
                (subtitle != null ? "&nbsp;<small>" + subtitle + "</small>" : "");
        }

        /// <summary>
        /// Retorna true se o registro criado foi feito pelo proprio usuario logado. Valida, caso não seja informada a role admin, se o usuario nao tem essa role
        /// </summary>
        public static bool IsProprietario(this NPage page, IAuditoria entity, string adminRole = null)
        {
            return 
                entity.LoginCadastro.Equals(page.User.Identity.Name, StringComparison.InvariantCultureIgnoreCase) ||
                page.User.IsInRole(adminRole);
        }
    }
}