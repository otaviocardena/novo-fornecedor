# Origem: 3554102 - Taubaté
# -------------------------------------------------------------------
3554102;Taubaté;3500105;Adamantina;712
3554102;Taubaté;3500204;Adolfo;559
3554102;Taubaté;3500303;Aguaí;285
3554102;Taubaté;3500402;Águas da Prata;317
3554102;Taubaté;3500501;Águas de Lindóia;228
3554102;Taubaté;3500550;Águas de Santa Bárbara;428
3554102;Taubaté;3500600;Águas de São Pedro;292
3554102;Taubaté;3500709;Agudos;447
3554102;Taubaté;3500758;Alambari;288
3554102;Taubaté;3500808;Alfredo Marcondes;719
3554102;Taubaté;3500907;Altair;568
3554102;Taubaté;3501004;Altinópolis;426
3554102;Taubaté;3501103;Alto Alegre;624
3554102;Taubaté;3501152;Alumínio;213
3554102;Taubaté;3501202;Álvares Florence;642
3554102;Taubaté;3501301;Álvares Machado;701
3554102;Taubaté;3501400;Álvaro de Carvalho;556
3554102;Taubaté;3501509;Alvinlândia;550
3554102;Taubaté;3501608;Americana;231
3554102;Taubaté;3501707;Américo Brasiliense;388
3554102;Taubaté;3501806;Américo de Campos;631
3554102;Taubaté;3501905;Amparo;199
3554102;Taubaté;3502002;Analândia;318
3554102;Taubaté;3502101;Andradina;761
3554102;Taubaté;3502200;Angatuba;348
3554102;Taubaté;3502309;Anhembi;333
3554102;Taubaté;3502408;Anhumas;684
3554102;Taubaté;3502507;Aparecida;42
3554102;Taubaté;3502606;Aparecida dOeste;733
3554102;Taubaté;3502705;Apiaí;459
3554102;Taubaté;3502754;Araçariguama;187
3554102;Taubaté;3502804;Araçatuba;661
3554102;Taubaté;3502903;Araçoiaba da Serra;256
3554102;Taubaté;3503000;Aramina;542
3554102;Taubaté;3503109;Arandu;413
3554102;Taubaté;3503158;Arapeí;170
3554102;Taubaté;3503208;Araraquara;377
3554102;Taubaté;3503307;Araras;272
3554102;Taubaté;3503356;Arco-Íris;666
3554102;Taubaté;3503406;Arealva;452
3554102;Taubaté;3503505;Areias;119
3554102;Taubaté;3503604;Areiópolis;405
3554102;Taubaté;3503703;Ariranha;481
3554102;Taubaté;3503802;Artur Nogueira;238
3554102;Taubaté;3503901;Arujá;97
3554102;Taubaté;3503950;Aspásia;711
3554102;Taubaté;3504008;Assis;568
3554102;Taubaté;3504107;Atibaia;134
3554102;Taubaté;3504206;Auriflama;672
3554102;Taubaté;3504305;Avaí;501
3554102;Taubaté;3504404;Avanhandava;606
3554102;Taubaté;3504503;Avaré;401
3554102;Taubaté;3504602;Bady Bassitt;554
3554102;Taubaté;3504701;Balbinos;535
3554102;Taubaté;3504800;Bálsamo;568
3554102;Taubaté;3504909;Bananal;187
3554102;Taubaté;3505005;Barão de Antonina;513
3554102;Taubaté;3505104;Barbosa;622
3554102;Taubaté;3505203;Bariri;426
3554102;Taubaté;3505302;Barra Bonita;375
3554102;Taubaté;3505351;Barra do Chapéu;484
3554102;Taubaté;3505401;Barra do Turvo;470
3554102;Taubaté;3505500;Barretos;527
3554102;Taubaté;3505609;Barrinha;441
3554102;Taubaté;3505708;Barueri;164
3554102;Taubaté;3505807;Bastos;670
3554102;Taubaté;3505906;Batatais;456
3554102;Taubaté;3506003;Bauru;463
3554102;Taubaté;3506102;Bebedouro;486
3554102;Taubaté;3506201;Bento de Abreu;697
3554102;Taubaté;3506300;Bernardino de Campos;460
3554102;Taubaté;3506359;Bertioga;163
3554102;Taubaté;3506409;Bilac;657
3554102;Taubaté;3506508;Birigui;641
3554102;Taubaté;3506607;Biritiba Mirim;104
3554102;Taubaté;3506706;Boa Esperança do Sul;405
3554102;Taubaté;3506805;Bocaina;402
3554102;Taubaté;3506904;Bofete;326
3554102;Taubaté;3507001;Boituva;255
3554102;Taubaté;3507100;Bom Jesus dos Perdões;121
3554102;Taubaté;3507159;Bom Sucesso de Itararé;485
3554102;Taubaté;3507209;Borá;621
3554102;Taubaté;3507308;Boracéia;425
3554102;Taubaté;3507407;Borborema;481
3554102;Taubaté;3507456;Borebi;443
3554102;Taubaté;3507506;Botucatu;373
3554102;Taubaté;3507605;Bragança Paulista;153
3554102;Taubaté;3507704;Braúna;633
3554102;Taubaté;3507753;Brejo Alegre;648
3554102;Taubaté;3507803;Brodósqui;442
3554102;Taubaté;3507902;Brotas;350
3554102;Taubaté;3508009;Buri;391
3554102;Taubaté;3508108;Buritama;639
3554102;Taubaté;3508207;Buritizal;540
3554102;Taubaté;3508306;Cabrália Paulista;492
3554102;Taubaté;3508405;Cabreúva;212
3554102;Taubaté;3508504;Caçapava;22
3554102;Taubaté;3508603;Cachoeira Paulista;73
3554102;Taubaté;3508702;Caconde;380
3554102;Taubaté;3508801;Cafelândia;546
3554102;Taubaté;3508900;Caiabu;692
3554102;Taubaté;3509007;Caieiras;172
3554102;Taubaté;3509106;Caiuá;761
3554102;Taubaté;3509205;Cajamar;175
3554102;Taubaté;3509254;Cajati;379
3554102;Taubaté;3509304;Cajobi;526
3554102;Taubaté;3509403;Cajuru;391
3554102;Taubaté;3509452;Campina do Monte Alegre;357
3554102;Taubaté;3509502;Campinas;193
3554102;Taubaté;3509601;Campo Limpo Paulista;169
3554102;Taubaté;3509700;Campos do Jordão;44
3554102;Taubaté;3509809;Campos Novos Paulista;549
3554102;Taubaté;3509908;Cananéia;405
3554102;Taubaté;3509957;Canas;67
3554102;Taubaté;3510005;Cândido Mota;565
3554102;Taubaté;3510104;Cândido Rodrigues;456
3554102;Taubaté;3510153;Canitar;509
3554102;Taubaté;3510203;Capão Bonito;365
3554102;Taubaté;3510302;Capela do Alto;270
3554102;Taubaté;3510401;Capivari;254
3554102;Taubaté;3510500;Caraguatatuba;113
3554102;Taubaté;3510609;Carapicuíba;160
3554102;Taubaté;3510708;Cardoso;663
3554102;Taubaté;3510807;Casa Branca;322
3554102;Taubaté;3510906;Cássia dos Coqueiros;397
3554102;Taubaté;3511003;Castilho;775
3554102;Taubaté;3511102;Catanduva;489
3554102;Taubaté;3511201;Catiguá;503
3554102;Taubaté;3511300;Cedral;528
3554102;Taubaté;3511409;Cerqueira César;426
3554102;Taubaté;3511508;Cerquilho;274
3554102;Taubaté;3511607;Cesário Lange;284
3554102;Taubaté;3511706;Charqueada;293
3554102;Taubaté;3557204;Chavantes;488
3554102;Taubaté;3511904;Clementina;650
3554102;Taubaté;3512001;Colina;510
3554102;Taubaté;3512100;Colômbia;571
3554102;Taubaté;3512209;Conchal;265
3554102;Taubaté;3512308;Conchas;313
3554102;Taubaté;3512407;Cordeirópolis;263
3554102;Taubaté;3512506;Coroados;631
3554102;Taubaté;3512605;Coronel Macedo;479
3554102;Taubaté;3512704;Corumbataí;306
3554102;Taubaté;3512803;Cosmópolis;227
3554102;Taubaté;3512902;Cosmorama;605
3554102;Taubaté;3513009;Cotia;180
3554102;Taubaté;3513108;Cravinhos;396
3554102;Taubaté;3513207;Cristais Paulista;519
3554102;Taubaté;3513306;Cruzália;614
3554102;Taubaté;3513405;Cruzeiro;88
3554102;Taubaté;3513504;Cubatão;186
3554102;Taubaté;3513603;Cunha;93
3554102;Taubaté;3513702;Descalvado;347
3554102;Taubaté;3513801;Diadema;161
3554102;Taubaté;3513850;Dirce Reis;711
3554102;Taubaté;3513900;Divinolândia;357
3554102;Taubaté;3514007;Dobrada;423
3554102;Taubaté;3514106;Dois Córregos;370
3554102;Taubaté;3514205;Dolcinópolis;703
3554102;Taubaté;3514304;Dourado;380
3554102;Taubaté;3514403;Dracena;768
3554102;Taubaté;3514502;Duartina;501
3554102;Taubaté;3514601;Dumont;435
3554102;Taubaté;3514700;Echaporã;599
3554102;Taubaté;3514809;Eldorado;392
3554102;Taubaté;3514908;Elias Fausto;247
3554102;Taubaté;3514924;Elisiário;501
3554102;Taubaté;3514957;Embaúba;521
3554102;Taubaté;3515004;Embu;176
3554102;Taubaté;3515103;Embu-Guaçu;200
3554102;Taubaté;3515129;Emilianópolis;737
3554102;Taubaté;3515152;Engenheiro Coelho;251
3554102;Taubaté;3515186;Espírito Santo do Pinhal;282
3554102;Taubaté;3515194;Espírito Santo do Turvo;459
3554102;Taubaté;3557303;Estiva Gerbi;261
3554102;Taubaté;3515301;Estrela do Norte;729
3554102;Taubaté;3515202;Estrela dOeste;729
3554102;Taubaté;3515350;Euclides da Cunha Paulista;836
3554102;Taubaté;3515400;Fartura;495
3554102;Taubaté;3515608;Fernando Prestes;466
3554102;Taubaté;3515509;Fernandópolis;657
3554102;Taubaté;3515657;Fernão;518
3554102;Taubaté;3515707;Ferraz de Vasconcelos;116
3554102;Taubaté;3515806;Flora Rica;754
3554102;Taubaté;3515905;Floreal;626
3554102;Taubaté;3516002;Flórida Paulista;727
3554102;Taubaté;3516101;Florínea;609
3554102;Taubaté;3516200;Franca;504
3554102;Taubaté;3516309;Francisco Morato;166
3554102;Taubaté;3516408;Franco da Rocha;160
3554102;Taubaté;3516507;Gabriel Monteiro;663
3554102;Taubaté;3516606;Gália;527
3554102;Taubaté;3516705;Garça;538
3554102;Taubaté;3516804;Gastão Vidigal;642
3554102;Taubaté;3516853;Gavião Peixoto;411
3554102;Taubaté;3516903;General Salgado;650
3554102;Taubaté;3517000;Getulina;587
3554102;Taubaté;3517109;Glicério;624
3554102;Taubaté;3517208;Guaiçara;574
3554102;Taubaté;3517307;Guaimbê;583
3554102;Taubaté;3517406;Guaíra;536
3554102;Taubaté;3517505;Guapiaçu;544
3554102;Taubaté;3517604;Guapiara;398
3554102;Taubaté;3517703;Guará;504
3554102;Taubaté;3517802;Guaraçaí;740
3554102;Taubaté;3517901;Guaraci;563
3554102;Taubaté;3518008;Guarani dOeste;684
3554102;Taubaté;3518107;Guarantã;539
3554102;Taubaté;3518206;Guararapes;679
3554102;Taubaté;3518305;Guararema;76
3554102;Taubaté;3518404;Guaratinguetá;48
3554102;Taubaté;3518503;Guareí;318
3554102;Taubaté;3518602;Guariba;441
3554102;Taubaté;3518701;Guarujá;196
3554102;Taubaté;3518800;Guarulhos;125
3554102;Taubaté;3518859;Guatapará;401
3554102;Taubaté;3518909;Guzolândia;681
3554102;Taubaté;3519006;Herculândia;632
3554102;Taubaté;3519055;Holambra;226
3554102;Taubaté;3519071;Hortolândia;218
3554102;Taubaté;3519105;Iacanga;487
3554102;Taubaté;3519204;Iacri;667
3554102;Taubaté;3519253;Iaras;419
3554102;Taubaté;3519303;Ibaté;351
3554102;Taubaté;3519402;Ibirá;521
3554102;Taubaté;3519501;Ibirarema;529
3554102;Taubaté;3519600;Ibitinga;451
3554102;Taubaté;3519709;Ibiúna;218
3554102;Taubaté;3519808;Icém;601
3554102;Taubaté;3519907;Iepê;650
3554102;Taubaté;3520004;Igaraçu do Tietê;379
3554102;Taubaté;3520103;Igarapava;550
3554102;Taubaté;3520202;Igaratá;83
3554102;Taubaté;3520301;Iguape;349
3554102;Taubaté;3520426;Ilha Comprida;356
3554102;Taubaté;3520442;Ilha Solteira;764
3554102;Taubaté;3520400;Ilhabela;148
3554102;Taubaté;3520509;Indaiatuba;217
3554102;Taubaté;3520608;Indiana;681
3554102;Taubaté;3520707;Indiaporã;701
3554102;Taubaté;3520806;Inúbia Paulista;699
3554102;Taubaté;3520905;Ipaussu;478
3554102;Taubaté;3521002;Iperó;260
3554102;Taubaté;3521101;Ipeúna;296
3554102;Taubaté;3521150;Ipiguá;561
3554102;Taubaté;3521200;Iporanga;452
3554102;Taubaté;3521309;Ipuã;515
3554102;Taubaté;3521408;Iracemápolis;265
3554102;Taubaté;3521507;Irapuã;534
3554102;Taubaté;3521606;Irapuru;751
3554102;Taubaté;3521705;Itaberá;458
3554102;Taubaté;3521804;Itaí;421
3554102;Taubaté;3521903;Itajobi;498
3554102;Taubaté;3522000;Itaju;438
3554102;Taubaté;3522109;Itanhaém;236
3554102;Taubaté;3522158;Itaóca;478
3554102;Taubaté;3522208;Itapecerica da Serra;184
3554102;Taubaté;3522307;Itapetininga;307
3554102;Taubaté;3522406;Itapeva;424
3554102;Taubaté;3522505;Itapevi;174
3554102;Taubaté;3522604;Itapira;235
3554102;Taubaté;3522653;Itapirapuã Paulista;513
3554102;Taubaté;3522703;Itápolis;458
3554102;Taubaté;3522802;Itaporanga;500
3554102;Taubaté;3522901;Itapuí;416
3554102;Taubaté;3523008;Itapura;781
3554102;Taubaté;3523107;Itaquaquecetuba;107
3554102;Taubaté;3523206;Itararé;479
3554102;Taubaté;3523305;Itariri;281
3554102;Taubaté;3523404;Itatiba;167
3554102;Taubaté;3523503;Itatinga;361
3554102;Taubaté;3523602;Itirapina;317
3554102;Taubaté;3523701;Itirapuã;487
3554102;Taubaté;3523800;Itobi;329
3554102;Taubaté;3523909;Itu;235
3554102;Taubaté;3524006;Itupeva;195
3554102;Taubaté;3524105;Ituverava;517
3554102;Taubaté;3524204;Jaborandi;522
3554102;Taubaté;3524303;Jaboticabal;447
3554102;Taubaté;3524402;Jacareí;59
3554102;Taubaté;3524501;Jaci;567
3554102;Taubaté;3524600;Jacupiranga;367
3554102;Taubaté;3524709;Jaguariúna;216
3554102;Taubaté;3524808;Jales;689
3554102;Taubaté;3524907;Jambeiro;42
3554102;Taubaté;3525003;Jandira;168
3554102;Taubaté;3525102;Jardinópolis;433
3554102;Taubaté;3525201;Jarinu;156
3554102;Taubaté;3525300;Jaú;396
3554102;Taubaté;3525409;Jeriquara;543
3554102;Taubaté;3525508;Joanópolis;163
3554102;Taubaté;3525607;João Ramalho;628
3554102;Taubaté;3525706;José Bonifácio;585
3554102;Taubaté;3525805;Júlio Mesquita;564
3554102;Taubaté;3525854;Jumirim;286
3554102;Taubaté;3525904;Jundiaí;181
3554102;Taubaté;3526001;Junqueirópolis;758
3554102;Taubaté;3526100;Juquiá;308
3554102;Taubaté;3526209;Juquitiba;221
3554102;Taubaté;3526308;Lagoinha;63
3554102;Taubaté;3526407;Laranjal Paulista;293
3554102;Taubaté;3526506;Lavínia;721
3554102;Taubaté;3526605;Lavrinhas;95
3554102;Taubaté;3526704;Leme;292
3554102;Taubaté;3526803;Lençóis Paulista;421
3554102;Taubaté;3526902;Limeira;255
3554102;Taubaté;3527009;Lindóia;222
3554102;Taubaté;3527108;Lins;565
3554102;Taubaté;3527207;Lorena;60
3554102;Taubaté;3527256;Lourdes;658
3554102;Taubaté;3527306;Louveira;185
3554102;Taubaté;3527405;Lucélia;707
3554102;Taubaté;3527504;Lucianópolis;515
3554102;Taubaté;3527603;Luís Antônio;378
3554102;Taubaté;3527702;Luiziânia;643
3554102;Taubaté;3527801;Lupércio;562
3554102;Taubaté;3527900;Lutécia;604
3554102;Taubaté;3528007;Macatuba;399
3554102;Taubaté;3528106;Macaubal;615
3554102;Taubaté;3528205;Macedônia;672
3554102;Taubaté;3528403;Mairinque;205
3554102;Taubaté;3528502;Mairiporã;145
3554102;Taubaté;3528601;Manduri;442
3554102;Taubaté;3528700;Marabá Paulista;772
3554102;Taubaté;3528809;Maracaí;596
3554102;Taubaté;3528858;Marapoama;511
3554102;Taubaté;3528908;Mariápolis;721
3554102;Taubaté;3529005;Marília;569
3554102;Taubaté;3529104;Marinópolis;727
3554102;Taubaté;3529203;Martinópolis;673
3554102;Taubaté;3529302;Matão;409
3554102;Taubaté;3529401;Mauá;155
3554102;Taubaté;3529500;Mendonça;552
3554102;Taubaté;3529609;Meridiano;649
3554102;Taubaté;3529658;Mesópolis;724
3554102;Taubaté;3529708;Miguelópolis;545
3554102;Taubaté;3529807;Mineiros do Tietê;377
3554102;Taubaté;3530003;Mira Estrela;692
3554102;Taubaté;3529906;Miracatu;287
3554102;Taubaté;3530102;Mirandópolis;728
3554102;Taubaté;3530201;Mirante do Paranapanema;744
3554102;Taubaté;3530300;Mirassol;556
3554102;Taubaté;3530409;Mirassolândia;570
3554102;Taubaté;3530508;Mococa;355
3554102;Taubaté;3530607;Mogi das Cruzes;106
3554102;Taubaté;3530706;Mogi-Guaçu;257
3554102;Taubaté;3530805;Moji-Mirim;243
3554102;Taubaté;3530904;Mombuca;268
3554102;Taubaté;3531001;Monções;632
3554102;Taubaté;3531100;Mongaguá;219
3554102;Taubaté;3531209;Monte Alegre do Sul;198
3554102;Taubaté;3531308;Monte Alto;460
3554102;Taubaté;3531407;Monte Aprazível;578
3554102;Taubaté;3531506;Monte Azul Paulista;504
3554102;Taubaté;3531605;Monte Castelo;811
3554102;Taubaté;3531803;Monte Mor;232
3554102;Taubaté;3531704;Monteiro Lobato;71
3554102;Taubaté;3531902;Morro Agudo;484
3554102;Taubaté;3532009;Morungaba;180
3554102;Taubaté;3532058;Motuca;411
3554102;Taubaté;3532108;Murutinga do Sul;749
3554102;Taubaté;3532157;Nantes;659
3554102;Taubaté;3532207;Narandiba;714
3554102;Taubaté;3532306;Natividade da Serra;57
3554102;Taubaté;3532405;Nazaré Paulista;112
3554102;Taubaté;3532504;Neves Paulista;574
3554102;Taubaté;3532603;Nhandeara;613
3554102;Taubaté;3532702;Nipoã;598
3554102;Taubaté;3532801;Nova Aliança;551
3554102;Taubaté;3532827;Nova Campina;445
3554102;Taubaté;3532843;Nova Canaã Paulista;750
3554102;Taubaté;3532868;Nova Castilho;669
3554102;Taubaté;3532900;Nova Europa;421
3554102;Taubaté;3533007;Nova Granada;575
3554102;Taubaté;3533106;Nova Guataporanga;792
3554102;Taubaté;3533205;Nova Independência;792
3554102;Taubaté;3533304;Nova Luzitânia;651
3554102;Taubaté;3533403;Nova Odessa;226
3554102;Taubaté;3533254;Novais;512
3554102;Taubaté;3533502;Novo Horizonte;503
3554102;Taubaté;3533601;Nuporanga;477
3554102;Taubaté;3533700;Ocauçu;561
3554102;Taubaté;3533809;Óleo;451
3554102;Taubaté;3533908;Olímpia;542
3554102;Taubaté;3534005;Onda Verde;568
3554102;Taubaté;3534104;Oriente;591
3554102;Taubaté;3534203;Orindiúva;628
3554102;Taubaté;3534302;Orlândia;469
3554102;Taubaté;3534401;Osasco;156
3554102;Taubaté;3534500;Oscar Bressane;617
3554102;Taubaté;3534609;Osvaldo Cruz;692
3554102;Taubaté;3534708;Ourinhos;512
3554102;Taubaté;3534807;Ouro Verde;798
3554102;Taubaté;3534757;Ouroeste;692
3554102;Taubaté;3534906;Pacaembu;737
3554102;Taubaté;3535002;Palestina;597
3554102;Taubaté;3535101;Palmares Paulista;498
3554102;Taubaté;3535200;Palmeira d Oeste;717
3554102;Taubaté;3535309;Palmital;548
3554102;Taubaté;3535408;Panorama;807
3554102;Taubaté;3535507;Paraguaçu Paulista;600
3554102;Taubaté;3535606;Paraibuna;64
3554102;Taubaté;3535705;Paraíso;500
3554102;Taubaté;3535804;Paranapanema;395
3554102;Taubaté;3535903;Paranapuã;710
3554102;Taubaté;3536000;Parapuã;693
3554102;Taubaté;3536109;Pardinho;342
3554102;Taubaté;3536208;Pariquera-Açu;363
3554102;Taubaté;3536257;Parisi;637
3554102;Taubaté;3536307;Patrocínio Paulista;488
3554102;Taubaté;3536406;Paulicéia;804
3554102;Taubaté;3536505;Paulínia;212
3554102;Taubaté;3536570;Paulistânia;475
3554102;Taubaté;3536604;Paulo de Faria;641
3554102;Taubaté;3536703;Pederneiras;419
3554102;Taubaté;3536802;Pedra Bela;185
3554102;Taubaté;3536901;Pedranópolis;651
3554102;Taubaté;3537008;Pedregulho;541
3554102;Taubaté;3537107;Pedreira;213
3554102;Taubaté;3537156;Pedrinhas Paulista;616
3554102;Taubaté;3537206;Pedro de Toledo;294
3554102;Taubaté;3537305;Penápolis;613
3554102;Taubaté;3537404;Pereira Barreto;728
3554102;Taubaté;3537503;Pereiras;302
3554102;Taubaté;3537602;Peruíbe;265
3554102;Taubaté;3537701;Piacatu;670
3554102;Taubaté;3537800;Piedade;248
3554102;Taubaté;3537909;Pilar do Sul;284
3554102;Taubaté;3538006;Pindamonhangaba;16
3554102;Taubaté;3538105;Pindorama;482
3554102;Taubaté;3538204;Pinhalzinho;179
3554102;Taubaté;3538303;Piquerobi;738
3554102;Taubaté;3538501;Piquete;79
3554102;Taubaté;3538600;Piracaia;138
3554102;Taubaté;3538709;Piracicaba;268
3554102;Taubaté;3538808;Piraju;468
3554102;Taubaté;3538907;Pirajuí;518
3554102;Taubaté;3539004;Pirangi;484
3554102;Taubaté;3539103;Pirapora do Bom Jesus;189
3554102;Taubaté;3539202;Pirapozinho;698
3554102;Taubaté;3539301;Pirassununga;315
3554102;Taubaté;3539400;Piratininga;470
3554102;Taubaté;3539509;Pitangueiras;468
3554102;Taubaté;3539608;Planalto;614
3554102;Taubaté;3539707;Platina;559
3554102;Taubaté;3539806;Poá;113
3554102;Taubaté;3539905;Poloni;592
3554102;Taubaté;3540002;Pompéia;602
3554102;Taubaté;3540101;Pongaí;518
3554102;Taubaté;3540200;Pontal;455
3554102;Taubaté;3540259;Pontalinda;683
3554102;Taubaté;3540309;Pontes Gestal;637
3554102;Taubaté;3540408;Populina;714
3554102;Taubaté;3540507;Porangaba;307
3554102;Taubaté;3540606;Porto Feliz;252
3554102;Taubaté;3540705;Porto Ferreira;332
3554102;Taubaté;3540754;Potim;57
3554102;Taubaté;3540804;Potirendaba;536
3554102;Taubaté;3540853;Pracinha;709
3554102;Taubaté;3540903;Pradópolis;419
3554102;Taubaté;3541000;Praia Grande;200
3554102;Taubaté;3541059;Pratânia;398
3554102;Taubaté;3541109;Presidente Alves;517
3554102;Taubaté;3541208;Presidente Bernardes;714
3554102;Taubaté;3541307;Presidente Epitácio;779
3554102;Taubaté;3541406;Presidente Prudente;692
3554102;Taubaté;3541505;Presidente Venceslau;745
3554102;Taubaté;3541604;Promissão;585
3554102;Taubaté;3541653;Quadra;297
3554102;Taubaté;3541703;Quatá;623
3554102;Taubaté;3541802;Queiroz;628
3554102;Taubaté;3541901;Queluz;106
3554102;Taubaté;3542008;Quintana;618
3554102;Taubaté;3542107;Rafard;263
3554102;Taubaté;3542206;Rancharia;642
3554102;Taubaté;3542305;Redenção da Serra;36
3554102;Taubaté;3542404;Regente Feijó;680
3554102;Taubaté;3542503;Reginópolis;507
3554102;Taubaté;3542602;Registro;337
3554102;Taubaté;3542701;Restinga;493
3554102;Taubaté;3542800;Ribeira;491
3554102;Taubaté;3542909;Ribeirão Bonito;367
3554102;Taubaté;3543006;Ribeirão Branco;429
3554102;Taubaté;3543105;Ribeirão Corrente;526
3554102;Taubaté;3543204;Ribeirão do Sul;527
3554102;Taubaté;3543238;Ribeirão dos Índios;739
3554102;Taubaté;3543253;Ribeirão Grande;375
3554102;Taubaté;3543303;Ribeirão Pires;140
3554102;Taubaté;3543402;Ribeirão Preto;417
3554102;Taubaté;3543600;Rifaina;568
3554102;Taubaté;3543709;Rincão;395
3554102;Taubaté;3543808;Rinópolis;682
3554102;Taubaté;3543907;Rio Claro;277
3554102;Taubaté;3544004;Rio das Pedras;274
3554102;Taubaté;3544103;Rio Grande da Serra;146
3554102;Taubaté;3544202;Riolândia;659
3554102;Taubaté;3543501;Riversul;494
3554102;Taubaté;3544251;Rosana;882
3554102;Taubaté;3544301;Roseira;31
3554102;Taubaté;3544400;Rubiácea;692
3554102;Taubaté;3544509;Rubinéia;736
3554102;Taubaté;3544608;Sabino;598
3554102;Taubaté;3544707;Sagres;695
3554102;Taubaté;3544806;Sales;540
3554102;Taubaté;3544905;Sales Oliveira;467
3554102;Taubaté;3545001;Salesópolis;81
3554102;Taubaté;3545100;Salmourão;718
3554102;Taubaté;3545159;Saltinho;281
3554102;Taubaté;3545209;Salto;223
3554102;Taubaté;3545308;Salto de Pirapora;259
3554102;Taubaté;3545407;Salto Grande;517
3554102;Taubaté;3545506;Sandovalina;737
3554102;Taubaté;3545605;Santa Adélia;475
3554102;Taubaté;3545704;Santa Albertina;719
3554102;Taubaté;3545803;Santa Bárbara d Oeste;241
3554102;Taubaté;3546009;Santa Branca;65
3554102;Taubaté;3546108;Santa Clara d Oeste;742
3554102;Taubaté;3546207;Santa Cruz da Conceição;302
3554102;Taubaté;3546256;Santa Cruz da Esperança;407
3554102;Taubaté;3546306;Santa Cruz das Palmeiras;338
3554102;Taubaté;3546405;Santa Cruz do Rio Pardo;480
3554102;Taubaté;3546504;Santa Ernestina;428
3554102;Taubaté;3546603;Santa Fé do Sul;729
3554102;Taubaté;3546702;Santa Gertrudes;271
3554102;Taubaté;3546801;Santa Isabel;87
3554102;Taubaté;3546900;Santa Lúcia;394
3554102;Taubaté;3547007;Santa Maria da Serra;325
3554102;Taubaté;3547106;Santa Mercedes;795
3554102;Taubaté;3547403;Santa Rita d Oeste;352
3554102;Taubaté;3547502;Santa Rita do Passa Quatro;352
3554102;Taubaté;3547601;Santa Rosa de Viterbo;378
3554102;Taubaté;3547650;Santa Salete;705
3554102;Taubaté;3547205;Santana da Ponte Pensa;718
3554102;Taubaté;3547304;Santana de Parnaíba;174
3554102;Taubaté;3547700;Santo Anastácio;723
3554102;Taubaté;3547809;Santo André;165
3554102;Taubaté;3547908;Santo Antônio da Alegria;424
3554102;Taubaté;3548005;Santo Antônio de Posse;230
3554102;Taubaté;3548054;Santo Antônio do Aracangua;691
3554102;Taubaté;3548104;Santo Antônio do Jardim;294
3554102;Taubaté;3548203;Santo Antônio do Pinhal;34
3554102;Taubaté;3548302;Santo Expedito;733
3554102;Taubaté;3548401;Santópolis do Aguapeí;657
3554102;Taubaté;3548500;Santos;201
3554102;Taubaté;3548609;São Bento do Sapucaí;57
3554102;Taubaté;3548708;São Bernardo do Campo;170
3554102;Taubaté;3548807;São Caetano do Sul;154
3554102;Taubaté;3548906;São Carlos;336
3554102;Taubaté;3549003;São Francisco;710
3554102;Taubaté;3549102;São João da Boa Vista;309
3554102;Taubaté;3549201;São João das Duas Pontes;676
3554102;Taubaté;3549250;São João de Iracema;667
3554102;Taubaté;3549300;São João do Pau d Alho;799
3554102;Taubaté;3549409;São Joaquim da Barra;486
3554102;Taubaté;3549508;São José da Bela Vista;497
3554102;Taubaté;3549607;São José do Barreiro;141
3554102;Taubaté;3549706;São José do Rio Pardo;347
3554102;Taubaté;3549805;São José do Rio Preto;542
3554102;Taubaté;3549904;São José dos Campos;44
3554102;Taubaté;3549953;São Lourenço da Serra;202
3554102;Taubaté;3550001;São Luís do Paraitinga;44
3554102;Taubaté;3550100;São Manuel;393
3554102;Taubaté;3550209;São Miguel Arcanjo;318
3554102;Taubaté;3550308;São Paulo;140
3554102;Taubaté;3550407;São Pedro;301
3554102;Taubaté;3550506;São Pedro do Turvo;497
3554102;Taubaté;3550605;São Roque;200
3554102;Taubaté;3550704;São Sebastião;131
3554102;Taubaté;3550803;São Sebastião da Grama;343
3554102;Taubaté;3550902;São Simão;382
3554102;Taubaté;3551009;São Vicente;194
3554102;Taubaté;3551108;Sarapuí;285
3554102;Taubaté;3551207;Sarutaiá;481
3554102;Taubaté;3551306;Sebastianópolis do Sul;609
3554102;Taubaté;3551405;Serra Azul;406
3554102;Taubaté;3551603;Serra Negra;207
3554102;Taubaté;3551504;Serrana;418
3554102;Taubaté;3551702;Sertãozinho;437
3554102;Taubaté;3551801;Sete Barras;352
3554102;Taubaté;3551900;Severínia;523
3554102;Taubaté;3552007;Silveiras;92
3554102;Taubaté;3552106;Socorro;202
3554102;Taubaté;3552205;Sorocaba;233
3554102;Taubaté;3552304;Sud Mennucci;718
3554102;Taubaté;3552403;Sumaré;223
3554102;Taubaté;3552551;Suzanápolis;731
3554102;Taubaté;3552502;Suzano;115
3554102;Taubaté;3552601;Tabapuã;513
3554102;Taubaté;3552700;Tabatinga;435
3554102;Taubaté;3552809;Taboão da Serra;179
3554102;Taubaté;3552908;Taciba;678
3554102;Taubaté;3553005;Taguaí;481
3554102;Taubaté;3553104;Taiaçu;474
3554102;Taubaté;3553203;Taiúva;467
3554102;Taubaté;3553302;Tambaú;350
3554102;Taubaté;3553401;Tanabi;581
3554102;Taubaté;3553500;Tapiraí;284
3554102;Taubaté;3553609;Tapiratiba;369
3554102;Taubaté;3553658;Taquaral;467
3554102;Taubaté;3553708;Taquaritinga;438
3554102;Taubaté;3553807;Taquarituba;462
3554102;Taubaté;3553856;Taquarivaí;402
3554102;Taubaté;3553906;Tarabai;707
3554102;Taubaté;3553955;Tarumã;590
3554102;Taubaté;3554003;Tatuí;275
3554102;Taubaté;3554201;Tejupá;484
3554102;Taubaté;3554300;Teodoro Sampaio;787
3554102;Taubaté;3554409;Terra Roxa;510
3554102;Taubaté;3554508;Tietê;277
3554102;Taubaté;3554607;Timburi;490
3554102;Taubaté;3554656;Torre de Pedra;309
3554102;Taubaté;3554706;Torrinha;345
3554102;Taubaté;3554755;Trabiju;412
3554102;Taubaté;3554805;Tremembé;7
3554102;Taubaté;3554904;Três Fronteiras;726
3554102;Taubaté;3554953;Tuiuti;179
3554102;Taubaté;3555000;Tupã;648
3554102;Taubaté;3555109;Tupi Paulista;780
3554102;Taubaté;3555208;Turiúba;648
3554102;Taubaté;3555307;Turmalina;702
3554102;Taubaté;3555356;Ubarana;573
3554102;Taubaté;3555406;Ubatuba;94
3554102;Taubaté;3555505;Ubirajara;533
3554102;Taubaté;3555604;Uchoa;520
3554102;Taubaté;3555703;União Paulista;607
3554102;Taubaté;3555802;Urânia;698
3554102;Taubaté;3555901;Uru;525
3554102;Taubaté;3556008;Urupês;523
3554102;Taubaté;3556107;Valentim Gentil;639
3554102;Taubaté;3556206;Valinhos;190
3554102;Taubaté;3556305;Valparaíso;698
3554102;Taubaté;3556354;Vargem;164
3554102;Taubaté;3556404;Vargem Grande do Sul;327
3554102;Taubaté;3556453;Vargem Grande Paulista;193
3554102;Taubaté;3556503;Várzea Paulista;174
3554102;Taubaté;3556602;Vera Cruz;555
3554102;Taubaté;3556701;Vinhedo;184
3554102;Taubaté;3556800;Viradouro;502
3554102;Taubaté;3556909;Vista Alegre do Alto;474
3554102;Taubaté;3556958;Vitória Brasil;692
3554102;Taubaté;3557006;Votorantim;239
3554102;Taubaté;3557105;Votuporanga;626
3554102;Taubaté;3557154;Zacarias;627
