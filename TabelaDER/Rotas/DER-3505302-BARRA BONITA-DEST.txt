# Destino: 3505302 - Barra Bonita
# -------------------------------------------------------------------
3500105;Adamantina;3505302;Barra Bonita;323
3500204;Adolfo;3505302;Barra Bonita;202
3500303;Aguaí;3505302;Barra Bonita;195
3500402;Águas da Prata;3505302;Barra Bonita;226
3500501;Águas de Lindóia;3505302;Barra Bonita;241
3500550;Águas de Santa Bárbara;3505302;Barra Bonita;112
3500600;Águas de São Pedro;3505302;Barra Bonita;83
3500709;Agudos;3505302;Barra Bonita;59
3500758;Alambari;3505302;Barra Bonita;193
3500808;Alfredo Marcondes;3505302;Barra Bonita;385
3500907;Altair;3505302;Barra Bonita;267
3501004;Altinópolis;3505302;Barra Bonita;229
3501103;Alto Alegre;3505302;Barra Bonita;235
3501152;Alumínio;3505302;Barra Bonita;238
3501202;Álvares Florence;3505302;Barra Bonita;332
3501301;Álvares Machado;3505302;Barra Bonita;367
3501400;Álvaro de Carvalho;3505302;Barra Bonita;167
3501509;Alvinlândia;3505302;Barra Bonita;171
3501608;Americana;3505302;Barra Bonita;149
3501707;Américo Brasiliense;3505302;Barra Bonita;109
3501806;Américo de Campos;3505302;Barra Bonita;321
3501905;Amparo;3505302;Barra Bonita;233
3502002;Analândia;3505302;Barra Bonita;119
3502101;Andradina;3505302;Barra Bonita;372
3502200;Angatuba;3505302;Barra Bonita;188
3502309;Anhembi;3505302;Barra Bonita;104
3502408;Anhumas;3505302;Barra Bonita;367
3502507;Aparecida;3505302;Barra Bonita;415
3502606;Aparecida dOeste;3505302;Barra Bonita;401
3502705;Apiaí;3505302;Barra Bonita;300
3502754;Araçariguama;3505302;Barra Bonita;237
3502804;Araçatuba;3505302;Barra Bonita;272
3502903;Araçoiaba da Serra;3505302;Barra Bonita;193
3503000;Aramina;3505302;Barra Bonita;308
3503109;Arandu;3505302;Barra Bonita;97
3503158;Arapeí;3505302;Barra Bonita;543
3503208;Araraquara;3505302;Barra Bonita;96
3503307;Araras;3505302;Barra Bonita;157
3503356;Arco-Íris;3505302;Barra Bonita;277
3503406;Arealva;3505302;Barra Bonita;84
3503505;Areias;3505302;Barra Bonita;493
3503604;Areiópolis;3505302;Barra Bonita;41
3503703;Ariranha;3505302;Barra Bonita;171
3503802;Artur Nogueira;3505302;Barra Bonita;171
3503901;Arujá;3505302;Barra Bonita;306
3503950;Aspásia;3505302;Barra Bonita;401
3504008;Assis;3505302;Barra Bonita;252
3504107;Atibaia;3505302;Barra Bonita;247
3504206;Auriflama;3505302;Barra Bonita;331
3504305;Avaí;3505302;Barra Bonita;112
3504404;Avanhandava;3505302;Barra Bonita;217
3504503;Avaré;3505302;Barra Bonita;84
3504602;Bady Bassitt;3505302;Barra Bonita;230
3504701;Balbinos;3505302;Barra Bonita;145
3504800;Bálsamo;3505302;Barra Bonita;257
3504909;Bananal;3505302;Barra Bonita;561
3505005;Barão de Antonina;3505302;Barra Bonita;197
3505104;Barbosa;3505302;Barra Bonita;236
3505203;Bariri;3505302;Barra Bonita;59
3505351;Barra do Chapéu;3505302;Barra Bonita;325
3505401;Barra do Turvo;3505302;Barra Bonita;396
3505500;Barretos;3505302;Barra Bonita;243
3505609;Barrinha;3505302;Barra Bonita;178
3505708;Barueri;3505302;Barra Bonita;261
3505807;Bastos;3505302;Barra Bonita;281
3505906;Batatais;3505302;Barra Bonita;222
3506003;Bauru;3505302;Barra Bonita;73
3506102;Bebedouro;3505302;Barra Bonita;201
3506201;Bento de Abreu;3505302;Barra Bonita;307
3506300;Bernardino de Campos;3505302;Barra Bonita;143
3506359;Bertioga;3505302;Barra Bonita;375
3506409;Bilac;3505302;Barra Bonita;268
3506508;Birigui;3505302;Barra Bonita;251
3506607;Biritiba Mirim;3505302;Barra Bonita;340
3506706;Boa Esperança do Sul;3505302;Barra Bonita;63
3506805;Bocaina;3505302;Barra Bonita;44
3506904;Bofete;3505302;Barra Bonita;97
3507001;Boituva;3505302;Barra Bonita;172
3507100;Bom Jesus dos Perdões;3505302;Barra Bonita;255
3507159;Bom Sucesso de Itararé;3505302;Barra Bonita;254
3507209;Borá;3505302;Barra Bonita;265
3507308;Boracéia;3505302;Barra Bonita;52
3507407;Borborema;3505302;Barra Bonita;124
3507456;Borebi;3505302;Barra Bonita;55
3507506;Botucatu;3505302;Barra Bonita;54
3507605;Bragança Paulista;3505302;Barra Bonita;246
3507704;Braúna;3505302;Barra Bonita;244
3507753;Brejo Alegre;3505302;Barra Bonita;259
3507803;Brodósqui;3505302;Barra Bonita;208
3507902;Brotas;3505302;Barra Bonita;68
3508009;Buri;3505302;Barra Bonita;231
3508108;Buritama;3505302;Barra Bonita;276
3508207;Buritizal;3505302;Barra Bonita;306
3508306;Cabrália Paulista;3505302;Barra Bonita;113
3508405;Cabreúva;3505302;Barra Bonita;219
3508504;Caçapava;3505302;Barra Bonita;352
3508603;Cachoeira Paulista;3505302;Barra Bonita;447
3508702;Caconde;3505302;Barra Bonita;267
3508801;Cafelândia;3505302;Barra Bonita;157
3508900;Caiabu;3505302;Barra Bonita;346
3509007;Caieiras;3505302;Barra Bonita;248
3509106;Caiuá;3505302;Barra Bonita;426
3509205;Cajamar;3505302;Barra Bonita;239
3509254;Cajati;3505302;Barra Bonita;360
3509304;Cajobi;3505302;Barra Bonita;225
3509403;Cajuru;3505302;Barra Bonita;231
3509452;Campina do Monte Alegre;3505302;Barra Bonita;197
3509502;Campinas;3505302;Barra Bonita;183
3509601;Campo Limpo Paulista;3505302;Barra Bonita;229
3509700;Campos do Jordão;3505302;Barra Bonita;413
3509809;Campos Novos Paulista;3505302;Barra Bonita;210
3509908;Cananéia;3505302;Barra Bonita;386
3509957;Canas;3505302;Barra Bonita;441
3510005;Cândido Mota;3505302;Barra Bonita;248
3510104;Cândido Rodrigues;3505302;Barra Bonita;171
3510153;Canitar;3505302;Barra Bonita;192
3510203;Capão Bonito;3505302;Barra Bonita;240
3510302;Capela do Alto;3505302;Barra Bonita;178
3510401;Capivari;3505302;Barra Bonita;150
3510500;Caraguatatuba;3505302;Barra Bonita;414
3510609;Carapicuíba;3505302;Barra Bonita;268
3510708;Cardoso;3505302;Barra Bonita;353
3510807;Casa Branca;3505302;Barra Bonita;202
3510906;Cássia dos Coqueiros;3505302;Barra Bonita;248
3511003;Castilho;3505302;Barra Bonita;386
3511102;Catanduva;3505302;Barra Bonita;179
3511201;Catiguá;3505302;Barra Bonita;193
3511300;Cedral;3505302;Barra Bonita;218
3511409;Cerqueira César;3505302;Barra Bonita;109
3511508;Cerquilho;3505302;Barra Bonita;150
3511607;Cesário Lange;3505302;Barra Bonita;146
3511706;Charqueada;3505302;Barra Bonita;96
3557204;Chavantes;3505302;Barra Bonita;171
3511904;Clementina;3505302;Barra Bonita;261
3512001;Colina;3505302;Barra Bonita;225
3512100;Colômbia;3505302;Barra Bonita;286
3512209;Conchal;3505302;Barra Bonita;184
3512308;Conchas;3505302;Barra Bonita;110
3512407;Cordeirópolis;3505302;Barra Bonita;150
3512506;Coroados;3505302;Barra Bonita;241
3512605;Coronel Macedo;3505302;Barra Bonita;162
3512704;Corumbataí;3505302;Barra Bonita;119
3512803;Cosmópolis;3505302;Barra Bonita;175
3512902;Cosmorama;3505302;Barra Bonita;295
3513009;Cotia;3505302;Barra Bonita;267
3513108;Cravinhos;3505302;Barra Bonita;173
3513207;Cristais Paulista;3505302;Barra Bonita;284
3513306;Cruzália;3505302;Barra Bonita;298
3513405;Cruzeiro;3505302;Barra Bonita;462
3513504;Cubatão;3505302;Barra Bonita;324
3513603;Cunha;3505302;Barra Bonita;467
3513702;Descalvado;3505302;Barra Bonita;150
3513801;Diadema;3505302;Barra Bonita;288
3513850;Dirce Reis;3505302;Barra Bonita;393
3513900;Divinolândia;3505302;Barra Bonita;249
3514007;Dobrada;3505302;Barra Bonita;137
3514106;Dois Córregos;3505302;Barra Bonita;26
3514205;Dolcinópolis;3505302;Barra Bonita;394
3514304;Dourado;3505302;Barra Bonita;68
3514403;Dracena;3505302;Barra Bonita;378
3514502;Duartina;3505302;Barra Bonita;123
3514601;Dumont;3505302;Barra Bonita;179
3514700;Echaporã;3505302;Barra Bonita;219
3514809;Eldorado;3505302;Barra Bonita;329
3514908;Elias Fausto;3505302;Barra Bonita;167
3514924;Elisiário;3505302;Barra Bonita;191
3514957;Embaúba;3505302;Barra Bonita;211
3515004;Embu;3505302;Barra Bonita;284
3515103;Embu-Guaçu;3505302;Barra Bonita;308
3515129;Emilianópolis;3505302;Barra Bonita;402
3515152;Engenheiro Coelho;3505302;Barra Bonita;167
3515186;Espírito Santo do Pinhal;3505302;Barra Bonita;226
3515194;Espírito Santo do Turvo;3505302;Barra Bonita;131
3557303;Estiva Gerbi;3505302;Barra Bonita;215
3515301;Estrela do Norte;3505302;Barra Bonita;413
3515202;Estrela dOeste;3505302;Barra Bonita;413
3515350;Euclides da Cunha Paulista;3505302;Barra Bonita;520
3515400;Fartura;3505302;Barra Bonita;178
3515608;Fernando Prestes;3505302;Barra Bonita;159
3515509;Fernandópolis;3505302;Barra Bonita;347
3515657;Fernão;3505302;Barra Bonita;129
3515707;Ferraz de Vasconcelos;3505302;Barra Bonita;306
3515806;Flora Rica;3505302;Barra Bonita;375
3515905;Floreal;3505302;Barra Bonita;315
3516002;Flórida Paulista;3505302;Barra Bonita;337
3516101;Florínea;3505302;Barra Bonita;292
3516200;Franca;3505302;Barra Bonita;269
3516309;Francisco Morato;3505302;Barra Bonita;243
3516408;Franco da Rocha;3505302;Barra Bonita;251
3516507;Gabriel Monteiro;3505302;Barra Bonita;274
3516606;Gália;3505302;Barra Bonita;138
3516705;Garça;3505302;Barra Bonita;149
3516804;Gastão Vidigal;3505302;Barra Bonita;309
3516853;Gavião Peixoto;3505302;Barra Bonita;93
3516903;General Salgado;3505302;Barra Bonita;343
3517000;Getulina;3505302;Barra Bonita;198
3517109;Glicério;3505302;Barra Bonita;235
3517208;Guaiçara;3505302;Barra Bonita;184
3517307;Guaimbê;3505302;Barra Bonita;193
3517406;Guaíra;3505302;Barra Bonita;285
3517505;Guapiaçu;3505302;Barra Bonita;234
3517604;Guapiara;3505302;Barra Bonita;274
3517703;Guará;3505302;Barra Bonita;270
3517802;Guaraçaí;3505302;Barra Bonita;351
3517901;Guaraci;3505302;Barra Bonita;262
3518008;Guarani dOeste;3505302;Barra Bonita;373
3518107;Guarantã;3505302;Barra Bonita;149
3518206;Guararapes;3505302;Barra Bonita;289
3518305;Guararema;3505302;Barra Bonita;328
3518404;Guaratinguetá;3505302;Barra Bonita;422
3518503;Guareí;3505302;Barra Bonita;142
3518602;Guariba;3505302;Barra Bonita;156
3518701;Guarujá;3505302;Barra Bonita;354
3518800;Guarulhos;3505302;Barra Bonita;277
3518859;Guatapará;3505302;Barra Bonita;145
3518909;Guzolândia;3505302;Barra Bonita;348
3519006;Herculândia;3505302;Barra Bonita;242
3519055;Holambra;3505302;Barra Bonita;186
3519071;Hortolândia;3505302;Barra Bonita;164
3519105;Iacanga;3505302;Barra Bonita;105
3519204;Iacri;3505302;Barra Bonita;278
3519253;Iaras;3505302;Barra Bonita;102
3519303;Ibaté;3505302;Barra Bonita;118
3519402;Ibirá;3505302;Barra Bonita;197
3519501;Ibirarema;3505302;Barra Bonita;212
3519600;Ibitinga;3505302;Barra Bonita;94
3519709;Ibiúna;3505302;Barra Bonita;265
3519808;Icém;3505302;Barra Bonita;291
3519907;Iepê;3505302;Barra Bonita;333
3520004;Igaraçu do Tietê;3505302;Barra Bonita;4
3520103;Igarapava;3505302;Barra Bonita;316
3520202;Igaratá;3505302;Barra Bonita;293
3520301;Iguape;3505302;Barra Bonita;393
3520426;Ilha Comprida;3505302;Barra Bonita;399
3520442;Ilha Solteira;3505302;Barra Bonita;430
3520400;Ilhabela;3505302;Barra Bonita;449
3520509;Indaiatuba;3505302;Barra Bonita;187
3520608;Indiana;3505302;Barra Bonita;340
3520707;Indiaporã;3505302;Barra Bonita;391
3520806;Inúbia Paulista;3505302;Barra Bonita;310
3520905;Ipaussu;3505302;Barra Bonita;161
3521002;Iperó;3505302;Barra Bonita;177
3521101;Ipeúna;3505302;Barra Bonita;108
3521150;Ipiguá;3505302;Barra Bonita;251
3521200;Iporanga;3505302;Barra Bonita;367
3521309;Ipuã;3505302;Barra Bonita;281
3521408;Iracemápolis;3505302;Barra Bonita;135
3521507;Irapuã;3505302;Barra Bonita;177
3521606;Irapuru;3505302;Barra Bonita;361
3521705;Itaberá;3505302;Barra Bonita;192
3521804;Itaí;3505302;Barra Bonita;124
3521903;Itajobi;3505302;Barra Bonita;173
3522000;Itaju;3505302;Barra Bonita;70
3522109;Itanhaém;3505302;Barra Bonita;374
3522158;Itaóca;3505302;Barra Bonita;354
3522208;Itapecerica da Serra;3505302;Barra Bonita;292
3522307;Itapetininga;3505302;Barra Bonita;178
3522406;Itapeva;3505302;Barra Bonita;227
3522505;Itapevi;3505302;Barra Bonita;259
3522604;Itapira;3505302;Barra Bonita;212
3522653;Itapirapuã Paulista;3505302;Barra Bonita;355
3522703;Itápolis;3505302;Barra Bonita;114
3522802;Itaporanga;3505302;Barra Bonita;183
3522901;Itapuí;3505302;Barra Bonita;43
3523008;Itapura;3505302;Barra Bonita;422
3523107;Itaquaquecetuba;3505302;Barra Bonita;297
3523206;Itararé;3505302;Barra Bonita;238
3523305;Itariri;3505302;Barra Bonita;378
3523404;Itatiba;3505302;Barra Bonita;214
3523503;Itatinga;3505302;Barra Bonita;85
3523602;Itirapina;3505302;Barra Bonita;96
3523701;Itirapuã;3505302;Barra Bonita;290
3523800;Itobi;3505302;Barra Bonita;214
3523909;Itu;3505302;Barra Bonita;197
3524006;Itupeva;3505302;Barra Bonita;208
3524105;Ituverava;3505302;Barra Bonita;283
3524204;Jaborandi;3505302;Barra Bonita;237
3524303;Jaboticabal;3505302;Barra Bonita;162
3524402;Jacareí;3505302;Barra Bonita;320
3524501;Jaci;3505302;Barra Bonita;239
3524600;Jacupiranga;3505302;Barra Bonita;348
3524709;Jaguariúna;3505302;Barra Bonita;202
3524808;Jales;3505302;Barra Bonita;379
3524907;Jambeiro;3505302;Barra Bonita;361
3525003;Jandira;3505302;Barra Bonita;258
3525102;Jardinópolis;3505302;Barra Bonita;199
3525201;Jarinu;3505302;Barra Bonita;233
3525300;Jaú;3505302;Barra Bonita;23
3525409;Jeriquara;3505302;Barra Bonita;309
3525508;Joanópolis;3505302;Barra Bonita;290
3525607;João Ramalho;3505302;Barra Bonita;283
3525706;José Bonifácio;3505302;Barra Bonita;215
3525805;Júlio Mesquita;3505302;Barra Bonita;175
3525854;Jumirim;3505302;Barra Bonita;136
3525904;Jundiaí;3505302;Barra Bonita;214
3526001;Junqueirópolis;3505302;Barra Bonita;368
3526100;Juquiá;3505302;Barra Bonita;326
3526209;Juquitiba;3505302;Barra Bonita;329
3526308;Lagoinha;3505302;Barra Bonita;436
3526407;Laranjal Paulista;3505302;Barra Bonita;129
3526506;Lavínia;3505302;Barra Bonita;332
3526605;Lavrinhas;3505302;Barra Bonita;468
3526704;Leme;3505302;Barra Bonita;163
3526803;Lençóis Paulista;3505302;Barra Bonita;40
3526902;Limeira;3505302;Barra Bonita;145
3527009;Lindóia;3505302;Barra Bonita;235
3527108;Lins;3505302;Barra Bonita;176
3527207;Lorena;3505302;Barra Bonita;434
3527256;Lourdes;3505302;Barra Bonita;286
3527306;Louveira;3505302;Barra Bonita;202
3527405;Lucélia;3505302;Barra Bonita;318
3527504;Lucianópolis;3505302;Barra Bonita;136
3527603;Luís Antônio;3505302;Barra Bonita;164
3527702;Luiziânia;3505302;Barra Bonita;253
3527801;Lupércio;3505302;Barra Bonita;172
3527900;Lutécia;3505302;Barra Bonita;235
3528007;Macatuba;3505302;Barra Bonita;25
3528106;Macaubal;3505302;Barra Bonita;264
3528205;Macedônia;3505302;Barra Bonita;362
3528403;Mairinque;3505302;Barra Bonita;245
3528502;Mairiporã;3505302;Barra Bonita;265
3528601;Manduri;3505302;Barra Bonita;126
3528700;Marabá Paulista;3505302;Barra Bonita;438
3528809;Maracaí;3505302;Barra Bonita;280
3528858;Marapoama;3505302;Barra Bonita;182
3528908;Mariápolis;3505302;Barra Bonita;342
3529005;Marília;3505302;Barra Bonita;179
3529104;Marinópolis;3505302;Barra Bonita;394
3529203;Martinópolis;3505302;Barra Bonita;327
3529302;Matão;3505302;Barra Bonita;124
3529401;Mauá;3505302;Barra Bonita;294
3529500;Mendonça;3505302;Barra Bonita;195
3529609;Meridiano;3505302;Barra Bonita;339
3529658;Mesópolis;3505302;Barra Bonita;418
3529708;Miguelópolis;3505302;Barra Bonita;311
3529807;Mineiros do Tietê;3505302;Barra Bonita;15
3530003;Mira Estrela;3505302;Barra Bonita;381
3529906;Miracatu;3505302;Barra Bonita;345
3530102;Mirandópolis;3505302;Barra Bonita;339
3530201;Mirante do Paranapanema;3505302;Barra Bonita;428
3530300;Mirassol;3505302;Barra Bonita;246
3530409;Mirassolândia;3505302;Barra Bonita;260
3530508;Mococa;3505302;Barra Bonita;239
3530607;Mogi das Cruzes;3505302;Barra Bonita;318
3530706;Mogi-Guaçu;3505302;Barra Bonita;207
3530805;Moji-Mirim;3505302;Barra Bonita;194
3530904;Mombuca;3505302;Barra Bonita;142
3531001;Monções;3505302;Barra Bonita;299
3531100;Mongaguá;3505302;Barra Bonita;356
3531209;Monte Alegre do Sul;3505302;Barra Bonita;245
3531308;Monte Alto;3505302;Barra Bonita;175
3531407;Monte Aprazível;3505302;Barra Bonita;268
3531506;Monte Azul Paulista;3505302;Barra Bonita;219
3531605;Monte Castelo;3505302;Barra Bonita;422
3531803;Monte Mor;3505302;Barra Bonita;166
3531704;Monteiro Lobato;3505302;Barra Bonita;367
3531902;Morro Agudo;3505302;Barra Bonita;250
3532009;Morungaba;3505302;Barra Bonita;224
3532058;Motuca;3505302;Barra Bonita;135
3532108;Murutinga do Sul;3505302;Barra Bonita;360
3532157;Nantes;3505302;Barra Bonita;342
3532207;Narandiba;3505302;Barra Bonita;397
3532306;Natividade da Serra;3505302;Barra Bonita;430
3532405;Nazaré Paulista;3505302;Barra Bonita;265
3532504;Neves Paulista;3505302;Barra Bonita;242
3532603;Nhandeara;3505302;Barra Bonita;303
3532702;Nipoã;3505302;Barra Bonita;234
3532801;Nova Aliança;3505302;Barra Bonita;217
3532827;Nova Campina;3505302;Barra Bonita;247
3532843;Nova Canaã Paulista;3505302;Barra Bonita;415
3532868;Nova Castilho;3505302;Barra Bonita;322
3532900;Nova Europa;3505302;Barra Bonita;105
3533007;Nova Granada;3505302;Barra Bonita;265
3533106;Nova Guataporanga;3505302;Barra Bonita;403
3533205;Nova Independência;3505302;Barra Bonita;402
3533304;Nova Luzitânia;3505302;Barra Bonita;300
3533403;Nova Odessa;3505302;Barra Bonita;150
3533254;Novais;3505302;Barra Bonita;202
3533502;Novo Horizonte;3505302;Barra Bonita;146
3533601;Nuporanga;3505302;Barra Bonita;243
3533700;Ocauçu;3505302;Barra Bonita;186
3533809;Óleo;3505302;Barra Bonita;134
3533908;Olímpia;3505302;Barra Bonita;232
3534005;Onda Verde;3505302;Barra Bonita;257
3534104;Oriente;3505302;Barra Bonita;202
3534203;Orindiúva;3505302;Barra Bonita;318
3534302;Orlândia;3505302;Barra Bonita;235
3534401;Osasco;3505302;Barra Bonita;271
3534500;Oscar Bressane;3505302;Barra Bonita;221
3534609;Osvaldo Cruz;3505302;Barra Bonita;303
3534708;Ourinhos;3505302;Barra Bonita;196
3534807;Ouro Verde;3505302;Barra Bonita;398
3534757;Ouroeste;3505302;Barra Bonita;382
3534906;Pacaembu;3505302;Barra Bonita;348
3535002;Palestina;3505302;Barra Bonita;287
3535101;Palmares Paulista;3505302;Barra Bonita;188
3535200;Palmeira d Oeste;3505302;Barra Bonita;384
3535309;Palmital;3505302;Barra Bonita;231
3535408;Panorama;3505302;Barra Bonita;417
3535507;Paraguaçu Paulista;3505302;Barra Bonita;256
3535606;Paraibuna;3505302;Barra Bonita;365
3535705;Paraíso;3505302;Barra Bonita;196
3535804;Paranapanema;3505302;Barra Bonita;161
3535903;Paranapuã;3505302;Barra Bonita;400
3536000;Parapuã;3505302;Barra Bonita;304
3536109;Pardinho;3505302;Barra Bonita;81
3536208;Pariquera-Açu;3505302;Barra Bonita;344
3536257;Parisi;3505302;Barra Bonita;327
3536307;Patrocínio Paulista;3505302;Barra Bonita;282
3536406;Paulicéia;3505302;Barra Bonita;415
3536505;Paulínia;3505302;Barra Bonita;172
3536570;Paulistânia;3505302;Barra Bonita;119
3536604;Paulo de Faria;3505302;Barra Bonita;331
3536703;Pederneiras;3505302;Barra Bonita;41
3536802;Pedra Bela;3505302;Barra Bonita;277
3536901;Pedranópolis;3505302;Barra Bonita;341
3537008;Pedregulho;3505302;Barra Bonita;307
3537107;Pedreira;3505302;Barra Bonita;216
3537156;Pedrinhas Paulista;3505302;Barra Bonita;299
3537206;Pedro de Toledo;3505302;Barra Bonita;370
3537305;Penápolis;3505302;Barra Bonita;223
3537404;Pereira Barreto;3505302;Barra Bonita;397
3537503;Pereiras;3505302;Barra Bonita;119
3537602;Peruíbe;3505302;Barra Bonita;396
3537701;Piacatu;3505302;Barra Bonita;280
3537800;Piedade;3505302;Barra Bonita;233
3537909;Pilar do Sul;3505302;Barra Bonita;239
3538006;Pindamonhangaba;3505302;Barra Bonita;392
3538105;Pindorama;3505302;Barra Bonita;172
3538204;Pinhalzinho;3505302;Barra Bonita;262
3538303;Piquerobi;3505302;Barra Bonita;403
3538501;Piquete;3505302;Barra Bonita;453
3538600;Piracaia;3505302;Barra Bonita;266
3538709;Piracicaba;3505302;Barra Bonita;113
3538808;Piraju;3505302;Barra Bonita;151
3538907;Pirajuí;3505302;Barra Bonita;129
3539004;Pirangi;3505302;Barra Bonita;189
3539103;Pirapora do Bom Jesus;3505302;Barra Bonita;247
3539202;Pirapozinho;3505302;Barra Bonita;382
3539301;Pirassununga;3505302;Barra Bonita;152
3539400;Piratininga;3505302;Barra Bonita;87
3539509;Pitangueiras;3505302;Barra Bonita;194
3539608;Planalto;3505302;Barra Bonita;248
3539707;Platina;3505302;Barra Bonita;242
3539806;Poá;3505302;Barra Bonita;303
3539905;Poloni;3505302;Barra Bonita;282
3540002;Pompéia;3505302;Barra Bonita;212
3540101;Pongaí;3505302;Barra Bonita;159
3540200;Pontal;3505302;Barra Bonita;212
3540259;Pontalinda;3505302;Barra Bonita;355
3540309;Pontes Gestal;3505302;Barra Bonita;327
3540408;Populina;3505302;Barra Bonita;416
3540507;Porangaba;3505302;Barra Bonita;117
3540606;Porto Feliz;3505302;Barra Bonita;172
3540705;Porto Ferreira;3505302;Barra Bonita;165
3540754;Potim;3505302;Barra Bonita;431
3540804;Potirendaba;3505302;Barra Bonita;210
3540853;Pracinha;3505302;Barra Bonita;323
3540903;Pradópolis;3505302;Barra Bonita;163
3541000;Praia Grande;3505302;Barra Bonita;338
3541059;Pratânia;3505302;Barra Bonita;40
3541109;Presidente Alves;3505302;Barra Bonita;128
3541208;Presidente Bernardes;3505302;Barra Bonita;379
3541307;Presidente Epitácio;3505302;Barra Bonita;445
3541406;Presidente Prudente;3505302;Barra Bonita;354
3541505;Presidente Venceslau;3505302;Barra Bonita;410
3541604;Promissão;3505302;Barra Bonita;196
3541653;Quadra;3505302;Barra Bonita;139
3541703;Quatá;3505302;Barra Bonita;278
3541802;Queiroz;3505302;Barra Bonita;239
3541901;Queluz;3505302;Barra Bonita;479
3542008;Quintana;3505302;Barra Bonita;229
3542107;Rafard;3505302;Barra Bonita;172
3542206;Rancharia;3505302;Barra Bonita;296
3542305;Redenção da Serra;3505302;Barra Bonita;409
3542404;Regente Feijó;3505302;Barra Bonita;363
3542503;Reginópolis;3505302;Barra Bonita;141
3542602;Registro;3505302;Barra Bonita;319
3542701;Restinga;3505302;Barra Bonita;259
3542800;Ribeira;3505302;Barra Bonita;332
3542909;Ribeirão Bonito;3505302;Barra Bonita;84
3543006;Ribeirão Branco;3505302;Barra Bonita;256
3543105;Ribeirão Corrente;3505302;Barra Bonita;292
3543204;Ribeirão do Sul;3505302;Barra Bonita;210
3543238;Ribeirão dos Índios;3505302;Barra Bonita;405
3543253;Ribeirão Grande;3505302;Barra Bonita;250
3543303;Ribeirão Pires;3505302;Barra Bonita;330
3543402;Ribeirão Preto;3505302;Barra Bonita;179
3543600;Rifaina;3505302;Barra Bonita;334
3543709;Rincão;3505302;Barra Bonita;130
3543808;Rinópolis;3505302;Barra Bonita;293
3543907;Rio Claro;3505302;Barra Bonita;135
3544004;Rio das Pedras;3505302;Barra Bonita;131
3544103;Rio Grande da Serra;3505302;Barra Bonita;337
3544202;Riolândia;3505302;Barra Bonita;349
3543501;Riversul;3505302;Barra Bonita;199
3544251;Rosana;3505302;Barra Bonita;565
3544301;Roseira;3505302;Barra Bonita;405
3544400;Rubiácea;3505302;Barra Bonita;303
3544509;Rubinéia;3505302;Barra Bonita;426
3544608;Sabino;3505302;Barra Bonita;208
3544707;Sagres;3505302;Barra Bonita;305
3544806;Sales;3505302;Barra Bonita;183
3544905;Sales Oliveira;3505302;Barra Bonita;233
3545001;Salesópolis;3505302;Barra Bonita;352
3545100;Salmourão;3505302;Barra Bonita;317
3545159;Saltinho;3505302;Barra Bonita;129
3545209;Salto;3505302;Barra Bonita;183
3545308;Salto de Pirapora;3505302;Barra Bonita;214
3545407;Salto Grande;3505302;Barra Bonita;201
3545506;Sandovalina;3505302;Barra Bonita;420
3545605;Santa Adélia;3505302;Barra Bonita;165
3545704;Santa Albertina;3505302;Barra Bonita;409
3545803;Santa Bárbara d Oeste;3505302;Barra Bonita;137
3546009;Santa Branca;3505302;Barra Bonita;336
3546108;Santa Clara d Oeste;3505302;Barra Bonita;431
3546207;Santa Cruz da Conceição;3505302;Barra Bonita;158
3546256;Santa Cruz da Esperança;3505302;Barra Bonita;217
3546306;Santa Cruz das Palmeiras;3505302;Barra Bonita;182
3546405;Santa Cruz do Rio Pardo;3505302;Barra Bonita;163
3546504;Santa Ernestina;3505302;Barra Bonita;143
3546603;Santa Fé do Sul;3505302;Barra Bonita;419
3546702;Santa Gertrudes;3505302;Barra Bonita;142
3546801;Santa Isabel;3505302;Barra Bonita;312
3546900;Santa Lúcia;3505302;Barra Bonita;116
3547007;Santa Maria da Serra;3505302;Barra Bonita;49
3547106;Santa Mercedes;3505302;Barra Bonita;406
3547403;Santa Rita d Oeste;3505302;Barra Bonita;185
3547502;Santa Rita do Passa Quatro;3505302;Barra Bonita;185
3547601;Santa Rosa de Viterbo;3505302;Barra Bonita;200
3547650;Santa Salete;3505302;Barra Bonita;395
3547205;Santana da Ponte Pensa;3505302;Barra Bonita;408
3547304;Santana de Parnaíba;3505302;Barra Bonita;262
3547700;Santo Anastácio;3505302;Barra Bonita;388
3547809;Santo André;3505302;Barra Bonita;292
3547908;Santo Antônio da Alegria;3505302;Barra Bonita;259
3548005;Santo Antônio de Posse;3505302;Barra Bonita;200
3548054;Santo Antônio do Aracangua;3505302;Barra Bonita;302
3548104;Santo Antônio do Jardim;3505302;Barra Bonita;239
3548203;Santo Antônio do Pinhal;3505302;Barra Bonita;402
3548302;Santo Expedito;3505302;Barra Bonita;395
3548401;Santópolis do Aguapeí;3505302;Barra Bonita;268
3548500;Santos;3505302;Barra Bonita;339
3548609;São Bento do Sapucaí;3505302;Barra Bonita;409
3548708;São Bernardo do Campo;3505302;Barra Bonita;286
3548807;São Caetano do Sul;3505302;Barra Bonita;281
3548906;São Carlos;3505302;Barra Bonita;120
3549003;São Francisco;3505302;Barra Bonita;391
3549102;São João da Boa Vista;3505302;Barra Bonita;218
3549201;São João das Duas Pontes;3505302;Barra Bonita;375
3549250;São João de Iracema;3505302;Barra Bonita;360
3549300;São João do Pau d Alho;3505302;Barra Bonita;410
3549409;São Joaquim da Barra;3505302;Barra Bonita;252
3549508;São José da Bela Vista;3505302;Barra Bonita;263
3549607;São José do Barreiro;3505302;Barra Bonita;515
3549706;São José do Rio Pardo;3505302;Barra Bonita;231
3549805;São José do Rio Preto;3505302;Barra Bonita;232
3549904;São José dos Campos;3505302;Barra Bonita;333
3549953;São Lourenço da Serra;3505302;Barra Bonita;310
3550001;São Luís do Paraitinga;3505302;Barra Bonita;418
3550100;São Manuel;3505302;Barra Bonita;33
3550209;São Miguel Arcanjo;3505302;Barra Bonita;216
3550308;São Paulo;3505302;Barra Bonita;267
3550407;São Pedro;3505302;Barra Bonita;77
3550506;São Pedro do Turvo;3505302;Barra Bonita;180
3550605;São Roque;3505302;Barra Bonita;244
3550704;São Sebastião;3505302;Barra Bonita;432
3550803;São Sebastião da Grama;3505302;Barra Bonita;242
3550902;São Simão;3505302;Barra Bonita;181
3551009;São Vicente;3505302;Barra Bonita;332
3551108;Sarapuí;3505302;Barra Bonita;210
3551207;Sarutaiá;3505302;Barra Bonita;165
3551306;Sebastianópolis do Sul;3505302;Barra Bonita;299
3551405;Serra Azul;3505302;Barra Bonita;195
3551603;Serra Negra;3505302;Barra Bonita;242
3551504;Serrana;3505302;Barra Bonita;193
3551702;Sertãozinho;3505302;Barra Bonita;195
3551801;Sete Barras;3505302;Barra Bonita;300
3551900;Severínia;3505302;Barra Bonita;238
3552007;Silveiras;3505302;Barra Bonita;466
3552106;Socorro;3505302;Barra Bonita;254
3552205;Sorocaba;3505302;Barra Bonita;208
3552304;Sud Mennucci;3505302;Barra Bonita;359
3552403;Sumaré;3505302;Barra Bonita;158
3552551;Suzanápolis;3505302;Barra Bonita;397
3552502;Suzano;3505302;Barra Bonita;305
3552601;Tabapuã;3505302;Barra Bonita;203
3552700;Tabatinga;3505302;Barra Bonita;112
3552809;Taboão da Serra;3505302;Barra Bonita;287
3552908;Taciba;3505302;Barra Bonita;361
3553005;Taguaí;3505302;Barra Bonita;164
3553104;Taiaçu;3505302;Barra Bonita;189
3553203;Taiúva;3505302;Barra Bonita;182
3553302;Tambaú;3505302;Barra Bonita;196
3553401;Tanabi;3505302;Barra Bonita;271
3553500;Tapiraí;3505302;Barra Bonita;268
3553609;Tapiratiba;3505302;Barra Bonita;254
3553658;Taquaral;3505302;Barra Bonita;182
3553708;Taquaritinga;3505302;Barra Bonita;153
3553807;Taquarituba;3505302;Barra Bonita;145
3553856;Taquarivaí;3505302;Barra Bonita;218
3553906;Tarabai;3505302;Barra Bonita;390
3553955;Tarumã;3505302;Barra Bonita;273
3554003;Tatuí;3505302;Barra Bonita;158
3554102;Taubaté;3505302;Barra Bonita;375
3554201;Tejupá;3505302;Barra Bonita;167
3554300;Teodoro Sampaio;3505302;Barra Bonita;471
3554409;Terra Roxa;3505302;Barra Bonita;225
3554508;Tietê;3505302;Barra Bonita;149
3554607;Timburi;3505302;Barra Bonita;173
3554656;Torre de Pedra;3505302;Barra Bonita;122
3554706;Torrinha;3505302;Barra Bonita;48
3554755;Trabiju;3505302;Barra Bonita;69
3554805;Tremembé;3505302;Barra Bonita;387
3554904;Três Fronteiras;3505302;Barra Bonita;416
3554953;Tuiuti;3505302;Barra Bonita;241
3555000;Tupã;3505302;Barra Bonita;259
3555109;Tupi Paulista;3505302;Barra Bonita;391
3555208;Turiúba;3505302;Barra Bonita;291
3555307;Turmalina;3505302;Barra Bonita;401
3555356;Ubarana;3505302;Barra Bonita;216
3555406;Ubatuba;3505302;Barra Bonita;468
3555505;Ubirajara;3505302;Barra Bonita;155
3555604;Uchoa;3505302;Barra Bonita;210
3555703;União Paulista;3505302;Barra Bonita;250
3555802;Urânia;3505302;Barra Bonita;388
3555901;Uru;3505302;Barra Bonita;162
3556008;Urupês;3505302;Barra Bonita;180
3556107;Valentim Gentil;3505302;Barra Bonita;329
3556206;Valinhos;3505302;Barra Bonita;193
3556305;Valparaíso;3505302;Barra Bonita;309
3556354;Vargem;3505302;Barra Bonita;273
3556404;Vargem Grande do Sul;3505302;Barra Bonita;222
3556453;Vargem Grande Paulista;3505302;Barra Bonita;262
3556503;Várzea Paulista;3505302;Barra Bonita;223
3556602;Vera Cruz;3505302;Barra Bonita;165
3556701;Vinhedo;3505302;Barra Bonita;199
3556800;Viradouro;3505302;Barra Bonita;217
3556909;Vista Alegre do Alto;3505302;Barra Bonita;178
3556958;Vitória Brasil;3505302;Barra Bonita;382
3557006;Votorantim;3505302;Barra Bonita;214
3557105;Votuporanga;3505302;Barra Bonita;316
3557154;Zacarias;3505302;Barra Bonita;261
