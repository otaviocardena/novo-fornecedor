# Destino: 3551603 - Serra Negra
# -------------------------------------------------------------------
3500105;Adamantina;3551603;Serra Negra;546
3500204;Adolfo;3551603;Serra Negra;403
3500303;Aguaí;3551603;Serra Negra;93
3500402;Águas da Prata;3551603;Serra Negra;116
3500501;Águas de Lindóia;3551603;Serra Negra;20
3500550;Águas de Santa Bárbara;3551603;Serra Negra;340
3500600;Águas de São Pedro;3551603;Serra Negra;158
3500709;Agudos;3551603;Serra Negra;303
3500758;Alambari;3551603;Serra Negra;226
3500808;Alfredo Marcondes;3551603;Serra Negra;631
3500907;Altair;3551603;Serra Negra;409
3501004;Altinópolis;3551603;Serra Negra;234
3501103;Alto Alegre;3551603;Serra Negra;458
3501152;Alumínio;3551603;Serra Negra;185
3501202;Álvares Florence;3551603;Serra Negra;485
3501301;Álvares Machado;3551603;Serra Negra;613
3501400;Álvaro de Carvalho;3551603;Serra Negra;390
3501509;Alvinlândia;3551603;Serra Negra;395
3501608;Americana;3551603;Serra Negra;106
3501707;Américo Brasiliense;3551603;Serra Negra;232
3501806;Américo de Campos;3551603;Serra Negra;474
3501905;Amparo;3551603;Serra Negra;17
3502002;Analândia;3551603;Serra Negra;154
3502101;Andradina;3551603;Serra Negra;595
3502200;Angatuba;3551603;Serra Negra;274
3502309;Anhembi;3551603;Serra Negra;200
3502408;Anhumas;3551603;Serra Negra;596
3502507;Aparecida;3551603;Serra Negra;247
3502606;Aparecida dOeste;3551603;Serra Negra;577
3502705;Apiaí;3551603;Serra Negra;385
3502754;Araçariguama;3551603;Serra Negra;154
3502804;Araçatuba;3551603;Serra Negra;495
3502903;Araçoiaba da Serra;3551603;Serra Negra;194
3503000;Aramina;3551603;Serra Negra;372
3503109;Arandu;3551603;Serra Negra;325
3503158;Arapeí;3551603;Serra Negra;375
3503208;Araraquara;3551603;Serra Negra;221
3503307;Araras;3551603;Serra Negra;99
3503356;Arco-Íris;3551603;Serra Negra;500
3503406;Arealva;3551603;Serra Negra;295
3503505;Areias;3551603;Serra Negra;324
3503604;Areiópolis;3551603;Serra Negra;261
3503703;Ariranha;3551603;Serra Negra;325
3503802;Artur Nogueira;3551603;Serra Negra;74
3503901;Arujá;3551603;Serra Negra;155
3503950;Aspásia;3551603;Serra Negra;555
3504008;Assis;3551603;Serra Negra;480
3504107;Atibaia;3551603;Serra Negra;79
3504206;Auriflama;3551603;Serra Negra;516
3504305;Avaí;3551603;Serra Negra;335
3504404;Avanhandava;3551603;Serra Negra;440
3504503;Avaré;3551603;Serra Negra;313
3504602;Bady Bassitt;3551603;Serra Negra;398
3504701;Balbinos;3551603;Serra Negra;369
3504800;Bálsamo;3551603;Serra Negra;411
3504909;Bananal;3551603;Serra Negra;393
3505005;Barão de Antonina;3551603;Serra Negra;425
3505104;Barbosa;3551603;Serra Negra;466
3505203;Bariri;3551603;Serra Negra;270
3505302;Barra Bonita;3551603;Serra Negra;241
3505351;Barra do Chapéu;3551603;Serra Negra;409
3505401;Barra do Turvo;3551603;Serra Negra;451
3505500;Barretos;3551603;Serra Negra;368
3505609;Barrinha;3551603;Serra Negra;286
3505708;Barueri;3551603;Serra Negra;145
3505807;Bastos;3551603;Serra Negra;504
3505906;Batatais;3551603;Serra Negra;261
3506003;Bauru;3551603;Serra Negra;297
3506102;Bebedouro;3551603;Serra Negra;327
3506201;Bento de Abreu;3551603;Serra Negra;531
3506300;Bernardino de Campos;3551603;Serra Negra;372
3506359;Bertioga;3551603;Serra Negra;230
3506409;Bilac;3551603;Serra Negra;491
3506508;Birigui;3551603;Serra Negra;475
3506607;Biritiba Mirim;3551603;Serra Negra;195
3506706;Boa Esperança do Sul;3551603;Serra Negra;249
3506805;Bocaina;3551603;Serra Negra;246
3506904;Bofete;3551603;Serra Negra;232
3507001;Boituva;3551603;Serra Negra;181
3507100;Bom Jesus dos Perdões;3551603;Serra Negra;86
3507159;Bom Sucesso de Itararé;3551603;Serra Negra;410
3507209;Borá;3551603;Serra Negra;488
3507308;Boracéia;3551603;Serra Negra;275
3507407;Borborema;3551603;Serra Negra;325
3507456;Borebi;3551603;Serra Negra;298
3507506;Botucatu;3551603;Serra Negra;243
3507605;Bragança Paulista;3551603;Serra Negra;53
3507704;Braúna;3551603;Serra Negra;467
3507753;Brejo Alegre;3551603;Serra Negra;482
3507803;Brodósqui;3551603;Serra Negra;272
3507902;Brotas;3551603;Serra Negra;194
3508009;Buri;3551603;Serra Negra;317
3508108;Buritama;3551603;Serra Negra;483
3508207;Buritizal;3551603;Serra Negra;370
3508306;Cabrália Paulista;3551603;Serra Negra;337
3508405;Cabreúva;3551603;Serra Negra;120
3508504;Caçapava;3551603;Serra Negra;183
3508603;Cachoeira Paulista;3551603;Serra Negra;279
3508702;Caconde;3551603;Serra Negra;183
3508801;Cafelândia;3551603;Serra Negra;380
3508900;Caiabu;3551603;Serra Negra;569
3509007;Caieiras;3551603;Serra Negra;124
3509106;Caiuá;3551603;Serra Negra;672
3509205;Cajamar;3551603;Serra Negra;122
3509254;Cajati;3551603;Serra Negra;360
3509304;Cajobi;3551603;Serra Negra;366
3509403;Cajuru;3551603;Serra Negra;199
3509452;Campina do Monte Alegre;3551603;Serra Negra;283
3509502;Campinas;3551603;Serra Negra;81
3509601;Campo Limpo Paulista;3551603;Serra Negra;100
3509700;Campos do Jordão;3551603;Serra Negra;244
3509809;Campos Novos Paulista;3551603;Serra Negra;434
3509908;Cananéia;3551603;Serra Negra;386
3509957;Canas;3551603;Serra Negra;272
3510005;Cândido Mota;3551603;Serra Negra;477
3510104;Cândido Rodrigues;3551603;Serra Negra;300
3510153;Canitar;3551603;Serra Negra;421
3510203;Capão Bonito;3551603;Serra Negra;291
3510302;Capela do Alto;3551603;Serra Negra;209
3510401;Capivari;3551603;Serra Negra;130
3510500;Caraguatatuba;3551603;Serra Negra;245
3510609;Carapicuíba;3551603;Serra Negra;141
3510708;Cardoso;3551603;Serra Negra;507
3510807;Casa Branca;3551603;Serra Negra;130
3510906;Cássia dos Coqueiros;3551603;Serra Negra;205
3511003;Castilho;3551603;Serra Negra;609
3511102;Catanduva;3551603;Serra Negra;333
3511201;Catiguá;3551603;Serra Negra;347
3511300;Cedral;3551603;Serra Negra;372
3511409;Cerqueira César;3551603;Serra Negra;337
3511508;Cerquilho;3551603;Serra Negra;164
3511607;Cesário Lange;3551603;Serra Negra;192
3511706;Charqueada;3551603;Serra Negra;150
3557204;Chavantes;3551603;Serra Negra;399
3511904;Clementina;3551603;Serra Negra;484
3512001;Colina;3551603;Serra Negra;351
3512100;Colômbia;3551603;Serra Negra;412
3512209;Conchal;3551603;Serra Negra;73
3512308;Conchas;3551603;Serra Negra;194
3512407;Cordeirópolis;3551603;Serra Negra;107
3512506;Coroados;3551603;Serra Negra;465
3512605;Coronel Macedo;3551603;Serra Negra;391
3512704;Corumbataí;3551603;Serra Negra;141
3512803;Cosmópolis;3551603;Serra Negra;80
3512902;Cosmorama;3551603;Serra Negra;449
3513009;Cotia;3551603;Serra Negra;161
3513108;Cravinhos;3551603;Serra Negra;227
3513207;Cristais Paulista;3551603;Serra Negra;327
3513306;Cruzália;3551603;Serra Negra;526
3513405;Cruzeiro;3551603;Serra Negra;294
3513504;Cubatão;3551603;Serra Negra;196
3513603;Cunha;3551603;Serra Negra;298
3513702;Descalvado;3551603;Serra Negra;178
3513801;Diadema;3551603;Serra Negra;160
3513850;Dirce Reis;3551603;Serra Negra;555
3513900;Divinolândia;3551603;Serra Negra;160
3514007;Dobrada;3551603;Serra Negra;266
3514106;Dois Córregos;3551603;Serra Negra;233
3514205;Dolcinópolis;3551603;Serra Negra;547
3514304;Dourado;3551603;Serra Negra;224
3514403;Dracena;3551603;Serra Negra;602
3514502;Duartina;3551603;Serra Negra;346
3514601;Dumont;3551603;Serra Negra;265
3514700;Echaporã;3551603;Serra Negra;442
3514809;Eldorado;3551603;Serra Negra;373
3514908;Elias Fausto;3551603;Serra Negra;122
3514924;Elisiário;3551603;Serra Negra;345
3514957;Embaúba;3551603;Serra Negra;365
3515004;Embu;3551603;Serra Negra;157
3515103;Embu-Guaçu;3551603;Serra Negra;182
3515129;Emilianópolis;3551603;Serra Negra;649
3515152;Engenheiro Coelho;3551603;Serra Negra;75
3515186;Espírito Santo do Pinhal;3551603;Serra Negra;81
3515194;Espírito Santo do Turvo;3551603;Serra Negra;355
3557303;Estiva Gerbi;3551603;Serra Negra;69
3515301;Estrela do Norte;3551603;Serra Negra;641
3515202;Estrela dOeste;3551603;Serra Negra;641
3515350;Euclides da Cunha Paulista;3551603;Serra Negra;748
3515400;Fartura;3551603;Serra Negra;406
3515608;Fernando Prestes;3551603;Serra Negra;310
3515509;Fernandópolis;3551603;Serra Negra;501
3515657;Fernão;3551603;Serra Negra;352
3515707;Ferraz de Vasconcelos;3551603;Serra Negra;161
3515806;Flora Rica;3551603;Serra Negra;598
3515905;Floreal;3551603;Serra Negra;469
3516002;Flórida Paulista;3551603;Serra Negra;561
3516101;Florínea;3551603;Serra Negra;521
3516200;Franca;3551603;Serra Negra;312
3516309;Francisco Morato;3551603;Serra Negra;120
3516408;Franco da Rocha;3551603;Serra Negra;114
3516507;Gabriel Monteiro;3551603;Serra Negra;497
3516606;Gália;3551603;Serra Negra;361
3516705;Garça;3551603;Serra Negra;373
3516804;Gastão Vidigal;3551603;Serra Negra;486
3516853;Gavião Peixoto;3551603;Serra Negra;254
3516903;General Salgado;3551603;Serra Negra;493
3517000;Getulina;3551603;Serra Negra;421
3517109;Glicério;3551603;Serra Negra;458
3517208;Guaiçara;3551603;Serra Negra;407
3517307;Guaimbê;3551603;Serra Negra;417
3517406;Guaíra;3551603;Serra Negra;366
3517505;Guapiaçu;3551603;Serra Negra;388
3517604;Guapiara;3551603;Serra Negra;324
3517703;Guará;3551603;Serra Negra;334
3517802;Guaraçaí;3551603;Serra Negra;574
3517901;Guaraci;3551603;Serra Negra;403
3518008;Guarani dOeste;3551603;Serra Negra;527
3518107;Guarantã;3551603;Serra Negra;373
3518206;Guararapes;3551603;Serra Negra;513
3518305;Guararema;3551603;Serra Negra;160
3518404;Guaratinguetá;3551603;Serra Negra;253
3518503;Guareí;3551603;Serra Negra;230
3518602;Guariba;3551603;Serra Negra;275
3518701;Guarujá;3551603;Serra Negra;226
3518800;Guarulhos;3551603;Serra Negra;127
3518859;Guatapará;3551603;Serra Negra;245
3518909;Guzolândia;3551603;Serra Negra;525
3519006;Herculândia;3551603;Serra Negra;466
3519055;Holambra;3551603;Serra Negra;62
3519071;Hortolândia;3551603;Serra Negra;93
3519105;Iacanga;3551603;Serra Negra;331
3519204;Iacri;3551603;Serra Negra;501
3519253;Iaras;3551603;Serra Negra;330
3519303;Ibaté;3551603;Serra Negra;195
3519402;Ibirá;3551603;Serra Negra;365
3519501;Ibirarema;3551603;Serra Negra;441
3519600;Ibitinga;3551603;Serra Negra;295
3519709;Ibiúna;3551603;Serra Negra;199
3519808;Icém;3551603;Serra Negra;431
3519907;Iepê;3551603;Serra Negra;562
3520004;Igaraçu do Tietê;3551603;Serra Negra;245
3520103;Igarapava;3551603;Serra Negra;380
3520202;Igaratá;3551603;Serra Negra;124
3520301;Iguape;3551603;Serra Negra;330
3520426;Ilha Comprida;3551603;Serra Negra;337
3520442;Ilha Solteira;3551603;Serra Negra;608
3520400;Ilhabela;3551603;Serra Negra;281
3520509;Indaiatuba;3551603;Serra Negra;114
3520608;Indiana;3551603;Serra Negra;563
3520707;Indiaporã;3551603;Serra Negra;545
3520806;Inúbia Paulista;3551603;Serra Negra;533
3520905;Ipaussu;3551603;Serra Negra;389
3521002;Iperó;3551603;Serra Negra;189
3521101;Ipeúna;3551603;Serra Negra;140
3521150;Ipiguá;3551603;Serra Negra;405
3521200;Iporanga;3551603;Serra Negra;417
3521309;Ipuã;3551603;Serra Negra;346
3521408;Iracemápolis;3551603;Serra Negra;112
3521507;Irapuã;3551603;Serra Negra;377
3521606;Irapuru;3551603;Serra Negra;585
3521705;Itaberá;3551603;Serra Negra;384
3521804;Itaí;3551603;Serra Negra;353
3521903;Itajobi;3551603;Serra Negra;341
3522000;Itaju;3551603;Serra Negra;282
3522109;Itanhaém;3551603;Serra Negra;246
3522158;Itaóca;3551603;Serra Negra;404
3522208;Itapecerica da Serra;3551603;Serra Negra;165
3522307;Itapetininga;3551603;Serra Negra;229
3522406;Itapeva;3551603;Serra Negra;350
3522505;Itapevi;3551603;Serra Negra;155
3522604;Itapira;3551603;Serra Negra;32
3522653;Itapirapuã Paulista;3551603;Serra Negra;439
3522703;Itápolis;3551603;Serra Negra;301
3522802;Itaporanga;3551603;Serra Negra;411
3522901;Itapuí;3551603;Serra Negra;266
3523008;Itapura;3551603;Serra Negra;625
3523107;Itaquaquecetuba;3551603;Serra Negra;152
3523206;Itararé;3551603;Serra Negra;404
3523305;Itariri;3551603;Serra Negra;282
3523404;Itatiba;3551603;Serra Negra;62
3523503;Itatinga;3551603;Serra Negra;272
3523602;Itirapina;3551603;Serra Negra;161
3523701;Itirapuã;3551603;Serra Negra;295
3523800;Itobi;3551603;Serra Negra;137
3523909;Itu;3551603;Serra Negra;135
3524006;Itupeva;3551603;Serra Negra;106
3524105;Ituverava;3551603;Serra Negra;347
3524204;Jaborandi;3551603;Serra Negra;359
3524303;Jaboticabal;3551603;Serra Negra;290
3524402;Jacareí;3551603;Serra Negra;151
3524501;Jaci;3551603;Serra Negra;411
3524600;Jacupiranga;3551603;Serra Negra;348
3524709;Jaguariúna;3551603;Serra Negra;48
3524808;Jales;3551603;Serra Negra;533
3524907;Jambeiro;3551603;Serra Negra;193
3525003;Jandira;3551603;Serra Negra;150
3525102;Jardinópolis;3551603;Serra Negra;263
3525201;Jarinu;3551603;Serra Negra;87
3525300;Jaú;3551603;Serra Negra;246
3525409;Jeriquara;3551603;Serra Negra;350
3525508;Joanópolis;3551603;Serra Negra;95
3525607;João Ramalho;3551603;Serra Negra;506
3525706;José Bonifácio;3551603;Serra Negra;429
3525805;Júlio Mesquita;3551603;Serra Negra;398
3525854;Jumirim;3551603;Serra Negra;167
3525904;Jundiaí;3551603;Serra Negra;91
3526001;Junqueirópolis;3551603;Serra Negra;592
3526100;Juquiá;3551603;Serra Negra;289
3526209;Juquitiba;3551603;Serra Negra;202
3526308;Lagoinha;3551603;Serra Negra;268
3526407;Laranjal Paulista;3551603;Serra Negra;174
3526506;Lavínia;3551603;Serra Negra;555
3526605;Lavrinhas;3551603;Serra Negra;300
3526704;Leme;3551603;Serra Negra;124
3526803;Lençóis Paulista;3551603;Serra Negra;277
3526902;Limeira;3551603;Serra Negra;99
3527009;Lindóia;3551603;Serra Negra;14
3527108;Lins;3551603;Serra Negra;400
3527207;Lorena;3551603;Serra Negra;266
3527256;Lourdes;3551603;Serra Negra;502
3527306;Louveira;3551603;Serra Negra;86
3527405;Lucélia;3551603;Serra Negra;541
3527504;Lucianópolis;3551603;Serra Negra;359
3527603;Luís Antônio;3551603;Serra Negra;208
3527702;Luiziânia;3551603;Serra Negra;477
3527801;Lupércio;3551603;Serra Negra;396
3527900;Lutécia;3551603;Serra Negra;458
3528007;Macatuba;3551603;Serra Negra;265
3528106;Macaubal;3551603;Serra Negra;459
3528205;Macedônia;3551603;Serra Negra;515
3528403;Mairinque;3551603;Serra Negra;175
3528502;Mairiporã;3551603;Serra Negra;102
3528601;Manduri;3551603;Serra Negra;354
3528700;Marabá Paulista;3551603;Serra Negra;684
3528809;Maracaí;3551603;Serra Negra;508
3528858;Marapoama;3551603;Serra Negra;355
3528908;Mariápolis;3551603;Serra Negra;565
3529005;Marília;3551603;Serra Negra;403
3529104;Marinópolis;3551603;Serra Negra;570
3529203;Martinópolis;3551603;Serra Negra;551
3529302;Matão;3551603;Serra Negra;253
3529401;Mauá;3551603;Serra Negra;200
3529500;Mendonça;3551603;Serra Negra;396
3529609;Meridiano;3551603;Serra Negra;493
3529658;Mesópolis;3551603;Serra Negra;568
3529708;Miguelópolis;3551603;Serra Negra;375
3529807;Mineiros do Tietê;3551603;Serra Negra;240
3530003;Mira Estrela;3551603;Serra Negra;535
3529906;Miracatu;3551603;Serra Negra;268
3530102;Mirandópolis;3551603;Serra Negra;563
3530201;Mirante do Paranapanema;3551603;Serra Negra;656
3530300;Mirassol;3551603;Serra Negra;400
3530409;Mirassolândia;3551603;Serra Negra;414
3530508;Mococa;3551603;Serra Negra;163
3530607;Mogi das Cruzes;3551603;Serra Negra;173
3530706;Mogi-Guaçu;3551603;Serra Negra;59
3530805;Moji-Mirim;3551603;Serra Negra;47
3530904;Mombuca;3551603;Serra Negra;143
3531001;Monções;3551603;Serra Negra;476
3531100;Mongaguá;3551603;Serra Negra;229
3531209;Monte Alegre do Sul;3551603;Serra Negra;9
3531308;Monte Alto;3551603;Serra Negra;304
3531407;Monte Aprazível;3551603;Serra Negra;422
3531506;Monte Azul Paulista;3551603;Serra Negra;344
3531605;Monte Castelo;3551603;Serra Negra;645
3531803;Monte Mor;3551603;Serra Negra;107
3531704;Monteiro Lobato;3551603;Serra Negra;198
3531902;Morro Agudo;3551603;Serra Negra;314
3532009;Morungaba;3551603;Serra Negra;43
3532058;Motuca;3551603;Serra Negra;255
3532108;Murutinga do Sul;3551603;Serra Negra;583
3532157;Nantes;3551603;Serra Negra;571
3532207;Narandiba;3551603;Serra Negra;625
3532306;Natividade da Serra;3551603;Serra Negra;262
3532405;Nazaré Paulista;3551603;Serra Negra;97
3532504;Neves Paulista;3551603;Serra Negra;418
3532603;Nhandeara;3551603;Serra Negra;457
3532702;Nipoã;3551603;Serra Negra;442
3532801;Nova Aliança;3551603;Serra Negra;395
3532827;Nova Campina;3551603;Serra Negra;370
3532843;Nova Canaã Paulista;3551603;Serra Negra;594
3532868;Nova Castilho;3551603;Serra Negra;513
3532900;Nova Europa;3551603;Serra Negra;265
3533007;Nova Granada;3551603;Serra Negra;419
3533106;Nova Guataporanga;3551603;Serra Negra;626
3533205;Nova Independência;3551603;Serra Negra;626
3533304;Nova Luzitânia;3551603;Serra Negra;495
3533403;Nova Odessa;3551603;Serra Negra;101
3533254;Novais;3551603;Serra Negra;356
3533502;Novo Horizonte;3551603;Serra Negra;347
3533601;Nuporanga;3551603;Serra Negra;289
3533700;Ocauçu;3551603;Serra Negra;409
3533809;Óleo;3551603;Serra Negra;363
3533908;Olímpia;3551603;Serra Negra;380
3534005;Onda Verde;3551603;Serra Negra;411
3534104;Oriente;3551603;Serra Negra;425
3534203;Orindiúva;3551603;Serra Negra;459
3534302;Orlândia;3551603;Serra Negra;299
3534401;Osasco;3551603;Serra Negra;144
3534500;Oscar Bressane;3551603;Serra Negra;444
3534609;Osvaldo Cruz;3551603;Serra Negra;526
3534708;Ourinhos;3551603;Serra Negra;424
3534807;Ouro Verde;3551603;Serra Negra;621
3534757;Ouroeste;3551603;Serra Negra;536
3534906;Pacaembu;3551603;Serra Negra;571
3535002;Palestina;3551603;Serra Negra;441
3535101;Palmares Paulista;3551603;Serra Negra;342
3535200;Palmeira d Oeste;3551603;Serra Negra;561
3535309;Palmital;3551603;Serra Negra;460
3535408;Panorama;3551603;Serra Negra;641
3535507;Paraguaçu Paulista;3551603;Serra Negra;480
3535606;Paraibuna;3551603;Serra Negra;197
3535705;Paraíso;3551603;Serra Negra;344
3535804;Paranapanema;3551603;Serra Negra;321
3535903;Paranapuã;3551603;Serra Negra;554
3536000;Parapuã;3551603;Serra Negra;527
3536109;Pardinho;3551603;Serra Negra;253
3536208;Pariquera-Açu;3551603;Serra Negra;344
3536257;Parisi;3551603;Serra Negra;481
3536307;Patrocínio Paulista;3551603;Serra Negra;296
3536406;Paulicéia;3551603;Serra Negra;639
3536505;Paulínia;3551603;Serra Negra;82
3536570;Paulistânia;3551603;Serra Negra;343
3536604;Paulo de Faria;3551603;Serra Negra;472
3536703;Pederneiras;3551603;Serra Negra;269
3536802;Pedra Bela;3551603;Serra Negra;47
3536901;Pedranópolis;3551603;Serra Negra;495
3537008;Pedregulho;3551603;Serra Negra;349
3537107;Pedreira;3551603;Serra Negra;35
3537156;Pedrinhas Paulista;3551603;Serra Negra;528
3537206;Pedro de Toledo;3551603;Serra Negra;275
3537305;Penápolis;3551603;Serra Negra;447
3537404;Pereira Barreto;3551603;Serra Negra;571
3537503;Pereiras;3551603;Serra Negra;192
3537602;Peruíbe;3551603;Serra Negra;275
3537701;Piacatu;3551603;Serra Negra;504
3537800;Piedade;3551603;Serra Negra;202
3537909;Pilar do Sul;3551603;Serra Negra;222
3538006;Pindamonhangaba;3551603;Serra Negra;223
3538105;Pindorama;3551603;Serra Negra;326
3538204;Pinhalzinho;3551603;Serra Negra;27
3538303;Piquerobi;3551603;Serra Negra;650
3538501;Piquete;3551603;Serra Negra;284
3538600;Piracaia;3551603;Serra Negra;79
3538709;Piracicaba;3551603;Serra Negra;135
3538808;Piraju;3551603;Serra Negra;379
3538907;Pirajuí;3551603;Serra Negra;352
3539004;Pirangi;3551603;Serra Negra;327
3539103;Pirapora do Bom Jesus;3551603;Serra Negra;144
3539202;Pirapozinho;3551603;Serra Negra;610
3539301;Pirassununga;3551603;Serra Negra;138
3539400;Piratininga;3551603;Serra Negra;310
3539509;Pitangueiras;3551603;Serra Negra;298
3539608;Planalto;3551603;Serra Negra;458
3539707;Platina;3551603;Serra Negra;471
3539806;Poá;3551603;Serra Negra;158
3539905;Poloni;3551603;Serra Negra;435
3540002;Pompéia;3551603;Serra Negra;436
3540101;Pongaí;3551603;Serra Negra;362
3540200;Pontal;3551603;Serra Negra;285
3540259;Pontalinda;3551603;Serra Negra;527
3540309;Pontes Gestal;3551603;Serra Negra;481
3540408;Populina;3551603;Serra Negra;558
3540507;Porangaba;3551603;Serra Negra;213
3540606;Porto Feliz;3551603;Serra Negra;161
3540705;Porto Ferreira;3551603;Serra Negra;162
3540754;Potim;3551603;Serra Negra;262
3540804;Potirendaba;3551603;Serra Negra;380
3540853;Pracinha;3551603;Serra Negra;546
3540903;Pradópolis;3551603;Serra Negra;255
3541000;Praia Grande;3551603;Serra Negra;210
3541059;Pratânia;3551603;Serra Negra;260
3541109;Presidente Alves;3551603;Serra Negra;351
3541208;Presidente Bernardes;3551603;Serra Negra;625
3541307;Presidente Epitácio;3551603;Serra Negra;691
3541406;Presidente Prudente;3551603;Serra Negra;577
3541505;Presidente Venceslau;3551603;Serra Negra;656
3541604;Promissão;3551603;Serra Negra;419
3541653;Quadra;3551603;Serra Negra;211
3541703;Quatá;3551603;Serra Negra;501
3541802;Queiroz;3551603;Serra Negra;462
3541901;Queluz;3551603;Serra Negra;311
3542008;Quintana;3551603;Serra Negra;452
3542107;Rafard;3551603;Serra Negra;138
3542206;Rancharia;3551603;Serra Negra;519
3542305;Redenção da Serra;3551603;Serra Negra;241
3542404;Regente Feijó;3551603;Serra Negra;592
3542503;Reginópolis;3551603;Serra Negra;350
3542602;Registro;3551603;Serra Negra;318
3542701;Restinga;3551603;Serra Negra;301
3542800;Ribeira;3551603;Serra Negra;416
3542909;Ribeirão Bonito;3551603;Serra Negra;211
3543006;Ribeirão Branco;3551603;Serra Negra;355
3543105;Ribeirão Corrente;3551603;Serra Negra;334
3543204;Ribeirão do Sul;3551603;Serra Negra;439
3543238;Ribeirão dos Índios;3551603;Serra Negra;651
3543253;Ribeirão Grande;3551603;Serra Negra;301
3543303;Ribeirão Pires;3551603;Serra Negra;185
3543402;Ribeirão Preto;3551603;Serra Negra;248
3543600;Rifaina;3551603;Serra Negra;376
3543709;Rincão;3551603;Serra Negra;239
3543808;Rinópolis;3551603;Serra Negra;516
3543907;Rio Claro;3551603;Serra Negra;121
3544004;Rio das Pedras;3551603;Serra Negra;144
3544103;Rio Grande da Serra;3551603;Serra Negra;191
3544202;Riolândia;3551603;Serra Negra;503
3543501;Riversul;3551603;Serra Negra;420
3544251;Rosana;3551603;Serra Negra;794
3544301;Roseira;3551603;Serra Negra;236
3544400;Rubiácea;3551603;Serra Negra;526
3544509;Rubinéia;3551603;Serra Negra;580
3544608;Sabino;3551603;Serra Negra;432
3544707;Sagres;3551603;Serra Negra;529
3544806;Sales;3551603;Serra Negra;383
3544905;Sales Oliveira;3551603;Serra Negra;297
3545001;Salesópolis;3551603;Serra Negra;184
3545100;Salmourão;3551603;Serra Negra;541
3545159;Saltinho;3551603;Serra Negra;148
3545209;Salto;3551603;Serra Negra;130
3545308;Salto de Pirapora;3551603;Serra Negra;197
3545407;Salto Grande;3551603;Serra Negra;429
3545506;Sandovalina;3551603;Serra Negra;649
3545605;Santa Adélia;3551603;Serra Negra;319
3545704;Santa Albertina;3551603;Serra Negra;563
3545803;Santa Bárbara d Oeste;3551603;Serra Negra;117
3546009;Santa Branca;3551603;Serra Negra;168
3546108;Santa Clara d Oeste;3551603;Serra Negra;585
3546207;Santa Cruz da Conceição;3551603;Serra Negra;133
3546256;Santa Cruz da Esperança;3551603;Serra Negra;215
3546306;Santa Cruz das Palmeiras;3551603;Serra Negra;146
3546405;Santa Cruz do Rio Pardo;3551603;Serra Negra;392
3546504;Santa Ernestina;3551603;Serra Negra;272
3546603;Santa Fé do Sul;3551603;Serra Negra;573
3546702;Santa Gertrudes;3551603;Serra Negra;115
3546801;Santa Isabel;3551603;Serra Negra;144
3546900;Santa Lúcia;3551603;Serra Negra;238
3547007;Santa Maria da Serra;3551603;Serra Negra;192
3547106;Santa Mercedes;3551603;Serra Negra;630
3547403;Santa Rita d Oeste;3551603;Serra Negra;183
3547502;Santa Rita do Passa Quatro;3551603;Serra Negra;183
3547601;Santa Rosa de Viterbo;3551603;Serra Negra;186
3547650;Santa Salete;3551603;Serra Negra;549
3547205;Santana da Ponte Pensa;3551603;Serra Negra;562
3547304;Santana de Parnaíba;3551603;Serra Negra;133
3547700;Santo Anastácio;3551603;Serra Negra;635
3547809;Santo André;3551603;Serra Negra;164
3547908;Santo Antônio da Alegria;3551603;Serra Negra;232
3548005;Santo Antônio de Posse;3551603;Serra Negra;47
3548054;Santo Antônio do Aracangua;3551603;Serra Negra;525
3548104;Santo Antônio do Jardim;3551603;Serra Negra;93
3548203;Santo Antônio do Pinhal;3551603;Serra Negra;234
3548302;Santo Expedito;3551603;Serra Negra;645
3548401;Santópolis do Aguapeí;3551603;Serra Negra;491
3548500;Santos;3551603;Serra Negra;211
3548609;São Bento do Sapucaí;3551603;Serra Negra;240
3548708;São Bernardo do Campo;3551603;Serra Negra;158
3548807;São Caetano do Sul;3551603;Serra Negra;154
3548906;São Carlos;3551603;Serra Negra;180
3549003;São Francisco;3551603;Serra Negra;554
3549102;São João da Boa Vista;3551603;Serra Negra;108
3549201;São João das Duas Pontes;3551603;Serra Negra;520
3549250;São João de Iracema;3551603;Serra Negra;511
3549300;São João do Pau d Alho;3551603;Serra Negra;633
3549409;São Joaquim da Barra;3551603;Serra Negra;316
3549508;São José da Bela Vista;3551603;Serra Negra;305
3549607;São José do Barreiro;3551603;Serra Negra;346
3549706;São José do Rio Pardo;3551603;Serra Negra;155
3549805;São José do Rio Preto;3551603;Serra Negra;386
3549904;São José dos Campos;3551603;Serra Negra;165
3549953;São Lourenço da Serra;3551603;Serra Negra;183
3550001;São Luís do Paraitinga;3551603;Serra Negra;249
3550100;São Manuel;3551603;Serra Negra;249
3550209;São Miguel Arcanjo;3551603;Serra Negra;257
3550308;São Paulo;3551603;Serra Negra;139
3550407;São Pedro;3551603;Serra Negra;167
3550506;São Pedro do Turvo;3551603;Serra Negra;408
3550605;São Roque;3551603;Serra Negra;170
3550704;São Sebastião;3551603;Serra Negra;264
3550803;São Sebastião da Grama;3551603;Serra Negra;146
3550902;São Simão;3551603;Serra Negra;204
3551009;São Vicente;3551603;Serra Negra;204
3551108;Sarapuí;3551603;Serra Negra;223
3551207;Sarutaiá;3551603;Serra Negra;393
3551306;Sebastianópolis do Sul;3551603;Serra Negra;452
3551405;Serra Azul;3551603;Serra Negra;231
3551504;Serrana;3551603;Serra Negra;239
3551702;Sertãozinho;3551603;Serra Negra;268
3551801;Sete Barras;3551603;Serra Negra;333
3551900;Severínia;3551603;Serra Negra;363
3552007;Silveiras;3551603;Serra Negra;297
3552106;Socorro;3551603;Serra Negra;28
3552205;Sorocaba;3551603;Serra Negra;172
3552304;Sud Mennucci;3551603;Serra Negra;562
3552403;Sumaré;3551603;Serra Negra;98
3552551;Suzanápolis;3551603;Serra Negra;574
3552502;Suzano;3551603;Serra Negra;160
3552601;Tabapuã;3551603;Serra Negra;357
3552700;Tabatinga;3551603;Serra Negra;279
3552809;Taboão da Serra;3551603;Serra Negra;160
3552908;Taciba;3551603;Serra Negra;590
3553005;Taguaí;3551603;Serra Negra;392
3553104;Taiaçu;3551603;Serra Negra;318
3553203;Taiúva;3551603;Serra Negra;311
3553302;Tambaú;3551603;Serra Negra;158
3553401;Tanabi;3551603;Serra Negra;425
3553500;Tapiraí;3551603;Serra Negra;236
3553609;Tapiratiba;3551603;Serra Negra;177
3553658;Taquaral;3551603;Serra Negra;310
3553708;Taquaritinga;3551603;Serra Negra;282
3553807;Taquarituba;3551603;Serra Negra;374
3553856;Taquarivaí;3551603;Serra Negra;328
3553906;Tarabai;3551603;Serra Negra;619
3553955;Tarumã;3551603;Serra Negra;502
3554003;Tatuí;3551603;Serra Negra;189
3554102;Taubaté;3551603;Serra Negra;207
3554201;Tejupá;3551603;Serra Negra;396
3554300;Teodoro Sampaio;3551603;Serra Negra;699
3554409;Terra Roxa;3551603;Serra Negra;349
3554508;Tietê;3551603;Serra Negra;158
3554607;Timburi;3551603;Serra Negra;402
3554656;Torre de Pedra;3551603;Serra Negra;221
3554706;Torrinha;3551603;Serra Negra;208
3554755;Trabiju;3551603;Serra Negra;256
3554805;Tremembé;3551603;Serra Negra;218
3554904;Três Fronteiras;3551603;Serra Negra;570
3554953;Tuiuti;3551603;Serra Negra;38
3555000;Tupã;3551603;Serra Negra;482
3555109;Tupi Paulista;3551603;Serra Negra;615
3555208;Turiúba;3551603;Serra Negra;492
3555307;Turmalina;3551603;Serra Negra;545
3555356;Ubarana;3551603;Serra Negra;416
3555406;Ubatuba;3551603;Serra Negra;299
3555505;Ubirajara;3551603;Serra Negra;379
3555604;Uchoa;3551603;Serra Negra;364
3555703;União Paulista;3551603;Serra Negra;451
3555802;Urânia;3551603;Serra Negra;542
3555901;Uru;3551603;Serra Negra;369
3556008;Urupês;3551603;Serra Negra;367
3556107;Valentim Gentil;3551603;Serra Negra;483
3556206;Valinhos;3551603;Serra Negra;82
3556305;Valparaíso;3551603;Serra Negra;532
3556354;Vargem;3551603;Serra Negra;72
3556404;Vargem Grande do Sul;3551603;Serra Negra;130
3556453;Vargem Grande Paulista;3551603;Serra Negra;174
3556503;Várzea Paulista;3551603;Serra Negra;97
3556602;Vera Cruz;3551603;Serra Negra;389
3556701;Vinhedo;3551603;Serra Negra;79
3556800;Viradouro;3551603;Serra Negra;339
3556909;Vista Alegre do Alto;3551603;Serra Negra;318
3556958;Vitória Brasil;3551603;Serra Negra;536
3557006;Votorantim;3551603;Serra Negra;178
3557105;Votuporanga;3551603;Serra Negra;469
3557154;Zacarias;3551603;Serra Negra;471
