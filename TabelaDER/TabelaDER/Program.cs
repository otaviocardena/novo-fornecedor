﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace TestDER
{
    class Program
    {
        // Barretos, Araraquara, São José do Rio Preto, Marília, Campos do Jordão, Serra Negra e Barra Bonita

        public static string CIDADE = "";
        public static string PATH = @"C:\Projetos\Creasp\Src\TabelaDER\Rotas\";

        public static IWebDriver driver;

        static void Main(string[] args)
        {
            if(!Directory.Exists(PATH)) Directory.CreateDirectory(PATH);

            var cidades = File.ReadAllLines("CIDADES.txt");

            foreach(var c in cidades)
            {
                if (Municipio.Dados.FirstOrDefault(x => x.NoMunicipio == c) == null)
                {
                    Console.WriteLine("Cidade não encontrada == " + c);
                    Console.ReadKey();
                    return;
                }

                Console.WriteLine("Iniciando cidade == " + c);

                while (ExecutaCidade(c) == false)
                {
                    Thread.Sleep(5000);
                }
            }

            Console.WriteLine("FIM do processamento");
            Console.ReadKey();
        }

        public static bool ExecutaCidade(string cidade)
        {
            try
            {
                Program.CIDADE = cidade;

                driver = new ChromeDriver();

                driver.Manage().Timeouts().SetPageLoadTimeout(new TimeSpan(0, 2, 0));
                driver.Manage().Timeouts().SetScriptTimeout(new TimeSpan(0, 2, 0));
                driver.Manage().Timeouts().ImplicitlyWait(new TimeSpan(0, 2, 0));

                ProcessaOrigem.Executar();
                ProcessaDestino.Executar();

                driver.Close();

                Console.WriteLine("Cidade Finalizada :: " + cidade);

                return true;
            }
            catch (Exception ex)
            {
                try { driver.Close(); } catch(Exception) { }

                Console.WriteLine("ERRO ==> " + ex.Message);

                return false;
            }
        }
    }
}
