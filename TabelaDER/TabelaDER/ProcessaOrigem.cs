﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace TestDER
{
    public class ProcessaOrigem
    {
        static IWebDriver driver;

        public static void Executar()
        {
            Inicializa();

            var origem = Municipio.Dados.Single(x => x.NoMunicipio == Program.CIDADE);

            var arq = Program.PATH + "DER-" + origem.CdIbge + "-" + origem.NoMunicipio.ToUpper() + "-ORIG.txt";

            if (!File.Exists(arq))
            {
                File.WriteAllText(arq, "# Origem: " + origem.CdIbge + " - " + origem.NoMunicipio + Environment.NewLine + "# -------------------------------------------------------------------" + Environment.NewLine);
            }

            var processados = File.ReadAllLines(arq)
                .Where(x => x.Contains(";"))
                .Select(x => Convert.ToInt32(x.Split(';')[2]));

            foreach (var m in Municipio.Dados
                .Where(x => x.CdIbge != origem.CdIbge)
                .Where(x => !processados.Contains(x.CdIbge))
                .OrderBy(x => x.NoMunicipio))
            {
                var km = BuscaDistancia(m.NoMunicipio);

                Console.WriteLine(origem.NoMunicipio + " -> " + m.NoMunicipio + " = " + km);

                var linha = origem.CdIbge + ";" + origem.NoMunicipio + ";" +
                    m.CdIbge + ";" + m.NoMunicipio + ";" +
                    km + Environment.NewLine;

                File.AppendAllText(arq, linha);
            }

            Console.WriteLine("\nOk");
        }

        private static void Inicializa()
        {
            driver = Program.driver;

            driver.SwitchTo().ParentFrame().Navigate().GoToUrl("http://200.144.30.104/website/webrota/viewer.htm");

            var frame = driver.FindElement(By.Name("TextFrame"));
            driver.SwitchTo().Frame(frame);

            var link = driver.FindElement(By.PartialLinkText("Distância e Rotas"));

            link.Click();

            var cmbOrig = driver.FindElement(By.Name("cidadesor"));

            cmbOrig.SendKeys(Program.CIDADE);
        }

        public static int BuscaDistancia(string destino)
        {
            var dest = driver.FindElement(By.Name("cidadesdes"));

            dest.SendKeys(destino);

            driver.FindElement(By.Name("Submit")).Click();

            string km = null;

            for (var tent = 0; tent <= 5; tent++)
            {
                try
                {
                    var el = driver.FindElement(By.XPath("//table[@class='tabelas']/tbody/tr[last()]"));

                    km = el.Text.Replace("km", "").Trim();

                    if (!string.IsNullOrEmpty(km)) break;
                }
                catch (Exception)
                {
                    Thread.Sleep(1000 * tent);
                }
            }

            if (km == null) throw new Exception("ERRO na leitura de distancia. Tente novamente");

            // voltar
            driver.FindElement(By.CssSelector("a[href='rota.htm']")).Click();

            return Convert.ToInt32(km);
        }
    }
}
