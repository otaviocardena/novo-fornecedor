﻿<%@ Page Title="NERP" Resource="nerp" Language="C#" %>

<script runat="server">

    protected CreaspContext db = new CreaspContext();
    protected int? idFornecedor = null;

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO", "ALL.FORNECEDOR" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        idFornecedor = db.Pessoas.ObtemIdFornecedor(Auth.Current<CreaspUser>().IdPessoa);

        grdEvento.RegisterPagedDataSource((p) => db.Nerps.ListaEvento(p, txtDtEvento.Text.To<DateTime?>(), txtNoEvento.Text.To<string>(), tabAdianta.Selected));

        grdSolicitacao.RegisterDataSource(() => db.Solicitacao.ObtemPorIdPessoa(Auth.Current<CreaspUser>().IdPessoa, txtNumNF.Text, txtEtapa.Text, dtSolicitacao.Periodo));

        grdNerpFornecedor.RegisterPagedDataSource((p) => db.Nerps.ListaNerps(p,
           txtNumF.Text.To<int?>(),
           txtEmpenhoF.Text.To<int?>(),
           emissaoF.Periodo,
           Periodo.Vazio,
           ativoF.Checked ? true : inativoF.Checked ? false : (bool?)null,
           ucUniF.CdUnidade,
           null,
           null,
           null,
           null,
           autf.IdPessoa,
           Auth.Current<CreaspUser>().Fornecedor,
           "",
           Auth.Current<CreaspUser>().Fornecedor,
           idFornecedor));

        // ao voltar, nao abre popups
        RegisterRestorePageState(() =>
        {
            pnlSelEvento.Close();
            pnlMesAno.Close();
            grdNerpFornecedor.DataBind();
        });

    }

    protected override void OnPrepareForm()
    {

        if (Auth.Current<CreaspUser>().Fornecedor)
        {
            tabSolicitacao.Visible = true;
            tabMainFornecedor.Visible = true;
            tabSolicitacao.Selected = true;
        }
        else
        {
            tabSolicitacao.Visible = false;
            tabMainFornecedor.Visible = false;
        }

    }

    protected void drpIncluir_ItemClick(object sender, NDropDownItem e)
    {
        if (e.Key == TpNerp.Evento)
        {
            grdEvento.DataBind();
            pnlSelEvento.Open();
        }
        else if (e.Key == TpNerp.ART)
        {
            cmbMes.SelectedValue = DateTime.Now.Month.ToString();
            cmbAno.SelectedValue = DateTime.Now.Year.ToString();
            btnMesAno.CommandName = "ART";
            pnlMesAno.Open(btnMesAno, cmbMes);
        }
        else if (e.Key == TpNerp.CessaoUso)
        {
            cmbMes.SelectedValue = DateTime.Now.Month.ToString();
            cmbAno.SelectedValue = DateTime.Now.Year.ToString();
            btnMesAno.CommandName = "CESSAO-USO";
            pnlMesAno.Open(btnMesAno, cmbMes);
        }
    }

    protected void grdList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var data = e.Row.DataItem as IFgAtivo;
            if (!data.FgAtivo) e.Row.ForeColor = System.Drawing.Color.Red;
        }
    }

    protected void tabAdianta_TabChanged(object sender, EventArgs e)
    {
        txtDtEvento.Text = "";
        txtNoEvento.Text = "";
        grdEvento.DataBind();
    }

    protected void txtDtEvento_TextChanged(object sender, EventArgs e)
    {
        grdEvento.DataBind();
    }

    protected void btnMesAno_Command(object sender, CommandEventArgs e)
    {
        if (e.CommandName == "ART")
        {
            Redirect("Nerp.ART.aspx?Mes=" + cmbMes.SelectedValue + "&Ano=" + cmbAno.SelectedValue);
        }
        else
        {
            Redirect("Nerp.CessaoUso.aspx?Mes=" + cmbMes.SelectedValue + "&Ano=" + cmbAno.SelectedValue);
        }
    }

    protected void btnPesquisarNerpFornecedor_Click(object sender, EventArgs e)
    {
        grdNerpFornecedor.DataBind();
    }

    protected void btnBuscaSolicitacoes_Click(object sender, EventArgs e)
    {
        grdSolicitacao.DataBind();
    }
</script>
<%--<asp:Content ContentPlaceHolderID="title" runat="server">
    <box:NDropDown runat="server" ID="drpIncluir" Icon="plus" Text="Nova NERP" OnItemClick="drpIncluir_ItemClick" ButtonTheme="Success" Visible="false" />
</asp:Content>--%>
<asp:Content ContentPlaceHolderID="content" runat="server">
    <script> 
        try {
            //Create class in div
            var x = document.querySelector(".n-grid-footer").parentNode;
            x.setAttribute('id', 'table-wrapper');
        }
        catch{

        }

        try {

            //Remove text drop-menu
            var registros = document.querySelector('.n-grid-paging');
            var vet = [];
            let spanSelected = null;

            for (let i = 0; i <= 3; i++) vet.push(registros.getElementsByTagName('a')[i]);

            spanSelected = registros.getElementsByTagName('span')[0];

            for (let i = 0; i < vet.length; i++) {
                if (Number(vet[i].innerText) > Number(spanSelected.innerText)) {
                    vet.splice(i, 0, spanSelected);
                    break;
                } else if (Number(spanSelected.innerText) > Number(vet[vet.length - 1].innerText)) {
                    vet.splice(vet.length, 0, spanSelected);
                    break;
                }
            }

            var p = document.createElement('p');
            p.setAttribute('class', 'm-view');
            p.innerText = "VISUALIZAR";

            vet.splice(0, 0, p);

            registros.innerHTML = ""
            for (let i = 0; i <= 5; i++) registros.appendChild(vet[i])

            //Calc count of pages
            let totalReg = Number(document.querySelector('.n-grid-total').innerText.split(':')[1]);
            let pageCurrent = Number(document.querySelector('.n-grid-pager tr td span').innerText);
            var countTotalPages = Math.round(totalReg / Number(spanSelected.innerText));

            document.querySelector('.n-grid-pager td table').setAttribute("style", "display: none");
            let aBack = () => {
                if (pageCurrent <= 1) {
                    return `<span><i class="fas fa-chevron-left c-arrow-not-selected"></i></span>`;
                }

                return `<a href="javascript: __doPostBack('ctl00$content$grdList', 'Page$${pageCurrent - 1}')"><i class="fas fa-chevron-left select-true"></i></a>`;
            }

            let aNext = () => {

                if (pageCurrent >= countTotalPages) {
                    return `<span><i class="fas fa-chevron-right c-arrow-not-selected"></i></span>`;
                }

                return `<a href="javascript: __doPostBack('ctl00$content$grdList', 'Page$${pageCurrent + 1}')"><i class="fas fa-chevron-right select-true"></i></a>`;
            }


            let divPager = document.createElement('div');
            divPager.setAttribute('class', 'n-paginator');
            divPager.innerHTML = `<span>${pageCurrent} - ${countTotalPages} de ${totalReg}</span>
                                    ${aBack()}
                                    ${aNext()}`;
            let tablePaginator = document.querySelector('.n-grid-pager td');
            tablePaginator.appendChild(divPager);

        }
        catch {

        }


        try {
            //Add icon arrow down
            var gridTotal = document.querySelector('.n-grid-total');

            var i = document.createElement('i');
            i.setAttribute('class', 'fas fa-sort-amount-down-alt ml-8 c-primary-yellow');
            i.innerHTML = '&nbsp;&nbsp;&nbsp;';

            gridTotal.appendChild(i);
        } catch {
        }


    </script>

    <box:NTab runat="server" ID="tabSolicitacao" Text="Solicitações de Pagamento" Selected="true" AutoPostBack="true" Visible="false" CssClass="withoutPaged">

        <%--<box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnpesquisar" icon="search" text="pesquisar" theme="default" onclick="btnpesquisar_click" />
        </box:NToolBar>--%>
        <div class="subTitle-wrapper">
            <span class="subTitle">Solicitações</span>


            <div class="subTitle-new" runat="server" id="divSolicita2">
                <a href="/solicitacao/Solicitacao.Novo.aspx">
                    <div class="flex-center">
                        <i class="far fa-plus-square" style="color: #FABB00; margin-bottom: 10px"></i>
                    </div>
                    <div class="f8 c-black">
                        Nova Solicitação de Pagamento
                    </div>
                </a>
            </div>
        </div>

        <div class="form-div">

            <div class="sub-form-row">
                <div>
                    <div class="margin-bottom"><span>Situação</span></div>
                    <div>
                        <box:NRadioButton runat="server" ID="ativoS" GroupName="fgAtivo" Text="Ativas" Checked="true" />
                        <box:NRadioButton runat="server" ID="inativoS" GroupName="fgAtivo" Text="Inativas" />
                        <box:NRadioButton runat="server" ID="todasS" GroupName="fgAtivo" Text="Todas" />
                    </div>
                </div>
            </div>

            <div class="sub-form-row">
                <div class="div-sub-form">
                    <div><span>Número da Nota Fiscal</span></div>
                    <box:NTextBox runat="server" ID="txtNumNF" />
                </div>

                <div class="div-sub-form">
                    <div><span>Etapa Processo</span></div>
                    <box:NTextBox runat="server" ID="txtEtapa" />
                </div>

                <div class="div-sub-form">
                    <div><span>Data da Solicitação</span></div>
                    <uc:Periodo runat="server" ID="dtSolicitacao" ShowTextBox="true" />
                </div>
            </div>
        </div>

        <div class="sub-form-row">
            <div>
                <box:NButton runat="server" ID="btnBuscaSolicitacoes" Text="pesquisar" Theme="primary" OnClick="btnBuscaSolicitacoes_Click" Width="150" Height="40" CssClass="btn-form-search space-top" />
            </div>

        </div>

        <box:NGridView runat="server" ID="grdSolicitacao" ItemType="Creasp.Core.Model.Solicitacao" PageSize="300">
            <Columns>
                <asp:BoundField DataField="IdSolicitacao" HeaderText="Número" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="80" SortExpression="IdSolicitacao" />
                <asp:BoundField DataField="DtCadastro" HeaderText="Data da Solicitação" DataFormatString="{0:d}" HeaderStyle-Width="110" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" SortExpression="DtCadastro" />
                <asp:BoundField DataField="NumNF" HeaderText="Numero da Nota Fiscal" HeaderStyle-Width="210" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" SortExpression="NumNF" />
                <asp:BoundField DataField="LoginCadastro" HeaderText="Usuário Solicitante" SortExpression="LoginCadastro" HeaderStyle-Width="310" />
                <asp:BoundField DataField="Status" HeaderText="Etapa Processo" SortExpression="Status" HeaderStyle-Width="310" />
                <asp:BoundField DataField="ValorBruto" HeaderText="Valor" SortExpression="ValorBruto" HeaderStyle-Width="310" />
            </Columns>
            <EmptyDataTemplate>Nenhuma Solicitação encontrada no filtro informado</EmptyDataTemplate>
        </box:NGridView>

    </box:NTab>

    <box:NTab runat="server" ID="tabMainFornecedor" Text="Histórico de Pagamentos" Selected="true" AutoPostBack="true" Visible="false" CssClass="withoutPaged">

        <%--<box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnpesquisar" icon="search" text="pesquisar" theme="default" onclick="btnpesquisar_click" />
        </box:NToolBar>--%>
        <div class="subTitle-wrapper">
            <span class="subTitle">NERP</span>
        </div>

        <div class="form-div">

            <div class="sub-form-row">
                <div>
                    <div class="margin-bottom"><span>Situação</span></div>
                    <div>
                        <box:NRadioButton runat="server" ID="ativoF" GroupName="fgAtivo" Text="Ativas" Checked="true" />
                        <box:NRadioButton runat="server" ID="inativoF" GroupName="fgAtivo" Text="Inativas" />
                        <box:NRadioButton runat="server" ID="todasF" GroupName="fgAtivo" Text="Todas" />
                    </div>
                </div>
            </div>

            <div class="sub-form-row">
                <div class="div-sub-form">
                    <div><span>Número da Nota Fiscal</span></div>
                    <box:NTextBox runat="server" ID="txtNumF" MaskType="Integer" />
                </div>

                <div class="div-sub-form">
                    <div><span>Número Empenho</span></div>
                    <box:NTextBox runat="server" ID="txtEmpenhoF" MaskType="Integer" />
                </div>

                <div class="div-sub-form">
                    <div><span>Emissão</span></div>
                    <uc:Periodo runat="server" ID="emissaoF" ShowTextBox="true" />
                </div>
            </div>

            <div class="sub-form-row">
                <div class="div-sub-form">
                    <div><span>Autorizador</span></div>
                    <div class="btn-form-search">
                        <uc:Pessoa runat="server" ID="autf" IsFuncionario="true" />
                    </div>
                </div>

                <div class="div-sub-form">
                    <div><span>Unidade solicitante</span></div>
                    <uc:Unidade runat="server" ID="ucUniF" Width="96%" />
                </div>
            </div>

            <div class="sub-form-row" style="align-items: center">

                <div>
                    <box:NButton runat="server" ID="btnPesquisarNerpFornecedor" Text="pesquisar" Theme="primary" OnClick="btnPesquisarNerpFornecedor_Click" Width="150" Height="40" CssClass="btn-form-search space-top" />
                </div>

                <div style="margin-top: 20px">
                    <span><i class="far fa-check-circle" style="color: #0aa20a" runat="server"></i>= Pagamento Confirmado</span>
                </div>

            </div>
        </div>

        <box:NGridView runat="server" ID="grdNerpFornecedor" ItemType="Creasp.Nerp.NerpResultadoDTO" PageSize="300">
            <Columns>
                <asp:BoundField DataField="NrNerp" HeaderText="Número" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="80" SortExpression="NrNerp" />
                <asp:BoundField DataField="DtEmissao" HeaderText="Emissão" DataFormatString="{0:d}" HeaderStyle-Width="110" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" SortExpression="DtEmissao" />
                <asp:BoundField DataField="DtVencto" HeaderText="Vencimento" DataFormatString="{0:d}" HeaderStyle-Width="110" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" SortExpression="DtVencto" />
                <asp:BoundField DataField="NoNerp" HeaderText="Descrição" SortExpression="NoNerp" />
                <asp:TemplateField HeaderText="Responsável" HeaderStyle-Width="300">
                    <ItemTemplate>
                        <div><%# Item.NoResponsavel %></div>
                        <small><%# NerpSituacao.NoSituacao(Item.TpSituacao) %></small>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Valor" HeaderStyle-Width="120" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <div class="left-right">
                            <span>
                                <i class="far fa-check-circle" style="color: darkorange" runat="server" visible="<%# Item.VrSolic > 0 && Item.VrPagto != Item.VrBruto %>" title='<%# "Solicitado: " +  Item.VrSolic.ToString("c2") + " - Confirmado: " + Item.VrPagto.ToString("c2") %>'></i>
                                <i class="far fa-check-circle" style="color: #0aa20a" runat="server" visible="<%# Item.VrPagto == Item.VrBruto %>" title='<%# "Confirmado: " + Item.VrBruto.ToString("c2") %>'></i>
                            </span>
                            <span><%# Item.VrBruto.ToString("n2") %></span>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>Nenhuma NERP encontrada no filtro informado</EmptyDataTemplate>
        </box:NGridView>


    </box:NTab>

    <box:NPopup runat="server" ID="pnlSelEvento" Title="Selecionar evento" Width="750" Height="550">

        <box:NTab runat="server" ID="tabReemb" Text="Ressarcimentos" Selected="true" TabCssClass="no-tab" AutoPostBack="true" OnTabChanged="tabAdianta_TabChanged"></box:NTab>
        <box:NTab runat="server" ID="tabAdianta" Text="Adiantamentos" AutoPostBack="true" OnTabChanged="tabAdianta_TabChanged"></box:NTab>

        <table class="form">
            <caption>Pesquisa</caption>
            <tr>
                <td>Data do evento</td>
                <td>
                    <box:NTextBox runat="server" ID="txtDtEvento" MaskType="Date" AutoPostBack="true" OnTextChanged="txtDtEvento_TextChanged" />
                </td>
            </tr>
            <tr>
                <td>Descrição</td>
                <td>
                    <box:NTextBox runat="server" ID="txtNoEvento" TextCapitalize="Upper" OnEnter="txtDtEvento_TextChanged" Width="100%" />
                </td>
            </tr>
        </table>

        <box:NGridView runat="server" ID="grdEvento" PageSize="10" AllowCustomPaging="false">
            <Columns>
                <asp:HyperLinkField DataTextField="DtEvento" DataNavigateUrlFields="IdEvento" DataNavigateUrlFormatString="Nerp.Evento.aspx?IdEvento={0}" HeaderText="Data" DataTextFormatString="{0:dd/MM/yyyy HH:mm}" HeaderStyle-Width="160" SortExpression="DtEvento" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="NoEvento" HeaderText="Evento" SortExpression="NoEvento" />
            </Columns>
            <EmptyDataTemplate>Nenhum evento na data informada</EmptyDataTemplate>
        </box:NGridView>

    </box:NPopup>

    <box:NPopup runat="server" ID="pnlMesAno" Title="Selecione o mês/ano">
        <box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnMesAno" Text="Selecionar" Icon="calendar" OnCommand="btnMesAno_Command" />
        </box:NToolBar>
        <table class="form">
            <tr>
                <td>Informe o mês/ano</td>
                <td>
                    <box:NDropDownList runat="server" ID="cmbMes">
                        <asp:ListItem Value="1">Janeiro</asp:ListItem>
                        <asp:ListItem Value="2">Fevereiro</asp:ListItem>
                        <asp:ListItem Value="3">Março</asp:ListItem>
                        <asp:ListItem Value="4">Abril</asp:ListItem>
                        <asp:ListItem Value="5">Maio</asp:ListItem>
                        <asp:ListItem Value="6">Junho</asp:ListItem>
                        <asp:ListItem Value="7">Julho</asp:ListItem>
                        <asp:ListItem Value="8">Agosto</asp:ListItem>
                        <asp:ListItem Value="9">Setembro</asp:ListItem>
                        <asp:ListItem Value="10">Outubro</asp:ListItem>
                        <asp:ListItem Value="11">Novembro</asp:ListItem>
                        <asp:ListItem Value="12">Dezembro</asp:ListItem>
                    </box:NDropDownList>
                    &nbsp;&nbsp;/&nbsp;&nbsp;
                    <box:NDropDownList runat="server" ID="cmbAno">
                        <asp:ListItem Value="2015">2015</asp:ListItem>
                        <asp:ListItem Value="2016">2016</asp:ListItem>
                        <asp:ListItem Value="2017">2017</asp:ListItem>
                        <asp:ListItem Value="2018">2018</asp:ListItem>
                        <asp:ListItem Value="2019">2019</asp:ListItem>
                        <asp:ListItem Value="2020">2020</asp:ListItem>
                        <asp:ListItem Value="2021">2021</asp:ListItem>
                    </box:NDropDownList>
                </td>
            </tr>
        </table>
        <div class="help">Este periodo será informado ao CREANET para buscar todos os convenios de ART/contratos.</div>
    </box:NPopup>

    </div>

</asp:Content>
