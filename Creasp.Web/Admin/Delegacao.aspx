﻿<%@ Page Title="Delegação de Acesso" Resource="?" Language="C#" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO"};
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        grdList.RegisterDataSource(() => db.Query<SubDelegacao>()
                .Include(x => x.Pessoa)
                .Include(x => x.PessoaCedido)
                .WherePeriodo(ucPesqPeriodo.Periodo)
                .Where(ucPesqUsuario.IdPessoa.HasValue, x => x.IdPessoa == ucPesqUsuario.IdPessoa)
                .OrderBy(x => x.Pessoa.NoPessoa).ToList());
    }

    protected override void OnPrepareForm()
    {
        ucPesqPeriodo.Periodo = Periodo.Hoje;
        ucPesqUsuario.LoadPessoa(Auth.Current<CreaspUser>().IdPessoa);
        grdList.DataBind();
    }

    protected void btnPesquisar_Click(object sender, EventArgs e)
    {
        grdList.DataBind();
    }

    protected void lnkIncluir_Click(object sender, EventArgs e)
    {
        ucPessoa.LoadPessoa(Auth.Current<CreaspUser>().IdPessoa);
        ucPessoaCedido.LoadPessoa(null);
        ucPeriodo.Periodo = new Periodo("EsteMes");
        pnlDelegacao.Open(btnSalvar, ucPessoa);
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        db.Pessoas.DelegarAcesso(ucPessoa.IdPessoa.Value, ucPessoaCedido.IdPessoa.Value, ucPeriodo.Periodo);

        ShowInfoBox("Delegação cadastrada com sucesso");

        pnlDelegacao.Close();
        grdList.DataBind();
    }

    protected void grdList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "del")
        {
            var idPessoa = e.DataKey<int>(sender, "IdPessoa");
            var dtIni = e.DataKey<DateTime>(sender, "DtIni");

            db.Pessoas.ExcluirDelegacao(idPessoa, dtIni);

            ShowInfoBox("Operação realizada com sucesso");
            grdList.DataBind();
        }
    }

</script>
<asp:Content runat="server" ContentPlaceHolderID="content">

    <div class="mt-30 c-primary-blue">
        <span>Delegação de Acesso</span>
        <box:NButton runat="server" ID="lnkIncluir" ForeColor="white" Theme="Success" Icon="plus" Text="Nova delegação" OnClick="lnkIncluir_Click"  CssClass="right new ml-10"/>
        <box:NButton runat="server" ID="btnPesquisar" Icon="search" Text="Pesquisar" OnClick="btnPesquisar_Click" CausesValidation="false" CssClass="right" />
    </div>

    <div class="form-div">
        <div class="sub-form-row">
            <div class="div-sub-form form-uc">
                <div><span>Periodo</span></div>
                <uc:Periodo runat="server" ID="ucPesqPeriodo" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form form-uc">
                <div><span>Usuário</span></div>
                <uc:Pessoa runat="server" ID="ucPesqUsuario" IsUsuario="true" />
            </div>
        </div>
    </div>

    <box:NGridView runat="server" ID="grdList" ItemType="Creasp.Nerp.SubDelegacao" DataKeyNames="IdPessoa,DtIni" OnRowCommand="grdList_RowCommand">
        <Columns>
            <asp:BoundField DataField="Pessoa.NoPessoa" HeaderText="Usuário" />
            <asp:BoundField DataField="PessoaCedido.NoPessoa" HeaderText="Delega acesso para" />
            <asp:TemplateField HeaderText="Periodo" HeaderStyle-Width="220" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <%# Item.GetPeriodo().ToString(false, false) %>
                </ItemTemplate>
            </asp:TemplateField>
            <box:NButtonField CommandName="del" Theme="Danger" Icon="cancel" ToolTip="Excluir" OnClientClick="return confirm('Confirma excluir este registro?');" />
        </Columns>
        <EmptyDataTemplate>Nenhuma delegação de acesso para o filtro informado</EmptyDataTemplate>
    </box:NGridView>

    <box:NPopup runat="server" ID="pnlDelegacao" Title="Delegar acesso" Width="650">

        <div class="form-div">
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Usuário</span></div>
                    <uc:Pessoa runat="server" ID="ucPessoa" IsUsuario="true" Required="true" />
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Delegar acesso para</span></div>
                    <uc:Pessoa runat="server" ID="ucPessoaCedido" IsUsuario="true" Required="true" />
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Periodo</span></div>
                    <uc:Periodo runat="server" ID="ucPeriodo" ShowTextBox="true" Required="true" />
                </div>
            </div>
        </div>
        <box:NButton runat="server" ID="btnSalvar" Text="Salvar" Width="200" Icon="ok" Theme="Primary" OnClick="btnSalvar_Click" CssClass="right"/>

    </box:NPopup>

    <script src="../Content/Scripts/sidebar.js"></script>
    <script>selectButton("admin-delegacao"); showReload("admin");</script>

</asp:Content>