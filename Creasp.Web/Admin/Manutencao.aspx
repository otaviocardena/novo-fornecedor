﻿<%@ Page Title="Manutenção" Resource="admin.manutencao" Language="C#" %>

<script runat="server">

    public CreaspContext db = new CreaspContext();
    public int idNerp { get { return ViewState["idNerp"].To<int>(); } set { ViewState["idNerp"] = value; } }
    public int _uco { get { return ViewState["uco"].To<int>(); } set { ViewState["uco"] = value; } }
    public int _ufi { get { return ViewState["ufi"].To<int>(); } set { ViewState["ufi"] = value; } }
    public int _cont { get { return ViewState["cont"].To<int>(); } set { ViewState["cont"] = value; } }
    public int? idNerpItem { get { return ViewState["idNerpItem"].To<int?>(); } set { ViewState["idNerpItem"] = value; } }

    protected override void OnPrepareForm()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO"};
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        _uco = new UnidadeService(db).UCO.CdUnidade;
        _ufi = new UnidadeService(db).UFI.CdUnidade;
        _cont = new UnidadeService(db).CONT.CdUnidade;

        cmbUnidade.Items.Add(new ListItem("UCO", _uco.ToString()));
        cmbUnidade.Items.Add(new ListItem("UFI", _ufi.ToString()));
        cmbUnidade.Items.Add(new ListItem("CONT", _cont.ToString()));
        
        cmbTpSituacao.Bind(NerpSituacao.ListaSituacoes(), true);
    }

    private void btnPesquisar_Click(object sender, EventArgs e)
    {
        txtNrNerp.Validate().Required();

        var _nerp = db.Query<Nerp>()
                    .Include(x => x.PessoaResp)
                    .Include(x => x.UnidadeResp)
                    .Where(x => x.NrAno == db.NrAnoBase)
                    .Where(x => x.NrNerp == txtNrNerp.Text.To<int>()).FirstOrDefault();

        if (_nerp == null) throw new NBoxException("Nerp não encontrada");

        idNerp = _nerp.IdNerp;

        if (_nerp.TpSituacao == NerpSituacao.Finalizada)
        {
            btnReabrir.Visible = true;
        }
        else
        {
            btnAlteraResp.Visible = true;
        }

        lblNoSituacao.Text = NerpSituacao.NoSituacao(_nerp.TpSituacao);
        lblNoSituacao.Theme = _nerp.FgAtivo ? ThemeStyle.None : ThemeStyle.Danger;

        lblNoResponsavel.Text = _nerp.NoResponsavel;

        lblDtEmissao.Text = _nerp.DtEmissao.ToString("d");

        lblNrNerp.Text = _nerp.NrNerp.ToString();

        //grdTotal.DataSource = db.Nerps.ListNerpItem(nerp.IdNerp);
        grdDespesas.DataSource = db.Query<NerpItem>()
                                .Include(x => x.CentroCusto)
                                .Include(x => x.ContaContabil)
                                .Include(x => x.TipoDespesa, JoinType.Left)
                                .Where(x => x.IdNerp == _nerp.IdNerp)
                                .ToList();
        grdDespesas.DataBind();

    }

    private void grdDespesas_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if(e.CommandName == "alt")
        {
            pnlItem.Controls.ClearControls();

            idNerpItem = e.DataKey<int>(sender);

            var nerpItem = db.Query<NerpItem>().Where(x => x.IdNerpItem == idNerpItem).FirstOrDefault();

            ucCentroCustoAtual.LoadCentroCusto(nerpItem.IdCentroCusto);

            pnlItem.Open(btnSalvar, CentroCustoNovo);

        }
    }

    private void btnSalvar_Click(object sender, EventArgs e)
    {
        var nerpItem = db.Query<NerpItem>().Where(x => x.IdNerpItem == idNerpItem).FirstOrDefault();

        db.Nerps.AlteraNerpItem(nerpItem.IdNerpItem, CentroCustoNovo.IdCentroCusto.Value, nerpItem.QtItem, nerpItem.VrUnit);

        string hist = "Centro de custo alterado " + ucCentroCustoAtual.CentroCusto.NrCentroCustoFmt + " >> " + CentroCustoNovo.CentroCusto.NrCentroCustoFmt;

        new AuditLog().Table("Nerp", nerpItem.IdNerp, hist);

        ShowInfoBox("Centro de custo alterado com sucesso");

        pnlItem.Close();
        limpa();
    }

    private void limpa()
    {
        txtNrNerp.Text = "";
        lblNrNerp.Text = "";
        lblDtEmissao.Text = "";
        lblNoResponsavel.Text = "";
        lblNoSituacao.Text = "";

        btnAlteraResp.Visible = false;
        btnReabrir.Visible = false;

        btnRespSalvar.Visible = false;
        btnReabrirSalvar.Visible = false;

        grdDespesas.DataBind();
    }

    private void btnAlteraResp_Click(object sender, EventArgs e)
    {
        btnRespSalvar.Visible = true;
        pnlResp.Open(btnRespSalvar);


    }

    private void btnRespSalvar_Click(object sender, EventArgs e)
    {
        db.Nerps.AlteraNerpResponsavel(idNerp, cmbTpSituacao.SelectedValue, cmbUnidade.SelectedValue.To<int>());

        ShowInfoBox("Responsável alterado com sucesso");

        pnlResp.Close();
        limpa();
    }    
    
    private void btnReabrirSalvar_Click(object sender, EventArgs e)
    {
        db.Nerps.ReabrirNerp(idNerp, cmbTpSituacao.SelectedValue, cmbUnidade.SelectedValue.To<int>());

        ShowInfoBox("Nerp alterada com sucesso");

        pnlResp.Close();
        limpa();
    }

    private void btnReabrirResp_Click(object sender, EventArgs e)
    {
        btnReabrirSalvar.Visible = true;
        pnlResp.Open(btnReabrirSalvar);

    }

</script>


<asp:Content ContentPlaceHolderID="content" runat="server">

    <div class="mt-30 c-primary-blue">
        <span>Consulta NERP</span>
    </div>

    <div class="form-div">
        <div class="sub-form-row">
            <div class="div-sub-form form-uc">
                <div><span>Número</span></div>
                <div class="input-group">
                    <box:NTextBox runat="server" ID="txtNrNerp" MaskType="Integer" Width="130" />
                    <box:NButton runat="server" ID="NButton1" Icon="search" Text="" Theme="Default" OnClick="btnPesquisar_Click" />
                </div>
            </div>
        </div>
    </div>
    
    <box:NToolBar runat="server">
        <box:NButton runat="server" Visible="false" ID="btnAlteraResp" Icon="pencil" Theme="Primary" Text="Alterar responsável" OnClick="btnAlteraResp_Click" ToolTip="Alterar responsável da NERP"/>
        <box:NButton runat="server" Visible="false" ID="btnReabrir" Icon="pencil" Theme="Primary" Text="Reabrir" OnClick="btnReabrirResp_Click" ToolTip="Reabrir NERP"/>
    </box:NToolBar> 

    <div class="mt-30 c-primary-blue">
        <span>NERP</span>
    </div>

    <div class="form-div">
        <div class="sub-form-row">
            <div class="div-sub-form form-uc">
                <div><span>Número</span></div>
                <div class="input-group">
                    <box:NLabel runat="server" ID="lblNrNerp" CssClass="field"/>
                </div>
            </div>
            <div class="div-sub-form form-uc margin-left">
                <div><span>Emissão</span></div>
                <div class="input-group">
                    <box:NLabel runat="server" ID="lblDtEmissao" CssClass="field" />
                </div>
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form form-uc">
                <div><span>Situação</span></div>
                <div class="input-group">
                    <box:NLabel runat="server" ID="lblNoSituacao" Font-Bold="true" CssClass="field" />
                </div>
            </div>
            <div class="div-sub-form form-uc margin-left">
                <div><span>Responsável</span></div>
                <div class="input-group">
                    <box:NLabel runat="server" ID="lblNoResponsavel" CssClass="field" />
                </div>
            </div>
        </div>
    </div>

    <box:NGridView runat="server" ID="grdDespesas" ItemType="Creasp.NerpItem" DataKeyNames="IdNerpItem" ShowTotal="false" OnRowCommand="grdDespesas_RowCommand">
        <Columns>
            <asp:BoundField DataField="ContaContabil.NoContaContabilFmt" HeaderText="Despesas" />
            <asp:BoundField DataField="CentroCusto.NrCentroCustoFmt" HeaderText="Centro Custo" HtmlEncode="false" HeaderStyle-Width="150" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="VrTotal" DataFormatString="{0:n2}" HeaderText="Valor em R$" HeaderStyle-Width="130" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
            <box:NButtonField Icon="pencil" Theme="Info" ToolTip="Editar" CommandName="alt" /></Columns>
    </box:NGridView>

    <box:NPopup runat="server" ID="pnlItem" Title="Alterar Centro de Custo" Width="750">

        <div class="form-div">
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Centro de custo atual</span></div>
                    <uc:CentroCusto runat="server" ID="ucCentroCustoAtual" Enabled="false"/>
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Centro de custo novo</span></div>
                    <uc:CentroCusto runat="server" ID="CentroCustoNovo" Required="true"/>
                </div>
            </div>
        </div>

        <box:NButton runat="server" ID="btnSalvar" Text="Salvar" Width="150" Icon="ok" Theme="Primary" OnClick="btnSalvar_Click" CssClass="right" />

    </box:NPopup>

    <box:NPopup runat="server" ID="pnlResp" Title="Alterar responsável" Width="750">

        <div class="form-div">
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Situação</span></div>
                    <box:NDropDownList runat="server" ID="cmbTpSituacao" Width="100%" />
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Responsável</span></div>
                    <box:NDropDownList runat="server" ID="cmbUnidade" Width="100%"/>
                </div>
            </div>
        </div>

        <box:NButton runat="server" visible="false" ID="btnRespSalvar" Text="Salvar" Width="150" Icon="ok" Theme="Primary" OnClick="btnRespSalvar_Click" CssClass="right" />
        <box:NButton runat="server" Visible="false" ID="btnReabrirSalvar" Text="Reabrir" Width="150" Icon="ok" Theme="Primary" OnClick="btnReabrirSalvar_Click" CssClass="right" />

    </box:NPopup>

    <script src="../Content/Scripts/pagination.js"></script>
    <script>paginate()</script>

    <script src="../Content/Scripts/sidebar.js"></script>
    <script>selectButton("admin-manutencao"); showReload("admin");</script>

</asp:Content>