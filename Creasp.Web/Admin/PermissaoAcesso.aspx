﻿<%@ Page Title="Permissões de Acesso" Resource="admin.permissao" Language="C#" %>
<script runat="server">

    private CreaspContext db = new CreaspContext();

    protected override void OnPrepareForm()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO"};
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        cmbCdGrupo.Bind(db.Query<GrupoAcesso>().OrderBy(x => x.CdGrupo).Distinct(x => x.CdGrupo).Select(x => new ListItem(x, x)));

        grdPermissao.DataSource = Recurso.Recursos;
        grdPermissao.DataBind();
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        db.GrupoAcesso.Salvar(cmbCdGrupo.SelectedValue, grdPermissao.SelectedDataKeys.Select(x => x.Value.ToString()));
        ShowInfoBox("Permissões do grupo gravadas com sucesso");
    }

    protected void cmbCdGrupo_SelectedIndexChanged(object sender, EventArgs e)
    {
        grdPermissao.SelectedDataKeys =
            db.Query<GrupoAcesso>()
            .Where(x => x.CdGrupo == cmbCdGrupo.SelectedValue)
            .ToEnumerable()
            .Select(x => new DataKey(new OrderedDictionary() { { "CdRecurso", x.CdRecurso } }));
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        txtCdGrupo.Text = "";
        pnlAdd.Open(btnAddGrupo, txtCdGrupo);
    }

    protected void btnAddGrupo_Click(object sender, EventArgs e)
    {
        cmbCdGrupo.Items.Add(new ListItem { Value = txtCdGrupo.Text, Text = txtCdGrupo.Text });
        cmbCdGrupo.SelectedValue = txtCdGrupo.Text;
        grdPermissao.ClearSelection();
        pnlAdd.Close();
    }

</script>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <div class="mt-30 c-primary-blue">
        <span>Permissão de Acesso</span>
        <box:NButton runat="server" ID="btnSalvar" Text="Salvar" Theme="Primary" Icon="ok" OnClick="btnSalvar_Click" CssClass="right"/>
    </div>

    <div class="form-div">
        <div class="sub-form-row form-uc">
            <div class="div-sub-form form-uc">
                <div><span>Grupo de acesso</span></div>
                <div class="input-group">
                    <box:NDropdownList runat="server" ID="cmbCdGrupo" CssClass="no-radius-right" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="cmbCdGrupo_SelectedIndexChanged" Required="true"/>
                    <box:NButton runat="server" ID="btnAdd" Icon="plus" ToolTip="Novo grupo" Theme="Info" OnClick="btnAdd_Click" />
                </div>
                <div class="help">Para excluir um grupo basta remover todas as permissões</div>
            </div>
        </div>
    </div>

    <box:NGridView runat="server" ID="grdPermissao" DataKeyNames="CdRecurso" ItemType="Creasp.Core.Recurso" AllowGrouping="true">
        <Columns>
            <asp:TemplateField>
                <ItemTemplate><%# Item.CdRecurso.Split('.')[0].ToUpper() %></ItemTemplate>
            </asp:TemplateField>
            <box:NCheckBoxField />
            <asp:BoundField DataField="CdRecurso" HeaderText="ID" HeaderStyle-Width="200" />
            <asp:BoundField DataField="NoRecurso" HeaderText="Descrição" HtmlEncode="false" />
        </Columns>
    </box:NGridView>

    <box:NPopup runat="server" ID="pnlAdd" Title="Novo grupo">

        <div class="form-div">
            <div class="sub-form-row">
                <div class="div-sub-form" style="width:100%">
                    <div><span>Novo grupo</span></div>
                    <div class="input-group">
                        <box:NTextBox runat="server" ID="txtCdGrupo" TextCapitalize="Upper" MaxLength="40" Width="100%" Required="true" ValidationGroup="add" />
                    </div>
                </div>
                <div class="div-sub-form mt-25 margin-left" style="width:150px">
                    <box:NButton runat="server" ID="btnAddGrupo" Text="Adicionar" Icon="plus" Theme="Primary" OnClick="btnAddGrupo_Click" ValidationGroup="add" />
                </div>
            </div>
        </div>
    </box:NPopup>

    <script src="../Content/Scripts/sidebar.js"></script>
    <script>selectButton("admin-permissao"); showReload("admin");</script>

</asp:Content>
