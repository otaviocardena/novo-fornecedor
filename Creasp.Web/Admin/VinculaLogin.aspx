﻿<%@ Page Title="Vincular usuário" Language="C#" %>
<script runat="server">

    private CreaspContext db = new CreaspContext();

    protected override void OnPrepareForm()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO"};
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        SetDefaultButton(btnSalvar);
        txtNrCpf.Focus();
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        db.Pessoas.VinculaLogin(txtNrCpf.Text.UnMask(), txtNrMatricula.Text.To<int>(), txtNoPessoa.Text,  txtDtNasc.Text.To<DateTime>());

        ShowInfoBox("Seu usuário foi vinculado com sucesso. Favor efetuar novo login");
        Auth.Logout();

        ClosePopup("OK");
    }

</script>
<asp:Content runat="server" ContentPlaceHolderID="content">

    <box:NToolBar runat="server">
        <box:NButton runat="server" ID="btnSalvar" Icon="ok" Text="Vincular" Theme="Primary" OnClick="btnSalvar_Click" />
    </box:NToolBar>

    <table class="form">
        <caption>Preencha seus dados pessoais</caption>
        <tr>
            <td>CPF</td>
            <td>
                <box:NTextBox runat="server" ID="txtNrCpf" MaskType="Custom" MaskFormat="999.999.999-99" Required="true" Width="160" />
            </td>
        </tr>
        <tr>
            <td>Matricula</td>
            <td>
                <box:NTextBox runat="server" ID="txtNrMatricula" MaskType="Integer" Required="true" Width="160" />
            </td>
        </tr>
        <tr>
            <td>Nome completo</td>
            <td>
                <box:NTextBox runat="server" ID="txtNoPessoa" TextCapitalize="Upper" Required="true" Width="100%" />
            </td>
        </tr>
        <tr>
            <td>Data de nascimento</td>
            <td>
                <box:NTextBox runat="server" ID="txtDtNasc" MaskType="Date" Required="true" Width="120" />
            </td>
        </tr>
    </table>

    <div class="help">Para continuar no sistema, preencha os dados acima para a validação do seu usuário. Esta operação ocorre apenas no seu primeiro login.</div>

</asp:Content>