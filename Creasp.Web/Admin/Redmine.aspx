﻿<%@ Page Title="Suporte" Resource="?" Language="C#" %>
<script runat="server">

    private const string PROJECT = "p16";
    private const string HOST = "http://redmine.numeria.com.br/redmine";
    private const string ADMIN_TOKEN = "09df26bf56189886e338b7732083351f2ead891d";
    private const string NOT_ADMIN_TOKEN = "7d52e5b3fd0147884d6e7c1a94abf59242c0e3ec";
    private const string ASSIGNED_TO = "david";
    private const string DEFAULT_USER = "pedro.martins";

    private int? _issueId = null;

    //protected override void OnRegister()
    //{
    //    _issueId = Request.QueryString["IssueId"].To<int?>();

    //    grdIssues.RegisterPagedDataSource((p) =>
    //    {
    //        var mgr = GetManager();

    //        var result = new List<Issue>();

    //        result.AddRange(this.GetIssueList(mgr, "Suporte"));
    //        result.AddRange(this.GetIssueList(mgr, "Alteração"));
    //        result.AddRange(this.GetIssueList(mgr, "Nova funcionalidade"));
    //        result.AddRange(this.GetIssueList(mgr, "Bug de Produção"));

    //        return result.OrderBy(x => x.Project.Name).ThenBy(x => x.Id).ToPage(p);
    //    });
    //}

    //private List<Issue> GetIssueList(RedmineManager mgr, string trackName)
    //{
    //    var trackId = GetCachedObjects<Tracker>().FirstOrDefault(x => x.Name.StartsWith(trackName, StringComparison.OrdinalIgnoreCase)).Id.ToString();

    //    var parameters = new NameValueCollection();

    //    // parameters.Add(RedmineKeys.LIMIT, "100");
    //    parameters.Add(RedmineKeys.PROJECT_ID, PROJECT);
    //    parameters.Add(RedmineKeys.TRACKER_ID, trackId);
    //    parameters.Add(RedmineKeys.STATUS_ID, tabStatusOpen.Selected ? "open" : "closed");

    //    return mgr.GetObjects<Issue>(parameters);
    //}

    //protected override void OnPrepareForm()
    //{
    //    tabStatusOpen.Visible = tabStatusClose.Visible = grdIssues.Visible = btnNewIssuePopup.Visible = (_issueId == null);
    //    pnlDetail.Visible = (_issueId != null);

    //    if (_issueId == null)
    //    {
    //        grdIssues.DataBind();
    //    }
    //    else
    //    {
    //        this.BackTo("Redmine.aspx");
    //        LoadIssue();
    //    }
    //}

    //protected void tabStatus_TabChanged(object sender, EventArgs e)
    //{
    //    grdIssues.DataBind();
    //}

    //protected void grdList_RowDataBound(object sender, GridViewRowEventArgs e)
    //{
    //    if (e.Row.RowType == DataControlRowType.DataRow)
    //    {
    //        var issue = e.Row.DataItem as Issue;

    //        if (issue.Status.Name == "Pendente com o Cliente")
    //        {
    //            e.Row.Cells[2].ForeColor = e.Row.Cells[3].ForeColor = e.Row.Cells[4].ForeColor = System.Drawing.Color.Red;
    //        }
    //    }
    //}

    //protected void btnNewIssuePopup_Click(object sender, EventArgs e)
    //{
    //    if(cmbProject.Items.Count == 0)
    //    {
    //        var mgr = GetManager();

    //        var projects = GetCachedObjects<Project>()
    //            .Where(x => x.Identifier == "p05" || x.Identifier == "p06" || x.Identifier == "p07")
    //            .Select(x => new ListItem { Text = x.Name, Value = x.Id.ToString() });

    //        var priorities = GetCachedObjects<IssuePriority>()
    //            .Select(x => new ListItem { Text = x.Name, Value = x.Id.ToString() });

    //        cmbProject.Bind(projects);
    //        cmbPriority.Bind(priorities, false);
    //        cmbPriority.SelectedValue = "2"; // Normal
    //    }

    //    pnlNewIssue.Open(btnNewIssue, cmbProject);
    //}

    //protected void btnNewIssue_Click(object sender, EventArgs e)
    //{
    //    var mgr = GetManager(true);
    //    var issue = new Issue();

    //    issue.Project = new IdentifiableName() { Id = cmbProject.SelectedValue.To<int>() };
    //    issue.Priority = new IdentifiableName() { Id = cmbPriority.SelectedValue.To<int>() };
    //    issue.Subject = txtSubject.Text.Trim();
    //    issue.Description = txtDescription.Text.Trim();
    //    issue.Tracker = new IdentifiableName() { Id = GetCachedObjects<Tracker>().FirstOrDefault(x => x.Name.StartsWith("Suporte")).Id };

    //    // não funciona
    //    //issue.AssignedTo = new IdentifiableName() { Id = GetCachedObjects<User>().FirstOrDefault(x => x.Login == ASSIGNED_TO).Id };

    //    try
    //    {
    //        AddFiles(mgr, issue, uplFiles);

    //        _issueId = mgr.CreateObject(issue).Id;

    //        pnlNewIssue.Close();

    //        ShowInfoBox("Chamado criado com sucesso: #" + _issueId);
    //    }
    //    catch(Exception ex)
    //    {
    //        ShowErrorBox(ex.Message);
    //    }

    //    //TODO: Por algum motivo tá dando erro no redirect (e funciona)
    //    try
    //    {
    //        Redirect("Redmine.aspx?IssueId=" + _issueId);
    //    }
    //    catch(Exception)
    //    {
    //    }
    //}

    //private void LoadIssue()
    //{
    //    var mgr = GetManager();
    //    try
    //    {
    //        mgr = new RedmineManager(HOST, NOT_ADMIN_TOKEN); //tenta mostrar journals como usuário Jussara Ralisse (de fora da numéria), para ocultar notas privadas
    //    }
    //    catch (Exception)
    //    {
    //    }

    //    var parameters = new NameValueCollection { { RedmineKeys.INCLUDE, RedmineKeys.ATTACHMENTS + "," + RedmineKeys.JOURNALS } };

    //    var issue = mgr.GetObject<Issue>(_issueId.ToString(), parameters);

    //    if (issue == null || !issue.Project.Name.ToUpper().Contains("CREA-SP")) throw new NBoxException("Issue não encontrada");

    //    PageTitle = "#" + _issueId + " - " + issue.Subject;

    //    lblStatus.Text = issue.Status.Name;
    //    ltrTracker.Text = issue.Tracker.Name;
    //    ltrDescription.Text = issue.Description;
    //    ltrCreatedOn.Text = string.Format("{0:d} - {1}", issue.CreatedOn, issue.Author.Name);
    //    ltrUpdatedOn.Text = string.Format("{0:dd/MM/yyyy HH:mm}", issue.UpdatedOn);
    //    lblPriority.Text = issue.Priority.Name;
    //    lblPriority.Theme = issue.Priority.Name == "Urgente" ? ThemeStyle.Danger : ThemeStyle.Default;
    //    rptFiles.DataSource = issue.Attachments;
    //    rptFiles.DataBind();
    //    trFiles.Visible = issue.Attachments?.Count > 0;

    //    var status = GetCachedObjects<IssueStatus>().FirstOrDefault(x => x.Id == issue.Status.Id);

    //    if (status.IsClosed)
    //    {
    //        lblStatus.Theme = ThemeStyle.Danger;
    //        btnCloseIssue.Visible = false;
    //        btnNewInteraction.Text = "Reabrir chamado";
    //        btnNewInteraction.CommandArgument = "true";
    //    }
    //    else if(status.Name == "Pendente com o Cliente")
    //    {
    //        lblStatus.Theme = ThemeStyle.Warning;
    //    }

    //    rptHist.DataSource = issue.Journals;
    //    lblCounter.InnerText = "#" + ((issue.Journals?.Count ?? 0) + 1);
    //    rptHist.DataBind();
    //}

    //protected void lnkDownload_Command(object sender, CommandEventArgs e)
    //{
    //    var mgr = GetManager();
    //    var file = e.CommandArgument.ToString();
    //    var bytes = mgr.DownloadFile(file);
    //    this.Download(Path.GetFileName(file), bytes, true);
    //}

    //protected void btnNewInteraction_Command(object sender, CommandEventArgs e)
    //{
    //    txtNewDescription.Validate(e.CommandArgument.To<bool>()).Required();

    //    var mgr = GetManager(true);

    //    var issue = mgr.GetObject<Issue>(_issueId.ToString(), new NameValueCollection() { { RedmineKeys.INCLUDE, RedmineKeys.JOURNALS } });
    //    issue.Notes = txtNewDescription.Text.Trim();
    //    issue.Status = GetCachedObjects<IssueStatus>().Where(x => x.Name == e.CommandName).FirstOrDefault();
    //    issue.AssignedTo = this.GetLastAssignedTo(issue);

    //    AddFiles(mgr, issue, uplNewFiles);

    //    mgr.UpdateObject(_issueId.ToString(), issue);

    //    ShowInfoBox("Nova interação efetuada com sucesso");

    //    Redirect("Redmine.aspx?IssueId=" + _issueId);
    //}

    //private IdentifiableName GetLastAssignedTo(Issue issue)
    //{
    //    var userNumeria = this.UsersNumeria().Select(x => x.Id).ToList();

    //    var lastUser = issue.Journals
    //        .SelectMany(x => x.Details)
    //        .Where(x => x.Property == "attr" && x.Name == "assigned_to_id")
    //        .Where(x => userNumeria.Contains(Convert.ToInt32(x.NewValue)))
    //        .Select(x => this.UsersNumeria().FirstOrDefault(z => z.Id == Convert.ToInt32(x.NewValue)))
    //        .LastOrDefault() ??
    //        this.UsersNumeria().FirstOrDefault(x => x.Login == DEFAULT_USER);

    //    return new IdentifiableName() { Id = lastUser.Id, Name = lastUser.FirstName + " " + lastUser.LastName };
    //}

    //private List<User> UsersNumeria()
    //{
    //    return GetCachedObjects<User>()
    //        .Where(x => x.Email.Contains("@numeria"))
    //        .ToList();
    //}

    //private string GetDetail(Detail detail)
    //{
    //    var o = detail.OldValue;
    //    var n = detail.NewValue;

    //    Func<string, IssueStatus> getStatus = (s) => GetCachedObjects<IssueStatus>().FirstOrDefault(x => x.Id == s.To<int>());
    //    Func<string, User> getUser = (s) => GetCachedObjects<User>().FirstOrDefault(x => x.Id == s.To<int>());
    //    Func<string, IssuePriority> getPriority = (s) => GetCachedObjects<IssuePriority>().FirstOrDefault(x => x.Id == s.To<int>());

    //    switch(detail.Name)
    //    {
    //        case "status_id":
    //            return $"Alterou a situação de <b>{getStatus(o)?.Name}</b> para <b>{getStatus(n)?.Name}</b>";
    //        case "assigned_to_id":
    //            return $"Encaminhou para <b>{getUser(n)?.FirstName} {getUser(n)?.LastName}</b>";
    //        case "priority_id":
    //            return $"Alterou a prioridade de <b>{getPriority(o)?.Name}</b> para <b>{getPriority(n)?.Name}</b>";
    //        case "done_ratio":
    //            return $"Alterou o percentual de execução de <b>{o}%</b> para <b>{n}%</b>";
    //        case "subject":
    //            return $"Alterou o título de <b>{o}</b> para <b>{n}</b>";
    //        case "description":
    //            return $"Alterou a descrição de <b>{o}</b> para <b>{n}</b>";
    //    }

    //    if (detail.Property == "attachment")
    //        return detail.OldValue == null ? $"Adicionou o arquivo {detail.NewValue}" : $"Exclui o arquivo {detail.OldValue}";

    //    return $"{detail.Name} [{detail.Property}]: {o} => {n}";
    //}

    //private List<T> GetCachedObjects<T>() where T : class, new()
    //{
    //    var key = typeof(T).Name;
    //    var values = Cache.Get(key) as List<T>;

    //    if (values == null)
    //    {
    //        var mgr = GetManager();

    //        values = mgr.GetObjects<T>(new NameValueCollection()) as List<T>;

    //        Cache.Insert(key, values, null, Cache.NoAbsoluteExpiration, new TimeSpan(0, 10, 0));
    //    }

    //    return values;
    //}

    //private void AddFiles(RedmineManager mgr, Issue issue, NFileUpload control)
    //{
    //    foreach(var file in control.PostedFiles)
    //    {
    //        if (file.ContentLength == 0) continue;

    //        var mem = new MemoryStream();
    //        file.InputStream.CopyTo(mem);

    //        var upload = mgr.UploadFile(mem.ToArray());

    //        upload.FileName = file.FileName;
    //        upload.ContentType = file.ContentType;

    //        if (issue.Uploads == null) issue.Uploads = new List<Upload>();

    //        issue.Uploads.Add(upload);
    //    }
    //}

    //private RedmineManager GetManager(bool useUserToken = false)
    //{
    //    if (useUserToken)
    //    {
    //        var pessoas = new CreaspContext().Pessoas;
    //        var pessoa = pessoas.BuscaPessoa(Auth.Current<CreaspUser>().IdPessoa, false);

    //        var token = pessoa.CdTokenRedmine ?? "";

    //        if(token.Length != 40)
    //        {
    //            throw new NBoxException("Seu usuário não permissão para criar/alterar chamados.");
    //        }

    //        return new RedmineManager(HOST, token);
    //    }

    //    return new RedmineManager(HOST, ADMIN_TOKEN);
    //}

</script>
<asp:Content ContentPlaceHolderID="title" runat="server">
    <div>
        hi
    </div>
</asp:Content>