﻿<%@ Page Title="Integrações" Resource="admin.integracao" Language="C#" %>

<script runat="server">

    private CreaspContext db = new CreaspContext();
    public ISenior senior = Factory.Get<ISenior>();

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO"};
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        grdList.PageSize = 10;
        grdList.RegisterPagedDataSource((p) => db.Ceps.ListaCeps(p,
            txtPesquisaCep.Text.UnMask(),
            txtCidadePesquisaCep.Text,
            ddlCdUfPesquisaCep.SelectedValue));
    }

    protected override void OnPrepareForm()
    {
        RunScript("ping", "executaPingServidores();");
        SetDefaultButton(btnConsultaSenior);
        ddlUF.Bind(db.Ceps.ListaUFs().Select(x => new ListItem(x, x)), false, true);
        ddlCdUfPesquisaCep.Bind(db.Ceps.ListaUFs().Select(x => new ListItem(x, x)), true, true);
    }

    protected void btnSync_Click(object sender, EventArgs e)
    {
        var sb = new StringBuilder();

        db.Sync.Run(sb);

        trLogSync.Visible = true;
        tdLogSync.InnerText = sb.Length == 0 ? "Nenhuma atualização necessária" : sb.ToString();

        ShowInfoBox("Dados sincronizados com sucesso");
    }

    protected void btnImpCep_Click(object sender, EventArgs e)
    {
        if (!uplFile.HasFile) throw new NBoxException(uplFile, "Selecione um arquivo no formato .CSV");

        db.Ceps.ImportaDadosCep(uplFile.FileContent);

        ShowInfoBox("Arquivo importado com sucesso");
    }

    protected void btnSyncOrcSiscont_Click(object sender, EventArgs e)
    {
        StringBuilder sb = new StringBuilder();

        var lstErros = db.Sync.RunOrcamentoSiscont(sb);
        if (lstErros.Count == 0)
        {
            ShowInfoBox("Dados do orçamento (Siscont) sincronizados com sucesso");
        }
        else
        {
            ShowErrorBox("Dados do orçamento não sincronizados. Acesse a página 'Mapear Plano de Contas' para mapear as contas contábeis e sincronize novamente.");
            txtContasErros.Text = string.Join(", ", lstErros.OrderBy(x => x));
            pnlErros.Open();
        }
    }

    protected void btnSyncOrcSenior_Click(object sender, EventArgs e)
    {
        StringBuilder sb = new StringBuilder();

        var lstErros = db.Sync.RunOrcamentoSenior(sb);
        if (lstErros.Count == 0)
        {
            ShowInfoBox("Dados do orçamento (Senior) sincronizados com sucesso");
        }
        else
        {
            ShowErrorBox("Dados do orçamento não sincronizados. Acesse a página 'Mapear Plano de Contas' para mapear as contas contábeis e sincronize novamente.");
            txtContasErros.Text = string.Join(", ", lstErros.OrderBy(x => x));
            pnlErros.Open();
        }
    }

    protected void btnSyncProf_Click(object sender, EventArgs e)
    {
        btnSyncProf.Enabled = btnSync.Enabled = false;
        RunScript("sync", "executaSyncProfissional(0);");
    }

    //protected void btnExpDoc_Click(object sender, EventArgs e)
    //{
    //    btnExpDoc.Enabled = btnSync.Enabled = false;
    //    RunScript("sync", "ExecutaExportacaoArquivos(0,0);");
    //}

    [System.Web.Services.WebMethod]
    public static bool ExecutaSyncProfissional(int skip)
    {
        var db = new CreaspContext();

        var pessoa = db.Query<Pessoa>()
            .Where(x => x.NrCreasp != null)
            .OrderBy(x => x.IdPessoa)
            .Limit(skip, 1)
            .FirstOrDefault();

        if (pessoa == null) return false;

        db.Pessoas.AtualizaCadastro(pessoa);

        return true;
    }

    [WebMethod]
    public static bool ExecutaExportacaoArquivos(int skip)
    {
        var db = new CreaspContext();

        var doc = db.Query<DocumentoMigracao>()
            .Where(x => x.FgImportado == false)
            .OrderBy(x => x.IdDocumentoMigracao)
            .Limit(skip, 1)
            .FirstOrDefault();

        if (doc == null) return false;

        db.Documentos.MigrarDocumento(doc);

        return true;
    }


    [System.Web.Services.WebMethod]
    public static long Ping(string servico)
    {
        if (servico == "creanet") return Factory.Get<ICreanet>().Ping();
        if (servico == "siscont") return Factory.Get<ISiscont>().Ping();
        if (servico == "senior") return Factory.Get<ISenior>().Ping();

        return 0;
    }

    protected void btnConsultaSenior_Click(object sender, EventArgs e)
    {
        txtNrMatricula.Validate().Required();

        var mat = txtNrMatricula.Text.Length < 11 ? txtNrMatricula.Text.To<int?>() : null;
        var cpf = txtNrMatricula.Text.Length >= 11 ? txtNrMatricula.Text.UnMask() : null;

        var f = senior.BuscaFuncionarios(mat, cpf, null).FirstOrDefault();

        if (f == null) throw new NBoxException("Funcionário não encontrado na Senior RH");

        ltrNoFunc.Text = $"{f.NrMatricula} - {f.NrCpf.Mask("999.999.999-99")} - {f.NoFuncionario} <small>({f.NoCargo})</small>";
        ltrNoCentroCusto.Text = new CentroCusto { NrCentroCusto = f.NrCentroCusto }.NrCentroCustoFmt;
        ltrChefe.Text = $"{f.NrCpfChefe.Mask("999.999.999-99")} - {f.NoChefe}";
        // ltrGerente.Text = $"{f.NrCpfGerente.Mask("999.999.999-99")} - {f.NoGerente}";
        ltrSuperintendente.Text = $"{f.NrCpfSuperintendente.Mask("999.999.999-99")} - {f.NoSuperintendente}";
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        pnlErros.Close();
    }

    protected void btnImpFunc_Click(object sender, EventArgs e)
    {
        if (!uplFileFunc.HasFile) throw new NBoxException(uplFileFunc, "Selecione um arquivo no formato .CSV");

        var erros = new StringBuilder();

        db.Pessoas.ImportaDadosFunc(uplFileFunc.FileContent, erros);

        ShowInfoBox("Arquivo importado com sucesso");

        lblErroImpFunc.Text = erros.ToString();
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        HttpRuntime.UnloadAppDomain();

        ShowInfoBox("HttpRuntime.UnloadAppDomain");
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        db.Ceps.IncluiCepManual(txtNrCep.Text.UnMask(), txtNoLogradouro.Text, txtNoBairro.Text, ucCidade.NoCidade, ddlUF.SelectedValue, ucCidade.CdCidade);
        pnlCep.Close();
        ShowInfoBox("Cep incluído com sucesso!");
    }

    protected void btnIncCep_Click(object sender, EventArgs e)
    {
        pnlCep.Controls.ClearControls();
        pnlCep.Title = "Novo Cep";
        ddlUF.SelectedValue = "SP";
        btnSalvar.Visible = true;
        btnEditar.Visible = !btnSalvar.Visible;
        txtNrCep.Enabled = btnSalvar.Visible;
        ucCidade.Enabled = btnSalvar.Visible;
        pnlCep.Open(btnSalvar, txtNrCep);
    }

    protected void btnPesqCep_Click(object sender, EventArgs e)
    {
        pnlBuscaCep.Controls.ClearControls();
        grdList.Clear();
        pnlBuscaCep.Open(btnPesquisarCep, txtCidadePesquisaCep);
    }

    protected void btnPesquisarCep_Click(object sender, EventArgs e)
    {
        grdList.DataBind();
    }

    protected void grdList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        var audit = new AuditLog();
        var sCep = ((GridViewRow)grdList.Rows[e.CommandArgument.To<int>()]).Cells[0].Text.UnMask();

        if (e.CommandName == "altCep")
        {
            pnlCep.Controls.ClearControls();
            pnlCep.Title = "Editar Cep";
            var cep = db.Ceps.BuscaCep(sCep, false);
            txtNrCep.Text = cep.NrCepFmt;
            ddlUF.SelectedValue = cep.CdUf;
            txtNoLogradouro.Text = cep.NoLogradouro ?? "";
            txtNoBairro.Text = cep.NoBairro ?? "";
            ucCidade.LoadCidade(cep.NrCep);
            btnSalvar.Visible = false;
            btnEditar.Visible = !btnSalvar.Visible;
            txtNrCep.Enabled = btnSalvar.Visible;
            ucCidade.Enabled = btnSalvar.Visible;
            pnlCep.Open(btnEditar, txtNoLogradouro);

            audit.Log("Alteração do CEP:\n" + cep.NrCep);
        }
        else if (e.CommandName == "excCep")
        {
            db.Ceps.RemoverCep(sCep);

            audit.Log("Exclusão do CEP:\n" + sCep);
            grdList.DataBind();
        }
    }

    protected void btnEditar_Click(object sender, EventArgs e)
    {
        db.Ceps.AtualizaCep(txtNrCep.Text.UnMask(), txtNoLogradouro.Text, txtNoBairro.Text, ucCidade.NoCidade, ddlUF.SelectedValue, ucCidade.CdCidade);
        pnlCep.Close();
        ShowInfoBox("Cep atualizado com sucesso!");
        grdList.DataBind();
    }
</script>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <box:NTab runat="server" ID="tabConnect" Text="Conectividade" Selected="true">

        <div class="mt-30 c-primary-blue">
            <span>Teste de conectividade com serviços externos</span>
        </div>

        <div class="form-div">
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Creanet</span></div>
                    <div class="field"><span class="n-label" id="creanet">aguarde...</span></div>
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Siscont</span></div>
                    <div class="field"><span class="n-label" id="siscont">aguarde...</span></div>
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>RH Senior</span></div>
                    <div class="field"><span class="n-label" id="senior">aguarde...</span></div>
                </div>
            </div>
        </div>

    </box:NTab>

    <box:NTab runat="server" ID="tabTabelas" Text="Tabelas">

        <div class="mt-30 c-primary-blue">
            <span>Tabelas integradas</span>
            <box:NButton runat="server" ID="btnSync" Text="Sincronizar tabelas" Theme="Primary" Icon="cog" OnClick="btnSync_Click" CssClass="right" />
        </div>

        <div class="form-div">
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Plano de Contas</span></div>
                    <div class="field">
                        <%= db.Query<ContaContabil>().Where(x => x.FgAtivo && x.NrAno == db.NrAnoBase).Count().ToString("n0") %>
                        <span class="help">Exercício <%= db.NrAnoBase %></span>
                        <box:NLabel runat="server" Theme="Warning" Text="Siscont" CssClass="right" />
                    </div>
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Centro de Custo</span></div>
                    <div class="field">
                        <%= db.Query<CentroCusto>().Where(x => x.FgAtivo && x.NrAno == db.NrAnoBase).Count() %>
                        <span class="help">Exercício <%= db.NrAnoBase %></span>
                        <box:NLabel runat="server" Theme="Warning" Text="Siscont" CssClass="right" />
                    </div>
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Unidade</span></div>
                    <div class="field">
                        <%= db.Query<Unidade>().Where(x => x.FgAtivo).Count() %>
                        <box:NLabel runat="server" Theme="Info" Text="Creanet" CssClass="right" />
                    </div>
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Entidade de classe</span></div>
                    <div class="field">
                        <%= db.Query<Entidade>().Where(x => x.TpEntidade == TpEntidade.EntidadeClasse && x.FgAtivo).Count() %>
                        <box:NLabel runat="server" Theme="Info" Text="Creanet" CssClass="right" />
                    </div>
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Instituicao de Ensino</span></div>
                    <div class="field">
                        <%= db.Query<Entidade>().Where(x => x.TpEntidade == TpEntidade.InstituicaoEnsino && x.FgAtivo).Count() %>
                        <box:NLabel runat="server" Theme="Info" Text="Creanet" CssClass="right" />
                    </div>
                </div>
            </div>
            <div class="sub-form-row" runat="server" id="trLogSync" visible="false">
                <div class="div-sub-form form-uc">
                    <div><span>Log</span></div>
                    <div class="field">
                        <span runat="server" id="tdLogSync" class="pre"></span>
                    </div>
                </div>
            </div>
        </div>

    </box:NTab>

    <box:NTab runat="server" ID="tabProfissionais" Text="Profissionais">

        <div class="mt-30 c-primary-blue">
            <span>Profissionais</span>
            <box:NButton runat="server" ID="btnSyncProf" Text="Sincronizar profissionais" Theme="Primary" Icon="users" OnClick="btnSyncProf_Click" CssClass="right"/>
        </div>

        <div class="form-div">
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Profissionais</span></div>
                    <div class="field">
                        <span id="spnAtu">0</span> de <%= db.Query<Pessoa>().Where(x => x.NrCreasp != null).Count() %>
                        <box:NLabel runat="server" Theme="Info" Text="Creanet" CssClass="right" />
                    </div>
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Erros</span></div>
                    <div class="field">
                        <span id="log"></span>
                    </div>
                </div>
            </div>
            <div class="help">Atualiza os dados de profissionais no sistema (nome, sexo, titulos, email, endereço residencial, telefones e data de nascimento) com base nos dados do CREANET. Mantenha esta janela aberta durante toda a atualização</div>
        </div>
        
    </box:NTab>

<%--    <box:NTab runat="server" ID="tabDocumentos" Text="Exportação de documentos">
        <box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnExpDoc" Text="Exportar documentos" Theme="Primary" Icon="users" OnClick="btnExpDoc_Click" />
        </box:NToolBar>
        <table class="form">
            <tr>
                <td>Documentos</td>
                <td>
                    <span id="spnAtuImport">0</span> de <%= db.Query<DocumentoMigracao>().Where(x => x.FgImportado == false).Count()%>
                    <box:NLabel runat="server" Theme="Info" Text="AzureBlobStorage" CssClass="right" />
                </td>
            </tr>
            <tr>
                <td>Erros</td>
                <td id="logImport"></td>
            </tr>
        </table>
        <div class="help">(NÃO EXECUTAR) Exportar documentos para AzureBlobStorage. Mantenha esta janela aberta durante toda a atualização</div>
    </box:NTab>--%>

    <box:NTab runat="server" ID="tabFunc" Text="Funcionario">

        <div class="mt-30 c-primary-blue">
            <span>Consulta funcionário Senior/RH</span>
            <box:NButton runat="server" ID="btnConsultaSenior" Theme="Default" Text="Consulta Senior RH" OnClick="btnConsultaSenior_Click" CssClass="right"/>
        </div>

        <div class="form-div">
            <div class="sub-form-row">
                <div class="div-sub-form" style="150px">
                    <div><span>Matricula/CPF</span></div>
                    <box:NTextBox runat="server" ID="txtNrMatricula" Width="150" />
                </div>
                <div class="div-sub-form">
                    <div><span>Nome</span></div>
                    <div class="field">
                        <asp:Literal runat="server" ID="ltrNoFunc" />
                    </div>
                </div>
            </div>
            <div class="sub-form-row">
                
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Centro de custo</span></div>
                    <div class="field">
                        <asp:Literal runat="server" ID="ltrNoCentroCusto" />
                    </div>
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Chefe imediato</span></div>
                    <div class="field">
                        <asp:Literal runat="server" ID="ltrChefe" />
                    </div>
                </div>
                 <div class="div-sub-form form-uc margin-left">
                    <div><span>Superintendente</span></div>
                    <div class="field">
                        <asp:Literal runat="server" ID="ltrSuperintendente" />
                    </div>
                </div>
            </div>
            <%--<div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Gerente</span></div>
                    <div class="field">
                        <asp:Literal runat="server" ID="ltrGerente" />
                    </div>
                </div>
            </div>--%>
        </div>

        <div class="mt-30 c-primary-blue">
            <span>Importar funcionários</span>
        </div>

        <div class="form-div">
            <div class="sub-form-row">
                <div class="div-sub-form">
                    <div><span>Novo arquivo</span></div>
                    <div class="field" style="height: 43px;">
                        <box:NFileUpload runat="server" ID="uplFileFunc"/>
                    </div>
                    <div class="help">Selecione o arquivo de importação de funcionários no formato .csv.</div>
                </div>
                <div class="div-sub-form ml-10 mt-25" style="width: 115px">
                    <box:NButton runat="server" ID="btnImpFunc" Theme="Primary" Icon="upload" Text="Importar" OnClick="btnImpFunc_Click" />
                    <asp:Literal runat="server" ID="lblErroImpFunc" />
                </div>
            </div>
        </div>

    </box:NTab>

    <box:NTab runat="server" ID="tabCEP" Text="CEP">

        <div class="mt-30 c-primary-blue right">
            <box:NButton runat="server" ID="btnImpCep" Theme="Primary" Icon="upload" Text="Importar CEP" OnClick="btnImpCep_Click" />
            <box:NButton runat="server" ID="btnPesqCep" Theme="Primary" Icon="search" Text="Consultar CEP" OnClick="btnPesqCep_Click" />
            <box:NButton runat="server" ID="btnIncCep" Theme="Success" Icon="plus" Text="Inserir CEP" OnClick="btnIncCep_Click" />
        </div>

        <div class="form-div">
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>CEPs</span></div>
                    <div class="field">
                        <%= db.Query<Cep>().Count().ToString("n0") %>
                        <box:NLabel runat="server" Theme="Default" Text="Arquivo" CssClass="right" />
                    </div>
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Novo arquivo</span></div>
                    <div class="field" style="height: 43px;">
                        <box:NFileUpload runat="server" ID="uplFile" />
                    </div>
                    <div class="help">Selecione o arquivo de importação de CEPs no formato .csv. Consulte em <a href="http://www.qualocep.com/" target="_blank">http://www.qualocep.com/</a></div>
                </div>
            </div>
        </div>
    </box:NTab>

    <box:NTab runat="server" ID="NTab1" Text="IIS">

        <div class="form-div">
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <box:NButton runat="server" ID="btnReset" Text="UnloadApp" Theme="Primary" Icon="cog" OnClick="btnReset_Click" />
                </div>
            </div>
        </div>
    </box:NTab>

    <box:NTab runat="server" ID="tabOrcamento" Text="Orçamento">

        <div class="mt-30 c-primary-blue">
            <span>Tabelas integradas SISCONT</span>
            <box:NButton runat="server" ID="btnSyncOrcSiscont" Text="Sincronizar orçamento (Siscont)" Theme="Primary" Icon="money" OnClick="btnSyncOrcSiscont_Click" CssClass="right" />
        </div>

        <div class="form-div">
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Dotações orçamentárias</span></div>
                    <div class="field">
                        <%= db.Query<DotacaoOrcamentaria>().Where(x => x.NrAno == db.NrAnoBase).Count().ToString("n0") %>
                        <span class="help">Exercício <%= db.NrAnoBase %></span>
                        <box:NLabel runat="server" Theme="Warning" Text="Siscont" CssClass="right" />
                    </div>
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Posição atual do orçamento</span></div>
                    <div class="field">
                        <%= db.Query<PosicaoAtualOrcamento>().Where(x => x.NrAno == db.NrAnoBase).Count().ToString("n0") %>
                        <span class="help">Exercício <%= db.NrAnoBase %></span>
                        <box:NLabel runat="server" Theme="Warning" Text="Siscont" CssClass="right" />
                    </div>
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Disponibilidade atual do orçamento</span></div>
                    <div class="field">
                         <%= db.Query<DisponibilidadeAtualOrcamento>().Where(x => x.NrAno == db.NrAnoBase).Count().ToString("n0") %>
                        <span class="help">Exercício <%= db.NrAnoBase %></span>
                        <box:NLabel runat="server" Theme="Warning" Text="Siscont" CssClass="right" />
                    </div>
                </div>
            </div>
            <%--<div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Contratos</span></div>
                    <div class="field">
                        <%= db.Query<Creasp.Elo.Contratos>().Where(x => x.NrAno == db.NrAnoBase).Count().ToString("n0") %>
                        <span class="help">Exercício <%= db.NrAnoBase %></span>
                        <box:NLabel runat="server" Theme="Warning" Text="Senior" CssClass="right" />
                    </div>
                </div>
            </div>--%>
        </div>

        <div class="mt-30 c-primary-blue">
            <span>Tabelas integradas SENIOR</span>
            <box:NButton runat="server" ID="btnSyncOrcSenior" Text="Sincronizar orçamento (Senior)" Theme="Primary" Icon="money" OnClick="btnSyncOrcSenior_Click" CssClass="right" />
        </div>

         <div class="form-div">
             <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Orçamento</span></div>
                    <div class="field">
                        <%= db.Query<OrcamentoSenior>().Where(x => x.NrAno == db.NrAnoBase).Count().ToString("n0") %>
                        <span class="help">Exercício <%= db.NrAnoBase %></span>
                        <box:NLabel runat="server" Theme="Warning" Text="Senior" CssClass="right" />
                    </div>
                </div>
            </div>
        </div>
    </box:NTab>

    <box:NPopup runat="server" ID="pnlErros" Title="Erros encontrados no sincronismo" Width="600">

        <div class="mt-30 c-primary-blue">
            <span>As seguintes contas contábeis não tinham uma conta 5 de referência</span>
        </div>

        <div class="form-div">
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div class="field">
                        <box:NTextBox runat="server" ID="txtContasErros" TextMode="MultiLine" Width="100%" Height="200px"></box:NTextBox>
                    </div>
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Faça o cadastro de mapeamento de contas 6 para 5 e sincronize novamente o orçamento</span></div>
                </div>
            </div>
        </div>
        <box:NButton runat="server" ID="btnOK" Theme="Primary" Text="Entendi" Icon="ok" OnClick="btnOK_Click" CssClass="right"/>
    </box:NPopup>

    <box:NPopup runat="server" Style="z-index: 1001" ID="pnlCep" Title="Novo/Editar Cep" Width="600">

        <div class="form-div">
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Número Cep:</span></div>
                    <box:NTextBox MaskType="Custom" MaskFormat="999.99-999" runat="server" ID="txtNrCep"></box:NTextBox>
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Logradouro:</span></div>
                    <box:NTextBox TextCapitalize="Upper" runat="server" ID="txtNoLogradouro"></box:NTextBox>
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Bairro:</span></div>
                    <box:NTextBox TextCapitalize="Upper" runat="server" ID="txtNoBairro"></box:NTextBox>
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Cidade:</span></div>
                    <uc:Cidade ID="ucCidade" runat="server" />
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>UF:</span></div>
                    <box:NDropDownList runat="server" ID="ddlUF"></box:NDropDownList>
                </div>
                <div class="div-sub-form form-uc mt-25">
                    <box:NButton runat="server" Visible="false" Width="200" ID="btnSalvar" Text="Salvar" Icon="ok" Theme="Primary" OnClick="btnSalvar_Click" CssClass="right"/>
                    <box:NButton runat="server" Visible="false" Width="200" ID="btnEditar" Text="Editar" Icon="ok" Theme="Success" OnClick="btnEditar_Click" CssClass="right" />
                </div>
            </div>
        </div>
    </box:NPopup>

    <box:NPopup runat="server" ID="pnlBuscaCep" Title="Busca Cep" Width="900">

        <div class="mt-30 c-primary-blue">
            <span>Buscas Cep</span>
        </div>

        <div class="form-div">
            <div class="sub-form-row">
                <div class="div-sub-form">
                    <div><span>Cidade:</span></div>
                    <box:NTextBox Width="100%" TextCapitalize="Upper" runat="server" ID="txtCidadePesquisaCep"></box:NTextBox>
                </div>
            </div>
            <box:NToolBar runat="server">
                <div class="sub-form-row">
                    <div class="div-sub-form">
                        <div><span>Número Cep:</span></div>
                        <box:NTextBox MaskType="Custom" MaskFormat="99.999-999" runat="server" ID="txtPesquisaCep"></box:NTextBox>
                    </div>
                    <div class="div-sub-form">
                        <div><span>UF:</span></div>
                        <box:NDropDownList runat="server" ID="ddlCdUfPesquisaCep"></box:NDropDownList>
                    </div>
                    <div class="div-sub-form mt-25 no-margin-bottom" style="width:200px;">
                        <box:NButton runat="server" ID="btnPesquisarCep" Text="Pesquisar" Icon="search" Theme="Default" OnClick="btnPesquisarCep_Click" />
                    </div>
                </div>
            </box:NToolBar>
        </div>

        <box:NGridView runat="server" ID="grdList" ItemType="Creasp.Core.Cep" OnRowCommand="grdList_RowCommand">
            <Columns>
                <asp:BoundField DataField="NrCepFmt" HeaderText="Número" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="100" SortExpression="NrCep" />
                <asp:BoundField DataField="NoCidade" HeaderText="Cidade" DataFormatString="{0:d}" HeaderStyle-Width="160" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" SortExpression="NoCidade" />
                <asp:BoundField DataField="NoBairro" HeaderText="Bairro" DataFormatString="{0:d}" HeaderStyle-Width="120" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" SortExpression="NoBairro" />
                <asp:BoundField DataField="NoLogradouro" HeaderText="Logradouro" SortExpression="NoLogradouro" />
                <asp:BoundField DataField="CdUf" HeaderText="UF" SortExpression="CdUf" HeaderStyle-Width="50" />

                <box:NButtonField Icon="pencil" CommandName="altCep" Theme="Info" ToolTip="Editar CEP" />
                <box:NButtonField Icon="cancel" OnClientClick="if (!this.disabled) {return confirm('Tem certeza que deseja excluir esse CEP?');}" CommandName="excCep" Theme="Danger" ToolTip="Remover CEP" />
            </Columns>
            <EmptyDataTemplate>Nenhum CEP encontrado no filtro informado</EmptyDataTemplate>
        </box:NGridView>

    </box:NPopup>

    <script>

        function executaSyncProfissional(index) {
            $.ws("ExecutaSyncProfissional", { skip: index }, function (data) {
                if (data) {
                    $('#spnAtu').text(index + 1);
                    executaSyncProfissional(++index);
                }
                else {
                    alert('Fim da atualização de profissionais');
                }
            }, function (err) {
                $('#spnAtu').text(index + 1);
                $("#log").append("<div style='color:red'>" + ($("#log > div").length + 1) + ". " + err + "</div>");
                executaSyncProfissional(++index);
            });
        }

        function ExecutaExportacaoArquivos(skip, count) {
            $.ws("ExecutaExportacaoArquivos", { skip: skip }, function (data) {
                if (data) {
                    $('#spnAtuImport').text(++count);
                    ExecutaExportacaoArquivos(skip, count);
                }
                else {
                    alert('Fim da importação de documentos');
                }
            }, function (err) {
                    $('#spnAtuImport').text(++count);
                $("#logImport").append("<div style='color:red'>" + ($("#logImport > div").length + 1) + ". " + err + "</div>");
                ExecutaExportacaoArquivos(++skip, count);
            });
        }

        function executaPingServidores() {
            var ping = function (id) {
                return $.ws("Ping", { servico: id }, function (data) {
                    $('#' + id).addClass('n-label-success').text('OK ' + data + ' ms');
                }, function (err) {
                    $('#' + id).addClass('n-label-danger').text(err);
                });
            };

            ping('siscont').always(function () {
                return ping('creanet');
            }).always(function () {
                return ping('senior');
            });
        }

    </script>

    <script src="../Content/Scripts/pagination.js"></script>
    <script>paginate()</script>

    <script src="../Content/Scripts/sidebar.js"></script>
    <script>selectButton("admin-integracao"); showReload("admin");</script>

</asp:Content>
