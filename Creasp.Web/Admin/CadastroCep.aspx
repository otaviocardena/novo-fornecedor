﻿<%@ Page Title="Cadastro CEP" Resource="admin.permissao" Language="C#" %>

<script runat="server">

    private CreaspContext db = new CreaspContext();

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO"};
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        grdList.PageSize = 10;
        grdList.RegisterPagedDataSource((p) => db.Ceps.ListaCeps(p,
            txtPesquisaCep.Text.UnMask(),
            txtCidadePesquisaCep.Text,
            ddlCdUfPesquisaCep.SelectedValue));
    }

    protected override void OnPrepareForm()
    {
        ddlUF.Bind(db.Ceps.ListaUFs().Select(x => new ListItem(x, x)), false, true);
        ddlCdUfPesquisaCep.Bind(db.Ceps.ListaUFs().Select(x => new ListItem(x, x)), true, true);
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        db.Ceps.IncluiCepManual(txtNrCep.Text.UnMask(), txtNoLogradouro.Text, txtNoBairro.Text, ucCidade.NoCidade, ddlUF.SelectedValue, ucCidade.CdCidade);
        pnlCep.Close();
        ShowInfoBox("CEP incluído com sucesso!");
    }

    protected void btnIncCep_Click(object sender, EventArgs e)
    {
        pnlCep.Controls.ClearControls();
        pnlCep.Title = "Novo CEP";
        ddlUF.SelectedValue = "SP";
        btnSalvar.Visible = true;
        btnEditar.Visible = !btnSalvar.Visible;
        txtNrCep.Enabled = btnSalvar.Visible;
        ucCidade.Enabled = btnSalvar.Visible;
        pnlCep.Open(btnSalvar, txtNrCep);
    }

    protected void btnPesquisarCep_Click(object sender, EventArgs e)
    {
        grdList.DataBind();
    }

    protected void grdList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        var audit = new AuditLog();
        var sCep = ((GridViewRow)grdList.Rows[e.CommandArgument.To<int>()]).Cells[0].Text.UnMask();

        if (e.CommandName == "altCep")
        {
            pnlCep.Controls.ClearControls();
            pnlCep.Title = "Editar CEP";
            var cep = db.Ceps.BuscaCep(sCep, false);
            txtNrCep.Text = cep.NrCepFmt;
            ddlUF.SelectedValue = cep.CdUf;
            txtNoLogradouro.Text = cep.NoLogradouro ?? "";
            txtNoBairro.Text = cep.NoBairro ?? "";
            ucCidade.LoadCidade(cep.NrCep);
            btnSalvar.Visible = false;
            btnEditar.Visible = !btnSalvar.Visible;
            txtNrCep.Enabled = btnSalvar.Visible;
            ucCidade.Enabled = btnSalvar.Visible;
            pnlCep.Open(btnEditar, txtNoLogradouro);

            audit.Log("Alteração do CEP:\n" + cep.NrCep);
        }
        else if (e.CommandName == "excCep")
        {
            db.Ceps.RemoverCep(sCep);

            audit.Log("Exclusão do CEP:\n" + sCep);
            grdList.DataBind();
        }
    }

    protected void btnEditar_Click(object sender, EventArgs e)
    {
        db.Ceps.AtualizaCep(txtNrCep.Text.UnMask(), txtNoLogradouro.Text, txtNoBairro.Text, ucCidade.NoCidade, ddlUF.SelectedValue, ucCidade.CdCidade);
        pnlCep.Close();
        ShowInfoBox("CEP atualizado com sucesso!");
        grdList.DataBind();
    }

</script>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <div class="mt-30 c-primary-blue">
        <span>Cadastro CEP</span>
        <box:NButton runat="server" ID="btnIncCep" Theme="Success" Icon="plus" ForeColor="White" Text="Novo CEP" Width="140" OnClick="btnIncCep_Click" CssClass="right new" />
    </div>

    <div class="form-div">
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Cidade</span></div>
                <box:NTextBox Width="100%" TextCapitalize="Upper" runat="server" ID="txtCidadePesquisaCep"></box:NTextBox>
            </div>
        </div>
        <div class="sub-form-row" style="max-width: 800px;">
            <div class="div-sub-form">
                <div><span>Número Cep</span></div>
                <box:NTextBox MaskType="Custom" MaskFormat="99.999-999" runat="server" ID="txtPesquisaCep"></box:NTextBox>
            </div>
            <div class="div-sub-form">
                <div><span>UF</span></div>
                <box:NDropDownList runat="server" Width="100%" ID="ddlCdUfPesquisaCep"></box:NDropDownList>
            </div>
            <div class="div-sub-form">
                <box:NButton runat="server" ID="btnPesquisarCep" Text="Pesquisar" Icon="search" Theme="Default" OnClick="btnPesquisarCep_Click" CssClass="mt-25 ml-10"/>
            </div>
        </div> 
    </div>

    <box:NGridView runat="server" ID="grdList" ItemType="Creasp.Core.Cep" OnRowCommand="grdList_RowCommand">
        <columns>
                <asp:BoundField DataField="NrCepFmt" HeaderText="Número" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="100" SortExpression="NrCep" />
                <asp:BoundField DataField="NoCidade" HeaderText="Cidade" DataFormatString="{0:d}" HeaderStyle-Width="160" SortExpression="NoCidade" />
                <asp:BoundField DataField="NoBairro" HeaderText="Bairro" DataFormatString="{0:d}" HeaderStyle-Width="120" SortExpression="NoBairro" />
                <asp:BoundField DataField="NoLogradouro" HeaderText="Logradouro" SortExpression="NoLogradouro" />
                <asp:BoundField DataField="CdUf" HeaderText="UF" SortExpression="CdUf" HeaderStyle-Width="50" />

                <box:NButtonField Icon="pencil" CommandName="altCep" Theme="Info" ToolTip="Editar CEP" />
                <box:NButtonField Icon="cancel" OnClientClick="if (!this.disabled) {return confirm('Tem certeza que deseja excluir esse CEP?');}" CommandName="excCep" Theme="Danger" ToolTip="Remover CEP" />
            </columns>
        <emptydatatemplate>Nenhum CEP encontrado no filtro informado</emptydatatemplate>
    </box:NGridView>

    <box:NPopup runat="server" Style="z-index: 1001" ID="pnlCep" Title="Novo/Editar Cep" Width="600">

        <div class="form-div">
            <div class="sub-form-row">
                <div class="div-sub-form">
                    <div><span>Número CEP</span></div>
                    <box:NTextBox MaskType="Custom" MaskFormat="99.999-999" Width="100%" runat="server" ID="txtNrCep"></box:NTextBox>
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form">
                    <div><span>Logradouro</span></div>
                    <box:NTextBox TextCapitalize="Upper" Width="100%" runat="server" ID="txtNoLogradouro"></box:NTextBox>
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form">
                    <div><span>Bairro</span></div>
                    <box:NTextBox TextCapitalize="Upper" Width="100%" runat="server" ID="txtNoBairro"></box:NTextBox>
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form">
                    <div><span>Cidade</span></div>
                    <uc:Cidade ID="ucCidade" runat="server" />
                </div>
                <div class="div-sub-form margin-left">
                    <div><span>UF</span></div>
                    <box:NDropDownList runat="server" ID="ddlUF"></box:NDropDownList>
                </div>
                <div class="mt-25">
                    <box:NButton runat="server" Visible="false" ID="btnSalvar" Width="150" Text="Salvar" Icon="ok" Theme="Primary" OnClick="btnSalvar_Click" />
                    <box:NButton runat="server" Visible="false" ID="btnEditar" Width="150" Text="Editar" Icon="ok" Theme="Primary" OnClick="btnEditar_Click" />
                </div>
            </div>
        </div>
    </box:NPopup>

    <script src="../Content/Scripts/pagination.js"></script>
    <script>paginate()</script>

    <script src="../Content/Scripts/sidebar.js"></script>
    <script>selectButton("admin-cep"); showReload("admin");</script>

</asp:Content>
