﻿<%@ Page Title="Perfil do Usuário" Resource="?" Language="C#" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();

    private CreaspUser _user;
    protected int? idFornecedor = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO"};
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        _user = User as CreaspUser;

        if (!IsPostBack && Request.QueryString["Tab"] == "func")
        {
            tabFunc.Selected = true;
            tabFunc_TabChanged(null, null);
        }

        
        idFornecedor = db.Pessoas.ObtemIdFornecedor(Auth.Current<CreaspUser>().IdPessoa);

        gridList.RegisterPagedDataSource((p) => db.Fornecedor.listaFornecedores(p));
        //gridList.RegisterDataSource(db.Fornecedor.listaFornecedores);
        gridList.DataBind();

    }

    protected void tabFunc_TabChanged(object sender, EventArgs e)
    {
        if (ltrNrMatricula.Text.Length == 0)
        {
            var func = new FuncionarioService(db).BuscaFuncionarioHierarquia(_user.Login);

            ltrNrMatricula.Text = func.Pessoa.NrMatricula.ToString() + " - " + func.Pessoa.NoPessoa;
            ltrNoCentroCusto.Text = func.Unidade.NrCentroCustoFmt;
            ltrNoUnidade.Text = func.Unidade.CdUnidade + " - " + func.Unidade.NoUnidade;

            ltrChefe.Text = MontaPessoa(func.Chefe);
            // ltrGerente.Text = MontaPessoa(func.Gerente);
            ltrSuperintendente.Text = MontaPessoa(func.Superintendente);

            var resp = new PessoaService(db).ListaDelegacoes(func.Pessoa.IdPessoa).ToList();

            ltrDelegacao.Text = string.Join(", ", resp.Select(x => db.SingleById<Pessoa>(x).NoPessoa).ToList());

        }
    }

    private string MontaPessoa(Funcionario f)
    {
        return $"{f.Pessoa.NrMatricula} - {f.Pessoa.NoPessoa} - " +
            $"<small>Unidade: {f.Unidade.CdUnidade} - {f.Unidade.NoUnidade}</small> - " +
            $"<small>Centro de custo: {f.Unidade.NrCentroCustoFmt}</small>";
    }

    protected void grdList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var data = e.Row.DataItem as IFgAtivo;
            if (!data.FgAtivo) e.Row.ForeColor = System.Drawing.Color.Red;
        }
    }

</script>
<asp:Content runat="server" ContentPlaceHolderID="content">
    
    <box:NTab runat="server" Text="Usuário" Selected="true" ID="tabUsuario">
        <div class="mt-30 c-primary-blue">
            <span>Dados do Usuário</span>
        </div>
        <div class="form-div b-bottom-gray">
            <div class="sub-form-row">
                <div class="div-sub-form">
                    <div><span>Login</span></div>
                    <span class="field"><%= _user.Login %></span>
                </div>
                <div class="div-sub-form margin-left">
                    <div><span>Nome</span></div>
                    <span class="field"><%= _user.Nome %></span>
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form">
                    <div><span>Email</span></div>
                    <span class="field"><%= _user.Email %></span>
                </div>
                <div class="div-sub-form margin-left">
                    <div><span>Grupo de Acessos</span></div>
                    <span class="field"><%= string.Join(", ", _user.GruposAd) %></span>
                </div>
                <div class="div-sub-form margin-left">
                    <div><span>Permissões</span></div>
                    <span class="field"><%= string.Join(", ", _user.Recursos) %></span>
                </div>
            </div>
        </div>
    </box:NTab>

    <box:NTab runat="server" ID="tabFunc" Text="Funcionário" AutoPostBack="true" OnTabChanged="tabFunc_TabChanged">

        <div class="mt-30 c-primary-blue">
            <span>Dados do Funcionário</span>
        </div>
        <div class="form-div b-bottom-gray">
            <div class="sub-form-row">
                <div class="div-sub-form">
                    <div><span>Matricula</span></div>
                    <span class="field"><asp:Literal runat="server" ID="ltrNrMatricula" /></span>
                </div>
                <div class="div-sub-form margin-left">
                    <div><span>Centro de custo</span></div>
                    <span class="field">
                        <asp:Literal runat="server" ID="ltrNoCentroCusto" />
                        <box:NLabel runat="server" Text="Senior" Theme="Success" CssClass="right" />
                    </span>
                </div>
                <div class="div-sub-form margin-left">
                    <div><span>Unidade</span></div>
                    <span class="field">
                        <asp:Literal runat="server" ID="ltrNoUnidade" />
                        <box:NLabel runat="server" Text="Creanet" Theme="Info" CssClass="right" />
                    </span>
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form">
                    <div><span>Chefe imediato</span></div>
                    <span class="field">
                        <asp:Literal runat="server" ID="ltrChefe" />
                        <box:NLabel runat="server" Text="Senior" Theme="Success" CssClass="right" />
                    </span>
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form">
                    <div><span>Superintendente</span></div>
                    <span class="field">
                        <asp:Literal runat="server" ID="ltrSuperintendente" />
                        <box:NLabel runat="server" Text="Senior" Theme="Success" CssClass="right" />
                    </span>
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form">
                    <div><span>Tenho delegação de</span></div>
                    <span class="field">
                        <box:NLabel runat="server" id="ltrDelegacao" />
                    </span>
                </div>
            </div>
        </div>
    </box:NTab>

    <box:NTab runat="server" ID="tabFornecedore" Text="Gerenciar Fornecedores" OnTabChanged="tabFunc_TabChanged">
        <div class="mt-30 c-primary-blue">
            <span>Todos os fornecedores</span>
        </div>

        <box:NGridView runat="server" ID="gridList" DataKeyNames="Email" ItemType="Creasp.Core.Model.Fornecedor">
            <Columns>
                <asp:BoundField DataField="IdFornecedor" HeaderText="Id" DataFormatString="{0:d}" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="50"/>
                <asp:BoundField DataField="NomeFantasia" HeaderText="Nome Fantasia" DataFormatString="{0:d}" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="180"/>
                <asp:BoundField DataField="Cnpj" HeaderText="CNPJ" DataFormatString="{0:d}" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="150"/>
                <asp:BoundField DataField="CEP" HeaderText="CEP" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="120"/>
                <asp:TemplateField HeaderText="Logradouro" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="300">
                    <ItemTemplate>
                        <div style="display: flex;align-items: center; justify-content:center;">
                            <div class="name-cyan"><%#Eval("Logradouro").ToString() + ", " + Eval("Numero") %>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Complemento" HeaderText="Complemento" DataFormatString="{0:d}" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="120"/>
                <asp:BoundField DataField="Email" HeaderText="Email/Usuario" DataFormatString="{0:d}" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="180"/>
            </Columns>
            <EmptyDataTemplate>Não foram encontrados fornecedores.</EmptyDataTemplate>
        </box:NGridView>

        <script src="../Content/Scripts/pagination.js"></script>
        <script>paginate()</script>

    </box:NTab>

    <script src="../Content/Scripts/sidebar.js"></script>
    <script>selectButton("admin-home"); showReload("admin");</script>

</asp:Content>