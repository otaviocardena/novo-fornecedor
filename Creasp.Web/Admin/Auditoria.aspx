﻿<%@ Page Title="Auditoria" Resource="admin.auditoria" Language="C#" %>
<script runat="server">

    private CreaspContext db = new CreaspContext();

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO"};
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        grdList.RegisterPagedDataSource((p) =>
            db.Query<Auditoria>()
                .Where(!ucPeriodo.Periodo.IsVazio, x => ucPeriodo.Periodo.Inicio <= x.DtAuditoria && ucPeriodo.Periodo.Fim.AddDays(1) >= x.DtAuditoria)
                .Where(txtLogin.HasValue(), x => x.NoLogin.StartsWith(txtLogin.Text))
                .Where(cmbNoTabela.HasValue(), x => x.NoTabela == cmbNoTabela.SelectedValue)
                .ToPage(p.DefaultSort<Auditoria>(x => x.DtAuditoria, false)));
    }

    protected override void OnPrepareForm()
    {
        ucPeriodo.Periodo = Periodo.Hoje;

        cmbNoTabela.Items.Add(new ListItem("", ""));
        cmbNoTabela.Items.Add(new ListItem("Log", "Log"));
        cmbNoTabela.Items.Add(new ListItem("Login", "Login"));
        cmbNoTabela.Items.Add(new ListItem("Logout", "Logout"));
        cmbNoTabela.Items.Add(new ListItem("Erro", "Error"));
    }

    protected void btnPesquisar_Click(object sender, EventArgs e)
    {
        ucPeriodo.Periodo.ValidaMax('M', 3, false);
        grdList.DataBindPaged();
    }

</script>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <div class="mt-30 c-primary-blue">
        <span>Auditoria</span>
        <box:NButton runat="server" ID="btnPesquisar" Text="Pesquisar" Theme="Default" Icon="search" OnClick="btnPesquisar_Click" CssClass="right"/>
    </div>

    <div class="form-div" runat="server" id="frm">
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Período</span></div>
                <uc:Periodo runat="server" ID="ucPeriodo" Required="true" />
            </div>
        </div>
        <div class="sub-form-row" style="width:100%">
            <div class="div-sub-form" style="width:150px">
                <div><span>Recurso</span></div>
                <box:NDropDownList ID="cmbNoTabela" runat="server" Width="100%" />
            </div>
            <div class="div-sub-form margin-left">
                <div><span>Usuário</span></div>
                <box:NTextBox runat="server" ID="txtLogin" Width="100%" />
            </div>
        </div>
    </div>

    <box:NGridView runat="server" ID="grdList" AllowGrouping="true" ItemType="Creasp.Core.Auditoria" PageSize="50">
        <Columns>
            <asp:BoundField DataField="DtAuditoria" SortExpression="DtAuditoria" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="DtAuditoria" SortExpression="DtAuditoria" DataFormatString="{0:HH:mm:ss}" HeaderText="Hora" HeaderStyle-Width="70" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="NoLogin" HeaderText="Usuário" SortExpression="NoLogin" HeaderStyle-Width="155" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="NoTabela" HeaderText="Recurso" SortExpression="NoTabela" HeaderStyle-Width="180" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="nowrap-ellipsis"/>
            <asp:BoundField DataField="IdChave" HeaderText="ID" SortExpression="IdChave" HeaderStyle-Width="80" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="NoConteudo" HeaderText="Conteúdo" />
        </Columns>
        <EmptyDataTemplate>Nenhum registro de auditoria encontrado</EmptyDataTemplate>
    </box:NGridView>

    <script src="../Content/Scripts/sidebar.js"></script>
    <script>selectButton("admin-auditoria"); showReload("admin");</script>

</asp:Content>
