﻿<%@ Page Title="Tipos de documento" Resource="admin.tipodoc" Language="C#" %>
<script runat="server">

    private CreaspContext db = new CreaspContext();

    public string TpRef => Request.QueryString["TpRef"];

    public int? CdTipoDoc { get { return ViewState["CdTipoDoc"].To<int?>(); } set { ViewState["CdTipoDoc"] = value; } }

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO"};
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        grdList.RegisterDataSource(() => db.Query<TipoDocumento>().Where(x => x.TpRef == TpRef).ThenBy(x => x.NrOrdem).ToEnumerable());
    }

    protected override void OnPrepareForm()
    {
        PageTitle = "Tipos de documento";
        tabGeral.Text = TpRef.ToLower().Captalize();

        grdList.DataBind();
    }

    protected void btnIncluir_Click(object sender, EventArgs e)
    {
        CdTipoDoc = null;
        txtNoTipoDoc.Text = "";
        chkFgObrigatorio.Checked = false;
        chkFgAtivo.Checked = true;
        pnlForm.Open(btnSalvar, txtNoTipoDoc);
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        if(CdTipoDoc == null)
        {
            db.TipoDocumentos.Inclui(TpRef,
                txtNoTipoDoc.Text,
                cmbNrOrdem.SelectedValue.To<int>(),
                chkFgObrigatorio.Checked,
                chkFgAtivo.Checked);
        }
        else
        {
            db.TipoDocumentos.Altera(CdTipoDoc.Value,
                txtNoTipoDoc.Text,
                cmbNrOrdem.SelectedValue.To<int>(),
                chkFgObrigatorio.Checked,
                chkFgAtivo.Checked);
        }

        ShowInfoBox("Tipo de documento gravado com sucesso");
        grdList.DataBind();
        pnlForm.Close();
    }

    protected void grdList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if(e.CommandName == "alt")
        {
            CdTipoDoc = e.DataKey<int>(sender);
            var td = db.SingleById<TipoDocumento>(CdTipoDoc);
            txtNoTipoDoc.Text = td.NoTipoDoc;
            cmbNrOrdem.SelectedValue = td.NrOrdem.ToString();
            chkFgObrigatorio.Checked = td.FgObrigatorio;
            chkFgAtivo.Checked = td.FgAtivo;
            pnlForm.Open(btnSalvar, txtNoTipoDoc);
        }
        else if(e.CommandName == "del")
        {
            db.TipoDocumentos.Exclui(e.DataKey<int>(sender));
            grdList.DataBind();
            ShowInfoBox("Tipo de documento excluído com sucesso");
        }
    }

</script>
<asp:Content runat="server" ContentPlaceHolderID="content">

    <box:NTab runat="server" ID="tabGeral" Selected="true" Height="450">

        <box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnIncluir" OnClick="btnIncluir_Click" Theme="Default" Icon="plus" Text="Novo" />
        </box:NToolBar>

        <box:NGridView runat="server" ID="grdList" ItemType="Creasp.Core.TipoDocumento" DataKeyNames="CdTipoDoc" OnRowCommand="grdList_RowCommand" ShowTotal="false">
            <Columns>
                <asp:TemplateField HeaderText="Descrição">
                    <ItemTemplate>
                        <box:NLabel runat="server" Text=<%# Item.NoTipoDoc %> Font-Strikeout=<%# !Item.FgAtivo %> />
                        <box:NLabel runat="server" Text="*" Theme="Danger" ToolTip="Obrigatório" Visible=<%# Item.FgObrigatorio %> />
                    </ItemTemplate>
                </asp:TemplateField>
                <box:NButtonField Icon="pencil" ToolTip="Alterar" CommandName="alt" Theme="Info" />
                <box:NButtonField Icon="cancel" ToolTip="Excluir" CommandName="del" Theme="Danger" OnClientClick="return confirm('Confirma excluir este tipo de documento?')" />
            </Columns>
            <EmptyDataTemplate>Nenhum tipo de documento cadastrado</EmptyDataTemplate>
        </box:NGridView>

    </box:NTab>

    <box:NPopup runat="server" ID="pnlForm" Width="550" Title="Cadastro de tipo de documento">

        <box:NTab runat="server" ID="tabCad" Text="Geral" Selected="true" GroupName="b">

            <box:NToolBar runat="server">
                <box:NButton runat="server" ID="btnSalvar" OnClick="btnSalvar_Click" Theme="Primary" Icon="ok" Text="Salvar" />
            </box:NToolBar>

            <table class="form">
                <tr>
                    <td>Descrição</td>
                    <td><box:NTextBox runat="server" ID="txtNoTipoDoc" Width="100%" Required="true" MaxLength="40" /></td>
                </tr>
                <tr>
                    <td>Ordem de exibição</td>
                    <td>
                        <box:NDropDownList runat="server" ID="cmbNrOrdem">
                            <asp:ListItem Text="1" Value="1" />
                            <asp:ListItem Text="2" Value="2" />
                            <asp:ListItem Text="3" Value="3" />
                            <asp:ListItem Text="4" Value="4" />
                            <asp:ListItem Text="5" Value="5" />
                            <asp:ListItem Text="6" Value="6" />
                            <asp:ListItem Text="7" Value="7" />
                            <asp:ListItem Text="8" Value="8" />
                            <asp:ListItem Text="9" Value="9" />
                            <asp:ListItem Text="10" Value="10" />
                        </box:NDropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan="2"><box:NCheckBox runat="server" ID="chkFgObrigatorio" Text="Documento obrigatório" /></td>
                </tr>
                <tr>
                    <td colspan="2"><box:NCheckBox runat="server" ID="chkFgAtivo" Text="Ativo" Checked="true" /></td>
                </tr>
            </table>
        </box:NTab>
    </box:NPopup>
    
</asp:Content>