﻿<%@ Page Title="Hierarquia Senior" Resource="admin.senior" Language="C#" %>
<script runat="server">

    protected IDatabase db = Factory.Get<IDatabase>();

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO"};
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        grdList.RegisterPagedDataSource((p) => db.Query<FuncionarioSenior>()
                .Where(txtPesqFunc.HasValue(), x => x.NoFuncionario.StartsWith(txtPesqFunc.Text))
                .ToPage(p.DefaultSort<FuncionarioSenior>(z => z.NoFuncionario)));
    }

    protected override void OnPrepareForm()
    {
        grdList.DataBind();
    }

    protected void btnPesquisar_Click(object sender, EventArgs e)
    {
        grdList.DataBind();
    }

    protected void grdList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "alt")
        {
            txtNrMatricula.Enabled = false;
            var f = db.SingleById<FuncionarioSenior>(e.DataKey<int>(sender));

            txtNrMatricula.SetText(f.NrMatricula);
            txtNrCPF.SetText(f.NrCpf);
            txtNoFuncionario.Text = f.NoFuncionario;
            txtNoCargo.Text = f.NoCargo;
            ucCentroCusto.LoadCentroCustoByNrCentroCusto(f.NrCentroCusto);
            ucChefe.LoadPessoaByCpf(f.NrCpfChefe);
            ucGerente.LoadPessoaByCpf(f.NrCpfGerente);
            ucSuperintendente.LoadPessoaByCpf(f.NrCpfSuperintendente);

            pnlFunc.Open(btnSalvar, txtNoFuncionario);
        }
        else if (e.CommandName == "del")
        {
            var nrMatricula = e.DataKey<int>(sender, "NrMatricula");

            db.Delete<FuncionarioSenior>(nrMatricula);

            ShowInfoBox("Operação realizada com sucesso");
            grdList.DataBind();
        }
    }

    protected void lnkIncluir_Click(object sender, EventArgs e)
    {
        txtNrMatricula.Enabled = true;
        pnlFunc.Controls.ClearControls();
        pnlFunc.Open(btnSalvar, txtNrMatricula);
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        var f = new FuncionarioSenior
        {
            NrMatricula = txtNrMatricula.Text.To<int>(),
            NrCpf = txtNrCPF.Text.UnMask(),
            NoFuncionario = txtNoFuncionario.Text,
            NoCargo = txtNoCargo.Text,
            NrCentroCusto = ucCentroCusto.CentroCusto.NrCentroCusto,
            NrCpfChefe = ucChefe.Pessoa.NrCpf,
            NoChefe = ucChefe.Pessoa.NoPessoa,
            NrCpfGerente = ucGerente.Pessoa.NrCpf,
            NoGerente = ucGerente.Pessoa.NoPessoa,
            NrCpfSuperintendente= ucSuperintendente.Pessoa.NrCpf,
            NoSuperintendente = ucSuperintendente.Pessoa.NoPessoa
        };

        db.Save(txtNrMatricula.Enabled, f);

        ShowInfoBox("Hierarquia cadastrada com sucesso");

        pnlFunc.Close();
        grdList.DataBind();
    }

    protected void txtNrMatricula_TextChanged(object sender, EventArgs e)
    {
        if(txtNrMatricula.HasValue())
        {
            var senior = Factory.Get<ISenior>();

            var func = senior.BuscaFuncionarios(txtNrMatricula.Text.To<int>(), null, null).FirstOrDefault();

            if (func != null)
            {
                txtNrCPF.SetText(func.NrCpf);
                txtNoFuncionario.SetText(func.NoFuncionario);
                txtNoCargo.SetText(func.NoCargo);
                ucCentroCusto.LoadCentroCustoByNrCentroCusto(func.NrCentroCusto);
            }
        }

    }

</script>

<asp:Content runat="server" ContentPlaceHolderID="content">

    <div class="mt-30 c-primary-blue">
        <span>Hierarquia Sênior</span>
        <box:NButton runat="server" ID="lnkIncluir" ForeColor="white" Theme="Success" Icon="plus" Text="Novo funcionário" OnClick="lnkIncluir_Click"  CssClass="right new margin-left" />
    </div>

    <div class="form-div">
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Nome</span></div>
                <box:NTextBox runat="server" ID="txtPesqFunc" Width="100%"  TextCapitalize="Upper" />
                <div class="help" style="font-size: 14px;">
                    Os funcionários aqui digitados sobregravam o retorno do webservice da Senior. Todos demais funcionários que não
                    estejam nesta lista continuará valendo os dados vindos Senior/RH.
                </div>
            </div>
            <div class="div-sub-form" style="width: 140px;">
                <box:NButton runat="server" ID="btnPesquisar" Icon="search" Text="Pesquisar" OnClick="btnPesquisar_Click" CausesValidation="false" CssClass="mt-25 margin-left"/>
            </div>
        </div>
    </div>

    <box:NGridView runat="server" ID="grdList" ItemType="Creasp.Core.FuncionarioSenior" DataKeyNames="NrMatricula" OnRowCommand="grdList_RowCommand">
        <Columns>
            <asp:BoundField DataField="NrMatricula" HeaderText="#" ItemStyle-Width="70" HeaderStyle-Width="70" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
            <asp:TemplateField HeaderText="Nome">
                <ItemTemplate>
                    <%# Item.NoFuncionario %>
                    <div>
                        <small><%# Pessoa.FormataCpf(Item.NrCpf) + " - " + CentroCusto.Formata(Item.NrCentroCusto) + " - " + Item.NoCargo %></small>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Chefe Imediato" HeaderStyle-Width="240">
                <ItemTemplate>
                    <%# Item.NoChefe %>
                    <div><small><%# Pessoa.FormataCpf(Item.NrCpfChefe) %></small></div>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Superintendente" HeaderStyle-Width="240">
                <ItemTemplate>
                    <%# Item.NoSuperintendente %>
                    <div><small><%# Pessoa.FormataCpf(Item.NrCpfSuperintendente) %></small></div>
                </ItemTemplate>
            </asp:TemplateField>
            <box:NButtonField CommandName="alt" Theme="Info" Icon="pencil" ToolTip="Alterar" />
            <box:NButtonField CommandName="del" Theme="Danger" Icon="cancel" ToolTip="Excluir" OnClientClick="return confirm('Confirma excluir este registro?');" />
        </Columns>
        <EmptyDataTemplate>Nenhuma hierarquia cadastrada</EmptyDataTemplate>
    </box:NGridView>

    <box:NPopup runat="server" ID="pnlFunc" Title="Hierarquia de funcionário" Width="750">

        <div class="form-div">
            <div class="sub-form-row">
                <div class="div-sub-form">
                    <div><span>Matricula</span></div>
                    <box:NTextBox runat="server" ID="txtNrMatricula" MaskType="Integer" Width="300" Required="true" AutoPostBack="true" OnTextChanged="txtNrMatricula_TextChanged" />
                </div>
                <div class="div-sub-form">
                    <div><span>CPF</span></div>
                    <box:NTextBox runat="server" ID="txtNrCPF" MaskType="Custom" Width="150" MaskFormat="999.999.999-99" Required="true" />
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form">
                    <div><span>Nome</span></div>
                    <box:NTextBox runat="server" ID="txtNoFuncionario" Width="100%" TextCapitalize="Upper" Required="true" />
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form">
                    <div><span>Centro de Custo</span></div>
                    <uc:CentroCusto runat="server" ID="ucCentroCusto" Required="true" />
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form">
                    <div><span>Cargo</span></div>
                    <box:NTextBox runat="server" ID="txtNoCargo" Width="100%" TextCapitalize="Upper" Required="true" />
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Superior Imediato</span></div>
                    <uc:Pessoa runat="server" ID="ucChefe" IsFuncionario="true" Required="true" />
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Gerente</span></div>
                    <uc:Pessoa runat="server" ID="ucGerente" IsFuncionario="true" Required="true" />
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Superintendente</span></div>
                    <uc:Pessoa runat="server" ID="ucSuperintendente" IsFuncionario="true" Required="true" />
                </div>
            </div>
        </div>
        <box:NButton runat="server" ID="btnSalvar" Text="Salvar" Icon="ok" Theme="Primary" OnClick="btnSalvar_Click" CssClass="right"/>

    </box:NPopup>

    <script src="../Content/Scripts/pagination.js"></script>
    <script>paginate()</script>

    <script src="../Content/Scripts/sidebar.js"></script>
    <script>selectButton("admin-senior"); showReload("admin");</script>

</asp:Content>