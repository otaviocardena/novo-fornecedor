﻿<%@ Page Title="Cadastro de Pessoa" Language="C#" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO"};
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        grdList.RegisterPagedDataSource((p) =>
            db.Query<Pessoa>()
                .Where(txtNoPessoa.HasValue(), x => x.NoPessoa.Contains(txtNoPessoa.Text.ToLike()))
                .Where(txtNrCreasp.HasValue(), x => x.NrCreasp == txtNrCreasp.Text)
                .Where(txtNrCpf.HasValue(), x => x.NrCpf == txtNrCpf.Text.UnMask())
                .Where(txtNrMatricula.HasValue(), x => x.NrMatricula == txtNrMatricula.Text.To<int>())
                .Where(chkMatricula.Checked, x => x.NrMatricula != null)
                .ToPage(p.DefaultSort<Pessoa>(x => x.NoPessoa)));

        RegisterRestorePageState(() => grdList.DataBind());
    }

    protected override void OnPrepareForm()
    {
        SetDefaultButton(btnPesquisar);
    }

    protected void btnPesquisar_Click(object sender, EventArgs e)
    {
        grdList.DataBindPaged();
    }

    protected void grdList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "alt")
        {
            ucPessoa.EditPessoa(e.DataKey<int>(sender));
        }
    }

    protected void btnIncluir_Click(object sender, EventArgs e)
    {
        ucPessoa.InsertPessoa();
    }

</script>
<asp:Content ContentPlaceHolderID="title" runat="server">
    
</asp:Content>
<asp:Content ContentPlaceHolderID="content" runat="server">
    
    <div class="mt-30 c-primary-blue">
        <span>Cadastro CEP</span>
        <box:NButton runat="server" ID="btnIncluir" Theme="Success" Icon="plus" ForeColor="White" Text="Nova pessoa" OnClick="btnIncluir_Click" CssClass="right new ml-10"/>
        <box:NButton runat="server" ID="btnPesquisar" Icon="search" Text="Pesquisar" Theme="Default" OnClick="btnPesquisar_Click" CssClass="right" />
    </div>

    <uc:Pessoa runat="server" ID="ucPessoa" InsertOnly="true" OnPessoaChanged="btnPesquisar_Click" />

    <div class="form-div">
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Nome</span></div>
                <box:NTextBox runat="server" ID="txtNoPessoa" TextCapitalize="Upper" Width="100%" autofocus />
            </div>
            <div class="div-sub-form margin-left">
                <div><span>CREASP</span></div>
                <box:NTextBox runat="server" ID="txtNrCreasp" TextCapitalize="Upper" MaskType="Custom" MaskFormat="9999999999" Width="150" />
            </div>
        </div>
        <div class="sub-form-row" style="width:100%">
            <div class="div-sub-form" style="width:150px">
                <div><span>CPF/CNPJ</span></div>
                <box:NTextBox runat="server" ID="txtNrCpf" TextCapitalize="Upper" Width="150" />
            </div>
            <div class="div-sub-form margin-left">
                <div><span>Matricula</span></div>
                <box:NTextBox runat="server" ID="txtNrMatricula" TextCapitalize="Upper" Width="150" MaskType="Integer" />
                &nbsp;&nbsp;
                <box:NCheckBox runat="server" ID="chkMatricula" Text="Somente funcionários" />
            </div>
        </div>
    </div>

    <box:NGridView runat="server" ID="grdList" ItemType="Creasp.Core.Pessoa" OnRowCommand="grdList_RowCommand" DataKeyNames="IdPessoa">
        <Columns>
            <asp:BoundField DataField="NrCpfFmt" HeaderText="CPF/CNPJ" SortExpression="NrCpf" HeaderStyle-Width="160" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="NoPessoa" HeaderText="Nome/Razão Social" SortExpression="NoPessoa" />
            <asp:BoundField DataField="NrMatricula" HeaderText="Matricula" SortExpression="NrMatricula" HeaderStyle-Width="120" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
            <box:NButtonField CommandName="alt" Icon="pencil" Theme="Info" />
        </Columns>
        <EmptyDataTemplate>Não foi encontrado nenhuma pessoa para o filtro informado</EmptyDataTemplate>
    </box:NGridView>

    <script src="../Content/Scripts/pagination.js"></script>
    <script>paginate()</script>

    <script src="../Content/Scripts/sidebar.js"></script>
    <script>selectButton("admin-pessoa"); showReload("admin");</script>

</asp:Content>