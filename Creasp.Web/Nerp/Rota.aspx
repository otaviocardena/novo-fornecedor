﻿<%@ Page Title="Roterização de Municípios" Resource="nerp.rotas" Language="C#" %>
<script runat="server">

    private CreaspContext db = new CreaspContext();

    protected override void OnRegister()
    {
        grdList.RegisterDataSource(() => db.Ceps.ListaRotas(cmbUfOrigem.SelectedValue, cmbCidadeDestino.SelectedValue.To<int>()));
    }

    protected override void OnPrepareForm()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        cmbUfOrigem.Bind(db.Ceps.ListaUFs().Select(x => new ListItem(x, x)), false, true);
        cmbUfDestino.Bind(db.Ceps.ListaUFs().Select(x => new ListItem(x, x)), false, true);
        cmbUfDestino.SelectedValue = cmbUfOrigem.SelectedValue = "SP";
        cmbUfDestino_SelectedIndexChanged(null, null);

        SetDefaultButton(btnPesquisar);
    }

    protected void uplRota_FileUpload(object sender, EventArgs e)
    {
        db.Ceps.ImportaRota(uplRota.PostedFile.InputStream);
        ShowInfoBox("Arquivo importado com sucesso");
    }

    protected void btnPesquisar_Click(object sender, EventArgs e)
    {
        btnSalvar.Visible = btnCancelar.Visible = true;
        uplRota.Visible = btnPesquisar.Visible = cmbUfDestino.Enabled = cmbUfOrigem.Enabled = cmbCidadeDestino.Enabled = false;
        grdList.Columns[0].HeaderText = "De " + cmbCidadeDestino.SelectedItem.Text + " para";
        grdList.DataBind();
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        var list = new List<Rota>();

        for(var i = 0; i < grdList.Rows.Count; i++)
        {
            var row = grdList.Rows[i];
            list.Add(new Rota
            {
                CdCidadeOrigem = grdList.DataKeys[i].Value.To<int>(),
                CdCidadeDestino = cmbCidadeDestino.SelectedValue.To<int>(),
                QtKmIda = (row.FindControl("txtQtIda") as NTextBox).Text.To<int>(0),
                QtKmVolta = (row.FindControl("txtQtVolta") as NTextBox).Text.To<int>(0)
            });
        }

        db.Ceps.AtualizaRotas(list);

        ShowInfoBox("Rotas gravadas com sucesso");
        btnCancelar_Click(null, null);
    }

    protected void cmbUfDestino_SelectedIndexChanged(object sender, EventArgs e)
    {
        cmbCidadeDestino.Bind(db.Ceps.ListaCidades(cmbUfDestino.SelectedValue).Select(x => new ListItem(x.NoCidade, x.CdCidade.ToString())));;
    }

    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        grdList.Clear();
        btnSalvar.Visible = btnCancelar.Visible = false;
        uplRota.Visible = btnPesquisar.Visible = cmbUfOrigem.Enabled = cmbUfDestino.Enabled = cmbCidadeDestino.Enabled = true;
    }

</script>
<asp:Content runat="server" ContentPlaceHolderID="content">

    <div class="mt-30 c-primary-blue">
        <span>Roteirização de Municípios</span>
        <div class="right flex">
        <box:NButton runat="server" ID="btnPesquisar" Icon="search" Text="Pesquisar" OnClick="btnPesquisar_Click" CausesValidation="true" />
        <box:NButton runat="server" ID="btnSalvar" Icon="ok" Text="Salvar" OnClick="btnSalvar_Click" Visible="false" Theme="Primary" CssClass="margin-left"/>
        <box:NButton runat="server" ID="btnCancelar" Icon="" Text="Cancelar" OnClick="btnCancelar_Click" Visible="false" CssClass="margin-left"/>
        <box:NFileUpload runat="server" ID="uplRota" Icon="upload" Text="Upload" AutoPostBack="true" OnFileUpload="uplRota_FileUpload" ButtonTheme="Primary" CssClass="margin-left"/>
    </div>
    </div>

    <div class="form-div">
        <div class="sub-form-row">
            <div class="div-sub-form form-uc">
                <div><span>Origem</span></div>
                <box:NDropDownList runat="server" ID="cmbUfOrigem" Width="60" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form form-uc">
                <div><span>Destino</span></div>
                <box:NDropDownList runat="server" ID="cmbUfDestino" Width="60" AutoPostBack="true" OnSelectedIndexChanged="cmbUfDestino_SelectedIndexChanged" />
                <box:NDropDownList runat="server" ID="cmbCidadeDestino" Width="450" Required="true" />
            </div>
        </div>
    </div>

    <box:NGridView runat="server" ID="grdList" ItemType="Creasp.Core.Rota" DataKeyNames="CdCidadeOrigem">
        <Columns>
            <asp:BoundField DataField="NoOrigem" HeaderText="Origem" />
            <asp:TemplateField HeaderText="Ida" HeaderStyle-Width="150" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <box:NTextBox runat="server" ID="txtQtIda" MaskType="Integer" Text=<%# Item.QtKmIda == 0 ? "" : Item.QtKmIda.ToString() %> CssClass="t-center" Width="80" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Volta" HeaderStyle-Width="150" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <box:NTextBox runat="server" ID="txtQtVolta" MaskType="Integer" Text=<%# Item.QtKmVolta == 0 ? "" : Item.QtKmVolta.ToString() %> CssClass="t-center" Width="80" />
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </box:NGridView>

    <script src="../Content/Scripts/sidebar.js"></script>
    <script>selectButton("nerp-rota"); showReload("nerp")</script>

</asp:Content>