﻿<%@ Control Language="C#" %>
<script runat="server">

    protected CreaspContext db;

    protected int IdNerp => Request.QueryString["IdNerp"].To<int>();

    public void OnRegister(CreaspContext db, NerpPermissao permissao)
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO", "ALL.FORNECEDOR" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Page.Redirect("~/Login.aspx", false, true);
        }
        
        if (Auth.Current<CreaspUser>().Fornecedor)
        {
            btnAdd.Visible = false;
            txtNoHistorico.Visible = false;
        }

        this.db = db;

        grdHistorico.RegisterDataSource(() => db.Query<Auditoria>()
                .Where(x => x.NoTabela == "Nerp")
                .Where(x => x.IdChave == IdNerp)
                .OrderByDescending(x => x.DtAuditoria)
                .ToEnumerable());

    }

    public void MontaTela()
    {
        grdHistorico.DataBind();
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        txtNoHistorico.Validate().Required();

        new AuditLog().Table("Nerp", IdNerp, txtNoHistorico.Text);

        txtNoHistorico.Text = "";

        grdHistorico.DataBind();

        Page.ShowInfoBox("Comentário adicionado com sucesso");
    }

</script>

<div class="mt-30 c-primary-blue">
    <span>Novo comentário</span>
</div>

<div class="form-div">
    <div class="sub-form-row margin-top-0">
        <div class="div-sub-form">
            <div><span>Comentário</span></div>
            <div class="input-group">
                <box:NTextBox runat="server" ID="txtNoHistorico" Width="100%" Rows="3" CssClass="text-box" TextMode="MultiLine" placeholder="Escreva alguma observação sobre esta NERP"></box:NTextBox>
                <box:NButton runat="server" ID="btnAdd" Theme="Primary" Text="Adicionar" OnClick="btnAdd_Click" CssClass="right" />
            </div>
        </div>
    </div>
</div>

<box:NGridView runat="server" ID="grdHistorico" ItemType="Creasp.Core.Auditoria">
    <Columns>
        <asp:BoundField DataField="DtAuditoria" DataFormatString="{0:dd/MM/yyyy HH:mm}" HeaderStyle-Width="125" HeaderText="Data" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
        <asp:BoundField DataField="NoLogin" HeaderText="Usuário" HeaderStyle-Width="145" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
        <asp:BoundField DataField="NoConteudo" HeaderText="Conteúdo" />
    </Columns>
    <EmptyDataTemplate>Nenhum registro encontrado</EmptyDataTemplate>
</box:NGridView>
