﻿<%@ Page Title="Convênio de ART" Resource="nerp.art" Language="C#" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();

    protected List<ConvenioArtDTO> Convenios { get { return ViewState["Convenios"] as List<ConvenioArtDTO>; } set { ViewState["Convenios"] = value; } }

    protected int NrMes => Request.QueryString["Mes"].To<int>();
    protected int NrAno => Request.QueryString["Ano"].To<int>();

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        grdList.RegisterDataSource(() => Convenios);

        RegisterRestorePageState(() => grdList.DataBind());
    }

    protected override void OnPrepareForm()
    {
        var erros = new StringBuilder();
        Convenios = db.Nerps.ListaConvenioART(NrAno, NrMes, erros);

        if (erros.Length > 0)
        {
            ltrErros.Text = erros.ToString().Replace("\n", "<br/>");
            trErros.Visible = true;
        }

        grdList.DataBind();

        this.BackTo("Nerp.aspx");
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        if(grdList.SelectedDataKeys.Count() == 0) throw new NBoxException(grdList, "Nenhuma entidade selecionada para emitir NERP");

        var selecionados = Convenios
            .Where(x => grdList.SelectedDataKeys.Select(z => z.Value.To<int>()).Contains(x.IdEntidade))
            .ToList();

        db.Nerps.IncluiNerpART(selecionados, NrAno, NrMes, txtNoJustificativa.Text, txtDtVencto.Text.To<DateTime>());

        Redirect("Nerp.aspx?TpNerp=" + TpNerp.ART + "&DtEmissaoIni=" + DateTime.Now.ToString("yyyy-MM-dd") + "&DtEmissaoFim=" + DateTime.Now.ToString("yyyy-MM-dd"));
    }

</script>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <div class="mt-30 c-primary-blue">
        <span>Dados da NERP</span>
    </div>
    <div class="form-div">
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Justificativa</span></div>
                <box:NTextBox runat="server" ID="txtNoJustificativa" TextCapitalize="Upper" MaxLength="100" Required="true" Width="100%" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form" style="max-width: 300px">
                <div><span>Vencimento</span></div>
                <box:NTextBox runat="server" ID="txtDtVencto" MaskType="Date" Required="true" />
            </div>
        </div>
        <div class="sub-form-row" runat="server" id="trErros" visible="false">
            <div class="div-sub-form">
                <div><span>Erros</span></div>
                <span class="field"><asp:Literal runat="server" ID="ltrErros" /></span>
            </div>
        </div>
    </div>

    <box:NToolBar runat="server">
        <box:NButton runat="server" ID="btnSalvar" Icon="ok" Text="Emitir NERP" Theme="Primary" OnClick="btnSalvar_Click" />
    </box:NToolBar>

    <box:NGridView runat="server" ID="grdList" AllowGrouping="true" ItemType="Creasp.Nerp.ConvenioArtDTO" DataKeyNames="IdEntidade" ShowFooter="true">
        <Columns>
            <asp:BoundField DataField="NoGestor" />
            <box:NCheckBoxField />
            <asp:TemplateField HeaderText="Entidade" FooterStyle-HorizontalAlign="Right">
                <ItemTemplate><%# Item.NoEntidade %></ItemTemplate>
                <FooterTemplate>TOTAL</FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Qtd Ind" HeaderStyle-Width="80" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
                <ItemTemplate><%# Item.QtIndicados.ToString("n0") %></ItemTemplate>
                <FooterTemplate><%# Convenios.Sum(x => x.QtIndicados).ToString("n0") %></FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Indicado" HeaderStyle-Width="120" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                <ItemTemplate><%# Item.VrIndicado.ToString("n") %></ItemTemplate>
                <FooterTemplate><%# Convenios.Sum(x => x.VrIndicado).ToString("n") %></FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Rateio" HeaderStyle-Width="120" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                <ItemTemplate><%# Item.VrRateio.ToString("n") %></ItemTemplate>
                <FooterTemplate><%# Convenios.Sum(x => x.VrRateio).ToString("n") %></FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Valor" HeaderStyle-Width="120" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                <ItemTemplate><%# Item.VrTotal.ToString("n") %></ItemTemplate>
                <FooterTemplate><%# Convenios.Sum(x => x.VrTotal).ToString("n") %></FooterTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>O CREANET não retornou nenhum registro para o periodo informado</EmptyDataTemplate>
    </box:NGridView>

</asp:Content>