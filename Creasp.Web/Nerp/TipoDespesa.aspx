﻿<%@ Page Title="Tipo de Despesas" Resource="nerp.tipodesp" Language="C#" %>
<script runat="server">

    private CreaspContext db = new CreaspContext();

    protected override void OnRegister()
    {
        grdList.RegisterPagedDataSource((p) => db.Despesas.ListaTipoDespesa(txtNoPesquisa.Text.To<string>(), optAtivo.Checked ? (bool?)true : optInativo.Checked ? (bool?)false : null).ToPage(p));
    }

    protected override void OnPrepareForm()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        txtDtValidade.SetText(DateTime.Now);
        grdList.DataBind();

        SetDefaultButton(btnPesquisar);
    }

    protected void btnPesquisar_Click(object sender, EventArgs e)
    {
        grdList.DataBind();
    }

    protected void grdList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var data = e.Row.DataItem as IFgAtivo;
            if (!data.FgAtivo) e.Row.ForeColor = System.Drawing.Color.Red;
        }
    }

    protected void btnNovaDespesa_Click(object sender, EventArgs e)
    {
        txtCdTipoDespesa.Text = txtNoTipoDespesa.Text = txtNrCpfAgrupado.Text = "";
        ucContaContabil.LoadContaContabil(null);
        chkAtivo.Checked = true;
        pnlDespesa.Open(btnSalvar, txtNoTipoDespesa);
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        var despesa = new TipoDespesa();

        if(txtCdTipoDespesa.Text.Length > 0)
        {
            despesa = db.SingleById<TipoDespesa>(txtCdTipoDespesa.Text);
        }
        else
        {
            despesa.FgInterno = false;
        }

        despesa.NoTipoDespesa = txtNoTipoDespesa.Text;
        despesa.NrCpfAgrupado = txtNrCpfAgrupado.Text.UnMask();
        despesa.NrContaContabil = ucContaContabil.ContaContabil.NrContaContabil;
        despesa.FgAtivo = chkAtivo.Checked;

        db.Despesas.SalvaTipoDespesa(despesa);

        pnlDespesa.Close();

        grdList.DataBind();
    }

    protected void grdList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "alt")
        {
            var cdTipoDespesa = e.DataKey<string>(sender);
            var despesa = db.Despesas.BuscaTipoDespesa(cdTipoDespesa);

            txtCdTipoDespesa.Text = despesa.CdTipoDespesa;
            txtNoTipoDespesa.Text = despesa.NoTipoDespesa;
            txtNrCpfAgrupado.SetText(despesa.NrCpfAgrupado);
            ucContaContabil.LoadContaContabil(despesa.ContaContabil.IdContaContabil);
            chkAtivo.Checked = despesa.FgAtivo;

            pnlDespesa.Open(btnSalvar, txtNoTipoDespesa);
        }
        else if(e.CommandName == "del")
        {
            var cdTipoDespesa = e.DataKey<string>(sender);
            db.Despesas.ExcluiTipoDespesa(cdTipoDespesa);
            grdList.DataBind();
        }
        else if (e.CommandName == "det")
        {
            var cdTipoDespesa = e.DataKey<string>(sender);
            btnAdicionar.Enabled = true;

            grdValores.DataSource = db.Despesas.ListaValores(cdTipoDespesa);
            grdValores.DataBind();

            pnlValores.Open();
        }
    }

    protected void btnAdicionar_Click(object sender, EventArgs e)
    {
        var list = ListaValoresGrid();
        list.Add(new TipoDespesaValor { CdTipoDespesa = list.First().CdTipoDespesa });
        grdValores.DataSource = list;
        grdValores.DataBind();
        btnAdicionar.Enabled = false;
    }

    protected void btnSalvarValor_Click(object sender, EventArgs e)
    {
        db.Despesas.SalvaValores(ListaValoresGrid());
        ShowInfoBox("Valores atualizados com sucesso");
        pnlValores.Close();
        grdList.DataBind();
    }

    public List<TipoDespesaValor> ListaValoresGrid()
    {
        var list = new List<TipoDespesaValor>();

        for(var i = 0; i < grdValores.Rows.Count; i++)
        {
            var row = grdValores.Rows[i];
            var ctr = row.FindControl("ucPeriodo") as UserControl;

            list.Add(new TipoDespesaValor
            {
                CdTipoDespesa = grdValores.DataKeys[i]["CdTipoDespesa"].ToString(),
                DtIni = ((Periodo)ctr.GetType().GetProperty("Periodo").GetValue(ctr)).Inicio,
                DtFim = ((Periodo)ctr.GetType().GetProperty("Periodo").GetValue(ctr)).Fim,
                VrUnit = (row.FindControl("txtVrUnit") as NTextBox).Text.To<decimal>(0)
            });
        }

        return list;
    }

</script>
<asp:Content runat="server" ContentPlaceHolderID="title">
    
</asp:Content>
<asp:Content runat="server" ContentPlaceHolderID="content">

    <div class="mt-30 c-primary-blue">
        <span>Tipos de Despesa</span>
        <box:NButton runat="server" ID="btnNovaDespesa" Text="Nova despesa" CssClass="right ml-10 new" Theme="Success" OnClick="btnNovaDespesa_Click" />
        <box:NButton runat="server" ID="btnPesquisar" Icon="search" Text="Pesquisar" OnClick="btnPesquisar_Click" CssClass="right"/>
    </div>

    <div class="form-div">
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Validade</span></div>
                <box:NTextBox runat="server" ID="txtDtValidade" MaskType="Date" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Descrição</span></div>
                <box:NTextBox runat="server" ID="txtNoPesquisa" TextCapitalize="Upper" Width="100%" autofocus />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Situação</span></div>
                <box:NRadioButton runat="server" ID="optAtivo" GroupName="fgAtivo" Text="Somente ativas" Checked="true" />
                <box:NRadioButton runat="server" ID="optInativo" GroupName="fgAtivo" Text="Somente inativas" />
                <box:NRadioButton runat="server" ID="optTodas" GroupName="fgAtivo" Text="Todas" />
            </div>
        </div>
    </div>

    <box:NGridView runat="server" ID="grdList" DataKeyNames="CdTipoDespesa" ItemType="Creasp.Nerp.TipoDespesa" OnRowCommand="grdList_RowCommand" OnRowDataBound="grdList_RowDataBound">
        <Columns>
            <asp:TemplateField HeaderText="Descrição" SortExpression="NoTipoDespesa">
                <ItemTemplate>
                    <div><%# Item.NoTipoDespesa %></div>
                    <small><%# Item.CdTipoDespesa %></small>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="NrCpfFmtAgrupado" HeaderText="CPF Agrupador" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="180" HeaderStyle-HorizontalAlign="Center" SortExpression="NrCpfAgrupado" />
            <asp:TemplateField HeaderText="Conta contábil" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="220" HeaderStyle-HorizontalAlign="Center" SortExpression="NrContaContabil">
                <ItemTemplate>
                    <div><%# Item.ContaContabil.NrContaContabilFmt %></div>
                    <small><%# Item.ContaContabil.NoContaContabil %></small>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Vr. Unit" ItemStyle-HorizontalAlign="Right" HeaderStyle-Width="120" HeaderStyle-HorizontalAlign="Right">
                <ItemTemplate>
                    <%# Item.FgInterno ? Item.VrUnit(txtDtValidade.Text.To<DateTime>(DateTime.Now)).ToString("n2") : "-" %>
                </ItemTemplate>
            </asp:TemplateField>
            <box:NButtonField CommandName="det" Icon="th-list" Theme="Info" VisibleExpression="FgInterno" ToolTip="Alterar valores" />
            <box:NButtonField CommandName="alt" Icon="pencil" Theme="Info" ToolTip="Editar" />
            <box:NButtonField CommandName="del" Icon="cancel" Theme="Danger" EnabledExpression="!FgInterno" OnClientClick="return confirm('Confirma excluir este tipo de despesa?');" />
        </Columns>
        <EmptyDataTemplate>Não existem registros para o filtro informado</EmptyDataTemplate>
    </box:NGridView>

    <box:NPopup runat="server" ID="pnlDespesa" Title="Nova despesa" Width="750">
        <div class="form-div">
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Código</span></div>
                    <box:NTextBox runat="server" ID="txtCdTipoDespesa" Width="150" Enabled="false" />
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Descrição</span></div>
                    <box:NTextBox runat="server" ID="txtNoTipoDespesa" Width="100%" TextCapitalize="Upper" Required="true" ValidationGroup="add" />
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>CPF Agrupador</span></div>
                    <box:NTextBox runat="server" ID="txtNrCpfAgrupado" Width="150" MaskType="Custom" MaskFormat="999.999.999-99" Required="true" ValidationGroup="add" />
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Conta contábil</span></div>
                    <uc:ContaContabil runat="server" ID="ucContaContabil" Required="true" ValidationGroup="add" />
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form flex-center">
                    <box:NCheckBox runat="server" ID="chkAtivo" Text="Ativo" CssClass="left"/>
                    <box:NButton runat="server" ID="btnSalvar" Icon="ok" Text="Salvar" OnClick="btnSalvar_Click" ValidationGroup="add" Theme="Primary" CssClass="right" />
                </div>
            </div>
        </div>
    </box:NPopup>

    <box:NPopup runat="server" ID="pnlValores" Title="Alterar valores de despesa">
        <box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnSalvarValor" Icon="ok" Text="Salvar" Theme="Primary" OnClick="btnSalvarValor_Click" />
            <box:NButton runat="server" ID="btnAdicionar" Icon="plus" Text="Adicionar" OnClick="btnAdicionar_Click" />
        </box:NToolBar>

        <box:NGridView runat="server" ID="grdValores" ItemType="Creasp.Nerp.TipoDespesaValor" DataKeyNames="CdTipoDespesa,DtIni">
            <Columns>
                <asp:TemplateField HeaderText="Periodo de validade">
                    <ItemTemplate>
                        <uc:Periodo runat="server" ShowTextBox="true" ID="ucPeriodo" Periodo=<%# Item.GetPeriodo() %> />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Vr. Unitário" HeaderStyle-Width="160" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <box:NTextBox runat="server" ID="txtVrUnit" MaskType="Decimal" Text=<%# Item.VrUnit.ToString("n") %> />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>

        </box:NGridView>
    </box:NPopup>

    <script src="../Content/Scripts/pagination.js"></script>
    <script>paginate()</script>

    <script src="../Content/Scripts/sidebar.js"></script>
    <script>selectButton("nerp-tipDes"); showReload("nerp");</script>

</asp:Content>