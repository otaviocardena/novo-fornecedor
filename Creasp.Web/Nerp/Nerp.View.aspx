﻿<%@ Page Title="NERP" Resource="nerp" Language="C#" %>
<%@ Register Src="Nerp.View.Geral.ascx" TagName="NerpGeral" TagPrefix="uc" %>
<%@ Register Src="Nerp.View.Item.ascx" TagName="NerpItem" TagPrefix="uc" %>
<%@ Register Src="Nerp.View.Documento.ascx" TagName="NerpDocumento" TagPrefix="uc" %>
<%@ Register Src="Nerp.View.Favorecido.ascx" TagName="NerpFavorecido" TagPrefix="uc" %>
<%@ Register Src="Nerp.View.Empenho.ascx" TagName="NerpEmpenho" TagPrefix="uc" %>
<%@ Register Src="Nerp.View.Retencao.ascx" TagName="NerpRetencao" TagPrefix="uc" %>
<%@ Register Src="Nerp.View.Pagamento.ascx" TagName="NerpPagamento" TagPrefix="uc" %>
<%@ Register Src="Nerp.View.Adiantamento.ascx" TagName="NerpAdiantamento" TagPrefix="uc" %>
<%@ Register Src="Nerp.View.Historico.ascx" TagName="NerpHistorico" TagPrefix="uc" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();

    protected int IdNerp => Request.QueryString["IdNerp"].To<int>();

    protected NerpPermissao Permissao { get { return ViewState["Permissao"] as NerpPermissao; } set { ViewState["Permissao"] = value; } }

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO", "ALL.FORNECEDOR"  };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        // A tab de itens pode precisar informar que demais abas devem ser remontadas ao chamar (pois altera dados de uma pra outra)
        Action remontaAbas = () =>
        {
            tabGeral.AutoPostBack = tabDocs.AutoPostBack = tabFavs.AutoPostBack = tabEmpenho.AutoPostBack = tabPagtos.AutoPostBack = true;
        };

        if (Permissao == null)
        {
            Permissao = Request.QueryString["service"] == null ? db.Nerps.PermissaoNerp(IdNerp) : new NerpPermissao();
        }

        ucGeral.OnRegister(db, Permissao, remontaAbas);
        ucItem.OnRegister(db, Permissao, remontaAbas);
        ucDocumento.OnRegister(db, Permissao);
        ucFavorecido.OnRegister(db, Permissao);
        ucEmpenho.OnRegister(db, Permissao, remontaAbas);
        ucPagamento.OnRegister(db, Permissao);
        ucAdiantamento.OnRegister(db, Permissao);
        ucRetencao.OnRegister(db, Permissao);
        ucHistorico.OnRegister(db, Permissao);
    }

    protected override void OnPrepareForm()
    {
        var nerp = db.SingleById<Nerp>(IdNerp);

        PageTitle = "NERP # " + nerp.NrNerpFmt;

        ucGeral.MontaTela();

        // Confome o tipo de NERP, algumas abas podem estar escondidas por não fazer sentido
        tabRetencao.Visible = nerp.TpNerp != TpNerp.Evento;
        tabItems.Visible = nerp.TpNerp == TpNerp.Evento;
        tabDocs.Visible = !(nerp.TpNerp == TpNerp.ART || nerp.TpNerp == TpNerp.CessaoUso);
        tabAdiantamento.Visible = nerp.FgAdiantamento;
        tabPagtos.Visible = !tabAdiantamento.Visible;

        // exibe botão de editar NERP para o caso de NERP Contrato quando em cadastrada
        if ((nerp.TpNerp == TpNerp.ComEmpenho || nerp.TpNerp == TpNerp.SemEmpenho) && nerp.TpSituacao == NerpSituacao.Cadastrada)
        {
            lnkEditar.Visible = true;
            lnkEditar.NavigateUrl =  nerp.TpNerp == TpNerp.ComEmpenho ?
                "Nerp.ComEmpenho.aspx?IdNerp=" + IdNerp :
                "Nerp.SemEmpenho.aspx?IdNerp=" + IdNerp;
        }

        tabFavs.Visible = nerp.TpNerp != TpNerp.Evento;

        this.Audit(nerp, IdNerp);
        this.BackTo("Nerp.aspx");
    }

    protected void tabGeral_TabChanged(object sender, EventArgs e)
    {
        tabGeral.AutoPostBack = false;
        ucGeral.MontaTela();
    }

    protected void tabItems_TabChanged(object sender, EventArgs e)
    {
        tabItems.AutoPostBack = false;
        ucItem.MontaTela();
    }

    protected void tabDocs_TabChanged(object sender, EventArgs e)
    {
        tabDocs.AutoPostBack = false;
        ucDocumento.MontaTela();
    }

    protected void tabFavs_TabChanged(object sender, EventArgs e)
    {
        tabFavs.AutoPostBack = false;
        ucFavorecido.MontaTela();
    }

    protected void tabEmpenho_TabChanged(object sender, EventArgs e)
    {
        tabEmpenho.AutoPostBack = false;
        ucEmpenho.MontaTela();
    }

    protected void tabPagtos_TabChanged(object sender, EventArgs e)
    {
        tabPagtos.AutoPostBack = false;
        ucPagamento.MontaTela();
    }

    protected void tabRetencao_TabChanged(object sender, EventArgs e)
    {
        tabRetencao.AutoPostBack = false;
        ucRetencao.MontaTela();
    }

    protected void tabAdiantamento_TabChanged(object sender, EventArgs e)
    {
        tabAdiantamento.AutoPostBack = false;
        ucAdiantamento.MontaTela();
    }

    protected void tabHistorico_TabChanged(object sender, EventArgs e)
    {
        //tabHistorico.AutoPostBack = false;
        ucHistorico.MontaTela();
    }

</script>
<asp:Content ContentPlaceHolderID="title" runat="server">
    <box:NHyperLink runat="server" ID="lnkEditar" Icon="pencil" Text="Editar" ButtonTheme="Success" Visible="false" />
</asp:Content>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <box:NTab runat="server" ID="tabGeral" Text="Geral" Selected="true" OnTabChanged="tabGeral_TabChanged">
        <uc:NerpGeral runat="server" id="ucGeral" />
    </box:NTab>

    <box:NTab runat="server" ID="tabItems" Text="Despesas" AutoPostBack="true" OnTabChanged="tabItems_TabChanged">
        <uc:NerpItem runat="server" id="ucItem" />
    </box:NTab>

    <box:NTab runat="server" ID="tabDocs" Text="Documentos" AutoPostBack="true" OnTabChanged="tabDocs_TabChanged">
        <uc:NerpDocumento runat="server" id="ucDocumento" />
    </box:NTab>

    <box:NTab runat="server" ID="tabFavs" Text="Favorecidos" AutoPostBack="true" OnTabChanged="tabFavs_TabChanged">
        <uc:NerpFavorecido runat="server" id="ucFavorecido" />
    </box:NTab>

    <box:NTab runat="server" ID="tabEmpenho" Text="Empenhos" AutoPostBack="true" OnTabChanged="tabEmpenho_TabChanged">
        <uc:NerpEmpenho runat="server" ID="ucEmpenho" />
    </box:NTab>

    <box:NTab runat="server" ID="tabRetencao" Text="Retenções" AutoPostBack="true" OnTabChanged="tabRetencao_TabChanged">
        <uc:NerpRetencao runat="server" id="ucRetencao" />
    </box:NTab>

    <box:NTab runat="server" ID="tabPagtos" Text="Pagamentos" AutoPostBack="true" OnTabChanged="tabPagtos_TabChanged">
        <uc:NerpPagamento runat="server" ID="ucPagamento" />
    </box:NTab>

    <box:NTab runat="server" ID="tabAdiantamento" Text="Adiantamentos" AutoPostBack="true" OnTabChanged="tabAdiantamento_TabChanged">
        <uc:NerpAdiantamento runat="server" ID="ucAdiantamento" />
    </box:NTab>

    <box:NTab runat="server" ID="tabHistorico" Text="Histórico" AutoPostBack="true" OnTabChanged="tabHistorico_TabChanged">
        <uc:NerpHistorico runat="server" ID="ucHistorico" />
    </box:NTab>
</asp:Content>