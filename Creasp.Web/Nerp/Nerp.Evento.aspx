﻿<%@ Page Title="Evento" Resource="nerp.evento" Language="C#" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();

    protected List<NerpParticipanteDTO> Participantes { get { return ViewState["Participantes"] as List<NerpParticipanteDTO>; } set { ViewState["Participantes"] = value; } }

    protected int IdPessoa { get { return ViewState["IdPessoa"].To<int>(0); } set { ViewState["IdPessoa"] = value; } }

    protected int IdEvento => Request.QueryString["IdEvento"].To<int>();

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        grdList.RegisterDataSource(() => Participantes);

        RegisterRestorePageState(() => grdList.DataBind());
    }

    protected override void OnPrepareForm()
    {
        var e = db.Eventos.BuscaEvento(IdEvento, false);

        lblDtEvento.Text = e.DtEvento.ToString("d");
        lblHrEvento.Text = e.DtEvento.ToString("HH:mm") + " até " +
            (e.DtEvento.Date != e.DtEventoFim.Date ? e.DtEventoFim.ToString("d") + " - " : "") +
            e.DtEventoFim.ToString("HH:mm");
        txtDtEvento.Text = lblDtEvento.Text + " - " + lblHrEvento.Text;

        lblNoLocal.Text =  e.NoLocal + (e.Cep == null ? "" : (" - " + e.Cep.NoCidade + " / " + e.Cep.CdUf));
        txtNoNerp.Text = e.NoEvento;
        txtNoJustificativa.Text = e.NoEvento + " - " + e.NoLocal + " - " + e.DtEvento.ToString("d") +
            (e.DtEvento.Date == e.DtEventoFim.Date ? "" : " até " + e.DtEventoFim.ToString("d"));

        cmbDespesa.Bind(db.Despesas.ListaTipoDespesa().Select(x => new {  x.CdTipoDespesa, x.NoTipoDespesa }));

        var erros = new StringBuilder();

        Participantes = db.Nerps.ListaParticipantesNerp(IdEvento, erros);

        if(erros.Length > 0)
        {
            ShowErrorBox(erros[0].ToString());
        }

        grdList.DataBind();

        var funcionarios = new FuncionarioService(new CreaspContext());
        var func = funcionarios.BuscaFuncionarioHierarquia(Auth.Current<CreaspUser>().Login);
        ucChefe.LoadPessoa(func.Chefe.Pessoa.IdPessoa);
        ucSuperintendente.LoadPessoa(func.Superintendente.Pessoa.IdPessoa);

        if (User.IsInRole("nerp.altautoriza") == false)
        {
            ucChefe.Enabled = false;
            ucSuperintendente.Enabled = false;
        }

        this.BackTo("Nerp.aspx");
    }

    protected void btnEditar_Click(object sender, EventArgs e)
    {
        var descricao = txtNoNerp.Text;
        var local = lblNoLocal.Text;

        try {
            db.Eventos.AlteraEventoBasico(IdEvento, descricao, local);
            OnPrepareForm();
            ShowInfoBox("Atualizado com sucesso!");
        }
        catch
        {
            ShowErrorBox("Houve algum problema ao alterar o evento");
        }

    }

    protected void btnAtualizar_Click(object sender, EventArgs e)
    {
        OnPrepareForm();
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        if(grdList.SelectedDataKeys.Count() == 0) throw new NBoxException(grdList, "Nenhum participante selecionado para emitir NERP");

        var selectionados = Participantes
            .Where(x => grdList.SelectedDataKeys.Select(z => z.Value.To<int>()).Contains(x.IdPessoa))
            .ToList();

        var idNerp = db.Nerps.IncluiNerpEvento(IdEvento, txtNoNerp.Text, txtNoJustificativa.Text, ucChefe.IdPessoa, ucSuperintendente.IdPessoa, selectionados);

        pnlOk.Open();
    }

    protected void grdList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if(e.CommandName == "add")
        {
            cmbDespesa.SelectedValue = "";
            cmbDespesa_SelectedIndexChanged(null, null);
            IdPessoa = e.DataKey<int>(sender);
            pnlDespesa.Open(btnSalvaDespesa, cmbDespesa);
        }
    }

    protected void txtQtKm_TextChanged(object sender, EventArgs e)
    {
        AtualizaParticipantes();
        grdList.DataBind();
    }

    protected void btnDelKm_Command(object sender, CommandEventArgs e)
    {
        var index = e.CommandArgument.To<int>();
        (grdList.Rows[index].FindControl("txtQtKm") as NTextBox).Text = "";
        AtualizaParticipantes();
        grdList.DataBind();
    }

    protected void btnDelDiaria_Command(object sender, CommandEventArgs e)
    {
        var index = e.CommandArgument.To<int>();
        (grdList.Rows[index].FindControl("txtQtDiaria") as NTextBox).Text = "";
        AtualizaParticipantes();
        grdList.DataBind();
    }

    private void AtualizaParticipantes()
    {
        for(var i = 0; i < Participantes.Count; i++)
        {
            var qtKm = (grdList.Rows[i].FindControl("txtQtKm") as NTextBox).Text.To<decimal>(0);
            var qtDiaria = (grdList.Rows[i].FindControl("txtQtDiaria") as NTextBox).Text.To<decimal>(0);
            var item = Participantes[i];

            if (item.Km != null)
            {
                if(qtKm == 0)
                {
                    item.Despesas.Remove(item.Km);
                }
                else
                {
                    item.Km.QtUnit = qtKm;
                }
            }

            if (item.Diaria != null)
            {
                if(qtDiaria == 0)
                {
                    item.Despesas.Remove(item.Diaria);
                }
                else
                {
                    item.Diaria.QtUnit = qtDiaria;
                }
            }
        }
    }

    protected void cmbDespesa_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(cmbDespesa.HasValue())
        {
            var tipoDespesa = db.Despesas.BuscaTipoDespesa(cmbDespesa.SelectedValue);
            txtVrUnitDespesa.SetText(tipoDespesa.VrUnit(lblDtEvento.Text.To<DateTime>().Date));
            lblNoContaContabil.Text = tipoDespesa.ContaContabil.NoContaContabilFmt;
        }
        else
        {
            txtVrUnitDespesa.SetText(0);
            lblNoContaContabil.Text = "";
        }
        txtQtUnitDespesa.SetText(1);
        txtQtUnitDespesa_TextChanged(null, null);
    }

    protected void txtQtUnitDespesa_TextChanged(object sender, EventArgs e)
    {
        lblVrTotalDespesa.Text = (txtQtUnitDespesa.Text.To<decimal>(0) * txtVrUnitDespesa.Text.To<decimal>(0)).ToString("n2");
    }

    protected void btnSalvaDespesa_Click(object sender, EventArgs e)
    {
        var cdTipoDespesa = cmbDespesa.SelectedValue;

        var selectionados = Participantes
            .Where(x => grdList.SelectedDataKeys.Select(z => z.Value.To<int>()).Contains(x.IdPessoa))
            .ToList();

        // adiciona a pessoa que foi clicado no "+" (se ainda nao estiver)
        if (selectionados.Any(x => x.IdPessoa == IdPessoa) == false)
        {
            selectionados.Add(Participantes.Where(x => x.IdPessoa == IdPessoa).Single());
        }

        try
        {
            foreach(var p in selectionados)
            {
                SalvaDespesa(p, cdTipoDespesa);
            }
        }
        catch(Exception ex)
        {
            ShowErrorBox(ex.Message);
        }

        grdList.DataBind();
        pnlDespesa.Close();
    }

    protected void SalvaDespesa(NerpParticipanteDTO p, string cdTipoDespesa)
    {
        if (p.Km != null && cdTipoDespesa.StartsWith("KM-")) throw new NBoxException("Para incluir uma nova locomoção é necessário primeiro limpar o campo de locomoção na tela anterior.");
        if (p.Diaria != null && cdTipoDespesa.StartsWith("DIARIA-")) throw new NBoxException("Para incluir uma nova diária é necessário primeiro limpar o campo de diária na tela anterior.");
        if (p.Despesas.FirstOrDefault(x => x.CdTipoDespesa == cdTipoDespesa) != null) throw new NBoxException("Já existe esta despesa cadastrada");

        var despesa = new NerpDespesaDTO { CdTipoDespesa = cdTipoDespesa, NoTipoDespesa = cmbDespesa.SelectedItem.Text, QtUnit = txtQtUnitDespesa.Text.To<decimal>(), VrUnit = txtVrUnitDespesa.Text.To<decimal>() };

        if (despesa.VrTotal == 0) throw new NBoxException("O valor total deve ser maior que zero");

        p.Despesas.Add(despesa);
    }

    protected void rptOutros_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        var idPessoa = e.CommandName.To<int>();
        var cdTipoDespesa = e.CommandArgument.ToString();
        var part = Participantes.Single(x => x.IdPessoa == idPessoa);

        part.Despesas.Remove(part.Despesas.FirstOrDefault(x => x.CdTipoDespesa == cdTipoDespesa));

        grdList.DataBind();
    }

    protected void grdList_Sorting(object sender, GridViewSortEventArgs e)
    {
        AtualizaParticipantes();

        if (e.SortExpression == "PESSOA") Participantes = Participantes.OrderBy(x => x.NoCentroCusto).ThenBy(x => x.NoPessoa).ToList();
        if (e.SortExpression == "KM") Participantes = Participantes.OrderBy(x => x.NoCentroCusto).ThenBy(x => x.Km?.QtUnit).ToList();
        if (e.SortExpression == "DIARIA") Participantes = Participantes.OrderBy(x => x.NoCentroCusto).ThenBy(x => x.Diaria?.QtUnit).ToList();

        grdList.DataBind();
    }

    private List<Agrupador> _agrupador = new List<Agrupador>();

    protected void btnPrevisao_Click(object sender, EventArgs e)
    {
        if(grdList.SelectedDataKeys.Count() == 0) throw new NBoxException(grdList, "Nenhum participante selecionado para cacular NERP");

        var selectionados = Participantes
            .Where(x => grdList.SelectedDataKeys.Select(z => z.Value.To<int>()).Contains(x.IdPessoa))
            .ToList();

        _agrupador = new List<Agrupador>();

        foreach(var centroCusto in selectionados.GroupBy(x => x.NoCentroCusto))
        {
            _agrupador.AddRange(selectionados
                .Where(x => x.NoCentroCusto == centroCusto.Key)
                .SelectMany(x => x.Despesas)
                .GroupBy(x => x.CdTipoDespesa)
                .Select(x => new Agrupador
                {
                    NoDespesa = x.First().NoTipoDespesa +
                        " (" + db.Despesas.BuscaTipoDespesa(x.First().CdTipoDespesa).ContaContabil.NrContaContabilFmt + ")",
                    NoCentroCusto = centroCusto.Key,
                    VrTotal = x.Sum(d => d.VrTotal)
                })
                .Where(x => x.VrTotal > 0));
        }

        grdAgrupador.DataSource = _agrupador;
        grdAgrupador.DataBind();
        pnlAgrupador.Open();
    }

    public class Agrupador
    {
        public string NoDespesa { get; set; }
        public string NoCentroCusto { get; set; }
        public decimal VrTotal { get; set; }
    }

    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        Redirect("~/nerp/Nerp.Evento.Default.aspx");
    }

    protected void btnIncio_Click(object sender, EventArgs e)
    {
        pnlOk.Close();
        Redirect("~Default.aspx");
    }

    protected void btnNvEvento_Click(object sender, EventArgs e)
    {
        pnlOk.Close();
        Redirect("~/nerp/Nerp.Evento.Default.aspx");
    }
</script>
<asp:Content ContentPlaceHolderID="content" runat="server">
    <script> 
        try {
            //Create class in div
            var x = document.querySelector(".n-grid-footer").parentNode;
            x.setAttribute('id', 'table-wrapper');
        }
        catch{

        }

        try {

            //Remove text drop-menu
            var registros = document.querySelector('.n-grid-paging');
            var vet = [];
            let spanSelected = null;

            for (let i = 0; i <= 3; i++) vet.push(registros.getElementsByTagName('a')[i]);

            spanSelected = registros.getElementsByTagName('span')[0];

            for (let i = 0; i < vet.length; i++) {
                if (Number(vet[i].innerText) > Number(spanSelected.innerText)) {
                    vet.splice(i, 0, spanSelected);
                    break;
                } else if (Number(spanSelected.innerText) > Number(vet[vet.length - 1].innerText)) {
                    vet.splice(vet.length, 0, spanSelected);
                    break;
                }
            }

            var p = document.createElement('p');
            p.setAttribute('class', 'm-view');
            p.innerText = "VISUALIZAR";

            vet.splice(0, 0, p);

            registros.innerHTML = ""
            for (let i = 0; i <= 5; i++) registros.appendChild(vet[i])

            //Calc count of pages
            let totalReg = Number(document.querySelector('.n-grid-total').innerText.split(':')[1]);
            let pageCurrent = Number(document.querySelector('.n-grid-pager tr td span').innerText);
            var countTotalPages = Math.round(totalReg / Number(spanSelected.innerText));

            document.querySelector('.n-grid-pager td table').setAttribute("style", "display: none");
            let aBack = () => {
                if (pageCurrent <= 1) {
                    return `<span><i class="fas fa-chevron-left c-arrow-not-selected"></i></span>`;
                }

                return `<a href="javascript: __doPostBack('ctl00$content$grdList', 'Page$${pageCurrent - 1}')"><i class="fas fa-chevron-left select-true"></i></a>`;
            }

            let aNext = () => {

                if (pageCurrent >= countTotalPages) {
                    return `<span><i class="fas fa-chevron-right c-arrow-not-selected"></i></span>`;
                }

                return `<a href="javascript: __doPostBack('ctl00$content$grdList', 'Page$${pageCurrent + 1}')"><i class="fas fa-chevron-right select-true"></i></a>`;
            }


            let divPager = document.createElement('div');
            divPager.setAttribute('class', 'n-paginator');
            divPager.innerHTML = `<span>${pageCurrent} - ${countTotalPages} de ${totalReg}</span>
                                    ${aBack()}
                                    ${aNext()}`;
            let tablePaginator = document.querySelector('.n-grid-pager td');
            tablePaginator.appendChild(divPager);

        }
        catch {

        }


        try {
            //Add icon arrow down
            var gridTotal = document.querySelector('.n-grid-total');

            var i = document.createElement('i');
            i.setAttribute('class', 'fas fa-sort-amount-down-alt ml-8 c-primary-yellow');
            i.innerHTML = '&nbsp;&nbsp;&nbsp;';

            gridTotal.appendChild(i);
        } catch {
        }
    </script>

    <div class="mt-30 d-flex-space-b">
        <div>
            <span class="c-primary-blue">Evento | Dados da NERP</span>
        </div>
        <div class="d-flex actions-icon-wrapper">
            <asp:LinkButton runat="server" OnClick="btnAtualizar_Click" CssClass="d-flex-colum actions-icon">
                <i class="fas fa-redo-alt c-primary-yellow" style="font-size: 15px;"></i>
                <span>Atualizar</span>
            </asp:LinkButton>
            <asp:LinkButton runat="server" onclick="btnEditar_Click" class="d-flex-colum actions-icon">
                <i class="far fa-edit c-primary-yellow" style="font-size: 15px;"></i>
                <span>Editar</span>
            </asp:LinkButton>
        </div>
    </div>

    <div class="form-div">
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Data do evento</span></div>
                <box:NTextBox runat="server" ID="txtDtEvento" MaxLength="100" Enabled="false"/>
                <box:NLabel runat="server" ID="lblDtEvento"  Visible="false"/>
                <box:NLabel runat="server" ID="lblHrEvento" Visible="false"/>
            </div>
            <div class="div-sub-form">
                <div><span>Local</span></div>
                <box:NTextBox runat="server" ID="lblNoLocal" MaxLength="100" Required="true"  />
            </div>
            <div class="div-sub-form">
                <div><span>Descrição</span></div>
                <box:NTextBox runat="server" ID="txtNoNerp" TextCapitalize="Upper" MaxLength="100" Required="true" Width="100%" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Justificativa</span></div>
                <box:NTextBox runat="server" ID="txtNoJustificativa" TextCapitalize="Upper" MaxLength="100" Required="true" Width="100%" />
            </div>
        </div>
    </div>

     <div class="mt-30 c-primary-blue">
        <span>Autorizações</span>
    </div>

    <div class="form-div">
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Chefe imediato</span></div>
                <uc:Pessoa runat="server" IsFuncionario="true" ID="ucChefe" Required="true" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Superintendente</span></div>
                <uc:Pessoa runat="server" IsFuncionario="true" ID="ucSuperintendente" Required="true" />
            </div>
        </div>
    </div>

    <box:NGridView runat="server" ID="grdList" CssClass="grid-evento" AllowSorting="true" AllowGrouping="true" ItemType="Creasp.Nerp.NerpParticipanteDTO" DataKeyNames="IdPessoa" OnRowCommand="grdList_RowCommand" OnSorting="grdList_Sorting">
        <Columns>
            <asp:BoundField DataField="NoCentroCusto" ItemStyle-CssClass="c-primary-blue"/>
            <box:NCheckBoxField />
            <asp:TemplateField>
                <HeaderTemplate>
                    <div class="left-right">
                        <asp:LinkButton runat="server" Text="Pessoas" CommandName="Sort" CommandArgument="PESSOA" />
                        <input type="text" class="n-input margin-left" style="width: 100%" placeholder="Filtrar" runat="server" onkeyup="GridFilter(this);" />
                    </div>
                </HeaderTemplate>
                <ItemTemplate>
                    <span style="color:#054589"><%# Item.NoPessoa %></span>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Km" HeaderStyle-Width="215" SortExpression="KM" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <box:NTextBox runat="server" ID="txtQtKm" MaskType="Integer" Text=<%# Item.Km?.QtUnit.ToString("n0").Replace(".", "") %> Width="45" Enabled=<%# Item.Km != null %> AutoPostBack="true" OnTextChanged="txtQtKm_TextChanged" CssClass="t-center input-grid" ToolTip=<%# Item.Km?.NoInfo %> />
                    <span style="display: inline-block; width: 40px; text-align: right"><span style="color:#cbd5dd">x</span> <%# Item.Km?.VrUnit.ToString("n2") ?? "0,00" %></span> =
                    <span class="field t-right" style="width: 70px;"><%# (Item.Km?.VrTotal ?? 0).ToString("n2")  %></span>
                    <box:NLinkButton runat="server" ID="btnDelKm" Icon="cancel" Enabled=<%# Item.Km != null %> ToolTip="Limpar quilometragem" CommandName="DelKm" OnCommand="btnDelKm_Command" CommandArgument=<%# Container.DataItemIndex %> Theme="Danger" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Diárias" HeaderStyle-Width="240" SortExpression="DIARIA" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate>
                    <box:NTextBox runat="server" ID="txtQtDiaria" MaskType="Decimal" MaskFormat="1" Text=<%# Item.Diaria?.QtUnit.ToString("n1") %> Width="40" Enabled=<%# Item.Diaria != null %> AutoPostBack="true" OnTextChanged="txtQtKm_TextChanged" CssClass="t-center input-grid" ToolTip=<%# Item.Diaria?.NoInfo %> />
                    <span style="display: inline-block; width: 70px; text-align: right"><span style="color:#cbd5dd">x</span> <%# Item.Diaria?.VrUnit.ToString("n2") ?? "0,00" %></span> =
                    <span class="field t-right" style="width: 70px;"><%# (Item.Diaria?.VrTotal ?? 0).ToString("n2")  %></span>
                    <box:NLinkButton runat="server" ID="btnDelDiaria" Icon="cancel" Enabled=<%# Item.Diaria != null %> ToolTip="Limpar diária" CommandName="DelDiaria" OnCommand="btnDelDiaria_Command" CommandArgument=<%# Container.DataItemIndex %> Theme="Danger" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Outras despesas" HeaderStyle-Width="140" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right">
                <ItemTemplate>
                    <asp:Repeater runat="server" ID="rptOutros" DataSource=<%# Item.Outros %> ItemType="Creasp.Nerp.NerpDespesaDTO" OnItemCommand="rptOutros_ItemCommand">
                        <ItemTemplate>
                            <span class="field t-right" style="width: 80px;" title="<%# Item.NoTipoDespesa %>"><%# Item.VrTotal.ToString("n2") %></span>
                            <box:NLinkButton runat="server" ID="btnDelDespesa" Icon="cancel" ToolTip="Excluir" CommandName=<%# (((IDataItemContainer)Container.Parent.Parent.Parent).DataItem as NerpParticipanteDTO).IdPessoa %> CommandArgument=<%# Item.CdTipoDespesa %> Theme="Danger" OnClientClick="return confirm('Confirma excluir esta despesa?');" />
                        </ItemTemplate>
                    </asp:Repeater>
                </ItemTemplate>
            </asp:TemplateField>
            <box:NButtonField Icon="plus" CommandName="add" Theme="Info" />
            <asp:BoundField DataField="VrTotal" HeaderText="Total" DataFormatString="{0:n2}" HeaderStyle-Width="100" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
        </Columns>
        <EmptyDataTemplate>Nenhum participante neste evento ou sem registro de presença. <a href="/siplen/Evento.Presenca.aspx?IdEvento=<%= Request.QueryString["IdEvento"] %>">Clique aqui para confirmar o registro de presença dos participantes deste evento</a></EmptyDataTemplate>
    </box:NGridView>

    <box:NToolBar runat="server">
        <box:NButton runat="server" ID="btnCancelar" Text="Cancelar" Theme="None" OnClick="btnCancelar_Click" Width="150"/>
        <box:NButton runat="server" ID="btnSalvar" Text="Emitir NERP" Theme="Primary" OnClick="btnSalvar_Click" Width="150" />
        <box:NButton runat="server" ID="btnPrevisao" Text="Calcular previsão" OnClick="btnPrevisao_Click" Width="150"/>
    </box:NToolBar>

    <box:NPopup runat="server" ID="pnlAgrupador" Width="850" Title="Previsão de gastos">
        <box:NGridView runat="server" ID="grdAgrupador" ShowFooter="true" ShowTotal="false" ShowHeader="false" AllowGrouping="true">
            <Columns>
                <asp:BoundField DataField="NoCentroCusto" />
                <asp:BoundField DataField="NoDespesa" HeaderText="Despesa" />
                <asp:TemplateField HeaderText="Total" HeaderStyle-Width="120" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                    <ItemTemplate><%# Eval("VrTotal", "{0:n}") %></ItemTemplate>
                    <FooterTemplate><%# _agrupador.Sum(x => x.VrTotal).ToString("n")  %> </FooterTemplate>
                </asp:TemplateField>
            </Columns>
        </box:NGridView>
    </box:NPopup>

    <box:NPopup runat="server" ID="pnlDespesa" Title="Adicionar outras despesas" Width="750">
        <box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnSalvaDespesa" Icon="ok" Text="Adicionar" Theme="Primary" OnClick="btnSalvaDespesa_Click" ValidationGroup="add" />
        </box:NToolBar>
        <table class="form">
            <tr>
                <td>Despesa</td>
                <td>
                    <box:NDropDownList runat="server" ID="cmbDespesa" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="cmbDespesa_SelectedIndexChanged" Required="true" ValidationGroup="add" />
                </td>
            </tr>
            <tr>
                <td>Conta contábil</td>
                <td>
                    <box:NLabel runat="server" ID="lblNoContaContabil" />
                </td>
            </tr>
            <tr>
                <td>Quantidade</td>
                <td>
                    <box:NTextBox runat="server" ID="txtQtUnitDespesa" MaskType="Decimal" MaskFormat="1" Text="1,0" Width="120" AutoPostBack="true" OnTextChanged="txtQtUnitDespesa_TextChanged" Required="true" ValidationGroup="add" />
                </td>
            </tr>
            <tr>
                <td>Valor unitário</td>
                <td>
                    <box:NTextBox runat="server" ID="txtVrUnitDespesa" MaskType="Decimal" Text="0,00" Width="120" AutoPostBack="true" OnTextChanged="txtQtUnitDespesa_TextChanged" Required="true" ValidationGroup="add" />
                </td>
            </tr>
            <tr>
                <td>Total</td>
                <td>
                    <box:NLabel CssClass="field t-bold t-right" runat="server" ID="lblVrTotalDespesa" Text="0,00" Width="120" />
                </td>
            </tr>
        </table>
        <div class="help space-top">Para substituir uma diária ou quilometragem é preciso excluir a diária/quilometragem na tela anterior</div>
    </box:NPopup>

    <div class="popup-secundary">
        <box:NPopup runat="server" ID="pnlOk" Title="Tributo/Retenção enviado" Width="550">
            <div class="popup-wrapper" style="height:415px">
                <div class="mb-50"><a class="n-popup-close" href="javascript:;" onclick="__doPostBack('ctl00$content$pnlOk','close')"></a></div>
                <i class="far fa-check-circle"></i>
                <p>NERP cadastrada com sucesso!</p>
                <span style="color:black;margin-top: -35px;margin-bottom: 60px;">Deseja continuar cadastrando?</span>
                <div class="d-flex">
                    <div class="link-button-default" style="width: 205px; margin-right:15px">
                        <box:NLinkButton runat="server" ID="btnIncio" Text="Tela inicial" OnClick="btnIncio_Click" />
                    </div>
                    <div class="link-button-primary" style="width: 245px; margin-left:15px">
                        <box:NLinkButton runat="server" ID="btnNvEvento" Theme="Primary" Text="Novo Evento" OnClick="btnNvEvento_Click" />
                    </div>
                </div>
            </div>
        </box:NPopup>
    </div>

</asp:Content>