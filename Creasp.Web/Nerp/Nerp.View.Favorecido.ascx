﻿<%@ Control Language="C#" %>
<script runat="server">

    protected CreaspContext db;

    protected int IdNerp => Request.QueryString["IdNerp"].To<int>();

    protected bool validaSiscont = false;
    protected bool validaAnuidade = false;

    public void OnRegister(CreaspContext db, NerpPermissao permissao)
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO", "ALL.FORNECEDOR" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Page.Redirect("~/Login.aspx", false, true);
        }

        if (Auth.Current<CreaspUser>().Fornecedor)
        {
            btnValidaAnuidade.Visible = false;
            btnValidaSiscont.Visible = false;
            btnValidaSiscont.Visible = false;
        }

        this.db = db;
        grdFavs.RegisterDataSource(() => db.Nerps.ListaFavorecidos(IdNerp, validaSiscont, validaAnuidade));
    }

    public void MontaTela()
    {
        grdFavs.DataBind();
    }

    protected void btnValidaSiscont_Click(object sender, EventArgs e)
    {
        validaSiscont = true;
        grdFavs.DataBind();
    }

    protected void btnValidaAnuidade_Click(object sender, EventArgs e)
    {
        validaAnuidade = true;
        grdFavs.DataBind();
    }

    protected void grdFavs_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if(e.CommandName == "alt" && !Auth.Current<CreaspUser>().Fornecedor)
        {
            var idPessoa = e.DataKey<int>(sender);
            ucPessoa.Visible = true;
            ucPessoa.EditPessoa(idPessoa);
        }
    }

</script>
<box:NToolBar runat="server">
    <box:NButton runat="server" ID="btnValidaAnuidade" Icon="ok" Text="Consultar anuidade" Theme="Default" OnClick="btnValidaAnuidade_Click" />
    <box:NButton runat="server" ID="btnValidaSiscont" Icon="ok" Text="Consultar no SISCONT" CssClass="btn-executa" Theme="Default" OnClick="btnValidaSiscont_Click" />
</box:NToolBar>

<div class="container margin-top"><uc:Pessoa runat="server" ID="ucPessoa" EditOnly="true" Visible="false" /></div>

<div class="help">Para empenhos e pagamentos é necessário que os favorecidos tenham cadastro com dados bancários no SISCONT.</div>

<box:NGridView runat="server" ID="grdFavs" ItemType="Creasp.Core.Pessoa" DataKeyNames="IdPessoa" OnRowCommand="grdFavs_RowCommand">
    <Columns>
        <asp:BoundField DataField="NrCpfFmt" HeaderText="CPF/CNPJ" HeaderStyle-Width="160" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
        <asp:TemplateField HeaderText="Nome">
            <ItemTemplate>
                <%# Item.NoPessoa %>
                <box:NLabel runat="server" Text=<%# Item.Mensagem %> Theme=<%# Item.Mensagem == "OK" ? ThemeStyle.Success : ThemeStyle.Danger %> Visible=<%# Item.Mensagem != null %> />
            </ItemTemplate>
        </asp:TemplateField>
        <box:NButtonField Icon="pencil" Theme="Info" ToolTip="Editar cadastro" CommandName="alt" />
    </Columns>
</box:NGridView>