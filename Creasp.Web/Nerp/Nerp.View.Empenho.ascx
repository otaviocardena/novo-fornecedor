﻿<%@ Control Language="C#" %>
<script runat="server">

    protected CreaspContext db;

    protected int IdNerp => Request.QueryString["IdNerp"].To<int>();

    protected Action remontaAbas;

    public void OnRegister(CreaspContext db, NerpPermissao permissao, Action remontaAbas)
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO", "ALL.FORNECEDOR"};
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Page.Redirect("~/Login.aspx", false, true);
        }

        if (Auth.Current<CreaspUser>().Fornecedor)
        {
            btnDesfazSolic.Visible = false;
            btnSolicEmp.Visible = false;
            btnVerifSiscont.Visible = false;
        }

        this.db = db;
        this.remontaAbas = remontaAbas;

        grdEmpenho.RegisterDataSource(() => db.Nerps.ListaEmpenhos(IdNerp));

        Page.RegisterResource(permissao.FgSolicEmpenho).Disabled(btnSolicEmp, btnDesfazSolic);
    }

    public void MontaTela()
    {
        grdEmpenho.DataBind();
    }

    protected void btnSolicEmp_Click(object sender, EventArgs e)
    {
        try
        {
            if (db.Nerps.SolicEmp(IdNerp) > 0)
            {
                grdEmpenho.DataBind();
                Page.ShowInfoBox("Solicitação de empenho realizada com sucesso. Aprove os empenhos no sistema SISCONT.NET");
                remontaAbas();
            }
            else
            {
                Page.ShowErrorBox("Não existe nenhum valor a ser empenhado");
            }
        }
        catch(NBoxException ex)
        {
            Page.ShowErrorBox(ex.Message);
            grdEmpenho.DataBind();
        }
    }

    protected void btnVerifSiscont_Click(object sender, EventArgs e)
    {
        var ret = db.Nerps.VerificaSiscontEmpenho(db.SingleById<Nerp>(IdNerp));

        if (ret)
        {
            Page.ShowInfoBox("Empenho(s) aprovado(s) no SISCONT");
            grdEmpenho.DataBind();
        }
        else
        {
            Page.ShowErrorBox("Nenhum novo empenho realizado no SISCONT");
        }
    }

    protected void btnDesfazSolic_Click(object sender, EventArgs e)
    {
        db.Nerps.DesfazSolicEmp(IdNerp);

        Page.ShowInfoBox("Operação de desfazer realizada com sucesso.");

        grdEmpenho.DataBind();
    }

</script>
<box:NToolBar runat="server">
    <box:NButton runat="server" ID="btnSolicEmp" Icon="ok" Text="Solicitar empenho" Theme="Primary" OnClick="btnSolicEmp_Click" />
    <box:NButton runat="server" ID="btnDesfazSolic" Icon="cancel" Text="Desfazer solicitação" Theme="Default" OnClick="btnDesfazSolic_Click" OnClientClick="return confirm('ATENÇÃO: Esta operação não desfaz as solicitações de empenho no sistema SISCONT. Ao clicar em desfazer o usuário deve ter certeza que as solicitações já foram canceladas no SISCONT para evitar duplicidade. Esta operação poderá ser auditada posteriormente.');" />
    <box:NButton runat="server" ID="btnVerifSiscont" Icon="menu" Text="Verificar SISCONT" CssClass="right" Theme="Default" OnClick="btnVerifSiscont_Click" />
</box:NToolBar>

<box:NGridView runat="server" ID="grdEmpenho" ItemType="Creasp.Nerp.NerpEmpenho">
    <Columns>
        <asp:BoundField DataField="NoEmpenho" HeaderText="Empenho" HeaderStyle-Width="120" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
        <asp:TemplateField HeaderText="Credor">
            <ItemTemplate>
                <%# Item.Pessoa.NoPessoa %>
                <div><small><%# Item.Pessoa.NrCpfFmt %></small></div>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:BoundField DataField="ContaContabil.NoContaContabilFmt" HeaderText="Conta contábil" HeaderStyle-Width="380" />
        <asp:BoundField DataField="VrEmpenho" HeaderText="Valor" DataFormatString="{0:n2}" HeaderStyle-Width="100" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
    </Columns>
    <EmptyDataTemplate>Nenhum empenho solicitado</EmptyDataTemplate>
</box:NGridView>