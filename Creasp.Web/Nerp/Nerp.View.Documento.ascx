﻿<%@ Control Language="C#" %>
<script runat="server">

    protected CreaspContext db;

    protected int IdNerp => Request.QueryString["IdNerp"].To<int>();

    protected int? IdEvento { get { return ViewState["IdEvento"].To<int?>(); } set { ViewState["IdEvento"] = value; } }

    public void OnRegister(CreaspContext db, NerpPermissao permissao)
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO", "ALL.FORNECEDOR" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Page.Redirect("~/Login.aspx", false, true);
        }

        if (Auth.Current<CreaspUser>().Fornecedor)
        {
            btnMultiUpload.Visible = false;
            btnDocsEvento.Visible = false;
            uplDocs.Visible = false;
        }

        this.db = db;
        grdDocs.RegisterPagedDataSource((p) => db.Nerps.ListaDocumentosItem(p, IdNerp, txtNoPessoa.Text.To<string>(), txtNoDespesa.Text.To<string>()));

        Page.RegisterResource(permissao.FgDocumentar).GridColumns(grdDocs, "A", "del").Hidden(btnMultiUpload);

        Page.SetDefaultButton(btnFiltrar);
    }

    public void MontaTela()
    {
        grdDocs.DataBind();

        var nerp = db.SingleById<Nerp>(IdNerp);

        btnDocsEvento.Visible = nerp.IdEvento.HasValue;
        IdEvento = nerp.IdEvento;
    }

    protected void btnFiltrar_Click(object sender, EventArgs e)
    {
        grdDocs.DataBind();
    }

    protected void btnLimpar_Click(object sender, EventArgs e)
    {
        txtNoPessoa.Text = txtNoDespesa.Text = "";
        grdDocs.DataBind();
    }

    protected void grdDocs_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "download")
        {
            db.Documentos.Download(Page, e.DataKey<int>(sender, "IdDocumento"));
        }
        else if(e.CommandName == "del" && !Auth.Current<CreaspUser>().Fornecedor)
        {
            db.Documentos.ExcluiDocumento(e.DataKey<int>(sender, "IdDocumento"));
            grdDocs.DataBind();

            Page.ShowInfoBox("Arquivo excluido com sucesso");
        }
    }

    protected void btnMultiUpload_Click(object sender, EventArgs e)
    {
        if (grdDocs.Rows.Count == 1)
        {
            grdDocs.SelectedIndexes = new int[] { 0 };
        }
        else if (grdDocs.SelectedDataKeys.Count() == 0) throw new NBoxException(grdDocs, "Nenhum item selecionado");

        lblQtDocs.Text = grdDocs.SelectedDataKeys.Count().ToString();
        chkSelTodos.Visible = grdDocs.Rows.Count > 1;

        pnlDocs.Open();
    }

    protected void uplDocs_FileUpload(object sender, EventArgs e)
    {
        var sel = grdDocs.SelectedDataKeys.Select(x => x["IdNerpItem"].To<int>()).Distinct().ToList();

        if (chkSelTodos.Checked)
        {
            sel = db.Query<NerpItem>().Where(x => x.IdNerp == IdNerp).ProjectTo(x => x.IdNerpItem);
        }

        foreach(var file in uplDocs.PostedFiles)
        {
            db.Nerps.IncluiDocumentoMultiplos(sel, file);
        }

        Page.ShowInfoBox("Arquivo incluido com sucesso");
        grdDocs.DataBind();
        pnlDocs.Close();
    }

    protected void grdDocs_DataBound(object sender, EventArgs e)
    {
        grdDocs.MergeRows(0, 1, 2, 3);
    }

    protected void btnDocsEvento_Click(object sender, EventArgs e)
    {
        ucDocsEvento.MontaTela(TpRefDoc.Evento, IdEvento.Value, null, false);
        pnlDocsEvento.Open();
    }

</script>

<box:NToolBar runat="server">
    <box:NButton runat="server" ID="btnMultiUpload" Text="Anexar documentos" Icon="upload" OnClick="btnMultiUpload_Click" />
    <box:NButton runat="server" ID="btnDocsEvento" Text="Documentos do evento" Icon="doc" OnClick="btnDocsEvento_Click" />
</box:NToolBar>

<box:NPopup runat="server" ID="pnlDocsEvento" Title="Documentos do evento" Width="750"><div class="margin-top"><uc:Documento runat="server" ID="ucDocsEvento" /></div></box:NPopup>

<box:NPopup runat="server" ID="pnlDocs" Title="Anexar documento" Width="600">
    <div class="form-div">
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Despesas selecionadas</span></div>
                <span class="field margin-bottom"><box:NLabel runat="server" ID="lblQtDocs" /></span>
                <box:NCheckBox runat="server" ID="chkSelTodos" Text="Selecionar todas as despesas desta NERP" CssClass="left" />
            </div>
        </div>
        <div class="sub-form-row">
             <div class="div-sub-form">
                <div class="flex"><span class="margin-right">Selecione o arquivo</span>
                    <box:NFileUpload runat="server" ID="uplDocs" Icon="upload" Text="Anexar documentos" ButtonTheme="Primary" AutoPostBack="true" AllowMultiple="true" OnFileUpload="uplDocs_FileUpload" />
                </div>
            </div>
        </div>
    </div>
</box:NPopup>

<div class="mt-30 c-primary-blue">
    <span>Filtro</span>
</div>

<div class="form-div">
    <div class="sub-form-row margin-top-0">
        <div class="div-sub-form">
            <div><span>Credor</span></div>
            <box:NTextBox runat="server" ID="txtNoPessoa" TextCapitalize="Upper" Width="95%" Placeholder="Credor" />
        </div>
        <div class="div-sub-form">
            <div><span>Despesa</span></div>
            <div class="input-group">
                <box:NTextBox runat="server" ID="txtNoDespesa" CssClass="radius-right-0" TextCapitalize="Upper" Width="80%" Placeholder="Despesa" />
                <box:NButton runat="server" ID="btnLimpar" Icon="cancel" OnClick="btnLimpar_Click" />
                <box:NButton runat="server" ID="btnFiltrar" Icon="search" Text="Filtrar" CssClass="radius-right" OnClick="btnFiltrar_Click" />
            </div>
        </div>
    </div>
</div>

<box:NGridView runat="server" ID="grdDocs" ItemType="Creasp.Nerp.NerpDocumentoDTO" DataKeyNames="IdNerpItem,IdDocumento" OnRowCommand="grdDocs_RowCommand" PageSize="200" OnDataBound="grdDocs_DataBound" ShowRowHover="false">
    <Columns>
        <box:NCheckBoxField />
        <asp:BoundField DataField="NoPessoa" HeaderText="Credor" SortExpression="NoPessoa" />
        <asp:BoundField DataField="NoDespesa" HeaderText="Despesa" HeaderStyle-Width="240" SortExpression="NoDespesa" />
        <asp:BoundField DataField="VrTotal" HeaderText="Valor" DataFormatString="{0:n2}" HeaderStyle-Width="100" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
        <asp:BoundField DataField="NoArquivo" HeaderText="Documento" HeaderStyle-Width="250" SortExpression="NoArquivo" ItemStyle-CssClass="nowrap-ellipsis" />
        <box:NButtonField Icon="download" Theme="Info" CommandName="download" EnabledExpression="IdDocumento"  ToolTip="Download" />
        <box:NButtonField Icon="cancel" Theme="Danger" CommandName="del" EnabledExpression="IdDocumento" ToolTip="Excluir documento" OnClientClick="return confirm('Confirma excluir o documento anexado a este item?');" />
    </Columns>
</box:NGridView>