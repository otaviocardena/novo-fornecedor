﻿<%@ Page Title="Contratos de Cessão de Uso" Resource="nerp.cessaouso" Language="C#" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();

    protected List<ContratoEntidadeDTO> Contratos { get { return ViewState["Contratos"] as List<ContratoEntidadeDTO>; } set { ViewState["Contratos"] = value; } }

    protected int NrMes => Request.QueryString["Mes"].To<int>();
    protected int NrAno => Request.QueryString["Ano"].To<int>();

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        grdList.RegisterDataSource(() => Contratos);

        RegisterRestorePageState(() => grdList.DataBind());
    }

    protected override void OnPrepareForm()
    {
        var erros = new StringBuilder();
        Contratos = db.Nerps.ListaContratosEntidade(NrAno, NrMes, erros);

        if (erros.Length > 0)
        {
            ltrErros.Text = erros.ToString().Replace("\n", "<br/>");
            trErros.Visible = true;
        }

        grdList.DataBind();

        this.BackTo("Nerp.aspx");
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        if(grdList.SelectedDataKeys.Count() == 0) throw new NBoxException(grdList, "Nenhuma entidade selecionada para emitir NERP");

        var selecionados = Contratos
            .Where(x => grdList.SelectedDataKeys.Select(z => z.Value.To<int>()).Contains(x.IdEntidade))
            .ToList();

        db.Nerps.IncluiNerpCessaoUso(selecionados, NrAno, NrMes, txtNoJustificativa.Text);

        Redirect("Nerp.aspx?TpNerp=" + TpNerp.CessaoUso + "&DtEmissaoIni=" + DateTime.Now.ToString("yyyy-MM-dd") + "&DtEmissaoFim=" + DateTime.Now.ToString("yyyy-MM-dd"));
    }

</script>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <div class="mt-30 c-primary-blue">
        <span>Dados da NERP</span>
    </div>
    <div class="form-div">
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Justificativa</span></div>
                <box:NTextBox runat="server" ID="txtNoJustificativa" TextCapitalize="Upper" MaxLength="100" Required="true" Width="100%" />
            </div>
        </div>
        <div class="sub-form-row" runat="server" id="trErros" visible="false">
            <div class="div-sub-form">
                <div><span>Erros</span></div>
                <span class="field"><asp:Literal runat="server" ID="ltrErros" /></span>
            </div>
        </div>
    </div>

    <box:NToolBar runat="server">
        <box:NButton runat="server" ID="btnSalvar" Icon="ok" Text="Emitir NERP" Theme="Primary" OnClick="btnSalvar_Click" />
    </box:NToolBar>

    <box:NGridView runat="server" ID="grdList" AllowGrouping="true" ItemType="Creasp.Nerp.ContratoEntidadeDTO" DataKeyNames="IdEntidade" ShowFooter="true">
        <Columns>
            <asp:BoundField DataField="NoGestor" />
            <box:NCheckBoxField />
            <asp:TemplateField HeaderText="Entidade" FooterStyle-HorizontalAlign="Right">
                <ItemTemplate>
                    <div><%# Item.NoEntidade %></div>
                    <small>Processo: <%# Item.NrProcesso %></small>
                    <small class="margin-left">Contrato: <%# Item.NrContrato %></small>
                </ItemTemplate>
                <FooterTemplate>TOTAL</FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Valor" HeaderStyle-Width="120" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right">
                <ItemTemplate><%# Item.VrContratoMes.ToString("n") %></ItemTemplate>
                <FooterTemplate><%# Contratos.Sum(x => x.VrContratoMes).ToString("n") %></FooterTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>O CREANET não retornou nenhum registro para o periodo informado</EmptyDataTemplate>
    </box:NGridView>

</asp:Content>