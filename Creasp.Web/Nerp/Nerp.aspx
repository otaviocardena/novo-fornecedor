﻿<%@ Page Title="NERP" Resource="nerp" Language="C#" %>

<script runat="server">

    protected CreaspContext db = new CreaspContext();
    protected int? idFornecedor = null;

    protected override void OnRegister()
    {
        var validsClaims = new List<string> {"ALL", "ALL.USUARIO", "ALL.FORNECEDOR"};
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        if (Auth.Current<CreaspUser>().Fornecedor)
        {
            Redirect("~/DefaultFornecedor.aspx", false, true);
        }

        idFornecedor = db.Pessoas.ObtemIdFornecedor(Auth.Current<CreaspUser>().IdPessoa);

        grdEvento.RegisterPagedDataSource((p) => db.Nerps.ListaEvento(p, txtDtEvento.Text.To<DateTime?>(), txtNoEvento.Text.To<string>(), tabAdianta.Selected));

        grdList.RegisterPagedDataSource((p) => db.Nerps.ListaNerps(p,
            txtNrNerp.Text.To<int?>(),
            txtNrEmpenho.Text.To<int?>(),
            ucEmissao.Periodo,
            ucVencimento.Periodo,
            optAtivo.Checked ? true : optInativo.Checked ? false : (bool?)null,
            ucUnidade.CdUnidade,
            cmbTpNerp.SelectedValue.To<string>()?.Split(',').First(),
            cmbTpSituacao.SelectedValue.To<string>(),
            cmbTpNerp.SelectedValue.To<string>()?.Split(',').ElementAtOrDefault(1).To<bool?>(),
            ucFavorecido.IdPessoa,
            ucAutorizador.IdPessoa,
            tabMinhas.Selected || Auth.Current<CreaspUser>().Fornecedor,
            txtBusca.Text,
            Auth.Current<CreaspUser>().Fornecedor,
            idFornecedor));

        // ao voltar, nao abre popups
        RegisterRestorePageState(() =>
        {
            pnlSelEvento.Close();
            pnlMesAno.Close();
            grdList.DataBind();
        });

    }

    protected override void OnPrepareForm()
    {

        if (Auth.Current<CreaspUser>().Fornecedor)
        {
            divMenuNewSolicitation.Visible = true;
            divMenuNew.Visible = false;
            tabMinhas.Visible = false;
            tabMain.Visible = false;
        }
        else
        {
            divMenuNewSolicitation.Visible = false;
            divMenuNew.Visible = true;
            tabMain.Visible = true;
        }

        ucEmissao.Periodo = Periodo.Vazio;

        cmbTpNerp.Items.Add(new ListItem("Ressarcimentos de evento", TpNerp.Evento + ",false"));
        cmbTpNerp.Items.Add(new ListItem("Adiantamentos de evento", TpNerp.Evento + ",true"));
        cmbTpNerp.Items.Add(new ListItem("Convênios ART", TpNerp.ART));
        cmbTpNerp.Items.Add(new ListItem("Cessão de Uso", TpNerp.CessaoUso));
        cmbTpNerp.Items.Add(new ListItem("Contratos", TpNerp.ComEmpenho));
        cmbTpNerp.Items.Add(new ListItem("Pagamentos diversos", TpNerp.SemEmpenho));

        if (Request.QueryString["TpNerp"] != null)
        {
            cmbTpNerp.SelectedValue = Request.QueryString["TpNerp"];
        }
        if (Request.QueryString["DtEmissaoIni"] != null && Request.QueryString["DtEmissaoFim"] != null)
        {
            ucEmissao.Periodo = new Periodo(Request.QueryString["DtEmissaoIni"].To<DateTime>(), Request.QueryString["DtEmissaoFim"].To<DateTime>());
        }

        cmbTpSituacao.Bind(NerpSituacao.ListaSituacoes(), true);

        // adiciona os botões de nova nerp
        Action<string, NDropDownItem> novo = (r, item) =>
        {
            if (User.IsInRole(r))
            {
                //drpIncluir.Items.Add(item);
                //drpIncluir.Visible = true;
            }
        };

        novo("nerp.evento", new NDropDownItem { Key = "EVENTO", Text = "Ressarcimento/Adiantamento de evento" });
        novo("nerp.art", new NDropDownItem { Key = "ART", Text = "Convênio ART" });
        novo("nerp.cessaouso", new NDropDownItem { Key = "CESSAO-USO", Text = "Cessão de uso" });
        novo("nerp.comemp", new NDropDownItem { Key = "COM-EMP", Text = "Contrato", NavigateUrl = "Nerp.ComEmpenho.aspx" });
        novo("nerp.sememp", new NDropDownItem { Key = "SEM-EMP", Text = "Pagamentos diversos", NavigateUrl = "Nerp.SemEmpenho.aspx" });

        SetDefaultButton(btnBuscar);
    }

    protected void btnBuscar_Click(object sender, EventArgs e)
    {
        grdList.DataBind();
    }

    protected void drpIncluir_ItemClick(object sender, NDropDownItem e)
    {
        if (e.Key == TpNerp.Evento)
        {
            grdEvento.DataBind();
            pnlSelEvento.Open();
        }
        else if (e.Key == TpNerp.ART)
        {
            cmbMes.SelectedValue = DateTime.Now.Month.ToString();
            cmbAno.SelectedValue = DateTime.Now.Year.ToString();
            btnMesAno.CommandName = "ART";
            pnlMesAno.Open(btnMesAno, cmbMes);
        }
        else if (e.Key == TpNerp.CessaoUso)
        {
            cmbMes.SelectedValue = DateTime.Now.Month.ToString();
            cmbAno.SelectedValue = DateTime.Now.Year.ToString();
            btnMesAno.CommandName = "CESSAO-USO";
            pnlMesAno.Open(btnMesAno, cmbMes);
        }
    }

    protected void grdList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var data = e.Row.DataItem as IFgAtivo;
            if (!data.FgAtivo) e.Row.ForeColor = System.Drawing.Color.Red;
        }
    }

    protected void tabAdianta_TabChanged(object sender, EventArgs e)
    {
        txtDtEvento.Text = "";
        txtNoEvento.Text = "";
        grdEvento.DataBind();
    }

    protected void txtDtEvento_TextChanged(object sender, EventArgs e)
    {
        grdEvento.DataBind();
    }

    protected void tabNerp_TabChanged(object sender, EventArgs e)
    {
        grdList.DataBind();
    }

    protected void btnMesAno_Command(object sender, CommandEventArgs e)
    {
        if (e.CommandName == "ART")
        {
            Redirect("Nerp.ART.aspx?Mes=" + cmbMes.SelectedValue + "&Ano=" + cmbAno.SelectedValue);
        }
        else
        {
            Redirect("Nerp.CessaoUso.aspx?Mes=" + cmbMes.SelectedValue + "&Ano=" + cmbAno.SelectedValue);
        }
    }
</script>
<%--<asp:Content ContentPlaceHolderID="title" runat="server">
    <box:NDropDown runat="server" ID="drpIncluir" Icon="plus" Text="Nova NERP" OnItemClick="drpIncluir_ItemClick" ButtonTheme="Success" Visible="false" />
</asp:Content>--%>
<asp:Content ContentPlaceHolderID="content" runat="server">
    <div>
        <box:NTab runat="server" ID="tabMain" Text="Todas" Selected="true" AutoPostBack="true" OnTabChanged="tabNerp_TabChanged">

            <%--<box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnpesquisar" icon="search" text="pesquisar" theme="default" onclick="btnpesquisar_click" />
        </box:NToolBar>--%>
            <div class="subTitle-wrapper">
                <span class="subTitle">NERP</span>
                <div runat="server" ID="divMenuNew" class="subTitle-new">
                    <div>
                        <i class="far fa-plus-square" style="color: #FABB00"></i>
                    </div>
                    <div class="f8 c-black">
                        Nova NERP
                    </div>
                    <div class="menu-new" id="divSubMenu">
                        <div>
                            NOVA NERP
                            <ul>
                                <li><i class="fas fa-chevron-right"></i><a href="/nerp/Nerp.Evento.Default.aspx">Ressa./Adian. de Evento</a></li>
                                <li><i class="fas fa-chevron-right"></i><a href="/nerp/Nerp.ART.aspx">Convênio ART</a></li>
                                <li><i class="fas fa-chevron-right"></i><a href="/nerp/Nerp.CessaoUso.aspx">Cessão de Uso</a></li>
                                <li><i class="fas fa-chevron-right"></i><a href="/nerp/Nerp.ComEmpenho.aspx">Contrato</a></li>
                                <li><i class="fas fa-chevron-right"></i><a href="/nerp/Nerp.SemEmpenho.aspx">Pagamentos Diversos</a></li>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="subTitle-new" runat="server" id="divMenuNewSolicitation">
                    <a href="/solicitacao/Solicitacao.Novo.aspx">
                        <div class="flex-center">
                            <i class="far fa-plus-square" style="color: #FABB00"></i>
                        </div>
                        <div class="f8 c-black">
                            Nova Solicitação de Pagamento
                        </div>
                    </a>
                </div>
            </div>

            <div class="form-div">
                <div class="sub-form-row">
                    <div class="div-sub-form">
                        <div><span>Buscar</span></div>
                        <box:NTextBox runat="server" ID="txtBusca" Width="98%" />
                    </div>
                </div>

                <div class="sub-form-row">
                    <div class="div-sub-form">
                        <div><span>Número</span></div>
                        <box:NTextBox runat="server" ID="txtNrNerp" MaskType="Integer" />
                    </div>

                    <div class="div-sub-form">
                        <div><span>Número Empenho</span></div>
                        <box:NTextBox runat="server" ID="txtNrEmpenho" MaskType="Integer" />
                    </div>

                    <div class="div-sub-form">
                        <div><span>Emissão</span></div>
                        <uc:Periodo runat="server" ID="ucEmissao" ShowTextBox="true" />
                    </div>
                </div>

                <div class="sub-form-row">
                    <div class="div-sub-form">
                        <div><span>Vencimento</span></div>
                        <uc:Periodo runat="server" ID="ucVencimento" ShowTextBox="true" WithoutEdit="true" />
                    </div>

                    <div class="div-sub-form">
                        <div><span>Favorecido</span></div>
                        <div class="btn-form-search">
                            <uc:Pessoa runat="server" ID="ucFavorecido" WithoutEdit="true" />
                        </div>
                    </div>
                    <div class="div-sub-form">
                        <div><span>Autorizador</span></div>
                        <div class="btn-form-search">
                            <uc:Pessoa runat="server" ID="ucAutorizador" IsFuncionario="true" />
                        </div>
                    </div>
                </div>

                <div class="sub-form-row">

                    <div class="div-sub-form">
                        <div><span>Unidade solicitante</span></div>
                        <uc:Unidade runat="server" ID="ucUnidade"/>
                    </div>

                    <div class="div-sub-form">
                        <div><span>Tipo de NERP</span></div>
                        <box:NDropDownList runat="server" ID="cmbTpNerp" Width="96%">
                            <asp:ListItem Value="" Text="(Selecione uma opção abaixo)" />
                        </box:NDropDownList>
                    </div>

                    <div class="div-sub-form">
                        <div><span>Fluxo</span></div>
                        <box:NDropDownList runat="server" ID="cmbTpSituacao" Width="96%" />
                    </div>
                </div>

                <div class="sub-form-row">
                    <div>
                        <div class="margin-bottom"><span>Situação</span></div>
                        <div>
                            <box:NRadioButton runat="server" ID="optAtivo" GroupName="fgAtivo" Text="Ativas" Checked="true" />
                            <box:NRadioButton runat="server" ID="optInativo" GroupName="fgAtivo" Text="Inativas" />
                            <box:NRadioButton runat="server" ID="optTodas" GroupName="fgAtivo" Text="Todas" />
                        </div>
                    </div>

                    <div>
                        <box:NButton runat="server" ID="btnBuscar" Text="pesquisar" Theme="primary" OnClick="btnBuscar_Click" Width="150" Height="40" CssClass="btn-form-search space-top" />
                    </div>

                </div>
            </div>

        </box:NTab>

        <box:NTab runat="server" ID="tabMinhas" Text="Minhas NERP's" AutoPostBack="true" OnTabChanged="tabNerp_TabChanged"></box:NTab>
        
        <box:NGridView runat="server" ID="grdList" ItemType="Creasp.Nerp.NerpResultadoDTO" OnRowDataBound="grdList_RowDataBound">
            <Columns>
                <asp:HyperLinkField DataTextField="NrNerp" DataNavigateUrlFields="IdNerp" DataNavigateUrlFormatString="nerp/Nerp.View.aspx?IdNerp={0}" HeaderText="Número" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="80" SortExpression="NrNerp" />
                <asp:BoundField DataField="DtEmissao" HeaderText="Emissão" DataFormatString="{0:d}" HeaderStyle-Width="110" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" SortExpression="DtEmissao" />
                <asp:BoundField DataField="DtVencto" HeaderText="Vencimento" DataFormatString="{0:d}" HeaderStyle-Width="110" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" SortExpression="DtVencto" />
                <asp:BoundField DataField="NoNerp" HeaderText="Descrição" SortExpression="NoNerp" />
                <asp:TemplateField HeaderText="Responsável" HeaderStyle-Width="300">
                    <ItemTemplate>
                        <div><%# Item.NoResponsavel %></div>
                        <small><%# NerpSituacao.NoSituacao(Item.TpSituacao) %></small>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Valor" HeaderStyle-Width="120" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <div class="left-right">
                            <span>
                                <i class="far fa-check-circle" style="color: darkorange" runat="server" visible="<%# Item.VrSolic > 0 && Item.VrPagto != Item.VrBruto %>" title='<%# "Solicitado: " +  Item.VrSolic.ToString("c2") + " - Confirmado: " + Item.VrPagto.ToString("c2") %>'></i>
                                <i class="far fa-check-circle" style="color: #0aa20a" runat="server" visible="<%# Item.VrPagto == Item.VrBruto %>" title='<%# "Confirmado: " + Item.VrBruto.ToString("c2") %>'></i>
                            </span>
                            <span><%# Item.VrBruto.ToString("n2") %></span>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>Nenhuma NERP encontrada no filtro informado</EmptyDataTemplate>
        </box:NGridView>          

        <box:NPopup runat="server" ID="pnlSelEvento" Title="Selecionar evento" Width="750" Height="550">

            <box:NTab runat="server" ID="tabReemb" Text="Ressarcimentos" Selected="true" TabCssClass="no-tab" AutoPostBack="true" OnTabChanged="tabAdianta_TabChanged"></box:NTab>
            <box:NTab runat="server" ID="tabAdianta" Text="Adiantamentos" AutoPostBack="true" OnTabChanged="tabAdianta_TabChanged"></box:NTab>

            <table class="form">
                <caption>Pesquisa</caption>
                <tr>
                    <td>Data do evento</td>
                    <td>
                        <box:NTextBox runat="server" ID="txtDtEvento" MaskType="Date" AutoPostBack="true" OnTextChanged="txtDtEvento_TextChanged" />
                    </td>
                </tr>
                <tr>
                    <td>Descrição</td>
                    <td>
                        <box:NTextBox runat="server" ID="txtNoEvento" TextCapitalize="Upper" OnEnter="txtDtEvento_TextChanged" Width="100%" />
                    </td>
                </tr>
            </table>

            <box:NGridView runat="server" ID="grdEvento" PageSize="10" AllowCustomPaging="false">
                <Columns>
                    <asp:HyperLinkField DataTextField="DtEvento" DataNavigateUrlFields="IdEvento" DataNavigateUrlFormatString="Nerp.Evento.aspx?IdEvento={0}" HeaderText="Data" DataTextFormatString="{0:dd/MM/yyyy HH:mm}" HeaderStyle-Width="160" SortExpression="DtEvento" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField DataField="NoEvento" HeaderText="Evento" SortExpression="NoEvento" />
                </Columns>
                <EmptyDataTemplate>Nenhum evento na data informada</EmptyDataTemplate>
            </box:NGridView>

        </box:NPopup>

        <box:NPopup runat="server" ID="pnlMesAno" Title="Selecione o mês/ano">
            <box:NToolBar runat="server">
                <box:NButton runat="server" ID="btnMesAno" Text="Selecionar" Icon="calendar" OnCommand="btnMesAno_Command" />
            </box:NToolBar>
            <table class="form">
                <tr>
                    <td>Informe o mês/ano</td>
                    <td>
                        <box:NDropDownList runat="server" ID="cmbMes">
                            <asp:ListItem Value="1">Janeiro</asp:ListItem>
                            <asp:ListItem Value="2">Fevereiro</asp:ListItem>
                            <asp:ListItem Value="3">Março</asp:ListItem>
                            <asp:ListItem Value="4">Abril</asp:ListItem>
                            <asp:ListItem Value="5">Maio</asp:ListItem>
                            <asp:ListItem Value="6">Junho</asp:ListItem>
                            <asp:ListItem Value="7">Julho</asp:ListItem>
                            <asp:ListItem Value="8">Agosto</asp:ListItem>
                            <asp:ListItem Value="9">Setembro</asp:ListItem>
                            <asp:ListItem Value="10">Outubro</asp:ListItem>
                            <asp:ListItem Value="11">Novembro</asp:ListItem>
                            <asp:ListItem Value="12">Dezembro</asp:ListItem>
                        </box:NDropDownList>
                        &nbsp;&nbsp;/&nbsp;&nbsp;
                    <box:NDropDownList runat="server" ID="cmbAno">
                        <asp:ListItem Value="2015">2015</asp:ListItem>
                        <asp:ListItem Value="2016">2016</asp:ListItem>
                        <asp:ListItem Value="2017">2017</asp:ListItem>
                        <asp:ListItem Value="2018">2018</asp:ListItem>
                        <asp:ListItem Value="2019">2019</asp:ListItem>
                        <asp:ListItem Value="2020">2020</asp:ListItem>
                        <asp:ListItem Value="2021">2021</asp:ListItem>
                    </box:NDropDownList>
                    </td>
                </tr>
            </table>
            <div class="help">Este periodo será informado ao CREANET para buscar todos os convenios de ART/contratos.</div>
        </box:NPopup>

    </div>
    <script src="../Content/Scripts/pagination.js"></script>
    <script>paginate()</script>

    <script src="../Content/Scripts/sidebar.js"></script>
    <script>selectButton("nerp-home"); showReload("nerp")</script>
</asp:Content>
