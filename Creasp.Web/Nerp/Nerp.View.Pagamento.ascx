﻿<%@ Control Language="C#" %>
<script runat="server">

    protected CreaspContext db;

    protected int IdNerp => Request.QueryString["IdNerp"].To<int>();

    private bool _validaAnuidade = false;
    private bool _permiteAnexarDocumento = false;

    public void OnRegister(CreaspContext db, NerpPermissao permissao)
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO", "ALL.FORNECEDOR" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Page.Redirect("~/Login.aspx", false, true);
        }

        if (Auth.Current<CreaspUser>().Fornecedor)
        {
            btnSolicPagtoConfirma.Visible = false;
        }

        this.db = db;
        grdItems.RegisterPagedDataSource((p) => db.Nerps.ListNerpItem(p, IdNerp, txtFiltro.Text.To<string>()));

        Page.RegisterResource(permissao.FgSolicPagto).Disabled(btnSolicPagto, btnDesfazSolic, btnEstornaPagto);

        _permiteAnexarDocumento = permissao.FgSolicPagto;
    }

    public void MontaTela()
    {
        ucDocs.MontaTela(TpRefDoc.NerpPagto, IdNerp, null, _permiteAnexarDocumento || Page.User.IsInRole("nerp.editdoc"));

        cmbTipoDocumento.Bind(Factory.Get<ISiscont>().ListaTipoDocumentos().OrderBy(x => x).Select(x => new ListItem(x, x)));
        grdItems.DataBind();
    }

    protected void btnLimpar_Click(object sender, EventArgs e)
    {
        txtFiltro.Text = "";
        grdItems.DataBind();
    }

    protected void btnFiltrar_Click(object sender, EventArgs e)
    {
        grdItems.DataBind();
    }

    protected void btnSolicPagto_Click(object sender, EventArgs e)
    {
        if (grdItems.SelectedDataKeys.Count() == 0) throw new NBoxException(grdItems, "Nenhum item selecionado");

        var idItems = grdItems.SelectedDataKeys.Select(x => x.Value.To<int>());

        txtVrTotal.Text = db.Query<NerpItem>()
            .Where(x => idItems.Contains(x.IdNerpItem))
            .ToEnumerable()
            .Sum(x => x.VrTotal).ToString("n2");

        txtQtItems.Text = $"{idItems.Count()} pagamento(s)";

        pnlPagto.Open(btnSolicPagtoConfirma, txtDtPagto);
    }

    protected void btnSolicPagtoConfirma_Click(object sender, EventArgs e)
    {
        var count = db.Nerps.SolicPagtos(grdItems.SelectedDataKeys.Select(x => x.Value.To<int>()), txtDtPagto.Text.To<DateTime>(), cmbTipoDocumento.SelectedValue, txtNrDoc.Text, txtDtEmissaoDoc.Text.To<DateTime>());

        grdItems.DataBind();

        if (count == 0) throw new NBoxException("Nenhum pagamento realizado");

        Page.ShowInfoBox("Solicitações de pagamento realizadas com sucesso.");

        pnlPagto.Close();
    }

    protected void btnAnexarDocs_Click(object sender, EventArgs e)
    {
        pnlDocs.Open();
    }

    protected void btnVerifSiscont_Click(object sender, EventArgs e)
    {
        var ret = db.Nerps.VerificaSiscontPagto(db.SingleById<Nerp>(IdNerp));

        if (ret)
        {
            Page.ShowInfoBox("Pagamento(s) aprovado(s) no SISCONT");
            grdItems.DataBind();
        }
        else
        {
            Page.ShowErrorBox("Nenhum novo pagamento realizado no SISCONT");
        }
    }

    protected void btnDesfazSolic_Click(object sender, EventArgs e)
    {
        var idNerpItems = grdItems.SelectedDataKeys.Select(x => x.Value.To<int>());

        if(idNerpItems.Count() == 0) throw new NBoxException("Nenhum item selecionado");

        db.Nerps.DesfazSolicPagto(IdNerp, idNerpItems);

        Page.ShowInfoBox("Operação de desfazer realizada com sucesso.");

        grdItems.DataBind();
    }

    protected void btnEstornaPagto_Click(object sender, EventArgs e)
    {
        var idNerpItems = grdItems.SelectedDataKeys.Select(x => x.Value.To<int>());

        if(idNerpItems.Count() == 0) throw new NBoxException("Nenhum item selecionado");

        db.Nerps.EstornaPagto(IdNerp, idNerpItems);

        Page.ShowInfoBox("Operação de estorno realizada com sucesso.");

        grdItems.DataBind();
    }

    
</script>
<box:NToolBar runat="server">
    <box:NButton runat="server" ID="btnSolicPagto" Icon="money" Text="Solicitar pagamento" Theme="Default" OnClick="btnSolicPagto_Click" />
    <box:NButton runat="server" ID="btnDesfazSolic" Icon="cancel" Text="Desfazer solicitação" Theme="Default" OnClick="btnDesfazSolic_Click" OnClientClick="return confirm('ATENÇÃO: Esta operação não desfaz as solicitações de pagamento no sistema SISCONT. Ao clicar em desfazer o usuário deve ter certeza que as solicitações já foram canceladas no SISCONT para evitar duplicidade. Esta operação poderá ser auditada posteriormente.');" />
    <box:NButton runat="server" ID="btnEstornaPagto" Icon="ccw" Text="Estornar pagamento" Theme="Default" OnClick="btnEstornaPagto_Click" OnClientClick="return confirm('ATENÇÃO: Esta operação não estorna os pagamentos no sistema SISCONT. Ao clicar em estornar o usuário deve ter certeza que os pagamentos já foram estornados no SISCONT para evitar duplicidade. Esta operação poderá ser auditada posteriormente.');" />
    <box:NButton runat="server" ID="btnAnexarDocs" Icon="attach" Text="Comprovantes" Theme="Default" OnClick="btnAnexarDocs_Click" />
    <box:NButton runat="server" ID="btnVerifSiscont" Icon="menu" Text="Verificar SISCONT" CssClass="right" Theme="Default" OnClick="btnVerifSiscont_Click" />
</box:NToolBar>

<div class="form-div">
    <script>
        var element = document.querySelector(".n-grid-total");
        element.setAttribute("style", "display:none");
    </script>
    <div class="sub-form-row">
        <div class="div-sub-form form-uc">
            <div><span>Nome</span></div>
            <div class="input-group">
            <box:NTextBox runat="server" ID="txtFiltro" TextCapitalize="Upper" Width="100%" />
            <box:NButton runat="server" ID="btnLimpar" Icon="cancel" OnClick="btnLimpar_Click" />
            <box:NButton runat="server" ID="btnFiltrar" Text="Filtrar"  OnClick="btnFiltrar_Click" />
            </div>
        </div>
    </div>
</div>

<box:NGridView runat="server" ID="grdItems" ItemType="Creasp.Nerp.NerpItem" DataKeyNames="IdNerpItem,IdPessoaCredor" PageSize="1000">
    <Columns>
        <box:NCheckBoxField />
        <asp:BoundField DataField="NoPagto" HeaderText="Pagto" HeaderStyle-Width="130" SortExpression="NrPagto" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
        <asp:BoundField DataField="DtPagto" HeaderText="Data" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-Width="110" SortExpression="DtPagto" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
        <asp:BoundField DataField="PessoaCredor.NoPessoa" HeaderText="Pessoa" SortExpression="PessoaCredor.NoPessoa" />
        <asp:BoundField DataField="Empenho.NoEmpenho" HeaderText="Emp." HeaderStyle-Width="80" SortExpression="Empenho.NrEmpenho" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
        <asp:BoundField DataField="CentroCusto.NrCentroCustoFmt" HeaderText="C. custo" HeaderStyle-Width="100" SortExpression="CentroCusto.NrCentroCusto" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
        <asp:BoundField DataField="VrTotal" HeaderText="Total" DataFormatString="{0:n2}" HeaderStyle-Width="100" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
    </Columns>
</box:NGridView>

<box:NPopup runat="server" ID="pnlDocs" Title="Comprovantes de pagamento" Width="750">
    <uc:Documento runat="server" ID="ucDocs" Titulo="Comprovantes" />
</box:NPopup>

<box:NPopup runat="server" ID="pnlPagto" Title="Solicitação de pagamento" Width="650">
    <table class="form">
        <tr>
            <td>Data de pagamento</td>
            <td>
                <box:NTextBox runat="server" ID="txtDtPagto" MaskType="Date" Required="true" ValidationGroup="pg" />
            </td>
        </tr>
        <tr>
            <td>Tipo de documento</td>
            <td>
                <box:NDropDownList runat="server" ID="cmbTipoDocumento" Width="100%" Required="true" ValidationGroup="pg" />
            </td>
        </tr>
        <tr>
            <td>Número do documento</td>
            <td>
                <box:NTextBox runat="server" ID="txtNrDoc" Width="100%" TextCapitalize="Upper" />
            </td>
        </tr>
        <tr>
            <td>Emissão do documento</td>
            <td>
                <box:NTextBox runat="server" ID="txtDtEmissaoDoc" MaskType="Date" Required="true" ValidationGroup="pg" />
            </td>
        </tr>
        <tr>
            <td>Valor total</td>
            <td>
                <box:NTextBox runat="server" ID="txtVrTotal" MaskType="Decimal" Enabled="false" Width="120" />
                <box:NLabel runat="server" ID="txtQtItems" CssClass="margin-left" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <box:NButton runat="server" ID="btnSolicPagtoConfirma" Text="Confirmar" Icon="ok" Theme="Primary" OnClick="btnSolicPagtoConfirma_Click" ValidationGroup="pg" />
            </td>
        </tr>
    </table>
    <div class="help"><b>ATENÇÃO</b>: Todos os pagamentos selecionados serão enviados ao SISCONT com os mesmos paramêtros acima informado.</div>
</box:NPopup>