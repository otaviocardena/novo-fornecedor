﻿<%@ Control Language="C#" %>
<script runat="server">

    protected CreaspContext db;

    protected int IdNerp => Request.QueryString["IdNerp"].To<int>();

    private bool _permiteAnexarDocumento = false;

    public void OnRegister(CreaspContext db, NerpPermissao permissao)
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO", "ALL.FORNECEDOR" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Page.Redirect("~/Login.aspx", false, true);
        }

        if (Auth.Current<CreaspUser>().Fornecedor)
        {
            btnAnexarDocs.Visible = false;
            btnDesfazSolic.Visible = false;
            btnSolicAdiantamento.Visible = false;
            btnVerifSiscont.Visible = false;
            btnSolicAdiantamentoConfirma.Visible = false;
            btnFiltrar.Visible = false;
        }

        this.db = db;
        grdItems.RegisterPagedDataSource((p) => db.Nerps.ListNerpItem(p, IdNerp, txtFiltro.Text.To<string>()));

        Page.RegisterResource(permissao.FgSolicAdiantamento).Disabled(btnSolicAdiantamento, btnDesfazSolic);

        _permiteAnexarDocumento = permissao.FgAnexarComprovAdiant;
    }

    public void MontaTela()
    {
        ucDocs.MontaTela(TpRefDoc.NerpAdiantamento, IdNerp, null, _permiteAnexarDocumento);

        cmbTipoDocumento.Bind(Factory.Get<ISiscont>().ListaTipoDocumentos().OrderBy(x => x).Select(x => new ListItem(x, x)));
        grdItems.DataBind();
    }

    protected void btnAnexarDocs_Click(object sender, EventArgs e)
    {
        pnlDocs.Open();
    }

    protected void btnLimpar_Click(object sender, EventArgs e)
    {
        txtFiltro.Text = "";
        grdItems.DataBind();
    }

    protected void btnFiltrar_Click(object sender, EventArgs e)
    {
        grdItems.DataBind();
    }

    protected void btnSolicAdiantamento_Click(object sender, EventArgs e)
    {
        if (grdItems.SelectedDataKeys.Count() == 0) throw new NBoxException(grdItems, "Nenhum item selecionado");

        var idItems = grdItems.SelectedDataKeys.Select(x => x.Value.To<int>());

        txtVrTotal.Text = db.Query<NerpItem>()
            .Where(x => idItems.Contains(x.IdNerpItem))
            .ToEnumerable()
            .Sum(x => x.VrTotal).ToString("n2");

        txtQtItems.Text = $"{idItems.Count()} adiantamento(s)";

        // se nerp de evento, usa o periodo como o periodo do evento
        var nerp = db.Query<Nerp>()
            .Include(x => x.Evento, JoinType.Left)
            .Where(x => x.IdNerp == IdNerp)
            .Single();

        if (nerp != null)
        {
            ucPeriodo.Periodo = new Periodo(nerp.Evento.DtEvento, nerp.Evento.DtEventoFim);
        }

        pnlAdiantamento.Open(btnSolicAdiantamentoConfirma, txtDtAdiantamento);
    }

    protected void btnSolicAdiantamentoConfirma_Click(object sender, EventArgs e)
    {
        var count = db.Nerps.SolicAdiantamento(grdItems.SelectedDataKeys.Select(x => x.Value.To<int>()),
            txtDtAdiantamento.Text.To<DateTime>(),
            ucPeriodo.Periodo,
            ucBanco.IdContaContabil.Value,
            ucResponsavel.IdContaContabil.Value,
            ucPassivo.IdContaContabil,
            cmbTipoDocumento.SelectedValue,
            txtNrDoc.Text,
            txtDtEmissaoDoc.Text.To<DateTime>());

        grdItems.DataBind();

        if (count == 0) throw new NBoxException("Nenhum adiantamento realizado");

        Page.ShowInfoBox("Solicitações de adiantamento realizadas com sucesso.");

        pnlAdiantamento.Close();
    }

    protected void btnVerifSiscont_Click(object sender, EventArgs e)
    {
        var ret = db.Nerps.VerificaSiscontAdiantamento(db.SingleById<Nerp>(IdNerp));

        if (ret)
        {
            Page.ShowInfoBox("Adiantamento(s) aprovado(s) no SISCONT");
            grdItems.DataBind();
        }
        else
        {
            Page.ShowErrorBox("Nenhum novo adiantamento realizado no SISCONT");
        }
    }

    protected void btnDesfazSolic_Click(object sender, EventArgs e)
    {
        var idNerpItems = grdItems.SelectedDataKeys.Select(x => x.Value.To<int>());

        if(idNerpItems.Count() == 0) throw new NBoxException("Nenhum item selecionado");

        db.Nerps.DesfazSolicAdiantamento(IdNerp, idNerpItems);

        Page.ShowInfoBox("Operação de desfazer realizada com sucesso.");

        grdItems.DataBind();
    }

</script>
<box:NToolBar runat="server">
    <box:NButton runat="server" ID="btnSolicAdiantamento" Icon="money" Text="Solicitar adiantamento" Theme="Default" OnClick="btnSolicAdiantamento_Click" />
    <box:NButton runat="server" ID="btnDesfazSolic" Icon="cancel" Text="Desfazer solicitação" Theme="Default" OnClick="btnDesfazSolic_Click" OnClientClick="return confirm('ATENÇÃO: Esta operação não desfaz as solicitações de adiantamento no sistema SISCONT. Ao clicar em desfazer o usuário deve ter certeza que as solicitações já foram canceladas no SISCONT para evitar duplicidade. Esta operação poderá ser auditada posteriormente.');" />
    <box:NButton runat="server" ID="btnAnexarDocs" Icon="attach" Text="Comprovantes" Theme="Default" OnClick="btnAnexarDocs_Click" />
    <box:NButton runat="server" ID="btnVerifSiscont" Icon="menu" Text="Verificar SISCONT" CssClass="right" Theme="Default" OnClick="btnVerifSiscont_Click" />
</box:NToolBar>

<box:NPopup runat="server" ID="pnlDocs" Title="Comprovantes de pagamento" Width="750">
    <uc:Documento runat="server" ID="ucDocs" Titulo="Comprovantes" />
</box:NPopup>

<table class="form">
    <tr>
        <td>Nome</td>
        <td>
            <div class="input-group">
                <box:NTextBox runat="server" ID="txtFiltro" TextCapitalize="Upper" Width="100%" />
                <box:NButton runat="server" ID="btnLimpar" Icon="cancel" OnClick="btnLimpar_Click" />
                <box:NButton runat="server" ID="btnFiltrar" Icon="search" Text="Filtrar"  OnClick="btnFiltrar_Click" />
            </div>
        </td>
    </tr>
</table>

<box:NGridView runat="server" ID="grdItems" ItemType="Creasp.Nerp.NerpItem" DataKeyNames="IdNerpItem,IdPessoaCredor" PageSize="1000">
    <Columns>
        <box:NCheckBoxField />
        <asp:BoundField DataField="NoAdiantamento" HeaderText="Adiantamento" HeaderStyle-Width="150" SortExpression="NrAdiantamento" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
        <asp:BoundField DataField="DtAdiantamento" HeaderText="Data" DataFormatString="{0:dd/MM/yyyy}" HeaderStyle-Width="110" SortExpression="DtPagto" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
        <asp:BoundField DataField="PessoaCredor.NoPessoa" HeaderText="Pessoa" SortExpression="PessoaCredor.NoPessoa" />
        <asp:BoundField DataField="Empenho.NoEmpenho" HeaderText="Emp." HeaderStyle-Width="80" SortExpression="Empenho.NrEmpenho" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
        <asp:BoundField DataField="CentroCusto.NrCentroCustoFmt" HeaderText="C. custo" HeaderStyle-Width="100" SortExpression="CentroCusto.NrCentroCusto" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
        <asp:BoundField DataField="VrTotal" HeaderText="Total" DataFormatString="{0:n2}" HeaderStyle-Width="100" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
    </Columns>
</box:NGridView>

<box:NPopup runat="server" ID="pnlAdiantamento" Title="Solicitação de adiantamento" Width="800">
    <table class="form">
        <tr>
            <td>Data do adiantamento</td>
            <td>
                <box:NTextBox runat="server" ID="txtDtAdiantamento" MaskType="Date" Required="true" ValidationGroup="ad" />
            </td>
        </tr>
        <tr>
            <td>Periodo do suprimento</td>
            <td>
                <uc:Periodo runat="server" ID="ucPeriodo" ShowTextBox="true" Required="true" ValidationGroup="ad" />
            </td>
        </tr>
        <tr>
            <td>Banco</td>
            <td>
                <uc:ContaContabil runat="server" ID="ucBanco" NrContaFiltro="111" FgAnalitico="true" Required="true" ValidationGroup="ad"/>
            </td>
        </tr>
        <tr>
            <td>Conta do resp. por suprimento</td>
            <td>
                <uc:ContaContabil runat="server" ID="ucResponsavel" NrContaFiltro="113" FgAnalitico="true" Required="true" ValidationGroup="ad" />
            </td>
        </tr>
        <tr>
            <td>Conta do passivo</td>
            <td>
                <uc:ContaContabil runat="server" ID="ucPassivo" NrContaFiltro="2" FgAnalitico="true" Required="true" ValidationGroup="ad" />
            </td>
        </tr>
        <tr>
            <td>Tipo de documento</td>
            <td>
                <box:NDropDownList runat="server" ID="cmbTipoDocumento" Width="100%" Required="true" ValidationGroup="ad" />
            </td>
        </tr>
        <tr>
            <td>Número do documento</td>
            <td>
                <box:NTextBox runat="server" ID="txtNrDoc" Width="100%" TextCapitalize="Upper" />
            </td>
        </tr>
        <tr>
            <td>Emissão do documento</td>
            <td>
                <box:NTextBox runat="server" ID="txtDtEmissaoDoc" MaskType="Date" Required="true" ValidationGroup="ad" />
            </td>
        </tr>
        <tr>
            <td>Valor total</td>
            <td>
                <box:NTextBox runat="server" ID="txtVrTotal" MaskType="Decimal" Enabled="false" Width="120" />
                <box:NLabel runat="server" ID="txtQtItems" CssClass="margin-left" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <box:NButton runat="server" ID="btnSolicAdiantamentoConfirma" Text="Confirmar" Icon="ok" Theme="Primary" OnClick="btnSolicAdiantamentoConfirma_Click" ValidationGroup="ad" />
            </td>
        </tr>
    </table>
    <div class="help"><b>ATENÇÃO</b>: Todos os adiantamentos selecionados serão enviados ao SISCONT com os mesmos paramêtros acima informado.</div>
</box:NPopup>