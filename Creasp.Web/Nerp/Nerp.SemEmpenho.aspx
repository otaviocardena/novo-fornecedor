﻿<%@ Page Title="Pagamentos Diversos" Resource="nerp.sememp" Language="C#" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();
    protected ISiscont siscont = Factory.Get<ISiscont>();

    protected int? IdNerp => Request.QueryString["IdNerp"].To<int?>();

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO", "ALL.FORNECEDOR" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }
    }

    protected override void OnPrepareForm()
    {
        this.BackTo("Nerp.aspx", false);

        if (User.IsInRole("nerp.altautoriza") == false)
        {
            ucChefe.Enabled = ucChefe.Required = false;
            ucSuperintendente.Enabled = ucSuperintendente.Required = false;
        }

        if (IdNerp != null)
        {
            MontaTela();
        }
        else
        {
            var funcionarios = new FuncionarioService(new CreaspContext());
            var func = funcionarios.BuscaFuncionarioHierarquia(Auth.Current<CreaspUser>().Login);

            ucChefe.LoadPessoa(func.Chefe.Pessoa.IdPessoa);
            ucSuperintendente.LoadPessoa(func.Superintendente.Pessoa.IdPessoa);
        }
    }

    protected void MontaTela()
    {
        var nerp = db.SingleById<Nerp>(IdNerp);
        var emp = db.Nerps.ListaEmpenhos(IdNerp.Value).SingleOrDefault(); // só tem 1 empenho
        var item = db.Nerps.ListNerpItem(Pager.NoPage, IdNerp.Value, null).Items.SingleOrDefault(); // só tem 1 item

        if (nerp.TpSituacao != NerpSituacao.Cadastrada) throw new NBoxException("Esta NERP não pode ser editada");
        if (nerp.TpNerp != TpNerp.ComEmpenho && nerp.TpNerp != TpNerp.SemEmpenho) throw new NBoxException("Esta NERP não é do tipo contrato");

        ucFavorecido.LoadPessoa(item.IdPessoaCredor);
        ucContaContabil.LoadContaContabil(item.IdContaContabil);
        ucCentroCusto.LoadCentroCusto(item.IdCentroCusto);

        btnSalvarSemEmp.Text = "Alterar NERP";

        txtNoJustificativa.Text = nerp.NoJustificativa;
        txtNoNerp.Text = nerp.NoNerp;
        txtDtVencto.SetText(nerp.DtVencto);
        txtVrItem.SetText(item.VrTotal);

        var chefe = db.Single<NerpAssinatura>(x => x.IdNerp == IdNerp && x.TpAprovacao == TpAprovacao.Gestor);
        var superintendente = db.Single<NerpAssinatura>(x => x.IdNerp == IdNerp && x.TpAprovacao == TpAprovacao.Superintendente);

        ucChefe.LoadPessoa(chefe.IdPessoa);
        ucSuperintendente.LoadPessoa(superintendente.IdPessoa);
    }

    protected void btnSalvarSemEmp_Click(object sender, EventArgs e)
    {
        if (ucFavorecido.IdPessoa == null) throw new NBoxException(ucFavorecido, "Favorecido obrigatório");
        if (ucContaContabil.IdContaContabil == null) throw new NBoxException(ucContaContabil, "Conta contábil obrigatório");
        if (ucCentroCusto.IdCentroCusto == null) throw new NBoxException(ucCentroCusto, "Centro de custo obrigatório");

        if (IdNerp == null)
        {
            var idNerp = db.Nerps.IncluiNerpSemEmpenho(ucFavorecido.IdPessoa.Value, ucContaContabil.IdContaContabil.Value, ucCentroCusto.IdCentroCusto.Value, txtNoNerp.Text, txtNoJustificativa.Text, txtVrItem.Text.To<decimal>(), txtDtVencto.Text.To<DateTime?>(), ucChefe.IdPessoa.Value, ucSuperintendente.IdPessoa.Value);
            ShowInfoBox("NERP cadastrada com sucesso");
            Redirect("Nerp.View.aspx?IdNerp=" + idNerp);
        }
        else
        {
            db.Nerps.AlteraNerpSemEmpenho(IdNerp.Value, ucFavorecido.IdPessoa.Value, ucContaContabil.IdContaContabil.Value, ucCentroCusto.IdCentroCusto.Value, txtNoNerp.Text, txtNoJustificativa.Text, txtVrItem.Text.To<decimal>(), txtDtVencto.Text.To<DateTime?>(), ucChefe.IdPessoa.Value, ucSuperintendente.IdPessoa.Value);
            ShowInfoBox("NERP alterada com sucesso");
            Redirect("Nerp.View.aspx?IdNerp=" + IdNerp);
        }
    }

</script>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <div class="mt-30 c-primary-blue">
        <span>Informações gerais</span>
    </div>

    <div class="form-div">
        <div class="sub-form-row">
            <div class="div-sub-form form-uc">
                <div><span>Favorecido</span></div>
                <uc:Pessoa runat="server" ID="ucFavorecido" Required="true" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Conta contábil</span></div>
                <uc:ContaContabil runat="server" ID="ucContaContabil" NrContaFiltro="6" Required="true" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Centro de custo</span></div>
                <uc:CentroCusto runat="server" ID="ucCentroCusto" Required="true" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Descrição</span></div>
                <box:NTextBox runat="server" ID="txtNoNerp" Width="100%" TextCapitalize="Upper" MaxLength="100" Required="true" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Justificativa</span></div>
                <box:NTextBox runat="server" ID="txtNoJustificativa" Width="100%" TextCapitalize="Upper" TextMode="MultiLine" Rows="4" MaxLength="500" Required="true" />
            </div>
        </div>
        <div class="sub-form-row border-bottom">
            <div class="div-sub-form div-sub-form">
                <div>
                    <div><span>Vencimento</span></div>
                    <box:NTextBox runat="server" ID="txtDtVencto" cssClass="form-end" MaskType="Date" Width="356" Required="true" />
                </div>
            </div>
            <div class="div-sub-form div-sub-form margin-left">
                <div>
                    <div><span>Valor</span></div>
                    <box:NTextBox runat="server" ID="txtVrItem" cssClass="form-end" Text="0,00" MaskType="Decimal" Width="356" Required="true" />
                </div>
            </div>
            <div class="div-sub-form">
                <box:NToolBar runat="server" CssClass="bt-fomr-emit">
                    <box:NButton runat="server" ID="btnSalvarSemEmp" Text="Emitir" Width="150" Theme="Primary" OnClick="btnSalvarSemEmp_Click" />
                </box:NToolBar>
            </div>
        </div>
    </div>

    <div class="mt-30 c-primary-blue">
        <span>Autorizações</span>
    </div>

     
    <div class="form-div">
        <div class="sub-form-row">
            <div class="div-sub-form form-uc">
                <div><span>Chefe imediato</span></div>
                <uc:Pessoa runat="server" style="border-radius: 0px !important;" IsFuncionario="true" ID="ucChefe" Required="true" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form form-uc">
                <div><span>Superintendente</span></div>
                <uc:Pessoa runat="server" CssClass="radius-right-0" IsFuncionario="true" ID="ucSuperintendente" Required="true" />
            </div>
        </div>
    </div>

</asp:Content>