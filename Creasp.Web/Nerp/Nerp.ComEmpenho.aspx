﻿<%@ Page Title="Contrato" Resource="nerp.comemp" Language="C#" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();
    protected ISiscont siscont = Factory.Get<ISiscont>();

    protected int? IdNerp => Request.QueryString["IdNerp"].To<int?>();
    protected int? idFornecedor = null;

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO"};
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        if (!string.IsNullOrEmpty(Request.QueryString["idSolicitacao"]))
        {
            var idSolicitação = Convert.ToInt32(Request.QueryString["idSolicitacao"]);
            var solicitacao = db.Solicitacao.ObtemPorId(idSolicitação);

            if (solicitacao != null && solicitacao?.Status == StatusSolicitacao.Aprovada)
            {
                txtVrItem.Text = solicitacao.ValorBruto;
                idFornecedor = db.Pessoas.ObtemIdFornecedor(solicitacao.IdPessoa);

                if (!string.IsNullOrEmpty(solicitacao.NrEmpenho))
                {
                    MontaEmpenho(Convert.ToInt32(solicitacao.NrEmpenho), null, null);
                }

            }
        }

        grdEmpenhos.RegisterPagedDataSource((p) =>
        {
            return siscont.ConsultaEmpenho(db.NrAnoBase,
                null,
                ucPesqFavorecido.Pessoa?.NrCpfFmt,
                txtPesqNrEmp.Text.To<int?>(),
                txtPesqProcessoL.Text.To<string>(),
                null)
                .ToPage(p);
        });
    }

    protected override void OnPrepareForm()
    {
        this.BackTo("Nerp.aspx", false);

        if (IdNerp != null)
        {
            MontaTela();
        }
    }

    protected void MontaTela()
    {
        var nerp = db.SingleById<Nerp>(IdNerp);
        var emp = db.Nerps.ListaEmpenhos(IdNerp.Value).SingleOrDefault(); // só tem 1 empenho
        var item = db.Nerps.ListNerpItem(Pager.NoPage, IdNerp.Value, null).Items.SingleOrDefault(); // só tem 1 item
        var assinatura = db.Query<NerpAssinatura>().Where(x => x.IdNerp == IdNerp && x.TpAprovacao == TpAprovacao.Gestor).Single();

        if (nerp.TpSituacao != NerpSituacao.Cadastrada) throw new NBoxException("Esta NERP não pode ser editada");
        if (nerp.TpNerp != TpNerp.ComEmpenho && nerp.TpNerp != TpNerp.SemEmpenho) throw new NBoxException("Esta NERP não é do tipo contrato");

        MontaEmpenho(emp.NrEmpenho.Value, emp.FgRestoAPagar, emp.FgProcessado);
        cmbCentroCusto.SelectedValue = emp.VrCentroCusto.Keys.FirstOrDefault();
        btnSalvarComEmp.Text = "Alterar NERP";

        txtNoJustificativa.Text = nerp.NoJustificativa;
        txtNoNerp.Text = nerp.NoNerp;
        txtDtVencto.SetText(nerp.DtVencto);
        txtVrItem.SetText(item.VrTotal);

        ucGestor.LoadPessoa(assinatura.IdPessoa);
    }

    protected void btnPesquisar_Click(object sender, EventArgs e)
    {
        grdEmpenhos.DataBind();
    }

    protected void btnPesqEmpenho_Click(object sender, EventArgs e)
    {
        pnlEmpenho.Open(btnPesqEmpenho, txtPesqNrEmp);
    }

    protected void grdEmpenhos_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "sel")
        {
            var nrEmp = e.DataKey<int>(sender, "Numero");
            var fgRestoAPagar = e.DataKey<bool>(sender, "RestoAPagar");
            var fgProcessado = e.DataKey<bool>(sender, "Processado");

            MontaEmpenho(nrEmp, fgRestoAPagar,fgProcessado);
            pnlEmpenho.Close();
        }
    }

    protected void MontaEmpenho(int nrEmp, bool? restoAPagar, bool? processado)
    {
        var empenho = siscont.ConsultaEmpenho(db.NrAnoBase, null, null, nrEmp, null, restoAPagar, processado).FirstOrDefault();
        if (empenho == null) throw new NBoxException("Empenho {0} não encontrado no siscont", nrEmp);

        txtNrEmpenho.Text = empenho.Numero.ToString();
        lblNoEmpHist.Text = empenho.Historico;
        lblNoFavorecido.Text = empenho.FavorecidoCPFCNPJ + " - " + empenho.FavorecidoNome;
        lblNoContaContabil.Text = empenho.PlanoContaCodigo + " - " + empenho.PlanoContaNome;
        cmbCentroCusto.BindMultiColumn(db.CentroCustos.ListaSubAreas(empenho.CentrosCustos.Select(x => x.Identificador)).Select(x => new { x.NrCentroCusto, x.NrCentroCustoFmt, x.NoCentroCusto }), false, true);
        if (empenho.Historico.Length >= 500)
            txtNoJustificativa.Text = empenho.Historico.Substring(0, 499);
        else
            txtNoJustificativa.Text = empenho.Historico;
        txtNoNerp.Text = empenho.FavorecidoNome;
        lblRestoAPagar.Visible = empenho.RestoAPagar;
        lblProcessado.Visible = empenho.Processado;

        if(string.IsNullOrEmpty(empenho.GestorContratoCPF))
        {
            ucGestor.LoadPessoa(null);
            ucGestor.Enabled = true;
        }
        else
        {
            var gestor = db.Pessoas.BuscaOuIncluiPessoa(empenho.GestorContratoCPF.UnMask());

            ucGestor.LoadPessoa(gestor.IdPessoa);
            ucGestor.Enabled = false;
        }

        trHistorico.Visible = trFavorecido.Visible = trContaContabil.Visible = trCentroCusto.Visible = trDescription.Visible = trGestor.Visible = trJustify.Visible = trValue.Visible = trButton.Visible = true;
    }

    protected void btnSalvarComEmp_Click(object sender, EventArgs e)
    {
        txtNrEmpenho.Validate().Required();

        if (IdNerp == null)
        {
            var idNerp = db.Nerps.IncluiNerpComEmpenho(txtNrEmpenho.Text.To<int>(), lblRestoAPagar.Visible, lblProcessado.Visible, ucGestor.IdPessoa.Value, cmbCentroCusto.SelectedValue, txtNoNerp.Text, txtNoJustificativa.Text, txtVrItem.Text.To<decimal>(), txtDtVencto.Text.To<DateTime?>(), idFornecedor);
            ShowInfoBox("NERP cadastrada com sucesso");
            pnlOk.Open();
        }
        else
        {
            db.Nerps.AlteraNerpComEmpenho(IdNerp.Value, txtNrEmpenho.Text.To<int>(), lblRestoAPagar.Visible, lblProcessado.Visible, ucGestor.IdPessoa.Value, cmbCentroCusto.SelectedValue, txtNoNerp.Text, txtNoJustificativa.Text, txtVrItem.Text.To<decimal>(), txtDtVencto.Text.To<DateTime?>());
            ShowInfoBox("NERP alterada com sucesso");
            Redirect("Nerp.View.aspx?IdNerp=" + IdNerp);
        }
    }

    protected void btnIncio_Click(object sender, EventArgs e)
    {
        pnlOk.Close();
        Redirect("~Default.aspx");
    }

    protected void btnNvContrato_Click(object sender, EventArgs e)
    {
        pnlOk.Close();
        Redirect("~/nerp/Nerp.ComEmpenho.aspx");
    }
</script>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <div class="mt-30 c-primary-blue">
        <span>Selecionar empenho</span>
    </div>
    <div class="form-div">
        <div class="sub-form-row">
            <div class="div-sub-form" style="max-width: 300px">
                <div><span>Empenho</span></div>
                <span class="input-group">
                    <box:NTextBox runat="server" ID="txtNrEmpenho" CssClass="radius-right-0" Enabled="false" Required="true" />
                    <box:NButton runat="server" CssClass="btn-radius" ID="btnPesqEmpenho" Icon="search" ToolTip="Pesquisar empenho" OnClick="btnPesqEmpenho_Click" CausesValidation="false"/>
                </span>
                <box:NLabel runat="server" ID="lblRestoAPagar" Text="Resto a pagar" Theme="Default" Visible="false" />
                <box:NLabel runat="server" ID="lblProcessado" Text="Processado" Theme="Default" Visible="false" />
            </div>
        </div>
        <div class="sub-form-row" runat="server" id="trHistorico" visible="false">
            <div class="div-sub-form">
                <div><span>Histórico</span></div>
                <span class="field"><box:NLabel runat="server" ID="lblNoEmpHist" /></span>
            </div>
        </div>
        <div class="sub-form-row" runat="server" id="trFavorecido" visible="false">
            <div class="div-sub-form">
                <div><span>Favorecido</span></div>
                <span class="field"><box:NLabel runat="server" ID="lblNoFavorecido" /></span>
            </div>
            <div class="div-sub-form margin-left" runat="server" id="trContaContabil" visible="false">
                <div><span>Conta contábil</span></div>
                <span class="field"><box:NLabel runat="server" ID="lblNoContaContabil" /></span>
            </div>
            <div class="div-sub-form margin-left" runat="server" id="trCentroCusto" visible="false">
                <div><span>Centro de custo</span></div>
                <box:NDropDownList runat="server" ID="cmbCentroCusto" Width="100%" Required="true" />
            </div>
        </div>
        <div class="sub-form-row" >
            
        </div>
        <div class="sub-form-row" runat="server" id="trDescription" visible="false">
            <div class="div-sub-form">
                <div><span>Descrição da NERP</span></div>
                <box:NTextBox runat="server" ID="txtNoNerp" Width="100%" TextCapitalize="Upper" MaxLength="100" Required="true" />
            </div>
            <div class="div-sub-form margin-left" runat="server" id="trGestor" visible="false">
                <div><span>Gestor do contrato</span></div>
                <uc:Pessoa runat="server" ID="ucGestor" Required="true" IsFuncionario="true" />
            </div>
        </div>
        <div class="sub-form-row" runat="server" id="trJustify" visible="false">
            <div class="div-sub-form">
                <div><span>Justificativa</span></div>
                <box:NTextBox runat="server" ID="txtNoJustificativa" Width="100%" TextCapitalize="Upper" TextMode="MultiLine" Rows="4" MaxLength="500" Required="true" />
            </div>
        </div>
        <div class="sub-form-row border-bottom" runat="server" id="trValue" visible="false">
            <div class="div-sub-form div-sub-form">
                <div>
                    <div><span>Vencimento</span></div>
                    <box:NTextBox runat="server" ID="txtDtVencto" cssClass="form-end" MaskType="Date" Width="356" Required="true" />
                </div>
            </div>
            <div class="div-sub-form div-sub-form margin-left">
                <div>
                    <div><span>Valor</span></div>
                    <box:NTextBox runat="server" ID="txtVrItem" cssClass="form-end" Text="0,00" MaskType="Decimal" Width="356" Required="true" />
                </div>
            </div>
            <div class="div-sub-form">
                <box:NToolBar runat="server" CssClass="bt-fomr-emit" id="trButton" visible="true">
                    <box:NButton runat="server" ID="btnSalvarComEmp" Text="Emitir" Width="150" Height="40" Theme="Primary" OnClick="btnSalvarComEmp_Click" />
                </box:NToolBar>
            </div>
        </div>
    </div>

    <box:NPopup runat="server" ID="pnlEmpenho" Title="Pesquisa empenhos no SISCONT" Width="950" Height="650">
        <box:NTab runat="server" Text="Empenhos" Selected="true" GroupName="emp">
            <div class="form-div">
                <div class="sub-form-row" style="margin-top: -20px !important">
                    <div class="div-sub-form">
                        <div><span>Empenho</span></div>
                        <box:NTextBox runat="server" ID="txtPesqNrEmp" MaskType="Integer" Width="300" />
                    </div>
                    <div class="div-sub-form">
                        <div><span>Contrato L</span></div>
                        <box:NTextBox runat="server" ID="txtPesqProcessoL" Width="300" TextCapitalize="Upper" />
                    </div>
                </div>
                <div class="sub-form-row">
                    <div class="div-sub-form form-uc">
                        <div><span>Favorecido</span></div>
                        <uc:Pessoa runat="server" ID="ucPesqFavorecido" />
                        <div class="help">Caso o favorecido não seja encontrado pesquisa, clique em <box:NLabel runat="server" Theme="Info">+</box:NLabel> para adicionar</div>
                    </div>
                </div>
            </div>
            <box:NToolBar runat="server" CssClass="no-border-bottom no-margin-bottom flex">
                <box:NButton runat="server" ID="btnPesquisar" Icon="search" Text="Pesquisar" OnClick="btnPesquisar_Click" ValidationGroup="pesq" />
            </box:NToolBar>
            <box:NGridView runat="server" ID="grdEmpenhos" ItemType="Creasp.Siscont.Despesa.IntegracaoEmpenhosEntity" DataKeyNames="Numero,RestoAPagar,Processado" PageSize="25" OnRowCommand="grdEmpenhos_RowCommand">
                <Columns>
                    <asp:ButtonField DataTextField="Numero" HeaderText="#" HeaderStyle-Width="45" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" CommandName="sel" />

                    <asp:TemplateField HeaderText="Favorecido">
                        <ItemTemplate>
                            <%# Item.FavorecidoNome %>
                            <div>
                                <small><%# Item.Historico %></small>
                            </div>                            
                            <box:NLabel runat="server" Theme="Default" Text="Aditivo" Visible =<%# Item?.Aditivo == true %> />                            
                            <box:NLabel runat="server" Theme="Default" Text="Resto a pagar" Visible=<%# Item.RestoAPagar %> />
                            
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Conta Contábil" HeaderStyle-Width="160" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                        <ItemTemplate>
                            <%# Item.PlanoContaCodigo %>
                            <div class=""><small><%# Item.PlanoContaNome %></small></div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Valor" HeaderStyle-Width="120" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right">
                        <ItemTemplate>
                            <%# Item.ValorEmpenhado.ToString("n2") %>
                            <div class="left-right space-top">
                                <small>Saldo:</small>
                                <small><%# Item.SaldoEmpenho.ToString("n2") %></small>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <EmptyDataTemplate>Não foi encontrado nenhum empenho no SISCONT com este filtro</EmptyDataTemplate>
            </box:NGridView>
        </box:NTab>
    </box:NPopup>

    <div class="popup-secundary">
        <box:NPopup runat="server" ID="pnlOk" Title="Tributo/Retenção enviado" Width="550">
            <div class="popup-wrapper" style="height:415px">
                <div class="mb-50"><a class="n-popup-close" href="javascript:;" onclick="__doPostBack('ctl00$content$pnlOk','close')"></a></div>
                <i class="far fa-check-circle"></i>
                <p>NERP cadastrada com sucesso!</p>
                <span style="color:black;margin-top: -35px;margin-bottom: 60px;">Deseja continuar cadastrando?</span>
                <div class="d-flex">
                    <div class="link-button-default" style="width: 205px; margin-right:15px">
                        <box:NLinkButton runat="server" ID="btnIncio" Text="Tela inicial" OnClick="btnIncio_Click" />
                    </div>
                    <div class="link-button-primary" style="width: 245px; margin-left:15px">
                        <box:NLinkButton runat="server" ID="btnNvContrato" Theme="Primary" Text="Novo Contrato" OnClick="btnNvContrato_Click" />
                    </div>
                </div>
            </div>
        </box:NPopup>
    </div>

</asp:Content>