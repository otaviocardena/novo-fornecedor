﻿<%@ Page Title="Evento" Resource="nerp.evento" Language="C#" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();

    protected List<NerpParticipanteDTO> Participantes { get { return ViewState["Participantes"] as List<NerpParticipanteDTO>; } set { ViewState["Participantes"] = value; } }

    protected int IdPessoa { get { return ViewState["IdPessoa"].To<int>(0); } set { ViewState["IdPessoa"] = value; } }

    protected int IdEvento => Request.QueryString["IdEvento"].To<int>();

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        grdEvento.RegisterPagedDataSource((p) => db.Nerps.ListaEvento(p, txtDtEvento.Text.To<DateTime?>(), txtNoEvento.Text.To<string>(), tabAdianta.Selected));
    }

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void tabAdianta_TabChanged(object sender, EventArgs e)
    {
       grdEvento.DataBind();
       pResults.Visible = true;
    }

    protected void txtDtEvento_TextChanged(object sender, EventArgs e)
    {
        grdEvento.DataBind();
        pResults.Visible = true;
    }

</script>
<asp:Content ContentPlaceHolderID="content" runat="server">
    <script> 
        try {
            //Create class in div
            var x = document.querySelector(".n-grid-footer").parentNode;
            x.setAttribute('id', 'table-wrapper');
        }
        catch{

        }

        try {

            //Remove text drop-menu
            var registros = document.querySelector('.n-grid-paging');
            var vet = [];
            let spanSelected = null;

            for (let i = 0; i <= 3; i++) vet.push(registros.getElementsByTagName('a')[i]);

            spanSelected = registros.getElementsByTagName('span')[0];

            for (let i = 0; i < vet.length; i++) {
                if (Number(vet[i].innerText) > Number(spanSelected.innerText)) {
                    vet.splice(i, 0, spanSelected);
                    break;
                } else if (Number(spanSelected.innerText) > Number(vet[vet.length - 1].innerText)) {
                    vet.splice(vet.length, 0, spanSelected);
                    break;
                }
            }

            var p = document.createElement('p');
            p.setAttribute('class', 'm-view');
            p.innerText = "VISUALIZAR";

            vet.splice(0, 0, p);

            registros.innerHTML = ""
            for (let i = 0; i <= 5; i++) registros.appendChild(vet[i])

            //Calc count of pages
            let totalReg = Number(document.querySelector('.n-grid-total').innerText.split(':')[1]);
            let pageCurrent = Number(document.querySelector('.n-grid-pager tr td span').innerText);
            var countTotalPages = Math.round(totalReg / Number(spanSelected.innerText));

            document.querySelector('.n-grid-pager td table').setAttribute("style", "display: none");
            let aBack = () => {
                if (pageCurrent <= 1) {
                    return `<span><i class="fas fa-chevron-left c-arrow-not-selected"></i></span>`;
                }

                return `<a href="javascript: __doPostBack('ctl00$content$grdList', 'Page$${pageCurrent - 1}')"><i class="fas fa-chevron-left select-true"></i></a>`;
            }

            let aNext = () => {

                if (pageCurrent >= countTotalPages) {
                    return `<span><i class="fas fa-chevron-right c-arrow-not-selected"></i></span>`;
                }

                return `<a href="javascript: __doPostBack('ctl00$content$grdEvento', 'Page$${pageCurrent + 1}')"><i class="fas fa-chevron-right select-true"></i></a>`;
            }


            let divPager = document.createElement('div');
            divPager.setAttribute('class', 'n-paginator');
            divPager.innerHTML = `<span>${pageCurrent} - ${countTotalPages} de ${totalReg}</span>
                                    ${aBack()}
                                    ${aNext()}`;
            let tablePaginator = document.querySelector('.n-grid-pager td');
            tablePaginator.appendChild(divPager);

        }
        catch {

        }


        try {
            //Add icon arrow down
            var gridTotal = document.querySelector('.n-grid-total');

            var i = document.createElement('i');
            i.setAttribute('class', 'fas fa-sort-amount-down-alt ml-8 c-primary-yellow');
            i.innerHTML = '&nbsp;&nbsp;&nbsp;';

            gridTotal.appendChild(i);
        } catch {
        }


    </script>
        <box:NTab runat="server" ID="tabReemb" Text="Ressarcimentos" Selected="true" TabCssClass="no-tab" AutoPostBack="true" OnTabChanged="tabAdianta_TabChanged"></box:NTab>
        <box:NTab runat="server" ID="tabAdianta" Text="Adiantamentos" AutoPostBack="true" OnTabChanged="tabAdianta_TabChanged"></box:NTab>

        <div class="event-wrapper">
            <p>Selecionar Evento | Pesquisar</p>
            <div class="form-div">
                <div class="sub-form-row">
                        <div class="div-sub-form">
                            <div><span>Data do evento</span></div>                       
                            <box:NTextBox runat="server" ID="txtDtEvento" MaskType="Date" AutoPostBack="true" OnTextChanged="txtDtEvento_TextChanged" />
                        </div>

                        <div class="div-sub-form">
                            <div><span>Descrição</span></div>
                            <box:NTextBox runat="server" ID="txtNoEvento" OnTextChanged="txtDtEvento_TextChanged"/>
                        </div>
                    </div>
            </div>
        </div>

        <div class="event-wrapper-grid">
            <p runat="server" id="pResults" visible="false" style="margin-bottom:-15px">Resultados</p>
            <box:NGridView runat="server" ID="grdEvento" PageSize="25" AllowCustomPaging="false">
                <Columns>
                    <asp:HyperLinkField DataTextField="DtEvento" DataNavigateUrlFields="IdEvento" DataNavigateUrlFormatString="Nerp.Evento.aspx?IdEvento={0}" HeaderText="Data" DataTextFormatString="{0:dd/MM/yyyy HH:mm}" HeaderStyle-Width="160" SortExpression="DtEvento" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                    <asp:BoundField DataField="NoEvento" HeaderText="Evento" SortExpression="NoEvento" />
                </Columns>
                <EmptyDataTemplate>Nenhum evento na data informada</EmptyDataTemplate>
            </box:NGridView>
        </div>
</asp:Content>