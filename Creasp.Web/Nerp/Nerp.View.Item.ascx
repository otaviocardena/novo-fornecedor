﻿<%@ Control Language="C#" %>
<script runat="server">

    protected CreaspContext db;

    protected int IdNerp => Request.QueryString["IdNerp"].To<int>();

    protected int? IdItemEdit { get { return ViewState["IdItemEdit"].To<int?>(); } set { ViewState["IdItemEdit"] = value; } }

    protected Action remontaAbas;

    public void OnRegister(CreaspContext db, NerpPermissao permissao, Action remontaAbas)
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO", "ALL.FORNECEDOR" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Page.Redirect("~/Login.aspx", false, true);
        }
        
        if (Auth.Current<CreaspUser>().Fornecedor)
        {
            btnSalvaDespesa.Visible = false;
        }

        this.db = db;
        this.remontaAbas = remontaAbas;

        grdItems.RegisterPagedDataSource((p) => db.Nerps.ListNerpItem(p, IdNerp, txtFiltro.Text.To<string>()));

        Page.RegisterResource(permissao.FgEditarItem).Disabled(btnIncluiItem).GridColumns(grdItems, "alt", "del");

        Page.SetDefaultButton(btnFiltrar);
    }

    public void MontaTela()
    {
        cmbDespesa.Bind(db.Despesas.ListaTipoDespesa().Select(x => new {  x.CdTipoDespesa, x.NoTipoDespesa }));

        grdItems.DataBind();
    }

    protected void btnFiltrar_Click(object sender, EventArgs e)
    {
        grdItems.DataBind();
    }

    protected void btnLimpar_Click(object sender, EventArgs e)
    {
        txtFiltro.Text = "";
        grdItems.DataBind();
    }

    protected void btnIncluiItem_Click(object sender, EventArgs e)
    {
        cmbDespesa.SelectedValue = "";
        IdItemEdit = null;
        cmbDespesa_SelectedIndexChanged(null, null);
        ucCentroCusto.LoadCentroCusto(null);
        ucPessoa.LoadPessoa(null);

        cmbDespesa.Enabled = ucPessoa.Enabled = true;

        pnlDespesa.Open(btnSalvaDespesa, cmbDespesa);
    }

    protected void grdItems_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "alt")
        {
            IdItemEdit = e.DataKey<int>(sender);
            var item = db.SingleById<NerpItem>(IdItemEdit);

            cmbDespesa.SelectedValue = item.CdTipoDespesa;
            cmbDespesa_SelectedIndexChanged(null, null);
            ucCentroCusto.LoadCentroCusto(item.IdCentroCusto);
            ucPessoa.LoadPessoa(item.IdPessoaCredor);
            txtQtUnit.SetText(item.QtItem);
            txtVrUnit.SetText(item.VrUnit);
            txtQtUnit_TextChanged(null, null);

            cmbDespesa.Enabled = ucPessoa.Enabled = false;
            pnlDespesa.Open(btnSalvaDespesa, txtQtUnit);

            remontaAbas();
        }
        else if(e.CommandName == "del")
        {
            db.Nerps.ExcluiNerpItem(e.DataKey<int>(sender));
            grdItems.DataBind();

            Page.ShowInfoBox("Item excluido com sucesso");

            remontaAbas();
        }
    }

    protected void btnSalvaDespesa_Click(object sender, EventArgs e)
    {
        if (IdItemEdit == null)
        {
            db.Nerps.IncluiNerpItem(IdNerp,
                cmbDespesa.SelectedValue,
                ucPessoa.IdPessoa.Value,
                ucCentroCusto.IdCentroCusto.Value,
                txtQtUnit.Text.To<decimal>(),
                txtVrUnit.Text.To<decimal>());

            Page.ShowInfoBox("Item incluido com sucesso");
        }
        else
        {
            db.Nerps.AlteraNerpItem(IdItemEdit.Value,
                ucCentroCusto.IdCentroCusto.Value,
                txtQtUnit.Text.To<decimal>(),
                txtVrUnit.Text.To<decimal>());

            Page.ShowInfoBox("Item alterado com sucesso");
        }

        pnlDespesa.Close();
        grdItems.DataBind();
        remontaAbas();
    }

    protected void cmbDespesa_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(cmbDespesa.HasValue())
        {
            // busca a data do evento (ou usa a data atual)
            var dtEvento = db.Query<Nerp>()
                .Include(x => x.Evento)
                .Where(x => x.IdNerp == IdNerp && x.IdEvento != null)
                .ProjectTo(x => x.Evento.DtEvento);

            var dt = dtEvento.Count == 1 ? dtEvento.First() : DateTime.Now;

            var tipoDespesa = db.Despesas.BuscaTipoDespesa(cmbDespesa.SelectedValue);

            txtVrUnit.SetText(tipoDespesa.VrUnit(dt));
            lblNoContaContabil.Text = tipoDespesa.ContaContabil.NoContaContabilFmt;
        }
        else
        {
            txtVrUnit.SetText(0);
            lblNoContaContabil.Text = "";
        }
        txtQtUnit.SetText(1);
        txtQtUnit_TextChanged(null, null);
    }

    protected void txtQtUnit_TextChanged(object sender, EventArgs e)
    {
        lblVrTotalDespesa.Text = (txtQtUnit.Text.To<decimal>(0) * txtVrUnit.Text.To<decimal>(0)).ToString("n2");
    }

</script>

<box:NToolBar runat="server">
    <box:NButton runat="server" ID="btnIncluiItem" Icon="plus" Text="Novo item" Theme="Default" OnClick="btnIncluiItem_Click" />
</box:NToolBar>

<div class="form-div">
    <div class="sub-form-row">
        <div class="div-sub-form form-uc">
            <div><span>Nome</span></div>
            <div class="input-group">
            <box:NTextBox runat="server" ID="txtFiltro" TextCapitalize="Upper" Width="100%" />
            <box:NButton runat="server" ID="btnLimpar" Icon="cancel" OnClick="btnLimpar_Click" />
            <box:NButton runat="server" ID="btnFiltrar" Icon="search" Text="Filtrar"  OnClick="btnFiltrar_Click" />
            </div>
        </div>
    </div>
</div>

<box:NGridView runat="server" ID="grdItems" ItemType="Creasp.Nerp.NerpItem" DataKeyNames="IdNerpItem" PageSize="200" OnRowCommand="grdItems_RowCommand">
    <Columns>
        <asp:BoundField DataField="PessoaCredor.NoPessoa" HeaderText="Pessoa" SortExpression="PessoaCredor.NoPessoa" />
        <asp:BoundField DataField="TipoDespesa.NoTipoDespesa" HeaderText="Despesa" SortExpression="TipoDespesa.NoTipoDespesa" HeaderStyle-Width="240" />
        <asp:BoundField DataField="CentroCusto.NrCentroCustoFmt" HeaderText="C. custo" HeaderStyle-Width="95" SortExpression="CentroCusto.NrCentroCusto" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
        <asp:BoundField DataField="VrTotal" HeaderText="Total" DataFormatString="{0:n2}" HeaderStyle-Width="90" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
        <box:NButtonField Icon="pencil" Theme="Info" CommandName="alt" ToolTip="Alterar item" />
        <box:NButtonField Icon="cancel" Theme="Danger" CommandName="del" ToolTip="Excluir item" OnClientClick="return confirm('Confirma excluir este item?');" />
    </Columns>
</box:NGridView>

<box:NPopup runat="server" ID="pnlDespesa" Title="Despesa" Width="750">
    <div class="form-div">
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Despesa</span></div>
                <box:NDropDownList runat="server" ID="cmbDespesa" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="cmbDespesa_SelectedIndexChanged" Required="true" ValidationGroup="add" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Conta contábil</span></div>
                <span class="field"><box:NLabel runat="server" ID="lblNoContaContabil" /></span>
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form form-uc">
                <div><span>Favorecido</span></div>
                <uc:Pessoa runat="server" ID="ucPessoa" Required="true" ValidationGroup="add" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Centro de custo</span></div>
                <uc:CentroCusto runat="server" ID="ucCentroCusto" Required="true" ValidationGroup="add" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form form-despesa">
                <div class="end-line">
                    <div class="m-right-10">
                        <div><span>Quantidade</span></div>
                        <box:NTextBox runat="server" ID="txtQtUnit" MaskType="Decimal" MaskFormat="1" Text="1,0" Width="120" AutoPostBack="true" OnTextChanged="txtQtUnit_TextChanged" Required="true" ValidationGroup="add" />
                    </div>
                    <div class="m-right-10">
                        <div><span>Valor unitário</span></div>
                        <box:NTextBox runat="server" ID="txtVrUnit" MaskType="Decimal" Text="0,00" Width="120" AutoPostBack="true" OnTextChanged="txtQtUnit_TextChanged" Required="true" ValidationGroup="add" />
                    </div>
                    <div class="m-right-15">
                        <div><span>Total</span></div>
                        <box:NLabel CssClass="field t-bold t-right" runat="server" ID="lblVrTotalDespesa" Text="0,00" Width="120" />
                    </div>
                </div>
                <div>
                    <span><box:NButton runat="server" ID="btnSalvaDespesa" Icon="ok" Text="Salvar" Width="200" Theme="Primary" OnClick="btnSalvaDespesa_Click" ValidationGroup="add" /></span>
                </div>
            </div>
        </div>
    </div>
</box:NPopup>


