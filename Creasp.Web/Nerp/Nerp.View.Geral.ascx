﻿<%@ Control Language="C#" %>
<script runat="server">

    protected CreaspContext db;

    protected int IdNerp => Request.QueryString["IdNerp"].To<int>();

    protected Action remontaAbas;

    public void OnRegister(CreaspContext db, NerpPermissao permissao, Action remontaAbas)
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO", "ALL.FORNECEDOR" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Page.Redirect("~/Login.aspx", false, true);
        }

        if (Auth.Current<CreaspUser>().Fornecedor)
        {
            btnExecutar.Visible = false;
            rptWorkflow.Visible = false;
        }

        this.db = db;
        this.remontaAbas = remontaAbas;

        Page.RegisterResource(permissao.FgAcao).Disabled(rptWorkflow);
    }

    public void MontaTela()
    {
        var workflow = db.Nerps.GetWorkflow(IdNerp);
        var nerp = workflow.Nerp;
        var solicitadoPagtoAdiant = db.Query<NerpItem>()
                                .Where(x => x.IdNerp == nerp.IdNerp)
                                .Where(x => x.NrSolicAdiantamento != null || x.NrSolicPagto != null) // tem solic mas nao tem confirmação
                                .Any();

        //ltrCaption.InnerText = nerp.NoTpNerp;
        lblNoSituacao.Text = NerpSituacao.NoSituacao(nerp.TpSituacao);
        lblNoSituacao.Theme = nerp.FgAtivo ? ThemeStyle.None : ThemeStyle.Danger;

        //title nerp
        lblNerpTitle.Text = "Contrato | NERP # " + nerp.NrNerpFmt;

        lblDtEmissao.Text = nerp.DtEmissao.ToString("d");
        lblNoResponsavel.Text = nerp.NoResponsavel;

        lblDtVencto.Text = nerp.DtVencto?.ToString("d");

        //lblNoNerp.Text = nerp.NoNerp;
        lblNoJustificativa.Text = nerp.NoJustificativa;

        lblNoHistorico.Text = nerp.NoHistorico;
        trHist.Visible = nerp.NoHistorico != null;

        trEvento.Visible = nerp.Evento != null;

        if (nerp.Evento != null)
        {
            lnkEvento.Text = nerp.Evento.NoEvento + " :: " + nerp.Evento.DtEventoFmt;
            lnkEvento.NavigateUrl = "../Siplen/Evento.View.aspx?IdEvento=" + nerp.Evento.IdEvento;
        }

        grdTotal.DataSource = db.Nerps.ListaNerpItemAgrupado(IdNerp);
        grdTotal.DataBind();

        grdTotal.Rows.Cast<GridViewRow>().Last().CssClass = "t-bold";

        rptAssinaturas.DataSource = nerp.Assinaturas;
        rptAssinaturas.DataBind();

        rptWorkflow.DataSource = workflow.AcoesDisponiveis();
        rptWorkflow.DataBind();

        btnCancelar.Enabled = nerp.FgAtivo && Page.IsProprietario(nerp, "nerp.cancela") && !solicitadoPagtoAdiant;
    }

    protected void btnExecutar_Command(object sender, CommandEventArgs e)
    {
        if (e.CommandName == "cancelar")
        {
            db.Nerps.CancelaNerp(IdNerp, txtNoMotivo.Text);
            Page.Redirect("Nerp.View.aspx?IdNerp=" + IdNerp);
            Page.ShowInfoBox("NERP cancelada com sucesso");
        }
        else
        {
            db.Nerps.GetWorkflow(IdNerp).ExecucaoAcao(e.CommandName, txtNoMotivo.Text);
            Page.Redirect("Nerp.View.aspx?IdNerp=" + IdNerp);
            Page.ShowInfoBox("Operação realizada sucesso");
        }
    }

    protected void btnAcao_Command(object sender, CommandEventArgs e)
    {
        var btn = sender as NButton;

        // se botão for Danger, exige "Motivo"
        if (btn.Theme == ThemeStyle.Danger)
        {
            btnExecutar.CommandName = e.CommandName;
            pnlMotivo.Title = e.CommandName;
            pnlMotivo.Open(btnExecutar, txtNoMotivo);
        }
        else
        {
            db.Nerps.GetWorkflow(IdNerp).ExecucaoAcao(e.CommandName, txtNoMotivo.Text);
            Page.Redirect("Nerp.View.aspx?IdNerp=" + IdNerp);
            Page.ShowInfoBox("Operação realizada sucesso");
        }
    }

    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        btnExecutar.CommandName = "cancelar";
        pnlMotivo.Title = "Cancelamento de NERP";
        pnlMotivo.Open(btnExecutar, txtNoMotivo);
    }

</script>

<div class="mt-30 c-primary-blue">
    <box:NLabel runat="server" ID="lblNerpTitle" class="" Font-Bold="true" />
</div>

<div class="form-div">
    <div class="sub-form-row sub-small">
        <div class="div-sub-form">
            <div><span>Situação</span></div>
            <box:NLabel runat="server" ID="lblNoSituacao" class="field small-geral" Font-Bold="true" />
            
        </div>
        <div class="div-sub-form">
            <div><span>Responsável</span></div>
            <box:NLabel runat="server" ID="lblNoResponsavel" class="field small-geral" />
        </div>
    </div>
    <div class="sub-form-row sub-small">
        <div class="div-sub-form">
            <div><span>Emissão</span></div>
            <box:NLabel runat="server" ID="lblDtEmissao" class="field small-geral" />
        </div>
        <div class="div-sub-form">
            <div><span>Vencimento</span></div>
            <box:NLabel runat="server" ID="lblDtVencto" class="field small-geral" />
        </div>
    </div>
    <div class="sub-form-row">
        <div class="div-sub-form">
            <div><span>Justificativa</span></div>
            <box:NLabel runat="server" ID="lblNoJustificativa" class="field" />
        </div>
    </div>
    <div class="sub-form-row">
        <div class="div-sub-form" id="trEvento" runat="server">
            <div><span>Evento</span></div>
            <box:NHyperLink runat="server" ID="lnkEvento" class="field"/>
        </div>
        <div class="div-sub-form" id="trHist" runat="server">
            <div><span>Motivo</span></div>
            <box:NLabel runat="server" ID="lblNoHistorico" class="field" CssClass="pre" />
        </div>
    </div>

    <div class="sub-form-row">
        <box:NGridView runat="server" ID="grdTotal" CssClass="m-0" ItemType="Creasp.Nerp.NerpItemAgrupadoDTO" ShowTotal="false">
            <Columns>
                <asp:BoundField DataField="NoItem" HeaderText="Discriminação das Despesas" />
                <asp:BoundField DataField="NrCentroCustoFmt" HeaderText="Centro Custo" HtmlEncode="false" HeaderStyle-Width="150" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="QtItem" DataFormatString="{0:#.#}" HeaderText="Quantidade" HeaderStyle-Width="130" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="VrTotal" DataFormatString="{0:n2}" HeaderText="Valor em R$" HeaderStyle-Width="130" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
            </Columns>
        </box:NGridView>
    </div>
</div>

<%--<table class="form form-2">
    <tr>
        <td>Descrição</td>
        <td colspan="3"><box:NLabel runat="server" ID="lblNoNerp" /></td>
    </tr>
</table>--%>


<div class="form-div m-0 m-15" style="flex-direction: row">
    <div class="sub-form-row">
        <box:NRepeater runat="server" ID="rptAssinaturas" ItemType="Creasp.Nerp.NerpAssinatura">
            <ItemTemplate>

                <div class="div-sub-form person">
                    <div><span><%# Item.NoAssinante %></span></div>
                    <span class="field dinamic"><%# Item.Pessoa.NoPessoa %> | <%# Item.Unidade.NoUnidade %></span>
                </div>
            </ItemTemplate>
        </box:NRepeater>
    </div>
</div>

<box:NPopup runat="server" ID="pnlMotivo" Title="Motivo">
    <table class="form">
        <tr>
            <td>Motivo</td>
            <td>
                <box:NTextBox runat="server" ID="txtNoMotivo" TextCapitalize="Upper" TextMode="MultiLine" Rows="3" Width="100%" Required="true" ValidationGroup="motivo" />
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <box:NButton runat="server" ID="btnExecutar" Text="Confirmar" Theme="Primary" OnCommand="btnExecutar_Command" ValidationGroup="motivo" />
            </td>
        </tr>
    </table>
</box:NPopup>

<box:NToolBar runat="server">
    <box:NButton runat="server" ID="btnCancelar" Icon="cancel" Text="Cancelar" CssClass="right bt-cancel" OnClick="btnCancelar_Click" ToolTip="Cancelar NERP" />
    <box:NRepeater runat="server" ID="rptWorkflow" ItemType="Creasp.Nerp.NerpWorkflowAcao">
        <ItemTemplate>
            <box:NButton runat="server" CssClass="bts-geral"
                Icon="<%--<%# Item.ButtonIcon %>--%>"
                Theme="<%# Item.ButtonTheme %>"
                Text="<%# Item.Descricao %>"
                CommandName="<%# Item.Descricao %>"
                OnCommand="btnAcao_Command" />
        </ItemTemplate>
    </box:NRepeater>
</box:NToolBar>