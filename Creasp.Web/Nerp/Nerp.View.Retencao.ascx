﻿<%@ Control Language="C#" %>
<script runat="server">

    protected CreaspContext db;

    protected int IdNerp => Request.QueryString["IdNerp"].To<int>();

    protected int? IdRetencaoEdit { get { return ViewState["IdRetEdit"].To<int?>(); } set { ViewState["IdRetEdit"] = value; } }

    protected NerpPermissao permissao;

    public void OnRegister(CreaspContext db, NerpPermissao permissao)
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO", "ALL.FORNECEDOR" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Page.Redirect("~/Login.aspx", false, true);
        }

        if (Auth.Current<CreaspUser>().Fornecedor)
        {
            btnAltRetencao.Visible = false;
        }

        this.db = db;
        this.permissao = permissao;

        grdRetencao.RegisterDataSource(() => db.Nerps.ListaRetencoes(IdNerp));

        Page.RegisterResource(permissao.FgRetencao).Disabled(btnLanctoRet).GridColumns(grdRetencao, "alt", "del");
    }

    public void MontaTela()
    {
        cmbTributo.Bind(Factory.Get<ISiscont>().ListaTributos());

        cmbItemRet.Bind(db.Nerps.ListNerpItem(Pager.NoPage, IdNerp, null).Items
            .Select(x => new { k = x.IdNerpItem, v = x.PessoaCredor.NoPessoa + " => " + x.VrTotal.ToString("c2") }), false, false);

        ucDocs.MontaTela(TpRefDoc.Retencao, IdNerp, null, permissao.FgRetencao || Page.User.IsInRole("nerp.editdoc"));

        grdRetencao.DataBind();
    }

    protected void btnLanctoRet_Click(object sender, EventArgs e)
    {
        cmbItemRet_SelectedIndexChanged(null, null);
        IdRetencaoEdit = null;

        cmbItemRet_SelectedIndexChanged(null, null);
        cmbTributo.SelectedValue = "";
        txtDtVencto.SetText("");
        txtVrRetencao.SetText(0);
        txtPcRetencao.SetText(0);
        tabRetencao.Text = "Incluir retenção";
        cmbItemRet.Enabled = cmbTributo.Enabled = true;

        txtVrRetencao.Enabled = false;
        btnAltRetencao.Visible = true;

        pnlRetencao.Open(btnSalvar, cmbItemRet);
    }

    protected void grdRetencao_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "alt")
        {
            IdRetencaoEdit = e.DataKey<int>(sender);
            var ret = db.SingleById<NerpRetencao>(IdRetencaoEdit);

            cmbItemRet.SelectedValue = ret.IdNerpItem.ToString();
            cmbItemRet_SelectedIndexChanged(null, null);
            // pode mudar o texto da implanta
            try
            {
                cmbTributo.SelectedValue = ret.CdTributo;
            }
            catch(Exception)
            {
                cmbTributo.SelectedValue = "";
            }
            txtDtVencto.SetText(ret.DtVencto);
            txtVrBase.SetText(ret.VrBase);
            txtVrRetencao.SetText(ret.VrRetencao);
            ucFavorecido.LoadPessoa(ret.IdPessoaFav);

            cmbItemRet.Enabled = cmbTributo.Enabled = false;
            tabRetencao.Text = "Alterar retenção";

            txtVrRetencao.Enabled = false;
            btnAltRetencao.Visible = true;

            pnlRetencao.Open(btnSalvar, txtDtVencto);
        }
        else if (e.CommandName == "del")
        {
            db.Nerps.ExcluiRetencao(e.DataKey<int>(sender));
            grdRetencao.DataBind();
            Page.ShowInfoBox("Retenção excluida com sucesso");
        }
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        if (IdRetencaoEdit == null)
        {
            db.Nerps.IncluiRetencao(cmbItemRet.SelectedValue.To<int>(),
                cmbTributo.SelectedValue,
                cmbTributo.SelectedItem.Text,
                txtVrBase.Text.To<decimal>(),
                txtPcRetencao.Text.To<decimal>(),
                txtVrRetencao.Text.To<decimal>(),
                txtDtVencto.Text.To<DateTime?>(),
                ucFavorecido.IdPessoa.Value);

            Page.ShowInfoBox("Retenção incluida com sucesso");
        }
        else
        {
            db.Nerps.AlteraRetencao(IdRetencaoEdit.Value,
                cmbItemRet.SelectedValue.To<int>(),
                txtVrBase.Text.To<decimal>(),
                txtPcRetencao.Text.To<decimal>(),
                txtVrRetencao.Text.To<decimal>(),
                txtDtVencto.Text.To<DateTime?>(),
                ucFavorecido.IdPessoa.Value);

            Page.ShowInfoBox("Retenção incluida com sucesso");
        }

        pnlRetencao.Close();
        grdRetencao.DataBind();
    }

    protected void cmbItemRet_SelectedIndexChanged(object sender, EventArgs e)
    {
        var item = db.SingleById<NerpItem>(cmbItemRet.SelectedValue.To<int>());
        ucFavorecido.LoadPessoa(item.IdPessoaCredor);
        txtVrBase.SetText(item.VrTotal);
    }

    protected void btnAnexarDocs_Click(object sender, EventArgs e)
    {
        pnlDocs.Open();
    }

    protected void txtVrBase_TextChanged(object sender, EventArgs e)
    {
        txtVrRetencao.SetText(txtVrBase.Text.To<decimal>(0) * (txtPcRetencao.Text.To<decimal>(0) / 100));
    }

    protected void btnAltRetencao_Click(object sender, EventArgs e)
    {
        txtVrRetencao.Enabled = true;
        btnAltRetencao.Visible = false;
    }

</script>
<box:NToolBar runat="server">
    <box:NButton runat="server" ID="btnLanctoRet" Icon="th-list" Text="Lançar retenções" Theme="Default" OnClick="btnLanctoRet_Click" />
    <box:NButton runat="server" ID="btnAnexarDocs" Icon="attach" Text="Guias de retenção" Theme="Default" OnClick="btnAnexarDocs_Click" />
</box:NToolBar>

<box:NGridView runat="server" ID="grdRetencao" ItemType="Creasp.Nerp.NerpRetencao" DataKeyNames="IdNerpRetencao" OnRowCommand="grdRetencao_RowCommand">
    <Columns>
        <asp:BoundField DataField="Favorecido.NoPessoa" HeaderText="Favorecido" />
        <asp:BoundField DataField="NoTributo" HeaderText="Tributo" HeaderStyle-Width="280" />
        <asp:BoundField DataField="DtVencto" HeaderText="Vencimento" DataFormatString="{0:d}" HeaderStyle-Width="110" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
        <asp:BoundField DataField="VrBase" HeaderText="Vr. Base" DataFormatString="{0:n2}" HeaderStyle-Width="110" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
        <asp:BoundField DataField="PcRetencao" HeaderText="%" DataFormatString="{0:n2}" HeaderStyle-Width="60" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
        <asp:BoundField DataField="VrRetencao" HeaderText="Vr. Tributo" DataFormatString="{0:n2}" HeaderStyle-Width="110" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
        <box:NButtonField Icon="pencil" Theme="Info" ToolTip="Editar retenção" CommandName="alt" />
        <box:NButtonField Icon="cancel" Theme="Danger" ToolTip="Excluir retenção" CommandName="del" OnClientClick="return confirm('Confirma excluir esta retenção?');" />
    </Columns>
    <EmptyDataTemplate>Nenhum retenção informada</EmptyDataTemplate>
</box:NGridView>

<box:NPopup runat="server" ID="pnlDocs" Title="Guias de retenções" Width="750">
    <uc:Documento runat="server" ID="ucDocs" Titulo="Guias" />
</box:NPopup>

<box:NPopup runat="server" ID="pnlRetencao" Title="Retenção" Width="650">
    <box:NTab runat="server" ID="tabRetencao" Text="Retenção" Selected="true">
        <box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnSalvar" Icon="ok" Text="Salvar" Theme="Primary" OnClick="btnSalvar_Click" ValidationGroup="ret" />
        </box:NToolBar>
        <table class="form">
            <tr>
                <td>Item</td>
                <td>
                    <box:NDropDownList runat="server" ID="cmbItemRet" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="cmbItemRet_SelectedIndexChanged" />
                </td>
            </tr>
            <tr>
                <td>Tributo</td>
                <td>
                    <box:NDropDownList runat="server" ID="cmbTributo" Width="100%" Required="true" ValidationGroup="ret" />
                </td>
            </tr>
            <tr>
                <td>Vencimento</td>
                <td>
                    <box:NTextBox runat="server" ID="txtDtVencto" MaskType="Date" Required="true" ValidationGroup="ret" />
                </td>
            </tr>
            <tr>
                <td>Valor base</td>
                <td>
                    <box:NTextBox runat="server" ID="txtVrBase" Width="120" MaskType="Decimal" Required="true" ValidationGroup="ret" AutoPostBack="true" OnTextChanged="txtVrBase_TextChanged" />
                </td>
            </tr>
            <tr>
                <td>Percentual</td>
                <td>
                    <box:NTextBox runat="server" ID="txtPcRetencao" Width="120" MaskType="Decimal" Required="true" ValidationGroup="ret" AutoPostBack="true" OnTextChanged="txtVrBase_TextChanged" />
                </td>
            </tr>
            <tr>
                <td>Valor retenção</td>
                <td>
                    <box:NTextBox runat="server" ID="txtVrRetencao" Width="120" MaskType="Decimal" Required="true" ValidationGroup="ret" Enabled="false" />
                    <box:NLinkButton runat="server" ID="btnAltRetencao" Text="Alterar" Icon="pencil" Theme="Info" OnClick="btnAltRetencao_Click" />
                </td>
            </tr>
            <tr>
                <td>Favorecido</td>
                <td>
                    <uc:Pessoa runat="server" ID="ucFavorecido" Required="true" ValidationGroup="ret" />
                </td>
            </tr>
        </table>
    </box:NTab>
</box:NPopup>