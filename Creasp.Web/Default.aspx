﻿<%@ Page Title="NERP" Resource="nerp" Language="C#" %>

<script runat="server">

    protected CreaspContext db = new CreaspContext();
    protected int? idFornecedor = null;

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO", "ALL.FORNECEDOR" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        if (Auth.Current<CreaspUser>().Fornecedor)
        {
            Redirect("~/DefaultFornecedor.aspx", false, true);
        }

        var user = Page.User as CreaspUser;

        if (user != null)
        {
            var name = user.Login.Contains("@") ? user.Nome : user.Login;
            lblMsg.InnerText = $"Seja bem-vindo {name} aos sistemas SIPLEN, NERP e ELO. Utilize os links rápidos abaixo ou clique em um dos menus acima.";
        }
    }

</script>
<%--<asp:Content ContentPlaceHolderID="title" runat="server">
    <box:NDropDown runat="server" ID="drpIncluir" Icon="plus" Text="Nova NERP" OnItemClick="drpIncluir_ItemClick" ButtonTheme="Success" Visible="false" />
</asp:Content>--%>
<asp:content contentplaceholderid="content" runat="server">
    
    <div class="c-primary-blue home-description">
        <p runat="server" id="lblMsg" style="font-weight:bolder"/>
    </div>

    <div class="card-wrapper">
        <%--NERP--%>
        <div runat="server" ID="itemNerp" class="card-item">
            <div>
                <i class="fas fa-file-invoice-dollar card-icon"></i>
            </div>
            <box:NLinkButton CssClass="card-description" runat="server" PostBackUrl="Nerp/Nerp.aspx">
                <p>Nerp</p>
            </box:NLinkButton>
        </div>

        <%--Evento--%>
        <div runat="server" ID="itemEvento" class="card-item">
            <div>
                <i class="fas fa-calendar card-icon"></i>
            </div>
            <box:NLinkButton CssClass="card-description" runat="server" PostBackUrl="Siplen/Evento.aspx">
                <p>Eventos</p>
            </box:NLinkButton>
        </div>

        <%--Composição do plenário--%>
        <div runat="server" ID="itemComPl" class="card-item">
            <div>
                <i class="fas fa-handshake card-icon"></i>
            </div>
            <box:NLinkButton CssClass="card-description" runat="server" PostBackUrl="Nerp/Nerp.aspx">
                <p>Composição do plenário</p>
            </box:NLinkButton>
        </div>

        <%--Solicitações--%>
        <div runat="server" ID="itemSol" class="card-item">
            <div>
                <i class="fas fa-exclamation-circle card-icon"></i>
            </div>
            <box:NLinkButton CssClass="card-description" runat="server" PostBackUrl="Solicitacao/Solicitacao.Lista.aspx">
                <p>Solicitações</p>
            </box:NLinkButton>
        </div>
        
        <%--Prospostas--%>
        <div runat="server" ID="itemProp" class="card-item">
            <div>
                <i class="fas fa-file-contract card-icon"></i>
            </div>
            <box:NLinkButton CssClass="card-description" runat="server" PostBackUrl="Solicitacao/Solicitacao.Lista.aspx">
                <p>Propostas orçamentárias</p>
            </box:NLinkButton>
        </div>
        
        <%--Comissões--%>
        <div runat="server" ID="itemCom" class="card-item">
            <div>
                <i class="fas fa-percentage card-icon"></i>
            </div>
            <box:NLinkButton CssClass="card-description" runat="server" PostBackUrl="Solicitacao/Solicitacao.Lista.aspx">
                <p>Comissões</p>
            </box:NLinkButton>
        </div>
    </div>   

    <div class="c-primary-blue home-description">
        <p>Para conferir sua hierarquia no sistema <b>Senior/RH</b>, <box:NHyperLink runat="server" NavigateUrl="admin/default.aspx?Tab=func">clique aqui.</box:NHyperLink></p>
    </div>
</asp:content>
