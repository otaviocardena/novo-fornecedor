﻿<%@ Page Title="Solicitacao Imposto" Resource="nerp.comemp" Language="C#" %>

<script runat="server">

    protected CreaspContext db = new CreaspContext();

    protected void Page_Load(object sender, EventArgs e)
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        if (!string.IsNullOrEmpty(Request.QueryString["idSolicitacao"])) {
            var idSolicitação = Convert.ToInt32(Request.QueryString["idSolicitacao"]);
            var solicitacao = db.Solicitacao.ObtemPorId(idSolicitação);

            if (!string.IsNullOrEmpty(solicitacao?.NrEmpenho ?? ""))
            {
                btnCriaContratoNerp.Visible = true;
            }

            if (solicitacao != null)
            {
                if (solicitacao.Status != StatusSolicitacao.AgCalcImposto)
                {
                    btnEnviar.Visible = false;
                    return;
                }

                try
                {
                    var pessoa = db.Pessoas.BuscaPessoa(solicitacao.IdPessoa, false);
                    txtFavorecido.Text = pessoa.NoPessoa;
                }
                catch {
                    txtFavorecido.Text = txtFavorecido.Text;
                }
            }
            else
            {
                ShowErrorBox("Verifique o id da solicitação passado como parametro");
                btnEnviar.Visible = false;
            }
        }
        else
        {
            ShowErrorBox("Verifique o id da solicitação passado como parametro");
            btnEnviar.Visible = false;
        }
    }

    protected void btnEnviar_Click(object sender, EventArgs e)
    {

    }

    protected void radSim_CheckedChanged(object sender, EventArgs e)
    {
        if (radSim.Checked)
        {
            divBaseVenc.Visible = true;
            divFav.Visible = true;
            divTR.Visible = true;
            divTRFile.Visible = true;
        }

        if (radNao.Checked)
        {
            divBaseVenc.Visible = false;
            divFav.Visible = false;
            divTR.Visible = false;
            divTRFile.Visible = false;
        }
    }

    protected void btnEnviar_Click1(object sender, EventArgs e)
    {
        var idSolicitação = Convert.ToInt32(Request.QueryString["idSolicitacao"]);
        var retorno = db.Solicitacao.IncluiTributosFinalizaSolicitacao(certTributo.PostedFile, txtAliquota.Text, txtBaseCalc.Text, txtVencimento.Text, cmbTR.SelectedValue, idSolicitação);

        if (retorno) pnlOk.Open();
        else ShowErrorBox("Erro ao tentar aprovar solicitação e enviar os dados de tributo");
    }

    protected void btnCriaContratoNerp_Click(object sender, EventArgs e)
    {
        //redirecionar para revisão de nerp do tipo contrato e possivel cadastro
        if (!string.IsNullOrEmpty(Request.QueryString["idSolicitacao"]))
        {
            var idSolicitação = Convert.ToInt32(Request.QueryString["idSolicitacao"]);
            Redirect($"~/Nerp/Nerp.ComEmpenho.aspx?idSolicitacao={idSolicitação.ToString()}");
        }
    }
</script>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <div class="form-div" style="color: black;">
        <div class="sub-form-solicitation-col">
            <span class="title">CALCULAR IMPOSTOS</span>
            <span class="sub-title">Calculo de impostos de retenções</span>
        </div>

        <div>
            <div class="sub-form-arquivo">
                <span>TRIBUTOS/RETENÇÃO</span>
            </div>
            <div class="form-despesa" style="display: flex; justify-content: space-between;margin-top: -30px;margin-bottom: 20px;">
                <p>Aplicar tributação para este pagamento</p>
                <div class="radio-yellow">
                    <box:NRadioButton runat="server" ID="radSim" GroupName="tp" Text="Sim" AutoPostBack="true" OnCheckedChanged="radSim_CheckedChanged" AccessKey="S" />
                    <box:NRadioButton runat="server" ID="radNao" GroupName="tp" Text="Não" AutoPostBack="true" OnCheckedChanged="radSim_CheckedChanged" AccessKey="N" />
                </div>
            </div>
        </div>

        <div class="sub-form-row" runat="server" id="divTR" visible="false">
            <div class="div-sub-form">
                <div><span>TRIBUTO/RETENÇÃO</span></div>
                <box:NDropDownList runat="server" ID="cmbTR" Width="95%">
                    <asp:ListItem Value="" Text="(Selecione uma opção abaixo)" />
                    <asp:ListItem Value="TRIBUTO" Text="Tributo" />
                    <asp:ListItem Value="RETENCAO" Text="Retenção" />
                </box:NDropDownList>
            </div>

            <div class="div-sub-form">
                <div><span>ALIQUOTA</span></div>
                <box:NTextBox runat="server" ID="txtAliquota"/>
            </div>
        </div>

        <div class="sub-form-row" runat="server" id="divBaseVenc" visible="false">
            <div class="div-sub-form">
                <div><span>BASE CÁLCULO</span></div>
                <box:NTextBox runat="server" ID="txtBaseCalc" />
            </div>

            <div class="div-sub-form">
                <div><span>VENCIMENTO</span></div>
                <box:NTextBox runat="server" ID="txtVencimento" MaskType="Date" />
            </div>
        </div>

        <div class="sub-form-row" runat="server" id="divFav" visible="false">
            <div class="div-sub-form">
                <div><span>FAVORECIDO</span></div>
                <box:NTextBox runat="server" ID="txtFavorecido" Width="98%" />
            </div>
        </div>

        <div class="sub-form-row" runat="server" id="divTRFile" visible="false">
            <div class="div-sub-form">
                <div><span class="h1-solicitation"><b>TRIBUTO/RETENÇÃO</b></span></div>
                <div>
                    <input type="file" name="certTributo" runat="server" id="certTributo" class="inputfile" />
                    <label for="content_certTributo"><i class="file fas fa-paperclip"></i></label>
                </div>
            </div>
        </div>

        <box:NButton runat="server" ID="btnEnviar" Text="Enviar" OnClick="btnEnviar_Click1" Theme="Primary" CssClass="btn-long btn-login align-self" />
    </div>

    <div class="popup-secundary">
            <box:NPopup runat="server" ID="pnlOk" Title="Tributo/Retenção enviado" Width="550">
                <div class="popup-wrapper" style="height:415px">
                    <div class="mb-50"><a class="n-popup-close" href="javascript:;" onclick="__doPostBack('ctl00$content$pnlOk','close')"></a></div>
                    <i class="far fa-check-circle"></i>
                    <p>Tributo/Retenção enviado com sucesso, com isso o processo desta solicitação está encerrado!</p>
                    <div class="link-button-primary" style="width: 320px;"><box:NLinkButton runat="server" ID="btnCriaContratoNerp" Text="Ir para criação de nerp" OnClick="btnCriaContratoNerp_Click" Visible="false" /></div>
                </div>
            </box:NPopup>
        </div>
</asp:Content>
