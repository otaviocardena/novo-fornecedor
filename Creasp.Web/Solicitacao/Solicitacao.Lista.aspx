﻿<%@ Page Title="Solicitacao" Resource="nerp.comemp" Language="C#" %>

<script runat="server">

    protected CreaspContext db = new CreaspContext();

    protected void Page_Load(object sender, EventArgs e)
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        grdSolicitacao.RegisterDataSource(() => db.Solicitacao.ObtemPorAprovador(Auth.Current<CreaspUser>().IdPessoa));
        grdSolicitacao.DataBind();
    }

</script>
<asp:content contentplaceholderid="content" runat="server">
    <div class="subTitle-wrapper">
            <span class="subTitle">Solicitações pendentes de sua aprovação</span>
        </div>
    <box:NGridView runat="server" ID="grdSolicitacao" ItemType="Creasp.Core.DTO.SolicitacaoDTO" PageSize="50">
        <Columns>
            <asp:HyperLinkField DataTextField="IdSolicitacao" DataNavigateUrlFields="IdSolicitacao, Etapa" HeaderText="Número" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="80" SortExpression="IdSolicitacao" DataNavigateUrlFormatString="Solicitacao.Aprovacao{1}.aspx?idSolicitacao={0}" />
            <asp:BoundField DataField="DtCadastro" HeaderText="Data da Solicitação" DataFormatString="{0:d}" HeaderStyle-Width="110" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" SortExpression="DtCadastro" />
            <asp:BoundField DataField="NumNf" HeaderText="Numero da Nota Fiscal" HeaderStyle-Width="210" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" SortExpression="NumNF" />
            <asp:BoundField DataField="LoginCadastro" HeaderText="Usuário Solicitante" SortExpression="LoginCadastro" HeaderStyle-Width="310" />
            <asp:BoundField DataField="Status" HeaderText="Etapa Processo" SortExpression="Status" HeaderStyle-Width="310" />
            <asp:BoundField DataField="ValorBruto" HeaderText="Valor" SortExpression="ValorBruto" HeaderStyle-Width="310" />
        </Columns>
        <EmptyDataTemplate>Nenhuma Solicitação aguardando sua aprovação</EmptyDataTemplate>
    </box:NGridView>

</asp:content>
