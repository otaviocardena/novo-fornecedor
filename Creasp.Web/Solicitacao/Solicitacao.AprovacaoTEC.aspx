﻿<%@ Page Title="Solicitacao" Resource="nerp.comemp" Language="C#" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();

    protected void Page_Load(object sender, EventArgs e)
    {
        var validsClaims = new List<string> {"ALL", "ALL.USUARIO"};
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        if (Request.QueryString["idSolicitacao"] == "")
        {
            ShowErrorBox("Nenhuma solicitação com o id informado");
            return;
        }

        var idSolicitação = Convert.ToInt32(Request.QueryString["idSolicitacao"]);
        var solicitacao = db.Solicitacao.ObtemPorId(idSolicitação);

        if (solicitacao == null)
        {
            ShowErrorBox("Nenhuma solicitação com o id informado, por favor reveja o id informado como parametro");
        }

        var dtToString = solicitacao.DataVencimentoBoleto.ToShortDateString();
        txtDtVencimento.Text = dtToString == "01/01/1900" ? "" : dtToString;
        txtValorBruto.Text = solicitacao.ValorBruto;
        lblIdCert.Text = solicitacao.IdCertidao.ToString();
        lblIdRel.Text = solicitacao.IdRelatorio.ToString();
        lblIdNf.Text = solicitacao.IdNF.ToString();

        if (solicitacao.TecId != null && solicitacao.GestorId != null)
        {
            if (Auth.Current<CreaspUser>().IdPessoa == solicitacao.TecId || Auth.Current<CreaspUser>().IdPessoa == solicitacao.GestorId)
            {
                btnAprovado.Visible = true;
                btnRecusado.Visible = true;
                btnMaisInfo.Visible = true;
            }
            else
            {
                btnAprovado.Visible = false;
                btnRecusado.Visible = false;
                btnMaisInfo.Visible = false;
                return;
            }
        }


        if (solicitacao.Status == StatusSolicitacao.AgAprovacaoTEC || solicitacao.Status == StatusSolicitacao.AgMaisInfosTEC && !solicitacao.AprovadoTEC)
        {
            btnAprovado.Visible = true;
            btnRecusado.Visible = true;
            btnMaisInfo.Visible = true;
        }
    }

    protected void relFile_Click(object sender, EventArgs e)
    {
        var idRel = Convert.ToInt32(lblIdRel.Text);
        db.Documentos.Download(this, idRel);
    }

    protected void certFile_Click(object sender, EventArgs e)
    {
        var idCert = Convert.ToInt32(lblIdCert.Text);
        db.Documentos.Download(this, idCert);
    }

    protected void nfFile_Click(object sender, EventArgs e)
    {
        var idNf = Convert.ToInt32(lblIdNf.Text);
        db.Documentos.Download(this, idNf);
    }

    protected void btnAprovado_Click(object sender, EventArgs e)
    {
        db.Solicitacao.AprovaSolicitacaoTEC(Convert.ToInt32(Request.QueryString["idSolicitacao"]));
        ShowInfoBox("Solicitação aprovada e passada para a próxima fase de aprovação!");
        txtDtVencimento.Text = "";
        txtValorBruto.Text = "";
        lblIdCert.Text = "";
        lblIdNf.Text = "";
        btnAprovado.Visible = false;
        btnRecusado.Visible = false;
        btnMaisInfo.Visible = false;
    }

    protected void btnRecusado_Click(object sender, EventArgs e)
    {
        pnlRecusa.Open();
    }

    protected void btnMaisInfo_Click(object sender, EventArgs e)
    {
        Redirect($"~/Solicitacao/Solicitacao.MaisInformacoesTEC.aspx?idSolicitacao={Request.QueryString["idSolicitacao"]}");
    }

    protected void btnConfirmaRecusa_Click(object sender, EventArgs e)
    {
        try
        {
            db.Solicitacao.RecusaSolicitacao(Convert.ToInt32(Request.QueryString["idSolicitacao"]), txtObsRecusa.Text);
            ShowInfoBox("Recusa realizada com sucesso");
            pnlRecusa.Close();
            btnAprovado.Visible = false;
            btnRecusado.Visible = false;
            btnMaisInfo.Visible = false;
        }
        catch
        {
            ShowErrorBox("Recusa não realizada");
        }
    }
</script>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <box:NLabel runat="server" ID="lblIdCert" Visible="false"></box:NLabel>
    <box:NLabel runat="server" ID="lblIdNf" Visible="false"></box:NLabel>
    <box:NLabel runat="server" ID="lblIdRel" Visible="false"></box:NLabel>

    <div class="form-solicitation">
        <div class="sub-form-solicitation-col">
            <span class="title">Aprovação</span>
            <span class="sub-title">Aprovação técnica</span>
        </div>

        <div>
            <div class="sub-form-arquivo">
                <span>nota fiscal/fatura/boleto</span>
                <box:NLinkButton for="content_nfFile" runat="server" ID="nfFile" OnClick="nfFile_Click"><i class="far fa-file"></i></box:NLinkButton>
            </div>
            <box:NTextBox runat="server" ID="txtDtVencimento" placeholder="Em caso de boleto indicar a data de vencimento"/>
        </div>

        <div>
            <div class="sub-form-arquivo">
                <span class="h1-solicitation">certidão negativa receita federal/fgts</span>
                <box:NLinkButton for="content_certFile" runat="server" ID="certFile" OnClick="certFile_Click"><i class="far fa-file"></i></box:NLinkButton>
            </div>
        </div>

        <div>
            <div class="sub-form-arquivo">
                <span class="h1-solicitation">relatório de execução técnico</span>
                <box:NLinkButton runat="server" ID="relFile" OnClick="relFile_Click"><i class="far fa-file"></i></box:NLinkButton>
            </div>
        </div>

        <div class="sub-form-solicitation">
            <p>VALOR BRUTO</p>
            <box:NTextBox runat="server" ID="txtValorBruto" placeholder="Preencha o campo, com o valor solicitado." MaskFormat="Decimal" />
        </div>

        <div class="form-buttons">
            <box:NButton runat="server" ID="btnAprovado" Text="APROVADO" OnClick="btnAprovado_Click" Theme="Primary" CssClass="btn-long btn-login" Visible="false"/>
            <box:NButton runat="server" ID="btnRecusado" Text="RECUSADO" OnClick="btnRecusado_Click" Theme="Info" CssClass="btn-long btn-login" Visible="false"/>
            <box:NButton runat="server" ID="btnMaisInfo" Text="MAIS INFORMAÇÕES" OnClick="btnMaisInfo_Click" Theme="Info" CssClass="btn-long btn-login" Visible="false"/>
        </div>
    </div>     

    <div class="popup-secundary">
        <box:NPopup runat="server" ID="pnlRecusa" Title="Motivo da recusa" Width="500">
            <div class="popup-wrapper">
                <box:NTextBox runat="server" ID="txtObsRecusa" placeholder="Descreva o motivo da recusa" TextMode="MultiLine" Width="400" Height="250" />
                <box:NButton runat="server" ID="btnConfirmaRecusa" Text="CONFIRMAR" OnClick="btnConfirmaRecusa_Click" Theme="Primary" CssClass="btn-long btn-login"/>
            </div>
        </box:NPopup>
    </div>

</asp:Content>