﻿<%@ Page Title="Solicitacao" Resource="nerp.comemp" Language="C#" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();

    protected void Page_Load(object sender, EventArgs e)
    {
        var validsClaims = new List<string> {"ALL", "ALL.USUARIO"};
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        if (Request.QueryString["idSolicitacao"] == "")
        {
            ShowErrorBox("Nenhuma solicitação com o id informado");
            btnAprovado.Visible = false;
            return;
        }

        var idSolicitação = Convert.ToInt32(Request.QueryString["idSolicitacao"]);
        var solicitacao = db.Solicitacao.ObtemPorId(idSolicitação);

        if (solicitacao == null)
        {
            ShowErrorBox("Nenhuma solicitação com o id informado, por favor reveja o id informado como parametro");
        }

        var dtToString = solicitacao.DataVencimentoBoleto.ToShortDateString();
        txtDtVencimento.Text = dtToString == "01/01/1900" ? "" : dtToString;
        lblIdCert.Text = solicitacao.IdCertidao.ToString();
        lblIdRel.Text = solicitacao.IdRelatorio.ToString();
        lblIdNf.Text = solicitacao.IdNF.ToString();

        if (solicitacao.TecId != null && solicitacao.GestorId != null)
        {
            if (Auth.Current<CreaspUser>().IdPessoa == solicitacao.TecId || Auth.Current<CreaspUser>().IdPessoa == solicitacao.GestorId)
            {
                btnAprovado.Visible = true;
            }
            else
            {
                btnAprovado.Visible = false;
                return;
            }
        }

        if (solicitacao.Status == StatusSolicitacao.AgAprovacaoADM || solicitacao.Status == StatusSolicitacao.AgMaisInfosADM && !solicitacao.AprovadoADM)
        {
            //VALIDAR USUÁRIO
            btnAprovado.Visible = true;
        }
    }

    protected void relFile_Click(object sender, EventArgs e)
    {
        var idRel = Convert.ToInt32(lblIdRel.Text);
        db.Documentos.Download(this, idRel);
    }

    protected void certFile_Click(object sender, EventArgs e)
    {
        var idCert = Convert.ToInt32(lblIdCert.Text);
        db.Documentos.Download(this, idCert);
    }

    protected void nfFile_Click(object sender, EventArgs e)
    {
        var idNf = Convert.ToInt32(lblIdNf.Text);
        db.Documentos.Download(this, idNf);
    }

    protected void btnAprovado_Click1(object sender, EventArgs e)
    {
        db.Solicitacao.PedidoMaisInformacaoADM(Convert.ToInt32(Request.QueryString["idSolicitacao"]), txtValorBrutoSugerido.Text, txtObsSugerida.Text);
        ShowInfoBox("Solicitação de mais informações encaminhada com sucesso");
    }
</script>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <box:NLabel runat="server" ID="lblIdCert" Visible="false"></box:NLabel>
    <box:NLabel runat="server" ID="lblIdNf" Visible="false"></box:NLabel>
    <box:NLabel runat="server" ID="lblIdRel" Visible="false"></box:NLabel>

    <div class="form-solicitation">
        <div class="sub-form-solicitation-col">
            <span class="title">Aprovação</span>
            <span class="sub-title">Mais Informações</span>
        </div>

        <div>
            <div class="sub-form-arquivo">
                <span>nota fiscal/fatura/boleto</span>
                <box:NLinkButton for="content_nfFile" runat="server" ID="nfFile" OnClick="nfFile_Click"><i class="far fa-file"></i></box:NLinkButton>
            </div>
            <box:NTextBox runat="server" ID="txtDtVencimento" placeholder="Em caso de boleto indicar a data de vencimento" Enabled="false"/>
        </div>

        <div>
            <div class="sub-form-arquivo">
                <span class="h1-solicitation">certidão negativa receita federal/fgts</span>
                <box:NLinkButton for="content_certFile" runat="server" ID="certFile" OnClick="certFile_Click"><i class="far fa-file"></i></box:NLinkButton>
            </div>
        </div>

        <div>
            <div class="sub-form-arquivo">
                <span class="h1-solicitation">relatório de execução técnico</span>
                <box:NLinkButton runat="server" ID="relFile" OnClick="relFile_Click"><i class="far fa-file"></i></box:NLinkButton>
            </div>
        </div>

        <div class="sub-form-solicitation">
            <p>VALOR BRUTO</p>
            <box:NTextBox runat="server" ID="txtValorBrutoSugerido" placeholder="Preencha o campo, com o valor solicitado." />
        </div>

        <div class="sub-form-solicitation">
            <p>Observações</p>
            <box:NTextBox runat="server" ID="txtObsSugerida" placeholder="Preencha o campo, caso especificando o pedido de novas informações" TextMode="Number"/>
        </div>

        <div class="form-buttons">
            <box:NButton runat="server" ID="btnAprovado" Text="ENVIAR" OnClick="btnAprovado_Click1" Theme="Primary" CssClass="btn-long btn-login" />
        </div>
    </div>     

</asp:Content>