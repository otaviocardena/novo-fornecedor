﻿<%@ Page Title="Solicitacao" Resource="nerp.comemp" Language="C#" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();

    protected void Page_Load(object sender, EventArgs e)
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO", "ALL.FORNECEDOR" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }
    }

    protected void btnEnviar_Click(object sender, EventArgs e)
    {
        if (relFile.PostedFile?.FileName == "")
        {
            ShowErrorBox("Relatório obrigatório");
            return;
        }

        if (nfFile.PostedFile?.FileName == "")
        {
            ShowErrorBox("NF/Fatura/Boleto obrigatório");
            return;
        }

        if (certFile.PostedFile?.FileName == "")
        {
            ShowErrorBox("Certidão obrigatório");
            return;
        }

        var vlrBruto = txtValorBruto.Text;
        var obs = txtObs.Text;
        var check = chkAssinatura.Checked;
        var nfF = nfFile.PostedFile;
        var relF = relFile.PostedFile;
        var certF = certFile.PostedFile;
        var idPessoa = Auth.Current<CreaspUser>().IdPessoa;
        var numNf = txtNumNf.Text;


        var idSolicitacao = db.Solicitacao.IncluiSolicitacao(obs, vlrBruto, check, nfF, certF, relF, idPessoa, numNf);
        db.Solicitacao.EnviaEmailADM(idSolicitacao);
        db.Solicitacao.EnviaEmailGESTOR(idSolicitacao);


        txtObs.Text = "";
        txtValorBruto.Text = "";
        txtDtVencimento.Text = "";
        ShowInfoBox("Sua solicitação foi enviada para aprovação");
    }

</script>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <div class="form-solicitation">
        <div class="sub-form-solicitation-col">
            <span class="title">SOLICITAÇÃO</span>
            <span class="sub-title">Nova solicitação</span>
        </div>

        <div>
            <div class="sub-form-arquivo">
                <span>nota fiscal/fatura/boleto</span>
                <input type="file" name="nfFile" runat="server" id="nfFile" class="inputfile" />
                <label for="content_nfFile"><i class="file fas fa-paperclip"></i></label>
            </div>
            <box:NTextBox runat="server" ID="txtDtVencimento" placeholder="Em caso de boleto indicar a data de vencimento"/>
            <box:NTextBox runat="server" ID="txtNumNf" placeholder="Preencha o campo, com o número da NF" Required="true" Height="45px"/>
        </div>

        <div>
            <div class="sub-form-arquivo">
                <span class="h1-solicitation">certidão negativa receita federal/fgts</span>
                <input type="file" name="certFile" runat="server" id="certFile" class="inputfile" />
                <label for="content_certFile"><i class="file fas fa-paperclip"></i></label>
            </div>
        </div>

        <div>
            <div class="sub-form-arquivo">
                <span class="h1-solicitation">relatório de execução técnico</span>
                <input type="file" name="relFile" runat="server" id="relFile" class="inputfile" />
                <label for="content_relFile"><i class="file fas fa-paperclip"></i></label>
            </div>
        </div>

        <div class="sub-form-solicitation">
            <p>VALOR BRUTO</p>
            <box:NTextBox runat="server" ID="txtValorBruto" MaskType="Decimal" placeholder="Preencha o campo, com o valor solicitado." Required="true"/>
        </div>

        <div class="sub-form-solicitation">
            <p>Observações</p>
            <box:NTextBox runat="server" ID="txtObs" placeholder="Preencha o campo, caso não possua justificativa para os anexo." />
        </div>

        <div class="sub-form-solicitation">
            <box:NCheckBox runat="server" ID="chkAssinatura" Text="Informamos que o envio contará com a assinatura digital do usuário conforme lei 8539 de 2015, Art 6º P 1º." />
        </div>

        <box:NButton runat="server" ID="btnEnviar" Text="Enviar" OnClick="btnEnviar_Click" Theme="Primary" CssClass="btn-long btn-login" />
    </div>     

</asp:Content>