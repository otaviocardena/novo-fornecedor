﻿<%@ Page Title="Novo Fornecedor" Resource="nerp" Language="C#" %>
<%@ import Namespace="Creasp.Core.Model" %>
<%@ import Namespace="System.Data" %>

<script runat="server">

    protected CreaspContext db = new CreaspContext();

    protected override void OnRegister()
    {
        gridList.RegisterPagedDataSource<FornecedorUsuariosTemp>((p) =>
            db.Fornecedor.ListaUsuariosFornecedorTemp(p, Request.Cookies["idSecao"]?.Value)
        );
    }

    protected override void OnPrepareForm()
    {
        var newCookie = new HttpCookie("idSecao");
        newCookie.Value = Guid.NewGuid().ToString();
        newCookie.Expires = DateTime.Now.AddMinutes(15);
        Response.Cookies.Add(newCookie);

        //SetDefaultButton(btnAddUsuario);
    }

    protected void btnEnviar_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtCnpj.Text) || string.IsNullOrEmpty(txtFantasia.Text) ||
            string.IsNullOrEmpty(txtSocial.Text) || string.IsNullOrEmpty(txtEmail.Text) || string.IsNullOrEmpty(txtLogradouro.Text))
        {
            ShowErrorBox("Preencha os campos Cnpj, Email, Razão Social, Logradouro, Nome Fantasia para cadastrar um fornecedor");
            return;
        }

        var fornecedor = new Fornecedor
        {
            CEP = txtCep.Text,
            Cnpj = txtCnpj.Text,
            NomeFantasia = txtFantasia.Text,
            RazaoSocial = txtSocial.Text,
            Email = txtEmail.Text,
            Complemento = txtCompl.Text,
            Logradouro = txtLogradouro.Text,
            Numero = txtNumero.Text
        };

        var pessoas = db.Fornecedor.BuscaUsuarioTempSecao(Request.Cookies["idSecao"].Value);

        var ok = db.Fornecedor.NovoFornecedor(fornecedor, pessoas);

        if (!ok.Item1 && (ok?.Item2.Any() ?? false))
        {
            ShowErrorBox(ok.Item2.FirstOrDefault());
        }
        else if (ok.Item1)
        {
            pnlOk.Open(btnLoginRedirect);
        }
        else
        {
            ShowErrorBox("Ocorreu algum erro ao cadastrar o fonecedor");
        }
    }

    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        txtCnpj.Text = "";
        txtEmail.Text = "";
        txtSocial.Text = "";
        txtNome.Text = "";
        txtFantasia.Text = "";
        txtLogradouro.Text = "";
        txtCep.Text = "";
        txtCompl.Text = "";
        txtNumero.Text = "";
    }

    protected void tabGeral_TabChanged(object sender, EventArgs e)
    {

    }

    protected void txtCep_TextChanged(object sender, EventArgs e)
    {
        if (txtCep.Text.Length > 0)
        {
            var cep = db.Ceps.BuscaCep(txtCep.Text.UnMask());

            if (cep == null)
            {
                txtLogradouro.Text = "";
            }
            else
            {
                txtLogradouro.Text = cep.NoLogradouro;
            }
        }
        else {
            txtLogradouro.Text = "";
        }
    }

    protected void btnAddUsuario_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtSenha.Text) || string.IsNullOrEmpty(txtCSenha.Text) ||
            string.IsNullOrEmpty(txtNome.Text) || string.IsNullOrEmpty(txtEmailUs.Text))
        {
            ShowErrorBox("Todos os campos são obrigatórios para registro de usuário");
            return;
        }

        if (txtSenha.Text != txtCSenha.Text)
        {
            ShowErrorBox("Senhas não conferem");
            return;
        }

        if (db.Fornecedor.ExisteUsuarioTempPorEmail(txtEmailUs.Text.Trim(), Request.Cookies["idSecao"]?.Value))
        {
            ShowErrorBox("Já existe um usuário para esse email");
            return;
        }

        var usuario = new FornecedorUsuariosTemp
        {
            Email = txtEmailUs.Text,
            IdSecao = Request.Cookies["idSecao"]?.Value ?? Guid.NewGuid().ToString(),
            Nome = txtNome.Text,
            Senha = txtSenha.Text
        };

        db.Fornecedor.InseririUsuarioTemporario(usuario);
        txtNome.Text = "";
        txtEmailUs.Text = "";
        gridList.DataBind();
    }

    protected void btnCancelarUsuario_Click(object sender, EventArgs e)
    {
        txtNome.Text = "";
        txtEmailUs.Text = "";
        txtSenha.Text = "";
        txtCSenha.Text = "";

        if(btnEditUsuario.Visible)
        {
            btnEditUsuario.Visible = false;
            btnAddUsuario.Visible = true;
        }

        txtCSenha.Required = true;
        txtSenha.Required = true;
    }

    protected void iconEditarUs_Click(object sender, CommandEventArgs e)
    {
        if (string.IsNullOrEmpty(txtSenha.Text) || string.IsNullOrEmpty(txtCSenha.Text) ||
            string.IsNullOrEmpty(txtNome.Text) || string.IsNullOrEmpty(txtEmailUs.Text))
        {
            ShowErrorBox("Todos os campos são obrigatórios para edição de usuário");
            return;
        }

        var email = e.CommandArgument.ToString();
        var idSecao = Request.Cookies["idSecao"].Value;
        var usuarioTemp = db.Fornecedor.BuscaUsuarioTempPorEmail(email, idSecao);

        if (usuarioTemp == null)
        {
            ShowErrorBox("Erro ao editar o usuário, recarregue a página e tente novamente");
            return;
        }

        txtEmailUs.Text = usuarioTemp.Email;
        lblOldEmail.Text = usuarioTemp.Email;
        txtNome.Text = usuarioTemp.Nome;
        txtSenha.Text = usuarioTemp.Senha;
        txtCSenha.Text = usuarioTemp.Senha;
        txtCSenha.Required = false;
        txtSenha.Required = false;
        btnAddUsuario.Visible = false;
        btnEditUsuario.Visible = true;
    }

    protected void iconExcluirUd_Click(object sender, CommandEventArgs e)
    {
        var email = e.CommandArgument.ToString();
        db.Fornecedor.ExcluirUsarioTemp(email, Request.Cookies["idSecao"].Value);

        gridList.DataBind();
    }

    protected void btnEditUsuario_Click(object sender, EventArgs e)
    {
        if (txtSenha.Text != txtCSenha.Text)
        {
            ShowErrorBox("Senhas não conferem");
            return;
        }

        var oldEmail = lblOldEmail.Text;
        var usuario = new FornecedorUsuariosTemp
        {
            Email = txtEmailUs.Text,
            IdSecao = Request.Cookies["idSecao"]?.Value ?? Guid.NewGuid().ToString(),
            Nome = txtNome.Text,
            Senha = txtSenha.Text
        };

        db.Fornecedor.EditarUsarioTemp(oldEmail, usuario);
        gridList.DataBind();
        txtCSenha.Required = true;
        txtSenha.Required = true;
        btnAddUsuario.Visible = true;
        btnEditUsuario.Visible = false;
        txtNome.Text = "";
        txtEmailUs.Text = "";
        txtSenha.Text = "";
        txtCSenha.Text = "";
    }

    protected void btnLoginRedirect_Click(object sender, EventArgs e)
    {
        pnlOk.Close();
        Redirect("~/Login.aspx?action=logout", false, true);
    }
</script>
<asp:Content ContentPlaceHolderID="content" runat="server">
    <script>
        window.onload = function () {
            var header = document.getElementsByClassName("acao-header");
            header.item(0).setAttribute('style', 'display:none');

            var menu = document.getElementsByClassName("page-hmenu");
            menu.item(0).setAttribute('style', 'display:none');
        }
    </script>

    <div>
        <box:NLabel Visible="false" ID="lblOldEmail" runat="server"></box:NLabel>
        <box:NTab runat="server" ID="tabMain" Text="Geral" Selected="true" AutoPostBack="true" OnTabChanged="tabGeral_TabChanged">
            <div class="mt-30 c-primary-blue">
                <span>Dados da Empresa</span>
            </div>
            <div class="form-div b-bottom-gray">
                <div class="sub-form-row">
                    <div class="div-sub-form">
                        <div><span>CNPJ</span></div>
                        <box:NTextBox runat="server" ID="txtCnpj" MaskType="Custom" MaskFormat="99.999.999/9999-99"/>
                    </div>

                    <div class="div-sub-form">
                        <div><span>Razão Social</span></div>
                        <box:NTextBox runat="server" ID="txtSocial" MaskType="String" />
                    </div>

                    <div class="div-sub-form">
                        <div><span>Nome Fantasia</span></div>
                        <box:NTextBox runat="server" ID="txtFantasia" MaskType="String" />
                    </div>
                </div>

                <div class="sub-form-row">
                    <div class="div-sub-form">
                        <div><span>CEP</span></div>
                        <box:NTextBox runat="server" ID="txtCep" MaskType="Custom" AutoPostBack="true" MaskFormat="999.99-999" OnTextChanged="txtCep_TextChanged"/>
                    </div>

                    <div class="div-sub-form">
                        <div><span>Logradouro</span></div>
                        <box:NTextBox runat="server" ID="txtLogradouro" MaskType="String" />
                    </div>

                    <div class="div-sub-form">
                        <div><span>Número</span></div>
                        <box:NTextBox runat="server" ID="txtNumero" MaskType="Integer" />
                    </div>
                </div>

                <div class="sub-form-row">
                    <div class="div-sub-form">
                        <div><span>Complemeto</span></div>
                        <box:NTextBox runat="server" ID="txtCompl" MaskType="String" />
                    </div>

                    <div class="div-sub-form">
                        <div><span>Email</span></div>
                        <box:NTextBox runat="server" ID="txtEmail" MaskType="String" />
                    </div>

                    <div class="div-sub-form">
                    </div>
                </div>

                <div class="sub-form-row d-flex-end mt-70">
                    <div>
                        <box:NButton runat="server" ID="btnCancelar" Text="Cancelar" Theme="primary" OnClick="btnCancelar_Click" Width="150" Height="40" CssClass="btn-form-search btn-grey" />
                    </div>
                    <div>
                        <box:NButton runat="server" ID="btnEnviarCadastro" Text="Enviar Cadastro" Theme="primary" OnClick="btnEnviar_Click" Width="150" Height="40" CssClass="btn-form-search" />
                    </div>
                </div>
            </div>
        </box:NTab>
        <box:NTab runat="server" ID="tabGeral" Text="Usuários" AutoPostBack="true" OnTabChanged="tabGeral_TabChanged">
            <div class="mt-30 c-primary-blue">
                <span>Usuários da Empresa</span>
            </div>

            <div class="form-div b-bottom-gray">
                <div class="sub-form-row">
                    <div class="div-sub-form">
                        <div><span>Nome</span></div>
                        <box:NTextBox runat="server" ID="txtNome"/>
                    </div>

                    <div class="div-sub-form">
                        <div><span>Email/Login</span></div>
                        <box:NTextBox runat="server" ID="txtEmailUs" MaskType="String"/>
                    </div>

                    <div class="div-sub-form">
                    </div>
                </div>

                <div class="sub-form-row">
                    <div class="div-sub-form">
                        <div><span>Senha</span></div>
                        <box:NTextBox runat="server" ID="txtSenha" TextMode="Password" />
                    </div>

                    <div class="div-sub-form">
                        <div><span>Confirmação Senha</span></div>
                        <box:NTextBox runat="server" ID="txtCSenha" TextMode="Password" />
                    </div>

                    <div style="display:flex;flex-direction:row;margin-top:15px;align-items:center">
                        <div>
                            <box:NButton runat="server" ID="btnCancelarUsuario" Text="Cancelar" Theme="primary" OnClick="btnCancelarUsuario_Click" Width="160" Height="40" CssClass="btn-form-search btn-grey" />
                        </div>
                        <div>
                            <box:NButton runat="server" ID="btnEditUsuario" Text="Editar" Theme="primary" Visible="false" OnClick="btnEditUsuario_Click" Width="160" Height="40" CssClass="btn-form-search" />
                            <box:NButton runat="server" ID="btnAddUsuario" Text="Adicionar +" Theme="primary" OnClick="btnAddUsuario_Click" Width="160" Height="40" CssClass="btn-form-search" />
                        </div>
                    </div>
                </div>
            </div>

            <!--table-->
            <div>
                <box:NGridView runat="server" ID="gridList" DataKeyNames="Email" ItemType="Creasp.Core.Model.FornecedorUsuariosTemp" CssClass="grid-secundary">
                    <Columns>
                        <asp:BoundField DataField="DataCadastro" HeaderText="Data de Cadastro" DataFormatString="{0:d}"/>
                        <asp:TemplateField HeaderText="Usuário">
                            <ItemTemplate>
                                <div style="display: flex;align-items: center;">
                                    <div class="name-cyan"><%#Eval("Nome").ToString().Substring(0,1).ToUpper() %></div>&nbsp;&nbsp;<asp:Label Text=<%#Eval("Nome") %> runat="server"></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ações">
                            <ItemTemplate>
                                <div>
                                    <span>
                                        <asp:LinkButton ID="iconExcluirUd" runat="server" OnCommand="iconExcluirUd_Click" CommandArgument=<%#Eval("Email")%>><i class="fas fa-trash" style="color: #858585" runat="server"></i></asp:LinkButton>
                                        <asp:LinkButton ID="iconEditarUs" runat="server" OnCommand="iconEditarUs_Click" CommandArgument=<%#Eval("Email")%>><i class="fas fa-pen" style="color: #054589" runat="server"></i></asp:LinkButton>
                                    </span>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>Vazio</EmptyDataTemplate>
                </box:NGridView>
            </div>
        </box:NTab>

        <div class="popup-secundary">
            <box:NPopup runat="server" ID="pnlOk" Title="Cadastro Enviado" Width="500">
                <div class="popup-wrapper">
                    <div class="mb-50"><a class="n-popup-close" href="javascript:;" onclick="__doPostBack('ctl00$content$pnlOk','close')"></a></div>
                    <i class="far fa-check-circle"></i>
                    <p>Cadastro enviado com sucesso</p>
                    <div class="link-button-primary"><box:NLinkButton runat="server" ID="btnLoginRedirect" Text="Login" OnClick="btnLoginRedirect_Click" /></div>
                </div>
            </box:NPopup>
        </div>
    </div>
</asp:Content>
