﻿<%@ Page Title="Perfil Fornecedor" Resource="nerp" Language="C#" %>
<%@ import Namespace="Creasp.Core.Model" %>
<%@ import Namespace="System.Data" %>

<script runat="server">

    protected CreaspContext db = new CreaspContext();

    protected override void OnRegister()
    {

        var idFornecedor = db.Pessoas.ObtemIdFornecedor(Auth.Current<CreaspUser>().IdPessoa);

        gridList.RegisterDataSource(() => db.Fornecedor.ObtemPessoaPorFornecedorNew(idFornecedor));

        var fornecedor = db.Fornecedor.ObtemFornecedor(idFornecedor);

        txtCnpj.Text = fornecedor?.Cnpj ?? "";
        txtEmail.Text = fornecedor?.Email ?? "";
        txtSocial.Text = fornecedor?.RazaoSocial ?? "";
        txtFantasia.Text = fornecedor?.NomeFantasia ?? "";
        txtLogradouro.Text = fornecedor?.Logradouro ?? "";
        txtCep.Text = fornecedor?.CEP ?? "";
        txtCompl.Text = fornecedor?.Complemento ?? "";
        txtNumero.Text = fornecedor?.Numero ?? "";

        gridList.DataBind();
    }

    protected override void OnPrepareForm()
    {
        var newCookie = new HttpCookie("idSecao");
        newCookie.Value = Guid.NewGuid().ToString();
        newCookie.Expires = DateTime.Now.AddMinutes(15);
        Response.Cookies.Add(newCookie);

        //SetDefaultButton(btnAddUsuario);
    }

    protected void btnEnviar_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtCnpj.Text) || string.IsNullOrEmpty(txtFantasia.Text) ||
            string.IsNullOrEmpty(txtSocial.Text) || string.IsNullOrEmpty(txtEmail.Text) || string.IsNullOrEmpty(txtLogradouro.Text))
        {
            ShowErrorBox("Preencha os campos Cnpj, Email, Razão Social, Logradouro, Nome Fantasia para cadastrar um fornecedor");
            return;
        }

        var fornecedor = new Fornecedor
        {
            CEP = txtCep.Text,
            Cnpj = txtCnpj.Text,
            NomeFantasia = txtFantasia.Text,
            RazaoSocial = txtSocial.Text,
            Email = txtEmail.Text,
            Complemento = txtCompl.Text,
            Logradouro = txtLogradouro.Text,
            Numero = txtNumero.Text
        };

        var pessoas = db.Fornecedor.BuscaUsuarioTempSecao(Request.Cookies["idSecao"].Value);

        var ok = db.Fornecedor.NovoFornecedor(fornecedor, pessoas);

        if (!ok.Item1 && (ok?.Item2.Any() ?? false))
        {
            ShowErrorBox(ok.Item2.FirstOrDefault());
        }
        else if (ok.Item1)
        {
            pnlOk.Open(btnLoginRedirect);
        }
        else
        {
            ShowErrorBox("Ocorreu algum erro ao cadastrar o fonecedor");
        }
    }

    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        txtCnpj.Text = "";
        txtEmail.Text = "";
        txtSocial.Text = "";
        txtNome.Text = "";
        txtFantasia.Text = "";
        txtLogradouro.Text = "";
        txtCep.Text = "";
        txtCompl.Text = "";
        txtNumero.Text = "";
    }

    protected void tabGeral_TabChanged(object sender, EventArgs e)
    {

    }

    protected void txtCep_TextChanged(object sender, EventArgs e)
    {
        if (popupCep.Text.Length > 0)
        {
            var cep = db.Ceps.BuscaCep(popupCep.Text.UnMask());

            if (cep == null)
            {
                popupLogradouro.Text = "";
            }
            else
            {
                popupLogradouro.Text = cep.NoLogradouro;
            }
        }
        else {
            popupLogradouro.Text = "";
        }
    }

    protected void btnAddUsuario_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtSenha.Text) || string.IsNullOrEmpty(txtCSenha.Text) ||
            string.IsNullOrEmpty(txtNome.Text) || string.IsNullOrEmpty(txtEmailUs.Text))
        {
            ShowErrorBox("Todos os campos são obrigatórios para registro de usuário");
            return;
        }

        if (txtSenha.Text != txtCSenha.Text)
        {
            ShowErrorBox("Senhas não conferem");
            return;
        }

        //new
        if (db.Fornecedor.ExisteUsuarioPorEmail(txtEmailUs.Text.Trim()))
        {
            ShowErrorBox("Já existe um usuário para esse email");
            return;
        }

        var idFornecedor = db.Pessoas.ObtemIdFornecedor(Auth.Current<CreaspUser>().IdPessoa);
        var fornecedor = db.Fornecedor.ObtemFornecedor(idFornecedor);

        var ok = db.Fornecedor.InserirUsuarioFornecedor(fornecedor, txtEmailUs.Text, txtNome.Text, txtSenha.Text);

        txtNome.Text = "";
        txtEmailUs.Text = "";
        gridList.DataBind();

        if (ok != null) {
            ShowErrorBox(ok);
        }
    }

    protected void btnCancelarUsuario_Click(object sender, EventArgs e)
    {
        txtNome.Text = "";
        txtEmailUs.Text = "";
        txtSenha.Text = "";
        txtCSenha.Text = "";

        if(btnEditUsuario.Visible)
        {
            btnEditUsuario.Visible = false;
            btnAddUsuario.Visible = true;
        }

        txtCSenha.Required = true;
        txtSenha.Required = true;
    }

    protected void iconEditarUs_Click(object sender, CommandEventArgs e)
    {
        /*if (string.IsNullOrEmpty(txtSenha.Text) || string.IsNullOrEmpty(txtCSenha.Text) ||
            string.IsNullOrEmpty(txtNome.Text) || string.IsNullOrEmpty(txtEmailUs.Text))
        {
            ShowErrorBox("Todos os campos são obrigatórios para edição de usuário");
            return;
        }*/

        var id = Convert.ToInt32(e.CommandArgument);
        //var idSecao = Request.Cookies["idSecao"].Value;
        //var usuarioTemp = db.Fornecedor.BuscaUsuarioTempPorEmail(email, idSecao);

        var pessoa = db.Pessoas.BuscaPessoa(id, false);
        //var loginFornecedor = db.Fornecedor.BuscaLoginFornecedor(pessoa.NoLogin);

        if (pessoa == null/* || loginFornecedor == null*/)
        {
            ShowErrorBox("Erro ao editar o usuário, recarregue a página e tente novamente");
            return;
        }

        txtEmailUs.Text = pessoa.NoEmail;
        lblOldEmail.Text = pessoa.NoEmail;
        txtNome.Text = pessoa.NoPessoa;
        txtSenha.Text = "";
        txtCSenha.Text = "";
        txtCSenha.Required = false;
        txtSenha.Required = false;
        btnAddUsuario.Visible = false;
        btnEditUsuario.Visible = true;
    }

    protected void iconExcluirUd_Click(object sender, CommandEventArgs e)
    {
        var idFornecedor = db.Pessoas.ObtemIdFornecedor(Auth.Current<CreaspUser>().IdPessoa);
        var pessoasDoFornecedor = db.Fornecedor.ObtemPessoaPorFornecedorNew(idFornecedor).Count;

        if (db.Fornecedor.ObtemPessoaPorFornecedorNew(idFornecedor).Count < 2)
        {
            ShowErrorBox("Não é possível excluir todos os usuários, ao menos é necessário para acessar o sistema.");
            return;
        }

        var idPessoa = Int32.Parse(e.CommandArgument.ToString());
        db.Fornecedor.ExcluirUsarioDoFornecedor(idPessoa, idFornecedor);

        gridList.DataBind();
    }

    protected void btnEditUsuario_Click(object sender, EventArgs e)
    {
        if (txtSenha.Text != txtCSenha.Text)
        {
            ShowErrorBox("Senhas não conferem");
            return;
        }

        var oldEmail = lblOldEmail.Text;

        db.Fornecedor.EditarUsarioFornecedor(oldEmail, txtEmailUs.Text, txtNome.Text, txtSenha.Text);
        gridList.DataBind();

        txtCSenha.Required = true;
        txtSenha.Required = true;
        btnAddUsuario.Visible = true;
        btnEditUsuario.Visible = false;
        txtNome.Text = "";
        txtEmailUs.Text = "";
        txtSenha.Text = "";
        txtCSenha.Text = "";
    }

    protected void btnLoginRedirect_Click(object sender, EventArgs e)
    {
        pnlOk.Close();
        Redirect("~/Login.aspx?action=logout", false, true);
    }

    protected void btnEditarFornecedor_Click(object sender, EventArgs e)
    {
        var idFornecedor = db.Pessoas.ObtemIdFornecedor(Auth.Current<CreaspUser>().IdPessoa);
        var fornecedor = db.Fornecedor.ObtemFornecedor(idFornecedor);

        popupCnpj.Text = fornecedor?.Cnpj ?? "";
        popupEmail.Text = fornecedor?.Email ?? "";
        popupRazao.Text = fornecedor?.RazaoSocial ?? "";
        popupNome.Text = fornecedor?.NomeFantasia ?? "";
        popupLogradouro.Text = fornecedor?.Logradouro ?? "";
        popupCep.Text = fornecedor?.CEP ?? "";
        popupComplemento.Text = fornecedor?.Complemento ?? "";
        popupNumero.Text = fornecedor?.Numero ?? "";

        nPopupEdit.Open();
    }

    protected void btnSalvarAlteracoesFornecedor_Click(object sender, EventArgs e)
    {

        if (string.IsNullOrEmpty(popupCnpj.Text) || string.IsNullOrEmpty(popupNome.Text) ||
            string.IsNullOrEmpty(popupRazao.Text) || string.IsNullOrEmpty(popupEmail.Text) || string.IsNullOrEmpty(popupLogradouro.Text))
        {
            ShowErrorBox("Preencha os campos Cnpj, Email, Razão Social, Logradouro, Nome Fantasia para atualizar um fornecedor");
            return;
        }

        var idFornecedor = db.Pessoas.ObtemIdFornecedor(Auth.Current<CreaspUser>().IdPessoa);

        Fornecedor fornecedor = new Fornecedor();

        fornecedor.Cnpj = popupCnpj.Text;
        fornecedor.Email = popupEmail.Text;
        fornecedor.RazaoSocial = popupRazao.Text;
        fornecedor.NomeFantasia = popupNome.Text;
        fornecedor.Logradouro = popupLogradouro.Text;
        fornecedor.CEP = popupCep.Text;
        fornecedor.Complemento = popupComplemento.Text;
        fornecedor.Numero = popupNumero.Text;

        db.Fornecedor.AtualizaDadosFornecedor(idFornecedor, fornecedor);
        OnRegister();

        nPopupEdit.Close();
    }


</script>
<asp:Content ContentPlaceHolderID="content" runat="server">
    <div>
        <box:NLabel Visible="false" ID="lblOldEmail" runat="server"></box:NLabel>
        <box:NTab runat="server" ID="tabMain" Text="Meu perfil" Selected="true" AutoPostBack="true" OnTabChanged="tabGeral_TabChanged">
            <div class="mt-30 c-primary-blue">
                <span>Dados da Empresa</span><box:NLinkButton runat="server" ID="btnEditFornecedor" CssClass="right icon-right" OnClick="btnEditarFornecedor_Click"><i class="fas fa-edit"></i><span>Editar</span></box:NLinkButton>
            </div>
            <div class="form-div b-bottom-gray">
                <div class="sub-form-row">
                    <div class="div-sub-form">
                        <div><span>CNPJ</span></div>
                        <box:NLabel runat="server" ID="txtCnpj" MaskType="Custom" MaskFormat="99.999.999/9999-99" CssClass="n-cus field w-96"/>
                        
                    </div>

                    <div class="div-sub-form">
                        <div><span>Razão Social</span></div>
                        <box:NLabel runat="server" ID="txtSocial" MaskType="String" CssClass="field w-96"/>
                    </div>

                    <div class="div-sub-form">
                        <div><span>Nome Fantasia</span></div>
                        <box:NLabel runat="server" ID="txtFantasia" MaskType="String" CssClass="field w-100"/>
                    </div>
                </div>

                <div class="sub-form-row">
                    <div class="div-sub-form">
                        <div><span>CEP</span></div>
                        <box:NLabel runat="server" ID="txtCep" MaskType="Custom" AutoPostBack="true" MaskFormat="999.99-999" OnTextChanged="txtCep_TextChanged" CssClass="field w-96"/>
                    </div>

                    <div class="div-sub-form">
                        <div><span>Logradouro</span></div>
                        <box:NLabel runat="server" ID="txtLogradouro" MaskType="String" CssClass="field w-96"/>
                    </div>

                    <div class="div-sub-form">
                        <div><span>Número</span></div>
                        <box:NLabel runat="server" ID="txtNumero" MaskType="Integer" CssClass="field w-100"/>
                    </div>
                </div>

                <div class="sub-form-row">
                    <div class="div-sub-form">
                        <div><span>Complemeto</span></div>
                        <box:NLabel runat="server" ID="txtCompl" MaskType="String" CssClass="field w-96"/>
                    </div>

                    <div class="div-sub-form">
                        <div><span>Email</span></div>
                        <box:NLabel runat="server" ID="txtEmail" MaskType="String" CssClass="field w-96"/>
                    </div>

                    <div class="div-sub-form">
                        
                    </div>
                </div>
            </div>
        </box:NTab>

        <box:NPopup runat="server" ID="nPopupEdit" Title="Editar Fornecedor" Width="750">

            <div class="form-div b-bottom-gray">
                <div class="sub-form-row">
                    <div class="div-sub-form">
                        <div><span>CNPJ</span></div>
                        <box:NTextBox runat="server" ID="popupCnpj" MaskType="Custom" MaskFormat="99.999.999/9999-99" CssClass="n-cus field w-96"/>
                        
                    </div>

                    <div class="div-sub-form">
                        <div><span>Razão Social</span></div>
                        <box:NTextBox runat="server" ID="popupRazao" MaskType="String" CssClass="field w-96"/>
                    </div>

                    <div class="div-sub-form">
                        <div><span>Nome Fantasia</span></div>
                        <box:NTextBox runat="server" ID="popupNome" MaskType="String" CssClass="field w-100"/>
                    </div>
                </div>

                <div class="sub-form-row">
                    <div class="div-sub-form">
                        <div><span>CEP</span></div>
                        <box:NTextBox runat="server" ID="popupCep" MaskType="Custom" AutoPostBack="true" MaskFormat="999.99-999" OnTextChanged="txtCep_TextChanged" CssClass="field w-96"/>
                    </div>

                    <div class="div-sub-form">
                        <div><span>Logradouro</span></div>
                        <box:NTextBox runat="server" ID="popupLogradouro" MaskType="String" CssClass="field w-96"/>
                    </div>

                    <div class="div-sub-form">
                        <div><span>Número</span></div>
                        <box:NTextBox runat="server" ID="popupNumero" MaskType="Integer" CssClass="field w-100"/>
                    </div>
                </div>

                <div class="sub-form-row">
                    <div class="div-sub-form">
                        <div><span>Complemeto</span></div>
                        <box:NTextBox runat="server" ID="popupComplemento" MaskType="String" CssClass="field w-96"/>
                    </div>

                    <div class="div-sub-form">
                        <div><span>Email</span></div>
                        <box:NTextBox runat="server" ID="popupEmail" MaskType="String" CssClass="field w-96"/>
                    </div>

                    <div class="div-sub-form">
                        <box:NButton runat="server" Theme="Primary" ID="btnPopupEdit" MaskType="String" CssClass="field w-96 mt-25" Text="Salvar alterações" OnClick="btnSalvarAlteracoesFornecedor_Click"/>
                    </div>
                </div>
            </div>
        </box:NPopup>

        <box:NTab runat="server" ID="tabGeral" Text="Gerenciar usuários" AutoPostBack="true" OnTabChanged="tabGeral_TabChanged">
            <div class="mt-30 c-primary-blue">
                <span>Usuários da Empresa</span>
            </div>

            <div class="form-div b-bottom-gray">
                <div class="sub-form-row">
                    <div class="div-sub-form">
                        <div><span>Nome</span></div>
                        <box:NTextBox runat="server" ID="txtNome"/>
                    </div>

                    <div class="div-sub-form">
                        <div><span>Email/Login</span></div>
                        <box:NTextBox runat="server" ID="txtEmailUs" MaskType="String"/>
                    </div>

                    <div class="div-sub-form">
                    </div>
                </div>

                <div class="sub-form-row">
                    <div class="div-sub-form">
                        <div><span>Senha</span></div>
                        <box:NTextBox runat="server" ID="txtSenha" TextMode="Password" />
                    </div>

                    <div class="div-sub-form">
                        <div><span>Confirmação Senha</span></div>
                        <box:NTextBox runat="server" ID="txtCSenha" TextMode="Password" />
                    </div>

                    <div style="display:flex;flex-direction:row;margin-top:15px;align-items:center">
                        <div>
                            <box:NButton runat="server" ID="btnCancelarUsuario" Text="Cancelar" Theme="primary" OnClick="btnCancelarUsuario_Click" Width="160" Height="40" CssClass="btn-form-search btn-grey" />
                        </div>
                        <div>
                            <box:NButton runat="server" ID="btnEditUsuario" Text="Editar" Theme="primary" Visible="false" OnClick="btnEditUsuario_Click" Width="160" Height="40" CssClass="btn-form-search" />
                            <box:NButton runat="server" ID="btnAddUsuario" Text="Adicionar +" Theme="primary" OnClick="btnAddUsuario_Click" Width="160" Height="40" CssClass="btn-form-search" />
                        </div>
                    </div>
                </div>
            </div>

            <!--table-->
            <div>
                <box:NGridView runat="server" ID="gridList" DataKeyNames="NoEmail" ItemType="Creasp.Core.Pessoa" CssClass="grid-secundary">
                    <Columns>
                        <asp:BoundField DataField="DtCadastro" HeaderText="Data de Cadastro" DataFormatString="{0:d}"/>
                        <asp:TemplateField HeaderText="Usuário">
                            <ItemTemplate>
                                <div style="display: flex;align-items: center;">
                                    <div class="name-cyan"><%#Eval("NoPessoa").ToString().Substring(0,1).ToUpper() %></div>&nbsp;&nbsp;<asp:Label Text=<%#Eval("NoPessoa") %> runat="server"></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ações">
                            <ItemTemplate>
                                <div>
                                    <span>
                                        <asp:LinkButton ID="iconExcluirUd" runat="server" OnCommand="iconExcluirUd_Click" CommandArgument=<%#Eval("IdPessoa")%>><i class="fas fa-trash" style="color: #858585" runat="server"></i></asp:LinkButton>
                                        <asp:LinkButton ID="iconEditarUs" runat="server" OnCommand="iconEditarUs_Click" CommandArgument=<%#Eval("IdPessoa")%>><i class="fas fa-pen" style="color: #054589" runat="server"></i></asp:LinkButton>
                                    </span>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <EmptyDataTemplate>Vazio</EmptyDataTemplate>
                </box:NGridView>
            </div>
        </box:NTab>

        <div class="popup-secundary">
            <box:NPopup runat="server" ID="pnlOk" Title="Cadastro Enviado" Width="500">
                <div class="popup-wrapper">
                    <div class="mb-50"><a class="n-popup-close" href="javascript:;" onclick="__doPostBack('ctl00$content$pnlOk','close')"></a></div>
                    <i class="far fa-check-circle"></i>
                    <p>Cadastro enviado com sucesso</p>
                    <div class="link-button-primary"><box:NLinkButton runat="server" ID="btnLoginRedirect" Text="Login" OnClick="btnLoginRedirect_Click" /></div>
                </div>
            </box:NPopup>
        </div>
    </div>
</asp:Content>
