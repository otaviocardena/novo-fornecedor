﻿<%@ Page Title="SICOP - Versão Mobile" Language="C#" %>
<script runat="server">

    private CreaspContext db = new CreaspContext();

    protected override void OnPrepareForm()
    {
        var sb = new StringBuilder();
        
        sb.Append("<h4>SIPLEN</h4>");
        sb.Append(Page.BuildSiteMap("~/Content/Sitemap/siplen.sitemap"));
        sb.Append("<h4>NERP</h4>");
        sb.Append(Page.BuildSiteMap("~/Content/Sitemap/nerp.sitemap"));
        sb.Append("<h4>ELO</h4>");
        sb.Append(Page.BuildSiteMap("~/Content/Sitemap/elo.sitemap"));
        sb.Append("<h4>Admin</h4>");
        sb.Append(Page.BuildSiteMap("~/Content/Sitemap/admin.sitemap"));

        ltrSitemap.Text = sb.ToString();
    }

</script>
<asp:Content runat="server" ContentPlaceHolderID="content">

    <div>
        <a href="/Login.aspx?action=logout" target="_self">Sair</a>
    </div>

    <asp:Literal runat="server" ID="ltrSitemap" EnableViewState="false" />

</asp:Content>