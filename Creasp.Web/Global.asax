﻿<%@ Application Language="C#" %>
<script runat="server">

    protected void Application_Start(object sender, EventArgs e)
    {
        RegisterFactory();
    }

    /// <summary>
    /// Registra todas as interfaces utilizando o NBox Factory IoC
    /// </summary>
    protected void RegisterFactory()
    {
        // Sistema de autenticação
        Factory.Register<IUser>(() => new CreaspUser());
        Factory.Register<IAuth>(() => new CreanetADAuth());
        //Factory.Register<IAuth>(() => new MockADAuth());

        // Interfaces do NBox
        Factory.Register<IAuditWriter>(() => new AuditWriter());
        Factory.Register<ICache>(() => new WebCache(TimeSpan.FromMinutes(ConfigurationManager.AppSettings["nbox.cache.timeout"].To<int>())));
        Factory.Register<IStorage>(() => new CreaspStorage());

        Factory.Register<ISendEmail>(() => new MockSendEmail());
        // Factory.Register<ISendEmail>(() => new SmtpSender());

        // Sistemas externos do CREA

        //Factory.Register<ICreanet>(() => new MockCreanet());
        Factory.Register<ICreanet>(() => new CreanetService());

        //Factory.Register<ISiscont>(() => new MockSiscont());
        Factory.Register<ISiscont>(() => new SiscontService());

        //Factory.Register<ISenior>(() => new MockSenior());
        Factory.Register<ISenior>(() => new SeniorService());

        // Conexão aos bancos de dados (com debug para VS :: funciona apenas quando o VS está rodando em modo debug - F5)
        Factory.Register<IDatabase>(() => new CreaspDatabase());
    }

</script>
