﻿<%@ Page Title="Reformulação Orçamentária" Resource="elo" Language="C#" %>

<script runat="server">

    private CreaspContext db = new CreaspContext();

    private int? IdReformulacaoOrcamentaria => Request.QueryString["IdReformulacaoOrcamentaria"].To<int?>();
    private int IdReformulacaoOrcamentariaItem { get { return (int)ViewState["IdReformulacaoOrcamentariaItem"]; } set { ViewState["IdReformulacaoOrcamentariaItem"] = value; } }

    private Creasp.Elo.ReformulacaoOrcamentaria ReformulacaoOrcamentaria;
    private List<Creasp.Elo.ReformulacaoOrcamentariaItem> ItensReformulacao { get { return ViewState["ItensReformulacao"] as List<Creasp.Elo.ReformulacaoOrcamentariaItem>; } set { ViewState["ItensReformulacao"] = value; } }

    private decimal? toVrAprovado = 0;
    private decimal? toVrDotacaoAtual = 0;
    private decimal? toVrExecutado = 0;
    private decimal? toVrSuplementacao = 0;
    private decimal? toVrReducao = 0;
    private decimal? toVrReformulado = 0;

    private string nrCentroCusto = string.Empty;
    private string noResponsavel = string.Empty;
    private string nrAno = string.Empty;

    #region Init
    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        if (!IdReformulacaoOrcamentaria.HasValue && Request.QueryString["service"]?.ToString() != "autocomplete")
        {
            Redirect("ReformulacaoOrcamentaria.aspx");
        }

        RegisterResource("elo.reformulacaoorcamentaria");

        grdList.RegisterDataSource(() => ItensReformulacao);
    }

    protected override void OnPrepareForm()
    {
        if (ReformulacaoOrcamentaria == null)
        {
            ReformulacaoOrcamentaria = db.Reformulacoes.BuscaReformulacaoOrcamentaria(IdReformulacaoOrcamentaria.Value);
            lblCentroCusto.Text = ReformulacaoOrcamentaria.CentroCustoOriginal.NoCentroCustoFmt;
            lblResponsavel.Text = ReformulacaoOrcamentaria.Responsavel?.NoPessoa;
            lblAno.Text = ReformulacaoOrcamentaria.Reformulacao.NrAno.ToString();

            if (!string.IsNullOrEmpty(ReformulacaoOrcamentaria.CdTpFormulario))
            {
                lblFormulario.Text = ReformulacaoOrcamentaria.CdTpFormulario;
            }
            else
            {
                lblFormulario.Text = "Não";
            }
        }

        if (ItensReformulacao == null)
        {
            CarregaItens();
        }

        AtualizaTotalizadores();
        grdList.DataBind();

        this.Audit(ReformulacaoOrcamentaria, IdReformulacaoOrcamentaria);
        this.BackTo("ReformulacaoOrcamentaria.aspx?IdReformulacao=" + ReformulacaoOrcamentaria?.IdReformulacao);
    }
    #endregion

    #region Events
    protected void grdList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("EditJustificativa"))
        {
            IdReformulacaoOrcamentariaItem = Convert.ToInt32(grdList.DataKeys[e.CommandArgument.To<int>()].Value);

            txtNoJustificativa.Text = ItensReformulacao
                .Where(x => x.IdReformulacaoOrcamentariaItem == IdReformulacaoOrcamentariaItem)
                .Single()?
                .NoJustificativa;

            pnlPopupJustificativa.Open(btnOkJustificativa);
        }
        else if (e.CommandName.Equals("del"))
        {
            IdReformulacaoOrcamentariaItem = Convert.ToInt32(grdList.DataKeys[e.CommandArgument.To<int>()].Value);

            //db.Reformulacoes.ExcluiFormularioItem(IdReformulacaoOrcamentariaItem);

            CarregaItens();
            grdList.DataBind();
        }
    }

    //private int RetornaIdReformulacaOrcamentaria(TextBox txt)
    //{
    //    GridViewRow row = txt.NamingContainer as GridViewRow;
    //    return Convert.ToInt32(grdList.DataKeys[row.RowIndex].Value);

    //}

    //protected void txtVrSuplementacao_TextChanged(object sender, EventArgs e)
    //{
    //    decimal novoValor = 0;
    //    TextBox txt = sender as TextBox;

    //    Decimal.TryParse(txt.Text, out novoValor);

    //    GridViewRow row = txt.NamingContainer as GridViewRow;
    //    int idReformulacaoOrcamentaria = RetornaIdReformulacaOrcamentaria(sender as TextBox);

    //    if (ItensReformulacao != null)
    //    {
    //        ItensReformulacao.Single(x => x.IdReformulacaoOrcamentariaItem == idReformulacaoOrcamentaria).VrSuplementacao = novoValor;
    //        AtualizaTotalizadores();
    //    }

    //    grdList.DataBind();
    //}

    //protected void txtVrReducao_TextChanged(object sender, EventArgs e)
    //{
    //    decimal novoValor = 0;
    //    TextBox txt = sender as TextBox;

    //    Decimal.TryParse(txt.Text, out novoValor);

    //    GridViewRow row = txt.NamingContainer as GridViewRow;
    //    int idReformulacaoOrcamentaria = RetornaIdReformulacaOrcamentaria(sender as TextBox);

    //    if (ItensReformulacao != null)
    //    {
    //        ItensReformulacao.Single(x => x.IdReformulacaoOrcamentariaItem == idReformulacaoOrcamentaria).VrReducao = novoValor;
    //        AtualizaTotalizadores();
    //    }

    //    grdList.DataBind();
    //}

    protected void btnOkJustificativa_Click(object sender, EventArgs e)
    {
        ItensReformulacao.Single(x => x.IdReformulacaoOrcamentariaItem == IdReformulacaoOrcamentariaItem).NoJustificativa = txtNoJustificativa.Text.Trim();
        AtualizaTotalizadores();
        grdList.DataBind();
        pnlPopupJustificativa.Close();
    }

    protected void btnVoltarJustificativa_Click(object sender, EventArgs e)
    {
        pnlPopupJustificativa.Close();
    }

    //protected void btnVoltar_Click(object sender, EventArgs e)
    //{
    //    Redirect("ReformulacaoOrcamentaria.aspx");
    //}

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        db.Reformulacoes.SalvaRefomulcaoItens(ItensReformulacao);
        AtualizaTotalizadores();
        grdList.DataBind();
        ShowInfoBox("Alterações salvas com sucesso.");
    }
    #endregion

    #region Privados
    private void CarregaItens()
    {
        ItensReformulacao = db.Reformulacoes.BuscaReformulacaoOrcamentariaItens(IdReformulacaoOrcamentaria.Value);
    }

    private void AtualizaTotalizadores()
    {
        if (ItensReformulacao != null)
        {
            //toVrOrcadoAnoAnterior = ItensReformulacao.Sum(x => x.VrOrcadoAnoAnterior);
            toVrAprovado = ItensReformulacao.Sum(x => x.VrOrcadoInicialmente);
            toVrExecutado = ItensReformulacao.Sum(x => x.VrExecutado);
            toVrDotacaoAtual = ItensReformulacao.Sum(x => x.VrDotacaoAtual);
            toVrSuplementacao = ItensReformulacao.Sum(x => x.VrSuplementacao.HasValue ? x.VrSuplementacao.Value : 0);
            toVrReducao = ItensReformulacao.Sum(x => x.VrReducao.HasValue ? x.VrReducao.Value : 0);
            toVrReformulado = ItensReformulacao.Sum(x => x.VrOrcamentoReformulado.HasValue ? x.VrOrcamentoReformulado.Value : 0);
        }
    }
    #endregion

    protected void btnIncluirFormularioItem_Click(object sender, EventArgs e)
    {
        pnlIncluirFormularioItem.Open();
    }

    protected void btnSalvarItemFormulario_Click(object sender, EventArgs e)
    {
        var idContaContabil = txtContaContabil.Value.To<int>();
        //db.Reformulacoes.IncluiFormularioItem(IdReformulacaoOrcamentaria.Value, idContaContabil);

        txtContaContabil.Value = "";
        txtContaContabil.Text = "";

        CarregaItens();
        pnlIncluirFormularioItem.Close();
        grdList.DataBind();
    }


</script>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <table class="form">
        <tr>
            <td>Centro de custo</td>
            <td>
                <box:NLabel runat="server" ID="lblCentroCusto" /></td>
        </tr>
        <tr>
            <td>Formulário</td>
            <td>
                <box:NLabel runat="server" ID="lblFormulario"/></td>
        </tr>
        <tr>
            <td>Responsável</td>
            <td>
                <box:NLabel runat="server" ID="lblResponsavel" /></td>
        </tr>
        <tr>
            <td>Ano</td>
            <td>
                <box:NLabel runat="server" ID="lblAno" /></td>
        </tr>
    </table>

    <box:NGridView runat="server" ID="grdList" ShowFooter="true" DataKeyNames="IdReformulacaoOrcamentariaItem" OnRowCommand="grdList_RowCommand" ItemType="Creasp.Elo.ReformulacaoOrcamentariaItem">
        <Columns>
            <asp:TemplateField HeaderText="Número" HeaderStyle-Width="145">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblOrcadoAnoAnterior"><%# Item?.ContaContabil?.NrContaContabilFmt %></asp:Label>
                </ItemTemplate>
                <FooterTemplate>
                    Total
                </FooterTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ContaContabil.NoContaContabil" HeaderText="Nome" SortExpression="ContaContabil.NoContaContabil" />
            <%--<asp:BoundField DataField="ContaContabil.CdResumido" HeaderText="Cod." HeaderStyle-Width="45" SortExpression="ContaContabil.CdResumido" />--%>
            <asp:BoundField DataField="NrProcessoL" HeaderText="Processo" SortExpression="NrProcessoL" />
            <asp:BoundField DataField="NoJustificativa" HeaderText="Justificativa" SortExpression="NoJustificativa" ItemStyle-CssClass="nowrap-ellipsis" />
            <box:NButtonField Icon="search" Theme="Info" ToolTip="Editar justificativa" CommandName="EditJustificativa" EnabledExpression="IdReformulacaoOrcamentariaItem" />

            <%--<asp:TemplateField HeaderText="Orçado ano anterior">
                <HeaderTemplate>
                    <%# string.Format("Orçado {0}", DateTime.Now.Year ) %>
                </HeaderTemplate>
                <ItemTemplate>
                    <box:NLabel runat="server" Text='<%# Item.VrOrcadoAnoAnterior.ToString("n2") %>' />
                </ItemTemplate>
                <FooterTemplate>
                    <%# toVrOrcadoAnoAnterior.ToString("n2") %>
                </FooterTemplate>
            </asp:TemplateField>--%>
            <asp:TemplateField HeaderText="Aprovado" ItemStyle-HorizontalAlign="Right">
                <HeaderTemplate>
                    <%# string.Format("Aprovado {0}", db.NrAnoBase) %> 
                </HeaderTemplate>
                <ItemTemplate>
                    <box:NLabel runat="server" Text='<%# Item.VrOrcadoInicialmente.HasValue ? Item.VrOrcadoInicialmente.Value.ToString("n2") : "0,00" %>' />
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label runat="server" ID="lblAprovado"><%# toVrAprovado.HasValue ? toVrAprovado.Value.ToString("n2") : "0,00" %></asp:Label>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Executado ano anterior" ItemStyle-HorizontalAlign="Right">
                <HeaderTemplate>
                    <%# string.Format("Executado até {0}", ItensReformulacao.Where(x => x.DtRefVrExecutado.HasValue).FirstOrDefault() != null ? ItensReformulacao.Where(x => x.DtRefVrExecutado.HasValue).FirstOrDefault().DtRefVrExecutado.Value.ToString("dd/MM/yyyy") : "01/01/01") %>
                </HeaderTemplate>
                <ItemTemplate>
                    <box:NLabel runat="server" Text='<%# Item.VrExecutado.HasValue ? Item.VrExecutado.Value.ToString("n2") : "0,00" %>' />
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label runat="server" ID="lblExecutadoAnoAnterior"><%# toVrExecutado.HasValue ? toVrExecutado.Value.ToString("n2") : "0,00" %></asp:Label>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Dotação atual" ItemStyle-HorizontalAlign="Right">
                <HeaderTemplate>
                    <%# string.Format("Dotação Atual {0}", db.NrAnoBase) %> 
                </HeaderTemplate>
                <ItemTemplate>
                    <box:NLabel runat="server" Text='<%# Item.VrDotacaoAtual.HasValue ? Item.VrDotacaoAtual.Value.ToString("n2") : "0,00" %>' />
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label CssClass="t-right"><%# toVrDotacaoAtual.HasValue ? toVrDotacaoAtual.Value.ToString("n2") : "0,00" %></asp:Label>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Suplementação" ItemStyle-HorizontalAlign="Right">
                <HeaderTemplate>
                    <%# string.Format("Supl. {0}", db.NrAnoBase) %> 
                </HeaderTemplate>
                <ItemTemplate>
                    <box:NTextBox runat="server" ID="txtVrSuplementacao" MaskType="Decimal" Text='<%# Item.VrSuplementacao.HasValue ? Item.VrSuplementacao.Value.ToString("n2") : "0,00" %>' Width="80" MaxLength="20" CssClass="t-right" Enabled="false" />
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label CssClass="t-right"><%# toVrSuplementacao.HasValue ? toVrSuplementacao.Value.ToString("n2") : "0,00" %></asp:Label>
                </FooterTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Redução">
                <HeaderTemplate>
                    <%# string.Format("Redução {0}", db.NrAnoBase) %>
                </HeaderTemplate>
                <ItemTemplate>
                    <box:NTextBox runat="server" ID="txtVrReducao" MaskType="Decimal" Text='<%# Item.VrReducao.HasValue ? Item.VrReducao.Value.ToString("n2") : "0,00" %>' Width="80" MaxLength="20" Enabled="false" CssClass="t-right" />
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label CssClass="t-right"><%# toVrReducao.HasValue ? toVrReducao.Value.ToString("n2") : "0,00" %></asp:Label>
                </FooterTemplate>
            </asp:TemplateField>

            <asp:TemplateField HeaderText="Reformulado" ItemStyle-HorizontalAlign="Right">
                <HeaderTemplate>
                    <%# string.Format("Reformulado {0}", db.NrAnoBase) %> 
                </HeaderTemplate>
                <ItemTemplate>
                    <box:NLabel runat="server" Text='<%# Item.VrOrcamentoReformulado.HasValue ? Item.VrOrcamentoReformulado.Value.ToString("n2") : "0,00" %>' />
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label CssClass="t-right"><%# toVrReformulado.HasValue ? toVrReformulado.Value.ToString("n2") : "0,00" %></asp:Label>
                </FooterTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>Esta reformulacao orçamentária está vazia.</EmptyDataTemplate>
    </box:NGridView>

    <box:NPopup runat="server" ID="pnlPopupJustificativa" Title="Editar justificativa" Width="650">
        <box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnOkJustificativa" OnClick="btnOkJustificativa_Click" Theme="Default" Text="OK" Icon="ok" />
            <box:NButton runat="server" ID="btnVoltarJustificativa" OnClick="btnVoltarJustificativa_Click" Theme="Default" Text="Cancelar" Icon="cancel" />
        </box:NToolBar>
        <table class="form">
            <tr>
                <td>Justificativa</td>
                <td>
                    <box:NTextBox runat="server" ID="txtNoJustificativa" TextMode="MultiLine" ShowTextBox="false" Enabled="false" Width="100%" />
                </td>
            </tr>
        </table>
    </box:NPopup>

    <box:NPopup runat="server" ID="pnlIncluirFormularioItem" Title="Incluir contas contábeis" Width="650">
        <box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnSalvarItemFormulario" OnClick="btnSalvarItemFormulario_Click" Theme="Primary" Text="Salvar" Icon="ok" />
        </box:NToolBar>
        <table class="form">
            <tr>
                <td>Conta</td>
                <td>
                    <box:NAutocomplete runat="server" ID="txtContaContabil" TextCapitalize="Upper" Width="100%" DataValueField="IdContaContabil" DataTextField="NoContaContabilFmt" AutoPostBack="true" />
                </td>
            </tr>
        </table>
    </box:NPopup>

</asp:Content>
