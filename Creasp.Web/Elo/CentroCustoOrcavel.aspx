﻿<%@ Page Title="Centros de custos" Resource="elo.configuracao" Language="C#" %>

<script runat="server">
    private CreaspContext db = new CreaspContext();

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        grdList.RegisterPagedDataSource((p) => db.CentrosCustosResponsaveis.ListaCentrosCustosResponsaveis(p,
            chkApenasOrcaveis.Checked,
            drpTipoCentroCusto.SelectedValue,
            txtPesquisaNrCentroCusto.Text.To<string>(),
            txtPesquisaNoCentroCusto.Text.To<string>()
            ));
    }

    protected override void OnPrepareForm()
    {
        grdList.DataBind();
    }

    protected void btnPesquisar_Click(object sender, EventArgs e)
    {
        grdList.DataBind();
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        var centrosOrcaveisSelecionados = grdList.SelectedDataKeys.Select(x => x.Value.To<int>()).ToArray();

        int[] centrosOrcaveisTotal = new int[grdList.DataKeys.Count];

        for (int i = 0; i < grdList.DataKeys.Count; i++)
        {
            centrosOrcaveisTotal[i] = Convert.ToInt32(grdList.DataKeys[i].Value);
        }

        if (db.CentrosCustosResponsaveis.ValidaESalvaCentrosOrcaveis(centrosOrcaveisTotal, centrosOrcaveisSelecionados))
        {
            ShowInfoBox("Todas informações salvas com sucesso");
        }
        else
        {
            ShowInfoBox("Suas alterações foram salvas, mas algumas foram descartadas por regras de validação.");
        }
        grdList.DataBind();
    }

    //Função que impede que seja desmarcado um centro de custo com perfil responsável vinculado
    protected void grdList_SelectedIndexChanged(object sender, EventArgs e)
    {
        var todosCentrosCustoResponsaveis = db.CentrosCustosResponsaveis.ListaCentrosCustosResponsaveis().Where(x => x.CentroCusto.NrAno == db.NrAnoBase).ToList();
        var NrCentroCustoComPerfilContabilidade = todosCentrosCustoResponsaveis.Where(x => !string.IsNullOrEmpty(x.NoPerfis)).Select(y => y.CentroCusto.NrCentroCustoFmt).ToList();
        foreach (GridViewRow row in grdList.Rows)
        {
            if(grdList.SelectedIndexes.ToList().IndexOf(row.RowIndex) == -1)
            {
                foreach(string nrCentroCusto in NrCentroCustoComPerfilContabilidade)
                    if (grdList.Rows[row.RowIndex].Cells[0].Text.Equals(nrCentroCusto))
                    {
                        var check = grdList.Rows[row.RowIndex].Cells[3].Controls[0] as CheckBox;
                        check.Checked = true;
                        ShowErrorBox("Este centro de custo está vinculado a um perfil responsável");
                    }
            }
        }
    }
</script>
<asp:Content runat="server" ContentPlaceHolderID="content">

    <box:NToolBar runat="server">
        <box:NButton runat="server" ID="btnPesquisar" Theme="Default" Icon="search" Text="Pesquisar" OnClick="btnPesquisar_Click" />
        <box:NButton runat="server" ID="btnSalvar" Theme="Primary" Icon="ok" Text="Salvar" OnClick="btnSalvar_Click" CssClass="right" />
    </box:NToolBar>

    <table class="form">
        <tr>
            <td>Número</td>
            <td>
                <box:NTextBox runat="server" ID="txtPesquisaNrCentroCusto" TextCapitalize="Upper" MaxLength="80" Width="100%" />
            </td>
        </tr>
        <tr>
            <td>Nome</td>
            <td>
                <box:NTextBox runat="server" ID="txtPesquisaNoCentroCusto" TextCapitalize="Upper" MaxLength="80" Width="100%" />
            </td>
        </tr>
        <tr>
            <td>Tipo</td>
            <td>
                <box:NDropDownList runat="server" ID="drpTipoCentroCusto" Width="20%">
                    <asp:ListItem Value="" Text="Selecione..." Selected="True"></asp:ListItem>
                    <asp:ListItem Value="A" Text="Analítico"></asp:ListItem>
                    <asp:ListItem Value="S" Text="Sintético"></asp:ListItem>
                    <asp:ListItem Value="X" Text="Sub-Área"></asp:ListItem>
                </box:NDropDownList>
            </td>
        </tr>
        <tr>
            <td>Apenas orçáveis</td>
            <td>
                <box:NCheckBox runat="server" ID="chkApenasOrcaveis" />
            </td>
        </tr>
    </table>

    <box:NGridView runat="server" ID="grdList" DataKeyNames="IdCentroCustoResponsavel" OnSelectedIndexChanged="grdList_SelectedIndexChanged" ItemType="Creasp.Elo.CentroCustoResponsavel">
        <Columns>
            <asp:BoundField DataField="CentroCusto.NrCentroCustoFmt" HeaderText="Número" SortExpression="CentroCusto.NrCentroCusto" />
            <asp:BoundField DataField="CentroCusto.NoCentroCusto" HeaderText="Nome" SortExpression="CentroCusto.NoCentroCusto" />
            <asp:BoundField DataField="CentroCusto.TpCentroCustoFmt" HeaderText="Tipo" SortExpression="CentroCusto.TpCentroCusto" />
            <box:NCheckBoxField AutoPostBack="true" SelectedExpression="FgOrcavel" />
        </Columns>
        <EmptyDataTemplate>Não existem centros de custo para o filtro informado</EmptyDataTemplate>
    </box:NGridView>
</asp:Content>
