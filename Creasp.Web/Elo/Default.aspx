﻿<%@ Page Title="Elaboração Orçamentária" Language="C#" %>

<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }
    }

</script>
<asp:Content runat="server" ContentPlaceHolderID="content">
    <div class="app-container" style="width: 100%">
        <div class="app-card" style="width: 30%; float: left; margin: 1%;">
            <div class="img" style="padding: 50px;"><i class="icon-chart-bar"></i></div>
            <div class="app-title" style="font-size:18px;">O que é?</div>
            <div class="app-desc" style="padding: 10px; font-size:14px;">O módulo ELO refere-se à Elaboração Orçamentária completa do Crea-SP. É aqui que orçamos a despesa de todas unidades com o intuito de que esteja de acordo com a receita.</div>
        </div>
        <div class="app-card" style="width: 30%; float: left; margin: 1%;">
            <div class="img" style="padding: 50px;"><i class="icon-retweet"></i></div>
            <div class="app-title" style="font-size:18px;">Integração</div>
            <div class="app-desc" style="padding: 10px; font-size:14px;">Os dados são importados do sistema SISCONT no ano anterior para que haja uma base de dados no início da realização da elaboração do orçamento. Ao terminá-la, será possível enviar a despesa e receita orçadas ao SISCONT para que elas sejam acatadas.</div>
        </div>
        <div class="app-card" style="width: 30%; float: left; margin: 1%;">
            <div class="img" style="padding: 50px;"><i class="icon-ok"></i></div>
            <div class="app-title" style="font-size:18px;">Resultado</div>
            <div class="app-desc" style="padding: 10px; font-size:14px;">Dados coerentes, íntegros e de acordo com a realidade do Crea-SP. Alterações monitoradas por auditoria e exportação de relatórios oficiais.</div>
        </div>
    </div>
</asp:Content>
