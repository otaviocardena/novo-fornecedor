﻿<%@ Page Title="Reformulações orçamentárias" Resource="elo" Language="C#" %>

<script runat="server">

    private CreaspContext db = new CreaspContext();

    public int? IdReformulacao => Request.QueryString["IdReformulacao"].To<int?>();
    public int? IdReformulacaoOrcamentaria { get { return ViewState["IdReformulacaoOrcamentaria"].To<int?>(); } set { ViewState["IdReformulacaoOrcamentaria"] = value; } }
    //private string Tipo => Request.QueryString["Tipo"].To<string>();


    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        DefineElementosVisiveis();

        grdList.RegisterPagedDataSource((p) => db.Reformulacoes.ListaReformulacaoOrcamentaria(p, IdReformulacao.Value,
                                        txtPesquisaNrCentroCusto.Text.To<string>(),
                                        txtPesquisaNoCentroCusto.Text.To<string>(),
                                        txtPesquisaNoResponsavel.Text.To<string>(),
                                        drpStatusProposta.SelectedValue.To<string>(),
                                        drpCdTpFormulario.SelectedValue.To<string>()));

        drpStatusProposta.Bind(db.StatusPropostasOrcamentarias.ListaStatusPropostas());

        //RegisterResource("elo.configuracao").Disabled(btnIncluir);
    }

    protected override void OnPrepareForm()
    {
        DefineElementosVisiveis();
        grdList.DataBind();
    }

    protected void btnPesquisar_Click(object sender, EventArgs e)
    {
        grdList.DataBind();
    }

    protected void btnPreIncluirProposta_Click(object sender, EventArgs e)
    {
        pnlIncluirProposta.Open();
        cmbCentroCustoParaProposta.Bind(db.CentrosCustosResponsaveis.ListaCentrosCustosPassiveisDeViraremPropostasDeReformulacao(db.NrAnoBase, IdReformulacao.Value).Select(x => new { x.NrCentroCusto, x.NoCentroCustoFmt }), false, true);
    }

    protected void btnPreIncluirFormulario_Click(object sender, EventArgs e)
    {
        //cmbCentroCusto.Bind(db.CentrosCustosResponsaveis.ListaCentroCustoOrcaveisUsuarioLogado().Select(x => new { x.CentroCusto.NoCentroCustoFmt }), false, false);
        pnlIncluirFormulario.Open();
        cmbCentroCusto.BindMultiColumn(db.CentrosCustosResponsaveis.ListaCentrosCustosOrcaveisDoUsuarioLogado().Select(x => new { x.CentroCusto.NrCentroCusto, x.CentroCusto.NrCentroCustoFmt, x.CentroCusto.NoCentroCusto }), false, true);
    }

    protected void btnIncluirFormulario_Click(object sender, EventArgs e)
    {
        var nrCentroCusto = cmbCentroCusto.SelectedValue.ToString();

        if (!string.IsNullOrEmpty(drpTipoFormulario.SelectedValue))
        {
            if (IdReformulacao.HasValue)
            {
                db.Reformulacoes.ValidaEIncluiFormulario(IdReformulacao.Value, drpTipoFormulario.SelectedValue, nrCentroCusto);
                grdList.DataBind();
                pnlIncluirFormulario.Close();
            }
            else
            {
                ShowErrorBox("Um erro aconteceu, recarregue a página.");
            }
        }
        else
        {
            ShowErrorBox("Escolha um tipo de formulário");
        }
    }

    protected void btnIncluirProposta_Click(object sender, EventArgs e)
    {
        var nrCentroCusto = cmbCentroCustoParaProposta.SelectedValue.ToString();

        db.Reformulacoes.IncluiReformulacaoOrcamentaria(IdReformulacao.Value, nrCentroCusto);
        grdList.DataBind();
        pnlIncluirProposta.Close();

        ShowInfoBox("Proposta incluída com sucesso");

    }

    protected void grdList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "view")
        {
            var idReformulacaoOrcamentaria = e.DataKey<int>(sender);
            Redirect("ReformulacaoOrcamentaria.Form.aspx?IdReformulacaoOrcamentaria=" + idReformulacaoOrcamentaria);
        }
        else if (e.CommandName == "env")
        {
            var idReformulacaoOrcamentaria = e.DataKey<int>(sender);
            db.Reformulacoes.DisponibilizaReformulacaoAoProximoResponsavel(idReformulacaoOrcamentaria);
            ShowInfoBox("Reformulacao disponibilizada para o próximo responsável.");
            DefineElementosVisiveis();
            grdList.DataBind();
        }
        else if (e.CommandName == "vol")
        {
            IdReformulacaoOrcamentaria = e.DataKey<int>(sender);
            if (!db.Reformulacoes.PodeMoverReformulacaoOrcamentaria(IdReformulacaoOrcamentaria.Value))
            {
                ShowErrorBox("Você não tem permissão para isto.");
            }
            else
            {
                txtMotivoRejeicao.Text = "";
                pnlMotivoRejeicao.Open();
            }
        }
    }

    protected void btnPlenariaOK_Click(object sender, EventArgs e)
    {
        var lstErros = db.Reformulacoes.DisponibilizaTodasReformulacoesDespesaParaProximoStatus(IdReformulacao.Value, StPropostaOrcamentaria.Plenario);
        if (lstErros?.Count > 0)
        {
            ShowInfoBox("Não foi possível integrar os dados");
            txtErros.Text = string.Join(", ", lstErros);
            pnlErros.Open();
        }
        else
        {
            ShowInfoBox("Aprovação da Plenária OK!");
        }

        DefineElementosVisiveis();
        grdList.DataBind();
    }

    protected void btnEntendi_Click(object sender, EventArgs e)
    {
        pnlErros.Close();
    }

    //protected void btnPlenariaOK_Click(object sender, EventArgs e)
    //{
    //    db.Reformulacoes.DisponibilizaTodasReformulacoesDespesaParaPlenaria(IdReformulacao.Value);
    //    ShowInfoBox("Aprovação da Plenária OK!");
    //    grdList.DataBind();
    //}

    protected void btnConfirmarRetorno_Click(object sender, EventArgs e)
    {
        string noMotivoRejeicao = txtMotivoRejeicao.Text.To<string>();

        db.Reformulacoes.DisponibilizaReformulacaoAoResponsavelAnterior(IdReformulacaoOrcamentaria.Value, noMotivoRejeicao);
        ShowInfoBox("Reformulação disponibilizada para o próximo responsável.");

        grdList.DataBind();
        DefineElementosVisiveis();

        pnlMotivoRejeicao.Close();
    }

    protected void btnCancelarRetorno_Click(object sender, EventArgs e)
    {
        pnlMotivoRejeicao.Close();
    }

    protected void grdList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var data = e.Row.DataItem as Creasp.Elo.ReformulacaoOrcamentaria;
            if (data.FgEstaReprovado) e.Row.ForeColor = System.Drawing.Color.Red;
        }
    }

    protected void btnValidacaoContabilidade_Click(object sender, EventArgs e)
    {
        if (db.Reformulacoes.VerificaSeTodasReformulacoesTemMesmoStatus(IdReformulacao.Value, StPropostaOrcamentaria.ContabValidacao))
        {
            db.Reformulacoes.DisponibilizaTodasReformulacoesDespesaParaProximoStatus(IdReformulacao.Value, StPropostaOrcamentaria.ContabValidacao);
            DefineElementosVisiveis();
            grdList.DataBind();
            ShowInfoBox("Validação da contabilidade OK!");
        }
        else
        {
            ShowErrorBox("Todas as propostas precisam ter o mesmo status.");
        }
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        db.Reformulacoes.DisponibilizaTodasReformulacoesDespesaParaStatusAnterior(IdReformulacao.Value);
        DefineElementosVisiveis();
        grdList.DataBind();
        ShowInfoBox("Aprovação da Plenária OK!");
    }

    private void DefineElementosVisiveis()
    {
        if (!IdReformulacao.HasValue)
        {
            ShowErrorBox("Nenhuma reformulação selecionada.");
            Redirect("Reformulacao.aspx");
        }

        //Verifica se o usuário logado é contabilidade.
        bool isContabilidade = db.CentrosCustosPerfis.ValidaUsuarioLogadoPossuiPerfil(TpPerfilCentroCusto.Contabilidade);

        //Lista todos os status das reformulações orçamentárias da reformulção corrente.
        var lstStatusDasReformulacoes = db.Reformulacoes.ListaStatusDasReformulacoes(IdReformulacao.Value);

        //Se todas reformulações estiverem no mesmo status, guarda esse status. Senão, deixa a string vazia.
        string unicoStatusDasReformulacoes = lstStatusDasReformulacoes.Count == 1 ? lstStatusDasReformulacoes.Single() : "";

        if (isContabilidade)
        {
            //Se for contabilidade, mostra o total da despesa no ano.
            lblTotalDespesaReformulado.Text = string.Format("Total de despesa do ano: R$ {0}", db.Reformulacoes.BuscaTotalOrcadoAno().ToString("n2"));
        }

        //Visível apenas para contabilidade quando todas reformulações estiverem no status "Ag. Aprovação Plenário".
        btnPlenariaOK.Visible = isContabilidade && unicoStatusDasReformulacoes == StPropostaOrcamentaria.Plenario;

        //Visível apenas para contabilidade quando todas reformulações estiverem no status "Ag. Validação Contabilidade".
        btnValidacaoContabilidade.Visible = isContabilidade && unicoStatusDasReformulacoes == StPropostaOrcamentaria.ContabValidacao;

        //Visível apenas para contabilidade quando todas reformulações estiverem num dos status abaixo, pois é quando todas propostas podem ser reprovadas juntas.
        btnRetornar.Visible = isContabilidade &&
            (unicoStatusDasReformulacoes == StPropostaOrcamentaria.ContabValidacao ||
            unicoStatusDasReformulacoes == StPropostaOrcamentaria.Plenario ||
            unicoStatusDasReformulacoes == StPropostaOrcamentaria.PlenarioOK);

        //Visível apenas para contabilidade quando todas reformulações NÃO estiverem num dos status abaixo, pois ela só podem incluir uma proposta antes destes status.
        btnPreIncluirProposta.Visible = isContabilidade &&
            unicoStatusDasReformulacoes != StPropostaOrcamentaria.ContabValidacao &&
            unicoStatusDasReformulacoes != StPropostaOrcamentaria.Plenario &&
            unicoStatusDasReformulacoes != StPropostaOrcamentaria.PlenarioOK;

        //Visível para todos os usuários quando todas reformulações NÃO estiverem num dos status abaixo, pois elas só podem incluir formulários antes destes status.
        btnPreIncluirFormulario.Visible = unicoStatusDasReformulacoes != StPropostaOrcamentaria.ContabValidacao &&
            unicoStatusDasReformulacoes != StPropostaOrcamentaria.Plenario &&
            unicoStatusDasReformulacoes != StPropostaOrcamentaria.PlenarioOK;
    }
</script>

<asp:Content runat="server" ContentPlaceHolderID="content">

    <box:NToolBar runat="server">
        <box:NButton runat="server" ID="btnPesquisar" Theme="Default" Icon="search" Text="Pesquisar" OnClick="btnPesquisar_Click" />
    </box:NToolBar>

    <table class="form">
        <tr>
            <td>Número</td>
            <td>
                <box:NTextBox runat="server" ID="txtPesquisaNrCentroCusto" TextCapitalize="Upper" MaxLength="80" Width="100%" />
            </td>
        </tr>
        <tr>
            <td>Nome</td>
            <td>
                <box:NTextBox runat="server" ID="txtPesquisaNoCentroCusto" TextCapitalize="Upper" MaxLength="80" Width="100%" />
            </td>
        </tr>
        <tr>
            <td>Responsável</td>
            <td>
                <box:NTextBox runat="server" ID="txtPesquisaNoResponsavel" TextCapitalize="Upper" MaxLength="80" Width="100%" />
            </td>
        </tr>
        <tr>
            <td>Status</td>
            <td>
                <box:NDropDownList runat="server" ID="drpStatusProposta" Width="40%">
                    <asp:ListItem Value="" Text="Selecione..." Selected="True"></asp:ListItem>
                    <asp:ListItem Value="A" Text="Analítico"></asp:ListItem>
                    <asp:ListItem Value="S" Text="Sintético"></asp:ListItem>
                    <asp:ListItem Value="X" Text="Sub-Área"></asp:ListItem>
                </box:NDropDownList>
            </td>
        </tr>
        <tr>
            <td>Tipo formulário</td>
            <td>
                <box:NDropDownList runat="server" ID="drpCdTpFormulario" Width="40%">
                    <asp:ListItem Value="" Text="Selecione..." Selected="True"></asp:ListItem>
                    <asp:ListItem Value="Almoxarifado" Text="Almoxarifado"></asp:ListItem>
                    <asp:ListItem Value="DAS" Text="DAS"></asp:ListItem>
                    <asp:ListItem Value="Informatica" Text="Informatica"></asp:ListItem>
                </box:NDropDownList>
            </td>
        </tr>
    </table>

    <box:NToolBar runat="server">
        <box:NButton runat="server" ID="btnValidacaoContabilidade" Theme="Success" Icon="ok" Text="Validação contabilidade" OnClick="btnValidacaoContabilidade_Click" Visible="false" OnClientClick="return confirm('Esta ação enviará todas as reformulações para o plenário. \r\n\r\nDeseja continuar?')" />
        <box:NButton runat="server" ID="btnPlenariaOK" Theme="Success" Icon="ok" Text="Aprovação plenária" OnClick="btnPlenariaOK_Click" Visible="false" OnClientClick="return confirm('Esta ação validará todas as reformulações como 'plenário OK' \r\n\r\nDeseja continuar?')" />
        <box:NButton runat="server" ID="btnRetornar" Theme="Danger" Icon="reply" Text="Retornar" OnClick="btnRetornar_Click" Visible="false" OnClientClick="return confirm('Esta ação retornará todas as reformulações. \r\n\r\nDeseja continuar?')" />
        <box:NButton runat="server" ID="btnPreIncluirProposta" Theme="Default" Icon="plus" CssClass="right" Text="Incluir proposta" OnClick="btnPreIncluirProposta_Click" Visible="false" />
        <box:NButton runat="server" ID="btnPreIncluirFormulario" Theme="Default" Icon="plus" Text="Incluir formulário" OnClick="btnPreIncluirFormulario_Click" />
        <%--<box:NButton runat="server" ID="btnDisponibilizar" Theme="Primary" Icon="right-fat" CssClass="right" Text="Disponibilizar todas reformulacaos" OnClick="btnDisponibilizarReformulacaos_Click" />--%>
    </box:NToolBar>

    <box:NToolBar runat="server">
        <box:NLabel runat="server" ID="lblTotalDespesaReformulado" CssClass="t-bold t-right" Width="100%" />
    </box:NToolBar>

    <box:NGridView runat="server" ID="grdList" DataKeyNames="IdReformulacaoOrcamentaria" OnRowDataBound="grdList_RowDataBound" OnRowCommand="grdList_RowCommand" ItemType="Creasp.Elo.ReformulacaoOrcamentaria">
        <Columns>
            <asp:BoundField DataField="CentroCustoOriginal.NrCentroCustoFmt" HeaderText="Número" SortExpression="CentroCustoOriginal.NrCentroCusto"
                HeaderStyle-Width="100" ItemStyle-Width="100" />
            <asp:BoundField DataField="CentroCustoOriginal.NoCentroCusto" HeaderText="Nome" SortExpression="CentroCustoOriginal.NoCentroCusto" />
            <asp:BoundField DataField="Responsavel.NoPessoa" HeaderText="Responsável" SortExpression="Responsavel.NoPessoa" />
            <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="StatusPropostaOrcamentaria.NoDescricao"
                HeaderStyle-Width="100" ItemStyle-Width="100" />
            <asp:BoundField DataField="CdTpFormulario" HeaderText="Tipo" SortExpression="CdTpFormulario"
                HeaderStyle-Width="100" ItemStyle-Width="80" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="VrOrcadoTotal" HeaderText="Total" DataFormatString="{0:n2}" SortExpression=""
                HeaderStyle-Width="100" ItemStyle-Width="100" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
            <asp:BoundField DataField="ReformulacaoOrcamentariaItens.Count" HeaderText="Contas" SortExpression=""
                HeaderStyle-Width="60" ItemStyle-Width="60" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
            <box:NButtonField Icon="search" CommandName="view" Theme="Info" ToolTip="Ver valores" />
            <box:NButtonField Icon="ok" CommandName="env" Theme="Info" ToolTip="Disponibilizar reformulacao" EnabledExpression="PodeEnviar" OnClientClick="return confirm('Deseja disponibilizar esta reformulação para o próximo responsável?')" />
            <box:NButtonField Icon="left-fat" CommandName="vol" Theme="Info" ToolTip="Reprovar proposta" EnabledExpression="PodeRetornar" OnClientClick="return confirm('Deseja reprovar esta reformulação para o responsável anterior?')" />
        </Columns>
        <EmptyDataTemplate>Não existem reformulacaos orçamentárias para você visualizar no momento</EmptyDataTemplate>
    </box:NGridView>

    <%--<box:NPopup runat="server" ID="pnlPopupDisponibilizarReformulacaos" Title="Disponibilizar reformulacaos orçamentárias" Width="650">
        <box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnOK" Theme="Primary" Text="Salvar" Icon="ok" />
        </box:NToolBar>
    </box:NPopup>--%>

    <box:NPopup runat="server" ID="pnlIncluirFormulario" Title="Incluir formulário" Width="650">
        <box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnIncluir" Theme="Primary" Text="Incluir" Icon="ok" OnClick="btnIncluirFormulario_Click" />
        </box:NToolBar>

        <table class="form">
            <tr>
                <td>Tipo</td>
                <td>
                    <box:NDropDownList runat="server" ID="drpTipoFormulario">
                        <asp:ListItem Value="" Text="Selecione..." Selected="True"></asp:ListItem>
                        <asp:ListItem Value="Almoxarifado" Text="Almoxarifado"></asp:ListItem>
                        <asp:ListItem Value="DAS" Text="DAS"></asp:ListItem>
                        <asp:ListItem Value="Informatica" Text="Informática"></asp:ListItem>
                    </box:NDropDownList>
                </td>
            </tr>
            <tr>
                <td>Centro de custos</td>
                <td>
                    <box:NDropDownList runat="server" ID="cmbCentroCusto" Width="450" />
                </td>
            </tr>
        </table>
        
    </box:NPopup>

    <box:NPopup runat="server" ID="pnlErros" Title="Erros encontrados na integração" Width="600">
        <box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnEntendi" Theme="Default" Text="Entendi" Icon="ok" OnClick="btnEntendi_Click" />
        </box:NToolBar>

        <table class="form">
            <tr>
                <td>Os seguintes erros foram encontrados ao tentar enviar as informações ao SISCONT</td>
            </tr>
            <tr>
                <td>
                    <box:NTextBox runat="server" ID="txtErros" TextMode="MultiLine" Width="100%" Height="200px"></box:NTextBox></td>
            </tr>
        </table>

    </box:NPopup>

    <box:NPopup runat="server" ID="pnlMotivoRejeicao" Title="Motivo retorno" Width="650">
        <box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnConfirmarRetorno" OnClick="btnConfirmarRetorno_Click" Theme="Primary" Text="Confirmar" Icon="ok" />
            <box:NButton runat="server" ID="btnCancelarRetorno" OnClick="btnCancelarRetorno_Click" Theme="Default" Text="Cancelar" Icon="cancel" />
        </box:NToolBar>
        <table class="form">
            <tr>
                <td>Motivo</td>
                <td>
                    <box:NTextBox runat="server" ID="txtMotivoRejeicao" TextMode="MultiLine" MaxLength="1500" Width="100%" />
                </td>
            </tr>
        </table>
    </box:NPopup>

    <box:NPopup runat="server" ID="pnlIncluirProposta" Title="Incluir proposta" Width="650">
         <box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnIncluirProposta" Theme="Primary" Text="Incluir" Icon="ok" OnClick="btnIncluirProposta_Click" />
        </box:NToolBar>

        <table class="form">
            <tr>
                <td>Importante</td>
                <td>A inclusão de uma proposta só poderá ser feita por centros de custos orçáveis, que possuam um executor responsável e ainda não estejam em nenhuma proposta orçamentária.
                </td>
            </tr>
            <tr>
                <td>Dica</td>
                <td>Antes de incluir um centro de custo como proposta orçamentária, certifique-se que as contas contábeis corretas estão relacionadas a ele.
                </td>
            </tr>
            <tr>
                <td>Centro de custos</td>
                <td>
                    <box:NDropDownList runat="server" ID="cmbCentroCustoParaProposta" Width="450" />
                </td>
            </tr>
        </table>
    </box:NPopup>

</asp:Content>


