﻿<%@ Page Title="Propostas orçamentárias" Resource="elo" Language="C#" %>

<script runat="server">

    private CreaspContext db = new CreaspContext();

    public int? IdPropostaOrcamentariaDespesa { get { return ViewState["IdPropostaOrcamentariaDespesa"].To<int?>(); } set { ViewState["IdPropostaOrcamentariaDespesa"] = value; } }


    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        grdList.RegisterPagedDataSource((p) => db.PropostasOrcamentarias.ListaPropostaOrcamentariaAnoAtual(p,
                                                txtPesquisaNrCentroCusto.Text.To<string>(),
                                                txtPesquisaNoCentroCusto.Text.To<string>(),
                                                txtPesquisaNoResponsavel.Text.To<string>(),
                                                drpStatusProposta.SelectedValue.To<string>(),
                                                drpCdTpFormulario.SelectedValue.To<string>()));

        drpStatusProposta.Bind(db.StatusPropostasOrcamentarias.ListaStatusPropostas());

        grdList.DataBind();
    }

    protected override void OnPrepareForm()
    {
        DefineElementosVisiveis();
        grdList.DataBind();
    }

    protected void btnPesquisar_Click(object sender, EventArgs e)
    {
        grdList.DataBind();
    }

    protected void btnPreIncluirProposta_Click(object sender, EventArgs e)
    {
        pnlIncluirProposta.Open();
        cmbCentroCustoParaProposta.Bind(db.CentrosCustosResponsaveis.ListaCentrosCustosPassiveisDeViraremPropostas().Select(x => new { x.NrCentroCusto, x.NoCentroCustoFmt }), false, true);
    }

    protected void btnPreIncluirFormulario_Click(object sender, EventArgs e)
    {
        pnlIncluirFormulario.Open();
        cmbCentroCusto.BindMultiColumn(db.CentrosCustosResponsaveis.ListaCentrosCustosOrcaveisDoUsuarioLogado().Select(x => new { x.CentroCusto.NrCentroCusto, x.CentroCusto.NoCentroCustoFmt, }), false, true);
    }

    protected void btnIncluirFormulario_Click(object sender, EventArgs e)
    {
        var nrCentroCusto = cmbCentroCusto.SelectedValue.ToString();

        if (!string.IsNullOrEmpty(drpTipoFormulario.SelectedValue))
        {
            db.PropostasOrcamentarias.ValidaEIncluiFormulario(drpTipoFormulario.SelectedValue, nrCentroCusto);
            pnlIncluirFormulario.Close();

            ShowInfoBox("Formulário incluído com sucesso");

            grdList.DataBind();
        }
        else
        {
            ShowErrorBox("Escolha um tipo de formulário");
        }
    }

    protected void btnIncluirProposta_Click(object sender, EventArgs e)
    {
        var nrCentroCusto = cmbCentroCustoParaProposta.SelectedValue.ToString();

        db.PropostasOrcamentarias.ValidaEIncluiPropostaOrcamentaria(db.NrAnoBase, nrCentroCusto);
        grdList.DataBind();
        pnlIncluirProposta.Close();

        ShowInfoBox("Proposta incluída com sucesso");

    }

    protected void grdList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "view")
        {
            var idPropostaOrcamentaria = e.DataKey<int>(sender);
            Redirect("PropostaOrcamentaria.Form.aspx?IdPropostaOrcamentariaDespesa=" + idPropostaOrcamentaria);
        }
        else if (e.CommandName == "env")
        {
            var idPropostaOrcamentaria = e.DataKey<int>(sender);
            db.PropostasOrcamentarias.DisponibilizaPropostaAoProximoResponsavel(idPropostaOrcamentaria, true);
            grdList.DataBind();
            DefineElementosVisiveis();
            ShowInfoBox("Proposta disponibilizada para o próximo responsável.");
        }
        else if (e.CommandName == "vol")
        {
            IdPropostaOrcamentariaDespesa = e.DataKey<int>(sender);
            if (!db.PropostasOrcamentarias.PodeMoverProposta(IdPropostaOrcamentariaDespesa.Value))
            {
                ShowErrorBox("Você não tem permissão para isto.");
            }
            else
            {
                txtMotivoRejeicao.Text = "";
                pnlMotivoRejeicao.Open();
            }
        }
        else if (e.CommandName == "del")
        {
            var idPropostaOrcamentaria = e.DataKey<int>(sender);
            db.PropostasOrcamentarias.ExcluiProposta(idPropostaOrcamentaria);
            DefineElementosVisiveis();
            grdList.DataBind();
            ShowInfoBox("Proposta removida com sucesso.");
        }
    }

    protected void btnEntendi_Click(object sender, EventArgs e)
    {
        pnlErros.Close();
    }

    protected void btnConfirmarRetorno_Click(object sender, EventArgs e)
    {
        string noMotivoRejeicao = txtMotivoRejeicao.Text.To<string>();

        db.PropostasOrcamentarias.DisponibilizaPropostaAoResponsavelAnterior(IdPropostaOrcamentariaDespesa.Value, true, noMotivoRejeicao);
        grdList.DataBind();
        pnlMotivoRejeicao.Close();
        DefineElementosVisiveis();
        ShowInfoBox("Proposta reprovada para o responsável anterior.");
    }

    protected void btnCancelarRetorno_Click(object sender, EventArgs e)
    {
        pnlMotivoRejeicao.Close();
    }

    protected void grdList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var data = e.Row.DataItem as PropostaOrcamentariaDespesa;
            if (data.FgEstaReprovado) e.Row.ForeColor = System.Drawing.Color.Red;
        }
    }

    protected void btnValidacaoContabilidade_Click(object sender, EventArgs e)
    {
        if (db.PropostasOrcamentarias.VerificaSeTodasPropostasTemMesmoStatus(db.NrAnoBase, StPropostaOrcamentaria.ContabValidacao))
        {
            decimal vrOrcadoDespesa = Math.Round(db.PropostasOrcamentarias.BuscaTotalOrcadoAno(), 2);
            decimal vrOrcadoReceita = Math.Round(db.MetodologiasReceita.BuscaTotalOrcadoAno(), 2);

            if (vrOrcadoDespesa == vrOrcadoReceita)
            {
                db.PropostasOrcamentarias.DisponibilizaTodasPropostasParaProximoStatus(db.NrAnoBase);
                DefineElementosVisiveis();
                grdList.DataBind();
                ShowInfoBox("Validação da contabilidade OK!");
            }
            else
            {
                ShowErrorBox($"Os valores de despesa não são iguais a receita. <br>Despesa: { vrOrcadoDespesa.ToString("n2") } <br>Receita: { vrOrcadoReceita.ToString("n2") }");
            }
        }
        else
        {
            ShowErrorBox("Todas as propostas precisam ter o mesmo status.");
        }
    }

    protected void btnPlenariaOK_Click(object sender, EventArgs e)
    {
        db.PropostasOrcamentarias.DisponibilizaTodasPropostasParaProximoStatus(db.NrAnoBase);
        DefineElementosVisiveis();
        grdList.DataBind();
        ShowInfoBox("Aprovação da Plenária OK!");
    }

    private void DefineElementosVisiveis()
    {
        //Verifica se o usuário logado é contabilidade.
        bool isContabilidade = db.CentrosCustosPerfis.ValidaUsuarioLogadoPossuiPerfil(TpPerfilCentroCusto.Contabilidade);

        //Lista todos os status das propostas orçamentárias do ano corrente.
        var lstStatusDasPropostas = db.PropostasOrcamentarias.ListaStatusDasPropostas(db.NrAnoBase);

        //Se todas propostas estiverem no mesmo status, guarda esse status. Senão, deixa a string vazia.
        string unicoStatusDasPropostas = lstStatusDasPropostas.Count == 1 ? lstStatusDasPropostas.Single() : "";

        if (isContabilidade)
        {
            //Se for contabilidade, mostra o total da despesa no ano.
            lblTotalDespesaOrcado.Text = string.Format("Total de despesa do ano: R$ {0}", db.PropostasOrcamentarias.BuscaTotalOrcadoAno().ToString("n2"));
        }

        //Visível apenas para contabilidade quando todas propostas estiverem no status "Ag. Aprovação Plenário".
        btnPlenariaOK.Visible = isContabilidade && unicoStatusDasPropostas == StPropostaOrcamentaria.Plenario;

        //Visível apenas para contabilidade quando todas propostas estiverem no status "Ag. Validação Contabilidade".
        btnValidacaoContabilidade.Visible = isContabilidade && unicoStatusDasPropostas == StPropostaOrcamentaria.ContabValidacao;

        //Visível apenas para contabilidade quando todas propostas estiverem num dos status abaixo, pois é quando todas propostas podem ser reprovadas juntas.
        btnRetornar.Visible = isContabilidade &&
            (unicoStatusDasPropostas == StPropostaOrcamentaria.ContabValidacao ||
            unicoStatusDasPropostas == StPropostaOrcamentaria.Plenario ||
            unicoStatusDasPropostas == StPropostaOrcamentaria.PlenarioOK);
        
        //Visível apenas para contabilidade quando todas propostas NÃO estiverem num dos status abaixo, pois ela só podem incluir uma proposta antes destes status.
        btnPreIncluirProposta.Visible = isContabilidade &&
            unicoStatusDasPropostas != StPropostaOrcamentaria.ContabValidacao &&
            unicoStatusDasPropostas != StPropostaOrcamentaria.Plenario &&
            unicoStatusDasPropostas != StPropostaOrcamentaria.PlenarioOK;
        
        //Visível para todos os usuários quando todas propostas NÃO estiverem num dos status abaixo, pois elas só podem incluir formulários antes destes status.
        btnPreIncluirFormulario.Visible = unicoStatusDasPropostas != StPropostaOrcamentaria.ContabValidacao &&
            unicoStatusDasPropostas != StPropostaOrcamentaria.Plenario &&
            unicoStatusDasPropostas != StPropostaOrcamentaria.PlenarioOK;
    }

    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        db.PropostasOrcamentarias.DisponibilizaTodasPropostasParaStatusAnterior(db.NrAnoBase);
        DefineElementosVisiveis();
        grdList.DataBind();
        ShowInfoBox("Todas as propostas foram retornadas com sucesso.");
    }
</script>

<asp:Content runat="server" ContentPlaceHolderID="content">

    <box:NToolBar runat="server">
        <box:NButton runat="server" ID="btnPesquisar" Theme="Default" Icon="search" Text="Pesquisar" OnClick="btnPesquisar_Click" />
        <box:NLabel runat="server" ID="lblTotalDespesaOrcado" CssClass="t-bold t-right" Width="100%" />
    </box:NToolBar>

    <table class="form">
        <tr>
            <td>Número</td>
            <td>
                <box:NTextBox runat="server" ID="txtPesquisaNrCentroCusto" TextCapitalize="Upper" MaxLength="80" Width="100%" />
            </td>
        </tr>
        <tr>
            <td>Nome</td>
            <td>
                <box:NTextBox runat="server" ID="txtPesquisaNoCentroCusto" TextCapitalize="Upper" MaxLength="80" Width="100%" />
            </td>
        </tr>
        <tr>
            <td>Responsável</td>
            <td>
                <box:NTextBox runat="server" ID="txtPesquisaNoResponsavel" TextCapitalize="Upper" MaxLength="80" Width="100%" />
            </td>
        </tr>
        <tr>
            <td>Status</td>
            <td>
                <box:NDropDownList runat="server" ID="drpStatusProposta" Width="40%">
                    <asp:ListItem Value="" Text="Selecione..." Selected="True"></asp:ListItem>
                    <asp:ListItem Value="A" Text="Analítico"></asp:ListItem>
                    <asp:ListItem Value="S" Text="Sintético"></asp:ListItem>
                    <asp:ListItem Value="X" Text="Sub-Área"></asp:ListItem>
                </box:NDropDownList>
            </td>
        </tr>
        <tr>
            <td>Tipo formulário</td>
            <td>
                <box:NDropDownList runat="server" ID="drpCdTpFormulario" Width="40%">
                    <asp:ListItem Value="" Text="Selecione..." Selected="True"></asp:ListItem>
                    <asp:ListItem Value="Almoxarifado" Text="Almoxarifado"></asp:ListItem>
                    <asp:ListItem Value="DAS" Text="DAS"></asp:ListItem>
                    <asp:ListItem Value="Informatica" Text="Informatica"></asp:ListItem>
                </box:NDropDownList>
            </td>
        </tr>
    </table>
    <box:NToolBar runat="server">
        <box:NButton runat="server" ID="btnValidacaoContabilidade" Theme="Success" Icon="ok" CssClass="" Text="Validação contabilidade" OnClick="btnValidacaoContabilidade_Click" Visible="false" OnClientClick="return confirm('Esta ação validará os dados de despesa com a receita orçada. \r\n\r\nDeseja continuar?')" />
        <box:NButton runat="server" ID="btnPlenariaOK" Theme="Success" Icon="ok" CssClass="" Text="Aprovação plenária" OnClick="btnPlenariaOK_Click" Visible="false" OnClientClick="return confirm('Esta ação aprovará e fechará todas as propostas orçamentárias e isto não poderá ser desfeito. \r\n\r\nDeseja continuar?')" />
        <box:NButton runat="server" ID="btnRetornar" Theme="Danger" Icon="reply" CssClass="" Text="Retornar" OnClick="btnRetornar_Click" Visible="false" OnClientClick="return confirm('Esta ação retornará todas as propostas. \r\n\r\nDeseja continuar?')" />
        <box:NButton runat="server" ID="btnPreIncluirProposta" Theme="Default" Icon="plus" CssClass="right" Text="Incluir proposta" OnClick="btnPreIncluirProposta_Click" Visible="false" />
        <box:NButton runat="server" ID="btnPreIncluirFormulario" Theme="Default" Icon="plus" Text="Incluir formulário" OnClick="btnPreIncluirFormulario_Click" />
    </box:NToolBar>

    <box:NGridView runat="server" ID="grdList" DataKeyNames="IdPropostaOrcamentariaDespesa" OnRowDataBound="grdList_RowDataBound" OnRowCommand="grdList_RowCommand" ItemType="Creasp.Elo.PropostaOrcamentariaDespesa">
        <Columns>
            <asp:BoundField DataField="CentroCustoOriginal.NrCentroCustoFmt" HeaderText="Número" SortExpression="CentroCustoOriginal.NrCentroCusto"
                HeaderStyle-Width="100" ItemStyle-Width="100" />
            <asp:BoundField DataField="CentroCustoOriginal.NoCentroCusto" HeaderText="Nome" SortExpression="CentroCustoOriginal.NoCentroCusto" />
            <asp:BoundField DataField="Responsavel.NoPessoa" HeaderText="Responsável" SortExpression="Responsavel.NoPessoa" />
            <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="StatusPropostaOrcamentaria.NoDescricao"
                HeaderStyle-Width="100" ItemStyle-Width="100" />
            <asp:BoundField DataField="CdTpFormulario" HeaderText="Tipo" SortExpression="CdTpFormulario"
                HeaderStyle-Width="100" ItemStyle-Width="80" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="VrOrcadoTotal" HeaderText="Total" DataFormatString="{0:n2}" SortExpression=""
                HeaderStyle-Width="100" ItemStyle-Width="100" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
            <asp:BoundField DataField="PropostaOrcamentariaItens.Count" HeaderText="Contas" SortExpression=""
                HeaderStyle-Width="60" ItemStyle-Width="60" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />

            <box:NButtonField Icon="search" CommandName="view" Theme="Info" ToolTip="Ver valores" />
            <box:NButtonField Icon="ok" CommandName="env" Theme="Info" ToolTip='Disponibilizar proposta' EnabledExpression="PodeEnviar" OnClientClick="return confirm('Deseja disponibilizar esta proposta para o próximo responsável?')" />
            <box:NButtonField Icon="left-fat" CommandName="vol" Theme="Info" ToolTip="Reprovar proposta" EnabledExpression="PodeRetornar" OnClientClick="return confirm('Deseja reprovar esta proposta para o responsável anterior?')" />
            <box:NButtonField Icon="cancel" CommandName="del" Theme="Danger" ToolTip="Excluir proposta" VisibleExpression="PodeExcluir" OnClientClick="return confirm('Deseja remover esta proposta?')" />
        </Columns>
        <EmptyDataTemplate>Não existem propostas orçamentárias para você visualizar no momento</EmptyDataTemplate>
    </box:NGridView>

    <box:NPopup runat="server" ID="pnlIncluirFormulario" Title="Incluir formulário" Width="650">
        <box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnIncluirFormulario" Theme="Primary" Text="Incluir" Icon="ok" OnClick="btnIncluirFormulario_Click" />
        </box:NToolBar>
        
        <table class="form">
            <tr>
                <td>Tipo</td>
                <td>
                    <box:NDropDownList runat="server" ID="drpTipoFormulario">
                        <asp:ListItem Value="" Text="Selecione..." Selected="True"></asp:ListItem>
                        <asp:ListItem Value="Almoxarifado" Text="Almoxarifado"></asp:ListItem>
                        <asp:ListItem Value="DAS" Text="DAS"></asp:ListItem>
                        <asp:ListItem Value="Informatica" Text="Informática"></asp:ListItem>
                    </box:NDropDownList>
                </td>
            </tr>
            <tr>
                <td>Centro de custos</td>
                <td>
                    <box:NDropDownList runat="server" ID="cmbCentroCusto" Width="450" />
                </td>
            </tr>
        </table>
    </box:NPopup>

    <box:NPopup runat="server" ID="pnlIncluirProposta" Title="Incluir proposta" Width="650">
         <box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnIncluirProposta" Theme="Primary" Text="Incluir" Icon="ok" OnClick="btnIncluirProposta_Click" />
        </box:NToolBar>
        
        <table class="form">
            <tr>
                <td>Importante</td>
                <td>A inclusão de uma proposta só poderá ser feita por centros de custos orçáveis, que possuam um executor responsável e ainda não estejam em nenhuma proposta orçamentária.
                </td>
            </tr>
            <tr>
                <td>Dica</td>
                <td>Antes de incluir um centro de custo como proposta orçamentária, certifique-se que as contas contábeis corretas estão relacionadas a ele.
                </td>
            </tr>
            <tr>
                <td>Centro de custos</td>
                <td>
                    <box:NDropDownList runat="server" ID="cmbCentroCustoParaProposta" Width="450" />
                </td>
            </tr>
        </table>
       
    </box:NPopup>

    <box:NPopup runat="server" ID="pnlErros" Title="Erros encontrados na integração" Width="600">
        <box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnEntendi" Theme="Default" Text="Entendi" Icon="ok" OnClick="btnEntendi_Click" />
        </box:NToolBar>

        <table class="form">
            <tr>
                <td>Os seguintes erros foram encontrados ao tentar enviar as informações ao SISCONT</td>
            </tr>
            <tr>
                <td>
                    <box:NTextBox runat="server" ID="txtErros" TextMode="MultiLine" Width="100%" Height="200px"></box:NTextBox></td>
            </tr>
        </table>
    </box:NPopup>

    <box:NPopup runat="server" ID="pnlMotivoRejeicao" Title="Motivo retorno" Width="650">
        <box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnConfirmarRetorno" OnClick="btnConfirmarRetorno_Click" Theme="Primary" Text="Confirmar" Icon="ok" />
            <box:NButton runat="server" ID="btnCancelarRetorno" OnClick="btnCancelarRetorno_Click" Theme="Default" Text="Cancelar" Icon="cancel" />
        </box:NToolBar>
        <table class="form">
            <tr>
                <td>Motivo</td>
                <td>
                    <box:NTextBox runat="server" ID="txtMotivoRejeicao" TextMode="MultiLine" MaxLength="1500" Width="100%" />
                </td>
            </tr>
        </table>
    </box:NPopup>
</asp:Content>


