﻿<%@ Page Title="Proposta Orçamentária" Resource="elo" Language="C#" %>

<script runat="server">

    private CreaspContext db = new CreaspContext();

    private int? IdPropostaOrcamentariaDespesa => Request.QueryString["IdPropostaOrcamentariaDespesa"].To<int?>();
    private int IdPropostaOrcamentariaDespesaItem { get { return (int)ViewState["IdPropostaOrcamentariaDespesaItem"]; } set { ViewState["IdPropostaOrcamentariaDespesaItem"] = value; } }

    private PropostaOrcamentariaDespesa Proposta;
    private List<PropostaOrcamentariaDespesaItem> ItensProposta { get { return ViewState["ItensProposta"] as List<PropostaOrcamentariaDespesaItem>; } set { ViewState["ItensProposta"] = value; } }
    private bool? ItensNaoSalvos { get { return ViewState["ItensNaoSalvos"] as Nullable<bool>; } set { ViewState["ItensNaoSalvos"] = value; } }

    //private decimal? toVrOrcadoAnoAnterior = 0;
    private decimal? toVrAprovadoAnoAnterior = 0;
    private decimal? toVrExecutadoAnoAnterior = 0;
    private decimal? toVrOrcado = 0;

    private string nrCentroCusto = string.Empty;
    private string noResponsavel = string.Empty;
    private string nrAno = string.Empty;

    #region Init
    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        if (!IdPropostaOrcamentariaDespesa.HasValue && Request.QueryString["service"]?.ToString() != "autocomplete")
        {
            Redirect("PropostaOrcamentaria.aspx");
        }

        if (IdPropostaOrcamentariaDespesa.HasValue)
        {
            if (!db.PropostasOrcamentarias.PodeAlterarProposta(IdPropostaOrcamentariaDespesa.Value))
            {
                ShowInfoBox("Você só pode visualizar esta proposta.");
                Redirect("PropostaOrcamentaria.View.aspx?IdPropostaOrcamentariaDespesa=" + IdPropostaOrcamentariaDespesa.Value);
            }

            RegisterResource("elo.propostaorcamentaria");

            grdList.RegisterDataSource(() => ItensProposta);
        }

        txtContaContabil.RegisterDataSource((q) =>
                db.Query<ContaContabil>()
                    .Where(x => x.NrAno == db.NrAnoBase)
                    .Where(x => x.FgAnalitico && x.FgAtivo)
                    .Where(x => x.NrContaContabil.StartsWith(CdPrefixoContaContabil.DespesaInicial))
                    .Where(x => x.NoContaContabil.StartsWith(q.ToLike()) || x.NrContaContabil.StartsWith(q))
                    .Limit(10)
                    .ToList());
    }

    protected override void OnPrepareForm()
    {
        if (Proposta == null)
        {
            Proposta = db.PropostasOrcamentarias.BuscaPropostaOrcamentaria(IdPropostaOrcamentariaDespesa.Value);
            lblCentroCusto.Text = Proposta.CentroCustoOriginal.NoCentroCustoFmt;
            lblResponsavel.Text = Proposta.Responsavel?.NoPessoa;
            lblAno.Text = Proposta.NrAno.ToString();

            if (!string.IsNullOrEmpty(Proposta.CdTpFormulario))
            {
                lblFormulario.Text = Proposta.CdTpFormulario;
            }
            else
            {
                lblFormulario.Text = "Não";
            }

            //if (Proposta.CdTpFormulario != null || db.Pessoas.ValidaSeUsuarioLogadoEContabilidade())
            //{
            //    btnPreIncluirContaContabil.Visible = true;
            //}
        }

        AtualizaTotalizadores();
        grdList.DataBind();

        this.Audit(Proposta, IdPropostaOrcamentariaDespesa);
        this.BackTo("PropostaOrcamentaria.aspx");
    }
    #endregion

    #region Events
    protected void grdList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("EditEspecificacao"))
        {
            IdPropostaOrcamentariaDespesaItem = Convert.ToInt32(grdList.DataKeys[e.CommandArgument.To<int>()].Value);

            txtNoEspecificacao.Text = ItensProposta
                .Where(x => x.IdPropostaOrcamentariaDespesaItem == IdPropostaOrcamentariaDespesaItem)
                .Single()?
                .NoEspecificacao;

            pnlPopupEspecificacao.Open(btnOkEspecificacao);
        }
        else if (e.CommandName.Equals("EditJustificativa"))
        {
            IdPropostaOrcamentariaDespesaItem = Convert.ToInt32(grdList.DataKeys[e.CommandArgument.To<int>()].Value);

            txtNoJustificativa.Text = ItensProposta
                .Where(x => x.IdPropostaOrcamentariaDespesaItem == IdPropostaOrcamentariaDespesaItem)
                .Single()?
                .NoJustificativa;

            pnlPopupJustificativa.Open(btnOkJustificativa);
        }
        else if (e.CommandName.Equals("del"))
        {
            IdPropostaOrcamentariaDespesaItem = Convert.ToInt32(grdList.DataKeys[e.CommandArgument.To<int>()].Value);

            db.PropostasOrcamentarias.ExcluiFormularioItem(IdPropostaOrcamentariaDespesaItem);

            CarregaItens();
            grdList.DataBind();
        }
    }

    protected void txtVrOrcado_TextChanged(object sender, EventArgs e)
    {
        ItensNaoSalvos = true;

        decimal novoValor = 0;
        Decimal.TryParse(((TextBox)sender).Text, out novoValor);

        TextBox txt = sender as TextBox;
        GridViewRow row = txt.NamingContainer as GridViewRow;
        int idPropostaOrcamentaria = Convert.ToInt32(grdList.DataKeys[row.RowIndex].Value);

        if (ItensProposta != null)
        {
            ItensProposta.Single(x => x.IdPropostaOrcamentariaDespesaItem == idPropostaOrcamentaria).VrOrcadoAntesFinalizar = novoValor;
            AtualizaTotalizadores();
        }

        grdList.DataBind();
    }

    protected void btnOkEspecificacao_Click(object sender, EventArgs e)
    {
        ItensNaoSalvos = true;

        ItensProposta.Single(x => x.IdPropostaOrcamentariaDespesaItem == IdPropostaOrcamentariaDespesaItem).NoEspecificacao = txtNoEspecificacao.Text.Trim();
        AtualizaTotalizadores();
        grdList.DataBind();
        pnlPopupEspecificacao.Close();
    }

    protected void btnOkJustificativa_Click(object sender, EventArgs e)
    {
        ItensNaoSalvos = true;

        ItensProposta.Single(x => x.IdPropostaOrcamentariaDespesaItem == IdPropostaOrcamentariaDespesaItem).NoJustificativa = txtNoJustificativa.Text.Trim();
        AtualizaTotalizadores();
        grdList.DataBind();
        pnlPopupJustificativa.Close();
    }

    protected void btnVoltarJustificativa_Click(object sender, EventArgs e)
    {
        pnlPopupJustificativa.Close();
    }

    protected void btnVoltarEspecificacao_Click(object sender, EventArgs e)
    {
        pnlPopupEspecificacao.Close();
    }

    //protected void btnVoltar_Click(object sender, EventArgs e)
    //{
    //    Redirect("PropostaOrcamentaria.aspx");
    //}

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        SalvaItens();
    }

    protected void btnRelatoriosDemDetalhado_Click(object sender, ReportFormat e)
    {
        var rpt = new Report(Server.MapPath("~/Elo/Reports/DemonstrativoDetalhado.rdl"));
        rpt.AddDataSet("dsMain", db.RelatoriosElo.MontarGridDespesaDetalhado(true, IdPropostaOrcamentariaDespesa));
        ShowReport(rpt, e, "Relatorio-DemonstrativoDespesaDetalhado-" + DateTime.Now.ToString("yyyyMMddHHmmss"));
    }

    protected void btnPreIncluirContaContabil_Click(object sender, EventArgs e)
    {
        if (ItensNaoSalvos.HasValue && ItensNaoSalvos.Value)
        {
            SalvaItens();
        }

        txtContaContabil.Value = "";
        txtContaContabil.Text = "";
        txtNrProcessoL.Text = "";

        pnlIncluirContaContabil.Open();
    }

    protected void btnSalvarContaContabil_Click(object sender, EventArgs e)
    {
        var idContaContabil = txtContaContabil.Value.To<int>();
        var nrProcessoL = txtNrProcessoL.Text.To<string>();

        if (idContaContabil == 0)
        {
            throw new NBoxException("Selecione uma conta contábil.");
        }

        db.PropostasOrcamentarias.ValidaEIncluiContaContabil(IdPropostaOrcamentariaDespesa.Value, idContaContabil, nrProcessoL);

        CarregaItens();
        AtualizaTotalizadores();
        pnlIncluirContaContabil.Close();

        ShowInfoBox("Conta adicionada com sucesso!");
    }
    #endregion

    #region Privados
    private void SalvaItens()
    {
        ItensNaoSalvos = false;

        db.PropostasOrcamentarias.Salva(ItensProposta);
        AtualizaTotalizadores();
        grdList.DataBind();
        ShowInfoBox("Alterações salvas com sucesso.");
    }

    private void CarregaItens()
    {
        ItensProposta = db.PropostasOrcamentarias.ListaPropostaOrcamentariaItens(IdPropostaOrcamentariaDespesa.Value);
        grdList.DataBind();
    }

    private void AtualizaTotalizadores()
    {
        if (ItensProposta == null)
        {
            CarregaItens();
        }

        //toVrOrcadoAnoAnterior = ItensProposta.Sum(x => x.VrOrcadoAnoAnterior);
        toVrAprovadoAnoAnterior = ItensProposta.Sum(x => x.VrAprovadoAnoAnterior);
        toVrExecutadoAnoAnterior = ItensProposta.Sum(x => x.VrExecutadoAnoAnterior);
        toVrOrcado = ItensProposta.Sum(x => x.VrOrcadoAntesFinalizar.HasValue ? x.VrOrcadoAntesFinalizar.Value : 0);

        grdList.DataBind();
    }
    #endregion

</script>
<asp:content contentplaceholderid="content" runat="server">

    <table class="form">
        <tr>
            <td>Centro de custo</td>
            <td>
                <box:NLabel runat="server" ID="lblCentroCusto" /></td>
        </tr>
         <tr>
            <td>Formulário</td>
            <td>
                <box:NLabel runat="server" ID="lblFormulario"/></td>
        </tr>
        <tr>
            <td>Responsável</td>
            <td>
                <box:NLabel runat="server" ID="lblResponsavel" /></td>
        </tr>
        <tr>
            <td>Ano</td>
            <td>
                <box:NLabel runat="server" ID="lblAno" /></td>
        </tr>
    </table>

    <box:NToolBar runat="server">
        <box:NButton runat="server" ID="btnPreIncluirContaContabil" Theme="Default" Icon="plus" Text="Incluir conta contábil" OnClick="btnPreIncluirContaContabil_Click" />
        <box:NButton runat="server" ID="btnSalvar" Theme="Primary" Icon="ok" Text="Salvar" CssClass="right" OnClick="btnSalvar_Click" />
        <uc:ReportButton runat="server" ID="btnRelatoriosDemDetalhado" Text="Gerar Relatório DESPESA DETALHADO" OnClick="btnRelatoriosDemDetalhado_Click" />
    </box:NToolBar>

    <box:NGridView runat="server" ID="grdList" ShowFooter="true" DataKeyNames="IdPropostaOrcamentariaDespesaItem" OnRowCommand="grdList_RowCommand" ItemType="Creasp.Elo.PropostaOrcamentariaDespesaItem">
        <Columns>
            <asp:TemplateField HeaderText="Número" HeaderStyle-Width="145">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblOrcadoAnoAnterior"><%# Item?.ContaContabil?.NrContaContabilFmt %></asp:Label>
                </ItemTemplate>
                <FooterTemplate>
                    Total
                </FooterTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ContaContabil.NoContaContabil" HeaderText="Nome" SortExpression="ContaContabil.NoContaContabil" />
            <%--<asp:BoundField DataField="ContaContabil.CdResumido" HeaderText="Cod." HeaderStyle-Width="45" SortExpression="ContaContabil.CdResumido" />--%>
            <asp:BoundField DataField="NrProcessoL" HeaderText="Processo" SortExpression="NrProcessoL" />
            <asp:BoundField DataField="NoEspecificacao" HeaderText="Especificação" SortExpression="NoEspecificacao" ItemStyle-CssClass="nowrap-ellipsis" />
            <box:NButtonField Icon="pencil" Theme="Info" ToolTip="Editar especificação" CommandName="EditEspecificacao" EnabledExpression="!FgVrOrcadoOrigemSenior" />
            <asp:BoundField DataField="NoJustificativa" HeaderText="Justificativa" SortExpression="NoJustificativa" ItemStyle-CssClass="nowrap-ellipsis" />
            <box:NButtonField Icon="pencil" Theme="Info" ToolTip="Editar justificativa" CommandName="EditJustificativa" EnabledExpression="!FgVrOrcadoOrigemSenior" />

            <%--<asp:TemplateField HeaderText="Orçado ano anterior">
                <HeaderTemplate>
                    <%# string.Format("Orçado {0}", DateTime.Now.Year ) %>
                </HeaderTemplate>
                <ItemTemplate>
                    <box:NLabel runat="server" Text='<%# Item.VrOrcadoAnoAnterior.ToString("n2") %>' />
                </ItemTemplate>
                <FooterTemplate>
                    <%# toVrOrcadoAnoAnterior.ToString("n2") %>
                </FooterTemplate>
            </asp:TemplateField>--%>
            <asp:TemplateField HeaderText="Dotação" ItemStyle-HorizontalAlign="Right">
                <HeaderTemplate>
                    <%# string.Format("Dotação {0}", db.NrAnoBase - 1) %>
                </HeaderTemplate>
                <ItemTemplate>
                    <box:NLabel runat="server" Text='<%# Item.VrAprovadoAnoAnterior.HasValue ? Item.VrAprovadoAnoAnterior.Value.ToString("n2") : "0,00" %>' />
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label runat="server" ID="lblAprovadoAnoAnterior"><%# toVrAprovadoAnoAnterior.HasValue ? toVrAprovadoAnoAnterior.Value.ToString("n2") : "0,00" %></asp:Label>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Executado ano anterior" ItemStyle-HorizontalAlign="Right">
                <HeaderTemplate>
                    <%# string.Format("Executado até {0}", ItensProposta.Where(x => x.DtRefVrExecutado.HasValue).FirstOrDefault() != null ? ItensProposta.Where(x => x.DtRefVrExecutado.HasValue).FirstOrDefault().DtRefVrExecutado.Value.ToString("dd/MM/yyyy") : "01/01/01") %>
                </HeaderTemplate>
                <ItemTemplate>
                    <box:NLabel runat="server" 
                        Text='<%# Item.VrExecutadoAnoAnterior.HasValue ? Item.VrExecutadoAnoAnterior.Value.ToString("n2") : "0,00" %>' 
                        ToolTip='<%# Item.DtRefVrExecutado.HasValue ? Item.DtRefVrExecutado.Value.ToString("dd/MM/yyyy") : "" %>' />
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label runat="server" ID="lblExecutadoAnoAnterior"><%# toVrExecutadoAnoAnterior.HasValue ? toVrExecutadoAnoAnterior.Value.ToString("n2") : "0,00" %></asp:Label>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Orçado">
                <HeaderTemplate>
                    <%# string.Format("Orçado {0}", db.NrAnoBase) %>
                </HeaderTemplate>
                <ItemTemplate>
                    <box:NTextBox runat="server" ID="txtVrOrcado" ToolTip="<%# Item.VrHistorico %>" MaskType="Decimal" Enabled='<%# !Item.FgEdicaoDesabilitada %>'
                        Text='<%# Item.VrOrcadoAntesFinalizar.HasValue ? Item.VrOrcadoAntesFinalizar.Value.ToString("n2") : "0,00" %>'
                        Width="100" MaxLength="20" AutoPostBack="true" OnTextChanged="txtVrOrcado_TextChanged" CssClass="t-right" />
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label runat="server" ID="lblOrcado" CssClass="t-center"><%# toVrOrcado.HasValue ? toVrOrcado.Value.ToString("n2") : "0,00" %></asp:Label>
                </FooterTemplate>
            </asp:TemplateField>
            <box:NButtonField Icon="cancel" Theme="Danger" ToolTip="Remover" CommandName="del" VisibleExpression="PropostaOrcamentariaDespesa.PodeExcluir" />

        </Columns>
        <EmptyDataTemplate>Esta proposta orçamentária está vazia.</EmptyDataTemplate>
    </box:NGridView>

    <box:NPopup runat="server" ID="pnlPopupEspecificacao" Title="Editar especificação" Width="650">
        <box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnOkEspecificacao" OnClick="btnOkEspecificacao_Click" Theme="Default" Text="OK" Icon="ok" />
            <box:NButton runat="server" ID="btnVoltarEspecificacao" OnClick="btnVoltarEspecificacao_Click" Theme="Default" Text="Cancelar" Icon="cancel" />
        </box:NToolBar>
        <table class="form">
            <tr>
                <td>Especificação</td>
                <td>
                    <box:NTextBox runat="server" ID="txtNoEspecificacao" TextMode="MultiLine" ShowTextBox="false" Enabled="true" Width="100%" />
                </td>
            </tr>
        </table>
    </box:NPopup>

    <box:NPopup runat="server" ID="pnlPopupJustificativa" Title="Editar justificativa" Width="650">
        <box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnOkJustificativa" OnClick="btnOkJustificativa_Click" Theme="Default" Text="OK" Icon="ok" />
            <box:NButton runat="server" ID="btnVoltarJustificativa" OnClick="btnVoltarJustificativa_Click" Theme="Default" Text="Cancelar" Icon="cancel" />
        </box:NToolBar>
        <table class="form">
            <tr>
                <td>Justificativa</td>
                <td>
                    <box:NTextBox runat="server" ID="txtNoJustificativa" TextMode="MultiLine" ShowTextBox="false" Enabled="true" Width="100%" />
                </td>
            </tr>
        </table>
    </box:NPopup>

    <box:NPopup runat="server" ID="pnlIncluirContaContabil" Title="Incluir contas contábeis" Width="650">
        <box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnSalvarContaContabil" OnClick="btnSalvarContaContabil_Click" Theme="Primary" Text="Salvar" Icon="ok" />
        </box:NToolBar>
        <table class="form">
            <tr>
                <td>Conta</td>
                <td>
                    <box:NAutocomplete runat="server" ID="txtContaContabil" TextCapitalize="Upper" Width="100%" DataValueField="IdContaContabil" DataTextField="NoContaContabilFmt" AutoPostBack="true" />
                </td>
            </tr>

            <tr>
                <td>Processo</td>
                <td>
                    <box:NTextBox runat="server" ID="txtNrProcessoL" TextCapitalize="Upper" Width="100%" />
                </td>
            </tr>
        </table>
    </box:NPopup>

</asp:content>
