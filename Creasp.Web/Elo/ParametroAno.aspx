﻿<%@ Page Title="Configurações" Resource="elo.configuracao" Language="C#" %>

<script runat="server">

    private CreaspContext db = new CreaspContext();

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        grdList.RegisterDataSource(() => db.ParametroAno.ListaParametrosAnos());
    }

    protected override void OnPrepareForm()
    {
        grdList.DataBind();
    }

    protected void btnIncluir_Click(object sender, EventArgs e)
    {
        txtNrAno.Text = db.NrAnoBase.ToString(); //db.ParametroAno.RetornaProximoAnoParaSerIncluido().ToString();

        txtPrAplicadoDespesa.Enabled = true;
        txtPrAplicadoDespesa.Text = "";
        txtPrAplicadoReceita.Text = "";
        txtPrCotaParteConfea.Text = "";
        txtPrCotaParteMutua.Text = "";
        txtNoObservacoesPadraoReceita.Text = "";
        txtDeDiretrizesAosExecutores.Text = "";
        txtDeDiretrizesAosExecutores.Enabled = true;

        btnOK.Visible = true;
        btnAtualizar.Visible = false;

        pnlIncluirAno.Open();
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        var nrAno = txtNrAno.Text.To<int>();
        var prAplicadoReceita = txtPrAplicadoReceita.Text.To<decimal>();
        var prAplicadoDespesa = txtPrAplicadoDespesa.Text.To<decimal>();
        var prCotaParteConfea = txtPrCotaParteConfea.Text.To<decimal>();
        var prCotaParteMutua = txtPrCotaParteMutua.Text.To<decimal>();
        var noObservacoesPadraoReceita = txtNoObservacoesPadraoReceita.Text.To<string>();
        var deDiretrizesAosExecutores = txtDeDiretrizesAosExecutores.Text.To<string>();

        db.ParametroAno.Salvar(nrAno, prAplicadoReceita, prAplicadoDespesa, prCotaParteConfea, prCotaParteMutua, noObservacoesPadraoReceita, deDiretrizesAosExecutores);
        grdList.DataBind();
        pnlIncluirAno.Close();
        ShowInfoBox("Ano criado com sucesso.");
    }

    protected void grdList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "disp")
        {
            var nrAno = e.DataKey<int>(sender);

            if (nrAno == db.NrAnoBase)
            {
                db.ParametroAno.Disponibilizar(nrAno);
                grdList.DataBind();
                ShowInfoBox("Ano disponibilizado com sucesso.");
            }
            else
            {
                ShowErrorBox("Altere o ano base para o mesmo ano que você quer disponibilizar");
            }
        }
        else if (e.CommandName == "alt")
        {
            var nrAno = e.DataKey<int>(sender);

            if (!db.ParametroAno.PodeEditarParametroAno(nrAno))
            {
                ShowErrorBox("Você não pode editar este ano.");
            }
            else
            {
                var parametroAno = db.ParametroAno.BuscaParametroAno(nrAno);

                txtNrAno.Text = parametroAno.NrAno.ToString();
                txtPrAplicadoDespesa.Enabled = false;
                txtPrAplicadoDespesa.Text = parametroAno.PrAplicadoDespesa.HasValue ? parametroAno.PrAplicadoDespesa.Value.ToString("n2") : "0,00";
                txtPrAplicadoReceita.Text = parametroAno.PrAplicadoReceita.HasValue ? parametroAno.PrAplicadoReceita.Value.ToString("n2") : "0,00";
                txtPrCotaParteConfea.Text = parametroAno.PrCotaParteConfea.HasValue ? parametroAno.PrCotaParteConfea.Value.ToString("n2") : "0,00";
                txtPrCotaParteMutua.Text = parametroAno.PrCotaParteMutua.HasValue ? parametroAno.PrCotaParteMutua.Value.ToString("n2") : "0,00";
                txtNoObservacoesPadraoReceita.Text = parametroAno.NoObservacoesReceitaPadrao;
                txtDeDiretrizesAosExecutores.Enabled = false;

                btnOK.Visible = false;
                btnAtualizar.Visible = true;

                pnlIncluirAno.Open();
            }
        }
    }

    protected void btnEnviarOrcamentoSiscont_Click(object sender, EventArgs e)
    {
        //db.Sync.TestaReformulacao();

        var lstErros = db.Sync.ValidaEEnviaOrcamentoAtual();

        if (lstErros?.Count > 0)
        {
            txtErros.Text = string.Join(" \r\n", lstErros);
            pnlErros.Open();
        }
    }

    protected void btnEntendi_Click(object sender, EventArgs e)
    {
        pnlErros.Close();
    }

    protected void btnAtualizar_Click(object sender, EventArgs e)
    {
        var nrAno = txtNrAno.Text.To<int>();
        var prAplicadoReceita = txtPrAplicadoReceita.Text.To<decimal>();
        var prCotaParteConfea = txtPrCotaParteConfea.Text.To<decimal>();
        var prCotaParteMutua = txtPrCotaParteMutua.Text.To<decimal>();

        var noObservacoesPadraoReceita = txtNoObservacoesPadraoReceita.Text.To<string>();

        db.ParametroAno.AtualizaParametroAno(nrAno, prAplicadoReceita, prCotaParteConfea, prCotaParteMutua, noObservacoesPadraoReceita);
        grdList.DataBind();
        pnlIncluirAno.Close();
        ShowInfoBox("Metodologias de receita atualizadas");
    }
</script>

<asp:Content runat="server" ContentPlaceHolderID="content">

    <box:NToolBar runat="server">
        <box:NButton runat="server" ID="btnEnviarOrcamentoSiscont" Theme="Primary" Icon="right-fat" Text="Enviar receita e despesa para SISCONT" OnClick="btnEnviarOrcamentoSiscont_Click" OnClientClick="return confirm('Deseja enviar o orçamento ao SISCONT? Isto não poderá ser desfeito.');" />
        <box:NButton runat="server" ID="btnIncluir" Theme="Default" Icon="plus" CssClass="right" Text="Incluir ano" OnClick="btnIncluir_Click" />
    </box:NToolBar>

    <box:NGridView runat="server" ID="grdList" DataKeyNames="NrAno" OnRowCommand="grdList_RowCommand" ItemType="Creasp.Elo.ParametroAno">
        <Columns>
            <asp:BoundField DataField="NrAno" HeaderText="Número" SortExpression="NrAno" />
            <asp:BoundField DataField="PrAplicadoReceita" HeaderText="% Receita" SortExpression="PrAplicadoReceita" DataFormatString="{0:n2}" />
            <asp:BoundField DataField="PrAplicadoDespesa" HeaderText="% Despesa" SortExpression="PrAplicadoDespesa" DataFormatString="{0:n2}" />
            <asp:BoundField DataField="PrCotaParteConfea" HeaderText="% Confea" SortExpression="PrCotaParteConfea" DataFormatString="{0:n2}" />
            <asp:BoundField DataField="PrCotaParteMutua" HeaderText="% Mútua" SortExpression="PrCotaParteMutua" DataFormatString="{0:n2}" />
            <asp:BoundField DataField="DtDisponibilizacao" HeaderText="Data disponibilizada" SortExpression="DtDisponibilizacao" />
            <box:NButtonField Icon="ok" CommandName="disp" Theme="Info" ToolTip="Disponibilizar" EnabledExpression="FgDisponibilizavel" OnClientClick="return confirm('Esta ação criará todas as propopostas orçamentárias e metodologias de receita. \r\nÉ altamente aconselhável a sincronizar os dados de ORÇAMENTO com sistemas externos antes de continuar. \r\nDeseja continuar?')" />
            <box:NButtonField Icon="pencil" CommandName="alt" Theme="Info" ToolTip="Editar" EnabledExpression="!FgDisponibilizavel" />
        </Columns>
        <EmptyDataTemplate>Não existem anos para você visualizar no momento</EmptyDataTemplate>
    </box:NGridView>

    <box:NPopup runat="server" ID="pnlIncluirAno" Title="Configurar parâmetro ano" Width="650">
        <box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnAtualizar" Theme="Primary" Text="Atualizar" Icon="ok" OnClick="btnAtualizar_Click" />
            <box:NButton runat="server" ID="btnOK" Theme="Primary" Text="Salvar" Icon="ok" OnClick="btnSalvar_Click" />
        </box:NToolBar>
        
        <table class="form">
            <tr>
                <td>Ano</td>
                <td>
                    <box:NTextBox runat="server" ID="txtNrAno" Disabled="True" /></td>
            </tr>
            <tr>
                <td>% despesa</td>
                <td>
                    <box:NTextBox runat="server" ID="txtPrAplicadoDespesa" /></td>
            </tr>
            <tr>
                <td>% receita</td>
                <td>
                    <box:NTextBox runat="server" ID="txtPrAplicadoReceita" /></td>
            </tr>
            <tr>
                <td>% cota-parte confea</td>
                <td>
                    <box:NTextBox runat="server" ID="txtPrCotaParteConfea" /></td>
            </tr>
            <tr>
                <td>% cota-parte mútua</td>
                <td>
                    <box:NTextBox runat="server" ID="txtPrCotaParteMutua" /></td>
            </tr>
            <tr>
                <td>Observações de receita</td>
                <td>
                    <box:NTextBox runat="server" TextMode="MultiLine" MaxLength="1000" ID="txtNoObservacoesPadraoReceita" Width="100%" /></td>
            </tr>

            <tr>
                <td>Diretrizes aos executores</td>
                <td>
                    <box:NTextBox runat="server" TextMode="MultiLine" MaxLength="1000" ID="txtDeDiretrizesAosExecutores" Width="100%" /></td>
            </tr>
        </table>
    </box:NPopup>

    <box:NPopup runat="server" ID="pnlErros" Title="Erros encontrados na integração" Width="600">
        <box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnEntendi" Theme="Default" Text="Entendi" Icon="ok" OnClick="btnEntendi_Click" />
        </box:NToolBar>
        <table class="form">
            <tr>
                <td>Não foi possível efetuar a integração pelos motivos abaixo</td>
            </tr>
            <tr>
                <td>
                    <box:NTextBox runat="server" ID="txtErros" TextMode="MultiLine" Width="100%" Height="200px"></box:NTextBox></td>
            </tr>
            <tr>
                <td>Resolva as pendências e envie os dados novamente</td>
            </tr>
        </table>
    </box:NPopup>
</asp:Content>


