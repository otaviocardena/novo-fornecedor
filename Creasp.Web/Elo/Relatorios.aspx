﻿<%@ Page Title="Relatórios do orçamento" Resource="elo.relatorio" Language="C#" %>

<%@ Import Namespace="System.Data" %>

<script runat="server">
    //TODO: Este front está acessando direto o banco de dados repetidas vezes, deve-se colocar este código na classe de serviço e refatorar para evitar a duplicação de código.

    private CreaspContext db = new CreaspContext();

    private ParametroAno _parametroAno;
    private List<ContaContabil> _contas;
    private List<PosicaoAtualOrcamento> _posicaoAtualOrcamento;
    private List<DisponibilidadeAtualOrcamento> _disponibilidadeAtualOrcamento;
    private bool _fgIncluiValoresZerados;
    private string _dtRefVrExecutadoTexto;
    private string _dtRefVrArrecadadoTexto;
    private DateTime _dtRefVrExecutado;
    private DateTime _dtRefVrArrecadado;
    private const int QTD_CARACTERES_PARA_NEGRITO = 7; //Apenas para sintético
    private const int QTD_MINIMA_LINHAS_REL_SINTETICO_DESPESA_RECEITA = 40; //Apenas para sintético

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }
    }

    protected override void OnPrepareForm()
    {
        var relatorios = new List<ListItem>();

        relatorios.Add(new ListItem("Relatório Analítico Despesa", "analiticoDespesa"));
        relatorios.Add(new ListItem("Relatório Analítico Receita", "analiticoReceita"));
        relatorios.Add(new ListItem("Relatório Sintético Despesa", "sinteticoDespesa"));
        relatorios.Add(new ListItem("Relatório Sintético Receita", "sinteticoReceita"));
        relatorios.Add(new ListItem("Relatório Reformulação Analítico Despesa", "refAnaliticoDespesa"));
        relatorios.Add(new ListItem("Relatório Reformulação Analítico Receita", "refAnaliticoReceita"));
        relatorios.Add(new ListItem("Relatório Reformulação Sintético Despesa", "refSinteticoDespesa"));
        relatorios.Add(new ListItem("Relatório Reformulação Sintético Receita", "refSinteticoReceita"));
        relatorios.Add(new ListItem("Relatório Sintético Receita e Despesa", "sinteticoReceitaDespesa"));
        relatorios.Add(new ListItem("Relatório Despesa Detalhado", "detalheDespesa"));
        relatorios.Add(new ListItem("Relatório Metodologia", "metodologiaReceita"));

        cmbRelatorios.Bind(relatorios);
    }

    protected void cmbRelatorios_SelectedIndexChanged(object sender, EventArgs e)
    {
        trFiltroNrContaContabil.Visible = cmbRelatorios.SelectedValue == "metodologiaReceita";
    }

    protected void btnGerarRelatorio_Click(object sender, ReportFormat e)
    {
        Report rpt;

        switch (cmbRelatorios.SelectedValue)
        {
            case "analiticoDespesa":
                rpt = new Report(Server.MapPath("~/Elo/Reports/DemonstrativoAnaliticoDespesa.rdl"));
                rpt.AddDataSet("dsMain", MontarGridDespesaAnalitico());
                ShowReport(rpt, e, "Relatorio-DemonstrativoAnaliticoDespesa-" + DateTime.Now.ToString("yyyyMMddHHmmss"));
                break;
            case "analiticoReceita":
                rpt = new Report(Server.MapPath("~/Elo/Reports/DemonstrativoAnaliticoReceita.rdl"));
                rpt.AddDataSet("dsMain", MontarGridReceitaAnalitico());
                ShowReport(rpt, e, "Relatorio-DemonstrativoAnaliticoReceita-" + DateTime.Now.ToString("yyyyMMddHHmmss"));
                break;
            case "sinteticoDespesa":
                rpt = new Report(Server.MapPath("~/Elo/Reports/DemonstrativoSinteticoDespesa.rdl"));
                rpt.AddDataSet("dsMain", MontarGridDespesaSintetica());
                ShowReport(rpt, e, "Relatorio-DemonstrativoSinteticoDespesa-" + DateTime.Now.ToString("yyyyMMddHHmmss"));
                break;
            case "sinteticoReceita":
                rpt = new Report(Server.MapPath("~/Elo/Reports/DemonstrativoSinteticoReceita.rdl"));
                rpt.AddDataSet("dsMain", MontarGridReceitaSintetica());
                ShowReport(rpt, e, "Relatorio-DemonstrativoSinteticoReceita-" + DateTime.Now.ToString("yyyyMMddHHmmss"));
                break;
            case "refAnaliticoDespesa":
                rpt = new Report(Server.MapPath("~/Elo/Reports/ReformulacaoAnaliticoDespesa.rdl"));
                rpt.AddDataSet("dsMain", MontarGridReformulacaoDespesaAnalitico());
                ShowReport(rpt, e, "Relatorio-ReformulcaoAnaliticoDespesa-" + DateTime.Now.ToString("yyyyMMddHHmmss"));
                break;
            case "refAnaliticoReceita":
                rpt = new Report(Server.MapPath("~/Elo/Reports/ReformulacaoAnaliticoReceita.rdl"));
                rpt.AddDataSet("dsMain", MontarGridReformulacaoReceitaAnalitico());
                ShowReport(rpt, e, "Relatorio-ReformulcaoAnaliticoReceita-" + DateTime.Now.ToString("yyyyMMddHHmmss"));
                break;
            case "refSinteticoDespesa":
                rpt = new Report(Server.MapPath("~/Elo/Reports/ReformulacaoSinteticoDespesa.rdl"));
                rpt.AddDataSet("dsMain", MontarGridReformulacaoDespesaSintetico());
                ShowReport(rpt, e, "Relatorio-ReformulcaoSinteticoDespesa-" + DateTime.Now.ToString("yyyyMMddHHmmss"));
                break;
            case "refSinteticoReceita":
                rpt = new Report(Server.MapPath("~/Elo/Reports/ReformulacaoSinteticoReceita.rdl"));
                rpt.AddDataSet("dsMain", MontarGridReformulacaoReceitaSintetico());
                ShowReport(rpt, e, "Relatorio-ReformulcaoSinteticoReceita-" + DateTime.Now.ToString("yyyyMMddHHmmss"));
                break;
            case "sinteticoReceitaDespesa":
                rpt = new Report(Server.MapPath("~/Elo/Reports/DemonstrativoSinteticoReceitaEDespesa2.rdl"));
                rpt.AddDataSet("dsReceitaDespesa", MontarGridDemonstrativoReceitaEDespesaSintetico());
                rpt.AddDataSet("dsResumo", MontarGridDemonstrativoReceitaEDespesaSinteticoResumo());
                ShowReport(rpt, e, "Relatorio-DemonstrativoSinteticoReceitaEDespesa-" + DateTime.Now.ToString("yyyyMMddHHmmss"));
                break;
            case "detalheDespesa":
                rpt = new Report(Server.MapPath("~/Elo/Reports/DemonstrativoDetalhado.rdl"));
                rpt.AddDataSet("dsMain", db.RelatoriosElo.MontarGridDespesaDetalhado(chkFgIncluiValoresZerados.Checked));
                ShowReport(rpt, e, "Relatorio-DemonstrativoDespesaDetalhado-" + DateTime.Now.ToString("yyyyMMddHHmmss"));
                break;
            case "metodologiaReceita":
                rpt = new Report(Server.MapPath("~/Elo/Reports/DemonstrativoMetodologiaReceita.rdl"));
                rpt.AddDataSet("dsMain", db.RelatoriosElo.MontarGridMetodologiaReceitaAno(chkFgIncluiValoresZerados.Checked, ucContaContabil.IdContaContabil));
                ShowReport(rpt, e, "Relatorio-DemonstrativoMetodologiaReceita-" + DateTime.Now.ToString("yyyyMMddHHmmss"));
                break;
            default:
                ShowErrorBox("Selecione um relatório");
                break;
        }
    }

    private void CarregaValoresIniciais(bool fgReceita, bool trazerAnaliticasTambem)
    {
        _parametroAno = db.ParametroAno.BuscaParametroAno(db.NrAnoBase);
        if (fgReceita)
        {
            _contas = db.ContasContabeis.BuscaContasContabeisPorPrefixo(CdPrefixoContaContabil.ReceitaInicial, trazerAnaliticasTambem);
        }
        else
        {
            _contas = db.ContasContabeis.BuscaContasContabeisPorPrefixo(CdPrefixoContaContabil.DespesaInicial, trazerAnaliticasTambem);
        }

        _contas = _contas.Select(x => new ContaContabil()
        {
            NrContaContabil = x.NrContaContabil,
            NoContaContabil = x.NoContaContabil,
            FgAnalitico = x.FgAnalitico,
            NrAno = _parametroAno.NrAno
        })
        //.Where(x => x.NrContaContabil.Length != 4)
        .Distinct()
        .OrderBy(x => x.NrContaContabil)
        .ToList();

        _posicaoAtualOrcamento = db.DotacoesOrcamentarias.ListaPosicaoAtualOrcamento(_parametroAno.NrAno - 2, _parametroAno.NrAno);
        _disponibilidadeAtualOrcamento = db.DotacoesOrcamentarias.ListaDisponibilidadeAtualOrcamento(_parametroAno.NrAno - 2, _parametroAno.NrAno);

        _fgIncluiValoresZerados = chkFgIncluiValoresZerados.Checked;

        var dtRefVrExecutado = _posicaoAtualOrcamento
            .Where(x => x.NrAno == _parametroAno.NrAno - 1) //TODO: Testar com relatórios de reformulação.
            .Select(x => x.DtRefVrExecutado).DefaultIfEmpty().Max();

        _dtRefVrExecutado = dtRefVrExecutado.HasValue ? dtRefVrExecutado.Value : DateTime.MinValue;
        _dtRefVrExecutadoTexto = dtRefVrExecutado.HasValue ? dtRefVrExecutado.Value.ToString("dd/MM/yyyy") : "01/01/0001";

        _dtRefVrArrecadado = _disponibilidadeAtualOrcamento
            .Where(x => x.NrAno == _parametroAno.NrAno - 1)
            .Select(x => x.DtFim).DefaultIfEmpty().Max();

        _dtRefVrArrecadadoTexto = _dtRefVrArrecadado.ToString("dd/MM/yyyy");
    }

    private List<RelatorioAnaliticoDespesaDTO> MontarGridDespesaAnalitico() //Demonstrativo Analítico da Despesa
    {
        CarregaValoresIniciais(false, true);

        var propostasOrcamentarias = db.PropostasOrcamentarias.ListaPropostaOrcamentariaItens();

        List<RelatorioAnaliticoDespesaDTO> registrosDoRelatorio = new List<RelatorioAnaliticoDespesaDTO>();

        foreach (var conta in _contas)
        {
            var c = new ContaContabil() { NoContaContabil = conta.NoContaContabil, NrContaContabil = conta.NrContaContabil, FgAnalitico = conta.FgAnalitico };

            decimal vrDotacaoTotal = _posicaoAtualOrcamento
                .Where(x => x.NrAno == _parametroAno.NrAno - 1)
                .Where(x => x.NrContaContabil5.StartsWith(c.NrContaContabil))
                .Sum(x => x.VrEmpExercicio + x.VrSaldoOrcamentarioExercicio).To<decimal>();

            decimal vrExecutadoTotal = _posicaoAtualOrcamento
                .Where(x => x.NrAno == _parametroAno.NrAno - 1)
                .Where(x => x.NrContaContabil5.StartsWith(c.NrContaContabil))
                .Sum(x => x.VrExecutado).To<decimal>();

            decimal vrOrcadoTotal = propostasOrcamentarias
                .Where(x => x.ContaContabil.NrContaContabil.StartsWith(c.NrContaContabil))
                .Sum(x => x.VrOrcadoAntesFinalizar).To<decimal>();

            decimal prExecutado = vrDotacaoTotal != 0 ? (vrExecutadoTotal / vrDotacaoTotal * 100) : 0;
            decimal prOrcado = vrDotacaoTotal != 0 ? (vrOrcadoTotal * 100 / vrDotacaoTotal - 100) : 0;

            if (_fgIncluiValoresZerados || vrDotacaoTotal != 0 || vrExecutadoTotal != 0 || vrOrcadoTotal != 0)
            {
                RelatorioAnaliticoDespesaDTO registroRelatorio = new RelatorioAnaliticoDespesaDTO();
                registroRelatorio.NrAno = _parametroAno.NrAno;
                registroRelatorio.DtRefVrExecutado = _dtRefVrExecutado;
                registroRelatorio.Bold = c.FgAnalitico == false;
                registroRelatorio.NrContaContabil = c.NrContaContabilFmt;
                registroRelatorio.NoContaContabil = c.NoContaContabil;
                registroRelatorio.VrAprovado = vrDotacaoTotal;
                registroRelatorio.VrExecutado = vrExecutadoTotal;
                registroRelatorio.PrExecutado = prExecutado;
                registroRelatorio.VrOrcado = vrOrcadoTotal;
                registroRelatorio.PrOrcado = prOrcado;

                registrosDoRelatorio.Add(registroRelatorio);
            }
        }

        return registrosDoRelatorio;
    }

    private List<RelatorioAnaliticoReceitaDTO> MontarGridReceitaAnalitico() //Demonstrativo Analítico da Receita
    {
        CarregaValoresIniciais(true, true);

        var propostasOrcamentarias = db.PropostasOrcamentarias.ListaPropostaOrcamentariaItens();

        List<RelatorioAnaliticoReceitaDTO> registrosDoRelatorio = new List<RelatorioAnaliticoReceitaDTO>();

        var metodologias = db.MetodologiasReceita.BuscaMetodologias(_parametroAno.NrAno);

        _disponibilidadeAtualOrcamento.ForEach(x => x.NrContaContabil5 = x.NrContaContabil5.Replace(CdPrefixoContaContabil.ReceitaReformulacao, CdPrefixoContaContabil.ReceitaInicial));

        foreach (var c in _contas)
        {
            var p = metodologias.Where(x => x.ContaContabil.NrContaContabil.StartsWith(c.NrContaContabil)).ToList();

            var lstDisponibilidadeConta = _disponibilidadeAtualOrcamento
                                .Where(x => x.NrAno == _parametroAno.NrAno - 1)
                                .Where(x => x.NrContaContabil5.StartsWith(c.NrContaContabil))
                                .ToList();

            decimal vrAprovadoTotal = lstDisponibilidadeConta.Sum(x => x.VrOrcado).To<decimal>();
            decimal vrExecutado = lstDisponibilidadeConta.Sum(x => x.VrArrecadadoPeriodo).To<decimal>();

            decimal vrOrcadoTotal = p.Sum(x => x.VrOrcado).To<decimal>();

            decimal prExecutado = vrAprovadoTotal != 0 ? (vrExecutado / vrAprovadoTotal * 100) : 0;
            decimal prOrcado = vrAprovadoTotal != 0 ? (vrOrcadoTotal * 100 / vrAprovadoTotal - 100) : 0;

            if (_fgIncluiValoresZerados || vrAprovadoTotal != 0 || vrOrcadoTotal != 0)
            {
                RelatorioAnaliticoReceitaDTO registroRelatorio = new RelatorioAnaliticoReceitaDTO();
                registroRelatorio.NrAno = _parametroAno.NrAno;
                registroRelatorio.DtRefVrArrecadado = _dtRefVrArrecadado;
                registroRelatorio.Bold = c.FgAnalitico == false;
                registroRelatorio.NrContaContabil = c.NrContaContabilFmt;
                registroRelatorio.NoContaContabil = c.NoContaContabil;
                registroRelatorio.VrAprovado = vrAprovadoTotal;
                registroRelatorio.VrExecutado = vrExecutado;
                registroRelatorio.PrExecutado = prExecutado;
                registroRelatorio.VrOrcado = vrOrcadoTotal;
                registroRelatorio.PrOrcado = prOrcado;

                registrosDoRelatorio.Add(registroRelatorio);
            }
        }

        return registrosDoRelatorio;
    }

    private List<RelatorioSinteticoDespesaDTO> MontarGridDespesaSintetica() //Demonstrativo Sintético da Despesa
    {
        CarregaValoresIniciais(false, false);

        var propostasOrcamentarias = db.PropostasOrcamentarias.ListaPropostaOrcamentariaItens();

        List<RelatorioSinteticoDespesaDTO> registrosDoRelatorio = new List<RelatorioSinteticoDespesaDTO>();

        foreach (var c in _contas)
        {
            var vrOrcado2AnoAtras = _posicaoAtualOrcamento
                .Where(x => x.NrAno == _parametroAno.NrAno - 2)
                .Where(x => x.NrContaContabil5.StartsWith(c.NrContaContabil))
                .Sum(x => x.VrEmpExercicio + x.VrSaldoOrcamentarioExercicio).To<decimal>();

            var vrExecutado2AnoAtras = _posicaoAtualOrcamento.Where(x => x.NrAno == _parametroAno.NrAno - 2 && x.NrContaContabil5.StartsWith(c.NrContaContabil)).Sum(x => x.VrExecutado);

            var vrOrcado1AnoAtras = _posicaoAtualOrcamento
                .Where(x => x.NrAno == _parametroAno.NrAno - 1)
                .Where(x => x.NrContaContabil5.StartsWith(c.NrContaContabil))
                .Sum(x => x.VrEmpExercicio + x.VrSaldoOrcamentarioExercicio).To<decimal>();

            var vrExecutado1AnoAtras = _posicaoAtualOrcamento.Where(x => x.NrAno == _parametroAno.NrAno - 1 && x.NrContaContabil5.StartsWith(c.NrContaContabil)).Sum(x => x.VrExecutado);
            var vrOrcado = propostasOrcamentarias.Where(x => x.ContaContabil.NrContaContabil.StartsWith(c.NrContaContabil)).Sum(x => x.VrOrcadoAntesFinalizar);

            if (_fgIncluiValoresZerados || vrOrcado2AnoAtras != 0 || vrExecutado2AnoAtras != 0 || vrOrcado1AnoAtras != 0 || vrOrcado1AnoAtras != 0 || vrExecutado1AnoAtras != 0 || vrOrcado != 0)
            {
                RelatorioSinteticoDespesaDTO registroRelatorio = new RelatorioSinteticoDespesaDTO();
                registroRelatorio.NrAno = _parametroAno.NrAno;
                registroRelatorio.DtRefVrExecutado = _dtRefVrExecutado;
                registroRelatorio.Bold = c.NrContaContabil.Length <= QTD_CARACTERES_PARA_NEGRITO;
                registroRelatorio.NrContaContabil = c.NrContaContabilFmt;
                registroRelatorio.NoContaContabil = c.NoContaContabil;
                registroRelatorio.VrOrcado2AnoAtras = vrOrcado2AnoAtras;
                registroRelatorio.VrExecutado2AnoAtras = vrExecutado2AnoAtras.HasValue ? vrExecutado2AnoAtras.Value : 0;
                registroRelatorio.VrOrcado1AnoAtras = vrOrcado1AnoAtras;
                registroRelatorio.VrExecutado1AnoAtras = vrExecutado1AnoAtras.HasValue ? vrExecutado1AnoAtras.Value : 0;
                registroRelatorio.VrOrcado = vrOrcado.HasValue ? vrOrcado.Value : 0;

                registrosDoRelatorio.Add(registroRelatorio);
            }
        }

        return registrosDoRelatorio;
    }

    private List<RelatorioSinteticoReceitaDTO> MontarGridReceitaSintetica() //Demonstrativo Sintético da Receita
    {
        CarregaValoresIniciais(true, false);

        var metodologias = db.MetodologiasReceita.BuscaMetodologias(_parametroAno.NrAno);

        List<RelatorioSinteticoReceitaDTO> registrosDoRelatorio = new List<RelatorioSinteticoReceitaDTO>();

        _disponibilidadeAtualOrcamento.ForEach(x => x.NrContaContabil5 = x.NrContaContabil5.Replace(CdPrefixoContaContabil.ReceitaReformulacao, CdPrefixoContaContabil.ReceitaInicial));

        foreach (var c in _contas)
        {
            var lstDisponibilidadeConta = _disponibilidadeAtualOrcamento
                                .Where(x => x.NrAno == _parametroAno.NrAno - 1 || x.NrAno == _parametroAno.NrAno - 2)
                                .Where(x => x.NrContaContabil5.StartsWith(c.NrContaContabil))
                                .ToList();

            var vrOrcado2AnoAtras = lstDisponibilidadeConta
                .Where(x => x.NrAno == _parametroAno.NrAno - 2)
                .Sum(x => x.VrOrcado);

            var vrExecutado2AnoAtras = lstDisponibilidadeConta
                .Where(x => x.NrAno == _parametroAno.NrAno - 2)
                .Sum(x => x.VrArrecadadoPeriodo);

            var vrOrcado1AnoAtras = lstDisponibilidadeConta
                .Where(x => x.NrAno == _parametroAno.NrAno - 1)
                .Sum(x => x.VrOrcado);

            var vrExecutado1AnoAtras = lstDisponibilidadeConta
                .Where(x => x.NrAno == _parametroAno.NrAno - 1)
                .Sum(x => x.VrArrecadadoPeriodo);

            var vrOrcado = metodologias
                .Where(x => x.ContaContabil.NrContaContabil.StartsWith(c.NrContaContabil))
                .Sum(x => x.VrOrcado);

            if (_fgIncluiValoresZerados || vrOrcado2AnoAtras != 0 || vrExecutado2AnoAtras != 0 || vrOrcado1AnoAtras != 0 || vrOrcado1AnoAtras != 0 || vrExecutado1AnoAtras != 0 || vrOrcado != 0)
            {
                RelatorioSinteticoReceitaDTO registroRelatorio = new RelatorioSinteticoReceitaDTO();

                registroRelatorio.NrAno = _parametroAno.NrAno;
                registroRelatorio.DtRefVrArrecadado = _dtRefVrArrecadado;
                registroRelatorio.Bold = c.NrContaContabil.Length <= QTD_CARACTERES_PARA_NEGRITO; //c.FgAnalitico == false;
                registroRelatorio.NrContaContabil = c.NrContaContabilFmt;
                registroRelatorio.NoContaContabil = c.NoContaContabil;
                registroRelatorio.VrOrcado2AnoAtras = vrOrcado2AnoAtras;
                registroRelatorio.VrExecutado2AnoAtras = vrExecutado2AnoAtras;
                registroRelatorio.VrOrcado1AnoAtras = vrOrcado1AnoAtras;
                registroRelatorio.VrExecutado1AnoAtras = vrExecutado1AnoAtras;
                registroRelatorio.VrOrcado = vrOrcado.HasValue ? vrOrcado.Value : 0;

                registrosDoRelatorio.Add(registroRelatorio);
            }
        }

        return registrosDoRelatorio;
    }

    private List<RelatorioReformulacaoAnaliticoDespesaDTO> MontarGridReformulacaoDespesaAnalitico() //Reformulção Analítico da Despesa
    {
        CarregaValoresIniciais(false, true);

        List<Creasp.Elo.ReformulacaoOrcamentariaItem> reformulacoesOrcamentariasItens = new List<Creasp.Elo.ReformulacaoOrcamentariaItem>();
        List<Creasp.Elo.ReformulacaoOrcamentariaItem> reformulacoesOrcamentariasItensAnterior = new List<Creasp.Elo.ReformulacaoOrcamentariaItem>();

        var propostasOrcamentarias = db.PropostasOrcamentarias.ListaPropostaOrcamentariaItens();
        var reformulacoes = db.Reformulacoes.ListaReformulacoes();

        if (reformulacoes.Count > 0) reformulacoesOrcamentariasItens = db.Reformulacoes.ListaReformulacaoOrcamentariaItens(reformulacoes[0].IdReformulacao);
        if (reformulacoes.Count > 1) reformulacoesOrcamentariasItensAnterior = db.Reformulacoes.ListaReformulacaoOrcamentariaItens(reformulacoes[1].IdReformulacao);

        List<RelatorioReformulacaoAnaliticoDespesaDTO> registrosDoRelatorio = new List<RelatorioReformulacaoAnaliticoDespesaDTO>();


        foreach (var conta in _contas)
        {
            var c = new ContaContabil() { NoContaContabil = conta.NoContaContabil, NrContaContabil = conta.NrContaContabil, FgAnalitico = conta.FgAnalitico };

            decimal vrOrcadoInicialmente = propostasOrcamentarias
                .Where(x => x.ContaContabil.NrContaContabil.StartsWith(c.NrContaContabil))
                .Sum(x => x.VrOrcadoAntesFinalizar).To<decimal>();

            decimal vrReformulacaoAnterior = reformulacoesOrcamentariasItensAnterior
                .Where(x => x.ContaContabil.NrContaContabil.StartsWith(c.NrContaContabil))
                .Sum(x => x.VrOrcamentoReformulado).To<decimal>();

            decimal vrExecutadoTotal = _posicaoAtualOrcamento
                .Where(x => x.NrAno == _parametroAno.NrAno)
                .Where(x => x.NrContaContabil5.StartsWith(c.NrContaContabil))
                .Sum(x => x.VrExecutado).To<decimal>();

            decimal vrOrcamentoReformulado = reformulacoesOrcamentariasItens
                .Where(x => x.ContaContabil.NrContaContabil.StartsWith(c.NrContaContabil))
                .Sum(x => x.VrOrcamentoReformulado).To<decimal>();

            decimal vrDiferenca = 0;

            if (reformulacoesOrcamentariasItensAnterior.Count > 0) vrDiferenca = vrOrcamentoReformulado - vrReformulacaoAnterior;
            else vrDiferenca = vrOrcamentoReformulado - vrOrcadoInicialmente;

            decimal vrSuplementacao = 0;
            decimal vrReducao = 0;

            if (vrDiferenca > 0)
            {
                vrSuplementacao = vrDiferenca;
            }
            else
            {
                vrReducao = vrDiferenca * -1;
            }

            decimal prExecutado = vrExecutadoTotal != 0 ? (vrOrcadoInicialmente * 100 / vrExecutadoTotal - 100) : 0;
            decimal prSuplementacao = vrOrcadoInicialmente != 0 ? (vrSuplementacao / vrOrcadoInicialmente * 100) : 0;
            decimal prReducao = vrOrcadoInicialmente != 0 ? (vrReducao / vrOrcadoInicialmente * 100) : 0;

            if (_fgIncluiValoresZerados || vrOrcadoInicialmente != 0 || vrExecutadoTotal != 0 || vrSuplementacao != 0 || vrReducao != 0 || vrOrcamentoReformulado != 0)
            {
                RelatorioReformulacaoAnaliticoDespesaDTO registroRelatorio = new RelatorioReformulacaoAnaliticoDespesaDTO();
                registroRelatorio.NrAno = _parametroAno.NrAno;
                registroRelatorio.DtRefVrExecutado = _dtRefVrExecutado;
                registroRelatorio.Bold = c.FgAnalitico == false;
                registroRelatorio.NrContaContabil = c.NrContaContabilFmt;
                registroRelatorio.NoContaContabil = c.NoContaContabil;
                registroRelatorio.VrOrcadoInicialmente = vrOrcadoInicialmente;
                registroRelatorio.VrReformulacaoAnterior = vrReformulacaoAnterior;
                registroRelatorio.VrExecutado = vrExecutadoTotal;
                registroRelatorio.PrExecutado = prExecutado;
                registroRelatorio.VrSuplementacao = vrSuplementacao;
                registroRelatorio.PrSuplementacao = prSuplementacao;
                registroRelatorio.VrReducao = vrReducao;
                registroRelatorio.PrReducao = prReducao;
                registroRelatorio.VrOrcamentoReformulado = vrOrcamentoReformulado;

                registrosDoRelatorio.Add(registroRelatorio);
            }
        }

        return registrosDoRelatorio;
    }

    private List<RelatorioReformulacaoSinteticoDespesaDTO> MontarGridReformulacaoDespesaSintetico() //Reformulção Sintético da Despesa
    {
        CarregaValoresIniciais(false, false);

        List<Creasp.Elo.ReformulacaoOrcamentariaItem> reformulacoesOrcamentariasItens = new List<Creasp.Elo.ReformulacaoOrcamentariaItem>();
        List<Creasp.Elo.ReformulacaoOrcamentariaItem> reformulacoesOrcamentariasItensAnterior = new List<Creasp.Elo.ReformulacaoOrcamentariaItem>();

        var propostasOrcamentarias = db.PropostasOrcamentarias.ListaPropostaOrcamentariaItens();
        var reformulacoes = db.Reformulacoes.ListaReformulacoes();

        if (reformulacoes.Count > 0) reformulacoesOrcamentariasItens = db.Reformulacoes.ListaReformulacaoOrcamentariaItens(reformulacoes[0].IdReformulacao);
        if (reformulacoes.Count > 1) reformulacoesOrcamentariasItensAnterior = db.Reformulacoes.ListaReformulacaoOrcamentariaItens(reformulacoes[1].IdReformulacao);

        List<RelatorioReformulacaoSinteticoDespesaDTO> registrosDoRelatorio = new List<RelatorioReformulacaoSinteticoDespesaDTO>();

        foreach (var conta in _contas)
        {
            var c = new ContaContabil() { NoContaContabil = conta.NoContaContabil, NrContaContabil = conta.NrContaContabil, FgAnalitico = conta.FgAnalitico };

            decimal vrOrcadoInicialmente = propostasOrcamentarias
                .Where(x => x.ContaContabil.NrContaContabil.StartsWith(c.NrContaContabil))
                .Sum(x => x.VrOrcadoAntesFinalizar).To<decimal>();

            decimal vrReformulacaoAnterior = reformulacoesOrcamentariasItensAnterior
                .Where(x => x.ContaContabil.NrContaContabil.StartsWith(c.NrContaContabil))
                .Sum(x => x.VrOrcamentoReformulado).To<decimal>();

            decimal vrExecutadoTotal = _posicaoAtualOrcamento
                .Where(x => x.NrAno == _parametroAno.NrAno)
                .Where(x => x.NrContaContabil5.StartsWith(c.NrContaContabil))
                .Sum(x => x.VrExecutado).To<decimal>();

            decimal vrOrcamentoReformulado = reformulacoesOrcamentariasItens
                .Where(x => x.ContaContabil.NrContaContabil.StartsWith(c.NrContaContabil))
                .Sum(x => x.VrOrcamentoReformulado).To<decimal>(); //Não é necessário fazer este cálculo, apenas fazer "inicial - redução + suplementação".

            decimal vrDiferenca = 0;

            if (reformulacoesOrcamentariasItensAnterior.Count > 0) vrDiferenca = vrOrcamentoReformulado - vrReformulacaoAnterior;
            else vrDiferenca = vrOrcamentoReformulado - vrOrcadoInicialmente;

            decimal vrSuplementacao = 0;
            decimal vrReducao = 0;

            if (vrDiferenca > 0)
            {
                vrSuplementacao = vrDiferenca;
            }
            else
            {
                vrReducao = vrDiferenca * -1;
            }

            decimal prExecutado = vrExecutadoTotal != 0 ? (vrOrcadoInicialmente * 100 / vrExecutadoTotal - 100) : 0;
            decimal prSuplementacao = vrOrcadoInicialmente != 0 ? (vrSuplementacao / vrOrcadoInicialmente * 100) : 0;
            decimal prReducao = vrOrcadoInicialmente != 0 ? (vrReducao / vrOrcadoInicialmente * 100) : 0;

            if (_fgIncluiValoresZerados || vrOrcadoInicialmente != 0 || vrExecutadoTotal != 0 || vrSuplementacao != 0 || vrReducao != 0 || vrOrcamentoReformulado != 0)
            {
                RelatorioReformulacaoSinteticoDespesaDTO registroRelatorio = new RelatorioReformulacaoSinteticoDespesaDTO();
                registroRelatorio.NrAno = _parametroAno.NrAno;
                registroRelatorio.DtRefVrExecutado = _dtRefVrExecutado;
                registroRelatorio.Bold = c.NrContaContabil.Length <= QTD_CARACTERES_PARA_NEGRITO;
                registroRelatorio.NrContaContabil = c.NrContaContabilFmt;
                registroRelatorio.NoContaContabil = c.NoContaContabil;
                registroRelatorio.VrOrcadoInicialmente = vrOrcadoInicialmente;
                registroRelatorio.VrReformulacaoAnterior = vrReformulacaoAnterior;
                registroRelatorio.VrExecutado = vrExecutadoTotal;
                registroRelatorio.PrExecutado = prExecutado;
                registroRelatorio.VrSuplementacao = vrSuplementacao;
                registroRelatorio.PrSuplementacao = prSuplementacao;
                registroRelatorio.VrReducao = vrReducao;
                registroRelatorio.PrReducao = prReducao;
                registroRelatorio.VrOrcamentoReformulado = vrOrcamentoReformulado;

                registrosDoRelatorio.Add(registroRelatorio);
            }
        }

        return registrosDoRelatorio;
    }

    private List<RelatorioReformulacaoAnaliticoReceitaDTO> MontarGridReformulacaoReceitaAnalitico() //Reformulção Analítico da Receita
    {
        CarregaValoresIniciais(true, true);

        List<Creasp.Elo.MetodologiaReceitaAno> reformulacaoMetodologias = new List<Creasp.Elo.MetodologiaReceitaAno>();
        List<Creasp.Elo.MetodologiaReceitaAno> reformulacaoMetodologiasAnterior = new List<Creasp.Elo.MetodologiaReceitaAno>();

        var metodologiasOriginais = db.MetodologiasReceita.BuscaMetodologias(db.NrAnoBase);
        var reformulacoes = db.Reformulacoes.ListaReformulacoes();

        if (reformulacoes.Count > 0) reformulacaoMetodologias = db.MetodologiasReceita.BuscaMetodologias(db.NrAnoBase, reformulacoes[0].IdReformulacao);
        if (reformulacoes.Count > 1) reformulacaoMetodologiasAnterior = db.MetodologiasReceita.BuscaMetodologias(db.NrAnoBase, reformulacoes[1].IdReformulacao);

        List<RelatorioReformulacaoAnaliticoReceitaDTO> registrosDoRelatorio = new List<RelatorioReformulacaoAnaliticoReceitaDTO>();

        foreach (var conta in _contas)
        {
            var c = new ContaContabil() { NoContaContabil = conta.NoContaContabil, NrContaContabil = conta.NrContaContabil, FgAnalitico = conta.FgAnalitico };

            decimal vrOrcadoInicialmente = metodologiasOriginais
                .Where(x => x.ContaContabil.NrContaContabil.StartsWith(c.NrContaContabil))
                .Sum(x => x.VrOrcado).To<decimal>();

            decimal vrReformulacaoAnterior = reformulacaoMetodologiasAnterior
                .Where(x => x.ContaContabil.NrContaContabil.StartsWith(c.NrContaContabil))
                .Sum(x => x.VrOrcado).To<decimal>();

            decimal vrExecutadoTotal = _disponibilidadeAtualOrcamento
                .Where(x => x.NrAno == _parametroAno.NrAno)
                .Where(x => x.NrContaContabil5.Replace("5212", "5211").StartsWith(c.NrContaContabil))
                .Sum(x => x.VrArrecadadoPeriodo).To<decimal>();

            decimal vrOrcamentoReformulado = reformulacaoMetodologias
                .Where(x => x.ContaContabil.NrContaContabil.StartsWith(c.NrContaContabil))
                .Sum(x => x.VrOrcado).To<decimal>();

            decimal vrDiferenca = 0;

            if (reformulacaoMetodologiasAnterior.Count > 0) vrDiferenca = vrOrcamentoReformulado - vrReformulacaoAnterior;
            else vrDiferenca = vrOrcamentoReformulado - vrOrcadoInicialmente;

            decimal vrSuplementacao = 0;
            decimal vrReducao = 0;

            if (vrDiferenca > 0)
            {
                vrSuplementacao = vrDiferenca;
            }
            else
            {
                vrReducao = vrDiferenca * -1;
            }

            decimal prExecutado = vrOrcadoInicialmente != 0 ? (vrExecutadoTotal * 100 / vrOrcadoInicialmente): 0;
            decimal prSuplementacao = vrOrcadoInicialmente != 0 ? (vrSuplementacao / vrOrcadoInicialmente * 100) : 0;
            decimal prReducao = vrOrcadoInicialmente != 0 ? (vrReducao / vrOrcadoInicialmente * 100) : 0;

            if (_fgIncluiValoresZerados || vrOrcadoInicialmente != 0 || vrExecutadoTotal != 0 || vrSuplementacao != 0 || vrReducao != 0 || vrOrcamentoReformulado != 0)
            {
                RelatorioReformulacaoAnaliticoReceitaDTO registroRelatorio = new RelatorioReformulacaoAnaliticoReceitaDTO();
                registroRelatorio.NrAno = _parametroAno.NrAno;
                registroRelatorio.DtRefVrArrecadado = _dtRefVrArrecadado;
                registroRelatorio.Bold = c.FgAnalitico == false;
                registroRelatorio.NrContaContabil = c.NrContaContabilFmt;
                registroRelatorio.NoContaContabil = c.NoContaContabil;
                registroRelatorio.VrOrcadoInicialmente = vrOrcadoInicialmente;
                registroRelatorio.VrReformulacaoAnterior = vrReformulacaoAnterior;
                registroRelatorio.VrExecutado = vrExecutadoTotal;
                registroRelatorio.PrExecutado = prExecutado;
                registroRelatorio.VrSuplementacao = vrSuplementacao;
                registroRelatorio.PrSuplementacao = prSuplementacao;
                registroRelatorio.VrReducao = vrReducao;
                registroRelatorio.PrReducao = prReducao;
                registroRelatorio.VrOrcamentoReformulado = vrOrcamentoReformulado;

                registrosDoRelatorio.Add(registroRelatorio);
            }
        }

        return registrosDoRelatorio;
    }

    private List<RelatorioReformulacaoSinteticoReceitaDTO> MontarGridReformulacaoReceitaSintetico() //Reformulção Sintético da Receita
    {
        CarregaValoresIniciais(true, false);

        List<Creasp.Elo.MetodologiaReceitaAno> reformulacaoMetodologias = new List<Creasp.Elo.MetodologiaReceitaAno>();
        List<Creasp.Elo.MetodologiaReceitaAno> reformulacaoMetodologiasAnterior = new List<Creasp.Elo.MetodologiaReceitaAno>();

        var metodologiasOriginais = db.MetodologiasReceita.BuscaMetodologias(db.NrAnoBase);
        var reformulacoes = db.Reformulacoes.ListaReformulacoes();

        if (reformulacoes.Count > 0) reformulacaoMetodologias = db.MetodologiasReceita.BuscaMetodologias(db.NrAnoBase, reformulacoes[0].IdReformulacao);
        if (reformulacoes.Count > 1) reformulacaoMetodologiasAnterior = db.MetodologiasReceita.BuscaMetodologias(db.NrAnoBase, reformulacoes[1].IdReformulacao);

        List<RelatorioReformulacaoSinteticoReceitaDTO> registrosDoRelatorio = new List<RelatorioReformulacaoSinteticoReceitaDTO>();

        foreach (var conta in _contas)
        {
            var c = new ContaContabil() { NoContaContabil = conta.NoContaContabil, NrContaContabil = conta.NrContaContabil, FgAnalitico = conta.FgAnalitico };

            decimal vrOrcadoInicialmente = metodologiasOriginais
                .Where(x => x.ContaContabil.NrContaContabil.StartsWith(c.NrContaContabil))
                .Sum(x => x.VrOrcado).To<decimal>();

            decimal vrReformulacaoAnterior = reformulacaoMetodologiasAnterior
                .Where(x => x.ContaContabil.NrContaContabil.StartsWith(c.NrContaContabil))
                .Sum(x => x.VrOrcado).To<decimal>();

            decimal vrExecutadoTotal = _disponibilidadeAtualOrcamento
                .Where(x => x.NrAno == _parametroAno.NrAno)
                .Where(x => x.NrContaContabil5.Replace("5212", "5211").StartsWith(c.NrContaContabil))
                .Sum(x => x.VrArrecadadoPeriodo).To<decimal>();

            decimal vrOrcamentoReformulado = reformulacaoMetodologias
                .Where(x => x.ContaContabil.NrContaContabil.StartsWith(c.NrContaContabil))
                .Sum(x => x.VrOrcado).To<decimal>();

            decimal vrDiferenca = 0;

            if (reformulacaoMetodologiasAnterior.Count > 0) vrDiferenca = vrOrcamentoReformulado - vrReformulacaoAnterior;
            else vrDiferenca = vrOrcamentoReformulado - vrOrcadoInicialmente;

            decimal vrSuplementacao = 0;
            decimal vrReducao = 0;

            if (vrDiferenca > 0)
            {
                vrSuplementacao = vrDiferenca;
            }
            else
            {
                vrReducao = vrDiferenca * -1;
            }

            decimal prExecutado = vrOrcadoInicialmente != 0 ? (vrExecutadoTotal * 100 / vrOrcadoInicialmente) : 0;
            decimal prSuplementacao = vrOrcadoInicialmente != 0 ? (vrSuplementacao / vrOrcadoInicialmente * 100) : 0;
            decimal prReducao = vrOrcadoInicialmente != 0 ? (vrReducao / vrOrcadoInicialmente * 100) : 0;

            if (_fgIncluiValoresZerados || vrOrcadoInicialmente != 0 || vrExecutadoTotal != 0 || vrSuplementacao != 0 || vrReducao != 0 || vrOrcamentoReformulado != 0)
            {
                RelatorioReformulacaoSinteticoReceitaDTO registroRelatorio = new RelatorioReformulacaoSinteticoReceitaDTO();
                registroRelatorio.NrAno = _parametroAno.NrAno;
                registroRelatorio.DtRefVrArrecadado = _dtRefVrArrecadado;
                registroRelatorio.Bold = c.NrContaContabil.Length <= QTD_CARACTERES_PARA_NEGRITO;
                registroRelatorio.NrContaContabil = c.NrContaContabilFmt;
                registroRelatorio.NoContaContabil = c.NoContaContabil;
                registroRelatorio.VrOrcadoInicialmente = vrOrcadoInicialmente;
                registroRelatorio.VrReformulacaoAnterior = vrReformulacaoAnterior;
                registroRelatorio.VrExecutado = vrExecutadoTotal;
                registroRelatorio.PrExecutado = prExecutado;
                registroRelatorio.VrSuplementacao = vrSuplementacao;
                registroRelatorio.PrSuplementacao = prSuplementacao;
                registroRelatorio.VrReducao = vrReducao;
                registroRelatorio.PrReducao = prReducao;
                registroRelatorio.VrOrcamentoReformulado = vrOrcamentoReformulado;

                registrosDoRelatorio.Add(registroRelatorio);
            }
        }

        return registrosDoRelatorio;
    }

    private List<RelatorioDemonstrativoReceitaDespesaSinteticoDTO> MontarGridDemonstrativoReceitaEDespesaSintetico() //Reformulção Sintético da Receita
    {
        var contasReceita = db.ContasContabeis.BuscaContasContabeisPorPrefixo(CdPrefixoContaContabil.ReceitaInicial, false);
        var contasDespesa = db.ContasContabeis.BuscaContasContabeisPorPrefixo(CdPrefixoContaContabil.DespesaInicial, false);

        var metodologiasOriginais = db.MetodologiasReceita.BuscaMetodologias(db.NrAnoBase);
        var propostasOrcamentariasItens = db.PropostasOrcamentarias.ListaPropostaOrcamentariaItens();

        List<RelatorioDemonstrativoReceitaDespesaSinteticoDTO> registrosDoRelatorio = new List<RelatorioDemonstrativoReceitaDespesaSinteticoDTO>();

        if (!_fgIncluiValoresZerados) //filtra contas de despesa e receita que possuem valor orçado.
        {
            contasDespesa.ForEach(c => c.IdContaContabil = propostasOrcamentariasItens
                            .Where(x => x.ContaContabil.NrContaContabil.StartsWith(c.NrContaContabil))
                            .Sum(x => x.VrOrcadoAntesFinalizar).To<decimal>() > 0 ? c.IdContaContabil : 0);

            contasReceita.ForEach(c => c.IdContaContabil = metodologiasOriginais
                            .Where(x => x.ContaContabil.NrContaContabil.StartsWith(c.NrContaContabil))
                            .Sum(x => x.VrOrcado).To<decimal>() > 0 ? c.IdContaContabil : 0);

            contasDespesa = contasDespesa.Where(c => c.IdContaContabil != 0).ToList();
            contasReceita = contasReceita.Where(c => c.IdContaContabil != 0).ToList();
        }

        int contadorLinhas = contasReceita.Count > contasDespesa.Count ? contasReceita.Count : contasDespesa.Count;

        for (int i = 0; i < contadorLinhas; i++)
        {
            if (i < contasReceita.Count || i < contasDespesa.Count)
            {
                RelatorioDemonstrativoReceitaDespesaSinteticoDTO registroRelatorio = new RelatorioDemonstrativoReceitaDespesaSinteticoDTO();
                registroRelatorio.NrAno = db.NrAnoBase;

                if (i < contasReceita.Count)
                {
                    var c = new ContaContabil() { NoContaContabil = contasReceita[i].NoContaContabil, NrContaContabil = contasReceita[i].NrContaContabil, FgAnalitico = contasReceita[i].FgAnalitico };

                    decimal vrOrcadoReceita = metodologiasOriginais
                        .Where(x => x.ContaContabil.NrContaContabil.StartsWith(c.NrContaContabil))
                        .Sum(x => x.VrOrcado).To<decimal>();

                    registroRelatorio.BoldReceita = c.NrContaContabil.Length <= QTD_CARACTERES_PARA_NEGRITO;
                    registroRelatorio.NrContaContabilReceita = c.NrContaContabilFmt;
                    registroRelatorio.NoContaContabilReceita = c.NoContaContabil;
                    if (c.NrContaContabil.Length == 5)
                    {
                        registroRelatorio.VrOrcadoParcialReceita = null;
                        registroRelatorio.VrOrcadoTotalReceita = vrOrcadoReceita;
                    }
                    else
                    {
                        registroRelatorio.VrOrcadoParcialReceita = vrOrcadoReceita;
                        registroRelatorio.VrOrcadoTotalReceita = null;
                    }
                }

                if (i < contasDespesa.Count)
                {
                    var c = new ContaContabil() { NoContaContabil = contasDespesa[i].NoContaContabil, NrContaContabil = contasDespesa[i].NrContaContabil, FgAnalitico = contasDespesa[i].FgAnalitico };

                    decimal vrOrcadoDespesa = propostasOrcamentariasItens
                            .Where(x => x.ContaContabil.NrContaContabil.StartsWith(c.NrContaContabil))
                            .Sum(x => x.VrOrcadoAntesFinalizar).To<decimal>();

                    registroRelatorio.BoldDespesa = c.NrContaContabil.Length <= QTD_CARACTERES_PARA_NEGRITO;
                    registroRelatorio.NrContaContabilDespesa = c.NrContaContabilFmt;
                    registroRelatorio.NoContaContabilDespesa = c.NoContaContabil;
                    if (c.NrContaContabil.Length == 5)
                    {
                        registroRelatorio.VrOrcadoParcialDespesa = null;
                        registroRelatorio.VrOrcadoTotalDespesa = vrOrcadoDespesa;
                    }
                    else
                    {
                        registroRelatorio.VrOrcadoParcialDespesa = vrOrcadoDespesa;
                        registroRelatorio.VrOrcadoTotalDespesa = null;
                    }
                }

                registrosDoRelatorio.Add(registroRelatorio);
            }
        }

        return registrosDoRelatorio;
    }

    private DataTable MontarGridDemonstrativoReceitaEDespesaSinteticoReceita() //Reformulção Sintético da Receita
    {
        CarregaValoresIniciais(true, false);

        var metodologiasOriginais = db.MetodologiasReceita.BuscaMetodologias(db.NrAnoBase);

        DataTable dt = new DataTable("dsCrea");
        List<string> colunas = new List<string>()
        { "NrAno", "Bold", "NrContaContabil", "NoContaContabil", "VrOrcadoParcial", "VrOrcadoTotal" };

        colunas.ForEach(x => dt.Columns.Add(x));

        DataRow dr;

        foreach (var conta in _contas)
        {
            var c = new ContaContabil() { NoContaContabil = conta.NoContaContabil, NrContaContabil = conta.NrContaContabil, FgAnalitico = conta.FgAnalitico };

            decimal vrOrcado = metodologiasOriginais
                .Where(x => x.ContaContabil.NrContaContabil.StartsWith(c.NrContaContabil))
                .Sum(x => x.VrOrcado).To<decimal>();

            if (_fgIncluiValoresZerados || vrOrcado != 0)
            {
                dr = dt.NewRow();
                dr["NrAno"] = _parametroAno.NrAno;
                dr["Bold"] = c.NrContaContabil.Length <= QTD_CARACTERES_PARA_NEGRITO;
                dr["NrContaContabil"] = c.NrContaContabilFmt;
                dr["NoContaContabil"] = c.NoContaContabil;
                if (c.NrContaContabil.Length == 5)
                {
                    dr["VrOrcadoParcial"] = "";
                    dr["VrOrcadoTotal"] = vrOrcado.ToString("n2");
                }
                else
                {
                    dr["VrOrcadoParcial"] = vrOrcado.ToString("n2");
                    dr["VrOrcadoTotal"] = "";
                }
                dt.Rows.Add(dr);
            }
        }

        return dt;
    }

    private DataTable MontarGridDemonstrativoReceitaEDespesaSinteticoDespesa() //Reformulção Sintético da Receita
    {
        CarregaValoresIniciais(false, false);

        var propostasOrcamentariasItens = db.PropostasOrcamentarias.ListaPropostaOrcamentariaItens();

        DataTable dt = new DataTable("dsCrea");
        List<string> colunas = new List<string>()
        { "NrAno", "Bold", "NrContaContabil", "NoContaContabil", "VrOrcadoParcial", "VrOrcadoTotal" };

        colunas.ForEach(x => dt.Columns.Add(x));

        DataRow dr;

        foreach (var conta in _contas)
        {
            var c = new ContaContabil() { NoContaContabil = conta.NoContaContabil, NrContaContabil = conta.NrContaContabil, FgAnalitico = conta.FgAnalitico };

            decimal vrOrcado = propostasOrcamentariasItens
                .Where(x => x.ContaContabil.NrContaContabil.StartsWith(c.NrContaContabil))
                .Sum(x => x.VrOrcadoAntesFinalizar).To<decimal>();

            if (_fgIncluiValoresZerados || vrOrcado != 0)
            {
                dr = dt.NewRow();
                dr["NrAno"] = _parametroAno.NrAno;
                dr["Bold"] = c.NrContaContabil.Length <= QTD_CARACTERES_PARA_NEGRITO;
                dr["NrContaContabil"] = c.NrContaContabilFmt;
                dr["NoContaContabil"] = c.NoContaContabil;
                if (c.NrContaContabil.Length == 5)
                {
                    dr["VrOrcadoParcial"] = "";
                    dr["VrOrcadoTotal"] = vrOrcado.ToString("n2");
                }
                else
                {
                    dr["VrOrcadoParcial"] = vrOrcado.ToString("n2");
                    dr["VrOrcadoTotal"] = "";
                }
                dt.Rows.Add(dr);
            }
        }

        return dt;
    }

    private List<RelatorioDemonstrativoReceitaDespesaSinteticoResumoDTO> MontarGridDemonstrativoReceitaEDespesaSinteticoResumo() //Reformulção Sintético da Receita
    {
        var metodologiasOriginais = db.MetodologiasReceita.BuscaMetodologias(db.NrAnoBase);
        var propostasOrcamentariasItens = db.PropostasOrcamentarias.ListaPropostaOrcamentariaItens();

        List<RelatorioDemonstrativoReceitaDespesaSinteticoResumoDTO> RegistrosDoRelatorio = new List<RelatorioDemonstrativoReceitaDespesaSinteticoResumoDTO>();

        decimal vrOrcadoCorrentesReceita = metodologiasOriginais
            .Where(x => x.ContaContabil.NrContaContabil.StartsWith("52111"))
            .Sum(x => x.VrOrcado).To<decimal>();

        decimal vrOrcadoCorrentesDespesa = propostasOrcamentariasItens
                .Where(x => x.ContaContabil.NrContaContabil.StartsWith("52211"))
                .Sum(x => x.VrOrcadoAntesFinalizar).To<decimal>();

        decimal vrOrcadoCapitalReceita = metodologiasOriginais
            .Where(x => x.ContaContabil.NrContaContabil.StartsWith("52112"))
            .Sum(x => x.VrOrcado).To<decimal>();

        decimal vrOrcadoCapitalDespesa = propostasOrcamentariasItens
                .Where(x => x.ContaContabil.NrContaContabil.StartsWith("52212"))
                .Sum(x => x.VrOrcadoAntesFinalizar).To<decimal>();

        RelatorioDemonstrativoReceitaDespesaSinteticoResumoDTO registroDoRelatorio = new RelatorioDemonstrativoReceitaDespesaSinteticoResumoDTO();
        registroDoRelatorio.NoItem = "Receitas/Despesas Correntes";
        registroDoRelatorio.VrReceita = vrOrcadoCorrentesReceita;
        registroDoRelatorio.VrDespesa = vrOrcadoCorrentesDespesa;
        RegistrosDoRelatorio.Add(registroDoRelatorio);

        registroDoRelatorio = new RelatorioDemonstrativoReceitaDespesaSinteticoResumoDTO();
        registroDoRelatorio.NoItem = "Receitas/Despesas Capital";
        registroDoRelatorio.VrReceita = vrOrcadoCapitalReceita;
        registroDoRelatorio.VrDespesa = vrOrcadoCapitalDespesa;
        RegistrosDoRelatorio.Add(registroDoRelatorio);

        registroDoRelatorio = new RelatorioDemonstrativoReceitaDespesaSinteticoResumoDTO();
        registroDoRelatorio.NoItem = "Total";
        registroDoRelatorio.VrReceita = (vrOrcadoCorrentesReceita + vrOrcadoCapitalReceita);
        registroDoRelatorio.VrDespesa = (vrOrcadoCorrentesDespesa + vrOrcadoCapitalDespesa);
        RegistrosDoRelatorio.Add(registroDoRelatorio);

        return RegistrosDoRelatorio;
    }

</script>
<asp:Content ContentPlaceHolderID="content" runat="server">
    <box:NToolBar runat="server">
        <uc:ReportButton runat="server" ID="btnGerarRelatorio" Text="Gerar Relatório" OnClick="btnGerarRelatorio_Click" />
        <box:NCheckBox runat="server" ID="chkFgIncluiValoresZerados" Text="Inclui valores zerados" Checked="false" />
    </box:NToolBar>

    <table class="form">
        <tr>
            <td>Relatório</td>
            <td>
                <box:NDropDownList runat="server" ID="cmbRelatorios" Required="true" OnSelectedIndexChanged="cmbRelatorios_SelectedIndexChanged" AutoPostBack="true"></box:NDropDownList>
            </td>
        </tr>
        <tr runat="server" id="trFiltroNrContaContabil" visible="false">
            <td>Conta contábil</td>
            <td>
                <uc:ContaContabil runat="server" ID="ucContaContabil" NrContaFiltro="5211" Width="150px" />
            </td>
        </tr>
    </table>

</asp:Content>
