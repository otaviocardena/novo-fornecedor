﻿<%@ Page Title="Metodologia de Receita" Resource="elo.configuracao" Language="C#" %>

<script runat="server">

    private CreaspContext db = new CreaspContext();
    private int? IdReformulacao => Request.QueryString["IdReformulacao"].To<int?>();
    //private int? IdContaContabil => Request.QueryString["IdContaContabil"].To<int?>();
    private int? IdMetologiaReceitaAno => Request.QueryString["IdMetologiaReceitaAno"].To<int?>();
    private Creasp.Elo.MetodologiaReceitaAno Metodologia { get { return ViewState["Metodologia"] as Creasp.Elo.MetodologiaReceitaAno; } set { ViewState["Metodologia"] = value; } }

    #region Init
    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        if (!IdMetologiaReceitaAno.HasValue)
        {
            if (IdReformulacao.HasValue)
            {
                Redirect("MetodologiaReceita.aspx?IdReformulacao=" + IdReformulacao.Value);
            }
            else
            {
                Redirect("MetodologiaReceita.aspx");
            }
        }

        grdList.RegisterDataSource(() => Metodologia?.MetodologiasReceitaMes);
        RegisterRestorePageState(() => grdList.DataBind());
    }

    protected override void OnPrepareForm()
    {
        if (Metodologia == null)
        {
            Metodologia = db.MetodologiasReceita.BuscaMetodologiaReceita(IdMetologiaReceitaAno.Value);

            if (Metodologia == null) Redirect("MetodologiaReceita.aspx");

            if (!db.MetodologiasReceita.VerificaSeUsuarioPodeAlterarMetodologia(Metodologia.IdMetodologiaReceitaAno))
            {
                txtVrOrcadoBrutoAnoAnterior.Text = Metodologia.VrAprovadoAnoAnterior.Value.ToString("n2");

                btnSalvar.Enabled = false;
                txtPercentualAplicado.Enabled = false;
                txtVrOrcadoBruto.Enabled = false;
                txtPrCotaParteConfea.Enabled = false;
                txtPrCotaParteMutua.Enabled = false;
                txtPrSaoPaulo.Enabled = false;
                txtPrBrasilia.Enabled = false;
                txtObservacoes.Enabled = false;

                ShowInfoBox("Você só pode visualizar esta metodologia");
            }

            if (IdReformulacao.HasValue) //Se for uma reformulação, busca a original para colocar no tooltip
            {
                var metodologiaOriginal = db.MetodologiasReceita.BuscaMetodologiaReceita(IdMetologiaReceitaAno.Value);

                //txtVrOrcadoBrutoAnoAnterior.ToolTip = metodologiaOriginal.VrAprovadoAnoAnterior.HasValue ? metodologiaOriginal.VrAprovadoAnoAnterior.Value.ToString("n2") : "0,00";
                txtPercentualAplicado.ToolTip = metodologiaOriginal.PrAplicado.HasValue ? metodologiaOriginal.PrAplicado.Value.ToString("n2") : "0,00";
                txtPrCotaParteConfea.ToolTip = metodologiaOriginal.PrCotaParteConfea.HasValue ? metodologiaOriginal.PrCotaParteConfea.Value.ToString("n2") : "0,00";
                txtPrCotaParteMutua.ToolTip = metodologiaOriginal.PrCotaParteMutua.HasValue ? metodologiaOriginal.PrCotaParteMutua.Value.ToString("n2") : "0,00";
                txtPrSaoPaulo.ToolTip = metodologiaOriginal.PrSaoPaulo.HasValue ? metodologiaOriginal.PrSaoPaulo.Value.ToString("n2") : "0,00";
                txtPrBrasilia.ToolTip = metodologiaOriginal.PrBrasilia.HasValue ? metodologiaOriginal.PrBrasilia.Value.ToString("n2") : "0,00";
                txtVrOrcadoBruto.ToolTip = metodologiaOriginal.VrOrcado.HasValue ? metodologiaOriginal.VrOrcado.Value.ToString("n2") : "0,00";
            }
        }

        ExibeTextosCorretos();
        AtualizaDadosBasicos();
        AtualizaDadosComplementares();
        AtualizaDadosMensais();
        AtualizaDadosCotaParte();

        this.Audit(Metodologia, Metodologia.IdMetodologiaReceitaAno);
        if (IdReformulacao.HasValue)
        {
            this.BackTo("MetodologiaReceita.aspx?IdReformulacao=" + IdReformulacao.Value);
        }
        else
        {
            this.BackTo("MetodologiaReceita.aspx");
        }
    }
    #endregion

    #region Events
    protected void txtPercentualAplicado_TextChanged(object sender, EventArgs e)
    {
        decimal prAplicado = 0;
        decimal vrOrcadoBrutoAnoAnterior = 0;

        Decimal.TryParse(txtPercentualAplicado.Text, out prAplicado);
        Decimal.TryParse(txtVrOrcadoBrutoAnoAnterior.Text, out vrOrcadoBrutoAnoAnterior);

        decimal vrOrcadoBruto = vrOrcadoBrutoAnoAnterior * (1 + prAplicado / 100);

        Metodologia.PrAplicado = prAplicado;
        Metodologia.VrOrcado = vrOrcadoBruto;

        AtualizaDadosBasicos();
        AtualizaDadosMensais();
        AtualizaDadosCotaParte();
    }

    protected void txtVrOrcadoBruto_TextChanged(object sender, EventArgs e)
    {
        decimal vrOrcado = 0;
        decimal vrOrcadoBrutoAnoAnterior = 0;

        Decimal.TryParse(txtVrOrcadoBruto.Text, out vrOrcado);
        Decimal.TryParse(txtVrOrcadoBrutoAnoAnterior.Text, out vrOrcadoBrutoAnoAnterior);

        decimal prAplicado = vrOrcadoBrutoAnoAnterior > 0 ? (vrOrcado / vrOrcadoBrutoAnoAnterior - 1) * 100 : 0;

        Metodologia.PrAplicado = prAplicado;
        Metodologia.VrOrcado = vrOrcado;

        AtualizaDadosBasicos();
        AtualizaDadosMensais();
        AtualizaDadosCotaParte();
    }

    protected void txtPrCotaParteConfea_TextChanged(object sender, EventArgs e)
    {
        Metodologia.PrCotaParteConfea = txtPrCotaParteConfea.Text.To<decimal?>();

        AtualizaDadosComplementares();
        AtualizaDadosMensais();
        AtualizaDadosCotaParte();
    }

    protected void txtPrCotaParteMutua_TextChanged(object sender, EventArgs e)
    {
        Metodologia.PrCotaParteMutua = txtPrCotaParteMutua.Text.To<decimal?>();

        AtualizaDadosComplementares();
        AtualizaDadosMensais();
        AtualizaDadosCotaParte();
    }

    protected void txtPrSaoPaulo_TextChanged(object sender, EventArgs e)
    {
        Metodologia.PrSaoPaulo = txtPrSaoPaulo.Text.To<decimal?>();

        AtualizaDadosComplementares();
        AtualizaDadosMensais();
        AtualizaDadosCotaParte();
    }

    protected void txtPrBrasilia_TextChanged(object sender, EventArgs e)
    {
        Metodologia.PrBrasilia = txtPrBrasilia.Text.To<decimal?>();

        AtualizaDadosComplementares();
        AtualizaDadosMensais();
        AtualizaDadosCotaParte();
    }

    //protected void txtVrOrcadoBruto_TextChanged(object sender, EventArgs e)
    //{
    //    decimal prAplicado = 0;
    //    decimal vrOrcadoBrutoAnoAnterior = 0;
    //    decimal vrOrcadoBruto = 0;

    //    Decimal.TryParse(txtVrOrcadoBruto.Text, out vrOrcadoBruto);
    //    Decimal.TryParse(txtVrOrcadoBrutoAnoAnterior.Text, out vrOrcadoBrutoAnoAnterior);

    //    prAplicado = vrOrcadoBruto * 100 / vrOrcadoBrutoAnoAnterior - 100;
    //    txtPercentualAplicado.Text = prAplicado.ToString("n2");
    //    Metodologia.PrAplicado = prAplicado;

    //    AtualizaDadosMensais();
    //    AtualizaDadosCotaParte();
    //    grdList.DataBind();
    //}

    protected void txtPrAplicado_TextChanged(object sender, EventArgs e)
    {
        decimal novoValor = 0;
        Decimal.TryParse(((TextBox)sender).Text, out novoValor);

        TextBox txt = sender as TextBox;
        GridViewRow row = txt.NamingContainer as GridViewRow;
        int idMetodologiaReceitaMes = Convert.ToInt32(grdList.DataKeys[row.RowIndex].Value);

        if (Metodologia?.MetodologiasReceitaMes != null)
        {
            var metodologiaMes = Metodologia.MetodologiasReceitaMes.Single(x => x.IdMetodologiaReceitaMes == idMetodologiaReceitaMes);

            metodologiaMes.PrAplicado = novoValor;
            metodologiaMes.CalculaVrReceitaBruta(Metodologia.VrOrcado);
            metodologiaMes.CalculaVrReceitaLiquida(Metodologia.VrOrcado, Metodologia.PrCotaParteConfea, Metodologia.PrCotaParteMutua);
            //metodologiaMes.VrReceitaBruta = Metodologia.VrOrcado * (novoValor / 100);
            //metodologiaMes.VrReceitaLiquida = Metodologia.VrOrcado * (novoValor / 100);
        }

        grdList.DataBind();
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        Metodologia.NoObservacoes = txtObservacoes.Text;

        db.MetodologiasReceita.ValidaESalva(Metodologia);
        ShowInfoBox("Alterações salvas com sucesso.");
    }

    protected void btnRelatoriosDemMetodologiaReceita_Click(object sender, ReportFormat e)
    {
        if (!(Metodologia.IdContaContabil > 0)) throw new NBoxException("Selecione uma conta contábil");

        var rpt = new Report(Server.MapPath("~/Elo/Reports/DemonstrativoMetodologiaReceita.rdl"));
        rpt.AddDataSet("dsMain", db.RelatoriosElo.MontarGridMetodologiaReceitaAno(true, Metodologia.IdContaContabil));
        //rpt.AddDataSet("dsMeses", db.RelatoriosElo.MontarGridMetodologiaReceitaMeses(Metodologia.IdContaContabil));
        ShowReport(rpt, e, "Relatorio-DemonstrativoMetodologiaReceita-" + DateTime.Now.ToString("yyyyMMddHHmmss"));
    }
    #endregion

    #region Privados
    private void AtualizaTotalizadores()
    {
        //if (ItensProposta != null)
        //{
        //    //toVrOrcadoAnoAnterior = ItensProposta.Sum(x => x.VrOrcadoAnoAnterior);
        //    toVrAprovadoAnoAnterior = ItensProposta.Sum(x => x.VrAprovadoAnoAnterior);
        //    toVrExecitadoAnoAnterior = ItensProposta.Sum(x => x.VrExecutadoAnoAnterior);
        //    toVrOrcado = ItensProposta.Sum(x => x.VrOrcado.HasValue ? x.VrOrcado.Value : 0);

    }

    private void ExibeTextosCorretos()
    {
        if (IdReformulacao.HasValue)
        {
            lblValorAnterior.Text = "Valor aprovado";
            lblValorAtual.Text = "Valor reformulado";
        }
        else
        {
            lblValorAnterior.Text = "Valor aprovado (ano anterior)";
            lblValorAtual.Text = "Valor orçado";
        }
    }

    private void AtualizaDadosBasicos()
    {
        txtContaContabil.Text = Metodologia.ContaContabil.NoContaContabilFmt;
        txtVrOrcadoBrutoAnoAnterior.Text = Metodologia.VrAprovadoAnoAnterior.HasValue ? Metodologia.VrAprovadoAnoAnterior.Value.ToString("n2") : "0,00";
        txtPercentualAplicado.Text = Metodologia.PrAplicado.HasValue ? Metodologia.PrAplicado.Value.ToString("n2") : "0,00";
        txtVrOrcadoBruto.Text = Metodologia.VrOrcado.HasValue ? Metodologia.VrOrcado.Value.ToString("n2") : "0,00";

        //decimal vrOrcadoBrutoAnoAnterior = 0;
        //Decimal.TryParse(txtVrOrcadoBrutoAnoAnterior.Text, out vrOrcadoBrutoAnoAnterior);

        //txtPercentualAplicado.Text = Metodologia.PrAplicado.Value.ToString("n2");
        //if (Metodologia.VrAprovadoAnoAnterior > 0)
        //{
        //    //txtVrOrcadoBruto.Text = (vrOrcadoBrutoAnoAnterior * (1 + Metodologia.PrAplicado / 100)).Value.ToString("n2");
        //}
    }

    private void AtualizaDadosComplementares()
    {
        txtPrCotaParteConfea.Text = Metodologia.PrCotaParteConfea.HasValue ? Metodologia.PrCotaParteConfea.Value.ToString("n2") : "0,00";
        txtPrCotaParteMutua.Text = Metodologia.PrCotaParteMutua.HasValue ? Metodologia.PrCotaParteMutua.Value.ToString("n2") : "0,00";
        txtPrSaoPaulo.Text = Metodologia.PrSaoPaulo.HasValue ? Metodologia.PrSaoPaulo.Value.ToString("n2") : "0,00";
        txtPrBrasilia.Text = Metodologia.PrBrasilia.HasValue ? Metodologia.PrBrasilia.Value.ToString("n2") : "0,00";
        txtObservacoes.Text = Metodologia.NoObservacoes;
    }

    private void AtualizaDadosMensais()
    {
        var prCotaParteConfea = txtPrCotaParteConfea.Text.To<decimal>() / 100;
        var prCotaParteMutua = txtPrCotaParteMutua.Text.To<decimal>() / 100;

        var vrCotaParteConfea = Metodologia.VrOrcado * prCotaParteConfea;
        var vrCotaParteMutua = Metodologia.VrOrcado * prCotaParteMutua;

        foreach (var mes in Metodologia?.MetodologiasReceitaMes)
        {
            mes.VrReceitaBruta = Metodologia.VrOrcado * (mes.PrAplicado / 100);
            mes.VrReceitaLiquida = mes.VrReceitaBruta - (mes.VrReceitaBruta * (prCotaParteConfea + prCotaParteMutua));
        }

        grdList.DataBind();
    }

    private void AtualizaDadosCotaParte()
    {
        lblPrCotaParteConfea.Text = Metodologia.PrCotaParteConfea.HasValue ? Metodologia.PrCotaParteConfea.Value.ToString("n2") : "0,00";
        txtVrCotaParteConfea.Text = Metodologia.VrCotaParteConfea.HasValue ? Metodologia.VrCotaParteConfea.Value.ToString("n2") : "0,00";

        lblPrCotaParteMutua.Text = Metodologia.PrCotaParteMutua.HasValue ? Metodologia.PrCotaParteMutua.Value.ToString("n2") : "0,00";
        txtVrCotaParteMutua.Text = Metodologia.VrCotaParteMutua.HasValue ? Metodologia.VrCotaParteMutua.Value.ToString("n2") : "0,00";

        lblPrCotaParteMutuaSaoPaulo.Text = Metodologia.PrSaoPaulo.HasValue ? Metodologia.PrSaoPaulo.Value.ToString("n2") : "0,00";
        txtVrCotaParteMutuaSaoPaulo.Text = Metodologia.VrCotaSaoPaulo.HasValue ? Metodologia.VrCotaSaoPaulo.Value.ToString("n2") : "0,00";

        lblPrCotaParteMutuaBrasilia.Text = Metodologia.PrBrasilia.HasValue ? Metodologia.PrBrasilia.Value.ToString("n2") : "0,00";
        txtVrCotaParteMutuaBrasilia.Text = Metodologia.VrCotaBrasilia.HasValue ? Metodologia.VrCotaBrasilia.Value.ToString("n2") : "0,00";
    }

    #endregion


    protected void grdList_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var txtPrAplicadoMes = (NTextBox)e.Row.FindControl("txtPrAplicado");
            txtPrAplicadoMes.Enabled = db.MetodologiasReceita.VerificaSeUsuarioPodeAlterarMetodologia(Metodologia.IdMetodologiaReceitaAno);
        }
    }
</script>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <box:NToolBar runat="server">
        <uc:ReportButton runat="server" ID="btnRelatoriosDemMetodologiaReceita" Text="Gerar Relatório METODOLOGIA RECEITA" OnClick="btnRelatoriosDemMetodologiaReceita_Click" />
        <box:NButton runat="server" ID="btnSalvar" Theme="Primary" Icon="ok" Text="Salvar" CssClass="right" OnClick="btnSalvar_Click" />
    </box:NToolBar>

    <table class="form">
        <tr>
            <td>Conta contábil</td>
            <td colspan="3">
                <box:NTextBox runat="server" ID="txtContaContabil" Disabled="True" Width="480" />
            </td>
            <td>
                <box:NLabel runat="server" ID="lblValorAnterior" /></td>
            <td>
                <box:NTextBox runat="server" ID="txtVrOrcadoBrutoAnoAnterior" MaskFormat="Decimal" Enabled="false" />
            </td>
        </tr>
        <tr>
            <td>Percentual aplicado</td>
            <td colspan="3">
                <box:NTextBox runat="server" ID="txtPercentualAplicado" MaskFormat="Decimal" AutoPostBack="true" OnTextChanged="txtPercentualAplicado_TextChanged" Enabled="true" />
            </td>
            <td>
                <box:NLabel runat="server" ID="lblValorAtual" /></td>
            <td>
                <box:NTextBox runat="server" ID="txtVrOrcadoBruto" MaskFormat="Decimal" AutoPostBack="true" OnTextChanged="txtVrOrcadoBruto_TextChanged" Enabled="true" />
            </td>
        </tr>
        <tr>
            <td>% Cota-Parte Confea</td>
            <td>
                <box:NTextBox runat="server" ID="txtPrCotaParteConfea" MaskFormat="Decimal" AutoPostBack="true" OnTextChanged="txtPrCotaParteConfea_TextChanged" />
            </td>
        </tr>
        <tr>
            <td>% Cota-Parte Mutua</td>
            <td>
                <box:NTextBox runat="server" ID="txtPrCotaParteMutua" MaskFormat="Decimal" AutoPostBack="true" OnTextChanged="txtPrCotaParteMutua_TextChanged" />
            </td>
            <td>% São Paulo</td>
            <td>
                <box:NTextBox runat="server" ID="txtPrSaoPaulo" MaskFormat="Decimal" AutoPostBack="true" OnTextChanged="txtPrSaoPaulo_TextChanged" />
            </td>
            <td>% Brasília</td>
            <td>
                <box:NTextBox runat="server" ID="txtPrBrasilia" MaskFormat="Decimal" AutoPostBack="true" OnTextChanged="txtPrBrasilia_TextChanged" />
            </td>
        </tr>
    </table>

    <table class="form">
        <tr>
            <td>Observações</td>
            <td>
                <box:NTextBox runat="server" ID="txtObservacoes" TextMode="MultiLine" MaxLength="1000" Width="100%" /></td>
        </tr>
    </table>

    <box:NGridView runat="server" ID="grdList" ShowFooter="true" DataKeyNames="IdMetodologiaReceitaMes" ItemType="Creasp.Elo.MetodologiaReceitaMes" OnRowDataBound="grdList_RowDataBound">
        <Columns>
            <asp:BoundField DataField="NoMes" HeaderText="Mês" />
            <asp:TemplateField HeaderText="Percentual">
                <ItemTemplate>
                    <box:NTextBox runat="server" ID="txtPrAplicado" MaskType="Decimal" Text='<%# Item.PrAplicado.HasValue ? Item.PrAplicado.Value.ToString("n2") : "0,00" %>' AutoPostBack="true" MaxLength="20" CssClass="t-center" OnTextChanged="txtPrAplicado_TextChanged" />
                </ItemTemplate>
                <FooterTemplate>
                    <%# Metodologia.MetodologiasReceitaMes.Sum(x => x.PrAplicado)?.ToString("n2") %>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Receita Bruta">
                <ItemTemplate>
                    <box:NTextBox runat="server" ID="txtVrReceitaBruta" MaskType="Decimal" Text='<%# Item.VrReceitaBruta.HasValue ? Item.VrReceitaBruta.Value.ToString("n2") : "0,00" %>' MaxLength="20" CssClass="t-center" Disabled="True" />
                </ItemTemplate>
                <FooterTemplate>
                    <%# Metodologia.MetodologiasReceitaMes.Sum(x => x.VrReceitaBruta)?.ToString("n2") %>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Receita líquida">
                <ItemTemplate>
                    <box:NTextBox runat="server" ID="txtVrReceitaLiquida" MaskType="Decimal" Text='<%# Item.VrReceitaLiquida.HasValue ? Item.VrReceitaLiquida.Value.ToString("n2") : "0,00" %>' MaxLength="20" CssClass="t-center" Disabled="True" />
                </ItemTemplate>
                <FooterTemplate>
                    <%# Metodologia.MetodologiasReceitaMes.Sum(x => x.VrReceitaLiquida)?.ToString("n2") %>
                </FooterTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>Esta metodologia está vazia.</EmptyDataTemplate>
    </box:NGridView>



    <table class="form">
        <tr>
            <td>Cota-Parte Confea (<box:NLabel runat="server" ID="lblPrCotaParteConfea" />
                %)</td>
            <td>
                <box:NTextBox runat="server" ID="txtVrCotaParteConfea" Disabled="True" /></td>
        </tr>
        <tr>
            <td>Cota-Parte Mútua (<box:NLabel runat="server" ID="lblPrCotaParteMutua" />
                %)</td>
            <td>
                <box:NTextBox runat="server" ID="txtVrCotaParteMutua" Disabled="True" /></td>
            <%--</tr>
        <tr>
            <td>Obs.:</td>
        </tr>
        <tr>--%>
            <td><%--Cota-Parte Mútua --%>São Paulo (<box:NLabel runat="server" ID="lblPrCotaParteMutuaSaoPaulo" />
                %)</td>
            <td>
                <box:NTextBox runat="server" ID="txtVrCotaParteMutuaSaoPaulo" Disabled="True" /></td>
            <%-- </tr>
        <tr>--%>
            <td><%--Cota-Parte Mútua --%>Brasília (<box:NLabel runat="server" ID="lblPrCotaParteMutuaBrasilia" />
                %)</td>
            <td>
                <box:NTextBox runat="server" ID="txtVrCotaParteMutuaBrasilia" Disabled="True" /></td>
        </tr>
    </table>

</asp:Content>
