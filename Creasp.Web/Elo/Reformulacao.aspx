﻿<%@ Page Title="Reformulações" Resource="elo" Language="C#" %>

<script runat="server">
    private CreaspContext db = new CreaspContext();

    public int? IdReformulacao { get { return ViewState["IdReformulacao"].To<int?>(); } set { ViewState["IdReformulacao"] = value; } }

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        grdList.RegisterPagedDataSource((p) => db.Reformulacoes.ListaReformulacoes(p));
        var IdReformulacaoMaisRecente = db.Reformulacoes.BuscaIdReformulacaoMaisRecente();        
        grdListCentrosDeCustos.RegisterPagedDataSource((p) => db.Reformulacoes.ListarCentrosDeCustoParaNovaReformulacao(p, IdReformulacaoMaisRecente));
        //RegisterResource("elo.contacontabilmapeamento")
        //    .Edit.GridColumns(grdList, "alt", "del");
    }

    protected override void OnPrepareForm()
    {
        //cmbTipo.Bind()
        grdList.DataBind();
        grdListCentrosDeCustos.DataBind();
        RegisterResource("elo.configuracao").Disabled(btnIncluiReformulacao);
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtNrAno.Text)) throw new NBoxException("Nome da reformulação é obrigatório");
        if (string.IsNullOrEmpty(txtNoReformulacao.Text)) throw new NBoxException("Ano da reformulação é obrigatório");

        var idsSelecionados = grdListCentrosDeCustos.SelectedDataKeys.Select(x => int.Parse(x.Value.ToString())).ToArray();

        db.Reformulacoes.ValidaEIncluiReformulacao(txtNrAno.Text.To<int>(), txtNoReformulacao.Text, idsSelecionados);

        pnlPopup.Close();

        ShowInfoBox("Reformulação incluída com sucesso");

        txtNoReformulacao.Text = "";

        grdList.DataBind();
    }

    protected void grdList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "despesa")
        {
            IdReformulacao = e.DataKey<int>(sender);
            Redirect("ReformulacaoOrcamentaria.aspx?IdReformulacao=" + IdReformulacao.Value);
        }
        else if (e.CommandName == "receita")
        {
            if (!db.CentrosCustosPerfis.ValidaUsuarioLogadoPossuiPerfil(TpPerfilCentroCusto.Contabilidade))
            {
                throw new NBoxException("Apenas o perfil de contabilidade cadastrado no sistema pode fazer isto.");
            }

            IdReformulacao = e.DataKey<int>(sender);
            Redirect("MetodologiaReceita.aspx?IdReformulacao=" + IdReformulacao.Value);
        }
        else if (e.CommandName == "alt")
        {
            if (!db.CentrosCustosPerfis.ValidaUsuarioLogadoPossuiPerfil(TpPerfilCentroCusto.Contabilidade))
            {
                throw new NBoxException("Apenas o perfil de contabilidade cadastrado no sistema pode fazer isto.");
            }

            IdReformulacao = e.DataKey<int>(sender);
            var reformulacao = db.Reformulacoes.BuscaReformulacao(IdReformulacao.Value);

            decimal prAplicadoReceita = reformulacao.PrAplicadoReceita ?? 0;
            decimal prCotaParteConfea = reformulacao.PrCotaParteConfea ?? 0;
            decimal prCotaParteMutua = reformulacao.PrCotaParteMutua ?? 0;

            txtNoReformulacaoAlterar.Text = reformulacao.NoReformulacao;
            txtPrAplicadoReceita.Text = prAplicadoReceita.ToString("n2");
            txtPrCotaParteConfea.Text = prCotaParteConfea.ToString("n2");
            txtPrCotaParteMutua.Text = prCotaParteMutua.ToString("n2");
            txtNoObservacoesPadraoReceita.Text = reformulacao.NoObservacaoReceitaPadrao.To<string>();

            pnlAlterarReceita.Open();
        }
        else if (e.CommandName == "env")
        {
            if (!db.CentrosCustosPerfis.ValidaUsuarioLogadoPossuiPerfil(TpPerfilCentroCusto.Contabilidade))
            {
                throw new NBoxException("Apenas o perfil de contabilidade cadastrado no sistema pode fazer isto.");
            }

            IdReformulacao = e.DataKey<int>(sender);

            var lstErros = db.Sync.ValidaEEnviaReformulacaoAtual(IdReformulacao.Value);
            if (lstErros?.Count > 0)
            {
                txtErros.Text = string.Join(" \r\n ", lstErros);
                pnlErros.Open();
            }
            else
            {
                ShowInfoBox("Reformulação integrada com o SISCONT com sucesso.");
            }
        }
    }

    protected void btnIncluiReformulacao_Click(object sender, EventArgs e)
    {
        if (!db.CentrosCustosPerfis.ValidaUsuarioLogadoPossuiPerfil(TpPerfilCentroCusto.Contabilidade))
        {
            throw new NBoxException("Apenas o perfil de contabilidade cadastrado no sistema pode fazer isto.");
        }

        txtNrAno.Text = (db.NrAnoBase).ToString();
        pnlPopup.Open();
    }

    protected void btnEntendi_Click(object sender, EventArgs e)
    {
        pnlErros.Close();
    }

    protected void btnAtualizar_Click(object sender, EventArgs e)
    {
        if (!db.CentrosCustosPerfis.ValidaUsuarioLogadoPossuiPerfil(TpPerfilCentroCusto.Contabilidade))
        {
            throw new NBoxException("Apenas o perfil de contabilidade cadastrado no sistema pode fazer isto.");
        }

        if (IdReformulacao.HasValue)
        {
            var reformulacao = db.Reformulacoes.BuscaReformulacao(IdReformulacao.Value);

            db.MetodologiasReceita.AtualizaMetodologias(reformulacao.NrAno,
                    txtPrAplicadoReceita.Text.To<decimal>(),
                    txtPrCotaParteConfea.Text.To<decimal>(),
                    txtPrCotaParteMutua.Text.To<decimal>(),
                    txtNoObservacoesPadraoReceita.Text,
                    reformulacao.IdReformulacao);

            pnlAlterarReceita.Close();
            ShowInfoBox("Reformulação atualizada com sucesso.");
        }
        else
        {
            ShowErrorBox("Não foi possível localizar a reformulação a ser atualizada.");
        }
    }
</script>
<asp:Content runat="server" ContentPlaceHolderID="content">

    <box:NToolBar runat="server">
        <box:NButton runat="server" ID="btnIncluiReformulacao" Theme="Default" Icon="plus" Text="Incluir" OnClick="btnIncluiReformulacao_Click" />
    </box:NToolBar>

    <box:NGridView runat="server" ID="grdList" DataKeyNames="IdReformulacao" OnRowCommand="grdList_RowCommand" ItemType="Creasp.Elo.Reformulacao">
        <Columns>
            <asp:BoundField DataField="NrAno" HeaderText="Ano" SortExpression="NrAno" />
            <asp:BoundField DataField="NoReformulacao" HeaderText="Nome" SortExpression="NoReformulacao" />
            <%--<asp:BoundField DataField="ContaContabil6.NrAno" HeaderText="Ano" SortExpression="ContaContabil6.NrAno" />
            <asp:BoundField DataField="ContaContabil5.NrContaContabilFmt" HeaderText="Número" SortExpression="ContaContabil5.NrContaContabilFmt" />
            <asp:BoundField DataField="ContaContabil5.NoContaContabil" HeaderText="Nome" SortExpression="ContaContabil5.NoContaContabil" />--%>
            <box:NButtonField Icon="search" CommandName="despesa" Theme="Danger" ToolTip="Ver reformulação despesa" />
            <box:NButtonField Icon="search" CommandName="receita" Theme="Success" ToolTip="Ver reformulação receita" />
            <box:NButtonField Icon="pencil" CommandName="alt" Theme="Info" ToolTip="Editar receita" />
            <box:NButtonField Icon="right-fat" CommandName="env" Theme="Info" ToolTip="Enviar reformulação ao SISCONT" OnClientClick="return confirm('Deseja enviar esta reformulação ao SISCONT? Isto não poderá ser desfeito.');" />
        </Columns>
        <EmptyDataTemplate>Não existem reformulações cadastradas.</EmptyDataTemplate>
    </box:NGridView>

    <box:NPopup runat="server" ID="pnlPopup" Title="Incluir reformulação" Width="800">
        <box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnOK" OnClick="btnOK_Click" Theme="Default" Text="Salvar" Icon="ok" />
        </box:NToolBar>
        <table class="form">
            <tr>
                <td>Ano</td>
                <td>
                    <box:NTextBox runat="server" ID="txtNrAno" Disabled="True" Width="100" />
                </td>
            </tr>
            <tr>
                <td>Nome</td>
                <td>
                    <box:NTextBox runat="server" ID="txtNoReformulacao" Required="true" Width="400" />
                </td>
            </tr>
        </table>
        <br />
        <span style="font-size:1.1em;">Selecionar os centros de custo que não devem ser formulados, os mesmos estaram com status aguardando contabilidade.</span>                
        <box:NGridView runat="server" ID="grdListCentrosDeCustos" DataKeyNames="IdCentroCusto">
            <Columns>                
                <box:NCheckBoxField HeaderText="Selecionar" />
                <asp:BoundField DataField="NrCentroCustoFmt" HeaderText="Centro de Custo" SortExpression="NrCentroCustoFmt">
                    <ItemStyle HorizontalAlign="Center" Width="200"/>
                </asp:BoundField>
                <asp:BoundField DataField="NoCentroCustoFmt" HeaderText="Nome" SortExpression="NoCentroCusto" />
                <asp:BoundField DataField="Valor" HeaderText="Valor" SortExpression="Valor" DataFormatString="{0:n2}">
                    <ItemStyle HorizontalAlign="Right" Width="150" />
                </asp:BoundField>
            </Columns>
            <EmptyDataTemplate>Não existem registros para essa consulta.</EmptyDataTemplate>
        </box:NGridView>
    </box:NPopup>

    <box:NPopup runat="server" ID="pnlErros" Title="Erros encontrados na integração" Width="600">
        <box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnEntendi" Theme="Primary" Text="Entendi" Icon="ok" OnClick="btnEntendi_Click" />
        </box:NToolBar>
        <table class="form">
            <tr>
                <td>Não foi possível efetuar a integração pelos motivos abaixo</td>
            </tr>
            <tr>
                <td>
                    <box:NTextBox runat="server" ID="txtErros" TextMode="MultiLine" Width="100%" Height="200px"></box:NTextBox></td>
            </tr>
            <tr>
                <td>Resolva as pendências e envie os dados novamente</td>
            </tr>
        </table>
    </box:NPopup>

    <box:NPopup runat="server" ID="pnlAlterarReceita" Title="Atualizar metodologias de reformulação" Width="650">
        <box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnAtualizar" Theme="Primary" Text="Atualizar" Icon="ok" OnClick="btnAtualizar_Click" />
        </box:NToolBar>

        <table class="form">
            <tr>
                <td>Reformulação</td>
                <td>
                    <box:NTextBox runat="server" ID="txtNoReformulacaoAlterar" Disabled="True" /></td>
            </tr>
            <tr>
                <td>% receita</td>
                <td>
                    <box:NTextBox runat="server" ID="txtPrAplicadoReceita" /></td>
            </tr>
            <tr>
                <td>% cota-parte confea</td>
                <td>
                    <box:NTextBox runat="server" ID="txtPrCotaParteConfea" /></td>
            </tr>
            <tr>
                <td>% cota-parte mútua</td>
                <td>
                    <box:NTextBox runat="server" ID="txtPrCotaParteMutua" /></td>
            </tr>
            <tr>
                <td>Observações de receita</td>
                <td>
                    <box:NTextBox runat="server" TextMode="MultiLine" MaxLength="1000" ID="txtNoObservacoesPadraoReceita" Width="100%" /></td>
            </tr>
        </table>
    </box:NPopup>
</asp:Content>
