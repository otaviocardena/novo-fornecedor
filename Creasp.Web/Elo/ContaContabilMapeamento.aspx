﻿<%@ Page Title="Mapear plano de contas" Resource="elo.configuracao" Language="C#" %>

<script runat="server">
    private CreaspContext db = new CreaspContext();

    #region LoadFunctions
    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        grdList.RegisterPagedDataSource((p) => db.ContasContabeisMapeamentos.ListaContaContabilMapeamento(p,
            txtPesquisaNrContaContabil.Text.To<string>(),
            txtPesquisaNoContaContabil.Text.To<string>(),
            drpCdPrefixoContaContabil.SelectedValue.To<string>()));

        //RegisterResource("elo.contacontabilmapeamento")
        //    .Edit.GridColumns(grdList, "alt", "del");
    }

    protected override void OnPrepareForm()
    {
        //cmbTipo.Bind()
        grdList.DataBind();
    }
    #endregion

    #region MiscFunctions
    protected bool existe_MapeamentoDe5Para62211(int contaContabil5, string NrContaContabil6)
    {
        if (NrContaContabil6.StartsWith("62211"))
        {
            var mapeamentos = db.ContasContabeisMapeamentos.BuscaConta5Mapeamentos(contaContabil5); //busca mapeamentos da conta 5
            if (mapeamentos != null)
            {
                foreach (ContaContabilMapeamento c in mapeamentos) //e para cada mapeamento
                {
                    ContaContabil c6 = db.ContasContabeis.BuscaContaContabil(c.IdContaContabil6);
                    if (c6.NrContaContabil.StartsWith("62211") && NrContaContabil6 != c6.NrContaContabil) //confere se prefixo não é 62211
                    {
                        ShowErrorBox("Já existe uma conta contábil de prefixo 6.2.2.1.1 associada a essa Conta 5");
                        return true;
                    }
                    else if (c6.NrContaContabil.StartsWith("62211"))
                    {
                        ShowErrorBox("Mapeamento já existente");
                        return true;
                    }
                }
            }
            return false;
        }
        else
            return false;
    }
    #endregion

    #region EventFunctions
    protected void btnPesquisar_Click(object sender, EventArgs e)
    {
        grdList.DataBind();
    }

    protected void btnInclui_Click(object sender, EventArgs e)
    {
        if (!ucContaContabil6.IdContaContabil.HasValue) throw new NBoxException(ucContaContabil6, "Conta contábil 6 obrigatório");
        if (!ucContaContabil5.IdContaContabil.HasValue) throw new NBoxException(ucContaContabil5, "Conta contábil 5 obrigatório");

        if (!db.ContasContabeisMapeamentos.ExisteMapeamento(ucContaContabil6.IdContaContabil.Value))
        {
            if (!existe_MapeamentoDe5Para62211(ucContaContabil5.IdContaContabil.Value, ucContaContabil6.ContaContabil.NrContaContabil))
            {

                db.ContasContabeisMapeamentos.ValidaEIncluiMapeamento(ucContaContabil6.IdContaContabil.Value, ucContaContabil5.IdContaContabil.Value);

                pnlPopup.Close();

                ShowInfoBox("Mapeamento incluído com sucesso");
                ucContaContabil5.ClearContaContabil();
                ucContaContabil6.ClearContaContabil();

                grdList.DataBind();
            }
        }
        else
        {
            ShowErrorBox("Mapeamento já existente");
        }
    }

    protected void btnSalva_Click(object sender, EventArgs e)
    {
        if (!ucContaContabil6.IdContaContabil.HasValue) throw new NBoxException(ucContaContabil6, "Conta contábil 6 obrigatório");
        if (!ucContaContabil5.IdContaContabil.HasValue) throw new NBoxException(ucContaContabil5, "Conta contábil 5 obrigatório");

        if (!existe_MapeamentoDe5Para62211(ucContaContabil5.IdContaContabil.Value, ucContaContabil6.ContaContabil.NrContaContabil)) {

            db.ContasContabeisMapeamentos.ValidaEAlteraMapeamento(ucContaContabil6.IdContaContabil.Value, ucContaContabil5.IdContaContabil.Value);

            pnlPopup.Close();

            ShowInfoBox("Mapeamento incluído com sucesso");
            ucContaContabil5.ClearContaContabil();
            ucContaContabil5.ClearContaContabil();

            grdList.DataBind();
        }
    }

    protected void grdList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "del")
        {
            int rowIndex = e.CommandArgument.To<int>();

            var idConta5 = grdList.DataKeys[rowIndex].Values["IdContaContabil5"].To<int>();
            var idConta6 = grdList.DataKeys[rowIndex].Values["IdContaContabil6"].To<int>();

            db.ContasContabeisMapeamentos.ExcluiMapeamento(idConta5, idConta6);

            grdList.DataBind();
        }
        else if (e.CommandName == "alt")
        {
            ucContaContabil5.ClearContaContabil();
            ucContaContabil6.ClearContaContabil();

            int rowIndex = e.CommandArgument.To<int>();

            //var idConta5 = grdList.DataKeys[rowIndex].Values["IdContaContabil5"].To<int>();
            var idConta6 = grdList.DataKeys[rowIndex].Values["IdContaContabil6"].To<int>();

            var conta6 = db.ContasContabeisMapeamentos.BuscaContaMapeamento(idConta6);

            ucContaContabil6.LoadContaContabil(conta6.IdContaContabil6);
            if (conta6.IdContaContabil5.HasValue) ucContaContabil5.LoadContaContabil(conta6.IdContaContabil5);

            btnInclui.Visible = false;
            btnSalva.Visible = true;

            pnlPopup.Open(btnSalva);

            ucContaContabil5.Focus();

            grdList.DataBind();
        }
    }

    protected void btnIncluiMapeamento_Click(object sender, EventArgs e)
    {
        btnInclui.Visible = true;
        btnSalva.Visible = false;

        pnlPopup.Open(btnInclui, ucContaContabil6);
    }

    protected void btnMapeamentoInteligente_Click(object sender, EventArgs e)
    {
        db.ContasContabeisMapeamentos.MapeiaContasContabeisPendentes();
        grdList.DataBind();
        ShowInfoBox("Mapeamento automatico realizado com sucesso.");
    }

    protected void btnReplicarMapeamento_Click(object sender, EventArgs e)
    {
        pnlReplicarMapeamento.Open(txtAnoOrigem);
    }

    protected void btnConfirmarReplicaMapeamento_Click(object sender, EventArgs e)
    {
        int nrAnoOrigem = txtAnoOrigem.Text.To<int>();
        int nrAnoDestino = txtAnoDestino.Text.To<int>();

        if (nrAnoOrigem == 0 || nrAnoDestino == 0) throw new NBoxException("Ano inválido");
        if (nrAnoOrigem == nrAnoDestino) throw new NBoxException("Ano de origem e destino devem ser diferentes.");

        db.ContasContabeisMapeamentos.ReplicaMapeamentoContasContabeis(nrAnoOrigem, nrAnoDestino);
        pnlReplicarMapeamento.Close();
        grdList.DataBind();
        ShowInfoBox("Mapeamento replicado com sucesso.");
    }
    #endregion
</script>
<asp:Content runat="server" ContentPlaceHolderID="content">

    <box:NToolBar runat="server">
        <box:NButton runat="server" ID="btnPesquisar" Theme="Default" Icon="search" Text="Pesquisar" OnClick="btnPesquisar_Click" />
        <box:NButton runat="server" ID="btnIncluiMapeamento" Theme="Default" Icon="plus" Text="Incluir" OnClick="btnIncluiMapeamento_Click" />
        <box:NButton runat="server" ID="btnReplicarMapeamento" Theme="Default" Icon="ok" Text="Replicar mapeamento" OnClick="btnReplicarMapeamento_Click"
            ToolTip="Copia os mapeamentos feitos de um ano para outro" />
        <box:NButton runat="server" ID="btnMapeamentoInteligente" Theme="Primary" Icon="ok" Text="Mapear automaticamente" 
            ToolTip="Mapeia todas as contas contábeis com mapeamento pendente (de todos os anos) com as regras: ter com mesmo nome, prefixo similares e apenas uma correspondência" 
            OnClick="btnMapeamentoInteligente_Click" OnClientClick="return confirm('Isto mapeará várias contas contábeis automaticamente, deseja confirmar?')" />
    </box:NToolBar>

    <table class="form">
        <tr>
            <td>Número de conta 6</td>
            <td>
                <box:NTextBox runat="server" ID="txtPesquisaNrContaContabil" TextCapitalize="Upper" MaxLength="80" Width="100%" />
            </td>
        </tr>
        <tr>
            <td>Nome de conta 6</td>
            <td>
                <box:NTextBox runat="server" ID="txtPesquisaNoContaContabil" TextCapitalize="Upper" MaxLength="80" Width="100%" />
            </td>
        </tr>
        <tr>
            <td>Tipo</td>
            <td>
                <box:NDropDownList runat="server" ID="drpCdPrefixoContaContabil" Width="20%">
                    <asp:ListItem Value="" Text="Selecione..." Selected="True"></asp:ListItem>
                    <asp:ListItem Value="6211" Text="Receita inicial"></asp:ListItem>
                    <asp:ListItem Value="6212" Text="Receita reformulação"></asp:ListItem>
                    <asp:ListItem Value="6221" Text="Despesa inicial"></asp:ListItem>
                    <asp:ListItem Value="6222" Text="Despesa reformulação"></asp:ListItem>
                </box:NDropDownList>
            </td>
        </tr>
    </table>

    <box:NGridView runat="server" ID="grdList" DataKeyNames="IdContaContabil6,IdContaContabil5" OnRowCommand="grdList_RowCommand" ItemType="Creasp.Elo.ContaContabilMapeamento">
        <Columns>
            <asp:BoundField DataField="ContaContabil6.NrContaContabilFmt" HeaderText="Número" SortExpression="ContaContabil6.NrContaContabil" />
            <asp:BoundField DataField="ContaContabil6.NoContaContabil" HeaderText="Nome" SortExpression="ContaContabil6.NoContaContabil" />
            <asp:BoundField DataField="ContaContabil6.NrAno" HeaderText="Ano" SortExpression="ContaContabil6.NrAno" />
            <asp:BoundField DataField="ContaContabil5.NrContaContabilFmt" HeaderText="Número" SortExpression="ContaContabil5.NrContaContabil" />
            <asp:BoundField DataField="ContaContabil5.NoContaContabil" HeaderText="Nome" SortExpression="ContaContabil5.NoContaContabil" />
            <box:NButtonField Icon="pencil" CommandName="alt" Theme="Info" ToolTip="Editar mapeamento" />
            <box:NButtonField Icon="cancel" CommandName="del" Theme="Danger" ToolTip="Remover mapeamento" OnClientClick="return confirm('Tem certeza que deseja remover o mapeamento?')" />
        </Columns>
        <EmptyDataTemplate>Não existe mapeamento de contas contábeis para o filtro informado</EmptyDataTemplate>
    </box:NGridView>

    <box:NPopup runat="server" ID="pnlPopup" Title="Incluir mapeamento" Width="650">
        <box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnSalva" OnClick="btnSalva_Click" Theme="Primary" Text="Salvar" Icon="ok" />
            <box:NButton runat="server" ID="btnInclui" OnClick="btnInclui_Click" Theme="Primary" Text="Incluir" Icon="ok" />
        </box:NToolBar>
        <table class="form">
            <tr>
                <td>Conta 6</td>
                <td>
                    <uc:ContaContabil runat="server" ID="ucContaContabil6" Required="true" NrContaFiltro="6" />
                </td>
            </tr>
            <tr>
                <td>Conta 5</td>
                <td>
                    <uc:ContaContabil runat="server" ID="ucContaContabil5" Required="true" NrContaFiltro="5" />
                </td>
            </tr>
        </table>
    </box:NPopup>

    <box:NPopup runat="server" ID="pnlReplicarMapeamento" Title="Replicar mapeamento" Width="650">
        <box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnConfirmarReplicaMapeamento" OnClick="btnConfirmarReplicaMapeamento_Click" Theme="Primary" Text="Confirmar" Icon="ok" />
        </box:NToolBar>
        <table class="form">
            <tr>
                <td>Ano origem</td>
                <td>
                    <box:NTextBox runat="server" ID="txtAnoOrigem" MaskType="Integer" Required="true"></box:NTextBox>
                </td>
            </tr>
            <tr>
                <td>Ano destino</td>
                <td>
                    <box:NTextBox runat="server" ID="txtAnoDestino" MaskType="Integer" Required="true"></box:NTextBox>
                </td>
            </tr>
        </table>
    </box:NPopup>

</asp:Content>
