﻿<%@ Page Title="Centros de custos" Resource="elo.configuracao" Language="C#" %>

<script runat="server">
    private CreaspContext db = new CreaspContext();

    public int? IdCentroCustoResponsavel { get { return ViewState["IdCentroCustoResponsavel"].To<int?>(); } set { ViewState["IdCentroCustoResponsavel"] = value; } }
    public int? IdCentroCusto { get { return ViewState["IdCentroCusto"].To<int?>(); } set { ViewState["IdCentroCusto"] = value; } }
    public List<ContaContabil> ContasContabeis { get { return ViewState["ContasContabeis"] as List<ContaContabil>; } set { ViewState["ContasContabeis"] = value; } }

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        grdList.RegisterPagedDataSource((p) => db.CentrosCustosResponsaveis.ListaCentrosCustosResponsaveis(p,
            true,
            drpTipoCentroCusto.SelectedValue,
            txtPesquisaNrCentroCusto.Text.To<string>(),
            txtPesquisaNoCentroCusto.Text.To<string>(),
            txtPesquisaNoResponsavel.Text.To<string>()));

        grdListContasContabeis.RegisterDataSource(() => ContasContabeis);

        //TODO: Refatorar (criar func, método separado, qualquer coisa).
        txtPessoaExecuta.RegisterDataSource((q) => db.Query<Pessoa>().Where(x => x.NoPessoa.Contains(q)).Limit(10).ProjectTo(x => new { x.IdPessoa, x.NoPessoa }));
        txtPessoaAprova1.RegisterDataSource((q) => db.Query<Pessoa>().Where(x => x.NoPessoa.Contains(q)).Limit(10).ProjectTo(x => new { x.IdPessoa, x.NoPessoa }));
        txtPessoaAprova2.RegisterDataSource((q) => db.Query<Pessoa>().Where(x => x.NoPessoa.Contains(q)).Limit(10).ProjectTo(x => new { x.IdPessoa, x.NoPessoa }));
        txtPessoaAprova3.RegisterDataSource((q) => db.Query<Pessoa>().Where(x => x.NoPessoa.Contains(q)).Limit(10).ProjectTo(x => new { x.IdPessoa, x.NoPessoa }));
        txtPessoaAprova4.RegisterDataSource((q) => db.Query<Pessoa>().Where(x => x.NoPessoa.Contains(q)).Limit(10).ProjectTo(x => new { x.IdPessoa, x.NoPessoa }));

        txtContaContabilDespesa.RegisterDataSource((q) =>
                db.Query<ContaContabil>()
                    .Where(x => x.NrAno == db.NrAnoBase)
                    .Where(x => x.FgAnalitico && x.FgAtivo)
                    .Where(x => x.NrContaContabil.StartsWith(CdPrefixoContaContabil.DespesaInicial))
                    .Where(x => x.NoContaContabil.StartsWith(q.ToLike()) || x.NrContaContabil.StartsWith(q.ToLike()))
                    .Limit(10)
                    .ToList());
    }

    protected override void OnPrepareForm()
    {
        grdList.DataBind();
        grdListContasContabeis.DataBind();
    }

    protected void btnPesquisar_Click(object sender, EventArgs e)
    {
        grdList.DataBind();
    }

    protected void btnSalvarResponsaveis_Click(object sender, EventArgs e)
    {
        if (IdCentroCustoResponsavel.HasValue)
        {
            db.CentrosCustosResponsaveis.ValidaEAlteraResponsaveis(IdCentroCustoResponsavel.Value,
                txtPessoaExecuta.Value.To<int>(),
                txtPessoaAprova1.Value.To<int>(),
                txtPessoaAprova2.Value.To<int>(),
                txtPessoaAprova3.Value.To<int>(),
                txtPessoaAprova4.Value.To<int>(),
                chkPerfis.Items.Cast<ListItem>().Where(x => x.Selected).Select(x => x.Value).ToList()); //TODO: mandar a lista de checkboxselecionada

            pnlPopupResponsaveis.Close();

            ShowInfoBox("Centro de custo gravado com sucesso");

            grdList.DataBind();
        }
    }
    protected void grdListContasContabeis_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "del")
        {
            int rowIndex = e.CommandArgument.To<int>();

            ContasContabeis.RemoveAt(rowIndex);
            grdListContasContabeis.DataBind();
        }
    }

    protected void grdList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.StartsWith("altCont"))
        {
            var idCentroCustoResponsavel = e.DataKey<int>(sender);
            IdCentroCustoResponsavel = idCentroCustoResponsavel;

            var centroCustoResponsavel = db.CentrosCustosResponsaveis.BuscaCentroCustoResponsavel(idCentroCustoResponsavel);
            txtNrCentroCusto_Cont.Text = centroCustoResponsavel.CentroCusto.NrCentroCustoFmt;
            txtNoCentroCusto_Cont.Text = centroCustoResponsavel.CentroCusto.NoCentroCusto;

            IdCentroCusto = centroCustoResponsavel.IdCentroCusto;


            if (e.CommandName == "altCont-Despesa")
            {
                pnlPopupContasContabeis.Title = "Editar centro de custos (DESPESA)";
                ContasContabeis = db.CentroCustos.ListaContasContabeis(centroCustoResponsavel.IdCentroCusto, CdPrefixoContaContabil.DespesaInicial);
            }

            if (ContasContabeis == null)
            {
                ContasContabeis = new List<ContaContabil>();
            }

            grdListContasContabeis.DataBind();
            pnlPopupContasContabeis.Open(btnAdicionarConta);
        }
        else if (e.CommandName == "altResp")
        {
            var idCentroCustoResponsavel = e.DataKey<int>(sender);
            IdCentroCustoResponsavel = idCentroCustoResponsavel;

            var centroCustoResponsavel = db.CentrosCustosResponsaveis.BuscaCentroCustoResponsavel(idCentroCustoResponsavel);

            txtNrCentroCusto_Resp.Text = centroCustoResponsavel.CentroCusto.NrCentroCustoFmt;
            txtNoCentroCusto_Resp.Text = centroCustoResponsavel.CentroCusto.NoCentroCusto;

            txtPessoaExecuta.Value = centroCustoResponsavel.ResponsavelExecucao?.IdPessoa.ToString();
            txtPessoaExecuta.Text = centroCustoResponsavel.ResponsavelExecucao?.NoPessoa.ToString();
            txtPessoaAprova1.Value = centroCustoResponsavel.ResponsavelAprova?.IdPessoa.ToString();
            txtPessoaAprova1.Text = centroCustoResponsavel.ResponsavelAprova?.NoPessoa.ToString();
            txtPessoaAprova2.Value = centroCustoResponsavel.ResponsavelAprova2?.IdPessoa.ToString();
            txtPessoaAprova2.Text = centroCustoResponsavel.ResponsavelAprova2?.NoPessoa.ToString();
            txtPessoaAprova3.Value = centroCustoResponsavel.ResponsavelAprova3?.IdPessoa.ToString();
            txtPessoaAprova3.Text = centroCustoResponsavel.ResponsavelAprova3?.NoPessoa.ToString();
            txtPessoaAprova4.Value = centroCustoResponsavel.ResponsavelAprova4?.IdPessoa.ToString();
            txtPessoaAprova4.Text = centroCustoResponsavel.ResponsavelAprova4?.NoPessoa.ToString();

            switch (centroCustoResponsavel.CentroCusto.TpCentroCusto)
            {
                case TpCentroCusto.Sintetico:
                    lblTpCentroCusto.Text = "Sintético";
                    break;
                case TpCentroCusto.Analitico:
                    lblTpCentroCusto.Text = "Analítico";
                    break;
                case TpCentroCusto.SubArea:
                    lblTpCentroCusto.Text = "Sub-área";
                    break;
                default:
                    lblTpCentroCusto.Text = "";
                    break;
            }

            var perfisDoCentroCustoResponsavel = db.CentrosCustosPerfis.ListaCentroCustoResponsavelPerfil(idCentroCustoResponsavel).Select(x => x.CdCentroCustoPerfil).ToList();
            var perfisJaUtilizadosEmOutrosCentrosDeCusto = db.CentrosCustosPerfis.ListaCentroCustoPerfilJaUtilizadosPorOutrosCentroCusto(idCentroCustoResponsavel).Select(x => x.CdCentroCustoPerfil).ToList();

            var lstItemPerfis = db.CentrosCustosPerfis.ListaCentroCustosPerfis()
                .Select(x => new ListItem()
                {
                    Value = x.CdCentroCustoPerfil,
                    Text = x.NoCentroCustoPerfil,
                    Selected = perfisDoCentroCustoResponsavel.Contains(x.CdCentroCustoPerfil), //Marca como selecionado os perfis que já são deste CCR.
                    Enabled = !perfisJaUtilizadosEmOutrosCentrosDeCusto.Contains(x.CdCentroCustoPerfil) //Desabilita os perfis que não podem ser marcados porque já estarem em outro CCR.
                })
            .ToList();

            chkPerfis.Bind(lstItemPerfis, false);

            pnlPopupResponsaveis.Open(btnSalvarResponsaveis);
        }
    }

    protected void btnAdicionarConta_Click(object sender, EventArgs e)
    {
        AdicionarConta(txtContaContabilDespesa);
    }

    private void AdicionarConta(NAutocomplete _txtContaContabil)
    {
        if (!string.IsNullOrEmpty(_txtContaContabil.Text))
        {
            var idContaContabil = _txtContaContabil.Value.To<int>();

            if (!ContasContabeis.Any(x => x.IdContaContabil == idContaContabil))
            {
                var contaContabil = db.ContasContabeis.BuscaContaContabil(idContaContabil);
                ContasContabeis.Add(contaContabil);
            }
            else
            {
                ShowErrorBox("Conta já adicionada.");
            }

            grdListContasContabeis.DataBind();

            _txtContaContabil.Value = "";
            _txtContaContabil.Text = "";
        }
    }

    protected void btnSalvarContas_Click(object sender, EventArgs e)
    {
        int[] contasContabeisSelecionadas = new int[grdListContasContabeis.DataKeys.Count];

        for (int i = 0; i < grdListContasContabeis.DataKeys.Count; i++)
        {
            contasContabeisSelecionadas[i] = Convert.ToInt32(grdListContasContabeis.DataKeys[i].Value);
        }

        if (IdCentroCusto.HasValue)
        {
            db.CentroCustos.SalvaContas(IdCentroCusto.Value, contasContabeisSelecionadas, CdPrefixoContaContabil.Despesa);
            ShowInfoBox("Todas informações salvas com sucesso");
        }
        else
        {
            ShowErrorBox("Não foi possível salvar as alterações. Recarregue a página e tente novamente.");
        }

        grdList.DataBind();
        pnlPopupContasContabeis.Close();
    }

</script>
<asp:Content runat="server" ContentPlaceHolderID="content">

    <box:NToolBar runat="server">
        <box:NButton runat="server" ID="btnPesquisar" Theme="Default" Icon="search" Text="Pesquisar" OnClick="btnPesquisar_Click" />
    </box:NToolBar>

    <table class="form">
        <tr>
            <td>Número</td>
            <td>
                <box:NTextBox runat="server" ID="txtPesquisaNrCentroCusto" TextCapitalize="Upper" MaxLength="80" Width="100%" />
            </td>
        </tr>
        <tr>
            <td>Nome</td>
            <td>
                <box:NTextBox runat="server" ID="txtPesquisaNoCentroCusto" TextCapitalize="Upper" MaxLength="80" Width="100%" />
            </td>
        </tr>
        <tr>
            <td>Tipo</td>
            <td>
                <box:NDropDownList runat="server" ID="drpTipoCentroCusto" Width="20%">
                    <asp:ListItem Value="" Text="Selecione..." Selected="True"></asp:ListItem>
                    <asp:ListItem Value="A" Text="Analítico"></asp:ListItem>
                    <asp:ListItem Value="S" Text="Sintético"></asp:ListItem>
                    <asp:ListItem Value="X" Text="Sub-Área"></asp:ListItem>
                </box:NDropDownList>
            </td>
        </tr>
        <tr>
            <td>Responsável</td>
            <td>
                <box:NTextBox runat="server" ID="txtPesquisaNoResponsavel" TextCapitalize="Upper" MaxLength="80" Width="100%" />
            </td>
        </tr>
    </table>

    <box:NGridView runat="server" ID="grdList" DataKeyNames="IdCentroCustoResponsavel" OnRowCommand="grdList_RowCommand" ItemType="Creasp.Elo.CentroCustoResponsavel">
        <Columns>
            <asp:BoundField DataField="CentroCusto.NrCentroCustoFmt" HeaderText="Número" SortExpression="CentroCusto.NrCentroCusto" />
            <asp:BoundField DataField="CentroCusto.NoCentroCusto" HeaderText="Nome" SortExpression="CentroCusto.NoCentroCusto" />
            <asp:BoundField DataField="ResponsavelExecucao.NoPessoa" HeaderText="Executor" SortExpression="ResponsavelExecucao.NoPessoa" />
            <asp:BoundField DataField="ResponsavelAprova.NoPessoa" HeaderText="1º aprovador" SortExpression="ResponsavelAprova.NoPessoa" />
            <asp:BoundField DataField="ResponsavelAprova2.NoPessoa" HeaderText="2º aprovador" SortExpression="ResponsavelAprova2.NoPessoa" />
            <asp:BoundField DataField="ResponsavelAprova3.NoPessoa" HeaderText="3º aprovador" SortExpression="ResponsavelAprova3.NoPessoa" />
            <asp:BoundField DataField="ResponsavelAprova4.NoPessoa" HeaderText="4º aprovador" SortExpression="ResponsavelAprova4.NoPessoa" />
            <asp:BoundField DataField="NoPerfis" HeaderText="Perfis" />
            
            <box:NButtonField Icon="user" CommandName="altResp" Theme="Info" ToolTip="Editar responsável" />
            <box:NButtonField Icon="pencil" CommandName="altCont-Despesa" Theme="Info" ToolTip="Editar contas contábeis (DESPESA)" />
        </Columns>
        <EmptyDataTemplate>Não existem centros de custo para o filtro informado</EmptyDataTemplate>
    </box:NGridView>

    <box:NPopup runat="server" ID="pnlPopupResponsaveis" Title="Editar centro de custo" Width="650">
        <box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnSalvarResponsaveis" OnClick="btnSalvarResponsaveis_Click" Theme="Primary" Text="Salvar" Icon="ok" />
        </box:NToolBar>
        <table class="form">
            <tr>
                <td>Número</td>
                <td>
                    <box:NTextBox runat="server" ID="txtNrCentroCusto_Resp" ShowTextBox="false" Enabled="false" Width="100%" />
                </td>
            </tr>
            <tr>
                <td>Nome</td>
                <td>
                    <box:NTextBox runat="server" ID="txtNoCentroCusto_Resp" ShowTextBox="false" Enabled="false" Width="100%" />
                </td>
            </tr>
            <tr>
                <td>Tipo</td>
                <td>
                    <box:NLabel runat="server" ID="lblTpCentroCusto" Width="100%" />
                </td>
            </tr>
            <tr>
                <td>Perfis</td>
                <td>
                    <box:NCheckBoxList runat="server" ID="chkPerfis"></box:NCheckBoxList>
                </td>
            </tr>
            <tr>
                <td>Executor</td>
                <td>
                    <box:NAutocomplete runat="server" ID="txtPessoaExecuta" Width="100%" TextCapitalize="Upper" DataValueField="IdPessoa" DataTextField="NoPessoa" />
                </td>
            </tr>
            <tr>
                <td>1º aprovador</td>
                <td>
                    <box:NAutocomplete runat="server" ID="txtPessoaAprova1" Width="100%" TextCapitalize="Upper" DataValueField="IdPessoa" DataTextField="NoPessoa" />
                </td>
            </tr>
            <tr>
                <td>2º aprovador</td>
                <td>
                    <box:NAutocomplete runat="server" ID="txtPessoaAprova2" Width="100%" TextCapitalize="Upper" DataValueField="IdPessoa" DataTextField="NoPessoa" />
                </td>
            </tr>
            <tr>
                <td>3º aprovador</td>
                <td>
                    <box:NAutocomplete runat="server" ID="txtPessoaAprova3" Width="100%" TextCapitalize="Upper" DataValueField="IdPessoa" DataTextField="NoPessoa" />
                </td>
            </tr>
            <tr>
                <td>4º aprovador</td>
                <td>
                    <box:NAutocomplete runat="server" ID="txtPessoaAprova4" Width="100%" TextCapitalize="Upper" DataValueField="IdPessoa" DataTextField="NoPessoa" />
                </td>
            </tr>
        </table>
    </box:NPopup>

    <box:NPopup runat="server" ID="pnlPopupContasContabeis" Title="Editar centro de custo" Width="650">
        <box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnSalvarContas" OnClick="btnSalvarContas_Click" Theme="Primary" Text="Salvar" Icon="ok" />
        </box:NToolBar>
        <table class="form">
            <tr>
                <td>Número</td>
                <td>
                    <box:NTextBox runat="server" ID="txtNrCentroCusto_Cont" ShowTextBox="false" Enabled="false" Width="100%" />
                </td>
            </tr>
            <tr>
                <td>Nome</td>
                <td>
                    <box:NTextBox runat="server" ID="txtNoCentroCusto_Cont" ShowTextBox="false" Enabled="false" Width="100%" />
                </td>
            </tr>
            <tr>
                <td>Conta</td>
                <td>
                    <box:NAutocomplete runat="server" ID="txtContaContabilDespesa" TextCapitalize="Upper" Width="100%" DataValueField="IdContaContabil" DataTextField="NoContaContabilFmt" AutoPostBack="true" />
                    <%--<box:NAutocomplete runat="server" ID="txtContaContabilReceita" TextCapitalize="Upper" Width="100%" DataValueField="IdContaContabil" DataTextField="NoContaContabilFmt" AutoPostBack="true" Visible="false" />--%>
                </td>
                <td>
                    <box:NButton runat="server" ID="btnAdicionarConta" Theme="Default" Text="Adicionar" Icon="add" OnClick="btnAdicionarConta_Click" />
                </td>
            </tr>
        </table>

        <box:NGridView runat="server" ID="grdListContasContabeis" DataKeyNames="IdContaContabil" OnRowCommand="grdListContasContabeis_RowCommand" ItemType="Creasp.Elo.ContaContabil">
            <Columns>
                <asp:BoundField DataField="NrContaContabilFmt" HeaderText="Número" SortExpression="NrContaContabilFmt" />
                <asp:BoundField DataField="NoContaContabil" HeaderText="Nome" SortExpression="NoContaContabil" />
                <asp:BoundField DataField="CdResumido" HeaderText="Cd. Resumido" SortExpression="CdResumido" />
                <box:NButtonField Icon="cancel" CommandName="del" Theme="Danger" ToolTip="Remover conta" />
            </Columns>
        </box:NGridView>
    </box:NPopup>

</asp:Content>
