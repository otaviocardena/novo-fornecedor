﻿<%@ Page Title="Metodologias (Receita)" Resource="elo.configuracao" Language="C#" %>

<script runat="server">
    private CreaspContext db = new CreaspContext();

    private int? IdReformulacao => Request.QueryString["IdReformulacao"].To<int?>();
    public int? IdMetologiaReceitaAno { get { return ViewState["IdMetologiaReceitaAno"].To<int?>(); } set { ViewState["IdMetologiaReceitaAno"] = value; } }

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        grdList.RegisterPagedDataSource((p) => db.MetodologiasReceita.ListaMetodologias(p,
            IdReformulacao,
            drpTipoContaContabil.SelectedValue.To<int>(),
            txtPesquisaNrContaContabil.Text.To<string>(),
            txtPesquisaNoContaContabil.Text.To<string>()));

        

        if (db.CentrosCustosPerfis.ValidaUsuarioLogadoPossuiPerfil(TpPerfilCentroCusto.Contabilidade))
        {
            lblTotalReceitaOrcado.Text = string.Format("Total de receita do ano: R$ {0}", db.MetodologiasReceita.BuscaTotalOrcadoAno(IdReformulacao).ToString("n2"));   
        }
    }

    protected override void OnPrepareForm()
    {
        grdList.DataBind();
        DefineQuaisBotoesExibir();
    }

    protected void btnPesquisar_Click(object sender, EventArgs e)
    {
        grdList.DataBind();
    }

    //protected void btnOK_Click(object sender, EventArgs e)
    //{
    //    if (IdMetologiaReceitaAno.HasValue)
    //    {
    //        //db.ContasContabeis.Altera(IdContaContabil.Value, txtResponsavel.Value.To<int>());

    //        pnlPopup.Close();

    //        ShowInfoBox("Metodologia alterada com sucesso");

    //        grdList.DataBind();
    //    }
    //}

    protected void grdList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "alt")
        {
            var idMetodologiaReceita = e.DataKey<int>(sender);
            IdMetologiaReceitaAno = idMetodologiaReceita;

            if (IdReformulacao.HasValue)
            {
                Redirect(string.Format("MetodologiaReceita.Form.aspx?IdReformulacao={0}&IdMetologiaReceitaAno={1}",
                    IdReformulacao,
                    idMetodologiaReceita));
            }
            else
            {
                Redirect("MetodologiaReceita.Form.aspx?IdMetologiaReceitaAno=" + idMetodologiaReceita);
            }
        }
    }

    protected void btnAprovarMetodologias_Click(object sender, EventArgs e)
    {
        db.MetodologiasReceita.DisponibilizaMetodologiasReceitaAoProximoResponsavel(db.NrAnoBase, IdReformulacao);
        grdList.DataBind();
        DefineQuaisBotoesExibir();
        ShowInfoBox("Metodologias aprovadas com sucesso");
    }

    protected void btnReprovarMetodologias_Click(object sender, EventArgs e)
    {
        db.MetodologiasReceita.DisponibilizaMetodologiasReceitaAoResponsavelAnterior(db.NrAnoBase, IdReformulacao);
        grdList.DataBind();
        DefineQuaisBotoesExibir();
        ShowInfoBox("Metodologias reprovadas com sucesso");
    }

    private void DefineQuaisBotoesExibir()
    {
        btnAprovarMetodologias.Visible = db.MetodologiasReceita.VerificaSeUsuarioPodeAprovarMetodologia(IdReformulacao);
        btnReprovarMetodologias.Visible = db.MetodologiasReceita.VerificaSeUsuarioPodeReprovarMetodologia(IdReformulacao);
    }

    protected void btnEntendi_Click(object sender, EventArgs e)
    {
        pnlErros.Close();
    }

    protected void btnAtualizarObs_Click(object sender, EventArgs e)
    {
        pnlAtualizarObs.Open();
    }

    protected void btnAtualizar_Click(object sender, EventArgs e)
    {

        var noObservacoesPadraoReceita = txtNoObservacoesPadraoReceita.Text.To<string>();

        db.MetodologiasReceita.AtualizaObservacoesMetodologias(db.NrAnoBase, noObservacoesPadraoReceita);
        grdList.DataBind();
        pnlAtualizarObs.Close();
        ShowInfoBox("Observações atualizadas");
    }


</script>
<asp:Content runat="server" ContentPlaceHolderID="content">

    <box:NToolBar runat="server">
        <box:NButton runat="server" ID="btnPesquisar" Theme="Default" Icon="search" Text="Pesquisar" OnClick="btnPesquisar_Click" />
        <box:NButton runat="server" ID="btnAprovarMetodologias" Theme="Success" Icon="ok" Text="Aprovar metodologias" Visible="False" OnClick="btnAprovarMetodologias_Click" OnClientClick="return confirm('Deseja aprovar todas metodologias para o próximo responsável?')" />
        <box:NButton runat="server" ID="btnAtualizarObs" Theme="Primary" Icon="pencil" Text="Atualizar observações de receita" Visible="True" OnClick="btnAtualizarObs_Click" />
        <box:NButton runat="server" ID="btnReprovarMetodologias" Theme="Danger" Icon="cancel" Text="Reprovar metodologias" Visible="False" OnClick="btnReprovarMetodologias_Click" OnClientClick="return confirm('Deseja reprovar todas metodologias para o responsável anterior?')" />
        <box:NLabel runat="server" ID="lblTotalReceitaOrcado" CssClass="t-bold right"/>
    </box:NToolBar>

    <table class="form">
        <tr>
            <td>Número</td>
            <td>
                <box:NTextBox runat="server" ID="txtPesquisaNrContaContabil" TextCapitalize="Upper" MaxLength="80" Width="100%" />
            </td>
        </tr>
        <tr>
            <td>Nome</td>
            <td>
                <box:NTextBox runat="server" ID="txtPesquisaNoContaContabil" TextCapitalize="Upper" MaxLength="80" Width="100%" />
            </td>
        </tr>
        <tr>
            <td>Tipo</td>
            <td>
                <box:NDropDownList runat="server" ID="drpTipoContaContabil" Width="20%">
                    <asp:ListItem Value="0" Text="Selecione..." Selected="True"></asp:ListItem>
                    <asp:ListItem Value="1" Text="Analítico"></asp:ListItem>
                    <asp:ListItem Value="2" Text="Sintético"></asp:ListItem>
                </box:NDropDownList>
            </td>
        </tr>
    </table>

    <box:NGridView runat="server" ID="grdList" DataKeyNames="IdMetodologiaReceitaAno" OnRowCommand="grdList_RowCommand" ItemType="Creasp.Elo.MetodologiaReceitaAno">
        <Columns>
            <asp:BoundField DataField="ContaContabil.NrContaContabilFmt" HeaderText="Número" SortExpression="ContaContabil.NrContaContabil" 
                HeaderStyle-Width="170" ItemStyle-Width="170" />
            <asp:BoundField DataField="ContaContabil.NoContaContabil" HeaderText="Nome" SortExpression="ContaContabil.NoContaContabil" />
            <%--<asp:BoundField DataField="ContaContabil.CdResumido" HeaderText="Código" SortExpression="ContaContabil.CdResumido" />--%>
            <%--<asp:BoundField DataField="TpContaContabil" HeaderText="Tipo" SortExpression="FgAnalitico" />--%>
            <asp:BoundField DataField="Responsavel.NoPessoa" HeaderText="Responsável" SortExpression="Responsavel.NoPessoa" />
            <asp:BoundField DataField="StatusPropostaOrcamentaria.NoDescricao" HeaderText="Status" SortExpression="StatusPropostaOrcamentaria.NoDescricao" 
                HeaderStyle-Width="100" ItemStyle-Width="100" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"/>
            <asp:BoundField DataField="VrOrcado" HeaderText="Orçado" SortExpression="VrOrcado" DataFormatString="{0:n2}" 
                HeaderStyle-Width="110" ItemStyle-Width="110" HeaderStyle-HorizontalAlign="Right" ItemStyle-HorizontalAlign="Right" />
            <box:NButtonField Icon="search" CommandName="alt" Theme="Info" ToolTip="Orçar receita" />
            <%--<box:NButtonField Icon="ok"  CommandName="env" Theme="Info" ToolTip='Disponibilizar proposta' OnClientClick="return confirm('Deseja disponibilizar esta proposta para o próximo responsável?')" />
            <box:NButtonField Icon="left-fat" CommandName="vol" Theme="Info" ToolTip="Reprovar proposta" OnClientClick="return confirm('Deseja reprovar esta proposta para o responsável anterior? Suas alterações serão descartadas')" />--%>
        </Columns>
        <EmptyDataTemplate>Não existem contas contábeis para o filtro informado</EmptyDataTemplate>
    </box:NGridView>

    <box:NPopup runat="server" ID="pnlErros" Title="Erros encontrados na integração" Width="600">
        <box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnEntendi" Theme="Default" Text="Entendi" Icon="ok" OnClick="btnEntendi_Click" />
        </box:NToolBar>
        
        <table class="form">
            <tr>
                <td>Os seguintes erros foram encontrados ao tentar enviar as informações ao SISCONT</td>
            </tr>
            <tr>
                <td>
                    <box:NTextBox runat="server" ID="txtErros" TextMode="MultiLine" Width="100%" Height="200px"></box:NTextBox></td>
            </tr>
        </table>

    </box:NPopup>
        <box:NPopup runat="server" ID="pnlAtualizarObs" Title="Atualizar Observações de receita" Width="650">
        <box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnAtualizar" Theme="Primary" Text="Atualizar" Icon="ok" OnClick="btnAtualizar_Click" />
        </box:NToolBar>
        
        <table class="form">
            <tr>
                <td>Observações de receita</td>
                <td>
                    <box:NTextBox runat="server" TextMode="MultiLine" MaxLength="1000" ID="txtNoObservacoesPadraoReceita" Width="100%" /></td>
            </tr>
        </table>
    </box:NPopup>

    <%--<box:NPopup runat="server" ID="pnlPopup" Title="Editar conta contábil" Width="650">
        <box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnOK" OnClick="btnOK_Click" Theme="Primary" Text="Salvar" Icon="ok" />
        </box:NToolBar>
        <table class="form">
            <tr>
                <td>Número</td>
                <td>
                    <box:NTextBox runat="server" ID="txtNrContaContabil" ShowTextBox="false" Enabled="false" Width="100%" />
                </td>
            </tr>
            <tr>
                <td>Nome</td>
                <td>
                    <box:NTextBox runat="server" ID="txtNoContaContabil" ShowTextBox="false" Enabled="false" Width="100%" />
                </td>
            </tr>
            <tr>
                <td>Tipo</td>
                <td>
                    <box:NLabel runat="server" ID="lblTpContaContabil" Width="100%" />
                </td>
            </tr>
        </table>
    </box:NPopup>--%>
</asp:Content>
