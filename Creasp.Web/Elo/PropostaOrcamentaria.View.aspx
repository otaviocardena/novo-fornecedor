﻿<%@ Page Title="Proposta Orçamentária" Resource="elo" Language="C#" %>

<script runat="server">

    private CreaspContext db = new CreaspContext();

    private int? IdPropostaOrcamentariaDespesa => Request.QueryString["IdPropostaOrcamentariaDespesa"].To<int?>();
    private int IdPropostaOrcamentariaDespesaItem { get { return (int)ViewState["IdPropostaOrcamentariaDespesaItem"]; } set { ViewState["IdPropostaOrcamentariaDespesaItem"] = value; } }

    private PropostaOrcamentariaDespesa Proposta;
    private List<PropostaOrcamentariaDespesaItem> ItensProposta { get { return ViewState["ItensProposta"] as List<PropostaOrcamentariaDespesaItem>; } set { ViewState["ItensProposta"] = value; } }
    private bool? ItensNaoSalvos { get { return ViewState["ItensNaoSalvos"] as Nullable<bool>; } set { ViewState["ItensNaoSalvos"] = value; } }

    //private decimal? toVrOrcadoAnoAnterior = 0;
    private decimal? toVrAprovadoAnoAnterior = 0;
    private decimal? toVrExecitadoAnoAnterior = 0;
    private decimal? toVrOrcado = 0;

    private string nrCentroCusto = string.Empty;
    private string noResponsavel = string.Empty;
    private string nrAno = string.Empty;

    #region Init
    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        if (!IdPropostaOrcamentariaDespesa.HasValue)
        {
            Redirect("PropostaOrcamentaria.aspx");
        }

        grdList.RegisterDataSource(() => ItensProposta);
        RegisterRestorePageState(() => grdList.DataBind());

        //RegisterResource("elo.propostaorcamentaria"); //Precisa?
    }

    protected override void OnPrepareForm()
    {
        if (Proposta == null)
        {
            Proposta = db.PropostasOrcamentarias.BuscaPropostaOrcamentaria(IdPropostaOrcamentariaDespesa.Value);
            lblCentroCusto.Text = Proposta.CentroCustoOriginal.NoCentroCustoFmt;
            lblResponsavel.Text = Proposta.Responsavel?.NoPessoa;
            lblAno.Text = Proposta.NrAno.ToString();

            if (!string.IsNullOrEmpty(Proposta.CdTpFormulario))
            {
                lblFormulario.Text = Proposta.CdTpFormulario;
            }
            else
            {
                lblFormulario.Text = "Não";
            }
        }

        if (ItensProposta == null)
        {
            ItensProposta = db.PropostasOrcamentarias.ListaPropostaOrcamentariaItens(IdPropostaOrcamentariaDespesa.Value);
        }

        AtualizaTotalizadores();
        grdList.DataBind();

        this.Audit(Proposta, IdPropostaOrcamentariaDespesa);
        this.BackTo("PropostaOrcamentaria.aspx");
    }
    #endregion

    #region Events
    protected void grdList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName.Equals("ViewEspecificacao"))
        {
            IdPropostaOrcamentariaDespesaItem = Convert.ToInt32(grdList.DataKeys[e.CommandArgument.To<int>()].Value);

            txtNoEspecificacao.Text = ItensProposta
                .Where(x => x.IdPropostaOrcamentariaDespesaItem == IdPropostaOrcamentariaDespesaItem)
                .Single()?
                .NoEspecificacao;

            pnlPopupEspecificacao.Open();
        }
        else if (e.CommandName.Equals("ViewJustificativa"))
        {
            IdPropostaOrcamentariaDespesaItem = Convert.ToInt32(grdList.DataKeys[e.CommandArgument.To<int>()].Value);

            txtNoJustificativa.Text = ItensProposta
                .Where(x => x.IdPropostaOrcamentariaDespesaItem == IdPropostaOrcamentariaDespesaItem)
                .Single()?
                .NoJustificativa;

            pnlPopupJustificativa.Open();
        }
    }

    protected void btnVoltarJustificativa_Click(object sender, EventArgs e)
    {
        pnlPopupJustificativa.Close();
    }

    protected void btnVoltarEspecificacao_Click(object sender, EventArgs e)
    {
        pnlPopupEspecificacao.Close();
    }

    protected void btnRelatoriosDemDetalhado_Click(object sender, ReportFormat e)
    {
        var incluiZerados = chkFgIncluiValoresZerados.Checked;
        var rpt = new Report(Server.MapPath("~/Elo/Reports/DemonstrativoDetalhado.rdl"));
        rpt.AddDataSet("dsMain", db.RelatoriosElo.MontarGridDespesaDetalhado(incluiZerados, IdPropostaOrcamentariaDespesa));
        ShowReport(rpt, e, "Relatorio-DemonstrativoDespesaDetalhado-" + DateTime.Now.ToString("yyyyMMddHHmmss"));
    }
    #endregion

    #region Privados
    private void AtualizaTotalizadores()
    {
        if (ItensProposta != null)
        {
            //toVrOrcadoAnoAnterior = ItensProposta.Sum(x => x.VrOrcadoAnoAnterior);
            toVrAprovadoAnoAnterior = ItensProposta.Sum(x => x.VrAprovadoAnoAnterior);
            toVrExecitadoAnoAnterior = ItensProposta.Sum(x => x.VrExecutadoAnoAnterior);
            toVrOrcado = ItensProposta.Sum(x => x.VrOrcado);
        }
    }
    #endregion
</script>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <box:NToolBar runat="server">
        <uc:ReportButton runat="server" ID="btnRelatoriosDemDetalhado" Text="Gerar Relatório DESPESA DETALHADO" OnClick="btnRelatoriosDemDetalhado_Click" />
        <box:NCheckBox runat="server" ID="chkFgIncluiValoresZerados" Text="Inclui valores zerados" Checked="false" />
    </box:NToolBar>

    <table class="form">
        <tr>
            <td>Centro de custo</td>
            <td>
                <box:NLabel runat="server" ID="lblCentroCusto" /></td>
        </tr>
        <tr>
            <td>Formulário</td>
            <td>
                <box:NLabel runat="server" ID="lblFormulario"/></td>
        </tr>
        <tr>
            <td>Responsável</td>
            <td>
                <box:NLabel runat="server" ID="lblResponsavel" /></td>
        </tr>
        <tr>
            <td>Ano</td>
            <td>
                <box:NLabel runat="server" ID="lblAno" /></td>
        </tr>
    </table>

    <box:NGridView runat="server" ID="grdList" ShowFooter="true" DataKeyNames="IdPropostaOrcamentariaDespesaItem" OnRowCommand="grdList_RowCommand" ItemType="Creasp.Elo.PropostaOrcamentariaDespesaItem">
        <Columns>
            <asp:TemplateField HeaderText="Número" HeaderStyle-Width="145">
                <ItemTemplate>
                    <asp:Label runat="server" ID="lblOrcadoAnoAnterior"><%# Item?.ContaContabil?.NrContaContabilFmt %></asp:Label>
                </ItemTemplate>
                <FooterTemplate>
                    Total
                </FooterTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ContaContabil.NoContaContabil" HeaderText="Nome" SortExpression="ContaContabil.NoContaContabil" />
            <%--<asp:BoundField DataField="ContaContabil.CdResumido" HeaderText="Cod." HeaderStyle-Width="45" SortExpression="ContaContabil.CdResumido" />--%>
            <asp:BoundField DataField="NrProcessoL" HeaderText="Processo" SortExpression="NrProcessoL" />
            <asp:BoundField DataField="NoEspecificacao" HeaderText="Especificação" SortExpression="NoEspecificacao" ItemStyle-CssClass="nowrap-ellipsis" />
            <box:NButtonField Icon="search" Theme="Info" ToolTip="Ver especificação" CommandName="ViewEspecificacao" EnabledExpression="IdPropostaOrcamentariaDespesaItem" />
            <asp:BoundField DataField="NoJustificativa" HeaderText="Justificativa" SortExpression="NoJustificativa" ItemStyle-CssClass="nowrap-ellipsis" />
            <box:NButtonField Icon="search" Theme="Info" ToolTip="Ver justificativa" CommandName="ViewJustificativa" EnabledExpression="IdPropostaOrcamentariaDespesaItem" />

            <%--<asp:TemplateField HeaderText="Orçado ano anterior">
                <HeaderTemplate>
                    <%# string.Format("Orçado {0}", DateTime.Now.Year ) %>
                </HeaderTemplate>
                <ItemTemplate>
                    <box:NLabel runat="server" Text='<%# Item.VrOrcadoAnoAnterior.ToString("n2") %>' />
                </ItemTemplate>
                <FooterTemplate>
                    <%# toVrOrcadoAnoAnterior.ToString("n2") %>
                </FooterTemplate>
            </asp:TemplateField>--%>
            <asp:TemplateField HeaderText="Dotação" ItemStyle-HorizontalAlign="Right">
                <HeaderTemplate>
                    <%# string.Format("Dotação {0}", db.NrAnoBase - 1) %>
                </HeaderTemplate>
                <ItemTemplate>
                    <box:NLabel runat="server" Text='<%# Item.VrAprovadoAnoAnterior?.ToString("n2") %>' />
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label runat="server" ID="lblAprovadoAnoAnterior"><%# toVrAprovadoAnoAnterior?.ToString("n2") %></asp:Label>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Executado ano anterior" ItemStyle-HorizontalAlign="Right">
                <HeaderTemplate>
                    <%# string.Format("Executado até {0}", ItensProposta.Where(x => x.DtRefVrExecutado.HasValue).FirstOrDefault() != null ? ItensProposta.Where(x => x.DtRefVrExecutado.HasValue).FirstOrDefault().DtRefVrExecutado.Value.ToString("dd/MM/yyyy") : "01/01/01") %>
                </HeaderTemplate>
                <ItemTemplate>
                    <box:NLabel runat="server" Text='<%# Item.VrExecutadoAnoAnterior?.ToString("n2") %>' />
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label runat="server" ID="lblExecutadoAnoAnterior"><%# toVrExecitadoAnoAnterior?.ToString("n2") %></asp:Label>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Orçado" ItemStyle-HorizontalAlign="Right">
                <HeaderTemplate>
                    <%# string.Format("Orçado {0}", db.NrAnoBase) %>
                </HeaderTemplate>
                <ItemTemplate>
                    <box:NLabel runat="server" Text='<%# Item.VrOrcado?.ToString("n2") %>' />
                </ItemTemplate>
                <FooterTemplate>
                    <asp:Label runat="server" ID="lblOrcado" CssClass="t-center"><%# toVrOrcado?.ToString("n2") %></asp:Label>
                </FooterTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>Esta proposta orçamentária está vazia.</EmptyDataTemplate>
    </box:NGridView>

    <box:NPopup runat="server" ID="pnlPopupEspecificacao" Title="Ver especificação" Width="650">
        <box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnVoltarEspecificacao" OnClick="btnVoltarEspecificacao_Click" Theme="Default" Text="Voltar" Icon="reply" />
        </box:NToolBar>
        <table class="form">
            <tr>
                <td>Especificação</td>
                <td>
                    <box:NTextBox runat="server" ID="txtNoEspecificacao" TextMode="MultiLine" ShowTextBox="false" Width="100%" Disabled="Disabled" />
                </td>
            </tr>
        </table>
    </box:NPopup>

    <box:NPopup runat="server" ID="pnlPopupJustificativa" Title="Ver justificativa" Width="650">
        <box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnVoltarJustificativa" OnClick="btnVoltarJustificativa_Click" Theme="Default" Text="Voltar" Icon="reply" />
        </box:NToolBar>
        <table class="form">
            <tr>
                <td>Justificativa</td>
                <td>
                    <box:NTextBox runat="server" ID="txtNoJustificativa" TextMode="MultiLine" ShowTextBox="false" Width="100%" Disabled="Disabled" />
                </td>
            </tr>
        </table>
    </box:NPopup>

</asp:Content>
