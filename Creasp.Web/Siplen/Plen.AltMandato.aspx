﻿<%@ Page Title="Editar mandato" Resource="siplen" Language="C#" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();

    private int IdMandato => Request.QueryString["IdMandato"].To<int>();
    private int IdMandatoTit { get { return ViewState["IdMandatoTit"].To<int>(); } set { ViewState["IdMandatoTit"] = value; } }

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        RegisterResource("siplen.plen");
    }

    protected override void OnPrepareForm()
    {
        cmbMotivo.Bind(db.Query<MotivoAfastamento>().Where(x => x.FgAtivo).OrderBy(x => x.NoMotivo).ToEnumerable());

        var m = db.Query<Mandato>()
            .Include(x => x.Pessoa)
            .Include(x => x.Cargo)
            .Include(x => x.Camara)
            .Include(x => x.Entidade)
            .Where(x => x.TpMandato == TpMandato.Conselheiro)
            .Where(x => x.IdMandato == IdMandato)
            .Single();

        lblNoConselheiro.Text = m.Pessoa.NoPessoa;
        lblNoTitulo.Text = m.Pessoa.NoTituloAbr;
        lblNoTitulo.ToolTip = m.Pessoa.NoTitulo;
        lblNoMandato.Text = m.Cargo.NoCargo + " (" + m.AnoIni + " - " + m.AnoFim + ")";
        ucPeriodo.Periodo = m.GetPeriodo();
        cmbMotivo.SelectedValue = m.CdMotivo.To<string>();
        txtNoHistorico.Text = m.NoHistorico;
        chkFgValido.Checked = m.FgValido;

        IdMandatoTit = m.IdMandatoTit ?? m.IdMandato;
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        db.Conselheiros.AlteraMandato(IdMandato, ucPeriodo.Periodo, cmbMotivo.SelectedValue.To<int?>(), txtNoHistorico.Text, chkFgValido.Checked);
        ShowInfoBox("Mandato de conselheiro alterado com sucesso");
        ClosePopup();
    }

</script>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <div class="mt-30 c-primary-blue left-right">
        <box:NLabel runat="server" ID="lblNoConselheiro" />
        <box:NLabel runat="server" ID="lblNoTitulo" CssClass="no-titulo" />
    </div>

    <div class="form-div">
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Mandato</span></div>
                <box:NLabel runat="server" ID="lblNoMandato" CssClass="field" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Período</span></div>
                <uc:Periodo runat="server" id="ucPeriodo" ShowTextBox="true"/>
                <div class="help">Quando o período não terminar no último dia do mandato selecione o motivo</div>
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Motivo</span></div>
                <box:NDropDownList runat="server" ID="cmbMotivo" Width="100%" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Histórico</span></div>
                <box:NTextBox runat="server" ID="txtNoHistorico" Rows="4" Width="100%" TextMode="MultiLine" TextCapitalize="Upper" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <box:NCheckBox runat="server" ID="chkFgValido" Text="Mandato válido" />
            </div>
        </div>
    </div>

    <box:NButton runat="server" ID="btnSalvar" Icon="ok" Text="Salvar" CssClass="right" Theme="Primary" OnClick="btnSalvar_Click" />

</asp:Content>