﻿<%@ Page Title="Histórico Profissional" Resource="siplen" Language="C#" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();

    public int IdPessoa { get { return Request.QueryString["IdPessoa"].To<int>(); } }

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        // monta histórico
        grdMandato.RegisterDataSource(() => db.Query<Mandato>()                            
            .Include(x => x.Motivo, JoinType.Left)
            .Include(x => x.Cargo)
            .Include(x => x.Camara, JoinType.Left)
            .Include(x => x.Entidade, JoinType.Left)
            .Include(x => x.Grupo, JoinType.Left)
            .Include(x => x.Comissao, JoinType.Left)
            .Include(x => x.Inspetoria, JoinType.Left)
            .Where(x => x.IdPessoa == IdPessoa)
            .OrderByDescending(x => x.DtIni)
            .ToEnumerable());

        // monta participacao em eventos
        grdEvento.RegisterPagedDataSource((sp) => db.Query<EventoParticipante>()
            .Include(x => x.Evento)
            .Where(x => x.IdPessoaTit == IdPessoa || x.IdPessoaSup == IdPessoa)
            .OrderByDescending(x => x.Evento.DtEvento)
            .ToPage(sp));

        // monta licenças
        grdLicenca.RegisterDataSource(() => db.Query<Licenca>()
            .Where(x => x.IdPessoa == IdPessoa)
            .OrderByDescending(x => x.DtIni)
            .ToEnumerable());;

        // monta emails
        grdEmail.RegisterPagedDataSource((sp) => db.Query<Creasp.Core.Email>()
            .Where(x => x.IdPessoa == IdPessoa)
            .OrderByDescending(x => x.DtEnvio)
            .ToPage(sp, x => new { x.IdEmail, x.DtEnvio, NoMensagem = $"<i>{x.NoAssunto}</i> ({x.NoEmail})<br/>{x.NoMensagem.MaxLength(200)}" }));

        // monta documentos
        grdDocs.RegisterDataSource(() => db.Documentos.ListaDocumentosPessoa(IdPessoa));

        RegisterResource("siplen.hist").Disabled(btnSync).GridColumns(grdEmail, "resend").GridColumns(grdDocs, "download");
    }

    protected override void OnPrepareForm()
    {
        this.BackTo("Hist.aspx");
        MontaTela();
        grdMandato.DataBind();
    }

    private void MontaTela()
    {
        var p = db.Query<Pessoa>()
            .Include(x => x.Cep, JoinType.Left)
            .Where(x => x.IdPessoa == IdPessoa)
            .Single();

        ucPessoa.LoadPessoa(IdPessoa);
        ucPessoa.EditOnly = true;
        lblNoTitulo.Text = p.NoTitulo;
        lblNrCreasp.Text = p.NrCreasp;
        lblNrCPF.Text = p.NrCpfFmt;
        lblNrTelefone.Text = p.NrTelefone;
        lblTpSexo.Text = p.TpSexo == TpSexo.Masculino ? "Masculino" : "Feminino";
        lblDtNasc.Text = string.Format("{0:d}", p.DtNasc);
        lblNoEmail.Text = p.NoEmail;
        lblNoEndereco.Text = p.EnderecoFmt;
        lblNoEndCorresp.Text = p.NoEndCorresp;
        lblNoHistorico.Text = p.NoHistorico?.Replace("\n", "<br/>");
        trHistorico.Visible = p.NoHistorico != null;

        this.SetPageTitle(p.NoPessoa.Captalize(), p.NrCreasp);
    }

    private string GetPresenca(string tpPresenca)
    {
        switch(tpPresenca)
        {
            case TpPresenca.Presente: return "Presente";
            case TpPresenca.Licenciado: return "Licenciado";
            case TpPresenca.Justificado: return "Justificado";
            case TpPresenca.Falta: return "Falta";
            default: return "-";
        }
    }

    protected void btnSync_Click(object sender, EventArgs e)
    {
        var p = db.Pessoas.BuscaPessoa(IdPessoa, false);
        var atu = db.Pessoas.AtualizaCadastro(p);
        MontaTela();
        ShowInfoBox(atu ? "Dados sincronizados com sucesso" : "Nenhuma atualização necessária");
    }

    protected void ucPessoa_PessoaChanged(object sender, EventArgs e)
    {
        MontaTela();
    }

    protected void grdDocs_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        db.Documentos.Download(this, e.DataKey<int>(sender));
    }

    protected void grdEmail_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        db.Emails.ReenviaEmail(e.DataKey<int>(sender));
        ShowInfoBox("Email reenviado com sucesso");
        grdEmail.DataBind();
    }

    protected void TabChanged(object sender, EventArgs e)
    {
        var tab = sender as NTab;

        if (tab.ID == "tabHist" && grdMandato.Rows.Count == 0) grdMandato.DataBind();
        if (tab.ID == "tabEvento" && grdEvento.Rows.Count == 0) grdEvento.DataBind();
        if (tab.ID == "tabLic" && grdLicenca.Rows.Count == 0) grdLicenca.DataBind();
        if (tab.ID == "tabEmail" && grdEmail.Rows.Count == 0) grdEmail.DataBind();
        //if(tab.ID == "tabNerp" && grdMandato.Rows.Count == 0) grdMandato.DataBind();
        if (tab.ID == "tabDocs" && grdDocs.Rows.Count == 0) grdDocs.DataBind();

    }

</script>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <box:NTab runat="server" ID="tabGeral" Text="Dados cadastrais" Selected="true">

        <box:NButton runat="server" ID="btnSync" Text="Sincronizar" CssClass="right mt-20" Theme="Primary" ToolTip="Sincronizar os dados do profissional com o CREANET (nome, sexo, titulos, email, endereço residencial, telefones e data de nascimento)" OnClick="btnSync_Click" />

        <div class="form-div">
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Nome</span></div>
                    <uc:Pessoa runat="server" CssClass="field" ID="ucPessoa" OnPessoaChanged="ucPessoa_PessoaChanged" Enabled="false" IsProfissional="true" />
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>CREASP</span></div>
                    <box:NLabel runat="server" CssClass="field" ID="lblNrCreasp" />
                </div>
                <div class="div-sub-form form-uc">
                    <div><span>Sexo</span></div>
                    <box:NLabel runat="server" CssClass="field ml-10" ID="lblTpSexo" />
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>CPF</span></div>
                    <box:NLabel runat="server" CssClass="field" ID="lblNrCPF" />
                </div>
                <div class="div-sub-form form-uc">
                    <div><span>Nascimento</span></div>
                    <box:NLabel runat="server" CssClass="field ml-10" ID="lblDtNasc" />
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Email</span></div>
                    <box:NLabel runat="server" CssClass="field" ID="lblNoEmail" />
                </div>
                <div class="div-sub-form form-uc">
                    <div><span>Telefone(s)</span></div>
                    <box:NLabel runat="server" CssClass="field ml-10" ID="lblNrTelefone" />
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Título(s)</span></div>
                    <box:NLabel runat="server" CssClass="field" ID="lblNoTitulo" />
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Endereço de ressarcimento</span></div>
                    <box:NLabel runat="server" CssClass="pre field" ID="lblNoEndereco" />
                </div>
            </div>
             <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Endereço de correspondência</span></div>
                    <box:NLabel runat="server" ID="lblNoEndCorresp" CssClass="pre field" />
                </div>
            </div>
             <div class="sub-form-row" runat="server" id="trHistorico">
                <div class="div-sub-form form-uc">
                    <div><span>Observações</span></div>
                    <box:NLabel runat="server" CssClass="field" ID="lblNoHistorico" />
                </div>
            </div>
        </div>

    </box:NTab>

    <box:NTab runat="server" ID="tabHist" Text="Histórico" AutoPostBack="true" OnTabChanged="TabChanged">
        <box:NGridView runat="server" ID="grdMandato" ItemType="Creasp.Siplen.Mandato">
            <Columns>
                <asp:TemplateField HeaderText="Período" HeaderStyle-Width="200" ItemStyle-HorizontalAlign="Left" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <%# Item.GetPeriodo().ToString(false) %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Descrição">
                    <ItemTemplate>
                        <%# Item.TpMandato == TpMandato.Conselheiro ? "Conselheiro " + (Item.CdCargo == CdCargo.Suplente ? "suplente : " : "titular : ") + Item.AnoIni + "-" + Item.AnoFim + " - " + Item.Entidade.NoEntidade + " (" + Item.Camara.NoCamara + ")" : "" %>
                        <%# Item.TpMandato == TpMandato.Inspetor ? Item.Cargo.NoCargo + " - " + Item.Inspetoria.NoInspetoria : "" %>
                        <%# Item.TpMandato == TpMandato.Diretor ? Item.Cargo.NoCargo : "" %>
                        <%# Item.TpMandato == TpMandato.Camara ? Item.Cargo.NoCargo + "  de câmara especializada: " + Item.Camara.NoCamara : "" %>
                        <%# Item.TpMandato == TpMandato.Comissao ? (Item.NrOrdem > 0 ? Item.NrOrdem + "º " : "") + Item.Cargo.NoCargo + " da comissão: " + Item.Comissao.NoComissao : "" %>
                        <%# Item.TpMandato == TpMandato.Grupo ? Item.Cargo.NoCargo + "  do grupo: " + Item.Grupo.NoGrupo : "" %>
                        <div class="left-right space-top">
                            <small><%# Item.Motivo!=null ? "Motivo término do mandato: "+Item.Motivo?.NoMotivo : ""%></small>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>Este profissional não possui nenhum histórico</EmptyDataTemplate>
        </box:NGridView>
    </box:NTab>

    <box:NTab runat="server" ID="tabEvento" Text="Eventos" AutoPostBack="true" OnTabChanged="TabChanged">
        <box:NGridView runat="server" ID="grdEvento" ItemType="Creasp.Siplen.EventoParticipante">
            <Columns>
                <asp:BoundField DataField="Evento.DtEvento" HeaderText="Data" DataFormatString="{0:dd/MM/yyyy - HH:mm}" HeaderStyle-Width="150" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="Evento.NoEvento" HeaderText="Evento" />
                <asp:TemplateField HeaderText="Posição" HeaderStyle-Width="120" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <%# Item.IdPessoaTit == IdPessoa ? "Titular" : "Suplente" %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Presença" HeaderStyle-Width="120" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <%# GetPresenca(Item.IdPessoaTit == IdPessoa ? Item.TpPresencaTit : Item.TpPresencaSup) %>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>Este profissional não participou de nenhum evento</EmptyDataTemplate>
        </box:NGridView>
    </box:NTab>

    <box:NTab runat="server" ID="tabLic" Text="Licenças" AutoPostBack="true" OnTabChanged="TabChanged">
        <box:NGridView runat="server" ID="grdLicenca" ItemType="Creasp.Siplen.Licenca">
            <Columns>
                <asp:TemplateField HeaderText="Período" HeaderStyle-Width="250" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <%# Item.GetPeriodo() %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="NoMotivo" HeaderText="Motivo" />
            </Columns>
            <EmptyDataTemplate>Este profissional não possui nenhuma licença</EmptyDataTemplate>
        </box:NGridView>
    </box:NTab>

    <box:NTab runat="server" ID="tabEmail" Text="Emails" AutoPostBack="true" OnTabChanged="TabChanged">
        <box:NGridView runat="server" ID="grdEmail" DataKeyNames="IdEmail" OnRowCommand="grdEmail_RowCommand">
            <Columns>
                <asp:BoundField DataField="DtEnvio" HeaderText="Envio" DataFormatString="{0:dd/MM/yyyy - HH:mm}" HeaderStyle-Width="150" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="NoMensagem" HeaderText="Mensagem" HtmlEncode="false" />
                <box:NButtonField Icon="ccw" ToolTip="Reenviar" CommandName="resend" Theme="Info" OnClientClick="return confirm('Confirma o reenvio deste email?');" />
            </Columns>
            <EmptyDataTemplate>Não foi enviado nenhum email para este profissional</EmptyDataTemplate>
        </box:NGridView>
    </box:NTab>

    <box:NTab runat="server" ID="tabNerp" Text="NERPs" AutoPostBack="true" OnTabChanged="TabChanged" Visible="false">
        <h4>Não implementado</h4>
    </box:NTab>

    <box:NTab runat="server" ID="tabDocs" Text="Documentos" AutoPostBack="true" OnTabChanged="TabChanged">
        <box:NGridView runat="server" ID="grdDocs" ItemType="Creasp.Core.Documento" DataKeyNames="IdDocumento" OnRowCommand="grdDocs_RowCommand">
            <Columns>
                <asp:BoundField DataField="DtCadastro" HeaderText="Data" DataFormatString="{0:d}" HeaderStyle-Width="150" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="Tipo.TpRef" HeaderText="Categoria" HeaderStyle-Width="150" />
                <asp:BoundField DataField="Tipo.NoTipoDoc" HeaderText="Tipo" HeaderStyle-Width="220" />
                <asp:BoundField DataField="NoArquivo" HeaderText="Arquivo" />
                <box:NButtonField Icon="download" CommandName="download" Theme="Info" />
            </Columns>
            <EmptyDataTemplate>Este profissional não possui nenhum documento</EmptyDataTemplate>
        </box:NGridView>
    </box:NTab>

    <script src="../Content/Scripts/sidebar.js"></script>
    <script>selectButton("spl-his"); showReload("siplen");</script>

</asp:Content>