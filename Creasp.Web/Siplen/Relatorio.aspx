﻿<%@ Page Title="Relatórios" Resource="siplen" Language="C#" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();

    private const int FILTROS = 3;
    private ContentPlaceHolder content = null;

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        content = (ContentPlaceHolder)this.Master.FindControl("content");
    }

    protected override void OnPrepareForm()
    {
        cmbRelatorio.Items.Add(new ListItem("(Selecione uma opção abaixo)", ""));
        cmbRelatorio.Items.Add(new ListItem("Plenário", "DWPlenario"));
        cmbRelatorio.Items.Add(new ListItem("Câmaras Especializadas", "DWCamara"));
        cmbRelatorio.Items.Add(new ListItem("Conselheiros", "DWConselheiro"));
        cmbRelatorio.Items.Add(new ListItem("Comissões", "DWComissao"));
        cmbRelatorio.Items.Add(new ListItem("Representações", "DWIndicacao"));
        cmbRelatorio.Items.Add(new ListItem("Inspetores", "DWInspetor"));
        //cmbRelatorio.Items.Add(new ListItem("Profissionais", "DWProfissional"));

        ucPeriodo.Periodo = Periodo.Anos(DateTime.Now.Year, DateTime.Now.Year);
    }

    protected IDWDataSource GetDataSource()
    {
        switch(cmbRelatorio.SelectedValue)
        {
            case "DWPlenario": return new DWPlenarioDataSource();
            case "DWCamara": return new DWCamaraDataSource();
            case "DWConselheiro": return new DWConselheiroDataSource();
            case "DWComissao": return new DWComissaoDataSource();
            case "DWIndicacao": return new DWIndicacaoDataSource();
            case "DWInspetor": return new DWInspetorDataSource();
        }

        return null;
    }

    protected void cmbRelatorio_TextChanged(object sender, EventArgs e)
    {
        var ds = GetDataSource();

        if (ds != null)
        {
            divDescricao.InnerText = ds.Descricao;
            trPeriodo.Visible = ds.PorPeriodo;
            lstColunas.Bind(ds.ListaCampos().Where(x => x.IsColuna).Select(x => new ListItem { Text = x.Descricao, Value = x.Id.ToString(), Selected = x.IsPadrao }), false, true);
        }
        else
        {
            divDescricao.InnerText = "Selecione um relatório";
            lstColunas.Items.Clear();
            trPeriodo.Visible = true;
        }

        for(var i = 1; i <= FILTROS; i++)
        {
            var cmbFiltro = content.FindControl("cmbFiltro" + i) as NDropDownList;
            var txtValor = content.FindControl("txtValor" + i) as NTextBox;
            var cmbValor = content.FindControl("cmbValor" + i) as NDropDownList;

            if(ds == null)
            {
                cmbFiltro.Items.Clear();
            }
            else
            {
                cmbFiltro.Bind(ds.ListaCampos().Where(x => x.IsFiltro).Select(x => new ListItem { Text = x.Nome, Value = x.Id.ToString() }));
            }

            txtValor.Text = "";
            cmbValor.Items.Clear();
            txtValor.Visible = cmbValor.Visible = false;
        }

        lstSelecionados.Items.Clear();
    }

    protected void cmbFiltro_SelectedIndexChanged(object sender, EventArgs e)
    {
        var idx = (sender as DropDownList).ID.Replace("cmbFiltro", "");
        var cmbFiltro = sender as NDropDownList;
        var txtValor = content.FindControl("txtValor" + idx) as NTextBox;
        var cmbValor = content.FindControl("cmbValor" + idx) as NDropDownList;

        if (cmbFiltro.HasValue() == false)
        {
            txtValor.Visible = cmbValor.Visible = false;
            return;
        }

        var ds = GetDataSource();
        var campo = ds.ListaCampos().First(x => x.Id == cmbFiltro.SelectedValue.To<int>());

        if(campo.DropDownItems == null)
        {
            txtValor.Text = "";
            txtValor.MaskType = campo.MaskType;
            txtValor.MaskFormat = campo.MaskFormat;
            txtValor.Visible = true;
            cmbValor.Visible = false;
        }
        else
        {
            cmbValor.Bind(campo.DropDownItems(db, ucPeriodo.Periodo));
            cmbValor.Visible = true;
            txtValor.Visible = false;
        }
    }

    protected void btnExportar_Click(object sender, EventArgs e)
    {
        if (lstSelecionados.Items.Count == 0) throw new NBoxException(lstSelecionados, "Nenhum campo selecionado");

        // executando relatório DW
        using (var dw = GetDataSource())
        {
            var p = ucPeriodo.Periodo;
            var colunas = lstSelecionados.Items.Cast<ListItem>().Select(x => x.Value.To<int>()).ToList();
            var filtros = new List<KeyValuePair<int, string>>();

            for(var i = 1; i <= FILTROS; i++)
            {
                var cmbFiltro = content.FindControl("cmbFiltro" + i) as NDropDownList;
                var txtValor = content.FindControl("txtValor" + i) as NTextBox;
                var cmbValor = content.FindControl("cmbValor" + i) as NDropDownList;

                if(cmbFiltro.HasValue())
                {
                    txtValor.Validate(txtValor.Visible).Required();
                    cmbValor.Validate(cmbValor.Visible).Required();

                    filtros.Add(new KeyValuePair<int, string>(cmbFiltro.SelectedValue.To<int>(), txtValor.Visible ? txtValor.Text : cmbValor.SelectedValue));
                }
            }

            dw.Executa(p, colunas, filtros);
            dw.Download(this);
        }
    }

    protected void Campos_Command(object sender, CommandEventArgs e)
    {
        switch(e.CommandName)
        {
            case "lr":
                lstSelecionados.Items.AddRange(lstColunas.Items.Cast<ListItem>().Where(x => x.Selected).ToArray());
                lstColunas.Items.Cast<ListItem>().Where(x => x.Selected).ToList().ForEach((l, i) => lstColunas.Items.Remove(l));
                break;
            case "rl":
                lstColunas.Items.AddRange(lstSelecionados.Items.Cast<ListItem>().Where(x => x.Selected).ToArray());
                lstSelecionados.Items.Cast<ListItem>().Where(x => x.Selected).ToList().ForEach((l, i) => lstSelecionados.Items.Remove(l));
                break;
            case "up":
                for (var i = 0; i < lstSelecionados.Items.Count; i++)
                {
                    if (lstSelecionados.Items[i].Selected)
                    {
                        if (i > 0 && !lstSelecionados.Items[i - 1].Selected)
                        {
                            var bottom = lstSelecionados.Items[i];
                            lstSelecionados.Items.Remove(bottom);
                            lstSelecionados.Items.Insert(i - 1, bottom);
                            lstSelecionados.Items[i - 1].Selected = true;
                        }
                    }
                }
                break;
            case "dn":
                var startindex = lstSelecionados.Items.Count -1;
                for (var i = startindex; i > -1; i--)
                {
                    if (lstSelecionados.Items[i].Selected)
                    {
                        if (i < startindex && !lstSelecionados.Items[i + 1].Selected)
                        {
                            var bottom = lstSelecionados.Items[i];
                            lstSelecionados.Items.Remove(bottom);
                            lstSelecionados.Items.Insert(i + 1, bottom);
                            lstSelecionados.Items[i + 1].Selected = true;
                        }
                    }
                }
                break;
        }

    }

</script>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <div class="mt-30 c-primary-blue">
        <span>Relatórios</span>
        <box:NButton runat="server" ID="btnExportar" Text="Exportar" Icon="file-excel" Theme="Default" CssClass="right" OnClick="btnExportar_Click" />
    </div>

    <div class="form-div">
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Relatório</span></div>
                <box:NDropDownList runat="server" ID="cmbRelatorio" AutoPostBack="true" OnTextChanged="cmbRelatorio_TextChanged" Width="100%" />
                <div class="help" runat="server" id="divDescricao">Selecione um relatório</div>
            </div>
        </div>
        <div class="sub-form-row" runat="server" id="trPeriodo">
            <div class="div-sub-form">
                <div><span>Período</span></div>
                <uc:Periodo runat="server" ID="ucPeriodo" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Filtros</span></div>
                <box:NDropDownList runat="server" ID="cmbFiltro1" Width="40%" AutoPostBack="true" OnSelectedIndexChanged="cmbFiltro_SelectedIndexChanged" />
                <box:NTextBox runat="server" ID="txtValor1" Width="40%" Visible="false" TextCapitalize="Upper" />
                <box:NDropDownList runat="server" ID="cmbValor1" Width="40%" Visible="false" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <box:NDropDownList runat="server" ID="cmbFiltro2" Width="40%" AutoPostBack="true" OnSelectedIndexChanged="cmbFiltro_SelectedIndexChanged" />
                <box:NTextBox runat="server" ID="txtValor2" Width="40%" Visible="false" TextCapitalize="Upper" />
                <box:NDropDownList runat="server" ID="cmbValor2" Width="40%" Visible="false" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <box:NDropDownList runat="server" ID="cmbFiltro3" Width="40%" AutoPostBack="true" OnSelectedIndexChanged="cmbFiltro_SelectedIndexChanged" />
                <box:NTextBox runat="server" ID="txtValor3" Width="40%" Visible="false" TextCapitalize="Upper" />
                <box:NDropDownList runat="server" ID="cmbValor3" Width="40%" Visible="false" />
            </div>
        </div>
        <div class="sub-form-row flex">
            <div class="div-sub-form">
                <div><span>Disponíveis</span></div>
                <asp:ListBox runat="server" CssClass="n-select" ID="lstColunas" Rows="15" SelectionMode="Multiple" Width="100%" Height="300px" />
                <div class="help">Selecione vários campos ao mesmo tempo mantendo pressionada a tecla Control (Ctrl)</div>
            </div>
            <div class="div-sub-form" style="width: 46.5px; margin: 17px">
                <box:NButton runat="server" Icon="right-open" Theme="Default" CommandName="lr" OnCommand="Campos_Command" ToolTip="Mover para selecionados" /><br />
                <box:NButton runat="server" Icon="left-open" Theme="Default" CssClass="space-top" CommandName="rl" OnCommand="Campos_Command" ToolTip="Remover de selecionados" /><br />
            </div>
            <div class="div-sub-form">
                <div><span>Selecionados</span></div>
                <asp:ListBox runat="server" CssClass="n-select" ID="lstSelecionados" Rows="15" SelectionMode="Multiple" Width="100%" Height="300px" />
            </div>
            <div class="div-sub-form" style="width: 46.5px; margin-left: 17px">
                <box:NButton runat="server" Icon="up-open" Theme="Default" CommandName="up" OnCommand="Campos_Command" ToolTip="Mover para cima" /><br />
                <box:NButton runat="server" Icon="down-open" Theme="Default" CssClass="space-top" CommandName="dn" OnCommand="Campos_Command" ToolTip="Mover para baixo" />
            </div>
        </div>
        
    </div>

    <script src="../Content/Scripts/sidebar.js"></script>
    <script>selectButton("spl-rel"); showReload("siplen");</script>

</asp:Content>