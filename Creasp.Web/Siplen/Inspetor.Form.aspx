﻿<%@ Page Title="Indicação para Inspetor" Resource="siplen" Language="C#" %>
<script runat="server">

    private CreaspContext db = new CreaspContext();

    private int? IdMandato => Request.QueryString["IdMandato"].To<int?>();

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        txtInspetoria.RegisterDataSource((q) =>
            db.Query<Inspetoria>()
                .Include(x => x.Unidade)
                .WherePeriodo(Periodo.Hoje)
                .Where(x => x.NoInspetoria.StartsWith(q))
                .Limit(10)
                .ProjectTo(x => new { x.IdInspetoria, x.NoInspetoria, NoUnidade = x.Unidade.NoUnidade }));

        RegisterResource("siplen.inspetor").Disabled(btnSalvar, frm);
    }

    protected override void OnPrepareForm()
    {
        cmbMotivo.Bind(db.Query<MotivoAfastamento>().Where(x => x.FgAtivo).OrderBy(x => x.NoMotivo).ToEnumerable());
        cmbMandato.Items.AddRange(InspetorService.Mandatos.Select(x => new ListItem(string.Format("{0} - {1}", x, x + 2), x.ToString())).ToArray());
        cmbMandato.SelectedValue = InspetorService.MandatoAtual.ToString();

        cmbCargo.Items.Add(new ListItem("Inspetor", CdCargo.Inspetor));
        cmbCargo.Items.Add(new ListItem("Inspetor Chefe", CdCargo.InspetorChefe));
        cmbCargo.Items.Add(new ListItem("Inspetor Especial", CdCargo.InspetorEspecial));

        cmbCamara.Bind(db.Camaras.ListaCamaras(Periodo.Hoje));

        if(IdMandato.HasValue)
        {
            MontaTela();
        }

        ucDocs.Visible = IdMandato.HasValue;

        SetDefaultButton(btnSalvar);
        this.BackTo("Inspetor.aspx");
    }

    protected void MontaTela()
    {
        var insp = db.Inspetores.BuscaInspetor(IdMandato.Value);

        cmbMandato.SelectedValue = insp.AnoIni.ToString();
        cmbCargo.SelectedValue = insp.CdCargo;
        txtInspetoria.Text = insp.Inspetoria.NoInspetoria;
        txtInspetoria.Value = insp.IdInspetoria.ToString();
        txtNroProcesso.Text = insp.Inspetoria.NrProcesso == null ? "" : insp.Inspetoria.NrProcesso.ToString();
        cmbCamara.SelectedValue = insp.CdCamara.ToString();
        ucPessoa.LoadPessoa(insp.IdPessoa);
        ucPeriodo.Periodo = insp.GetPeriodo();
        cmbMotivo.SelectedValue = insp.CdMotivo.To<string>();

        cmbMandato.Enabled = ucPessoa.Enabled = false;

        ucDocs.MontaTela(TpRefDoc.Inspetor, IdMandato.Value, insp.IdPessoa);

        if(insp.GetPeriodo().IsVazio)
        {
            this.SetPageTitle(insp.Pessoa.NoPessoa, "Indicação");
        }
        else
        {
            this.SetPageTitle(insp.Pessoa.NoPessoa.Captalize(), "Inspetor");
            trVigencia.Visible = trMotivo.Visible = true;
        }

        this.Audit(insp, insp.IdMandato);
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        if(IdMandato.HasValue)
        {
            db.Inspetores.Altera(
                IdMandato.Value,
                cmbCargo.SelectedValue,
                txtInspetoria.Value.To<int>(),
                cmbCamara.SelectedValue.To<int>(),
                ucPeriodo.Periodo,
                cmbMotivo.SelectedValue.To<int?>(),
                txtNoHistorico.Text);

            ShowInfoBox(ucPeriodo.Periodo.IsVazio ?
                "Indicação alterada com sucesso" : "Mandato alterado com sucesso");
        }
        else
        {
            var ind = db.Inspetores.Inclui(
                txtInspetoria.Value.To<int>(),
                ucPessoa.IdPessoa.Value,
                cmbCargo.SelectedValue,
                cmbCamara.SelectedValue.To<int>(),
                cmbMandato.SelectedValue.To<int>(),
                Periodo.Vazio,
                txtNoHistorico.Text);

            ShowInfoBox("Nova indicação gravada com sucesso");

            Redirect("Inspetor.aspx?IdIndicacao=" + ind, true);
        }
    }

    protected void btnExcluir_Click(object sender, EventArgs e)
    {
        //db.Indicacoes.ExcluiIndicacao(IdIndicacao.Value);
        ShowInfoBox("Indicação excluida com sucesso");
        Redirect("PlenFut.aspx", true);
    }

    protected void txtInspetoria_TextChanged(object sender, EventArgs e)
    {
        var inspetoria = db.Query<Inspetoria>()
                            .Where(x => x.IdInspetoria == txtInspetoria.Value.To<int>())
                            .FirstOrDefault();

        txtNroProcesso.Text = inspetoria?.NrProcesso;
            
    }
</script>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <box:NButton runat="server" ID="btnSalvar" Theme="Primary" Icon="ok" CssClass="right margin-top margin-bottom" Width="150" Text="Salvar" OnClick="btnSalvar_Click" />

    <div class="form-div" runat="server" id="frm">
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Mandato</span></div>
                <box:NDropDownList runat="server" ID="cmbMandato" Width="200" Required="true" autofocus />
            </div>
            <div class="div-sub-form">
                <div><span>Cargo</span></div>
                <box:NDropDownList runat="server" ID="cmbCargo" Width="200" Required="true" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Município</span></div>
                <box:NAutocomplete runat="server" ID="txtInspetoria" Required="true" Width="100%" TextCapitalize="Upper"
                    DataValueField="IdInspetoria" DataTextField="NoInspetoria" OnTextChanged="txtInspetoria_TextChanged"
                    AutoPostBack="true" Template="{NoInspetoria} - {NoUnidade}" />
            </div>
            <div class="div-sub-form ml-10">
                <div><span>Nro. Processo</span></div>
                <box:NTextBox runat="server" ID="txtNroProcesso" Enabled="false" Width="100%" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Câmara</span></div>
                <box:NDropDownList runat="server" ID="cmbCamara" Width="100%" Required="true" />
            </div>
            <div class="div-sub-form ml-10 form-uc">
                <div><span>Profissional</span></div>
                <uc:Pessoa runat="server" ID="ucPessoa" IsProfissional="true" />
            </div>
        </div>
        <div class="sub-form-row" runat="server" id="trVigencia" visible="false">
            <div class="div-sub-form">
                <div><span>Vigência</span></div>
                <uc:Periodo runat="server" id="ucPeriodo" ShowTextBox="true" Required="true" />
                <div class="help">Quando o período não terminar no último dia do mandato selecione o motivo</div>
            </div>
        </div>
        <div class="sub-form-row" runat="server" id="trMotivo" visible="false">
            <div class="div-sub-form">
                <div><span>Motivo</span></div>
                <box:NDropDownList runat="server" ID="cmbMotivo" Width="100%" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Observações</span></div>
                <box:NTextBox runat="server" ID="txtNoHistorico" MaxLength="2000" Width="100%" TextMode="MultiLine" TextCapitalize="Upper" />
            </div>
        </div>
    </div>

    <uc:Documento runat="server" ID="ucDocs" />

    <script src="../Content/Scripts/sidebar.js"></script>
    <script>selectButton("spl-inspetores"); showReload("siplen");</script>

</asp:Content>