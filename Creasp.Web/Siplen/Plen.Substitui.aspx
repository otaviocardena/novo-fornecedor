﻿<%@ Page Title="Substituir conselheiro" Resource="siplen" Language="C#" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();

    private int IdMandato => Request.QueryString["IdMandato"].To<int>();
    private string CdCargoReq => Request.QueryString["CdCargo"];

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        RegisterResource("siplen.plen").ThrowIfNoAccess();
    }

    protected override void OnPrepareForm()
    {
        var tit = db.Query<Mandato>()
            .Include(x => x.Pessoa)
            .Include(x => x.Cargo)
            .Include(x => x.Camara)
            .Include(x => x.Entidade)
            .Where(x => x.TpMandato == TpMandato.Conselheiro)
            .Where(x => x.IdMandato == IdMandato)
            .Single();

        var mandato = tit;

        if(CdCargoReq == CdCargo.Suplente)
        {
            var sup = db.Query<Mandato>()
                .Include(x => x.Pessoa)
                .Include(x => x.Cargo)
                .Where(x => x.IdMandatoTit == IdMandato)
                .OrderByDescending(x => x.DtFim)
                .FirstOrDefault();

            mandato = sup;
        }

        ltrCargo.Text = mandato?.Cargo.NoCargo ?? "Suplente";
        ltrNovoCargo.Text = mandato?.Cargo.NoCargo.ToLower() ?? "suplente";
        ltrNoPessoa.Text = mandato?.Pessoa.NoPessoa ?? "[SEM INDICAÇÃO]";
        ltrNrCreasp.Text = mandato?.Pessoa.NrCreasp ?? "-";
        lblNoTitulo.Text = mandato?.Pessoa.NoTituloAbr;
        lblNoTitulo.ToolTip = mandato?.Pessoa.NoTitulo;
        lblNoMandato.Text = (mandato?.GetPeriodo() ?? Periodo.Vazio).ToString(false) + " (" + tit.AnoIni + " - " + tit.AnoFim + ")";
        lblNoCamara.Text = tit.Camara.NoCamara;
        lblEntidade.Text = tit.Entidade.NoEntidade;
        ltrDtFim.Text = string.Format("{0:d}", CdCargoReq == CdCargo.Titular ? new DateTime(tit.AnoFim, 12, 31) : tit.DtFim);
        txtDtIni.SetText(mandato?.GetPeriodo().Fim.AddDays(1) ?? tit.DtIni);

        ucPessoa.Focus();
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        var periodo = new Periodo(txtDtIni.Text.To<DateTime>(), ltrDtFim.Text.To<DateTime>());
        var idTit = db.Conselheiros.SubstituiConselheiro(IdMandato, CdCargoReq, ucPessoa.IdPessoa.Value, periodo);
        ShowInfoBox("Conselheiro substituido com sucesso");
        ClosePopup(idTit.ToString());
    }

</script>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <div class="form-div">
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span><asp:Literal runat="server" ID="ltrCargo" /></span></div>
                <span class="field left-right">
                    <span>
                        <asp:Literal runat="server" ID="ltrNoPessoa" />
                        <small>&nbsp;&nbsp;(<asp:Literal runat="server" ID="ltrNrCreasp" />)</small>
                    </span>
                    <box:NLabel runat="server" ID="lblNoTitulo" Theme="Info" CssClass="no-titulo" />
                </span>
            </div>
        </div>
       
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Mandato</span></div>
                <box:NLabel runat="server" ID="lblNoMandato" CssClass="field" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Câmara</span></div>
                <box:NLabel runat="server" ID="lblNoCamara" CssClass="field" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Entidade</span></div>
                <box:NLabel runat="server" ID="lblEntidade" CssClass="field" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Novo <asp:Literal runat="server" ID="ltrNovoCargo" /></span></div>
                <uc:Pessoa runat="server" ID="ucPessoa" IsProfissional="true" Required="true" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Período</span></div>
                <box:NTextBox runat="server" ID="txtDtIni" MaskType="Date" Required="true" /> a <asp:Literal runat="server" ID="ltrDtFim" />
                <div class="help">A data de início deve ser maior que a data de término do mandato atual</div>
            </div>
        </div>
    </div>

    <div class="help">A substituição de conselheiro deve ser feita somente em caso de encerramento do mandato por motivos permanentes (falecimento, renúncia entre outros). Por motivos temporários, utilize a opção "Licenças".</div>

    <box:NButton runat="server" ID="btnSalvar" Icon="ok" Text="Confirmar" CssClass="right" Theme="Primary" OnClick="btnSalvar_Click" />

    <script src="../Content/Scripts/sidebar.js"></script>
    <script>selectButton("spl-plen"); showReload("siplen");</script>

</asp:Content>