﻿using System;
using System.Collections.Generic;
using Creasp.Core;
using NPoco;

namespace Creasp.Siplen
{
    /// <summary>
    /// Classe que faz mapeamento dos centros de custo padrão para o cadastro de eventos
    /// </summary>
    [PrimaryKey("IdEventoCentroCusto", AutoIncrement = true), Serializable]
    public class EventoCentroCusto
    {
        public int IdEventoCentroCusto { get; set; }

        public string TpMandato { get; set; }
        public int? IdRef { get; set; }
        public string NrCentroCusto { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "NrCentroCusto", ReferenceMemberName = "NrCentroCusto")]
        public CentroCusto CentroCusto { get; set; }

        /// <summary>
        /// Categoria/TpMandato
        /// </summary>
        [Ignore]
        public string NoCategoria { get; set; }

        /// <summary>
        /// Utilizado para exibir ao que se refere um centro de custo de um evento
        /// </summary>
        [Ignore]
        public string NoRef { get; set; }
    }
}