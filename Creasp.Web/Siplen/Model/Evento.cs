﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using Creasp.Core;

namespace Creasp.Siplen
{
    [PrimaryKey("IdEvento", AutoIncrement = true)]
    public class Evento: IAuditoria
    {
        public int IdEvento { get; set; }

        public int NrAno { get; set; }
        public string NoEvento { get; set; }
        public DateTime DtEvento { get; set; }
        public DateTime DtEventoFim { get; set; }
        public string NrCep { get; set; }
        public string NoLocal { get; set; }
        public bool FgContaFalta { get; set; }
        public string NoHistorico { get; set; }
        public bool FgPermiteDiariaDup { get; set; }

        #region IAuditoria
        public DateTime DtCadastro { get; set; } = DateTime.Now;
        public DateTime? DtAlteracao { get; set; }
        public string LoginCadastro { get; set; }
        public string LoginAlteracao { get; set; }
        #endregion

        [Reference(ReferenceType.Many, ColumnName = "IdEvento", ReferenceMemberName = "IdEvento")]
        public List<EventoParticipante> Participantes { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "NrCep", ReferenceMemberName = "NrCep")]
        public Cep Cep { get; set; }

        [Ignore]
        public Documento Documento { get; set; }

        /// <summary>
        /// Retorna se o evento é fora do horario comercial : Inicio menor que 8h, Final após 18h ou mais de um dia
        /// </summary>
        [Ignore]
        public bool IsForaHorarioComercial => 
            DtEvento.Hour < 8 || 
            DtEventoFim.Hour > 18 || 
            DtEvento.Day != DtEventoFim.Day;

        /// <summary>
        /// Exibe de forma formatada a data/hora do evento
        /// </summary>
        [Ignore]
        public string DtEventoFmt => DtEvento.ToString("d") + " - " +
                DtEvento.ToString("HH:mm") + " até " +
                (DtEvento.Date != DtEventoFim.Date? DtEventoFim.ToString("d") + " - " : "") +
                DtEventoFim.ToString("HH:mm");
    }
}