﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using Creasp.Core;

namespace Creasp.Siplen
{
    [PrimaryKey("IdIndicacao", AutoIncrement = true)]
    public class Indicacao : IAuditoria
    {
        public int IdIndicacao { get; set; }

        public int AnoIni { get; set; }
        public int AnoFim { get; set; }
        public int CdCamara { get; set; }
        public int IdEntidade { get; set; }
        public int? IdPessoaTit { get; set; }
        public int? IdInspetoria { get; set; }
        public int? IdPessoaSup { get; set; }
        public DateTime? DtPosseTit { get; set; }
        public DateTime? DtPosseSup { get; set; }
        public int? IdMandatoTit { get; set; }
        public int? IdMandatoSup { get; set; }
        public string NoHistorico { get; set; }
        

        #region IAuditoria
        public DateTime DtCadastro { get; set; } = DateTime.Now;
        public DateTime? DtAlteracao { get; set; }
        public string LoginCadastro { get; set; }
        public string LoginAlteracao { get; set; }
        #endregion

        [Reference(ReferenceType.OneToOne, ColumnName = "CdCamara", ReferenceMemberName = "CdCamara")]
        public Camara Camara { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdInspetoria", ReferenceMemberName = "IdInspetoria")]
        public virtual Inspetoria Inspetoria { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdEntidade", ReferenceMemberName = "IdEntidade")]
        public Entidade Entidade { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdPessoaTit", ReferenceMemberName = "IdPessoa")]
        public Pessoa Titular { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdPessoaSup", ReferenceMemberName = "IdPessoa")]
        public Pessoa Suplente { get; set; }

        /// <summary>
        /// Mensagem que descreve a situação da indicação
        /// </summary>
        [Ignore]
        public string MensagemTit { get; set; }
        [Ignore]
        public string MensagemSup { get; set; }
    }
}