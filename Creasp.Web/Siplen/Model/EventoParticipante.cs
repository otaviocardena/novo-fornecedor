﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using Creasp.Core;

namespace Creasp.Siplen
{
    [PrimaryKey("IdParticipante", AutoIncrement = true)]
    public class EventoParticipante : IAuditoria
    {
        public int IdParticipante { get; set; }

        public int IdEvento { get; set; }
        public int IdCentroCusto { get; set; }
        public string TpMandato { get; set; }
        public int? IdPessoaTit { get; set; }
        public int? IdPessoaSup { get; set; }
        public int NrOrdem { get; set; }
        public int? IdMandatoTit { get; set; }
        public int? IdMandatoSup { get; set; }
        public string TpPresencaTit { get; set; }
        public string TpPresencaSup { get; set; }

        #region IAuditoria
        public DateTime DtCadastro { get; set; } = DateTime.Now;
        public DateTime? DtAlteracao { get; set; }
        public string LoginCadastro { get; set; }
        public string LoginAlteracao { get; set; }
        #endregion

        [Reference(ReferenceType.OneToOne, ColumnName = "IdEvento", ReferenceMemberName = "IdEvento")]
        public Evento Evento { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdPessoaTit", ReferenceMemberName = "IdPessoa")]
        public Pessoa Titular { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdPessoaSup", ReferenceMemberName = "IdPessoa")]
        public Pessoa Suplente { get; set; }

        [Reference(ReferenceType.OneToOne, ColumnName = "IdCentroCusto", ReferenceMemberName = "IdCentroCusto")]
        public CentroCusto CentroCusto { get; set; }

        [Ignore]
        public Periodo LicencaTit { get; set; } = Periodo.Vazio;
        [Ignore]
        public Periodo LicencaSup { get; set; } = Periodo.Vazio;

        [Ignore]
        public int NrFaltasTit { get; set; } = 0;
        [Ignore]
        public int NrFaltasSup { get; set; } = 0;

        [Ignore]
        public List<Nerp.Nerp> NerpsTit { get; set; } = new List<Nerp.Nerp>();

        [Ignore]
        public List<Nerp.Nerp> NerpsSup { get; set; } = new List<Nerp.Nerp>();
    }
}