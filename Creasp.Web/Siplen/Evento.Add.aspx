﻿<%@ Page Title="Adicionar participantes" Resource="siplen" Language="C#" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();

    private int IdEvento => Request.QueryString["IdEvento"].To<int>();

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        grdList.RegisterDataSource(() => db.Eventos.ListaConvidados(IdEvento, cmbTpMandato.SelectedValue, txtNoPessoa.Text.To<string>(),
            cmbCamara.SelectedValue.To<int?>(),
            cmbComissao.SelectedValue.To<int?>(),
            cmbGrupo.SelectedValue.To<int?>(),
            txtInspetoria.Value.To<int?>(),
            cmbCargo.SelectedValue.To<string>(),
            cmbGRE.SelectedValue.To<int?>()));

        txtInspetoria.RegisterDataSource((q) =>
            db.Query<Inspetoria>()
                .Include(x => x.Unidade)
                .WherePeriodo(Periodo.Hoje)
                .Where(x => x.NoInspetoria.StartsWith(q.ToLike()))
                .Limit(10)
                .ProjectTo(x => new { x.IdInspetoria, x.NoInspetoria, NoUnidade = x.Unidade.NoUnidade }));

        RegisterResource("siplen.evento").ThrowIfNoAccess();
    }

    protected override void OnPrepareForm()
    {
        var e = db.Eventos.BuscaEvento(IdEvento, false);

        cmbTpMandato.Items.Add(new ListItem("(Selecione uma opção abaixo)", ""));
        cmbTpMandato.Items.Add(new ListItem("Conselheiros", TpMandato.Conselheiro));
        cmbTpMandato.Items.Add(new ListItem("Coordenadores de câmaras", TpMandato.Camara));
        cmbTpMandato.Items.Add(new ListItem("Representantes", TpMandato.Representante));
        cmbTpMandato.Items.Add(new ListItem("Indicados", TpMandato.Indicacao));
        cmbTpMandato.Items.Add(new ListItem("Diretor", TpMandato.Diretor));
        cmbTpMandato.Items.Add(new ListItem("Comissões", TpMandato.Comissao));
        cmbTpMandato.Items.Add(new ListItem("Grupos de trabalho", TpMandato.Grupo));
        cmbTpMandato.Items.Add(new ListItem("Inspetores", TpMandato.Inspetor));
        cmbTpMandato.Items.Add(new ListItem("Funcionarios", TpMandato.Funcionario));
        cmbTpMandato.Items.Add(new ListItem("Outras pessoas", TpMandato.Outro));

        cmbCamara.Bind(db.Camaras.ListaCamaras(Periodo.UmDia(e.DtEvento)));
        cmbComissao.Bind(db.Comissoes.ListaComissoes(Periodo.UmDia(e.DtEvento), null));
        cmbGrupo.Bind(db.Grupos.ListaGrupos(Periodo.UmDia(e.DtEvento), null));
        cmbGRE.Bind(db.Unidades.ListaUnidades(TpUnidade.DeptoRegional, null).Select(x => new { x.CdUnidade, x.NoUnidade }));

        cmbCargo.Items.Add(new ListItem("(Selecione uma opção abaixo)", ""));
        cmbCargo.Items.Add(new ListItem("Inspetor", CdCargo.Inspetor));
        cmbCargo.Items.Add(new ListItem("Inspetor chefe", CdCargo.InspetorChefe));
        cmbCargo.Items.Add(new ListItem("Inspetor especial", CdCargo.InspetorEspecial));

        SetDefaultButton(btnPesquisar);
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if(grdList.SelectedDataKeys.Count() == 0) throw new NBoxException("Nenhuma pessoa selecionada");

        // busca o centro de custo padrão conforme os filtros selecionados na tela
        var tpMandato = cmbTpMandato.SelectedValue.ToString();
        var idRef = tpMandato == TpMandato.Conselheiro && cmbCamara.HasValue() ? cmbCamara.SelectedValue.To<int>() :
            tpMandato == TpMandato.Comissao ? cmbComissao.SelectedValue.To<int>() :
            tpMandato == TpMandato.Grupo ? cmbGrupo.SelectedValue.To<int>() : (int?)null;

        var cc = db.Eventos.BuscaCentroCustoPadrao(tpMandato, idRef);

        ucCentroCusto.LoadCentroCusto(cc?.IdCentroCusto);

        pnlCentroCusto.Open(btnAddCentroCusto, ucCentroCusto);
    }

    protected void btnPesquisar_Click(object sender, EventArgs e)
    {
        cmbTpMandato.Validate().Required();
        cmbComissao.Validate(cmbTpMandato.SelectedValue == TpMandato.Comissao).Required();
        cmbGrupo.Validate(cmbTpMandato.SelectedValue == TpMandato.Grupo).Required();
        txtNoPessoa.Validate(cmbTpMandato.SelectedValue == TpMandato.Outro).Required();

        grdList.Visible = true;
        grdList.DataBind();
    }

    protected void cmbTpMandato_SelectedIndexChanged(object sender, EventArgs e)
    {
        var tp = cmbTpMandato.SelectedValue.ToString();

        trCamara.Visible = tp == TpMandato.Conselheiro || tp == TpMandato.Camara || tp == TpMandato.Representante;
        trComisso.Visible = tp == TpMandato.Comissao;
        trGrupo.Visible = tp == TpMandato.Grupo;
        trInspetoria.Visible = tp == TpMandato.Inspetor;
        trCargo.Visible = tp == TpMandato.Inspetor;
        trGRE.Visible = tp == TpMandato.Inspetor;

        grdList.Columns[2].Visible = tp == TpMandato.Conselheiro || tp ==TpMandato.Comissao;

        grdList.Visible = false;
        grdList.Clear();
    }

    protected void btnAddCentroCusto_Click(object sender, EventArgs e)
    {
        if (ucCentroCusto.IdCentroCusto == null) throw new NBoxException("Centro de custo obrigatório");

        var lista = grdList.SelectedDataKeys.Select(x => new EventoConvidadoDTO
        {
            IdPessoaTit = x["IdPessoaTit"].To<int>(),
            IdMandatoTit = x["IdMandatoTit"].To<int?>(),
            FgLicencaTit = x["FgLicencaTit"].To<bool>(),
            IdPessoaSup = x["IdPessoaSup"].To<int?>(),
            IdMandatoSup = x["IdMandatoSup"].To<int?>(),
            FgLicencaSup = x["FgLicencaSup"].To<bool>(),
            NrOrdem = x["NrOrdem"].To<int>()
        }).ToList();

        var count = db.Eventos.AdicionaMembros(IdEvento, ucCentroCusto.IdCentroCusto.Value, cmbTpMandato.SelectedValue, lista);

        ShowInfoBox($"{count} pessoas foram adicionadas neste evento");

        grdList.Visible = true;
        grdList.DataBind();
        pnlCentroCusto.Close();
    }

    protected void btnCadastroPessoa_Click(object sender, EventArgs e)
    {
        ucPessoa.InsertPessoa();
    }

    protected void ucPessoa_PessoaChanged(object sender, EventArgs e)
    {
        if (ucPessoa.IdPessoa != null)
        {
            txtNoPessoa.Text = ucPessoa.Pessoa.NoPessoa;
            cmbTpMandato.SelectedValue = TpMandato.Outro;
            cmbTpMandato_SelectedIndexChanged(null, null);
            btnPesquisar_Click(null, null);
        }
    }

</script>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <uc:Pessoa runat="server" id="ucPessoa" InsertOnly="true" OnPessoaChanged="ucPessoa_PessoaChanged" />

    <div class="mt-30 c-primary-blue">
        <span>Adicionar Participantes</span><box:NLinkButton runat="server" ID="btnCadastroPessoa" CssClass="right icon-right" OnClick="btnCadastroPessoa_Click"><i class="fas fa-user-plus"></i><span>Cadastrar</span></box:NLinkButton>
    </div>

    <div class="form-div">
        <div class="sub-form-row margin-top-0">
            <div class="div-sub-form">
                <div><span>Buscar</span></div>
                <div class="input-group">
                    <box:NTextBox runat="server" ID="btnBuscar" TextCapitalize="Upper" Width="100%" CssClass="radius-right-0 no-border-right"/>
                    <box:NButton runat="server" ID="btnPesquisar" Icon="search" Width="61px" Theme="Default" Font-Size="14pt" CssClass="btn-radius" OnClick="btnPesquisar_Click" CausesValidation="false" />
                </div>
            </div>
        </div>
        <div class="sub-form-row margin-top-0">
            <div class="div-sub-form">
                <div><span>Tipo de mandato</span></div>
                <box:NDropDownList runat="server" ID="cmbTpMandato" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="cmbTpMandato_SelectedIndexChanged" />
            </div>
            <div class="div-sub-form margin-left" runat="server" id="trCamara" visible="false">
                <div><span>Câmara</span></div>
                <box:NDropDownList runat="server" ID="cmbCamara" Width="100%" />
            </div>
            <div class="div-sub-form margin-left">
                <div><span>Nome</span></div>
                <box:NTextBox runat="server" ID="txtNoPessoa" Width="100%" TextCapitalize="Upper" />
            </div>
        </div>
        <div class="sub-form-row margin-top-0">
            <div class="div-sub-form" runat="server" id="trComisso" visible="false">
                <div><span>Comissões</span></div>
                <box:NDropDownList runat="server" ID="cmbComissao" Width="100%" />
            </div>
            <div class="div-sub-form" runat="server" id="trGrupo" visible="false">
                <div><span>Grupos</span></div>
                <box:NDropDownList runat="server" ID="cmbGrupo" Width="100%" />
            </div>
            <div class="div-sub-form" runat="server" id="trGRE" visible="false">
                <div><span>Gerência</span></div>
                <box:NDropDownList runat="server" ID="cmbGRE" Width="100%" AutoPostBack="true" />
            </div>
            <div class="div-sub-form margin-left" runat="server" id="trInspetoria" visible="false">
                <div><span>Município</span></div>
                <box:NAutocomplete runat="server" ID="txtInspetoria" Width="100%" TextCapitalize="Upper" DataValueField="IdInspetoria" DataTextField="NoInspetoria" Template="{NoInspetoria} - {NoUnidade}" />
            </div>
            <div class="div-sub-form margin-left" runat="server" id="trCargo" visible="false">
                <div><span>Cargo</span></div>
                <box:NDropDownList runat="server" ID="cmbCargo" Width="100%" />
            </div>
        </div>
    </div>

    <div class="margin-top">
        <box:NGridView runat="server" ID="grdList" ItemType="Creasp.Siplen.EventoConvidadoDTO" DataKeyNames="IdPessoaTit,IdPessoaSup,IdMandatoTit,IdMandatoSup,FgLicencaTit,FgLicencaSup,NrOrdem">
            <Columns>
                <asp:TemplateField HeaderText="Titular" HeaderStyle-Width="50%">
                    <ItemTemplate>
                        <div class="left-right">
                            <%# Item.NoPessoaTit %>
                            <div>
                                <box:NLabel runat="server" Visible=<%# Item.FgLicencaTit %> Text="Licença" Theme="Danger" ToolTip="Esta pessoa está de licença no dia deste evento" />
                                <box:NLabel runat="server" Theme="Info" Visible=<%# Item.NoTituloTit != null %> Text=<%# Item.NoTituloTit %> CssClass="no-titulo" ToolTip=<%# Item.NoTituloTit %> />
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Suplente" HeaderStyle-Width="50%">
                    <ItemTemplate>
                        <div class="left-right">
                            <%# Item.NoPessoaSup %>
                            <div>
                                <box:NLabel runat="server" Visible=<%# Item.FgLicencaSup %> Text="Licença" Theme="Danger" ToolTip="Esta pessoa está de licença no dia deste evento" />
                                <box:NLabel runat="server" Theme="Info" Visible=<%# Item.NoTituloSup != null %> Text=<%# Item.NoTituloSup %> CssClass="no-titulo" ToolTip=<%# Item.NoTituloSup %> />
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <box:NCheckBoxField EnabledExpression="!FgConvidado" />
            </Columns>
            <EmptyDataTemplate>Nenhuma pessoa encontrada para o filtro</EmptyDataTemplate>
        </box:NGridView>
    </div>

    <box:NPopup runat="server" ID="pnlCentroCusto" Title="Adicionar participante" Width="750">
        <box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnAddCentroCusto" Icon="ok" Text="Confirmar" Theme="Primary" OnClick="btnAddCentroCusto_Click" ValidationGroup="cc" />
        </box:NToolBar>
        
        <div class="mt-30 c-primary-blue">
            <span>Definir centro de custo</span>
        </div>

        <div class="form-div">
            <div class="sub-form-row margin-top-0">
                <div class="div-sub-form">
                    <div><span>Centro de custo</span></div>
                    <uc:CentroCusto runat="server" ID="ucCentroCusto" />
                    <div class="help">Informe o centro de custo referente aos participantes selecionados da tela anterior. Este centro de custo será utilizado na emissão da NERP de ressarcimento/adiantamento.</div>
                </div>
            </div>
        </div>
    </box:NPopup>

    <box:NButton runat="server" ID="btnAdd" Icon="plus" Text="Adicionar" Theme="Primary" CssClass="right" OnClick="btnAdd_Click" />

    <script src="../Content/Scripts/sidebar.js"></script>
    <script>selectButton("nerp-event"); showReload("nerp");</script>

</asp:Content>