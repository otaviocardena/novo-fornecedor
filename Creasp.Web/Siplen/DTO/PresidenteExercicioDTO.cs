﻿using Creasp.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Creasp.Siplen
{
    public class PresidenteExercicioDTO: IPeriodo
    {
        public Pessoa Pessoa { get; set; }
        public DateTime DtIni { get; set; }
        public DateTime DtFim { get; set; }
    }
}
