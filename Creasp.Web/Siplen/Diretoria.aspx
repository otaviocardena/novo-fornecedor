﻿<%@ Page Title="Diretoria" Resource="siplen" Language="C#" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();
    protected PresidenteExercicioDTO presidenteExercicio = new  PresidenteExercicioDTO();

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        grdDir.RegisterDataSource(() => db.Diretoria.ListaDiretores(
            ucPeriodo.Periodo, false,
            cmbCargo.SelectedValue.To<string>()));

        RegisterClosePopup((s, e) => grdDir.DataBind());

        RegisterResource("siplen.diretoria").Disabled(btnIncluir).GridColumns(grdDir, "alt", "del");
    }

    protected override void OnPrepareForm()
    {
        var cargos = db.Query<Cargo>().WherePeriodo(ucPeriodo.Periodo).Where(x => x.NrOrdem >= CdCargo.NrDiretor).OrderBy(x => x.NrOrdem).ToList();

        btnIncluir.Items.Clear();

        cargos.ForEach((x, i) => btnIncluir.Items.Add(new NDropDownItem { Text = x.NoCargo, Key = x.CdCargo }));

        cmbCargo.Bind(cargos);

        if(!IsPostBack)
        {
            ucPeriodo.Periodo = Periodo.Hoje;
            grdDir.DataBind();
            MontaPresidenteExercicio();
        }

        SetDefaultButton(btnPesquisar);         
    }

    protected void btnPesquisar_Click(object sender, EventArgs e)
    {
        grdDir.DataBind();
        MontaPresidenteExercicio();
    }

    protected void ucPeriodo_PeriodoChanged(object sender, EventArgs e)
    {
        OnPrepareForm();
    }

    protected void grdDir_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        var idMandato = e.DataKey<int>(sender);

        if(e.CommandName == "alt")
        {
            OpenPopup("form", "Mandato.Edit.aspx?TpMandato=D&IdMandato=" + idMandato, 750);
        }
        else if(e.CommandName == "del")
        {
            db.Diretoria.ExcluiMandato(idMandato);
            grdDir.DataBind();
            ShowInfoBox("Diretor excluido com sucesso");
        }
    }

    protected void btnIncluir_ItemClick(object sender, NDropDownItem e)
    {
        OpenPopup("form", "Mandato.New.aspx?TpMandato=D&CdCargo=" + e.Key, 750);
    }

    protected void MontaPresidenteExercicio()
    {
        presidenteExercicio = db.Diretoria.BuscaPresidenteExercicio(ucPeriodo.Periodo.Inicio);
        lblNoPessoa.Text = presidenteExercicio?.Pessoa?.NoPessoa ?? "<div class='space-top'><i class='icon-attention danger'></i> Não encontrado</div>";
        dtIni.Text = presidenteExercicio.Pessoa == null ? "" : presidenteExercicio?.GetPeriodo().ToString(false);
    }

</script>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <div class="mt-30 c-primary-blue">
        <span>Diretoria</span>
        <box:NDropDown runat="server" Icon="plus" Text="Incluir" ID="btnIncluir" CssClass="right ml-10" OnItemClick="btnIncluir_ItemClick"></box:NDropDown>
        <box:NButton runat="server" ID="btnPesquisar" Icon="search" Text="Pesquisar" CssClass="right" Theme="Default" OnClick="btnPesquisar_Click" />    
    </div>

    <div class="form-div">
        <div class="sub-form-row">
            <div class="div-sub-form form-uc">
                <div><span>Período</span></div>
                <uc:Periodo runat="server" ID="ucPeriodo" OnPeriodoChanged="ucPeriodo_PeriodoChanged" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form form-uc">
                <div><span>Cargo</span></div>
                <box:NDropDownList runat="server" ID="cmbCargo" Width="100%" />
            </div>
        </div>
    </div>

     <div class="form-div">
        <div class="sub-form-row">
            <div class="div-sub-form form-uc">
                <div><span>Presidente em Exercício</span></div>
                <box:NLabel runat="server" ID="lblNoPessoa" CssClass="field"/>
                
            </div>
            <div class="div-sub-form form-uc">
                <small runat="server">
                    <box:NLabel runat="server" ID="dtIni" CssClass="field margin-left mt-25" />                    
                </small>
            </div>
        </div>
    </div>

    <box:NGridView runat="server" ID="grdDir" ItemType="Creasp.Siplen.Mandato" DataKeyNames="IdMandato" OnRowCommand="grdDir_RowCommand" ShowHeader="false">
        <Columns>
            <asp:BoundField HeaderText="Cargo" DataField="Cargo.NoCargo" HeaderStyle-Width="220" ItemStyle-HorizontalAlign="right" />
            <asp:TemplateField>
                <ItemTemplate>
                    <div class="left-right">
                        <div>
                            <%# Item.Pessoa != null ? Item.Pessoa.NoPessoa : "<div class='space-top'><i class='icon-attention danger'></i> Não informado</div>" %>
                            <small runat="server" visible=<%# Item.Pessoa != null %>>&nbsp;&nbsp;(<%# Item.Pessoa?.NrCreasp %>)</small>
                        </div>
                        <div>
                            <box:NRepeater runat="server" DataSource=<%# Item.Licencas %> ItemType="Creasp.Siplen.Licenca">
                                <ItemTemplate>
                                    <box:NLabel runat="server" Theme="Danger" CssClass="space-left" Text=<%# Item.NoMotivoFmt %> ToolTip=<%# "Período: " + Item.GetPeriodo() %> />
                                </ItemTemplate>
                            </box:NRepeater>
                            <box:NLabel runat="server" Theme="Info" Visible=<%# Item.Pessoa != null %> Text=<%# Item.Pessoa?.NoTituloAbr %> ToolTip=<%# Item.Pessoa?.NoTitulo %> CssClass="no-titulo" />
                        </div>
                    </div>
                    <div class="space-top">
                        <small><%# Item.Pessoa != null ? Item.GetPeriodo().ToString(false) : "" %></small>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
            <box:NButtonField Icon="pencil" CommandName="alt" Theme="Info" ToolTip="Alterar período" EnabledExpression="Pessoa" />
            <box:NButtonField Icon="cancel" CommandName="del" Theme="Danger" ToolTip="Excluir" EnabledExpression="Pessoa" OnClientClick="return confirm('Confirma a exclusão deste mandato?')" />
        </Columns>
    </box:NGridView>

    <script src="../Content/Scripts/sidebar.js"></script>
    <script>selectButton("spl-dir"); showReload("siplen");</script>

</asp:Content>