﻿<%@ Page Title="Visualizar Evento" Resource="siplen" Language="C#" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();

    private int IdEvento => Request.QueryString["IdEvento"].To<int>();

    private bool _validaAnuidade = false;
    private bool _validaSiscont = false;

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        grdPart.RegisterPagedDataSource((p) =>
        {
            var participantes = db.Eventos.ListaParticipantes(IdEvento, true).ToPage(p);

            // se houver validação, faz apenas na pagina atual
            if (_validaSiscont || _validaAnuidade)
            {
                participantes.Items.ForEach((x, i) =>
                {
                    db.Nerps.ValidaPessoaSituacao(x.Titular, _validaSiscont, _validaAnuidade);
                    db.Nerps.ValidaPessoaSituacao(x.Suplente, _validaSiscont, _validaAnuidade);
                });
            }

            var nerps = db.Query<NerpItem>()
                .Include(x => x.Nerp)
                .Where(x => x.Nerp.IdEvento == IdEvento)
                .ToList()
                .GroupBy(x => x.IdPessoaCredor);

            // adiciona as nerps dos titulares/suplentes
            participantes.Items.ForEach((x, i) =>
            {
                x.NerpsTit = nerps.FirstOrDefault(n => n.Key == x.IdPessoaTit)?.Select(n => n.Nerp).ToList();
                x.NerpsSup = nerps.FirstOrDefault(n => n.Key == x.IdPessoaSup)?.Select(n => n.Nerp).ToList();
            });

            return participantes;
        });

        RegisterClosePopup("add", (args) =>
        {
            MontaPresencaAgrupada();
            grdPart.DataBind();
        });

        RegisterRestorePageState(() => grdPart.DataBind());

        RegisterResource("siplen.evento").Disabled(btnAdd, btnConvocar).GridColumns(grdPart, "del");

        //TabAdd
        grdList.RegisterDataSource(() => db.Eventos.ListaConvidados(IdEvento, cmbTpMandato.SelectedValue, txtNoPessoa.Text.To<string>(),
            cmbCamara.SelectedValue.To<int?>(),
            cmbComissao.SelectedValue.To<int?>(),
            cmbGrupo.SelectedValue.To<int?>(),
            txtInspetoria.Value.To<int?>(),
            cmbCargo.SelectedValue.To<string>(),
            cmbGRE.SelectedValue.To<int?>()));

        txtInspetoria.RegisterDataSource((q) =>
            db.Query<Inspetoria>()
                .Include(x => x.Unidade)
                .WherePeriodo(Periodo.Hoje)
                .Where(x => x.NoInspetoria.StartsWith(q.ToLike()))
                .Limit(10)
                .ProjectTo(x => new { x.IdInspetoria, x.NoInspetoria, NoUnidade = x.Unidade.NoUnidade }));
        
        
        /*Tab Registrar Presenca*/
        grdPart2.RegisterDataSource(() => db.Eventos.ListaParticipantes(IdEvento, true, cmbFiltroPessoa.SelectedValue.To<TpPessoa?>()));

        RegisterResource("siplen.evento").ThrowIfNoAccess();
    }

    protected override void OnPrepareForm()
    {
        var e = db.Eventos.BuscaEvento(IdEvento, false);

        ltrCaption.Text = "Evento | " + e.NoEvento;
        lblDtEvento.Text = e.DtEvento.ToString("d");
        lblHrEvento.Text = e.DtEvento.ToString("HH:mm") + " até " +
            (e.DtEvento.Date != e.DtEventoFim.Date ? e.DtEventoFim.ToString("d") + " - " : "") +
            e.DtEventoFim.ToString("HH:mm");
        lblNoLocal.Text =  e.NoLocal + (e.Cep == null ? "" : (" - " + e.Cep.NoCidade + " / " + e.Cep.CdUf));
        lblNoHistorico.Text = e.NoHistorico;
        trHistorico.Visible = lblNoHistorico.Text.Length > 0;
        lnkEditar.NavigateUrl = "Evento.Form.aspx?IdEvento=" + IdEvento;
        //lnkPresenca.NavigateUrl = "Evento.Presenca.aspx?IdEvento=" + IdEvento;
        //lnkEmitirNerp.NavigateUrl = "/nerp/Nerp.Evento.aspx?IdEvento=" + IdEvento;

        MontaPresencaAgrupada();

        btnAdd.Enabled = this.IsProprietario(e, "siplen.evento.admin");
        btnConvocar.Enabled = this.IsProprietario(e, "siplen.evento.admin");
        //lnkPresenca.Visible = this.IsProprietario(e, "siplen.evento.admin");
        //lnkEmitirNerp.Visible = User.IsInRole("nerp.evento");

        this.Audit(e, e.IdEvento);
        this.BackTo("Evento.aspx");
        grdPart.DataBind();

        //TabAdd
        cmbTpMandato.Items.Add(new ListItem("(Selecione uma opção abaixo)", ""));
        cmbTpMandato.Items.Add(new ListItem("Conselheiros", TpMandato.Conselheiro));
        cmbTpMandato.Items.Add(new ListItem("Coordenadores de câmaras", TpMandato.Camara));
        cmbTpMandato.Items.Add(new ListItem("Representantes", TpMandato.Representante));
        cmbTpMandato.Items.Add(new ListItem("Indicados", TpMandato.Indicacao));
        cmbTpMandato.Items.Add(new ListItem("Diretor", TpMandato.Diretor));
        cmbTpMandato.Items.Add(new ListItem("Comissões", TpMandato.Comissao));
        cmbTpMandato.Items.Add(new ListItem("Grupos de trabalho", TpMandato.Grupo));
        cmbTpMandato.Items.Add(new ListItem("Inspetores", TpMandato.Inspetor));
        cmbTpMandato.Items.Add(new ListItem("Funcionarios", TpMandato.Funcionario));
        cmbTpMandato.Items.Add(new ListItem("Outras pessoas", TpMandato.Outro));

        cmbCamara.Bind(db.Camaras.ListaCamaras(Periodo.UmDia(e.DtEvento)));
        cmbComissao.Bind(db.Comissoes.ListaComissoes(Periodo.UmDia(e.DtEvento), null));
        cmbGrupo.Bind(db.Grupos.ListaGrupos(Periodo.UmDia(e.DtEvento), null));
        cmbGRE.Bind(db.Unidades.ListaUnidades(TpUnidade.DeptoRegional, null).Select(x => new { x.CdUnidade, x.NoUnidade }));

        cmbCargo.Items.Add(new ListItem("(Selecione uma opção abaixo)", ""));
        cmbCargo.Items.Add(new ListItem("Inspetor", CdCargo.Inspetor));
        cmbCargo.Items.Add(new ListItem("Inspetor chefe", CdCargo.InspetorChefe));
        cmbCargo.Items.Add(new ListItem("Inspetor especial", CdCargo.InspetorEspecial));

        SetDefaultButton(btnPesquisar);

        
        btnSalvar.Enabled = this.IsProprietario(e, "siplen.evento.admin");
        grdPart2.DataBind();
        this.BackTo("Evento.View.aspx?IdEvento=" + IdEvento);
    }

    protected void MontaPresencaAgrupada()
    {
        var d = db.Eventos.AgrupaPresenca(IdEvento);

        ltrPresenteTit.Text = d["PRESENTE_TIT"].ToString();
        ltrLicenciadoTit.Text = d["LICENCIADO_TIT"].ToString();
        ltrJustificadoTit.Text = d["JUSTIFICADO_TIT"].ToString();
        ltrFaltaTit.Text = d["FALTA_TIT"].ToString();

        // so exibe se alguma presença/falta foi informada
        trPresencaTit.Visible = d["PRESENTE_TIT"] > 0 || d["FALTA_TIT"] > 0;

        ltrPresenteSup.Text = d["PRESENTE_SUP"].ToString();
        ltrLicenciadoSup.Text = d["LICENCIADO_SUP"].ToString();
        ltrJustificadoSup.Text = d["JUSTIFICADO_SUP"].ToString();
        ltrFaltaSup.Text = d["FALTA_SUP"].ToString();

        // so exibe suplentes se estiver exibindo titulares e se existir suplentes
        trPresencaSup.Visible = d["SUPLENTES"] > 0 && trPresencaTit.Visible;
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        OpenPopup("add", "Evento.Add.aspx?IdEvento=" + IdEvento, 950);
    }

    protected void btnConvocar_Click(object sender, EventArgs e)
    {
        ucEmail.AbrePopup("Convocacao");
    }

    protected void btnDocs_Click(object sender, EventArgs e)
    {
        ucDocs.MontaTela(TpRefDoc.Evento, IdEvento, null, true);
        pnlDocs.Open();
    }

    protected void btnListaPres_ItemClick(object sender, NDropDownItem e)
    {
        pnlReport.Open(e.Key);
    }

    protected void pnlReport_Click(object sender, ReportFormat e)
    {
        var rpt = new Report(Server.MapPath("Reports/ListaPresenca" + pnlReport.ReportName + ".rdl"));

        rpt.AddParameter("pNoEvento", ltrCaption.Text);
        rpt.AddParameter("pDtEvento", lblDtEvento.Text + " - " + lblHrEvento.Text);
        rpt.AddParameter("pNoLocal", lblNoLocal.Text);

        TpParticipante? tpParticipante =
            pnlReport.ReportName == "Cons" ? TpParticipante.Conselheiro :
            pnlReport.ReportName == "Dire" ? TpParticipante.Diretor :
            pnlReport.ReportName == "Insp" ? TpParticipante.Inspetor :
            pnlReport.ReportName == "Func" ? TpParticipante.Funcionario :
            pnlReport.ReportName == "GT" ? TpParticipante.GrupoDeTrabalho :
            pnlReport.ReportName == "Outros" ?TpParticipante.Outros : (TpParticipante?)null;

        rpt.AddDataSet("dsMain", db.Relatorios.ListaPresenca(IdEvento, tpParticipante));

        ShowReport(rpt, e, "ListaPresenca-" + IdEvento);
    }

    protected void btnValidar_ItemClick(object sender, NDropDownItem e)
    {
        _validaAnuidade = e.Key == "ANUIDADE";
        _validaSiscont = e.Key == "SISCONT";
        grdPart.DataBind();
    }

    protected void btnDel_Click(object sender, EventArgs e)
    {
        var ids = grdPart.SelectedDataKeys.Select(x => x.Value.To<int>()).ToArray();

        if (ids.Length == 0) throw new NBoxException("Nenhum participante selecionado");

        db.Eventos.ExcluiParticipantes(ids);
        grdPart.DataBind();
        MontaPresencaAgrupada();

        ShowInfoBox("Participante(s) excluidos com sucesso");
    }

    //TabAdd
    protected void btnAddPerson_Click(object sender, EventArgs e)
    {
        if(grdList.SelectedDataKeys.Count() == 0) throw new NBoxException("Nenhuma pessoa selecionada");

        // busca o centro de custo padrão conforme os filtros selecionados na tela
        var tpMandato = cmbTpMandato.SelectedValue.ToString();
        var idRef = tpMandato == TpMandato.Conselheiro && cmbCamara.HasValue() ? cmbCamara.SelectedValue.To<int>() :
            tpMandato == TpMandato.Comissao ? cmbComissao.SelectedValue.To<int>() :
            tpMandato == TpMandato.Grupo ? cmbGrupo.SelectedValue.To<int>() : (int?)null;

        var cc = db.Eventos.BuscaCentroCustoPadrao(tpMandato, idRef);

        ucCentroCusto.LoadCentroCusto(cc?.IdCentroCusto);

        pnlCentroCusto.Open(btnAddCentroCusto, ucCentroCusto);
    }

    protected void btnPesquisar_Click(object sender, EventArgs e)
    {
        cmbTpMandato.Validate().Required();
        cmbComissao.Validate(cmbTpMandato.SelectedValue == TpMandato.Comissao).Required();
        cmbGrupo.Validate(cmbTpMandato.SelectedValue == TpMandato.Grupo).Required();
        txtNoPessoa.Validate(cmbTpMandato.SelectedValue == TpMandato.Outro).Required();

        grdList.Visible = true;
        grdList.DataBind();
    }

    protected void cmbTpMandato_SelectedIndexChanged(object sender, EventArgs e)
    {
        var tp = cmbTpMandato.SelectedValue.ToString();

        trCamara.Visible = tp == TpMandato.Conselheiro || tp == TpMandato.Camara || tp == TpMandato.Representante;
        trComisso.Visible = tp == TpMandato.Comissao;
        trGrupo.Visible = tp == TpMandato.Grupo;
        trInspetoria.Visible = tp == TpMandato.Inspetor;
        trCargo.Visible = tp == TpMandato.Inspetor;
        trGRE.Visible = tp == TpMandato.Inspetor;

        grdList.Columns[2].Visible = tp == TpMandato.Conselheiro || tp == TpMandato.Comissao;

        grdList.Visible = false;
        grdList.Clear();
    }

    protected void btnAddCentroCusto_Click(object sender, EventArgs e)
    {
        if (ucCentroCusto.IdCentroCusto == null) throw new NBoxException("Centro de custo obrigatório");

        var lista = grdList.SelectedDataKeys.Select(x => new EventoConvidadoDTO
        {
            IdPessoaTit = x["IdPessoaTit"].To<int>(),
            IdMandatoTit = x["IdMandatoTit"].To<int?>(),
            FgLicencaTit = x["FgLicencaTit"].To<bool>(),
            IdPessoaSup = x["IdPessoaSup"].To<int?>(),
            IdMandatoSup = x["IdMandatoSup"].To<int?>(),
            FgLicencaSup = x["FgLicencaSup"].To<bool>(),
            NrOrdem = x["NrOrdem"].To<int>()
        }).ToList();

        var count = db.Eventos.AdicionaMembros(IdEvento, ucCentroCusto.IdCentroCusto.Value, cmbTpMandato.SelectedValue, lista);

        ShowInfoBox($"{count} pessoas foram adicionadas neste evento");

        grdList.Visible = true;
        grdList.DataBind();
        pnlCentroCusto.Close();
    }

    protected void btnCadastroPessoa_Click(object sender, EventArgs e)
    {
        ucPessoa.InsertPessoa();
    }

    protected void ucPessoa_PessoaChanged(object sender, EventArgs e)
    {
        if (ucPessoa.IdPessoa != null)
        {
            txtNoPessoa.Text = ucPessoa.Pessoa.NoPessoa;
            cmbTpMandato.SelectedValue = TpMandato.Outro;
            cmbTpMandato_SelectedIndexChanged(null, null);
            btnPesquisar_Click(null, null);
        }
    }

    //Tab Registrar Presenca
    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        var participantes = db.Eventos.ListaParticipantes(IdEvento, false);

        foreach(var row in grdPart.Rows.Cast<GridViewRow>())
        {
            var idParticipante = grdPart.DataKeys[row.RowIndex].Values["IdParticipante"].To<int>();

            var p = participantes.Single(x => x.IdParticipante == idParticipante);

            p.TpPresencaTit = (row.FindControl("cmbTpPresencaTit") as NDropDownList).SelectedValue;
            p.TpPresencaSup = (row.FindControl("cmbTpPresencaSup") as NDropDownList)?.SelectedValue;
        }

        db.Eventos.RegistraPresenca(IdEvento, participantes);

        ShowInfoBox("Registro de presença confirmado");
        Redirect("Evento.View.aspx?IdEvento=" + IdEvento);
    }

    protected void cmbFiltroPessoa_SelectedIndexChanged(object sender, EventArgs e)
    {
        grdPart2.DataBind();
    }

</script>
<asp:Content ContentPlaceHolderID="title" runat="server">
    <box:NHyperLink runat="server" ID="lnkEditar" Icon="pencil" Text="Editar" ButtonTheme="Success" />
</asp:Content>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <uc:Email runat="server" ID="ucEmail" />

    <box:NPopup runat="server" ID="pnlDocs" Title="Documentos do evento" Width="750"><div class="margin-top"><uc:Documento runat="server" ID="ucDocs" /></div></box:NPopup>

    <uc:ReportPopup runat="server" Id="pnlReport" OnClick="pnlReport_Click" />

    <%--<box:NToolBar runat="server">
        <box:NDropDown runat="server" ID="btnValidar" Icon="menu" OnItemClick="btnValidar_ItemClick" CssClass="right">
            <box:NDropDownItem Key="ANUIDADE" Text="Anuidade" />
            <box:NDropDownItem Key="SISCONT" Text="Cadastro SISCONT" />
        </box:NDropDown>
    </box:NToolBar>--%>

    <%-- Tab Geral --%>
    <box:NTab runat="server" ID="tabGeral" Icon="users" Text="Geral" selected="true" Theme="Default" OnClick="btnAdd_Click">
        <div class="mt-30 c-primary-blue">
            <span><asp:Literal runat="server" ID="ltrCaption" /></span>
        </div>

        <div class="form-div">
            <div class="sub-form-row margin-top-0">
                <div class="div-sub-form">
                    <div><span>Data/Hora</span></div>
                    <span class="field w-96"><box:NLabel runat="server" ID="lblDtEvento" />&nbsp;-&nbsp;
                    <box:NLabel runat="server" ID="lblHrEvento" /></span>
                </div>
                <div class="div-sub-form">
                    <div><span>Local</span></div>
                    <box:NLabel runat="server" ID="lblNoLocal" CssClass="field" />
                </div>
            </div>
            <div class="sub-form-row margin-top-0" runat="server" id="trHistorico">
                <div class="div-sub-form">
                    <div><span>Histórico</span></div>
                    <box:NLabel runat="server" ID="lblNoHistorico" CssClass="field" />
                </div>
            </div>
            <div class="sub-form-row margin-top-0" runat="server" id="trPresencaTit">
                <div class="div-sub-form">
                    <div><span>Titulares</span></div>
                    <div style="display: flex; justify-content: space-between;">
                        <div style="width: 110px"><box:NLabel runat="server" Theme="Success" Text="PRESENTE" />&nbsp;=&nbsp;<asp:Literal runat="server" ID="ltrPresenteTit" Text="0" /></div>
                        <div style="width: 110px"><box:NLabel runat="server" Theme="Info" Text="LICENCIADO" />&nbsp;=&nbsp;<asp:Literal runat="server" ID="ltrLicenciadoTit" Text="0" /></div>
                        <div style="width: 110px"><box:NLabel runat="server" Theme="Info" Text="JUSTIFICADO" />&nbsp;=&nbsp;<asp:Literal runat="server" ID="ltrJustificadoTit" Text="0" /></div>
                        <div style="width: 110px"><box:NLabel runat="server" Theme="Danger" Text="FALTA" />&nbsp;=&nbsp;<asp:Literal runat="server" ID="ltrFaltaTit" Text="0" /></div>
                    </div>
                </div>
            </div>
            <div class="sub-form-row margin-top-0" runat="server" id="trPresencaSup">
                <div class="div-sub-form">
                    <div><span>Suplentes</span></div>
                    <div style="display: flex; justify-content: space-between;">
                        <div style="width: 110px"><box:NLabel runat="server" Theme="Success" Text="PRESENTE" />&nbsp;=&nbsp;<asp:Literal runat="server" ID="ltrPresenteSup" Text="0" /></div>
                        <div style="width: 110px"><box:NLabel runat="server" Theme="Info" Text="LICENCIADO" />&nbsp;=&nbsp;<asp:Literal runat="server" ID="ltrLicenciadoSup" Text="0" /></div>
                        <div style="width: 110px"><box:NLabel runat="server" Theme="Info" Text="JUSTIFICADO" />&nbsp;=&nbsp;<asp:Literal runat="server" ID="ltrJustificadoSup" Text="0" /></div>
                        <div style="width: 110px"><box:NLabel runat="server" Theme="Danger" Text="FALTA" />&nbsp;=&nbsp;<asp:Literal runat="server" ID="ltrFaltaSup" Text="0" /></div>
                    </div>
                </div>
            </div>
        </div>

        <box:NGridView runat="server" ID="grdPart" AllowGrouping="true" ItemType="Creasp.Siplen.EventoParticipante" DataKeyNames="IdParticipante">
            <Columns>
                <asp:BoundField DataField="CentroCusto.NoCentroCustoFmt"/>
                <asp:TemplateField HeaderText="Titular" HeaderStyle-Width="50%">
                    <ItemTemplate>
                        <div class="left-right">
                            <span>
                                <%# Item.Titular.NoPessoa %>
                                <small runat="server" visible=<%# Item.Titular.NrCreasp != null %>>&nbsp;&nbsp;(<a href="Hist.View.aspx?IdPessoa=<%# Item.Titular?.IdPessoa %>"><%# Item.Titular?.NrCreasp %></a>)</small>
                                <small runat="server" visible=<%# !Item.LicencaTit.IsVazio  %> class="danger"> (Licenciado de <%# Item.LicencaTit.ToString(false) %>)</small>
                            </span>
                            <box:NLabel runat="server" Text=<%# Item.Titular.NoTituloAbr %> Theme="Info" CssClass="no-titulo" ToolTip=<%# Item.Titular.NoTitulo %> />
                        </div>
                        <div class="left-right space-top">
                            <span>
                                <small><%# Item.CentroCusto.NrCentroCustoFmt %></small>
                                <box:NLabel runat="server" Theme="Info" Visible=<%# Item.NrOrdem > 0 %> Text=<%# Item.NrOrdem + "º Suplente" %> />
                            </span>
                            <span>
                                <box:NLabel Visible=<%# Item.Titular.Mensagem != null %> runat="server" Text=<%# Item.Titular.Mensagem %> Theme=<%# Item.Titular.Mensagem == "OK" ? ThemeStyle.Success : ThemeStyle.Danger  %> />

                                <box:NLabel Visible=<%# Item.TpPresencaTit == TpPresenca.Presente %> runat="server" Text="PRESENTE" Theme="Success" />
                                <box:NLabel Visible=<%# Item.TpPresencaTit == TpPresenca.Justificado %> runat="server" Text="JUSTIFICADO" Theme="Info" />
                                <box:NLabel Visible=<%# Item.TpPresencaTit == TpPresenca.Licenciado %> runat="server" Text="LICENCIADO" Theme="Info" />
                                <box:NLabel Visible=<%# Item.TpPresencaTit == TpPresenca.Falta %> runat="server" Text="FALTA" Theme="Danger" />
                                <box:NLabel Visible=<%# Item.NrFaltasTit > 0 %> runat="server" Text=<%# Item.NrFaltasTit %> Theme="Danger" ToolTip="Faltas" />
                            </span>
                        </div>
                        <box:NRepeater runat="server" DataSource=<%# Item.NerpsTit %> ItemType="Creasp.Nerp.Nerp">
                            <HeaderTemplate>
                                <div class="space-top">
                                    <small class="t-bold">NERP:&nbsp;</small>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <small>
                                    <a href="../Nerp/Nerp.View.aspx?IdNerp=<%# Item.IdNerp %>"># <%# Item.NrNerp %>/<%# Item.NrAno %></a>&nbsp;&nbsp;&nbsp;
                                </small>
                            </ItemTemplate>
                            <FooterTemplate>
                                </div>
                            </FooterTemplate>
                        </box:NRepeater>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Suplente" HeaderStyle-Width="50%">
                    <ItemTemplate>
                        <div class="left-right">
                            <span>
                                <%# Item.Suplente?.NoPessoa %>
                                <small runat="server" visible=<%# Item.Suplente?.NrCreasp != null %>>&nbsp;&nbsp;(<a href="Hist.View.aspx?IdPessoa=<%# Item.Suplente?.IdPessoa %>"><%# Item.Suplente?.NrCreasp %></a>)</small>
                                <small runat="server" visible=<%# !Item.LicencaSup.IsVazio %> class="danger"> (Licenciado de <%# Item.LicencaSup.ToString(false) %>)</small>
                            </span>
                            <box:NLabel runat="server" Text=<%# Item.Suplente?.NoTituloAbr %> Visible=<%# Item.Suplente != null %> Theme="Info" CssClass="no-titulo" ToolTip=<%# Item.Titular.NoTitulo %> />
                        </div>
                        <div class="left-right space-top">
                            <small><%# "" %>&nbsp;</small>
                            <span>
                                <box:NLabel Visible=<%# Item.Suplente?.Mensagem != null %> runat="server" Text=<%# Item.Suplente?.Mensagem %> Theme=<%# Item.Suplente?.Mensagem == "OK" ? ThemeStyle.Success : ThemeStyle.Danger  %> />

                                <box:NLabel Visible=<%# Item.TpPresencaSup == TpPresenca.Presente %> runat="server" Text="PRESENTE" Theme="Success" />
                                <box:NLabel Visible=<%# Item.TpPresencaSup == TpPresenca.Justificado %> runat="server" Text="JUSTIFICADO" Theme="Info" />
                                <box:NLabel Visible=<%# Item.TpPresencaSup == TpPresenca.Licenciado %> runat="server" Text="LICENCIADO" Theme="Info" />
                                <box:NLabel Visible=<%# Item.NrFaltasSup > 0 %> runat="server" Text=<%# Item.NrFaltasSup %> Theme="Danger" ToolTip="Faltas" />
                            </span>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <box:NCheckBoxField />
            </Columns>
            <EmptyDataTemplate>Nenhum participante neste evento</EmptyDataTemplate>
        </box:NGridView>
    </box:NTab>

    <%-- Tab Adicionar --%>
    <box:NTab runat="server" ID="tabAdd" Icon="users" Text="Adicionar" Theme="Default">
        <%--<box:NButton runat="server" ID="btnAdd" Icon="users" Text="Adicionar" Theme="Default" OnClick="btnAdd_Click" />--%>
        <uc:Pessoa runat="server" id="ucPessoa" InsertOnly="true" OnPessoaChanged="ucPessoa_PessoaChanged" />

        <div class="mt-30 c-primary-blue">
            <span>Adicionar Participantes</span><box:NLinkButton runat="server" ID="btnCadastroPessoa" CssClass="right icon-right" OnClick="btnCadastroPessoa_Click"><i class="fas fa-user-plus"></i><span>Cadastrar</span></box:NLinkButton>
        </div>

        <div class="form-div">
            <div class="sub-form-row margin-top-0">
                <div class="div-sub-form">
                    <div><span>Tipo de mandato</span></div>
                    <box:NDropDownList runat="server" ID="cmbTpMandato" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="cmbTpMandato_SelectedIndexChanged" />
                </div>
                <div class="div-sub-form margin-left" runat="server" id="trCamara" visible="false">
                    <div><span>Câmara</span></div>
                    <box:NDropDownList runat="server" ID="cmbCamara" Width="100%" />
                </div>
                <div class="div-sub-form margin-left">
                    <div><span>Nome</span></div>
                    <div class="input-group">
                        <box:NTextBox runat="server" ID="txtNoPessoa" Width="100%" TextCapitalize="Upper" CssClass="radius-right-0" />
                        <box:NButton runat="server" ID="btnPesquisar" Icon="search" Width="61px" Theme="Primary" Font-Size="14pt" CssClass="" OnClick="btnPesquisar_Click" CausesValidation="false"></box:NButton>
                    </div>
                </div>
            </div>
            <div class="sub-form-row margin-top-0">
                <div class="div-sub-form" runat="server" id="trComisso" visible="false">
                    <div><span>Comissões</span></div>
                    <box:NDropDownList runat="server" ID="cmbComissao" Width="100%" />
                </div>
                <div class="div-sub-form" runat="server" id="trGrupo" visible="false">
                    <div><span>Grupos</span></div>
                    <box:NDropDownList runat="server" ID="cmbGrupo" Width="100%" />
                </div>
                <div class="div-sub-form" runat="server" id="trGRE" visible="false">
                    <div><span>Gerência</span></div>
                    <box:NDropDownList runat="server" ID="cmbGRE" Width="100%" AutoPostBack="true" />
                </div>
                <div class="div-sub-form margin-left" runat="server" id="trInspetoria" visible="false">
                    <div><span>Município</span></div>
                    <box:NAutocomplete runat="server" ID="txtInspetoria" Width="100%" TextCapitalize="Upper" DataValueField="IdInspetoria" DataTextField="NoInspetoria" Template="{NoInspetoria} - {NoUnidade}" />
                </div>
                <div class="div-sub-form margin-left" runat="server" id="trCargo" visible="false">
                    <div><span>Cargo</span></div>
                    <box:NDropDownList runat="server" ID="cmbCargo" Width="100%" />
                </div>
            </div>
        </div>

        <div class="margin-top">
            <box:NGridView runat="server" ID="grdList" ItemType="Creasp.Siplen.EventoConvidadoDTO" DataKeyNames="IdPessoaTit,IdPessoaSup,IdMandatoTit,IdMandatoSup,FgLicencaTit,FgLicencaSup,NrOrdem">
                <Columns>
                    <asp:TemplateField HeaderText="Titular" HeaderStyle-Width="50%">
                        <ItemTemplate>
                            <div class="left-right">
                                <%# Item.NoPessoaTit %>
                                <div>
                                    <box:NLabel runat="server" Visible=<%# Item.FgLicencaTit %> Text="Licença" Theme="Danger" ToolTip="Esta pessoa está de licença no dia deste evento" />
                                    <box:NLabel runat="server" Theme="Info" Visible=<%# Item.NoTituloTit != null %> Text=<%# Item.NoTituloTit %> CssClass="no-titulo" ToolTip=<%# Item.NoTituloTit %> />
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Suplente" HeaderStyle-Width="50%">
                        <ItemTemplate>
                            <div class="left-right">
                                <%# Item.NoPessoaSup %>
                                <div>
                                    <box:NLabel runat="server" Visible=<%# Item.FgLicencaSup %> Text="Licença" Theme="Danger" ToolTip="Esta pessoa está de licença no dia deste evento" />
                                    <box:NLabel runat="server" Theme="Info" Visible=<%# Item.NoTituloSup != null %> Text=<%# Item.NoTituloSup %> CssClass="no-titulo" ToolTip=<%# Item.NoTituloSup %> />
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <box:NCheckBoxField EnabledExpression="!FgConvidado" />
                </Columns>
                <EmptyDataTemplate>Nenhuma pessoa encontrada para o filtro</EmptyDataTemplate>
            </box:NGridView>
        </div>

        <box:NPopup runat="server" ID="pnlCentroCusto" Title="Adicionar participante" Width="750">
            <box:NToolBar runat="server">
                <box:NButton runat="server" ID="btnAddCentroCusto" Icon="ok" Text="Confirmar" Theme="Primary" OnClick="btnAddCentroCusto_Click" ValidationGroup="cc" />
            </box:NToolBar>
        
            <div class="mt-30 c-primary-blue">
                <span>Definir centro de custo</span>
            </div>

            <div class="form-div">
                <div class="sub-form-row margin-top-0">
                    <div class="div-sub-form">
                        <div><span>Centro de custo</span></div>
                        <uc:CentroCusto runat="server" ID="ucCentroCusto" />
                        <div class="help">Informe o centro de custo referente aos participantes selecionados da tela anterior. Este centro de custo será utilizado na emissão da NERP de ressarcimento/adiantamento.</div>
                    </div>
                </div>
            </div>
        </box:NPopup>
        <box:NButton runat="server" ID="btnAdd" Icon="plus" Text="Adicionar" Theme="Primary" CssClass="right" OnClick="btnAddPerson_Click" />
    </box:NTab>

    <%-- Tab Lista de Presença --%>
    <box:NTab runat="server" ID="tabListaPres" Text="Lista de presença">
        <div class="mt-30 c-primary-blue">
            <span>Lista de Presença</span>
        </div>

        <div class="form-div">
            <div class="sub-form-row margin-top-0">
                <div class="div-sub-form">
                    <div><span>Relatório</span></div>
                    <box:NDropDown runat="server" cssClass="check-box-drop" ID="btnListaPres" Text="Lista de presença" Icon="print" OnItemClick="btnListaPres_ItemClick">
                        <box:NDropDownItem Key="Cons" Text="Conselheiros" />
                        <box:NDropDownItem Key="Dire" Text="Diretores" />
                        <box:NDropDownItem Key="Insp" Text="Inspetores" />
                        <box:NDropDownItem Key="Func" Text="Funcionários" />
                        <box:NDropDownItem Key="Outros" Text="Convidados" />
                        <box:NDropDownItem Key="GT" Text="Grupo de Trabalho" />
                    </box:NDropDown>
                </div>
            </div>
        </div>
    </box:NTab>

    <%-- Tab Email --%>
    <box:NTab runat="server" ID="tabEmail" Icon="mail" Text="Email">
        <div class="mt-30 c-primary-blue">
            <span>Notificação por Email</span>
        </div>

        <div class="form-div">
            <div class="sub-form-row margin-top-0">
                <div class="div-sub-form">
                    <%--<div><span>Notificação por Email</span></div>--%>
                    <box:NButton runat="server" ID="btnConvocar" Icon="mail" Text="Enviar Email" OnClick="btnConvocar_Click" />
                </div>
            </div>
        </div>
    </box:NTab>

    <%-- Tab Registrar Presença --%>
    <box:NTab runat="server" ID="tabPresenca" Icon="pencil" Text="Registrar presença" ButtonTheme="Default">
        <div class="form-div">
        <div class="sub-form-row margin-top-0">
            <div class="div-sub-form">
                <div><span>Filtrar por</span></div>
                <box:NDropDownList runat="server" ID="cmbFiltroPessoa" Width="350" AutoPostBack="true" OnSelectedIndexChanged="cmbFiltroPessoa_SelectedIndexChanged">
                    <asp:ListItem Value="" Text="" Selected="True" />
                    <asp:ListItem Value="Conselheiro" Text="Conselheiros" />
                    <asp:ListItem Value="Inspetor" Text="Inspetores" />
                    <asp:ListItem Value="Funcionario" Text="Funcionários" />
                    <asp:ListItem Value="Outros" Text="Outros" />
                </box:NDropDownList>
                <box:NButton runat="server" ID="btnSalvar" Icon="ok" Text="Registrar" Theme="Primary" OnClick="btnSalvar_Click" CssClass="margin-left" />
            </div>
        </div>
    </div>

    <box:NGridView runat="server" ID="grdPart2" ItemType="Creasp.Siplen.EventoParticipante" DataKeyNames="IdParticipante">
        <Columns>
            <asp:TemplateField HeaderText="Titular" HeaderStyle-Width="50%">
                <ItemTemplate>
                    <div class="left-right">
                        <div>
                            <%# Item.Titular.NoPessoa %>
                            <small runat="server" Visible=<%# Item.Titular.NrCreasp != null %>>&nbsp;&nbsp;(<%# Item.Titular.NrCreasp %>)</small>
                            <box:NLabel runat="server" Theme="Info" Visible=<%# Item.NrOrdem > 0 %> Text=<%# Item.NrOrdem + "º Suplente" %> />
                        </div>
                        <div>
                            <box:NDropDownList runat="server" ID="cmbTpPresencaTit" SelectedValue=<%# Item.TpPresencaTit %> CssClass="cmb-presenca">
                                <asp:ListItem Value="P" data-bg="#5CB85C">Presente</asp:ListItem>
                                <asp:ListItem Value="L" data-bg="#5BC0DE">Licenciado</asp:ListItem>
                                <asp:ListItem Value="J" data-bg="#5BC0DE">Justificado</asp:ListItem>
                                <asp:ListItem Value="F" data-bg="#D9534F">Falta</asp:ListItem>
                            </box:NDropDownList>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Suplente" HeaderStyle-Width="50%">
                <ItemTemplate>
                    <div class="left-right">
                        <div>
                            <%# Item.Suplente?.NoPessoa %>
                            <small runat="server" Visible=<%# Item.Suplente?.NrCreasp != null %>>&nbsp;&nbsp;(<%# Item.Suplente?.NrCreasp %>)</small>
                        </div>
                        <div>
                            <box:NDropDownList runat="server" ID="cmbTpPresencaSup" Visible=<%# Item.Suplente != null %> SelectedValue=<%# Item.TpPresencaSup %> CssClass="cmb-presenca">
                                <asp:ListItem Value=""></asp:ListItem>
                                <asp:ListItem Value="P" data-bg="#5CB85C">Presente</asp:ListItem>
                                <asp:ListItem Value="L" data-bg="#5BC0DE">Licenciado</asp:ListItem>
                                <asp:ListItem Value="J" data-bg="#5BC0DE">Justificado</asp:ListItem>
                                <asp:ListItem Value="F" data-bg="#D9534F">Falta</asp:ListItem>
                            </box:NDropDownList>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>Nenhum participante neste evento</EmptyDataTemplate>
    </box:NGridView>

    <script>
        $('.cmb-presenca')
            .each(mudaCorCombo)
            .on('click', mudaCorCombo)
            .css('border-radius', '10px')
            .find('option').css({ 'background-color': 'white', 'color': 'black' });

        function mudaCorCombo() {
            var el = $(this);
            var bg = el.find('option:selected').attr('data-bg') || 'white';
            el.css('color', bg == 'white' ? 'black' : 'white').css('background-color', bg);
        }

    </script>
    </box:NTab>

    <%-- Tab Documentos --%>
    <box:NTab runat="server" ID="tabDocs" Icon="doc" Text="Documentos">
        <div class="mt-30 c-primary-blue">
            <span>Documentos do Evento</span>
        </div>

        <div class="form-div">
            <div class="sub-form-row margin-top-0">
                <div class="div-sub-form">
                    <%--<div><span>Documentos</span></div>--%>
                    <box:NButton runat="server" ID="btnDocs" Icon="doc" Text="Documentos" OnClick="btnDocs_Click" />
                </div>
            </div>
        </div>
    </box:NTab>

    <%-- Tab Emitir Nerp --%>
    <%--<box:NTab runat="server" ID="tabEmitirNerp" ButtonTheme="Default" Icon="briefcase" Text="Emitir NERP">
        <box:NHyperLink runat="server" ID="lnkEmitirNerp" ButtonTheme="Default" Icon="briefcase" Text="Emitir NERP" />
    </box:NTab>--%>

    <script src="../Content/Scripts/sidebar.js"></script>
    <script>selectButton("nerp-event"); showReload("nerp");</script>
</asp:Content>