﻿<%@ Page Title="Grupos de Trabalho" Resource="siplen" Language="C#" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        grdList.RegisterDataSource(() => db.Grupos.ListaGrupos(
            ucPeriodo.Periodo,
            txtNoGrupo.Text));

        RegisterRestorePageState(() => grdList.DataBind());

        RegisterResource("siplen.gt").Disabled(lnkIncluir).GridColumns(grdList, 3);
    }

    protected override void OnPrepareForm()
    {
        ucPeriodo.Periodo = new Periodo("EsteAno");
        grdList.DataBind();
    }

    protected void btnPesquisar_Click(object sender, EventArgs e)
    {
        grdList.DataBind();
    }

    protected void grdList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if(e.CommandName == "alt")
        {
            var idGrupo = e.DataKey<int>(sender);
            Redirect("Grupo.Form.aspx?IdGrupo=" + idGrupo);
        }
        else if(e.CommandName == "del")
        {
            var idGrupo = e.DataKey<int>(sender);
            db.Grupos.ExcluiGrupo(idGrupo);
            grdList.DataBind();
            ShowInfoBox("Grupo excluido com sucesso");
        }
    }

</script>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <div class="mt-30 c-primary-blue">
        <span>Grupos de Trabalho</span>
        <box:NHyperLink runat="server" ID="lnkIncluir" ButtonTheme="Success" Icon="plus" CssClass="right new ml-10" Text="Novo grupo" NavigateUrl="Grupo.Form.aspx" />
        <box:NButton runat="server" ID="btnPesquisar" Icon="search" Text="Pesquisar" CssClass="right" OnClick="btnPesquisar_Click" />
    </div>

    <div class="form-div">
        <div class="sub-form-row">
            <div class="div-sub-form form-uc">
                <div><span>Período</span></div>
                <uc:Periodo runat="server" ID="ucPeriodo" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form form-uc">
                <div><span>Nome</span></div>
                <box:NTextBox runat="server" ID="txtNoGrupo" TextCapitalize="Upper" Width="100%" />
            </div>
        </div>
    </div>

    <box:NGridView runat="server" ID="grdList" ItemType="Creasp.Siplen.Grupo" DataKeyNames="IdGrupo" OnRowCommand="grdList_RowCommand">
        <Columns>
            <asp:BoundField DataField="NoGrupo" HeaderText="Grupo" />
            <asp:TemplateField HeaderText="Vigência" HeaderStyle-Width="220" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate><%# Item.GetPeriodo().ToString(false, true) %></ItemTemplate>
            </asp:TemplateField>
            <box:NButtonField Icon="pencil" CommandName="alt" Theme="Info" ToolTip="Alterar grupo" />
            <box:NButtonField Icon="cancel" CommandName="del" Theme="Danger" ToolTip="Excluir" OnClientClick="return confirm('Confirma a exclusão deste grupo de trabalho?')" />
        </Columns>
        <EmptyDataTemplate>Nenhum grupo encontrado com os filtros informados</EmptyDataTemplate>
    </box:NGridView>

    <script src="../Content/Scripts/sidebar.js"></script>
    <script>selectButton("spl-gru"); showReload("siplen");</script>

</asp:Content>