﻿<%@ Page Title="Consulta de Mandato" Resource="siplen" Language="C#" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();

    private int IdMandato => Request.QueryString["IdMandato"].To<int>();

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        this.RegisterClosePopup("edit", (args) => OnPrepareForm());

        this.RegisterClosePopup("subs", (args) =>
        {
            if (args != null && args.To<int?>() != IdMandato) Redirect("Plen.View.aspx?IdMandato=" + args); else OnPrepareForm();
        });

        RegisterResource("siplen.plen").Disabled(drp, rptCons);
    }

    protected override void OnPrepareForm()
    {
        rptCons.DataSource = db.Conselheiros.ListaMandatos(IdMandato);
        rptCons.DataBind();

        var m = db.Conselheiros.ListaMandatos(IdMandato).First();

        lblNoMandato.Text = m.AnoIni + " - " + m.AnoFim;
        lblNoCamara.Text = m.Camara.NoCamara;
        lblEntidade.Text = m.Entidade.NoEntidade;

        this.BackTo("Plen.aspx");
    }

    protected void btnEdit_Command(object sender, CommandEventArgs e)
    {
        OpenPopup("edit", "Plen.AltMandato.aspx?IdMandato=" + e.CommandArgument, 780);
    }

    protected void btnDel_Command(object sender, CommandEventArgs e)
    {
        var id = e.CommandArgument.To<int>();
        var m = db.SingleById<Mandato>(id);

        db.Conselheiros.ExcluiMandato(id);

        if(m.CdCargo == CdCargo.Suplente)
        {
            OnPrepareForm();
        }
        else
        {
            Redirect("Plen.aspx", true);
        }

        ShowInfoBox("Mandato excluido com sucesso");
    }

    protected void drp_ItemClick(object sender, NDropDownItem e)
    {
        var tit = db.Query<Mandato>()
            .Where(x => x.TpMandato == TpMandato.Conselheiro)
            .Where(x => x.IdMandato == IdMandato)
            .Single();

        var mandato = tit;

        if(e.Key == CdCargo.Suplente)
        {
            var sup = db.Query<Mandato>()
                .Where(x => x.IdMandatoTit == IdMandato)
                .OrderByDescending(x => x.DtFim)
                .FirstOrDefault();

            mandato = sup;
        }

        // Valida se existe tempo habil para um novo mandato de conselheiro titular/suplente
        if(mandato != null)
        {
            if(mandato.GetPeriodo().Fim == new DateTime(mandato.AnoFim, 12, 31))
            {
                throw new NBoxException("O mandato de {0} já termina em {1:d}", e.Key, mandato.GetPeriodo().Fim);
            }
        }

        OpenPopup("subs", "Plen.Substitui.aspx?IdMandato=" + IdMandato + "&CdCargo=" + e.Key, 780);
    }

</script>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <box:NDropDown runat="server" ID="drp" Icon="ccw" Text="Substituir" CssClass="right mt-25" AutoPostBack="true" OnItemClick="drp_ItemClick">
        <box:NDropDownItem Key="TITULAR" Text="Titular" />
        <box:NDropDownItem Key="SUPLENTE" Text="Suplente" />
    </box:NDropDown>

    <div class="form-div">
        <box:NRepeater runat="server" id="rptCons" ItemType="Creasp.Siplen.Mandato">
            <ItemTemplate>
                <div class="sub-form-row">
                    <div class="div-sub-form">
                        <div><span><%# Item.Cargo.NoCargo %></span></div>
                        <div class="input-group">
                            <span class="field left-right">
                                <span>
                                    <%# Item.Pessoa.NoPessoa %>
                                    <small>&nbsp;&nbsp;(<a href="Hist.View.aspx?IdPessoa=<%# Item.Pessoa.IdPessoa %>"><%# Item.Pessoa.NrCreasp %></a>)</small>
                                </span>
                                <box:NLabel runat="server" Theme="Info" Text=<%# Item.Pessoa.NoTituloAbr %> CssClass="no-titulo" ToolTip=<%# Item.Pessoa.NoTitulo %> />
                            </span>
                            <span class="nowrap field t-center" style="width: 250px;">
                                <%# Item.GetPeriodo().ToString(false) %>
                            </span>
                            <box:NButton runat="server" ID="btnEdit" Height="42" CommandArgument=<%# Item.IdMandato %> Icon="pencil" ToolTip="Editar mandato" OnCommand="btnEdit_Command" />
                            <box:NButton runat="server" ID="btnDel" Height="42" CommandArgument=<%# Item.IdMandato %> Icon="cancel" ToolTip="Excluir mandato" OnClientClick="return confirm('Confirma a exclusão deste mandato?\n\nATENÇÃO: Esta operação não poderá ser desfeita.');" OnCommand="btnDel_Command" />
                        </div>
                    </div>
                </div>
            </ItemTemplate>
        </box:NRepeater>
        
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Mandato</span></div>
                <box:NLabel runat="server" ID="lblNoMandato" CssClass="field" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Câmara</span></div>
                <box:NLabel runat="server" ID="lblNoCamara" CssClass="field" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Entidade</span></div>
                <box:NLabel runat="server" ID="lblEntidade" CssClass="field" />
            </div>
        </div>
    </div>

    <script src="../Content/Scripts/sidebar.js"></script>
    <script>selectButton("spl-plen"); showReload("siplen");</script>

</asp:Content>