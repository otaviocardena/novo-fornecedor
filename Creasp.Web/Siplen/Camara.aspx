﻿<%@ Page Title="Câmaras Especializadas" Resource="siplen" Language="C#" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        grdCamara.RegisterPagedDataSource((sp) => db.CamaraRep.ListaConselheiros(
            ucPeriodo.Periodo,
            cmbCamara.SelectedValue.To<int?>())
            .ToPage(sp));

        RegisterClosePopup((s, e) => grdCamara.DataBind());

        RegisterResource("siplen.camara").Disabled(btnIncluir).GridColumns(grdCamara, "alt", "del");
    }

    protected override void OnPrepareForm()
    {
        btnIncluir.Items.Clear();
        btnIncluir.Items.Add(new NDropDownItem { Text = "Coordenador", Key = CdCargo.Coordenador });
        btnIncluir.Items.Add(new NDropDownItem { Text = "Adjunto", Key = CdCargo.Adjunto });
        btnIncluir.Items.Add(new NDropDownItem { Text = "Representante", Key = CdCargo.Representante });

        ucPeriodo.Periodo = Periodo.Hoje;
        cmbCamara.Bind(db.Query<Camara>().WherePeriodo(ucPeriodo.Periodo).OrderBy(x => x.NoCamara).ToEnumerable());

        SetDefaultButton(btnPesquisar);
        grdCamara.DataBind();
    }

    protected void btnPesquisar_Click(object sender, EventArgs e)
    {
        grdCamara.DataBindPaged();
    }

    protected void grdCamara_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if(e.CommandName == "alt")
        {
            var idMandato = e.DataKey<int>(sender);
            OpenPopup("edit", "Mandato.Edit.aspx?TpMandato=R&IdMandato=" + idMandato, 750);
        }
        else if(e.CommandName == "del")
        {
            var idMandato = e.DataKey<int>(sender);
            db.CamaraRep.ExcluiMandato(idMandato);
            grdCamara.DataBind();
            ShowInfoBox("Membro excluido com sucesso");
        }
    }

    protected void btnIncluir_ItemClick(object sender, NDropDownItem e)
    {
        OpenPopup("form", $"Mandato.New.aspx?TpMandato=R&CdCargo={e.Key}&CdCamara={cmbCamara.SelectedValue}", 750);
    }

    protected void ucPeriodo_PeriodoChanged(object sender, EventArgs e)
    {
        cmbCamara.Bind(db.Query<Camara>().WherePeriodo(ucPeriodo.Periodo).OrderBy(x => x.NoCamara).ToEnumerable());
    }

</script>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <div class="mt-30 c-primary-blue">
        <span>Cameras Especializadas</span>
        <div class="right">
            <box:NButton runat="server" ID="btnPesquisar" Icon="search" Text="Pesquisar" Theme="Default" OnClick="btnPesquisar_Click" />
            <box:NDropDown runat="server" Icon="plus" Text="Incluir" ID="btnIncluir" OnItemClick="btnIncluir_ItemClick"></box:NDropDown>
        </div>
    </div>

    <div class="form-div">
        <div class="sub-form-row">
            <div class="div-sub-form form-uc">
                <div><span>Período</span></div>
                <uc:Periodo runat="server" ID="ucPeriodo" OnPeriodoChanged="ucPeriodo_PeriodoChanged" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form form-uc">
                <div><span>Câmara</span></div>
                <box:NDropDownList runat="server" ID="cmbCamara" Width="100%" />
            </div>
        </div>
    </div>

    <box:NGridView runat="server" ID="grdCamara" AllowGrouping="true" ItemType="Creasp.Siplen.Mandato" DataKeyNames="IdMandato" OnRowCommand="grdCamara_RowCommand" ShowHeader="false">
        <Columns>
            <asp:BoundField DataField="Camara.NoCamara" />
            <asp:TemplateField HeaderText="Cargo" HeaderStyle-Width="220" ItemStyle-HorizontalAlign="Right">
                <ItemTemplate>
                    <%# Item.Cargo.NoCargo %>
                    <div runat="server" visible=<%# Item.Titular != null %>>
                        <small><%# Item.Titular != null ? Item.Titular.Pessoa.NoPessoa : "" %></small>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Conselheiro">
                <ItemTemplate>
                    <div class="left-right">
                        <div>
                            <%# Item.Pessoa != null ? Item.Pessoa.NoPessoa : "<div class='space-top'><i class='icon-attention danger'></i> Não informado</div>" %>
                            <small runat="server" visible=<%# Item.Pessoa != null %>>&nbsp;&nbsp;(<a href="Hist.View.aspx?IdPessoa=<%# Item.IdPessoa %>"><%# Item.Pessoa?.NrCreasp %></a>)</small>
                        </div>
                        <div>
                            <box:NRepeater runat="server" DataSource=<%# Item.Licencas %> ItemType="Creasp.Siplen.Licenca">
                                <ItemTemplate>
                                    <box:NLabel runat="server" Theme="Danger" CssClass="space-left" Text=<%# Item.NoMotivoFmt %> ToolTip=<%# "Período: " + Item.GetPeriodo() %> />
                                </ItemTemplate>
                            </box:NRepeater>
                            <box:NLabel runat="server" Theme="Info" Visible=<%# Item.Pessoa != null %> Text=<%# Item.Pessoa?.NoTituloAbr %> ToolTip=<%# Item.Pessoa?.NoTitulo %> CssClass="no-titulo" />
                        </div>
                    </div>
                    <div class="space-top">
                        <small><%# Item.Pessoa != null ? Item.GetPeriodo().ToString(false) : "" %></small>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
            <box:NButtonField Icon="pencil" CommandName="alt" Theme="Info" ToolTip="Alterar período" EnabledExpression="Pessoa" VisibleExpression="Mensagem" />
            <box:NButtonField Icon="cancel" CommandName="del" Theme="Danger" ToolTip="Excluir" EnabledExpression="Pessoa" VisibleExpression="Mensagem" OnClientClick="return confirm('Confirma a exclusão deste mandato?')" />
        </Columns>
    </box:NGridView>

    <script src="../Content/Scripts/pagination.js"></script>
    <script>paginate()</script>

    <script src="../Content/Scripts/sidebar.js"></script>
    <script>selectButton("spl-cam"); showReload("siplen");</script>

</asp:Content>