﻿<%@ Page Title="Registro de Presença" Resource="siplen" Language="C#" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();

    private int IdEvento => Request.QueryString["IdEvento"].To<int>();

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        grdPart.RegisterDataSource(() => db.Eventos.ListaParticipantes(IdEvento, true, cmbFiltroPessoa.SelectedValue.To<TpPessoa?>()));

        RegisterResource("siplen.evento").ThrowIfNoAccess();
    }

    protected override void OnPrepareForm()
    {
        var e = db.Eventos.BuscaEvento(IdEvento, false);

        btnSalvar.Enabled = this.IsProprietario(e, "siplen.evento.admin");

        grdPart.DataBind();

        this.BackTo("Evento.View.aspx?IdEvento=" + IdEvento);
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        var participantes = db.Eventos.ListaParticipantes(IdEvento, false);

        foreach(var row in grdPart.Rows.Cast<GridViewRow>())
        {
            var idParticipante = grdPart.DataKeys[row.RowIndex].Values["IdParticipante"].To<int>();

            var p = participantes.Single(x => x.IdParticipante == idParticipante);

            p.TpPresencaTit = (row.FindControl("cmbTpPresencaTit") as NDropDownList).SelectedValue;
            p.TpPresencaSup = (row.FindControl("cmbTpPresencaSup") as NDropDownList)?.SelectedValue;
        }

        db.Eventos.RegistraPresenca(IdEvento, participantes);

        ShowInfoBox("Registro de presença confirmado");
        Redirect("Evento.View.aspx?IdEvento=" + IdEvento);
    }

    protected void cmbFiltroPessoa_SelectedIndexChanged(object sender, EventArgs e)
    {
        grdPart.DataBind();
    }

</script>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <div class="form-div">
        <div class="sub-form-row margin-top-0">
            <div class="div-sub-form">
                <div><span>Filtrar por</span></div>
                <box:NDropDownList runat="server" ID="cmbFiltroPessoa" Width="350" AutoPostBack="true" OnSelectedIndexChanged="cmbFiltroPessoa_SelectedIndexChanged">
                    <asp:ListItem Value="" Text="" Selected="True" />
                    <asp:ListItem Value="Conselheiro" Text="Conselheiros" />
                    <asp:ListItem Value="Inspetor" Text="Inspetores" />
                    <asp:ListItem Value="Funcionario" Text="Funcionários" />
                    <asp:ListItem Value="Outros" Text="Outros" />
                </box:NDropDownList>
                <box:NButton runat="server" ID="btnSalvar" Icon="ok" Text="Registrar" Theme="Primary" OnClick="btnSalvar_Click" CssClass="margin-left" />
            </div>
        </div>
    </div>

    <box:NGridView runat="server" ID="grdPart" ItemType="Creasp.Siplen.EventoParticipante" DataKeyNames="IdParticipante">
        <Columns>
            <asp:TemplateField HeaderText="Titular" HeaderStyle-Width="50%">
                <ItemTemplate>
                    <div class="left-right">
                        <div>
                            <%# Item.Titular.NoPessoa %>
                            <small runat="server" Visible=<%# Item.Titular.NrCreasp != null %>>&nbsp;&nbsp;(<%# Item.Titular.NrCreasp %>)</small>
                            <box:NLabel runat="server" Theme="Info" Visible=<%# Item.NrOrdem > 0 %> Text=<%# Item.NrOrdem + "º Suplente" %> />
                        </div>
                        <div>
                            <box:NDropDownList runat="server" ID="cmbTpPresencaTit" SelectedValue=<%# Item.TpPresencaTit %> CssClass="cmb-presenca">
                                <asp:ListItem Value="P" data-bg="#5CB85C">Presente</asp:ListItem>
                                <asp:ListItem Value="L" data-bg="#5BC0DE">Licenciado</asp:ListItem>
                                <asp:ListItem Value="J" data-bg="#5BC0DE">Justificado</asp:ListItem>
                                <asp:ListItem Value="F" data-bg="#D9534F">Falta</asp:ListItem>
                            </box:NDropDownList>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Suplente" HeaderStyle-Width="50%">
                <ItemTemplate>
                    <div class="left-right">
                        <div>
                            <%# Item.Suplente?.NoPessoa %>
                            <small runat="server" Visible=<%# Item.Suplente?.NrCreasp != null %>>&nbsp;&nbsp;(<%# Item.Suplente?.NrCreasp %>)</small>
                        </div>
                        <div>
                            <box:NDropDownList runat="server" ID="cmbTpPresencaSup" Visible=<%# Item.Suplente != null %> SelectedValue=<%# Item.TpPresencaSup %> CssClass="cmb-presenca">
                                <asp:ListItem Value=""></asp:ListItem>
                                <asp:ListItem Value="P" data-bg="#5CB85C">Presente</asp:ListItem>
                                <asp:ListItem Value="L" data-bg="#5BC0DE">Licenciado</asp:ListItem>
                                <asp:ListItem Value="J" data-bg="#5BC0DE">Justificado</asp:ListItem>
                                <asp:ListItem Value="F" data-bg="#D9534F">Falta</asp:ListItem>
                            </box:NDropDownList>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        <EmptyDataTemplate>Nenhum participante neste evento</EmptyDataTemplate>
    </box:NGridView>

    <script>
        $('.cmb-presenca')
            .each(mudaCorCombo)
            .on('click', mudaCorCombo)
            .css('border-radius', '10px')
            .find('option').css({ 'background-color': 'white', 'color': 'black' });

        function mudaCorCombo() {
            var el = $(this);
            var bg = el.find('option:selected').attr('data-bg') || 'white';
            el.css('color', bg == 'white' ? 'black' : 'white').css('background-color', bg);
        }

    </script>



</asp:Content>