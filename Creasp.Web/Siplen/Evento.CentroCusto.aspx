﻿<%@ Page Title="Centro de Custos para Eventos" Resource="siplen" Language="C#" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();

    private int IdEventoCentroCustoEdit { get { return ViewState["IdEventoCentroCustoEdit"].To<int>(); } set { ViewState["IdEventoCentroCustoEdit"] = value; } }
    private string TpMandatoEdit { get { return ViewState["TpMandatoEdit"].To<string>(); } set { ViewState["TpMandatoEdit"] = value; } }
    private int? IdRefEdit { get { return ViewState["IdRefEdit"].To<int?>(); } set { ViewState["IdRefEdit"] = value; } }

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        grdList.RegisterDataSource(() => db.Eventos.ListaCentroCustoPadrao());

        RegisterResource("siplen.evento").Disabled(btnSalvar).GridColumns(grdList, "alt", "del");
    }

    protected override void OnPrepareForm()
    {
        grdList.DataBind();

        this.BackTo("Evento.aspx");
    }

    protected void grdList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if(e.CommandName == "alt")
        {
            IdEventoCentroCustoEdit = e.DataKey<int>(sender, "IdEventoCentroCusto");
            TpMandatoEdit = e.DataKey<string>(sender, "TpMandato");
            IdRefEdit = e.DataKey<int?>(sender, "IdRef");
            lblRef.Text = grdList.Rows[e.CommandArgument.To<int>()].Cells[1].Text;
            ucCentroCusto.LoadCentroCusto(null);
            pnlEdit.Open(btnSalvar, ucCentroCusto);
        }
        else if(e.CommandName == "del")
        {
            var pk = e.DataKey<int>(sender, "IdEventoCentroCusto");
            db.Eventos.AtualizaCentroCustos(new EventoCentroCusto { IdEventoCentroCusto = pk });
            grdList.DataBind();
            ShowInfoBox("Centro de custo excluido com sucesso");
        }
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        var nrCentroCusto = ucCentroCusto.IdCentroCusto == null ? null : db.SingleById<CentroCusto>(ucCentroCusto.IdCentroCusto).NrCentroCusto;

        db.Eventos.AtualizaCentroCustos(new EventoCentroCusto
        {
            IdEventoCentroCusto = IdEventoCentroCustoEdit,
            TpMandato = TpMandatoEdit,
            IdRef = IdRefEdit,
            NrCentroCusto = nrCentroCusto
        });

        ShowInfoBox("Centro de custo gravado com sucesso");
        grdList.DataBind();
        pnlEdit.Close();
    }

</script>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <box:NTab runat="server" Text="Centro de custos" Selected="true">

        <div class="help margin-top">Vincule abaixo cada um dos items a um centro de custo. Este centro de custo vai aparecer como padrão ao adicionar um participante em um evento.</div>

        <box:NGridView runat="server" ID="grdList" AllowGrouping="true" ItemType="Creasp.Siplen.EventoCentroCusto" DataKeyNames="IdEventoCentroCusto,TpMandato,IdRef,NrCentroCusto" ShowHeader="false" OnRowCommand="grdList_RowCommand">
            <Columns>
                <asp:BoundField DataField="NoCategoria" />
                <asp:BoundField DataField="NoRef" HeaderText="Referencia" HeaderStyle-Width="550" HeaderStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="CentroCusto.NrCentroCustoFmt" HeaderText="Centro de Custo" HeaderStyle-Width="100" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                <asp:BoundField DataField="CentroCusto.NoCentroCusto" />
                <box:NButtonField Icon="pencil" ToolTip="Editar" CommandName="alt" Theme="Info" />
                <box:NButtonField Icon="cancel" ToolTip="Excluir" CommandName="del" Theme="Danger" EnabledExpression="IdEventoCentroCusto" />
            </Columns>
            <EmptyDataTemplate>Nenhum evento encontrado no filtro informado</EmptyDataTemplate>
        </box:NGridView>

    </box:NTab>

    <box:NPopup runat="server" ID="pnlEdit" Title="Alterar centro de custo" Width="650">

        <div class="form-div">
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Referência</span></div>
                    <box:NLabel runat="server" CssClass="field" ID="lblRef" />
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Centro de custo</span></div>
                    <uc:CentroCusto id="ucCentroCusto" runat="server" />
                </div>
            </div>
        </div>

        <box:NButton runat="server" ID="btnSalvar" Icon="ok" Text="Salvar" OnClick="btnSalvar_Click" CssClass="right" Theme="Primary" />

    </box:NPopup>

    <script src="../Content/Scripts/sidebar.js"></script>
    <script>selectButton("nerp-event"); showReload("nerp");</script>

</asp:Content>