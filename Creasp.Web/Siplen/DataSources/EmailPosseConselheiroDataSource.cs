﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp.Core;
using NBox.Core;

namespace Creasp.Siplen
{
    /// <summary>
    /// Email com a lista de documentos faltantes para posse de um conselheiro
    /// </summary>
    public class EmailPosseConselheiroDataSource : IEmailDataSource
    {
        protected CreaspContext db = new CreaspContext();
        protected ICreanet creanet = Factory.Get<ICreanet>();

        public string CdTemplate => "POSSE-CONSELHEIRO";
        public string[] ListaCampos { get; private set; }

        public EmailPosseConselheiroDataSource()
        {
            ListaCampos = typeof(EmailPosseDTO).GetProperties().Select(x => x.Name).Where(x => x != "Inspetoria").ToArray();
        }

        public IEnumerable<Pessoa> GetDestinatarios()
        {
            var indicacoes = db.Indicacoes.ListaPlenarioFuturo(false, DateTime.Now.Year);

            foreach(var ind in indicacoes)
            {
                if (ind.Titular != null) yield return ind.Titular;
                if (ind.Suplente != null) yield return ind.Suplente;
            }
        }

        public object GetDados(int idPessoa)
        {
            var ind = db.Query<Indicacao>()
                .Include(x => x.Titular)
                .Include(x => x.Suplente)
                .Include(x => x.Camara)
                .Include(x => x.Entidade)
                .Where(x => x.IdPessoaTit == idPessoa || x.IdPessoaSup == idPessoa)
                .Where(x => x.DtPosseTit == null || x.DtPosseSup == null)
                .Single();

            // a pessoa pode ser titular ou suplente na indicação
            var isTitular = ind.IdPessoaTit == idPessoa;
            var creasp = isTitular ? ind.Titular.NrCreasp : ind.Suplente.NrCreasp;

            // valida a documentação faltante
            var docs = db.Documentos.ListaDocumentos(TpRefDoc.Indicacao, isTitular ? ind.IdIndicacao : -ind.IdIndicacao);
            db.Documentos.AdicionaDocumentosFaltantes(docs, TpRefDoc.Conselheiro);

            var d = new EmailPosseDTO
            {
                NomePessoa =  isTitular ? ind.Titular.NoPessoa : ind.Suplente.NoPessoa,
                Creasp = creasp,
                Cargo = isTitular ? "Titular" : "Suplente",
                Camara = ind.Camara.NoCamara,
                Entidade = ind.Entidade.NoEntidade,
                DocumentosFaltantes = docs.Where(x => x.FileId == null).Select(x => x.Tipo.NoTipoDoc).ToArray(),
                EmDia = creanet.ConsultaAnuidade(creasp) == AnuidadeSituacao.EmDia
            };

            d.DocumentacaoOK = d.DocumentosFaltantes.Count() == 0;
            d.EmDebito = !d.EmDia;

            return d;
        }
    }
}