﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Creasp.Core;
using NBox.Core;
using NBox.Web.Controls;
using NPoco.Linq;

namespace Creasp.Siplen
{
    /// <summary>
    /// Relatorio DW para indicações a plenário
    /// </summary>
    public class DWIndicacaoDataSource : DWDataSource<Indicacao>
    {
        public override string Nome => "Representacao";
        public override string Descricao => "Dados de representação ao plenário (uma linha por representação titular/suplente)";
        public override bool PorPeriodo => false;

        public override void InicializaCampos()
        {
            Add("Mandato")
                .Coluna((c) => c.AnoIni + " - " + c.AnoFim)
                .Filtro((c, v) => c.AnoIni == v.To<int>())
                .Combo((db, p) =>
                {
                    var list = new List<ListItem>();
                    for(var i = DateTime.Today.Year + 1; i >= 2000; i--)
                    {
                        list.Add(new ListItem(string.Format("{0} - {1}", i, i + 2), i.ToString()));
                    }
                    return list;
                });

            Add("Ano")
                .Filtro((c, v) => 
                c.AnoIni <= v.To<int>() && c.AnoFim >= v.To<int>())
                .Combo((db, p) =>
                {
                    var list = new List<ListItem>();
                    for (var i = DateTime.Today.Year + 1; i >= 2000; i--)
                    {
                        list.Add(new ListItem(i.ToString(), i.ToString()));
                    }
                    return list;
                });

            Add("Câmara")
                .Coluna((c) => c.Camara.NoCamara)
                .Filtro((c, v) => c.Camara.CdCamara == v.To<int>())
                .Combo((db, p) => db.Camaras.ListaCamaras(p).Select(k => new ListItem { Text = k.NoCamara, Value = k.CdCamara.ToString() }));

            Add("Indicação")
                .Filtro((c, v) => c.Titular == null && c.Suplente == null)
                .Combo((db, p) => new ListItem[] { new ListItem { Text = "Sem indicação de conselheiro", Value = "X" } });

            Add("Histórico", false)
                .Coluna((c) => c.NoHistorico);

            Add("Entidade")
                .Coluna((c) => c.Entidade.NoEntidade);

            Add("Tipo entidade")
                .Coluna((c) => c.Entidade.TpEntidade == "I" ? "Instituição de Ensino" : "Entidade de classe");

            Add("CNPJ Entidade")
                .Coluna((c) => c.Entidade.NrCnpj.Mask("99.999.999/9999-99"))
                .Mask(MaskType.Custom, "99.999.999/9999-99");

            Add("Titular: Nome")
                .Coluna((c) => c.Titular?.NoPessoa);

            Add("Titular: Data de posse")
                .Coluna((c) => c.DtPosseTit);

            Add("Titular: Data de nascimento")
                .Coluna((c) => c.Titular?.DtNasc);

            Add("Titular: Email")
                .Coluna((c) => c.Titular?.NoEmail);

            Add("Titular: Endereço de ressarcimento", false)
                .Coluna((c) => c.Titular?.EnderecoFmt);

            Add("Titular: Endereço de correspondência", false)
                .Coluna((c) => c.Titular?.NoEndCorresp);

            Add("Titular: Telefones")
                .Coluna((c) => c.Titular?.NrTelefone);

            Add("Titular: Sexo")
                .Coluna((c) => c.Titular?.TpSexo)
                .Filtro((c, v) => c.Titular?.TpSexo == v)
                .Combo((db, p) => new ListItem[] { new ListItem { Text = "Masculino", Value = TpSexo.Masculino }, new ListItem { Text = "Feminino", Value = TpSexo.Feminino } });
            
            Add("Titular: CREASP")
                .Coluna((c) => c.Titular?.NrCreasp);
            
            Add("Titular: Matrícula")
                .Coluna((c) => c.Titular?.NrMatricula);

            Add("Titular: CPF")
                .Coluna((c) => c.Titular?.NrCpfFmt);

            Add("Titular: Titulos")
                .Coluna((c) => c.Titular?.NoTituloAbr);

            Add("Titular: Histórico")
                .Coluna((c) => c.Titular?.NoHistorico);

            // ====> SUPLENTE


            Add("Suplente: Nome")
                .Coluna((c) => c.Suplente?.NoPessoa);

            Add("Suplente: Data de posse")
                .Coluna((c) => c.DtPosseSup);
            
            Add("Suplente: Data de nascimento")
                .Coluna((c) => c.Suplente?.DtNasc);

            Add("Suplente: Email")
                .Coluna((c) => c.Suplente?.NoEmail);

            Add("Suplente: Endereço de ressarcimento", false)
                .Coluna((c) => c.Suplente?.EnderecoFmt);

            Add("Suplente: Endereço de correspondência", false)
                .Coluna((c) => c.Suplente?.NoEndCorresp);

            Add("Suplente: Telefones")
                .Coluna((c) => c.Suplente?.NrTelefone);

            Add("Suplente: Sexo")
                .Coluna((c) => c.Suplente?.TpSexo)
                .Filtro((c, v) => c.Suplente?.TpSexo == v)
                .Combo((db, p) => new ListItem[] { new ListItem { Text = "Masculino", Value = TpSexo.Masculino }, new ListItem { Text = "Feminino", Value = TpSexo.Feminino } });
            
            Add("Suplente: CREASP")
                .Coluna((c) => c.Suplente?.NrCreasp);

            Add("Suplente: Matrícula")
                .Coluna((c) => c.Suplente?.NrMatricula);

            Add("Suplente: CPF")
                .Coluna((c) => c.Suplente?.NrCpfFmt);

            Add("Suplente: Titulos")
                .Coluna((c) => c.Suplente?.NoTitulo);

            Add("Suplente: Histórico")
                .Coluna((c) => c.Suplente?.NoHistorico);
        }

        public override IEnumerable<Indicacao> GetDados(CreaspContext db, Periodo periodo)
        {
            return db.Query<Indicacao>()
                .Include(x => x.Titular, JoinType.Left)
                .Include(x => x.Suplente, JoinType.Left)
                .Include(x => x.Entidade)
                .Include(x => x.Camara)
                .OrderBy(x => x.AnoIni).ThenBy(x => x.Camara.NoCamara).ThenBy(x => x.Titular.NoPessoa)
                .ToList();
        }
    }
}