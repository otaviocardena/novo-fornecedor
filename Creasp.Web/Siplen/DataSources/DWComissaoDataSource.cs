﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Creasp.Core;
using NBox.Core;
using NBox.Web.Controls;

namespace Creasp.Siplen
{
    /// <summary>
    /// Relatorio DW para comissão
    /// </summary>
    public class DWComissaoDataSource : DWDataSource<TitularSuplenteDTO>
    {
        public override string Nome => "Comissão";
        public override string Descricao => "Dados de membros nas comissões (titular e suplente de comissão na mesma linha)";
        public override bool PorPeriodo => true;

        public override void InicializaCampos()
        {
            Add("Nome da comissão")
                .Coluna((c) => c.Comissao.NoComissao)
                .Filtro((c, v) => c.Comissao.IdComissao == v.To<int>())
                .Combo((db, p) => db.Comissoes.ListaComissoes(p, null).Select(x => new ListItem { Text = x.NoComissao, Value = x.IdComissao.ToString() }));

            Add("Comissão permanente?")
                .Coluna((c) => c.Comissao.FgPermanente ? "S" : "N")
                .Filtro((c, v) => c.Comissao.FgPermanente == v.To<bool>())
                .Combo((db, p) => new ListItem[] { new ListItem { Text = "Sim", Value = "true" }, new ListItem { Text = "Não", Value = "false" } });

            Add("Data de inicio da comissão")
                .Coluna((c) => c.Comissao.DtIni);

            Add("Data de final da comissão")
                .Coluna((c) => c.Comissao.DtFim);

            Add("Cargo")
                .Coluna((c) => c.Titular.Cargo.NoCargo);

            Add("Ordem")
                .Coluna((c) => c.Titular.NrOrdem);

            // ==== TITULAR
            Add("Titular: Data inicio na comissão")
                .Coluna((c) => c.Titular.DtIni);

            Add("Titular: Data fim na comissão")
                .Coluna((c) => c.Titular.DtFim);

            Add("Titular: CREASP")
                .Coluna((c) => c.Titular.Pessoa.NrCreasp)
                .Filtro((c, v) => c.Titular.Pessoa.NrCreasp == v)
                .Mask(MaskType.Custom, "R9999999999");

            Add("Titular: CPF")
                .Coluna((c) => c.Titular.Pessoa.NrCpfFmt)
                .Filtro((c, v) => c.Titular.Pessoa.NrCpf == v)
                .Mask(MaskType.Custom, "999.999.999-99");

            Add("Titular: Titulos")
                .Coluna((c) => c.Titular.Pessoa.NoTituloAbr);

            Add("Titular: Nome do conselheiro")
                .Coluna((c) => c.Titular.Pessoa.NoPessoa)
                .Filtro((c, v) => c.Titular.Pessoa.NoPessoa.StartsWith(v));

            Add("Titular: faltas").Desc("Titular: Quantidade de faltas")
                .Coluna((c) => c.Titular.NrFaltas);

            // ==== SUPLENTE

            Add("Suplente: Data inicio na comissão")
                .Coluna((c) => c.Suplente?.DtIni);

            Add("Suplente: Data fim na comissão")
                .Coluna((c) => c.Suplente?.DtFim);

            Add("Suplente: CREASP")
                .Coluna((c) => c.Suplente?.Pessoa.NrCreasp)
                .Filtro((c, v) => c.Suplente?.Pessoa.NrCreasp == v)
                .Mask(MaskType.Custom, "R9999999999");

            Add("Suplente: CPF")
                .Coluna((c) => c.Suplente?.Pessoa.NrCpfFmt)
                .Filtro((c, v) => c.Suplente?.Pessoa.NrCpf == v)
                .Mask(MaskType.Custom, "999.999.999-99");

            Add("Suplente: Titulos")
                .Coluna((c) => c.Suplente?.Pessoa.NoTituloAbr);

            Add("Suplente: Nome do conselheiro")
                .Coluna((c) => c.Suplente?.Pessoa.NoPessoa)
                .Filtro((c, v) => c.Suplente?.Pessoa.NoPessoa.StartsWith(v) ?? false);

            Add("Suplente: faltas").Desc("Suplente: Quantidade de faltas")
                .Coluna((c) => c.Suplente?.NrFaltas);

        }

        public override IEnumerable<TitularSuplenteDTO> GetDados(CreaspContext db, Periodo periodo)
        {
            var srv = new MandatoService(db);

            var mandatos = db.Query<Mandato>()
                .Include(x => x.Pessoa)
                .Include(x => x.Pessoa.Cep)
                .Include(x => x.Cargo)
                .Include(x => x.Comissao)
                .Where(x => x.TpMandato == TpMandato.Comissao)
                .WherePeriodo(periodo)
                .OrderBy(x => x.Comissao.NoComissao)
                .ToList();

            // conta as faltas
            mandatos = srv.ContaFaltas(mandatos, TpMandato.Comissao, periodo.Fim).ToList();

            return srv.MontaTitularSuplente(mandatos).ToList()
                .OrderBy(x => x.Comissao.NoComissao)
                .ThenBy(x => x.Titular.Cargo?.NrOrdem)
                .ThenBy(x => x.Titular?.NrOrdem)
                .ThenBy(x => x.Titular?.Pessoa?.NoPessoa);
        }
    }
}