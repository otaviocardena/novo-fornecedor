﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp.Core;
using NBox.Core;

namespace Creasp.Siplen
{
    /// <summary>
    /// Email com a lista de documentos faltantes para posse de um inspetor
    /// </summary>
    public class EmailPosseInspetorDataSource : IEmailDataSource
    {
        protected CreaspContext db = new CreaspContext();
        protected ICreanet creanet = Factory.Get<ICreanet>();

        public string CdTemplate => "POSSE-INSPETOR";
        public string[] ListaCampos { get; private set; }

        public EmailPosseInspetorDataSource()
        {
            ListaCampos = typeof(EmailPosseDTO).GetProperties().Select(x => x.Name).Where(x => x != "Entidade").ToArray();
        }

        public IEnumerable<Pessoa> GetDestinatarios()
        {
            var mandatos = db.Inspetores.ListaInspetores(Pager.NoPage, Periodo.Vazio, null, 0, null, null).Items;

            return mandatos.Select(x => x.Pessoa);
        }

        public object GetDados(int idPessoa)
        {
            var m = db.Query<Mandato>()
                .Include(x => x.Pessoa)
                .Include(x => x.Inspetoria)
                .Include(x => x.Camara)
                .Include(x => x.Cargo)
                .Where(x => x.TpMandato == TpMandato.Inspetor)
                .Where(x => x.IdPessoa == idPessoa)
                .Where(x => x.DtIni == Periodo.MinDate && x.DtFim == Periodo.MaxDate)
                .Single();

            var docs = db.Documentos.ListaDocumentos(TpRefDoc.Inspetor, m.IdMandato);
            db.Documentos.AdicionaDocumentosFaltantes(docs, TpRefDoc.Inspetor);

            var d = new EmailPosseDTO
            {
                NomePessoa = m.Pessoa.NoPessoa,
                Creasp = m.Pessoa.NrCreasp,
                Cargo = m.Cargo.NoCargo,
                Camara = m.Camara.NoCamara,
                Inspetoria = m.Inspetoria.NoInspetoria,
                DocumentosFaltantes = docs.Where(x => x.FileId == null).Select(x => x.Tipo.NoTipoDoc).ToArray(),
                EmDia = creanet.ConsultaAnuidade(m.Pessoa.NrCreasp) == AnuidadeSituacao.EmDia
            };

            d.DocumentacaoOK = d.DocumentosFaltantes.Count() == 0;
            d.EmDebito = !d.EmDia;

            return d;
        }
    }
}