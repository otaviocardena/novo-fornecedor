﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Creasp.Core;
using NBox.Core;
using NBox.Web.Controls;

namespace Creasp.Siplen
{
    /// <summary>
    /// Relatorio DW para plenario
    /// </summary>
    public class DWPlenarioDataSource : DWDataSource<TitularSuplenteDTO>
    {
        public override string Nome => "Plenario";
        public override string Descricao => "Dados de plenário aonde titular e o suplente aparecem na mesma linha";
        public override bool PorPeriodo => true;

        public override void InicializaCampos()
        {
            Add("Mandato")
                .Coluna((c) => c.AnoIni + " - " + c.AnoFim)
                .Filtro((c, v) => c.AnoIni == v.To<int>())
                .Combo((db, p) =>
                {
                    var list = new List<ListItem>();
                    for (var i = DateTime.Today.Year + 1; i >= 2000; i--)
                    {
                        list.Add(new ListItem(string.Format("{0} - {1}", i, i + 2), i.ToString()));
                    }
                    return list;
                });

            Add("Câmara")
                .Coluna((c) => c.Camara.NoCamara)
                .Filtro((c, v) => c.Camara.CdCamara == v.To<int>())
                .Combo((db, p) => db.Camaras.ListaCamaras(p).Select(k => new ListItem { Text = k.NoCamara, Value = k.CdCamara.ToString() }));
                        
            Add("Entidade")
                .Coluna((c) => c.Entidade.NoEntidade)
                .Filtro((c, v) => c.Entidade.IdEntidade == v.To<int>())
                .Combo((db, p) => db.Entidades.ListaEntidades().Select(k => new ListItem { Text = k.NoEntidade, Value = k.IdEntidade.ToString() }));

            Add("Tipo entidade")
                .Coluna((c) => c.Entidade.TpEntidade == "I" ? "Instituição de Ensino" : "Entidade de classe");

            Add("CNPJ Entidade")
                .Coluna((c) => c.Entidade.NrCnpj.Mask("99.999.999/9999-99"))
                .Mask(MaskType.Custom, "99.999.999/9999-99");

            Add("Ativo").Desc("Conselheiro ativo (período DEVE ser apenas de apenas 1 dia)")
                .Coluna((c) => c.Titular.Licencas.Count == 0 ? "Titular" : (c.Suplente?.Licencas.Count == 0 ? "Suplente" : "-"));

            // ==== TITULAR
            
            Add("Titular: Nome do conselheiro")
                .Coluna((c) => c.Titular.Pessoa.NoPessoa)
                .Filtro((c, v) => c.Titular.Pessoa.NoPessoa.StartsWith(v));

            Add("Titular: Data de Nascimento")
                .Coluna((c) => c.Titular.Pessoa.DtNasc);

            Add("Titular: Email")
                .Coluna((c) => c.Titular.Pessoa.NoEmail);

            Add("Titular: Endereço ressarcimento")
                .Coluna((c) => c.Titular.Pessoa.EnderecoFmt);

            Add("Titular: Endereço correspondência")
                .Coluna((c) => c.Titular.Pessoa.NoEndCorresp);

            Add("Titular: Telefones")
                .Coluna((c) => c.Titular.Pessoa.NrTelefone);

            Add("Titular: Sexo")
                .Coluna((c) => c.Titular.Pessoa.TpSexo == "M" ? "Masculino" : "Feminino")
                .Filtro((c, v) => c.Titular.Pessoa.TpSexo == v)
                .Combo((db, p) => new ListItem[] { new ListItem { Text = "Masculino", Value = TpSexo.Masculino }, new ListItem { Text = "Feminino", Value = TpSexo.Feminino } });

            Add("Titular: CREASP")
                .Coluna((c) => c.Titular.Pessoa.NrCreasp)
                .Filtro((c, v) => c.Titular.Pessoa.NrCreasp == v)
                .Mask(MaskType.Custom, "R9999999999");

            Add("Titular: Matrícula")
                .Coluna((c) => c.Titular.Pessoa.NrMatricula);

            Add("Titular: CPF")
                .Coluna((c) => c.Titular.Pessoa.NrCpfFmt)
                .Filtro((c, v) => c.Titular.Pessoa.NrCpf == v)
                .Mask(MaskType.Custom, "999.999.999-99");

            Add("Titular: Titulos")
                .Coluna((c) => c.Titular.Pessoa.NoTituloAbr);

            Add("Titular: Data inicio mandato")
                .Coluna((c) => c.Titular.DtIni);

            Add("Titular: Data fim mandato")
                .Coluna((c) => c.Titular.DtFim);

            Add("Titular: Histórico", false)
                .Coluna((c) => c.Titular.Pessoa.NoHistorico);

            Add("Titular: Licenças", false).Desc("Licenças no período")
                .Coluna((c) => string.Join("\n", c.Titular.Licencas.Select(x => x.GetPeriodo() + " - " + x.NoMotivo)));

            Add("Titular: Faltas").Desc("Titular: Quantidade de faltas")
                .Coluna((c) => c.Titular.NrFaltas);

            // ==== SUPLENTE

            Add("Suplente: Nome do conselheiro")
                .Coluna((c) => c.Suplente?.Pessoa.NoPessoa)
                .Filtro((c, v) => c.Suplente?.Pessoa.NoPessoa.StartsWith(v) ?? false);

            Add("Suplente: Data de Nascimento")
                .Coluna((c) => c.Suplente?.Pessoa.DtNasc);

            Add("Suplente: Email")
                .Coluna((c) => c.Suplente?.Pessoa.NoEmail);

            Add("Suplente: Endereço ressarcimento")
                .Coluna((c) => c.Suplente?.Pessoa.EnderecoFmt);

            Add("Suplente: Endereço correspondência")
                .Coluna((c) => c.Suplente?.Pessoa.NoEndCorresp);

            Add("Suplente: Telefones")
                .Coluna((c) => c.Suplente?.Pessoa.NrTelefone);

            Add("Suplente: Sexo")
                .Coluna((c) => c.Suplente?.Pessoa.TpSexo == "M" ? "Masculino" : "Feminino");

            Add("Suplente: CREASP")
                .Coluna((c) => c.Suplente?.Pessoa.NrCreasp)
                .Filtro((c, v) => c.Suplente?.Pessoa.NrCreasp == v)
                .Mask(MaskType.Custom, "R9999999999");

            Add("Suplente: Matrícula")
                .Coluna((c) => c.Suplente?.Pessoa.NrMatricula);

            Add("Suplente: CPF")
                .Coluna((c) => c.Suplente?.Pessoa.NrCpfFmt)
                .Filtro((c, v) => c.Suplente?.Pessoa.NrCpf == v)
                .Mask(MaskType.Custom, "999.999.999-99");

            Add("Suplente: Titulos")
                .Coluna((c) => c.Suplente?.Pessoa.NoTituloAbr);

            Add("Suplente: Data inicio mandato")
                .Coluna((c) => c.Suplente?.DtIni);

            Add("Suplente: Data fim mandato")
                .Coluna((c) => c.Suplente?.DtFim);
            
            Add("Suplente: Histórico", false)
                .Coluna((c) => c.Suplente?.Pessoa.NoHistorico);

            Add("Suplente: Licenças", false).Desc("Licenças no período")
                .Coluna((c) => c.Suplente != null ? string.Join("\n", c.Suplente.Licencas.Select(x => x.GetPeriodo() + " - " + x.NoMotivo)) : "");

            Add("Suplente: faltas").Desc("Suplente: Quantidade de faltas")
                .Coluna((c) => c.Suplente?.NrFaltas);

        }

        public override IEnumerable<TitularSuplenteDTO> GetDados(CreaspContext db, Periodo periodo)
        {
            return db.Conselheiros.ListaPlenario(periodo, null);
        }
    }
}