﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using Creasp.Core;
using NBox.Core;
using NBox.Web.Controls;

namespace Creasp.Siplen
{
    /// <summary>
    /// Relatorio DW para mandato de conselheiro
    /// </summary>
    public class DWCamaraDataSource : DWDataSource<Mandato>
    {
        public override string Nome => "CamaraEsp";
        public override string Descricao => "Dados de coordenadores, representantes e membros das câmaras especializadas (um mandato por linha)";
        public override bool PorPeriodo => true;

        public override void InicializaCampos()
        {
            Add("Mandato")
                .Coluna((c) => c.AnoIni + " - " + c.AnoFim)
                .Filtro((c, v) => c.AnoIni == v.To<int>())
                .Combo((db, p) =>
                {
                    var list = new List<ListItem>();
                    for (var i = DateTime.Today.Year + 1; i >= 2000; i--)
                    {
                        list.Add(new ListItem(string.Format("{0} - {1}", i, i + 2), i.ToString()));
                    }
                    return list;
                });

            Add("Câmara")
                .Coluna((c) => c.Camara.NoCamara)
                .Filtro((c, v) => c.Camara.CdCamara == v.To<int>())
                .Combo((db, p) => db.Camaras.ListaCamaras(p).Select(k => new ListItem { Text = k.NoCamara, Value = k.CdCamara.ToString() }));

            Add("Nome do conselheiro")
                .Coluna((c) => c.Pessoa.NoPessoa)
                .Filtro((c, v) => c.Pessoa?.NoPessoa.StartsWith(v) ?? false);

            Add("Cargo")
                .Coluna((c) => c.Cargo.NoCargo);

            Add("Tipo")
                .Filtro((c, v) => 
                    v.To<string>() == "T" ? (c.TpMandato == TpMandato.Camara) : 
                    v.To<string>() == "C" ? (c.TpMandato == TpMandato.Camara && (c.CdCargo == CdCargo.Coordenador || c.CdCargo == CdCargo.Adjunto)) :
                    v.To<string>() == "R" ? (c.TpMandato == TpMandato.Camara && c.CdCargo == CdCargo.Representante) : true)
                .Combo((db, p) => new ListItem[]
                {
                    new ListItem { Text = "Coordenadores, representantes e membros", Value = "X" },
                    new ListItem { Text = "Coordenadores e representantes", Value = "T" },
                    new ListItem { Text = "Somente coordenadores", Value = "C" },
                    new ListItem { Text = "Somente representantes", Value = "R" }
                });

            Add("Data de nascimento")
                .Coluna((c) => c.Pessoa.DtNasc);

            Add("Email")
                .Coluna((c) => c.Pessoa.NoEmail);

            Add("Endereço de ressarcimento", false)
                .Coluna((c) => c.Pessoa.EnderecoFmt);

            Add("Endereço de correspondência", false)
                .Coluna((c) => c.Pessoa.NoEndCorresp);

            Add("Telefones")
                .Coluna((c) => c.Pessoa.NrTelefone);

            Add("Sexo")
                .Coluna((c) => c.Pessoa.TpSexo)
                .Filtro((c, v) => c.Pessoa.TpSexo == v)
                .Combo((db, p) => new ListItem[] { new ListItem { Text = "Masculino", Value = TpSexo.Masculino }, new ListItem { Text = "Feminino", Value = TpSexo.Feminino } });

            Add("CREASP")
                .Coluna((c) => c.Pessoa.NrCreasp)
                .Filtro((c, v) => c.Pessoa.NrCreasp == v)
                .Mask(MaskType.Custom, "R9999999999");

            Add("Matrícula")
                .Coluna((c) => c.Pessoa.NrMatricula);

            Add("CPF")
                .Coluna((c) => c.Pessoa.NrCpfFmt)
                .Filtro((c, v) => c?.Pessoa.NrCpf == v)
                .Mask(MaskType.Custom, "999.999.999-99");

            Add("Titulos")
                .Coluna((c) => c.Pessoa.NoTituloAbr);

            Add("Data inicio")
                .Coluna((c) => c.DtIni);

            Add("Data fim")
                .Coluna((c) => c.DtFim);

            Add("Faltas").Desc("Quantidade de faltas")
                .Coluna((c) => c.NrFaltas);

            Add("Histórico", false)
                .Coluna((c) => c.Pessoa.NoHistorico);

            Add("Licenciado").Desc("Licenciado (período DEVE ser apenas de apenas 1 dia)")
                .Coluna((c) => c.Licencas.Count == 0 ? "Sim" : "Não")
                .Filtro((c, v) => c.Licencas.Count >= v.To<int>())
                .Combo((db, p) => new ListItem[] { new ListItem { Text = "Sim", Value = "1" }, new ListItem { Text = "Não", Value = "0" } });

            Add("Licenças", false).Desc("Licenças no período")
                .Coluna((c) => string.Join("\n", c.Licencas.Select(x => x.GetPeriodo() + " - " + x.NoMotivo)));

        }

        public override IEnumerable<Mandato> GetDados(CreaspContext db, Periodo periodo)
        {
            return db.CamaraRep.ListaConselheiros(periodo, null)
                .Where(x => x.Pessoa != null);
        }
    }
}