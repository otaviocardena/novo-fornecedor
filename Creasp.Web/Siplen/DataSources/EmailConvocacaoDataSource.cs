﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp.Core;
using NBox.Core;

namespace Creasp.Siplen
{
    /// <summary>
    /// Representa origem de dados para os emails da Lista de Presença de um Evento
    /// </summary>
    public class EmailConvocacaoDataSource : IEmailDataSource
    {
        protected CreaspContext db = new CreaspContext();
        protected ICreanet creanet = Factory.Get<ICreanet>();
        protected Evento evento;

        public string CdTemplate => "EVENTO";
        public string[] ListaCampos { get; private set; }

        public EmailConvocacaoDataSource(int idEvento)
        {
            evento = db.Eventos.BuscaEvento(idEvento, false);
            ListaCampos = typeof(EmailConvocacaoDTO).GetProperties().Select(x => x.Name).ToArray();
        }

        public IEnumerable<Pessoa> GetDestinatarios()
        {
            return db.Eventos.ListaParticipantes(evento.IdEvento, false).Select(x => x.Titular);
        }

        public object GetDados(int idPessoa)
        {
            var p = db.Query<EventoParticipante>()
                .Include(x => x.Titular)
                .Where(x => x.IdEvento == evento.IdEvento)
                .Where(x => x.IdPessoaTit == idPessoa).Single();

            var d = new EmailConvocacaoDTO
            {
                NomeEvento = evento.NoEvento,
                DataEvento = evento.DtEvento.ToString("d"),
                HoraEvento = evento.DtEvento.ToString("HH:mm"),
                LocalEvento = evento.NoLocal + evento.Cep == null ? "" : (" (" + evento.Cep.NoCidade + " / " + evento.Cep.CdUf + ")"),
                NomePessoa = p.Titular.NoPessoa,
                EmDia = true,
                EmDebito = false
            };

            if (p.Titular.NrCreasp != null)
            {
                d.EmDia = creanet.ConsultaAnuidade(p.Titular.NrCreasp) == AnuidadeSituacao.EmDia;
                d.EmDebito = !d.EmDia;
            }

            return d;
        }
    }
}