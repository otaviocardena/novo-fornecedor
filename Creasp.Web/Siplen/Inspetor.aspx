﻿<%@ Page Title="Inspetores" Resource="siplen" Language="C#" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        grdList.RegisterPagedDataSource((sp) =>
        {
            grdList.Columns[0].Visible = chkSemPosse.Checked;
            grdList.Columns.Cast<DataControlField>().Last().Visible = chkSemPosse.Checked;

            return db.Inspetores.ListaInspetores(sp, ucPeriodo.Periodo,
                txtNoPessoa.Text.To<string>(),
                chkSemPosse.Checked ? 0 : cmbMandato.SelectedValue.To<int?>(),
                txtInspetoria.Value.To<int?>(),
                cmbCamara.SelectedValue.To<int?>(),
                cmbCargo.SelectedValue.To<string>());
        });

        txtInspetoria.RegisterDataSource((q) =>
            db.Query<Inspetoria>()
                .Include(x => x.Unidade)
                .WherePeriodo(Periodo.Hoje)
                .Where(x => x.NoInspetoria.StartsWith(q))
                .Limit(10)
                .ProjectTo(x => new { x.IdInspetoria, x.NoInspetoria, NoUnidade = x.Unidade.NoUnidade }));

        RegisterRestorePageState(() => grdList.DataBind());

        RegisterResource("siplen.inspetor").Disabled(lnkNova, btnPosse, btnNotificar, chkSemPosse).GridColumns(grdList, "del");
    }

    protected override void OnPrepareForm()
    {
        ucPeriodo.Periodo = Periodo.Hoje;

        cmbCamara.Bind(db.Camaras.ListaCamaras(ucPeriodo.Periodo));

        cmbMandato.Items.Add(new ListItem("(Todos)", ""));
        cmbMandato.Items.AddRange(InspetorService.Mandatos.Select(x => new ListItem(string.Format("{0} - {1}", x, x + 2), x.ToString())).ToArray());
        cmbMandato.SelectedValue = InspetorService.MandatoAtual.ToString();

        cmbCargo.Items.Add(new ListItem("(Todos)", ""));
        cmbCargo.Items.Add(new ListItem("Inspetor", CdCargo.Inspetor));
        cmbCargo.Items.Add(new ListItem("Inspetor Chefe", CdCargo.InspetorChefe));
        cmbCargo.Items.Add(new ListItem("Inspetor Especial", CdCargo.InspetorEspecial));


        grdList.DataBind();

        SetDefaultButton(btnPesquisar);
    }

    protected void btnPesquisar_Click(object sender, EventArgs e)
    {
        grdList.DataBindPaged();
    }

    protected void ucPeriodo_PeriodoChanged(object sender, EventArgs e)
    {
        cmbCamara.Bind(db.Camaras.ListaCamaras(ucPeriodo.Periodo));
    }

    protected void grdList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if(e.CommandName == "alt")
        {
            var idMandato = e.DataKey<int>(sender);
            Redirect("Inspetor.Form.aspx?IdMandato=" + idMandato);
        }
        else if(e.CommandName == "del")
        {
            var idMandato = e.DataKey<int>(sender);
            db.Inspetores.ExcluiMandato(idMandato);
            grdList.DataBind();
            ShowInfoBox("Mandato de inspetor excluido com sucesso");
        }
    }

    protected void chkSemPosse_CheckedChanged(object sender, EventArgs e)
    {
        btnNotificar.Enabled = btnPosse.Enabled = chkSemPosse.Checked;
        ucPeriodo.Enabled = cmbMandato.Enabled = !chkSemPosse.Checked;

        if (chkSemPosse.Checked)
        {
            ucPeriodo.Periodo = Periodo.Vazio;
            cmbMandato.SelectedValue = "";
        }
        else
        {
            ucPeriodo.Periodo = cmbMandato.HasValue() ?
                Periodo.Anos(cmbMandato.SelectedValue.To<int>(), cmbMandato.SelectedValue.To<int>() + 2) :
                new Periodo("EsteAno");

            cmbMandato.SelectedValue = InspetorService.MandatoAtual.ToString();
        }

        grdList.DataBind();
    }

    protected void btnPosse_Click(object sender, EventArgs e)
    {
        if (grdList.SelectedIndexes.Count() == 0) throw new NBoxException("Nenhum inspetor indicado selecionado");

        pnlPosse.Open(btnPosseOK, txtDtPosse);
    }

    protected void btnPosseOK_Click(object sender, EventArgs e)
    {
        var ids = grdList.SelectedDataKeys.Select(x => x.Value.To<int>());

        db.Inspetores.Posse(ids, txtDtPosse.Text.To<DateTime>());

        ShowInfoBox("Operação realizada com sucesso");
        pnlPosse.Close();
        grdList.DataBind();
    }

    protected void btnCancelar_Click(object sender, EventArgs e)
    {
        pnlPosse.Close();
    }

    protected void btnNotificar_Click(object sender, EventArgs e)
    {
        ucEmail.AbrePopup("PosseInspetor");
    }

</script>

<asp:Content ContentPlaceHolderID="content" runat="server">

    <div class="mt-30 c-primary-blue">
        <span>Inspetores</span>
        <box:NHyperLink runat="server" ID="lnkNova" ButtonTheme="Success" Icon="plus" CssClass="right new ml-10" Text="Nova indicação" NavigateUrl="Inspetor.Form.aspx" />
        <box:NButton runat="server" ID="btnPesquisar" Icon="search" Text="Pesquisar" CssClass="right ml-10" Theme="Default" OnClick="btnPesquisar_Click" />
        <box:NButton runat="server" ID="btnPosse" Icon="menu" Text="Posse" CssClass="right ml-10" OnClick="btnPosse_Click" Enabled="false" />
        <box:NButton runat="server" ID="btnNotificar" Icon="mail" Text="Notificar" CssClass="right" OnClick="btnNotificar_Click" ToolTip="Enviar email de solicitação de documentação para posse de inspetores" Enabled="false" />
    </div>

    <div class="form-div">
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Período</span></div>
                <uc:Periodo runat="server" ID="ucPeriodo" OnPeriodoChanged="ucPeriodo_PeriodoChanged" />
            </div>
            <div class="div-sub-form">
                <div><span>Mandato</span></div>
                <box:NDropDownList runat="server" ID="cmbMandato" />
                &nbsp;&nbsp;
                <box:NCheckBox runat="server" ID="chkSemPosse" Checked="false" Text="Apenas indicações (sem posse)" AutoPostBack="true" OnCheckedChanged="chkSemPosse_CheckedChanged" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Nome</span></div>
                <box:NTextBox runat="server" ID="txtNoPessoa" TextCapitalize="Upper" Width="100%" autofocus />
            </div>
            <div class="div-sub-form ml-10">
                <div><span>Município</span></div>
                <box:NAutocomplete runat="server" ID="txtInspetoria" Width="100%" TextCapitalize="Upper"
                    DataValueField="IdInspetoria" DataTextField="NoInspetoria"
                    Template="{NoInspetoria} - {NoUnidade}" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Câmara</span></div>
                <box:NDropDownList runat="server" ID="cmbCamara" Width="100%" />
            </div>
            <div class="div-sub-form ml-10">
                <div><span>Cargo</span></div>
                <box:NDropDownList runat="server" ID="cmbCargo" Width="100%" />
            </div>
        </div>
    </div>

    <box:NGridView runat="server" ID="grdList" ItemType="Creasp.Siplen.Mandato" DataKeyNames="IdMandato" OnRowCommand="grdList_RowCommand">
        <Columns>
            <box:NCheckBoxField EnabledExpression="IsIndicado && !Mensagem" />
            <asp:TemplateField HeaderStyle-Width="50" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate><%# Item.AnoIni + " " + Item.AnoFim %></span>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Inspetor" SortExpression="Pessoa.NoPessoa">
                <ItemTemplate>
                    <div class="left-right">
                        <div>
                            <%# Item.Pessoa.NoPessoa %>
                            <small>&nbsp;&nbsp;(<a href="Hist.View.aspx?IdPessoa=<%# Item.Pessoa.IdPessoa %>"><%# Item.Pessoa.NrCreasp %></a>)</small>
                            <box:NLabel runat="server" Theme=<%# Item.CdCargo == CdCargo.InspetorChefe ? ThemeStyle.Success : ThemeStyle.Warning %> Text=<%# Item.Cargo.NoCargo%> Visible=<%# Item.CdCargo != CdCargo.Inspetor %> />
                        </div>
                        <div>
                            <box:NRepeater runat="server" DataSource=<%# Item.Licencas %> ItemType="Creasp.Siplen.Licenca">
                                <ItemTemplate>
                                    <box:NLabel runat="server" Theme="Danger" CssClass="space-left" Text=<%# Item.NoMotivoFmt %> ToolTip=<%# "Período: " + Item.GetPeriodo() %> />
                                </ItemTemplate>
                            </box:NRepeater>
                            <box:NLabel runat="server" Theme="Info" Text=<%# Item.Pessoa.NoTituloAbr %> ToolTip=<%# Item.Pessoa.NoTitulo %> CssClass="no-titulo" />
                        </div>
                    </div>
                    <div class="left-right space-top">
                        <small><%# Item.Camara.NoCamara %> - <%# Item.Inspetoria.NoInspetoria %> (Nº <%# Item.Inspetoria.NrProcesso %>)</small>
                        <small><%# Item.IsIndicado ? "<span style='color:red'>" + Item.Mensagem + "</span>" : Item.GetPeriodo().ToString(false) %></small>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
            <box:NButtonField Icon="pencil" CommandName="alt" Theme="Info" ToolTip="Consultar/Alterar dados" />
            <box:NButtonField Icon="cancel" CommandName="del" Theme="Danger" ToolTip="Excluir indicação de inspetor" OnClientClick="return confirm('Confirma a exclusão da indicação deste inspetor?\n\nATENÇÃO: Esta operação não poderá ser desfeita')" />
        </Columns>
        <EmptyDataTemplate>Não foi encontrado nenhum mandato de inspetor para o filtro informado</EmptyDataTemplate>
    </box:NGridView>

    <box:NPopup runat="server" ID="pnlPosse" Title="Posse">

        <div class="form-div">
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Data de posse</span></div>
                    <box:NTextBox runat="server" ID="txtDtPosse" MaskType="Date" Required="true" ValidationGroup="posse" />
                </div>
            </div>
        </div>
        <div class="help">Todos os inspetores selecionados tomarão posse na data informada acima.</div>

        <box:NButton ID="btnPosseOK" runat="server" Icon="ok" Text="Confirmar" CssClass="right ml-10" OnClick="btnPosseOK_Click" Theme="Primary" ValidationGroup="posse" />
        <box:NLinkButton runat="server" ID="btnCancelar" Text="Cancelar" CssClass="right" OnClick="btnCancelar_Click" />

    </box:NPopup>

    <uc:Email runat="server" ID="ucEmail" />

    <script src="../Content/Scripts/pagination.js"></script>
    <script>paginate()</script>

    <script src="../Content/Scripts/sidebar.js"></script>
    <script>selectButton("spl-inspetores"); showReload("siplen");</script>

</asp:Content>