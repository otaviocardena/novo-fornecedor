﻿<%@ Page Title="Eventos" Resource="siplen" Language="C#" %>

<script runat="server">

    protected CreaspContext db = new CreaspContext();

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        grdList.RegisterDataSource(() => db.Eventos.ListaEventos(ucPeriodo.Periodo, txtNoEvento.Text.To<string>(), txtNoLocal.Text.To<string>()));

        RegisterRestorePageState(() => grdList.DataBind());

        RegisterResource("siplen.evento").Disabled(lnkIncluir).GridColumns(grdList, "del");
    }

    protected override void OnPrepareForm()
    {
        ucPeriodo.Periodo = new Periodo("EsteMes");
        grdList.DataBind();

        SetDefaultButton(btnPesquisar);
    }

    protected void btnPesquisar_Click(object sender, EventArgs e)
    {
        grdList.DataBind();
    }

    protected void grdList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "del")
        {
            var idEvento = e.DataKey<int>(sender);
            var evt = db.SingleById<Evento>(idEvento);
            if (this.IsProprietario(evt, "siplen.evento.admin") == false) throw new NBoxException("Você não tem permissão de excluir este evento");
            db.Eventos.ExcluiEvento(idEvento);
            grdList.DataBind();
            ShowInfoBox("Evento excluido com sucesso");
        }
    }

</script>
<asp:Content ContentPlaceHolderID="content" runat="server">
    <div class="mt-30 c-primary-blue">
        <span>Eventos</span>
        <box:NHyperLink runat="server" ID="lnkIncluir" ButtonTheme="Success" Icon="plus" CssClass="right new ml-10" Text="Novo evento" NavigateUrl="Evento.Form.aspx" />
        <box:NDropDown runat="server" ID="drpOutros" Icon="menu" CssClass="right">
            <box:NDropDownItem NavigateUrl="Evento.CentroCusto.aspx" Text="Centro de custo padrão" />
        </box:NDropDown>
        <box:NButton runat="server" ID="btnPesquisar" Icon="search" Text="Pesquisar" Theme="Default" OnClick="btnPesquisar_Click" CssClass="right margin-right" />
    </div>

    <div class="form-div">
        <div class="sub-form-row">
            <div class="div-sub-form form-uc">
                <div><span>Período</span></div>
                <uc:Periodo runat="server" ID="ucPeriodo" OnPeriodoChanged="btnPesquisar_Click" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Descrição</span></div>
                <box:NTextBox runat="server" ID="txtNoEvento" TextCapitalize="Upper" Width="100%" />
            </div>
            <div class="div-sub-form margin-left">
                <div><span>Local</span></div>
                <box:NTextBox runat="server" ID="txtNoLocal" TextCapitalize="Upper" Width="100%" />
            </div>
        </div>
    </div>

    <box:NGridView runat="server" ID="grdList" AllowGrouping="true" ItemType="Creasp.Siplen.Evento" DataKeyNames="IdEvento" OnRowCommand="grdList_RowCommand" ShowHeader="false">
        <Columns>
            <asp:TemplateField>
                <ItemTemplate><%# Item.DtEvento.ToString("MMMM/yyyy").Captalize() %></ItemTemplate>
            </asp:TemplateField>
            <asp:HyperLinkField DataTextField="DtEvento" HeaderText="Data" DataNavigateUrlFields="IdEvento" DataNavigateUrlFormatString="Evento.View.aspx?IdEvento={0}" DataTextFormatString="{0:dd/MM/yyyy HH:mm}" HeaderStyle-Width="180" ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="NoEvento" HeaderText="Evento" />
            <box:NButtonField Icon="cancel" CommandName="del" Theme="Danger" ToolTip="Excluir" OnClientClick="return confirm('Confirma a exclusão deste evento?')" />
        </Columns>
        <EmptyDataTemplate>Nenhum evento encontrado no filtro informado</EmptyDataTemplate>
    </box:NGridView>

    <script src="../Content/Scripts/sidebar.js"></script>
    <script>selectButton("nerp-event"); showReload("nerp");</script>

</asp:Content>
