﻿<%@ Page Title="Alterar período" Resource="siplen" Language="C#" %>
<script runat="server">

    private CreaspContext db = new CreaspContext();

    private string Tipo => Request.QueryString["TpMandato"];
    private int IdMandato => Request.QueryString["IdMandato"].To<int>();

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

    }

    protected override void OnPrepareForm()
    {
        var titular = db.Query<Mandato>()
            .Include(x => x.Cargo)
            .Include(x => x.Pessoa)
            .Include(x => x.Camara, JoinType.Left)
            .Where(x => x.TpMandato == Tipo)
            .Where(x => x.IdMandato == IdMandato)
            .Single();

        ltrNoCargo.Text = titular.Cargo.NoCargo;
        txtDtIni.SetText(titular.DtIni);
        txtDtFim.SetText(titular.DtFim);
        chkEmAberto.Checked = titular.GetPeriodo().EmAberto;
        chkEmAberto_CheckedChanged(null, null);

        if(Tipo == TpMandato.Diretor)
        {
            ltrNoTitular.Text = WritePessoa(titular.Pessoa);
            trSuplente.Visible = false;
        }
        else if (Tipo == TpMandato.Camara)
        {
            ltrNoTitular.Text = WritePessoa(titular.Pessoa);
            trSuplente.Visible = false;
        }
        else if(Tipo == TpMandato.Comissao)
        {
            var comissao = db.SingleById<Comissao>(titular.IdComissao);
            PageTitle = comissao.NoComissao;
            ltrNoTitular.Text = WritePessoa(titular.Pessoa);

            if (comissao.TpSuplente == TpSuplente.SemSuplente)
            {
                trSuplente.Visible = false;
            }
            else if(comissao.TpSuplente == TpSuplente.Direto)
            {
                ltrTitular.Text = "Titular";

                var suplente = db.Query<Mandato>()
                    .Include(x => x.Pessoa)
                    .Where(x => x.TpMandato == TpMandato.Comissao)
                    .Where(x => x.CdCargo == CdCargo.Suplente)
                    .Where(x => x.IdMandatoTit == titular.IdMandato)
                    .SingleOrDefault();

                if(suplente != null)
                {
                    trSuplente.Visible = true;
                    trInicioSup.Visible = true;
                    ltrNoSuplente.Text = WritePessoa(suplente.Pessoa);
                    txtDtIniSup.SetText(suplente.DtIni);
                    txtDtFimSup.SetText(suplente.DtFim);
                    chkEmAbertoSup.Checked = suplente.GetPeriodo().EmAberto;
                    chkEmAberto_CheckedChanged(null, null);
                }
            }
            else if(comissao.TpSuplente == TpSuplente.Substituicao)
            {
                trSuplente.Visible = false;
            }

            if (!comissao.GetPeriodo().EmAberto)
            {
                chkEmAberto.Visible = false;
                trFim.Visible = true;
            }
        }
        else if(Tipo == TpMandato.Grupo)
        {
            var grupo = db.SingleById<Grupo>(titular.IdGrupo);
            PageTitle = grupo.NoGrupo;
            ltrTitular.Text = grupo.FgTecnico ? "Conselheiro" : "Pessoa";
            ltrNoTitular.Text = WritePessoa(titular.Pessoa);
            trSuplente.Visible = false;
            chkEmAberto.Visible = false;
        }

        txtDtIni.Focus();
        SetDefaultButton(btnSalvar);
    }

    private string WritePessoa(Pessoa p)
    {
        return p == null ? "[NÃO INDICADO]" :
            $@"{p.NoPessoa}&nbsp;&nbsp;
            <small>({p.NrCreasp})</small>&nbsp;&nbsp;
            <label class=""n-label n-label-info"">{p.NoTituloAbr}</label>";
    }

    protected void chkEmAberto_CheckedChanged(object sender, EventArgs e)
    {
        trFim.Visible = !chkEmAberto.Checked;
        trFimSup.Visible = !chkEmAbertoSup.Checked;

        if (chkEmAberto.Checked) txtDtFim.Text = "";
        if (chkEmAbertoSup.Checked) txtDtFimSup.Text = "";
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        txtDtIni.Validate(txtDtFim.HasValue()).LessThan(txtDtFim.Text.To<DateTime>());

        var periodo = new Periodo(txtDtIni.Text.To<DateTime>(), txtDtFim.Text.To(Periodo.MaxDate));
        var periodoSup = new Periodo(txtDtIniSup.Text.To<DateTime>(), txtDtFimSup.Text.To(Periodo.MaxDate));

        if(Tipo == TpMandato.Diretor)
        {
            db.Diretoria.AlteraDiretor(IdMandato, periodo);
            ShowInfoBox("Diretor alterado com sucesso");
        }
        else if(Tipo == TpMandato.Comissao)
        {
            db.Comissoes.AlteraMembro(IdMandato, periodo, periodoSup);
            ShowInfoBox("Membro de comissão alterado com sucesso");
        }
        else if(Tipo == TpMandato.Camara)
        {
            db.CamaraRep.AlteraMembro(IdMandato, periodo);
            ShowInfoBox("Membro alterado com sucesso");
        }
        else if(Tipo == TpMandato.Grupo)
        {
            db.Grupos.AlteraMembro(IdMandato, periodo);
            ShowInfoBox("Membro alterado com sucesso");
        }

        ClosePopup();
    }

</script>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <box:NTab runat="server" Text="Mandato" Selected="true">

        <div class="form-div">
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Cargo</span></div>
                    <div class="field">
                        <asp:Literal runat="server" ID="ltrNoCargo" />
                    </div>
                </div>
            </div>
            <div class="sub-form-row" id="trTitular" runat="server">
                <div class="div-sub-form form-uc">
                    <div><span><asp:Literal runat="server" ID="ltrTitular" Text="Conselheiro"/></span></div>
                    <div class="field">
                        <asp:Literal runat="server" ID="ltrNoTitular"/>
                    </div>
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form">
                    <div><span>Início</span></div>
                    <box:NTextBox runat="server" ID="txtDtIni" MaskType="Date" Required="true" />
                </div>
            </div>
            <div class="sub-form-row">
                <box:NCheckBox runat="server" ID="chkEmAberto" Text="Manter cargo em aberto até as próximas eleições" Checked="true" AutoPostBack="true" OnCheckedChanged="chkEmAberto_CheckedChanged" CssClass="" />
            </div>
            <div class="sub-form-row" id="trFim" runat="server" visible="false">
                <div class="div-sub-form">
                    <div><span>Fim</span></div>
                    <box:NTextBox runat="server" ID="txtDtFim" MaskType="Date" Required="true" />
                </div>
            </div>
            <div class="sub-form-row" id="trSuplente" runat="server">
                <div class="div-sub-form">
                    <div><span>Suplente</span></div>
                    <asp:Literal runat="server" ID="ltrNoSuplente" />
                </div>
            </div>
            <div class="sub-form-row" id="trInicioSup" runat="server" visible="false">
                <div class="div-sub-form">
                    <div><span>Início</span></div>
                    <box:NTextBox runat="server" ID="txtDtIniSup" MaskType="Date" Required="true" />
                </div>
            </div>
            <div class="sub-form-row">
                <box:NCheckBox runat="server" ID="chkEmAbertoSup" Text="Manter cargo em aberto até as próximas eleições" Checked="true" AutoPostBack="true" OnCheckedChanged="chkEmAberto_CheckedChanged" />
            </div>
            <div class="sub-form-row" >
                <div class="div-sub-form" id="trFimSup" runat="server" visible="false">
                    <div><span>Fim</span></div>
                    <box:NTextBox runat="server" ID="txtDtFimSup" MaskType="Date" Required="true" />
                </div>
            </div>
           
        </div>

        <box:NButton runat="server" ID="btnSalvar" Theme="Primary" Icon="ok" Width="150" Text="Salvar" OnClick="btnSalvar_Click" CssClass="right" />

    </box:NTab>

</asp:Content>
