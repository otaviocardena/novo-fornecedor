﻿<%@ Page Title="Comissões" Resource="siplen" Language="C#" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        grdList.RegisterDataSource(() => db.Comissoes.ListaComissoes(ucPeriodo.Periodo, txtNoComissao.Text));

        RegisterRestorePageState(() => grdList.DataBind());

        RegisterResource("siplen.comissao").Disabled(btnNovaComissao).GridColumns(grdList, "del");
    }

    protected override void OnPrepareForm()
    {
        ucPeriodo.Periodo = Periodo.UmAno(DateTime.Now.Year);
        grdList.DataBind();
        SetDefaultButton(btnPesquisar);
    }

    protected void btnPesquisar_Click(object sender, EventArgs e)
    {
        grdList.DataBind();
    }

    protected void grdList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if(e.CommandName == "alt")
        {
            var idComissao = e.DataKey<int>(sender);
            Redirect("Comissao.Form.aspx?IdComissao=" + idComissao);
        }
        else if(e.CommandName == "del")
        {
            var idComissao = e.DataKey<int>(sender);
            db.Comissoes.ExcluiComissao(idComissao);
            grdList.DataBind();
            ShowInfoBox("Comissão excluida com sucesso");
        }
    }

</script>
<asp:Content ContentPlaceHolderID="title" runat="server">
    
</asp:Content>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <div class="mt-30 c-primary-blue">
        <span>Comissão</span>
        <box:NHyperLink runat="server" ID="btnNovaComissao" ButtonTheme="Success" Icon="plus" CssClass="right new ml-10" Text="Nova comissão" NavigateUrl="Comissao.Form.aspx" />
        <box:NButton runat="server" ID="btnPesquisar" Icon="search" Text="Pesquisar" CssClass="right" Theme="Default" OnClick="btnPesquisar_Click" />
    </div>

    <div class="form-div">
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Período</span></div>
                <uc:Periodo runat="server" ID="ucPeriodo" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Nome</span></div>
                <box:NTextBox runat="server" ID="txtNoComissao" TextCapitalize="Upper" Width="100%" />
            </div>
        </div>
    </div>

    <box:NGridView runat="server" ID="grdList" ItemType="Creasp.Siplen.Comissao" DataKeyNames="IdComissao" OnRowCommand="grdList_RowCommand">
        <Columns>
            <asp:BoundField DataField="NoComissao" HeaderText="Comissão" />
            <asp:TemplateField HeaderText="Vigência" HeaderStyle-Width="220" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate><%# Item.GetPeriodo().ToString(false, true) %></ItemTemplate>
            </asp:TemplateField>
            <box:NButtonField Icon="pencil" CommandName="alt" Theme="Info" ToolTip="Alterar comissão" />
            <box:NButtonField Icon="cancel" CommandName="del" Theme="Danger" ToolTip="Excluir" OnClientClick="return confirm('Confirma a exclusão desta comissão?')" />
        </Columns>
        <EmptyDataTemplate>Nenhuma comissão encontrada no período selecionado</EmptyDataTemplate>
    </box:NGridView>

    <script src="../Content/Scripts/sidebar.js"></script>
    <script>selectButton("spl-com"); showReload("siplen");</script>

</asp:Content>