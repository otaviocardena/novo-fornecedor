﻿<%@ Page Title="Licenças" Resource="siplen" Language="C#" %>
<script runat="server">

    private CreaspContext db = new CreaspContext();

    private int? IdLicenca { get { return ViewState["IdLicenca"].To<int?>(); } set { ViewState["IdLicenca"] = value; } }

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        grdList.RegisterPagedDataSource((p) => db.Licencas.ListaLicencas(p,
            ucPeriodo.Periodo,
            txtNoPessoa.Text.To<string>()));

        RegisterResource("siplen.licenca").Disabled(btnIncluir).GridColumns(grdList, "alt", "del");
    }

    protected override void OnPrepareForm()
    {
        ucPeriodo.Periodo = Periodo.Hoje;
        grdList.DataBind();

        SetDefaultButton(btnPesquisar);
    }

    protected void btnIncluir_Click(object sender, EventArgs e)
    {
        pnlPopup.Controls.ClearControls();
        IdLicenca = null;
        ucPessoa.Enabled = true;
        ucPeriodoLic.Periodo = Periodo.Vazio;
        trFile.Visible = false;
        uplDoc.Visible = true;
        pnlPopup.Title = "Nova licença";
        pnlPopup.Open(btnOK, ucPessoa);
    }

    protected void btnOK_Click(object sender, EventArgs e)
    {
        if(IdLicenca == null)
        {
            db.Licencas.Inclui(ucPessoa.IdPessoa.Value,
                txtNoMotivo.Text,
                ucPeriodoLic.Periodo,
                uplDoc.PostedFile);
        }
        else
        {
            db.Licencas.Altera(IdLicenca.Value,
                txtNoMotivo.Text,
                ucPeriodoLic.Periodo,
                uplDoc.PostedFile);
        }

        pnlPopup.Close();

        ShowInfoBox("Licença gravada com sucesso");

        grdList.DataBind();
    }

    protected void grdList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if(e.CommandName == "del")
        {
            var idLicenca = e.DataKey<int>(sender);
            db.Licencas.Exclui(idLicenca);
            ShowInfoBox("Licença excluida com sucesso");
            grdList.DataBind();
        }
        else if(e.CommandName == "alt")
        {
            var idLicenca = e.DataKey<int>(sender);
            var lic = db.Licencas.BuscaLicenca(idLicenca);

            IdLicenca = idLicenca;
            pnlPopup.Title = "Alterar licença";
            ucPessoa.LoadPessoa(lic.IdPessoa);
            ucPessoa.Enabled = false;
            ucPeriodoLic.Periodo = lic.GetPeriodo();
            txtNoMotivo.Text = lic.NoMotivo;
            trFile.Visible = lic.Documento != null;
            uplDoc.Visible = lic.Documento == null;
            if (lic.Documento != null)
            {
                lnkDownload.CommandArgument = lic.Documento.IdDocumento.ToString();
                lnkDownload.Text = lic.Documento.NoArquivo;
            }
            pnlPopup.Open(btnOK, txtNoMotivo);
        }
    }

    protected void btnPesquisar_Click(object sender, EventArgs e)
    {
        grdList.DataBind();
    }

    protected void btnDelFile_Click(object sender, EventArgs e)
    {
        db.Documentos.ExcluiDocumento(lnkDownload.CommandArgument.To<int>());
        trFile.Visible = false;
        uplDoc.Visible = true;
        ShowInfoBox("Arquivo excluido com sucesso");
    }

    protected void lnkDownload_Command(object sender, CommandEventArgs e)
    {
        db.Documentos.Download(this, e.CommandArgument.To<int>());
    }

</script>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <div class="mt-30 c-primary-blue">
        <span>Licença</span>
        <box:NButton runat="server" ID="btnIncluir" Theme="Success" CssClass="right new ml-10" Icon="plus" Text="Nova licença" OnClick="btnIncluir_Click" />
        <box:NButton runat="server" ID="btnPesquisar" Theme="Default" Icon="search" Text="Pesquisar" CssClass="right" OnClick="btnPesquisar_Click" />
    </div>

    <div class="form-div">
        <div class="sub-form-row">
            <div class="div-sub-form form-uc">
                <div><span>Período</span></div>
                <uc:Periodo runat="server" ID="ucPeriodo" OnPeriodoChanged="btnPesquisar_Click" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form form-uc">
                <div><span>Nome</span></div>
                <box:NTextBox runat="server" ID="txtNoPessoa" TextCapitalize="Upper" MaxLength="80" Width="100%" />
            </div>
        </div>
    </div>

    <box:NGridView runat="server" ID="grdList" DataKeyNames="IdLicenca" OnRowCommand="grdList_RowCommand" ItemType="Creasp.Siplen.Licenca">
        <Columns>
            <asp:BoundField DataField="DtIni" HeaderText="Inicio" SortExpression="DtIni" DataFormatString="{0:d}" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="120" ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="DtFim" HeaderText="Fim" SortExpression="DtFim" DataFormatString="{0:d}" HeaderStyle-HorizontalAlign="Center" HeaderStyle-Width="120" ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="Pessoa.NoPessoa" HeaderText="Pessoa" SortExpression="Pessoa.NoPessoa" />
            <asp:BoundField DataField="NoMotivo" HeaderText="Motivo" SortExpression="NoMotivo" />
            <box:NButtonField Icon="pencil" CommandName="alt" Theme="Info" ToolTip="Alterar licença" />
            <box:NButtonField Icon="cancel" CommandName="del" Theme="Danger" ToolTip="Excluir licença" OnClientClick="return confirm('Confirma excluir esta licença?');" />
        </Columns>
        <EmptyDataTemplate>Não existem licenças para o filtro informado</EmptyDataTemplate>
    </box:NGridView>

    <box:NPopup runat="server" ID="pnlPopup" Title="Nova licença" Width="650">
        <box:NTab runat="server" Text="Licença" Selected="true">

             <div class="form-div">
                <div class="sub-form-row">
                    <div class="div-sub-form form-uc">
                        <div><span>Profissional</span></div>
                        <uc:Pessoa runat="server" ID="ucPessoa" IsProfissional="true" Required="true" />
                    </div>
                </div>
                <div class="sub-form-row">
                    <div class="div-sub-form">
                        <div><span>Período</span></div>
                        <uc:Periodo runat="server" ID="ucPeriodoLic" ShowTextBox="true" />
                    </div>
                </div>
                 <div class="sub-form-row">
                    <div class="div-sub-form">
                        <div><span>Motivo</span></div>
                        <box:NTextBox runat="server" ID="txtNoMotivo" Width="100%" Required="true" TextCapitalize="Upper" />
                    </div>
                </div>
                 <div class="sub-form-row">
                    <div class="div-sub-form">
                        <div><span>Documento</span></div>
                        <div class="left-right" runat="server" id="trFile">
                            <box:NLinkButton runat="server" ID="lnkDownload" OnCommand="lnkDownload_Command" />
                            <box:NLinkButton runat="server" ID="btnDelFile" Icon="cancel" Theme="Danger" OnClick="btnDelFile_Click" OnClientClick="return confirm('Confirma excluir este arquivo vinculado a esta licença?\n\nATENÇÃO: Esta operação não poderá ser desfeita.');" />
                        </div>
                        <box:NFileUpload runat="server" ID="uplDoc" Text="Inserir documento" Icon="doc" CssClass="linkBtsDoc" ButtonTheme="Info" />
                    </div>
                </div>
            </div>
            <box:NButton runat="server" ID="btnOK" CssClass="right" Width="150" OnClick="btnOK_Click" Theme="Primary" Text="Salvar" Icon="ok"  />

        </box:NTab>
    </box:NPopup>

    <script src="../Content/Scripts/pagination.js"></script>
    <script>paginate()</script>

    <script src="../Content/Scripts/sidebar.js"></script>
    <script>selectButton("spl-lic"); showReload("siplen");</script>

</asp:Content>