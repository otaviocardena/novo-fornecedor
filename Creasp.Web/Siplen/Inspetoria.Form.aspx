﻿<%@ Page Title="Cadastro de Inspetoria" Resource="siplen" Language="C#" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();

    private int? IdInspetoria { get { return ViewState["IdInspetoria"].To<int?>(); } set { ViewState["IdInspetoria"] = value; } }

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        RegisterResource("siplen.inspetoria").Disabled(btnSalvar, frm);
    }

    protected override void OnPrepareForm()
    {
        cmbGRE.Bind(db.Unidades.ListaUnidades(TpUnidade.DeptoRegional, null).Select(x => new { x.CdUnidade, x.NoUnidade }));

        IdInspetoria = Request.QueryString["IdInspetoria"].To<int?>();

        if (IdInspetoria.HasValue)
        {
            MontaTela();
        }

        this.BackTo("Inspetoria.aspx");
    }

    private void MontaTela()
    {
        var insp = db.Query<Inspetoria>()
            .Include(x => x.Cep)
            .Include(x => x.Unidade)
            .Single(x => x.IdInspetoria == IdInspetoria);

        cmbGRE.SelectedValue = db.Unidades.BuscaUnidadePai(insp.CdUnidade, TpUnidade.DeptoRegional)?.CdUnidade.To<string>();
        cmbGRE_SelectedIndexChanged(null, null);

        txtNoInspetoria.Text = insp.NoInspetoria;
        cmbUnidade.SelectedValue = insp.CdUnidade.ToString();
        txtNrTelefone.Text = insp.NrTelefone;
        txtNrProcesso.Text = insp.NrProcesso;
        txtDtAprovacao.SetText(insp.DtAprovacao);
        txtNrDecisaoPlen.Text = insp.NrDecisaoPlen;
        txtNoEmail.Text = insp.NoEmail;
        txtNrCep.SetText(insp.NrCep);
        lblCidade.Text = insp.Cep.NoCidade + " / " + insp.Cep.CdUf;
        txtNoLogradouro.Text = insp.NoLogradouro;
        txtNrEndereco.Text = insp.NrEndereco;
        txtNoComplemento.Text = insp.NoComplemento;
        chkFgRepresentacao.Checked = insp.FgRepresentacao;
        ucVigencia.Periodo = insp.GetPeriodo();
        txtNoHistorico.Text = insp.NoHistorico;

        this.Audit(insp, insp.IdInspetoria);
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        txtNoEmail.Validate(txtNoEmail.HasValue()).Email();

        var insp = IdInspetoria == null ? new Inspetoria() : db.SingleById<Inspetoria>(IdInspetoria);

        insp.IdInspetoria = IdInspetoria ?? 0;
        insp.NoInspetoria = txtNoInspetoria.Text;
        insp.CdUnidade = cmbUnidade.SelectedValue.To<int>();
        insp.NrTelefone = txtNrTelefone.Text;
        insp.NrProcesso = txtNrProcesso.Text;
        insp.NrDecisaoPlen = txtNrDecisaoPlen.Text;
        insp.DtAprovacao = txtDtAprovacao.Text.To<DateTime?>();
        insp.NoEmail = txtNoEmail.Text;
        insp.NrCep = txtNrCep.Text.UnMask();
        insp.NoLogradouro = txtNoLogradouro.Text;
        insp.NrEndereco = txtNrEndereco.Text;
        insp.NoComplemento = txtNoComplemento.Text;
        insp.FgRepresentacao = chkFgRepresentacao.Checked;
        insp.DtIni = ucVigencia.Periodo.Inicio;
        insp.DtFim = ucVigencia.Periodo.Fim;
        insp.NoHistorico = txtNoHistorico.Text;

        if(IdInspetoria == null)
        {
            db.Inspetorias.Inclui(insp);
        }
        else
        {
            db.Inspetorias.Altera(insp);
        }

        ShowInfoBox("Inspetoria gravada com sucesso");

        Redirect("Inspetoria.aspx", true);
    }

    protected void txtNrCep_TextChanged(object sender, EventArgs e)
    {
        if (txtNrCep.Text.Length > 0)
        {
            var cep = db.Ceps.BuscaCep(txtNrCep.Text.UnMask());

            if (cep == null)
            {
                txtNrCep.Text = "";
                lblCidade.Text = "";
                ShowErrorBox(txtNrCep, "CEP não encontrado");
            }
            else
            {
                lblCidade.Text = cep.NoCidade + " / " + cep.CdUf;
                txtNoLogradouro.Text = cep.NoLogradouro;
            }
        }
        else
        {
            lblCidade.Text = "";
        }
    }

    protected void cmbGRE_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(cmbGRE.HasValue())
        {
            cmbUnidade.Bind(db.Unidades.ListaUnidadesFilhas(cmbGRE.SelectedValue.To<int>()).Select(x => new { x.CdUnidade, x.NoUnidade }));
            cmbUnidade.Enabled = true;
        }
        else
        {
            cmbUnidade.Items.Clear();
            cmbUnidade.Enabled = true;
        }
    }

    protected void cmbUnidade_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (cmbUnidade.HasValue())
        {
            var unid = db.SingleById<Unidade>(cmbUnidade.SelectedValue.To<int>());
            txtNrCep.SetText(unid.NrCep);
        }
        else
        {
            txtNrCep.SetText("");
        }

        txtNrCep_TextChanged(null, null);
    }

</script>
<asp:Content ContentPlaceHolderID="content" runat="server">
        
    <box:NButton runat="server" ID="btnSalvar" Icon="ok" Text="Salvar" CssClass="right mt-25" Theme="Primary" OnClick="btnSalvar_Click" />

    <div class="form-div" runat="server" id="frm">
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Nome</span></div>
                <box:NTextBox runat="server" ID="txtNoInspetoria" TextCapitalize="Upper" Width="100%" Required="true" autofocus />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Gerência</span></div>
                <box:NDropDownList runat="server" ID="cmbGRE" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="cmbGRE_SelectedIndexChanged"/>
            </div>
            <div class="div-sub-form ml-10">
                <div><span>Unidade</span></div>
                <box:NDropDownList runat="server" ID="cmbUnidade" Width="100%" Required="true" Enabled="false" AutoPostBack="true" OnSelectedIndexChanged="cmbUnidade_SelectedIndexChanged" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Nº decisão plenária</span></div>
                <box:NTextBox runat="server" ID="txtNrDecisaoPlen" MaxLength="15" Width="100%" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Nº processo</span></div>
                <box:NTextBox runat="server" ID="txtNrProcesso" MaxLength="10" MaskFormat="9999999999" Width="100%" />
            </div>
            <div class="div-sub-form ml-10">
                <div><span>Data de aprovação</span></div>
                <div class="left-right">
                    <span style="width: 200px;"><box:NTextBox runat="server" ID="txtDtAprovacao" MaskType="Date" /></span>
                    <box:NCheckBox runat="server" ID="chkFgRepresentacao" Text="Representação" />
                </div>
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Telefone</span></div>
                <box:NTextBox runat="server" ID="txtNrTelefone" MaxLength="100" TextCapitalize="Upper" Width="100%" />
            </div>
            <div class="div-sub-form ml-10">
                <div><span>Email</span></div>
                <box:NTextBox runat="server" ID="txtNoEmail" MaxLength="100" Width="100%" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>CEP</span></div>
                <div class="input-group">
                    <box:NTextBox runat="server" ID="txtNrCep" MaskType="Custom" MaskFormat="99.999-999" AutoPostBack="true" OnTextChanged="txtNrCep_TextChanged" Width="100" Required="true" />
                    <box:NLabel runat="server" ID="lblCidade" CssClass="space-left field" />
                </div>
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Logradouro</span></div>
                <box:NTextBox runat="server" ID="txtNoLogradouro" Width="100%" MaxLength="80" TextCapitalize="Upper" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Número</span></div>
                <box:NTextBox runat="server" ID="txtNrEndereco" Width="160" MaxLength="10" TextCapitalize="Upper" />
            </div>
            <div class="div-sub-form">
                <div><span>Complemento</span></div>
                <box:NTextBox runat="server" ID="txtNoComplemento" Width="100%" MaxLength="40" TextCapitalize="Upper" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Vigência</span></div>
                <uc:Periodo runat="server" ID="ucVigencia" ShowTextBox="true" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Observações</span></div>
                <box:NTextBox runat="server" ID="txtNoHistorico" MaxLength="2000" Width="100%" TextMode="MultiLine" TextCapitalize="Upper" />
            </div>
        </div>
    </div>

    <script src="../Content/Scripts/sidebar.js"></script>
    <script>selectButton("spl-inspetoria"); showReload("siplen");</script>

</asp:Content>