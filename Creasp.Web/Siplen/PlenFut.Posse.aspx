﻿<%@ Page Title="Posse dos Indicados" Resource="siplen" Language="C#" %>
<script runat="server">

    private CreaspContext db = new CreaspContext();

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        RegisterResource("siplen.plenfut").ThrowIfNoAccess();
    }

    protected override void OnPrepareForm()
    {
        this.BackTo("PlenFut.aspx");

        cmbMandato.Items.Clear();

        for(var ano = DateTime.Now.Year - 2; ano <= DateTime.Now.Year + 3; ano++)
        {
            cmbMandato.Items.Add(new ListItem(string.Format("{0} - {1}", ano, ano + 2), ano.ToString()));
        }

        cmbMandato.SelectedValue = DateTime.Now.Year.ToString();

        MontaGrid(null, null);
    }

    protected void cmbMandato_SelectedIndexChanged(object sender, EventArgs e)
    {
        MontaGrid(null, null);
    }

    protected void MontaGrid(object sender, EventArgs e)
    {
        var ind = db.Indicacoes.ListaPlenarioFuturo(true, cmbMandato.SelectedValue.To<int>());

        grdListTit.DataSource = ind;
        grdListSup.DataSource = ind;

        grdListTit.DataBind();
        grdListSup.DataBind();
    }

    protected void btnPosse_Click(object sender, EventArgs e)
    {
        var inds = new List<Indicacao>();
        var dtPosse = txtDtPosse.Text.To<DateTime>();
        var indTit = grdListTit.SelectedDataKeys.Select(x => x.Value.To<int>()).ToArray();
        var indSup = grdListSup.SelectedDataKeys.Select(x => x.Value.To<int>()).ToArray();

        for(var i = 0; i < grdListTit.Rows.Count; i++)
        {
            var id = grdListTit.DataKeys[i].Value.To<int>();

            if(indTit.Contains(id) || indSup.Contains(id))
            {
                var ind = db.SingleById<Indicacao>(id);
                ind.DtPosseTit = indTit.Contains(id) ? dtPosse : ind.DtPosseTit;
                ind.DtPosseSup = indSup.Contains(id) ? dtPosse : ind.DtPosseSup;
                inds.Add(ind);
            }
        }

        if (inds.Count() == 0) throw new NBoxException("Nenhuma indicação selecionada");

        db.Indicacoes.PosseIndicados(inds);

        ShowInfoBox("Posse dos conselheiros efetuada com sucesso");

        this.Back();
    }

</script>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <div class="mt-30 c-primary-blue right">
        <box:NButton runat="server" ID="btnPosse" Theme="Primary" Icon="ok" Text="Confirmar posse" OnClick="btnPosse_Click" ToolTip="Confirmar posse dos indicados selecionados" />
    </div>

    <div class="form-div">
        <div class="sub-form-row">
            <div class="div-sub-form form-uc" style="width: 120px">
                <div><span>Mandato</span></div>
                <box:NDropDownList runat="server" ID="cmbMandato" Width="120" Required="true" AutoPostBack="true" OnSelectedIndexChanged="cmbMandato_SelectedIndexChanged" />
            </div>
            <div class="div-sub-form ml-10">
                <div><span>Data de posse</span></div>
                <box:NTextBox runat="server" ID="txtDtPosse" CssClass="w-170" Required="true" MaskType="Date"/>
            </div>
        </div>
    </div>

    <div class="left-right">

        <box:NGridView runat="server" ID="grdListTit" AllowGrouping="true" DataKeyNames="IdIndicacao" ItemType="Creasp.Siplen.Indicacao" ShowTotal="false">
            <Columns>
                <asp:BoundField DataField="Camara.NoCamara" />
                <box:NCheckBoxField EnabledExpression="!MensagemTit" SelectedExpression="!DtPosseTit && !MensagemTit" />
                <asp:TemplateField HeaderText="Titular">
                    <ItemTemplate>
                        <div class="left-right" style="height: 20px;overflow: hidden">
                            <div>
                                <%# Item.Titular == null ? "[SEM TITULAR]" : Item.Titular.NoPessoa %>
                                <small runat="server" visible=<%# Item.Titular != null %>>&nbsp;&nbsp;(<%# Item.Titular?.NrCreasp %>)</small>
                            </div>
                            <box:NLabel runat="server" Theme="Info" Visible=<%# Item.Titular != null %> Text=<%# Item.Titular?.NoTituloAbr %> CssClass="no-titulo" />
                        </div>
                        <div class="nowrap-ellipsis">
                            <small style="color:red" class="nowrap-ellipsis"><%# Item.MensagemTit %>&nbsp;</small>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>Não existem indicações</EmptyDataTemplate>
        </box:NGridView>

        <div style="margin-left: 20px">
        <box:NGridView runat="server" ID="grdListSup" AllowGrouping="true" DataKeyNames="IdIndicacao" ItemType="Creasp.Siplen.Indicacao">
            <Columns>
                <asp:BoundField DataField="Camara.NoCamara" />
                <box:NCheckBoxField EnabledExpression="!MensagemSup" SelectedExpression="!DtPosseSup && !MensagemSup" />
                <asp:TemplateField HeaderText="Suplente">
                    <ItemTemplate>
                        <div class="left-right" style="height: 20px;overflow: hidden">
                            <div>
                                <%# Item.Suplente == null ? "[SEM SUPLENTE]" : Item.Suplente.NoPessoa %>
                                <small runat="server" visible=<%# Item.Suplente != null %>>&nbsp;&nbsp;(<%# Item.Suplente?.NrCreasp %>)</small>
                            </div>
                            <box:NLabel runat="server" Theme="Info" Visible=<%# Item.Suplente != null %> Text=<%# Item.Suplente?.NoTituloAbr %> CssClass="no-titulo" />
                        </div>
                        <div class="nowrap-ellipsis">
                            <small style="color:red"><%# Item.MensagemSup %>&nbsp;</small>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EmptyDataTemplate>Não existem indicações</EmptyDataTemplate>
        </box:NGridView>
        </div>

    </div>

    <script src="../Content/Scripts/sidebar.js"></script>
    <script>selectButton("spl-plenFut"); showReload("siplen");</script>

</asp:Content>
