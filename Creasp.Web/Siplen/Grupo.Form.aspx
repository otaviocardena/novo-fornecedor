﻿<%@ Page Title="Grupo de Trabalho" Resource="siplen" Language="C#" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();

    private int? IdGrupo => Request.QueryString["IdGrupo"].To<int?>();

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        grdMembro.RegisterDataSource(() => db.Grupos.ListaMembros(IdGrupo.Value,ucPeriodo.Periodo));

        RegisterClosePopup((s, e) => grdMembro.DataBind());

        RegisterResource("siplen.gt").Disabled(btnIncluir, btnSalvar, frm).GridColumns(grdMembro, 2, 3);
    }

    protected override void OnPrepareForm()
    {
        btnIncluir.Items.Add(new NDropDownItem { Text = "Coordenador", Key = CdCargo.Coordenador });
        btnIncluir.Items.Add(new NDropDownItem { Text = "Adjunto", Key = CdCargo.Adjunto });
        btnIncluir.Items.Add(new NDropDownItem { Text = "Membro", Key = CdCargo.Membro });

        pnlMembros.Visible = IdGrupo.HasValue;

        ucDuracao.Periodo = Periodo.Vazio;
        ucPeriodo.Periodo = Periodo.Hoje;

        cmbCamara.Bind(db.Camaras.ListaCamaras(Periodo.Hoje));

        if (IdGrupo.HasValue)
        {
            MontaTela();
            grdMembro.DataBind();
        }
        this.BackTo("Grupo.aspx");
    }

    private void MontaTela()
    {
        var grupo = db.SingleById<Grupo>(IdGrupo);
        txtNoGrupo.Text = grupo.NoGrupo;
        radGT.Checked = !grupo.FgTecnico;
        radGTT.Checked = grupo.FgTecnico;
        trCamara.Visible = radGTT.Checked;
        cmbCamara.SelectedValue = grupo.CdCamara.ToString();
        ucDuracao.Periodo = grupo.GetPeriodo();
        txtNoHistorico.Text = grupo.NoHistorico;

        cmbCamara.Enabled = radGT.Enabled = radGTT.Enabled = false;

        this.Audit(grupo, grupo.IdGrupo);
    }

    protected void radTipo_CheckedChanged(object sender, EventArgs e)
    {
        trCamara.Visible = radGTT.Checked;
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        if (IdGrupo.HasValue)
        {
            db.Grupos.AlteraGrupo(IdGrupo.Value, txtNoGrupo.Text, ucDuracao.Periodo, txtNoHistorico.Text);

            grdMembro.DataBind();

            ShowInfoBox("Grupo de trabalho alterado com sucesso");
        }
        else
        {
            var idGrupo = db.Grupos.IncluiGrupo(txtNoGrupo.Text,
                radGTT.Checked,
                cmbCamara.SelectedValue.To<int?>(),
                ucDuracao.Periodo,
                txtNoHistorico.Text);

            ShowInfoBox("Grupo de trabalho criado com sucesso");

            Redirect("Grupo.Form.aspx?IdGrupo=" + idGrupo);
        }
    }

    protected void grdMembro_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if(e.CommandName == "alt")
        {
            var idMandato = e.DataKey<int>(sender);
            OpenPopup("form", "Mandato.Edit.aspx?TpMandato=G&IdMandato=" + idMandato, 750);
        }
        else if(e.CommandName == "del")
        {
            var idMandato = e.DataKey<int>(sender);
            db.Grupos.ExcluiMandato(idMandato);
            grdMembro.DataBind();
            ShowInfoBox("Membro excluido com sucesso");
        }
    }

    protected void btnIncluir_ItemClick(object sender, NDropDownItem e)
    {
        OpenPopup("form", $"Mandato.New.aspx?TpMandato=G&CdCargo={e.Key}&IdGrupo={IdGrupo}", 750);
    }

    protected void ucPeriodo_PeriodoChanged(object sender, EventArgs e)
    {
        grdMembro.DataBind();
    }

</script>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <box:NTab runat="server" Text="Grupo de Trabalho" Selected="true">

        <box:NButton runat="server" ID="btnSalvar" Icon="ok" Text="Salvar" Theme="Primary" CssClass="right mt-20" OnClick="btnSalvar_Click" />

        <div class="form-div" runat="server" id="frm">
            <div class="sub-form-row">
                <div class="div-sub-form">
                    <div><span>Descrição</span></div>
                    <box:NTextBox runat="server" ID="txtNoGrupo" TextCapitalize="Upper" Width="100%" Required="true" />
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form">
                    <div><span>Tipo</span></div>
                    <box:NRadioButton runat="server" ID="radGT" Text="Grupo de trabalho" Checked="true" GroupName="tp" AutoPostBack="true" OnCheckedChanged="radTipo_CheckedChanged" />
                    <box:NRadioButton runat="server" ID="radGTT" Text="Grupo técnico de trabalho" GroupName="tp" AutoPostBack="true" OnCheckedChanged="radTipo_CheckedChanged" />
                </div>
            </div>
            <div class="sub-form-row" runat="server" id="trCamara" visible="false">
                <div class="div-sub-form">
                    <div><span>Câmara especializada</span></div>
                    <box:NDropDownList runat="server" ID="cmbCamara" Width="400" Required="true" />
                </div>
            </div>
            <div class="sub-form-row" style="min-width: 450px; width: 40%">
                <div class="div-sub-form">
                    <div><span>Vigência</span></div>
                    <uc:Periodo runat="server" ID="ucDuracao" ShowTextBox="true" Required="true" />
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form">
                    <div><span>Observações</span></div>
                    <box:NTextBox runat="server" ID="txtNoHistorico" MaxLength="2000" Width="100%" TextMode="MultiLine" TextCapitalize="Upper" />
                </div>
            </div>
        </div>

    </box:NTab>
    <box:NTab runat="server" ID="pnlMembros" Text="Membros" Visible="false">

        <div class="right mt-25">
            <uc:Periodo runat="server" ID="ucPeriodo" OnPeriodoChanged="ucPeriodo_PeriodoChanged" />
            <box:NDropDown runat="server" Icon="plus" Text="Incluir" ID="btnIncluir" OnItemClick="btnIncluir_ItemClick"></box:NDropDown>
        </div>

        <box:NGridView runat="server" ID="grdMembro" ItemType="Creasp.Siplen.Mandato" DataKeyNames="IdMandato" OnRowCommand="grdMembro_RowCommand">
            <Columns>
                <asp:BoundField HeaderText="Cargo" DataField="Cargo.NoCargo" HeaderStyle-Width="220" ItemStyle-HorizontalAlign="Right" HeaderStyle-HorizontalAlign="Right" />
                <asp:TemplateField HeaderText="Pessoa">
                    <ItemTemplate>
                        <div class="left-right">
                            <div>
                                <%# Item.Pessoa != null ? Item.Pessoa.NoPessoa : "<div class='space-top'><i class='icon-attention danger'></i> Não informado</div>" %>
                                <small runat="server" visible=<%# Item.Pessoa != null %>>&nbsp;&nbsp;(<%# Item.Pessoa?.NrCreasp %>)</small>
                            </div>
                            <div>
                                <box:NRepeater runat="server" DataSource=<%# Item.Licencas %> ItemType="Creasp.Siplen.Licenca">
                                    <ItemTemplate>
                                        <box:NLabel runat="server" Theme="Danger" CssClass="space-left" Text=<%# Item.NoMotivoFmt %> ToolTip=<%# "Período: " + Item.GetPeriodo() %> />
                                    </ItemTemplate>
                                </box:NRepeater>
                                <box:NLabel runat="server" Theme="Info" Visible=<%# Item.Pessoa != null %> Text=<%# Item.Pessoa?.NoTituloAbr %> ToolTip=<%# Item.Pessoa?.NoTitulo %> CssClass="no-titulo" />
                            </div>
                        </div>
                        <div class="space-top">
                            <small><%# Item.Pessoa != null ? Item.GetPeriodo().ToString(false) : "" %></small>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <box:NButtonField Icon="pencil" CommandName="alt" Theme="Info" ToolTip="Alterar período" EnabledExpression="Pessoa" />
                <box:NButtonField Icon="cancel" CommandName="del" Theme="Danger" ToolTip="Excluir" EnabledExpression="Pessoa" OnClientClick="return confirm('Confirma a exclusão desta pessoa neste grupo?')" />
            </Columns>
        </box:NGridView>

    </box:NTab>

    <script src="../Content/Scripts/sidebar.js"></script>
    <script>selectButton("spl-gru"); showReload("siplen");</script>

</asp:Content>