﻿<%@ Page Title="Comissões" Resource="siplen" Language="C#" %>

<script runat="server">

    protected CreaspContext db = new CreaspContext();

    private int? IdComissao => Request.QueryString["IdComissao"].To<int?>();

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        grdMembro.RegisterDataSource(() => db.Comissoes.ListaMembros(IdComissao.Value, ucPeriodo.Periodo));

        RegisterClosePopup((s, e) => grdMembro.DataBind());

        RegisterResource("siplen.comissao").Disabled(btnSalvar, btnIncluir, frm).GridColumns(grdMembro, 2, 3);
    }

    protected override void OnPrepareForm()
    {
        ucDuracao.Periodo = Periodo.Vazio;
        ucPeriodo.Periodo = Periodo.Hoje;

        if (IdComissao.HasValue)
        {
            MontaTela();
            MontaTelaMembros();
            grdMembro.DataBind();
        }

        this.BackTo("Comissao.aspx");
    }

    private void MontaTela()
    {
        var comissao = db.SingleById<Comissao>(IdComissao);
        txtNoComissao.Text = comissao.NoComissao;
        radPer.Checked = comissao.FgPermanente;
        radEsp.Checked = !comissao.FgPermanente;
        radDir.Checked = comissao.TpSuplente == TpSuplente.Direto;
        radSub.Checked = comissao.TpSuplente == TpSuplente.Substituicao;
        radSem.Checked = comissao.TpSuplente == TpSuplente.SemSuplente;
        ucDuracao.Periodo = comissao.GetPeriodo();
        txtNoHistorico.Text = comissao.NoHistorico;

        this.Audit(comissao, comissao.IdComissao);
    }

    protected void MontaTelaMembros()
    {
        btnIncluir.Items.Add(new NDropDownItem { Text = "Coordenador", Key = CdCargo.Coordenador });
        btnIncluir.Items.Add(new NDropDownItem { Text = "Adjunto", Key = CdCargo.Adjunto });
        btnIncluir.Items.Add(new NDropDownItem { Text = "Membro", Key = CdCargo.Membro });

        if (radSub.Checked)
        {
            btnIncluir.Items.Add(new NDropDownItem { Text = "Suplente", Key = CdCargo.Suplente });
        }

        pnlMembros.Visible = IdComissao.HasValue;

        grdMembro.Columns[1].HeaderText = radDir.Checked ? "Titular" : "Conselheiro";
        grdMembro.Columns[2].Visible = radDir.Checked;

        radPer.Enabled = radEsp.Enabled = radDir.Enabled = radSem.Enabled = radSub.Enabled = false;
        pnlMembros.Visible = true;

        grdMembro.DataBind();
    }


    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        if (IdComissao.HasValue)
        {
            db.Comissoes.AlteraComissao(IdComissao.Value, txtNoComissao.Text, ucDuracao.Periodo, txtNoHistorico.Text);

            ShowInfoBox("Comissão alterada com sucesso");
        }
        else
        {
            var idComissao = db.Comissoes.IncluiComissao(txtNoComissao.Text,
                radPer.Checked,
                radDir.Checked ? TpSuplente.Direto : radSem.Checked ? TpSuplente.SemSuplente : TpSuplente.Substituicao,
                ucDuracao.Periodo,
                txtNoHistorico.Text);

            ShowInfoBox("Comissão criada com sucesso");

            Redirect("Comissao.Form.aspx?IdComissao=" + idComissao);
        }
    }

    protected void grdMembro_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "alt")
        {
            var idMandato = e.DataKey<int>(sender);
            OpenPopup("form", "Mandato.Edit.aspx?TpMandato=S&IdMandato=" + idMandato, 750);
        }
        else if (e.CommandName == "del")
        {
            var idMandato = e.DataKey<int>(sender);
            db.Comissoes.ExcluiMandato(idMandato);
            grdMembro.DataBind();
            ShowInfoBox("Membro excluido com sucesso");

        }
    }

    protected void btnIncluir_ItemClick(object sender, NDropDownItem e)
    {
        if (e.Key == CdCargo.Suplente && !radSub.Checked) throw new NBoxException("Esta comissão não permite suplente por substituição");

        OpenPopup("form", $"Mandato.New.aspx?TpMandato=S&CdCargo={e.Key}&IdComissao={IdComissao}", 750);
    }

    protected void ucPeriodo_PeriodoChanged(object sender, EventArgs e)
    {
        grdMembro.DataBind();
    }

</script>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <box:NTab runat="server" Text="Comissão" Selected="true">

        <box:NButton runat="server" ID="btnSalvar" Icon="ok" Text="Salvar" Theme="Primary" CssClass="right mt-20" OnClick="btnSalvar_Click" />


        <div class="form-div" runat="server" id="frm">
            <div class="sub-form-row">
                <div class="div-sub-form">
                    <div><span>Descrição</span></div>
                    <box:NTextBox runat="server" ID="txtNoComissao" TextCapitalize="Upper" Width="100%" Required="true" autofocus />
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Tipo</span></div>
                    <box:NRadioButton runat="server" ID="radPer" Text="Permanente" GroupName="tp" />
                    <box:NRadioButton runat="server" ID="radEsp" Text="Especial" Checked="true" GroupName="tp" />
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Suplentes</span></div>
                    <box:NRadioButton runat="server" ID="radDir" Text="Direto" Checked="true" GroupName="sup" />
                    <box:NRadioButton runat="server" ID="radSub" Text="Substituição" GroupName="sup" />
                    <box:NRadioButton runat="server" ID="radSem" Text="Sem suplente" GroupName="sup" />
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form" style="min-width: 450px; width: 40%">
                    <div><span>Vigência</span></div>
                    <uc:Periodo runat="server" ID="ucDuracao" ShowTextBox="true"/>
                </div>
            </div>
            <span class="help margin-bottom" style="margin-top: -15px;"> Todos os membros devem ter período igual ou menos ao período de vigência da comissão</span>

            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Observações</span></div>
                    <box:NTextBox runat="server" ID="txtNoHistorico" MaxLength="2000" Width="100%" TextMode="MultiLine" TextCapitalize="Upper" />
                </div>
            </div>
        </div>

    </box:NTab>

    <box:NTab runat="server" ID="pnlMembros" Text="Membros" Visible="false">

        <box:NToolBar runat="server">
            <uc:Periodo runat="server" ID="ucPeriodo" OnPeriodoChanged="ucPeriodo_PeriodoChanged" />
            <box:NDropDown runat="server" Icon="plus" Text="Incluir" ID="btnIncluir" OnItemClick="btnIncluir_ItemClick"></box:NDropDown>
        </box:NToolBar>

        <box:NGridView runat="server" ID="grdMembro" ItemType="Creasp.Siplen.TitularSuplenteDTO" DataKeyNames="IdMandatoTit" OnRowCommand="grdMembro_RowCommand">
            <Columns>
                <asp:TemplateField HeaderStyle-Width="220" ItemStyle-HorizontalAlign="Right" HeaderText="Cargo" HeaderStyle-HorizontalAlign="Right">
                    <ItemTemplate>
                        <%# Item.Titular.NrOrdem > 0 ? Item.Titular.NrOrdem + "º " : "" %>
                        <%# Item.Titular.Cargo.NoCargo %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Conselheiro" ItemStyle-VerticalAlign="Top">
                    <ItemTemplate>
                        <div class="left-right">
                            <div>
                                <%# Item.Titular.Pessoa != null ? Item.Titular.Pessoa.NoPessoa : "<div class='space-top'><i class='icon-attention danger'></i> Não informado</div>" %>
                                <box:NLabel runat="server" Theme="Danger" Text="<%# Item.Titular?.NrFaltas %>" Visible="<%# Item.Titular?.NrFaltas > 0 %>" ToolTip="Faltas no periodo" />
                                <box:NLabel runat="server" Theme="Info" Text="<%# Item.Titular?.NrFaltasJustificadas %>" Visible="<%# Item.Titular?.NrFaltasJustificadas > 0 %>" ToolTip="Faltas Justificadas no periodo" />

                                <small runat="server" visible="<%# Item.Titular.Pessoa != null %>">&nbsp;&nbsp;(<%# Item.Titular.Pessoa?.NrCreasp %>)</small>
                            </div>
                            <div>
                                <box:NRepeater runat="server" DataSource="<%# Item.Titular.Licencas %>" ItemType="Creasp.Siplen.Licenca">
                                    <ItemTemplate>
                                        <box:NLabel runat="server" Theme="Danger" CssClass="space-left" Text="<%# Item.NoMotivoFmt %>" ToolTip='<%# "Período: " + Item.GetPeriodo() %>' />
                                    </ItemTemplate>
                                </box:NRepeater>
                                <box:NLabel runat="server" Theme="Info" CssClass="right no-titulo" Visible="<%# Item.Titular.Pessoa != null %>" Text="<%# Item.Titular.Pessoa?.NoTituloAbr %>" ToolTip="<%# Item.Titular.Pessoa?.NoTitulo %>" />
                            </div>
                        </div>
                        <div class="left-right space-top">
                            <small><%# Item.Titular.Camara?.NoCamara %></small>
                            <small><%# Item.Titular.Pessoa != null ? Item.Titular.GetPeriodo().ToString(false) : "" %></small>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Suplente" ItemStyle-VerticalAlign="Top">
                    <ItemTemplate>
                        <div class="left-right">
                            <div>
                                <%# Item.Suplente?.Pessoa?.NoPessoa %>
                                <box:NLabel runat="server" Theme="Danger" Text="<%# Item.Suplente?.NrFaltas %>" Visible="<%# Item.Suplente?.NrFaltas > 0 %>" ToolTip="Faltas no periodo" />
                                <box:NLabel runat="server" Theme="Info" Text="<%# Item.Suplente?.NrFaltasJustificadas %>" Visible="<%# Item.Suplente?.NrFaltasJustificadas > 0 %>" ToolTip="Faltas Justificadas no periodo" />

                                <small runat="server" visible="<%# Item.Suplente?.Pessoa != null %>">&nbsp;&nbsp;(<%# Item.Suplente?.Pessoa?.NrCreasp %>)</small>
                            </div>
                            <div>
                                <box:NRepeater runat="server" DataSource="<%# Item.Suplente?.Licencas %>" ItemType="Creasp.Siplen.Licenca">
                                    <ItemTemplate>
                                        <box:NLabel runat="server" Theme="Danger" CssClass="right space-left" Text="<%# Item.NoMotivoFmt %>" ToolTip='<%# "Período: " + Item.GetPeriodo() %>' />
                                    </ItemTemplate>
                                </box:NRepeater>
                                <box:NLabel runat="server" Theme="Info" CssClass="right no-titulo" Visible="<%# Item.Suplente?.Pessoa != null %>" Text="<%# Item.Suplente?.Pessoa?.NoTituloAbr %>" ToolTip="<%# Item.Suplente?.Pessoa?.NoTitulo %>" />
                            </div>
                        </div>
                        <div class="left-right space-top">
                            <small><%# Item.Suplente?.Camara?.NoCamara %></small>
                            <small><%# Item.Suplente?.Pessoa != null ? Item.Suplente.GetPeriodo().ToString(false) : "" %></small>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <box:NButtonField Icon="pencil" CommandName="alt" Theme="Info" ToolTip="Alterar período" EnabledExpression="Titular.Pessoa" />
                <box:NButtonField Icon="cancel" CommandName="del" Theme="Danger" ToolTip="Excluir" EnabledExpression="Titular.Pessoa" OnClientClick="return confirm('Confirma a exclusão deste conselheiro nesta comissão?')" />
            </Columns>
        </box:NGridView>
    </box:NTab>

    <script src="../Content/Scripts/sidebar.js"></script>
    <script>selectButton("spl-com"); showReload("siplen");</script>

</asp:Content>
