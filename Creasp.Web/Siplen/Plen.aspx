﻿<%@ Page Title="Composição do Plenário" Resource="siplen" Language="C#" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        grdList.RegisterPagedDataSource((sp) =>
        {
            return db.Conselheiros.ListaPlenario(
                        ucPeriodo.Periodo,
                        cmbMandato.SelectedValue.To<int?>(),
                        ucEntidade.IdEntidade,
                        txtNoPessoa.Text.To<string>(),
                        cmbCamara.SelectedValue.To<int?>()).ToPage(sp);
        });

        RegisterRestorePageState(() => grdList.DataBind());
    }

    protected override void OnPrepareForm()
    {
        cmbMandato.Items.Clear();
        cmbMandato.Items.Add(new ListItem("(Todos)", ""));

        for(var ano = DateTime.Today.Year; ano > DateTime.Today.Year - 20; ano--)
        {
            cmbMandato.Items.Add(new ListItem(string.Format("{0} - {1}", ano, ano + 2), ano.ToString()));
        }

        ucPeriodo.Periodo = Periodo.Hoje;
        cmbCamara.Bind(db.Query<Camara>().WherePeriodo(ucPeriodo.Periodo).OrderBy(x => x.NoCamara).ToEnumerable());

        SetDefaultButton(btnPesquisar);
        grdList.DataBind();
    }

    protected void btnPesquisar_Click(object sender, EventArgs e)
    {
        grdList.DataBindPaged();
    }

    protected void grdList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if(e.CommandName.StartsWith("ViewMandato"))
        {
            Redirect("Plen.View.aspx?IdMandato=" + e.DataKey<int>(grdList, "IdMandatoTit"));
            //OpenPopup("mand", "Plen.View.aspx?IdMandato=" + e.DataKey<int>(grdList, "IdMandatoTit"), 900);
        }
    }

    protected void ucPeriodo_PeriodoChanged(object sender, EventArgs e)
    {
        cmbCamara.Bind(db.Query<Camara>().WherePeriodo(ucPeriodo.Periodo).OrderBy(x => x.NoCamara).ToEnumerable());
    }

    protected void cmbMandato_SelectedIndexChanged(object sender, EventArgs e)
    {
        ucPeriodo.Periodo = cmbMandato.HasValue() ? Periodo.Anos(cmbMandato.SelectedValue.To<int>(), cmbMandato.SelectedValue.To<int>() + 2) : Periodo.Hoje;
    }

</script>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <div class="mt-30 c-primary-blue">
        <span>Composição do Plenário</span>
        <box:NButton runat="server" ID="btnPesquisar" Icon="search" Text="Pesquisar" CssClass="right" Theme="Default" OnClick="btnPesquisar_Click" />
    </div>

    <div class="form-div">
        <div class="sub-form-row">
            <div class="div-sub-form form-uc" style="width:100px">
                <div><span>Período</span></div>
                <uc:Periodo runat="server" id="ucPeriodo" OnPeriodoChanged="ucPeriodo_PeriodoChanged" />
            </div>
            <div class="div-sub-form form-uc margin-left">
                <div><span>Mandato</span></div>
                <box:NDropDownList runat="server" ID="cmbMandato" Width="120" AutoPostBack="true" OnSelectedIndexChanged="cmbMandato_SelectedIndexChanged" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Conselheiro</span></div>
                <box:NTextBox runat="server" ID="txtNoPessoa" TextCapitalize="Upper" MaxLength="80" Width="100%" autofocus />
            </div>
            <div class="div-sub-form margin-left">
                <div><span>Câmara</span></div>
                <box:NDropDownList runat="server" ID="cmbCamara" Width="100%" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form form-uc">
                <div><span>Entidade</span></div>
                <uc:Entidade runat="server" ID="ucEntidade"/>
            </div>
        </div>
    </div>

    <box:NGridView runat="server" ID="grdList" DataKeyNames="IdMandatoTit,IdMandatoSup" ItemType="Creasp.Siplen.TitularSuplenteDTO" OnRowCommand="grdList_RowCommand" PageSize="25">
        <Columns>
            <asp:TemplateField HeaderStyle-Width="50" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate><%# (Item.Titular?.AnoIni ?? Item.Suplente?.AnoIni) + " " + (Item.Titular?.AnoFim ?? Item.Suplente?.AnoFim) %></span>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Titular" HeaderStyle-Width="50%">
                <ItemTemplate>
                    <div class="left-right">
                        <div>
                            <%# Item.Titular?.Pessoa.NoPessoa ?? "-" %>
                            <box:NLabel runat="server" Theme="Danger" Text=<%# Item.Titular?.NrFaltas %> Visible=<%# Item.Titular?.NrFaltas > 0 %> ToolTip="Faltas no periodo" />
                            <small runat="server" visible=<%# Item.Titular != null %>>&nbsp;&nbsp;(<a href="Hist.View.aspx?IdPessoa=<%# Item.Titular?.IdPessoa %>"><%# Item.Titular?.Pessoa.NrCreasp %></a>)</small>
                        </div>
                        <div>
                            <box:NRepeater runat="server" DataSource=<%# Item.Titular?.Licencas %> ItemType="Creasp.Siplen.Licenca">
                                <ItemTemplate>
                                    <box:NLabel runat="server" Theme="Danger" CssClass="space-left" Text=<%# Item.NoMotivoFmt %> ToolTip=<%# "Período: " + Item.GetPeriodo() %> />
                                </ItemTemplate>
                            </box:NRepeater>
                            <box:NLabel runat="server" Theme="Info" Visible=<%# Item.Titular != null %> Text=<%# Item.Titular?.Pessoa.NoTituloAbr %> CssClass="no-titulo" ToolTip=<%# Item.Titular?.Pessoa.NoTitulo %> />
                        </div>
                    </div>
                    <div class="left-right space-top">
                        <small><%# Item.Titular?.Camara.NoCamara ?? Item.Suplente?.Camara.NoCamara %></small>
                        <small><%# Item.Titular?.GetPeriodo().ToString(false) %></small>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Suplente" HeaderStyle-Width="50%">
                <ItemTemplate>
                    <div class="left-right">
                        <div>
                            <%# Item.Suplente?.Pessoa.NoPessoa ?? "-" %>
                            <box:NLabel runat="server" Theme="Danger" Text=<%# Item.Suplente?.NrFaltas %> Visible=<%# Item.Suplente?.NrFaltas > 0 %> ToolTip="Faltas no periodo" />
                            <small runat="server" visible=<%# Item.Suplente != null %>>&nbsp;&nbsp;(<a href="Hist.View.aspx?IdPessoa=<%# Item.Suplente?.IdPessoa %>"><%# Item.Suplente?.Pessoa.NrCreasp %></a>)</small>
                        </div>
                        <div>
                            <box:NRepeater runat="server" DataSource=<%# Item.Suplente?.Licencas %> ItemType="Creasp.Siplen.Licenca">
                                <ItemTemplate>
                                    <box:NLabel runat="server" Theme="Danger" CssClass="space-left" Text=<%# Item.NoMotivoFmt %> ToolTip=<%# "Período: " + Item.GetPeriodo() %> />
                                </ItemTemplate>
                            </box:NRepeater>
                            <box:NLabel runat="server" Theme="Info" Visible=<%# Item.Suplente != null %> Text=<%# Item.Suplente?.Pessoa.NoTituloAbr %>  CssClass="no-titulo" ToolTip=<%# Item.Suplente?.Pessoa.NoTitulo %> />
                        </div>
                    </div>
                    <div class="left-right space-top">
                        <small class="nowrap-ellipsis"><%# Item.Titular?.Entidade.NoEntidade ?? Item.Suplente?.Entidade.NoEntidade %></small>
                        <small class="nowrap"><%# Item.Suplente?.GetPeriodo().ToString(false) %></small>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
            <box:NButtonField Icon="export" Theme="Info" ToolTip="Visualizar mandato" CommandName="ViewMandato" EnabledExpression="IdMandatoTit" />
        </Columns>
        <EmptyDataTemplate>Não existem conselheiros para o filtro acima</EmptyDataTemplate>
    </box:NGridView>

    <script src="../Content/Scripts/pagination.js"></script>
    <script>paginate()</script>

    <script src="../Content/Scripts/sidebar.js"></script>
    <script>selectButton("spl-plen"); showReload("siplen");</script>

</asp:Content>