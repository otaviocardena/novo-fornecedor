﻿<%@ Page Title="Incluir" Resource="siplen" Language="C#" %>
<script runat="server">

    private CreaspContext db = new CreaspContext();

    private string TipoMandato => Request.QueryString["TpMandato"];
    private string CodCargo => Request.QueryString["CdCargo"];
    private int? IdMandato => Request.QueryString["IdMandato"].To<int?>();

    private int IdComissao => Request.QueryString["IdComissao"].To<int>();
    private int IdGrupo => Request.QueryString["IdGrupo"].To<int>();

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

    }

    protected override void OnPrepareForm()
    {
        var cargo = db.SingleById<Cargo>(CodCargo);

        ltrCargo.Text = cargo.NoCargo;

        if(TipoMandato == TpMandato.Diretor)
        {
            trSuplente.Visible = false;
            ucPessoaTit.Focus();

        }
        else if(TipoMandato == TpMandato.Camara)
        {
            trCamara.Visible = true;
            cmbCamara.Bind(db.Camaras.ListaCamaras(Periodo.Hoje));
            cmbCamara.SelectedValue = Request.QueryString["CdCamara"];
            trSuplente.Visible = false;

            if(cmbCamara.HasValue())
            {
                ucPessoaTit.Focus();
            }
            else
            {
                cmbCamara.Focus();
            }
        }
        else if(TipoMandato == TpMandato.Comissao)
        {
            var comissao = db.SingleById<Comissao>(IdComissao);
            PageTitle = comissao.NoComissao;
            ltrTitular.Text = comissao.TpSuplente == TpSuplente.Direto ? "Titular" : "Conselheiro";
            trSuplente.Visible = comissao.TpSuplente == TpSuplente.Direto;
            trOrdem.Visible = cargo.CdCargo == CdCargo.Suplente;
            ucPessoaTit.Focus();

            if (!comissao.GetPeriodo().EmAberto)
            {
                chkEmAberto.Visible = false;
                txtDtIni.SetText(comissao.DtIni);
                txtDtFim.SetText(comissao.DtFim);
                trFim.Visible = true;
            }
        }
        else if(TipoMandato == TpMandato.Grupo)
        {
            var grupo = db.SingleById<Grupo>(IdGrupo);
            PageTitle = grupo.NoGrupo;
            ltrTitular.Text = grupo.FgTecnico ? "Conselheiro" : "Pessoa";
            trSuplente.Visible = false;
            ucPessoaTit.Focus();
            chkEmAberto.Visible = false;
            txtDtIni.SetText(grupo.DtIni);
            txtDtFim.SetText(grupo.DtFim);
            trFim.Visible = true;
        }

        SetDefaultButton(btnSalvar);
    }

    protected void chkSemDtFim_CheckedChanged(object sender, EventArgs e)
    {
        trFim.Visible = !chkEmAberto.Checked;

        if (chkEmAberto.Checked) txtDtFim.Text = "";
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        txtDtIni.Validate(txtDtFim.HasValue()).LessThan(txtDtFim.Text.To<DateTime>());

        var periodo = new Periodo(txtDtIni.Text.To<DateTime>(), txtDtFim.Text.To(DateTime.MaxValue));

        if(TipoMandato == TpMandato.Diretor)
        {
            db.Diretoria.IncluiDiretor(CodCargo, ucPessoaTit.IdPessoa.Value, periodo);
            ShowInfoBox("Diretor incluido com sucesso");
        }
        else if(TipoMandato == TpMandato.Camara)
        {
            db.CamaraRep.IncluiMembro(cmbCamara.SelectedValue.To<int>(), CodCargo, ucPessoaTit.IdPessoa.Value, periodo);
            ShowInfoBox("Novo membro incluido com sucesso");
        }
        else if(TipoMandato == TpMandato.Comissao)
        {
            db.Comissoes.IncluiMembro(IdComissao, CodCargo, ucPessoaTit.IdPessoa.Value, ucPessoaSup.IdPessoa, cmbOrdem.SelectedValue.To<int>(), periodo);
            ShowInfoBox("Novo membro de comissão incluido com sucesso");
        }
        else if(TipoMandato == TpMandato.Grupo)
        {
            db.Grupos.IncluiMembro(IdGrupo, CodCargo, ucPessoaTit.IdPessoa.Value, periodo);
            ShowInfoBox("Novo membro de grupo de trabalho incluido com sucesso");
        }

        ClosePopup();
    }

</script>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <box:NTab runat="server" Text="Mandato" Selected="true">

        <div class="form-div">
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Cargo</span></div>
                    <div class="field">
                        <asp:Literal runat="server" ID="ltrCargo"/>
                    </div>
                </div>
            </div>
            <div class="sub-form-row" id="trCamara" runat="server" visible="false">
                <div class="div-sub-form form-uc">
                    <div><span>Câmara especializada</span></div>
                    <box:NDropDownList runat="server" ID="cmbCamara" Width="100%" Required="true" />
                </div>
            </div>
            <div class="sub-form-row" id="trOrdem" runat="server" visible="false">
                <div class="div-sub-form form-uc">
                    <div><span>Ordem</span></div>
                    <box:NDropDownList runat="server" ID="cmbOrdem" Width="160">
                        <asp:ListItem Text="1º Suplente" Value="1" Selected="True" />
                        <asp:ListItem Text="2º Suplente" Value="2" />
                        <asp:ListItem Text="3º Suplente" Value="3" />
                        <asp:ListItem Text="4º Suplente" Value="4" />
                        <asp:ListItem Text="5º Suplente" Value="5" />
                    </box:NDropDownList>
                </div>
            </div>
            <div class="sub-form-row" id="trTitular" runat="server">
                <div class="div-sub-form form-uc">
                    <div><span><asp:Literal runat="server" ID="ltrTitular" Text="Conselheiro" /></span></div>
                    <uc:Pessoa runat="server" ID="ucPessoaTit" IsProfissional="true" Required="true" />
                </div>
            </div>
            <div class="sub-form-row" id="trSuplente" runat="server">
                <div class="div-sub-form form-uc">
                    <div><span>Suplente</span></div>
                    <uc:Pessoa runat="server" ID="ucPessoaSup" IsProfissional="true" />
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form">
                    <div><span>Início</span></div>
                    <box:NTextBox runat="server" ID="txtDtIni" MaskType="Date" Required="true" />
                </div>
            </div>
            <div class="sub-form-row">
                <box:NCheckBox runat="server" ID="chkEmAberto" Text="Manter cargo em aberto até as próximas eleições" Checked="true" AutoPostBack="true" OnCheckedChanged="chkSemDtFim_CheckedChanged" />
            </div>
            <div class="sub-form-row" id="trFim" runat="server" visible="false">
                <div class="div-sub-form">
                    <div><span>Fim</span></div>
                    <box:NTextBox runat="server" ID="txtDtFim" MaskType="Date" Required="true" />
                </div>
            </div>
        </div>

        <box:NButton runat="server" ID="btnSalvar" Theme="Primary" Icon="ok" Width="150" Text="Salvar" OnClick="btnSalvar_Click" CssClass="right" />

    </box:NTab>
</asp:Content>
