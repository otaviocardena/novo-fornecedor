﻿<%@ Page Title="Indicação para Plenário Futuro" Resource="siplen" Language="C#" %>
<script runat="server">

    private CreaspContext db = new CreaspContext();

    private int? IdIndicacao => Request.QueryString["IdIndicacao"].To<int?>();

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        RegisterResource("siplen.plenfut").ThrowIfNoAccess();

        txtInspetoria.RegisterDataSource(c =>
        {
            return db.Query<Inspetoria>()
                .Include(x => x.Unidade)
                .Include(x => x.Cep)
                .Where(x => x.NoInspetoria.ToUpper().Contains(c.ToUpper()))
                .ToList();
        });
    }

    protected override void OnPrepareForm()
    {
        cmbMandato.Items.Clear();

        for(var ano = DateTime.Now.Year - 2; ano <= DateTime.Now.Year + 3; ano++)
        {
            cmbMandato.Items.Add(new ListItem(string.Format("{0} - {1}", ano, ano + 2), ano.ToString()));
        }

        cmbMandato.SelectedValue = DateTime.Now.Year.ToString();

        ucEntidade_EntidadeChanged(null, null);

        if(IdIndicacao.HasValue)
        {
            var indicacao = db.SingleById<Indicacao>(IdIndicacao);
            var inspetoria = db.SingleOrDefault<Inspetoria>(i => i.IdInspetoria == indicacao.IdInspetoria);
            cmbMandato.SelectedValue = indicacao.AnoIni.ToString();
            ucEntidade.LoadEntidade(indicacao.IdEntidade);
            ucEntidade_EntidadeChanged(null, null);
            cmbCamara.SelectedValue = indicacao.CdCamara.ToString();
            ucPessoaTit.LoadPessoa(indicacao.IdPessoaTit);
            ucPessoaSup.LoadPessoa(indicacao.IdPessoaSup);
            ucPessoaTit.Enabled = indicacao.DtPosseTit == null;
            ucPessoaSup.Enabled = indicacao.DtPosseSup == null;
            txtNoHistorico.Text = indicacao.NoHistorico;
            txtInspetoria.Text = inspetoria?.NoInspetoria;
            txtInspetoria.Value = inspetoria?.IdInspetoria.ToString();
            cmbMandato.Enabled = ucEntidade.Enabled = cmbCamara.Enabled = false;

            ucPessoaTit.Enabled = !ucPessoaTit.IdPessoa.HasValue;
            ucPessoaSup.Enabled = !ucPessoaSup.IdPessoa.HasValue;

            spnTit.Visible = ucPessoaTit.IdPessoa.HasValue;
            spnSup.Visible = ucPessoaSup.IdPessoa.HasValue;

            this.Audit(indicacao, indicacao.IdIndicacao);
        }
        else
        {
            spnTit.Visible = spnSup.Visible = false;
        }

        this.BackTo("PlenFut.aspx");
        SetDefaultButton(btnSalvar);
    }

    protected void ucEntidade_EntidadeChanged(object sender, EventArgs e)
    {
        cmbCamara.Enabled = ucEntidade.IdEntidade.HasValue;

        if(ucEntidade.IdEntidade.HasValue)
        {
            cmbCamara.Bind(db.Camaras.BuscaPorEntidade(ucEntidade.IdEntidade.Value, Periodo.Hoje));
        }
        else
        {
            cmbCamara.Items.Clear();
        }
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        if(IdIndicacao.HasValue)
        {
            db.Indicacoes.AlteraIndicacao(
                IdIndicacao.Value,
                ucPessoaTit.IdPessoa,
                ucPessoaSup.IdPessoa,
                txtNoHistorico.Text,
                txtInspetoria.Value.To<int?>());

            ShowInfoBox("Indicação alterada com sucesso");

            OnPrepareForm();
        }
        else
        {
            var idIndicacao = db.Indicacoes.NovaIndicacao(
                cmbMandato.SelectedValue.To<int>(),
                ucEntidade.IdEntidade.Value,
                cmbCamara.SelectedValue.To<int>(),
                ucPessoaTit.IdPessoa,
                ucPessoaSup.IdPessoa,
                txtNoHistorico.Text,
                txtInspetoria.Value.To<int?>());

            ShowInfoBox("Nova indicação gravada com sucesso");

            Redirect("PlenFut.Indicacao.aspx?IdIndicacao=" + idIndicacao);
        }
    }

    protected void btnRemove_Command(object sender, CommandEventArgs e)
    {
        db.Indicacoes.RemoveTitularSuplente(IdIndicacao.Value, e.CommandName.To<bool>());
        OnPrepareForm();
    }

    protected void btnDocs_Command(object sender, CommandEventArgs e)
    {
        pnlDocs.Open();
        var titular = e.CommandName.To<bool>();
        tabDocs.Text = (titular ? "Titular" : "Suplente");

        ucDocs.MontaTela(
            TpRefDoc.Indicacao,
            titular ? IdIndicacao.Value : -IdIndicacao.Value,
            titular ? ucPessoaTit.IdPessoa.Value : ucPessoaSup.IdPessoa.Value,
            true);
    }

</script>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <box:NButton runat="server" ID="btnSalvar" Theme="Primary" Icon="ok" Text="Salvar" CssClass="right mt-30" OnClick="btnSalvar_Click" />

    <div class="form-div">
        <div class="sub-form-row">
            <div class="div-sub-form form-uc">
                <div><span>Mandato</span></div>
                <box:NDropDownList runat="server" ID="cmbMandato" Width="120" Required="true" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form form-uc">
                <div><span>Entidade</span></div>
                <uc:Entidade runat="server" ID="ucEntidade" OnEntidadeChanged="ucEntidade_EntidadeChanged" Required="true" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form form-uc">
                <div><span>Câmara</span></div>
                <box:NDropDownList runat="server" ID="cmbCamara" Width="100%" Required="true" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form form-uc">
                <div><span>Titular</span></div>
                <uc:Pessoa runat="server" ID="ucPessoaTit" IsProfissional="true" />
            </div>
            <div class="btsTit" runat="server" id="spnTit">
                <box:NLinkButton runat="server" ID="btnRemoveTit" CssClass="linkBts" ButtonTheme="Default" Icon="ccw" ToolTip="Substituir titular" OnClientClick="return confirm('Confirma substituir o titular?');" OnCommand="btnRemove_Command" CommandName="true" />
                <box:NLinkButton runat="server" ID="btnDocsTit" ButtonTheme="Default" Icon="doc" ToolTip="Documentação" CssClass="linkBts" OnCommand="btnDocs_Command" CommandName="true" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form form-uc">
                <div><span>Suplente</span></div>
                <uc:Pessoa runat="server" ID="ucPessoaSup" IsProfissional="true" />
                <span runat="server" id="spnSup" style="width: 100px; text-align: right">
                    <box:NLinkButton runat="server" ID="btnRemoveSup" ButtonTheme="Default" Icon="ccw" ToolTip="Substituir suplente" CssClass="space-left nowrap" OnClientClick="return confirm('Confirma substituir o titular?');" OnCommand="btnRemove_Command" CommandName="false" />
                    <box:NLinkButton runat="server" ID="btnDocsSup" ButtonTheme="Default" Icon="doc" ToolTip="Documentação" CssClass="space-left" OnCommand="btnDocs_Command" CommandName="false" />
                </span>
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Município</span></div>
                <box:NAutocomplete runat="server" ID="txtInspetoria" Width="100%" DataTextField="NoInspetoria" DataValueField="IdInspetoria" Template="{Text}" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Observações</span></div>
                <box:NTextBox runat="server" ID="txtNoHistorico" MaxLength="2000" Width="100%" TextMode="MultiLine" TextCapitalize="Upper" />
            </div>
        </div>
    </div>

    <box:NPopup runat="server" ID="pnlDocs" Title="Documentação" Width="750">
        <box:NTab runat="server" ID="tabDocs" Text="Titular/Suplete" Selected="true">
            <uc:Documento runat="server" ID="ucDocs" />
        </box:NTab>
    </box:NPopup>

    <script src="../Content/Scripts/sidebar.js"></script>
    <script>selectButton("spl-plenFut"); showReload("siplen");</script>

</asp:Content>
