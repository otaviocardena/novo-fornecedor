﻿<%@ Page Title="Inspetorias" Resource="siplen" Language="C#" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        grdList.RegisterPagedDataSource((p) =>
        {
            var unidades = new List<int>();

            if(cmbUnidade.HasValue())
            {
                unidades.Add(cmbUnidade.SelectedValue.To<int>());
            }
            else if(cmbGRE.HasValue())
            {
                unidades.AddRange(db.Unidades.ListaUnidadesFilhas(cmbGRE.SelectedValue.To<int>()).Select(x => x.CdUnidade));
            }

            return db.Query<Inspetoria>()
                .Include(x => x.Unidade)
                .Include(x => x.Cep)
                .Where(txtNoInspetoria.HasValue(), x => x.NoInspetoria.StartsWith(txtNoInspetoria.Text.ToLike()))
                .Where(unidades.Count > 0, x => unidades.Contains(x.CdUnidade))
                .Where(optRepSim.Checked, x => x.FgRepresentacao == true)
                .Where(optRepNao.Checked, x => x.FgRepresentacao == false)
                .Where(optAtivo.Checked, x => x.DtFim >= DateTime.Today)
                .Where(optInativos.Checked, x => x.DtFim < DateTime.Today)
                .ToPage(p.DefaultSort<Inspetoria>(x => x.NoInspetoria));
        });

        RegisterRestorePageState(() => grdList.DataBind());

        RegisterResource("siplen.inspetoria").Disabled(lnkIncluir).GridColumns(grdList, "del");
    }

    protected override void OnPrepareForm()
    {
        cmbGRE.Bind(db.Unidades.ListaUnidades(TpUnidade.DeptoRegional, null).Select(x => new { x.CdUnidade, x.NoUnidade }));

        grdList.DataBind();

        SetDefaultButton(btnPesquisar);
    }

    protected void btnPesquisar_Click(object sender, EventArgs e)
    {
        grdList.DataBindPaged();
    }

    protected void grdList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if(e.CommandName == "alt")
        {
            var idInspetoria = e.DataKey<int>(sender);
            Redirect("Inspetoria.Form.aspx?IdInspetoria=" + idInspetoria);
        }
        else if(e.CommandName == "del")
        {
            var idInspetoria = e.DataKey<int>(sender);
            db.Inspetorias.Exclui(idInspetoria);
            grdList.DataBind();
            ShowInfoBox("Inspetoria excluida com sucesso");
        }
    }

    protected void cmbGRE_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(cmbGRE.HasValue())
        {
            cmbUnidade.Bind(db.Unidades.ListaUnidadesFilhas(cmbGRE.SelectedValue.To<int>()).Select(x => new { x.CdUnidade, x.NoUnidade }));
            cmbUnidade.Enabled = true;
        }
        else
        {
            cmbUnidade.Items.Clear();
            cmbUnidade.Enabled = true;
        }
    }


</script>

<asp:Content ContentPlaceHolderID="content" runat="server">

    <div class="mt-30 c-primary-blue">
        <span>Inspetoria</span>
        <box:NHyperLink runat="server" ID="lnkIncluir" ButtonTheme="Success" Icon="plus" CssClass="right ml-10 new" Text="Nova inspetoria" NavigateUrl="Inspetoria.Form.aspx" />
        <box:NButton runat="server" ID="btnPesquisar" Icon="search" Text="Pesquisar" CssClass="right" Theme="Default" OnClick="btnPesquisar_Click" />
    </div>

    <div class="form-div">
        <div class="sub-form-row">
            <div class="div-sub-form ">
                <div><span>Nome</span></div>
                <box:NTextBox runat="server" ID="txtNoInspetoria" TextCapitalize="Upper" Width="100%" autofocus />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Gerência</span></div>
                <box:NDropDownList runat="server" ID="cmbGRE" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="cmbGRE_SelectedIndexChanged"/>
            </div>
            <div class="div-sub-form ml-10">
                <div><span>Unidade</span></div>
                <box:NDropDownList runat="server" ID="cmbUnidade" Width="100%" Enabled="false" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Exibir</span></div>
                <box:NRadioButton runat="server" ID="optAtivo" Text="Somente ativos" Checked="true" GroupName="dtfim" />
                <box:NRadioButton runat="server" ID="optInativos" Text="Somente inativos" GroupName="dtfim" />
                <box:NRadioButton runat="server" Text="Todos" GroupName="dtfim" />
            </div>
            <div class="div-sub-form">
                <div><span>Representação</span></div>
                <box:NRadioButton runat="server" ID="optRepSim" Text="Sim" GroupName="rep" />
                <box:NRadioButton runat="server" ID="optRepNao" Text="Não" GroupName="rep" />
                <box:NRadioButton runat="server" Text="Todos" GroupName="rep" Checked="true" />
            </div>
        </div>
    </div>

    <box:NGridView runat="server" ID="grdList" ItemType="Creasp.Siplen.Inspetoria" DataKeyNames="IdInspetoria" OnRowCommand="grdList_RowCommand">
        <Columns>
            <asp:TemplateField HeaderText="Nome" SortExpression="NoInspetoria">
                <ItemTemplate>
                    <%# Item.NoInspetoria %>
                    <box:NLabel runat="server" Theme="Info" ToolTip="Representação" Text="R" CssClass="space-left" Visible=<%# Item.FgRepresentacao %> />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Unidade.NoUnidade" HeaderText="Unidade" SortExpression="Unidade.NoUnidade" />
            <asp:BoundField DataField="Cep.NoCidade" HeaderText="Cidade" HeaderStyle-Width="250" SortExpression="Cep.NoCidade" />
            <box:NButtonField Icon="pencil" CommandName="alt" Theme="Info" ToolTip="Alterar" />
            <box:NButtonField Icon="cancel" CommandName="del" Theme="Danger" ToolTip="Excluir" OnClientClick="return confirm('Confirma a exclusão desta inspetoria?')" />
        </Columns>
        <EmptyDataTemplate>Nenhuma inspetoria encontrada para o filtro utilizado</EmptyDataTemplate>
    </box:NGridView>

    <script src="../Content/Scripts/pagination.js"></script>
    <script>paginate()</script>

    <script src="../Content/Scripts/sidebar.js"></script>
    <script>selectButton("spl-inspetoria"); showReload("siplen");</script>

</asp:Content>