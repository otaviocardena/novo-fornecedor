﻿<%@ Page Title="SIPLEN - Sistema de Plenário" Language="C#" %>
<script runat="server">

    protected void Page_Load(object sender, EventArgs e)
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        RegisterResource("elo");
    }

</script>
<asp:Content runat="server" ContentPlaceHolderID="content">

    <table style="width: 100%; display: none">
        <tr>
            <td style="width: 50%;" class="padding-right">
                <table class="form">
                    <caption>Próximos eventos</caption>
                    <tr>
                        <td>
                            A seguir...
                        </td>
                    </tr>
                </table>
            </td>
            <td style="width: 50%" class="padding-left">
                <table class="form">
                    <caption>Grupos encerrando</caption>
                    <tr>
                        <td>
                            A seguir...
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>

</asp:Content>