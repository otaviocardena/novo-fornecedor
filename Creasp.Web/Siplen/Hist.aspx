﻿<%@ Page Title="Histórico Profissional" Resource="siplen" Language="C#" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        grdList.RegisterPagedDataSource((p) =>
            db.Query<Pessoa>()
                .Where(txtNoPessoa.HasValue(), x => x.NoPessoa.Contains(txtNoPessoa.Text.ToLike()))
                .Where(txtNrCreasp.HasValue(), x => x.NrCreasp == txtNrCreasp.Text)
                .Where(txtNrCpf.HasValue(), x => x.NrCpf == txtNrCpf.Text.UnMask())
                .Where(x => x.NrCreasp != null) // somente profissionais tem creasp
                .ToPage(p.DefaultSort<Pessoa>(x => x.NoPessoa)));

        RegisterRestorePageState(() => grdList.DataBind());
    }

    protected override void OnPrepareForm()
    {
        SetDefaultButton(btnPesquisar);
    }

    protected void btnPesquisar_Click(object sender, EventArgs e)
    {
        grdList.DataBindPaged();
    }

</script>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <box:NToolBar runat="server" ID="tlbInsp">
        <box:NButton runat="server" ID="btnPesquisar" Icon="search" Text="Pesquisar" Theme="Default" OnClick="btnPesquisar_Click" />
    </box:NToolBar>

    <div class="mt-30 c-primary-blue">
        <span>Histórico</span>
    </div>

    <div class="form-div">
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Nome</span></div>
                <box:NTextBox runat="server" ID="txtNoPessoa" TextCapitalize="Upper" Width="100%" autofocus />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>CREASP</span></div>
                <box:NTextBox runat="server" ID="txtNrCreasp" TextCapitalize="Upper" MaskType="Custom" MaskFormat="9999999999" Width="150" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>CPF</span></div>
                <box:NTextBox runat="server" ID="txtNrCpf" TextCapitalize="Upper" Width="150" MaskType="Custom" MaskFormat="999.999.999-99" />
            </div>
        </div>
    </div>

    <box:NGridView runat="server" ID="grdList" ItemType="Creasp.Core.Pessoa">
        <Columns>
            <asp:HyperLinkField DataTextField="NrCreasp" DataNavigateUrlFields="IdPessoa" SortExpression="NrCreasp" DataNavigateUrlFormatString="Hist.View.aspx?IdPessoa={0}" HeaderText="CREASP" HeaderStyle-Width="130" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="NoPessoa" HeaderText="Profissional" SortExpression="NoPessoa" />
            <asp:BoundField DataField="NoTituloAbr" HeaderText="Título(s)" SortExpression="NoTitulo" ItemStyle-CssClass="nowrap-ellipsis" HeaderStyle-Width="270" />
        </Columns>
        <EmptyDataTemplate>Não foi encontrado nenhum profissional para o filtro informado</EmptyDataTemplate>
    </box:NGridView>

    <script src="../Content/Scripts/pagination.js"></script>
    <script>paginate()</script>

    <script src="../Content/Scripts/sidebar.js"></script>
    <script>selectButton("spl-his"); showReload("siplen");</script>

</asp:Content>