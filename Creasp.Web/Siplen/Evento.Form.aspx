﻿<%@ Page Title="Novo Evento" Resource="siplen" Language="C#" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();

    private int? IdEvento { get { return ViewState["IdEvento"].To<int?>(); } set { ViewState["IdEvento"] = value; } }

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        RegisterResource("siplen.evento").Disabled(btnSalvar, frm);
    }

    protected override void OnPrepareForm()
    {
        IdEvento = Request.QueryString["IdEvento"].To<int?>();

        if (IdEvento.HasValue)
        {
            MontaTela();
            PageTitle = "Editar Evento";
            this.BackTo("Evento.View.aspx?IdEvento=" + IdEvento);
        }
        else
        {
            PageTitle = "Novo Evento";
            this.BackTo("Evento.aspx");
        }

        SetDefaultButton(btnSalvar);
    }

    private void MontaTela()
    {
        var e = db.Eventos.BuscaEvento(IdEvento.Value, false);

        txtNoEvento.Text = e.NoEvento;
        txtDtEvento.SetText(e.DtEvento);
        txtDtEventoFim.SetText(e.DtEventoFim);
        txtHrEventoIni.Text = e.DtEvento.ToString("HH:mm");
        txtHrEventoFim.Text = e.DtEventoFim.ToString("HH:mm");
        txtNrCep.SetText(e.NrCep);
        txtNrCep_TextChanged(null, null);
        txtNoLocal.Text = e.NoLocal;
        radContaFaltaSim.Checked = e.FgContaFalta;
        radContaFaltaNao.Checked = !e.FgContaFalta;
        txtNoHistorico.Text = e.NoHistorico;
        radMultiplaSim.Checked = e.FgPermiteDiariaDup;
        radMultiplaNao.Checked = !e.FgPermiteDiariaDup;

        btnSalvar.Enabled = this.IsProprietario(e, "siplen.evento.admin");

    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        txtHrEventoIni.Validate().Time();
        txtHrEventoFim.Validate().Time();

        var dtEvento = txtDtEvento.Text.To<DateTime>() + TimeSpan.ParseExact(txtHrEventoIni.Text, "hh\\:mm", System.Globalization.CultureInfo.InvariantCulture);
        var dtEventoFim = txtDtEventoFim.Text.To<DateTime>() + TimeSpan.ParseExact(txtHrEventoFim.Text, "hh\\:mm", System.Globalization.CultureInfo.InvariantCulture);

        if (dtEventoFim < dtEvento) throw new NBoxException("A data de final do evento deve ser maior que a data de inicio do evento");

        if (IdEvento.HasValue == false)
        {
            IdEvento = db.Eventos.IncluiEvento(
                txtNoEvento.Text,
                dtEvento,
                dtEventoFim,
                txtNrCep.Text.UnMask(),
                txtNoLocal.Text,
                radContaFaltaSim.Checked,
                txtNoHistorico.Text,
                radMultiplaSim.Checked);

            ShowInfoBox("Evento criado com sucesso");
        }
        else
        {
            db.Eventos.AlteraEvento(IdEvento.Value,
                txtNoEvento.Text,
                dtEvento,
                dtEventoFim,
                txtNrCep.Text.UnMask(),
                txtNoLocal.Text,
                radContaFaltaSim.Checked,
                txtNoHistorico.Text,
                radMultiplaSim.Checked);

            ShowInfoBox("Evento alterado com sucesso");
        }

        Redirect("Evento.View.aspx?IdEvento=" + IdEvento);
    }

    protected void txtNrCep_TextChanged(object sender, EventArgs e)
    {
        if(txtNrCep.HasValue())
        {
            var cep = db.Ceps.BuscaCep(txtNrCep.Text.UnMask());
            if (cep == null) throw new NBoxException(txtNrCep, "Número de CEP inválido");
            lblNoEndereco.Text = $"{cep.NoCidade} / {cep.CdUf}";
        }
        else
        {
            lblNoEndereco.Text = "";
        }
    }

    protected void txtDtEvento_TextChanged(object sender, EventArgs e)
    {
        if(!txtDtEventoFim.HasValue())
        {
            txtDtEventoFim.Text = txtDtEvento.Text;
        }
    }

</script>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <div class="form-div" runat="server" id="frm">
        <div class="sub-form-row">
            <div class="div-sub-form form-uc">
                <div><span>Exercicio</span></div>
                <div class="field">
                    <%= db.NrAnoBase %>
                    <span class="help right">Indica que a emissão de NERP será executada no exercicio informado</span>
                </div>
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Descrição</span></div>
                <box:NTextBox runat="server" ID="txtNoEvento" MaxLength="80" Required="true" Width="100%" TextCapitalize="Upper" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Data/Hora</span></div>
                <box:NTextBox runat="server" ID="txtDtEvento" MaskType="Date" CssClass="w-150" Required="true" AutoPostBack="true" OnTextChanged="txtDtEvento_TextChanged" />
                &nbsp;-&nbsp;&nbsp;<box:NTextBox runat="server" ID="txtHrEventoIni" MaskType="Custom" MaskFormat="99:99" Width="60" Required="true" />
                &nbsp;&nbsp;até&nbsp;&nbsp;
                <box:NTextBox runat="server" ID="txtDtEventoFim" MaskType="Date" CssClass="w-150" Required="true" />
                &nbsp;-&nbsp;&nbsp;
                <box:NTextBox runat="server" ID="txtHrEventoFim" MaskType="Custom" MaskFormat="99:99" Width="60" Required="true" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>CEP</span></div>
                <div class="input-group">
                    <box:NTextBox runat="server" ID="txtNrCep" MaskType="Custom" MaskFormat="999.99-999" AutoPostBack="true" OnTextChanged="txtNrCep_TextChanged" Width="150" />
                    <box:NLabel runat="server" ID="lblNoEndereco" CssClass="space-left field" />
                </div>
                <div class="help nowrap">O campo de CEP é obrigatório para o cáculo automático de quilometragem</div>
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Local</span></div>
                <box:NTextBox runat="server" ID="txtNoLocal" Required="true" Width="100%" MaxLength="100" TextCapitalize="Upper" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Conta falta</span></div>
                <box:NRadioButton runat="server" ID="radContaFaltaSim" Text="Sim" GroupName="ContaFalta" />
                <box:NRadioButton runat="server" ID="radContaFaltaNao" Text="Não" Checked="true" GroupName="ContaFalta" />
                <span class="help right">Indica que as faltas deste evento contam para a perda do mandato</span>
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Multipla diária</span></div>
                <box:NRadioButton runat="server" ID="radMultiplaSim" Text="Sim" GroupName="MultiplaDiaria" />
                <box:NRadioButton runat="server" ID="radMultiplaNao" Text="Não" Checked="true" GroupName="MultiplaDiaria" />
                <span class="help right">Permitir pagar multiplas diárias aos credores.</span>
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Observações</span></div>
                <box:NTextBox runat="server" ID="txtNoHistorico" Width="100%" Rows="2" TextCapitalize="Upper" TextMode="MultiLine" />
            </div>
        </div>
    </div>

    <box:NButton runat="server" ID="btnSalvar" Icon="ok" Text="Salvar" CssClass="right" Theme="Primary" OnClick="btnSalvar_Click" />

    <script src="../Content/Scripts/sidebar.js"></script>
    <script>selectButton("nerp-event"); showReload("nerp");</script>

</asp:Content>