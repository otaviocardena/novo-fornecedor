﻿using NPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using Creasp.Core;
using NBox.Services;
using NBox.Core;

namespace Creasp.Siplen
{
    public class InspetorService : MandatoService
    {
        public static int[] Mandatos = new int[] { 2012, 2015, 2018, 2021, 2024 };

        protected ICreanet creanet = Factory.Get<ICreanet>();

        public InspetorService(ServiceContext context) : base(context) { }

        /// <summary>
        /// Inclui um mandato de inspetor
        /// </summary>
        public int Inclui(int idInspetoria, int idPessoa, string cdCargo, int cdCamara, int anoIni, Periodo periodo, string noHistorico)
        {
            ValidaCadastro(idPessoa, cdCamara);

            var mandato = new Mandato
            {
                TpMandato = TpMandato.Inspetor,
                CdCargo = cdCargo,
                IdPessoa = idPessoa,
                CdCamara = cdCamara,
                IdInspetoria = idInspetoria,
                DtIni = periodo.Inicio,
                DtFim = periodo.Fim,
                AnoIni = anoIni,
                AnoFim = anoIni + 2,
                NoHistorico = noHistorico
            };

            db.Insert(mandato);

            return mandato.IdMandato;
        }

        /// <summary>
        /// Altera um inspetor
        /// </summary>
        public void Altera(int idMandato, string cdCargo, int idInspetoria, int cdCamara, Periodo periodo, int? cdMotivo, string noHistorico)
        {
            periodo.ValidaMax('A', 3);

            var m = db.SingleById<Mandato>(idMandato);

            if (periodo.Inicio < new DateTime(m.AnoIni, 1, 1) || periodo.Fim > new DateTime(m.AnoFim, 12, 31)) throw new NBoxException("O período de mandato deve estar dentro de {0}-{1}", m.AnoIni, m.AnoFim);

            ValidaCadastro(m.IdPessoa, cdCamara);

            m.IdInspetoria = idInspetoria;
            m.CdCamara = cdCamara;
            m.CdCargo = cdCargo;
            m.DtIni = periodo.Inicio;
            m.DtFim = periodo.Fim;
            m.CdMotivo = cdMotivo;
            m.NoHistorico = noHistorico;

            db.AuditUpdate(m);
        }

        /// <summary>
        /// Dá posse aos inspetores na data inforada
        /// </summary>
        public void Posse(IEnumerable<int> mandatos, DateTime dtPosse)
        {
            using (var trans = db.GetTransaction())
            {
                foreach (var id in mandatos)
                {
                    var m = db.Query<Mandato>()
                        .Include(x => x.Pessoa)
                        .Where(x => x.IdMandato == id)
                        .Single();

                    if (dtPosse.Year > m.AnoFim || dtPosse.Year < m.AnoIni) throw new NBoxException("A data deve estar entre o intervalo de anos {0}-{1}", m.AnoIni, m.AnoFim);

                    m.DtIni = dtPosse;
                    m.DtFim = new DateTime(m.AnoFim, 12, 31);

                    var val = ValidaPosse(m, m.GetPeriodo());

                    if (val != null) throw new NBoxException(val);

                    db.Update(m);
                }

                trans.Complete();
            }
        }

        /// <summary>
        /// Carrega um inspetor
        /// </summary>
        public Mandato BuscaInspetor(int idMandato)
        {
            return db.Query<Mandato>()
                .Include(x => x.Cargo)
                .Include(x => x.Inspetoria)
                .Include(x => x.Camara)
                .Include(x => x.Pessoa)
                .Where(x => x.TpMandato == TpMandato.Inspetor)
                .Where(x => x.IdMandato == idMandato)
                .Single();
        }

        /// <summary>
        /// Lista os inspetores conforme filtro
        /// </summary>
        public QueryPage<Mandato> ListaInspetores(Pager p, Periodo periodo, string noPessoa = null, int? anoIni = null, int? idInspetoria = null, int? cdCamara = null, string cdCargo = null, bool incluiLicencas = false)
        {
            var q = db.Query<Mandato>()
                .Include(x => x.Camara)
                .Include(x => x.Inspetoria)
                .Include(x => x.Pessoa)
                .Include(x => x.Cargo)
                .Where(x => x.TpMandato == TpMandato.Inspetor)
                .Where(anoIni > 0, x => x.AnoIni == anoIni)
                .Where(noPessoa != null, x => x.Pessoa.NoPessoa.Contains(noPessoa.ToLike()))
                .Where(idInspetoria != null, x => x.IdInspetoria == idInspetoria)
                .Where(cdCamara != null, x => x.CdCamara == cdCamara)
                .Where(cdCargo != null, x => x.CdCargo == cdCargo);

            if (anoIni == 0)
            {
                q = q.Where(x => x.DtIni == Periodo.MinDate && x.DtFim == Periodo.MaxDate);
            }
            else
            {
                q = q.WherePeriodo(periodo).Where(x => x.DtIni > Periodo.MinDate);
            }

            var result = q.ToPage(p.DefaultSort<Mandato>(x => x.Pessoa.NoPessoa));

            if(incluiLicencas)
            {
                var lic = db.Query<Licenca>()
                    .WherePeriodo(periodo)
                    .OrderBy(x => x.DtIni)
                    .ToList();

                result.Items.ForEach((x, i) => x.Licencas = lic.Where(a => a.IdPessoa == x.IdPessoa).ToList());
            }

            if (anoIni == 0)
            {
                result.Items.ForEach(x => x.Mensagem = ValidaPosse(x, new Periodo(DateTime.Today, new DateTime(x.AnoFim, 12, 31))));
            }

            return result;
        }

        /// <summary>
        /// Antes de excluir uma indicação, anexa no historico do profissional que ele não assumiu como inspetor
        /// </summary>
        public override void ExcluiMandato(int idMandato)
        {
            var m = db.Query<Mandato>()
                .Include(x => x.Pessoa)
                .Include(x => x.Cargo)
                .Include(x => x.Inspetoria)
                .Where(x => x.IdMandato == idMandato)
                .SingleOrDefault();

            var msg = $"{DateTime.Today.Year} - Não tomou posse de {m.Cargo.NoCargo} em {m.Inspetoria.NoInspetoria} no mandato {m.AnoIni}-{m.AnoFim}";

            m.Pessoa.NoHistorico = m.Pessoa.NoHistorico == null ? msg : m.Pessoa.NoHistorico + "\n" + msg;
            db.Update(m.Pessoa);

            base.ExcluiMandato(idMandato);
        }

        /// <summary>
        /// Retorna o ano inicio do mandato atual (baseado na data de hoje)
        /// </summary>
        public static int MandatoAtual => Mandatos.Where(x => x == DateTime.Today.Year || x == DateTime.Today.Year - 1 || x == DateTime.Today.Year - 2).Single();

        #region Validações

        /// <summary>
        /// Valida se um inspetor está apto a tomar posse. Se tiver algum problema, retorna como mensagem string;
        /// </summary>
        public string ValidaPosse(Mandato mandato, Periodo periodo)
        {
            if (creanet.ConsultaAnuidade(mandato.Pessoa.NrCreasp) == AnuidadeSituacao.EmDebito) return "Inspetor não está com anuidade em dia";

            try
            {
                ValidaMandatoConselheiro(mandato.IdPessoa, periodo);
                ValidaCamarasInspetorias(mandato, periodo);
                ValidaDocumentacao(mandato.IdMandato);
                return null;
            }
            catch (NBoxException ex)
            {
                return ex.Message;
            }
        }

        /// <summary>
        /// Valida se a documentação obrigatória foi anexada
        /// </summary>
        private void ValidaDocumentacao(int idMandato)
        {
            var doc = new DocumentoService(context);
            var docs = doc.ListaDocumentos(TpRefDoc.Inspetor, idMandato);
            var faltante = docs.FirstOrDefault(x => x.Tipo.FgObrigatorio && x.NoArquivo == null);

            if (faltante != null)
            {
                throw new NBoxException("O documento '{0}' está não foi enviado", faltante.Tipo.NoTipoDoc);
            }
        }

        /// <summary>
        /// Valida se existe mandato de conselheiro para o periodo informado do inspetor
        /// </summary>
        private void ValidaMandatoConselheiro(int idPessoa, Periodo periodo)
        {
            var m = db.Query<Mandato>()
                .Include(x => x.Pessoa)
                .Include(x => x.Cargo)
                .Where(x => x.TpMandato == TpMandato.Conselheiro)
                .Where(x => x.IdPessoa == idPessoa)
                .WherePeriodo(periodo)
                .FirstOrDefault();

            if (m != null)
            {
                throw new NBoxException("O inspetor {0} possui mandato de conselheiro no período {1}-{2}",
                    m.Pessoa.NoPessoa,
                    m.AnoIni, m.AnoFim);
            }
        }

        /// <summary>
        /// Verifica se para o mandato informado já não está preenchida todas as vagas para 
        /// </summary>
        private void ValidaCamarasInspetorias(Mandato mandato, Periodo periodo)
        {
            var insp = db.SingleById<Inspetoria>(mandato.IdInspetoria.Value);

            var m = db.Query<Mandato>()
                .Include(x => x.Pessoa)
                .Include(x => x.Inspetoria)
                .Where(x => x.TpMandato == TpMandato.Inspetor)
                .Where(x => x.CdCamara == mandato.CdCamara)
                .Where(x => x.IdInspetoria == mandato.IdInspetoria)
                .Where(x => x.IdMandato != mandato.IdMandato)
                .WherePeriodo(periodo)
                .FirstOrDefault();

            if(m != null)
            {
                throw new NBoxException("Já existe um inspetor da câmara {0} na inspetoria {1} no período informado",
                    m.Pessoa.NoPessoa, m.Inspetoria.NoInspetoria);
            }
        }

        /// <summary>
        /// Valida se o profissional indica tem a mesma camara de indicação
        /// </summary>
        private void ValidaCadastro(int idPessoa, int cdCamara)
        {
            var pessoa = db.SingleById<Pessoa>(idPessoa);

            if (string.IsNullOrEmpty(pessoa.NrCep)) throw new NBoxException("{0} não possui endereço informado", pessoa.NoPessoa);

            var profissional = creanet.BuscaProfissionais(pessoa.NrCreasp, null, null).SingleOrDefault();
            if (profissional == null) throw new NBoxException("Não foi possível encontrar o profissional {0} no CREANET", pessoa.NoPessoa);

            if (!profissional.Camaras.Contains(cdCamara))
            {
                throw new NBoxException("{0} não possui título para a câmara {1}",
                    pessoa.NoPessoa,
                    db.SingleById<Camara>(cdCamara).NoCamara);
            }

            if(pessoa.NoEmail == null && profissional.NoEmail != null)
            {
                pessoa.NoEmail = profissional.NoEmail;
                db.Update(pessoa);
            }

            if (string.IsNullOrEmpty(pessoa.NoEmail)) throw new NBoxException("{0} não possui email informado", pessoa.NoPessoa);
        }

        #endregion
    }
}