﻿using System;
using System.Collections.Generic;
using System.Linq;
using Creasp.Core;
using Creasp;
using NBox.Core;
using NBox.Services;
using NPoco.Linq;

namespace Creasp.Siplen
{
    public class ConselheiroService : MandatoService
    {
        public ConselheiroService(ServiceContext context) : base(context) { }

        /// <summary>
        /// Busca o mandato de titular do conselheiro em uma determinada data
        /// </summary>
        public Mandato BuscaMandatoTitular(int idPessoa, DateTime dtIni)
        {
            // busca o mandato de conselheiro da pessoa na data
            var mandato = db.Query<Mandato>()
                .Include(x => x.Pessoa)
                .Where(x => x.TpMandato == TpMandato.Conselheiro)
                .Where(x => x.IdPessoa == idPessoa)
                .WherePeriodo(Periodo.UmDia(dtIni))
                .SingleOrDefault();

            if (mandato == null) throw new NBoxException("Não foi encontrado mandato de conselheiro titular para {0} válido em {1:d}",
                db.SingleById<Pessoa>(idPessoa).NoPessoa, dtIni);

            return mandato;
        }

        /// <summary>
        /// Lista o Plenario Atual
        /// </summary>
        public IEnumerable<TitularSuplenteDTO> ListaPlenario(Periodo periodo, int? anoIni, int? idEntidade = null, string noPessoa = null, int? cdCamara = null)
        {
            var lic = db.Query<Licenca>()
                .WherePeriodo(periodo)
                .OrderBy(x => x.DtIni)
                .ToList();

            var cons = db.Query<Mandato>()
                .Include(x => x.Pessoa)
                .Include(x => x.Pessoa.Cep)
                .Include(x => x.Cargo)
                .Include(x => x.Camara)
                .Include(x => x.Entidade)
                .Where(x => x.TpMandato == TpMandato.Conselheiro)
                .WherePeriodo(periodo)
                .Where(anoIni.HasValue, x => x.AnoIni == anoIni.Value)
                .Where(idEntidade.HasValue, x => x.IdEntidade == idEntidade.Value)
                //.Where(!string.IsNullOrEmpty(noPessoa), x => x.Pessoa.NoPessoa.StartsWith(noPessoa))
                .Where(cdCamara.HasValue, x => x.CdCamara == cdCamara)
                .OrderBy(x => x.Pessoa.NoPessoa)
                .ToList()
                .ForEach((x, i) => x.Licencas = lic.Where(a => a.IdPessoa == x.IdPessoa).ToList());

            // calcula as faltas do conselheiros - usando ToList() para evitar muitas chamadas
            cons = ContaFaltas(cons, TpMandato.Conselheiro, periodo.Fim).ToList();

            var titsup = MontaTitularSuplente(cons);

            if (string.IsNullOrEmpty(noPessoa))
            {
                return titsup;
            }
            else
            {
                if(noPessoa.StartsWith("%"))
                {
                    noPessoa = noPessoa.Substring(1);
                }
                return titsup
                        .Where(x => (x.Titular?.Pessoa.NoPessoa.Contains(noPessoa) ?? false) || (x.Suplente?.Pessoa.NoPessoa.Contains(noPessoa) ?? false));
            }
        }

        /// <summary>
        /// Lista o mandato do titular e os mandatos do suplente
        /// </summary>
        public IEnumerable<Mandato> ListaMandatos(int idMandato)
        {
            var titular = db.Query<Mandato>()
                .Include(x => x.Pessoa)
                .Include(x => x.Cargo)
                .Include(x => x.Camara)
                .Include(x => x.Entidade)
                .Where(x => x.TpMandato == TpMandato.Conselheiro)
                .Where(x => x.CdCargo == CdCargo.Titular)
                .Where(x => x.IdMandato == idMandato)
                .Single();

            yield return titular;

            var suplentes = db.Query<Mandato>()
                .Include(x => x.Pessoa)
                .Include(x => x.Cargo)
                .Include(x => x.Camara)
                .Include(x => x.Entidade)
                .Where(x => x.TpMandato == TpMandato.Conselheiro)
                .Where(x => x.CdCargo == CdCargo.Suplente)
                .Where(x => x.IdMandatoTit == idMandato)
                .ToEnumerable();

            foreach(var suplente in suplentes)
            {
                yield return suplente;
            }
        }

        /// <summary>
        /// Faz o afastamento permanente de um membro titular/suplente. Se for afastamento de titular o suplente deve assumir
        /// </summary>
        public void AlteraMandato(int idMandato, Periodo periodo, int? cdMotivo, string noHistorico, bool fgValido)
        {
            var mandato = db.SingleById<Mandato>(idMandato);

            // o periodo deve ser entre os anos de mandato
            if (periodo.Inicio.Year < mandato.AnoIni || periodo.Fim.Year > mandato.AnoFim) throw new NBoxException("O período deve ficar entre {0}-{1}", mandato.AnoIni, mandato.AnoFim);

            // Se o periodo não for ate o final do mandato, obriga motivo
            if (periodo.Fim != new DateTime(mandato.AnoFim, 12, 31) && cdMotivo == null) throw new NBoxException("O motivo é obrigatório quando o período encerrar antes do mandato");

            // se for titular, verifica periodo do suplente (nao pode ser menor que o suplente)
            if (mandato.CdCargo == CdCargo.Titular) ValidaSuplenteForaPeriodo(idMandato, periodo);
            if (mandato.CdCargo == CdCargo.Suplente) ValidaTitularForaPeriodo(mandato.IdMandatoTit.Value, periodo);

            // verifica alterações em periodos anteriores/posteriores
            var antes = periodo.Inicio > mandato.DtIni ? new Periodo(mandato.DtIni, periodo.Inicio.AddDays(-1)) : Periodo.Vazio;
            var depois = periodo.Fim < mandato.DtFim ? new Periodo(periodo.Fim.AddDays(1), mandato.DtFim) : Periodo.Vazio;

            // verifica se não ficou nenhum cargo em aberto no periodo
            if (!antes.IsVazio) ValidaCargosVigentes(mandato.IdPessoa, antes);
            if (!depois.IsVazio) ValidaCargosVigentes(mandato.IdPessoa, depois);

            mandato.DtIni = periodo.Inicio;
            mandato.DtFim = periodo.Fim;
            mandato.CdMotivo = cdMotivo;
            mandato.NoHistorico = noHistorico;
            mandato.FgValido = fgValido;

            db.Update(mandato);
        }

        /// <summary>
        /// Adiciona um novo conselheiro a uma já existente (de titular ou suplente). Retorna o novo mandato do titular (ou atual, em caso de suplente)
        /// </summary>
        public int SubstituiConselheiro(int idMandatoTit, string cdCargo, int idNovaPessoa, Periodo periodo)
        {
            var tit = db.Query<Mandato>()
                .Include(x => x.Pessoa)
                .Where(x => x.TpMandato == TpMandato.Conselheiro)
                .Where(x => x.IdMandato == idMandatoTit)
                .Single();

            var mandato = tit;

            if (cdCargo == CdCargo.Suplente)
            {
                var sup = db.Query<Mandato>()
                    .Include(x => x.Pessoa)
                    .Where(x => x.IdMandatoTit == idMandatoTit)
                    .OrderByDescending(x => x.DtFim)
                    .FirstOrDefault();

                mandato = sup;
            }

            var minima = mandato?.DtIni.AddDays(1) ?? tit.DtIni; // data minima para inicio do mandato
            var srv = new IndicacaoService(context);

            // a data de inicio deve ser maior que a 
            if (periodo.Inicio < minima) throw new NBoxException("A data de início deve ser maior que {0:d}", minima);

            // faz validações do cadastro para saber se pode assumir como conselheiro
            srv.ValidaCadastro(idNovaPessoa, tit.CdCamara.Value);
            srv.ValidaMandatoIndicacaoEmAberto(idNovaPessoa, periodo);
            srv.ValidaMandatoConsecutivo(idNovaPessoa, tit.AnoIni);

            var novo = new Mandato
            {
                TpMandato = TpMandato.Conselheiro,
                CdCargo = cdCargo,
                IdPessoa = idNovaPessoa,
                DtIni = periodo.Inicio,
                DtFim = periodo.Fim,
                AnoIni = tit.AnoIni,
                AnoFim = tit.AnoFim,
                CdCamara = tit.CdCamara,
                IdEntidade = tit.IdEntidade,
                IdMandatoTit = cdCargo == CdCargo.Suplente ? idMandatoTit : (int?)null,
                NoHistorico = mandato != null ? periodo.Inicio.Year + " - ASSUME MANDATO DE CONSELHEIRO " + cdCargo + " NO LUGAR DE " + mandato.Pessoa.NoPessoa : ""
            };

            db.Insert(novo);

            return cdCargo == CdCargo.Titular ? novo.IdMandato : idMandatoTit;
        }

        public override void ExcluiMandato(int idMandato)
        {
            using (var trans = db.GetTransaction())
            {
                var sups = db.Query<Mandato>()
                    .Where(x => x.IdMandatoTit == idMandato)
                    .ToList();

                if (sups.Count > 0)
                {
                    throw new NBoxException("Exclua primeiro os suplentes deste mandato");
                }

                // limpa possivel indicação
                var indicacao = db.Query<Indicacao>()
                    .Where(x => x.IdMandatoTit == idMandato || x.IdMandatoSup == idMandato)
                    .FirstOrDefault();

                if(indicacao != null)
                {
                    if (indicacao.IdMandatoTit == idMandato)
                    {
                        indicacao.IdMandatoTit = null;
                        indicacao.DtPosseTit = null;
                    }
                    else if(indicacao.IdMandatoSup == idMandato)
                    {
                        indicacao.IdMandatoSup = null;
                        indicacao.DtPosseSup = null;
                    }

                    db.AuditUpdate(indicacao);
                }

                // verifica se o mandato estava em algum evento
                var evento = db.Query<EventoParticipante>()
                    .Include(x => x.Evento)
                    .Where(x => x.IdMandatoTit == idMandato || x.IdMandatoSup == idMandato)
                    .FirstOrDefault();

                if (evento != null)
                {
                    throw new NBoxException("O conselheiro participou do evento " + evento.Evento.NoEvento + ". Remova o conselheiro primeiro deste evento.");
                }

                db.AuditDelete<Mandato>(idMandato);

                trans.Complete();
            }
        }

        #region Validações para conselheiro

        /// <summary>
        /// Verifica se existe suplente fora do intervalo informado para este mandato
        /// </summary>
        private void ValidaSuplenteForaPeriodo(int idMandato, Periodo periodo)
        {
            var sup = db.Query<Mandato>()
                .Include(x => x.Pessoa)
                .Where(x => x.IdMandatoTit == idMandato)
                .Where(x => x.DtIni < periodo.Inicio || x.DtFim > periodo.Fim)
                .FirstOrDefault();

            if (sup != null)
            {
                throw new NBoxException("O período do suplente {0} é maior que o período informado. Ajuste primeiro o mandato do suplente", sup.Pessoa.NoPessoa);
            }
        }

        /// <summary>
        /// Verifica se o mandato do titular suporta o periodo novo do suplente
        /// </summary>
        private void ValidaTitularForaPeriodo(int idMandato, Periodo periodo)
        {
            var tit = db.Query<Mandato>()
                .Include(x => x.Pessoa)
                .Where(x => x.IdMandato == idMandato)
                .Single();

            if (periodo.Inicio < tit.DtIni || periodo.Fim > tit.DtFim)
            {
                throw new NBoxException("O período do titular {0} é menor que o período informado. Ajuste primeiro o mandato do titular", tit.Pessoa.NoPessoa);
            }
        }
        /// <summary>
        /// Verifica se o conselheiro não está lotado em algum cargo dentro do periodo informado
        /// </summary>
        private void ValidaCargosVigentes(int idPessoa, Periodo periodo)
        {
            var tipos = new string[] { TpMandato.Camara, TpMandato.Comissao, TpMandato.Diretor, TpMandato.Grupo };

            var cargo = db.Query<Mandato>()
                .Include(x => x.Pessoa)
                .Include(x => x.Cargo)
                .Include(x => x.Comissao, JoinType.Left)
                .Include(x => x.Grupo, JoinType.Left)
                .Where(x => tipos.Contains(x.TpMandato))
                .Where(x => x.IdPessoa == idPessoa)
                .WherePeriodo(periodo)
                .FirstOrDefault();

            if(cargo != null)
            {
                throw new NBoxException("{0} está no cargo de {1} no período de {2} ({3})",
                    cargo.Pessoa.NoPessoa, 
                    cargo.Cargo.NoCargo, 
                    cargo.GetPeriodo().ToString(false),
                    cargo.Comissao?.NoComissao ?? cargo.Grupo?.NoGrupo);
            }
        }

        #endregion
    }
}