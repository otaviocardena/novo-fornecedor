﻿using NBox.Services;
using NBox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using NPoco;
using Creasp.Core;
using NPoco.Linq;

namespace Creasp.Siplen
{
    public class CamaraRepService : MandatoService
    {
        public CamaraRepService(ServiceContext context) : base(context) { }

        /// <summary>
        /// Inclui um novo representante
        /// </summary>
        public void IncluiMembro(int cdCamara, string cdCargo, int idPessoa, Periodo periodo)
        {
            // minimo de 3 dias, maximo de 1 ano
            periodo.ValidaMax('M', 13);

            // não permite 2 coordenadores/adjuntos/representante na mesma camara
            ValidaMesmoCargoPeriodo(cdCamara, cdCargo, periodo);

            // valida se conselheiro já não está nesta camara
            ValidaMandatoRepetido(idPessoa, cdCamara, periodo);

            // valida o periodo de existencia da camara
            var camara = db.SingleById<Camara>(cdCamara);

            if(!camara.GetPeriodo().NoIntervalo(periodo)) throw new NBoxException("O período máximo é de {0}", camara.GetPeriodo().ToString(false));

            var cons = new ConselheiroService(context);

            // procura pelo mandato do titular
            var titular = cons.BuscaMandatoTitular(idPessoa, periodo.Inicio);

            if (cdCargo == CdCargo.Representante)
            {
                if (titular.CdCamara == cdCamara) throw new NBoxException("Para ser representante do plenário o conselheiro não pode ser da mesma câmara especializada");
            }
            else
            {
                if (titular.CdCamara != cdCamara) throw new NBoxException("O conselheiro precisa ser da mesma câmara especializada");
            }

            var membro = new Mandato
            {
                TpMandato = TpMandato.Camara,
                CdCargo = cdCargo,
                IdPessoa = idPessoa,
                DtIni = periodo.Inicio,
                DtFim = periodo.FimMaisProximo(titular.DtFim),
                CdCamara = cdCamara
            };

            db.Insert(membro);
        }

        /// <summary>
        /// Altera apenas o periodo de cargo na camara
        /// </summary>
        public void AlteraMembro(int idMandato, Periodo periodo)
        {
            var membro = db.SingleById<Mandato>(idMandato);

            using (var trans = db.GetTransaction())
            {
                membro.DtIni = periodo.Inicio;
                membro.DtFim = periodo.Fim;

                db.Update(membro);

                trans.Complete();
            }
        }

        /// <summary>
        /// Lista todos os conselheiros (incluindo coordenadores) das camaras especializadas
        /// </summary>
        public IEnumerable<Mandato> ListaConselheiros(Periodo periodo, int? cdCamara)
        {
            var af = db.Query<Licenca>()
                .WherePeriodo(periodo)
                .OrderBy(x => x.DtIni)
                .ToList();

            var membros = db.Query<Mandato>()
                .Include(x => x.Pessoa)
                .Include(x => x.Cargo)
                .Include(x => x.Camara)
                .Where(x => x.TpMandato == TpMandato.Camara || x.TpMandato == TpMandato.Conselheiro)
                .Where(x => x.CdCargo != CdCargo.Suplente)
                .WherePeriodo(periodo)
                .Where(cdCamara != null, x => x.CdCamara == cdCamara)
                .OrderBy(x => x.Cargo.NrOrdem).ThenByDescending(x => x.Pessoa.NoPessoa)
                .ToList()
                .ForEach((x, i) => x.Licencas = af.Where(a => a.IdPessoa == x.IdPessoa).ToList());

            var camaras = db.Query<Camara>()
                .Where(cdCamara != null, x => x.CdCamara == cdCamara)
                .WherePeriodo(periodo)
                .OrderBy(x => x.NoCamara)
                .ToList()
                .ForEach((x, i) => x.Membros = membros.Where(m => m.CdCamara == x.CdCamara).ToList())
                .ForEach((x, i) => AdicionaCargosFaltantes(x.Membros, CdCargo.Coordenador, CdCargo.Adjunto, CdCargo.Representante));

            foreach(var camara in camaras)
            {
                foreach (var membro in camara.Membros.Where(x => x.CdCargo != CdCargo.Titular))
                {
                    membro.CdCamara = camara.CdCamara;
                    membro.Camara = camara;
                    membro.Mensagem = "true"; // Permite editar, caso exista registro 
                    yield return membro;
                }
                foreach (var membro in camara.Membros.Where(x => x.CdCargo == CdCargo.Titular).OrderBy(x => x.Pessoa.NoPessoa))
                {
                    yield return membro;

                    // Caso o membro titular tenha alguma licenca no periodo informado, retorna os suplentes do periodo
                    if (membro.Licencas.Count > 0)
                    {
                        var suplentes = db.Query<Mandato>()
                            .Include(x => x.Pessoa)
                            .Include(x => x.Cargo)
                            .Include(x => x.Camara)
                            .WherePeriodo(periodo)
                            .Where(x => x.IdMandatoTit == membro.IdMandato)
                            .ToList()
                            .ForEach((x, i) => x.Titular = membro)
                            .ForEach((x, i) => x.Licencas = af.Where(a => a.IdPessoa == x.IdPessoa).ToList());

                        foreach (var suplente in suplentes)
                        {
                            yield return suplente;
                        }
                    }
                }
            }
        }

        #region Validações

        /// <summary>
        /// Valida se não existe um mesmo cargo na mesma camara num periodo existente
        /// </summary>
        private void ValidaMesmoCargoPeriodo(int cdCamara, string cdCargo, Periodo periodo)
        {
            var mandato = db.Query<Mandato>()
                .Include(x => x.Cargo)
                .Where(x => x.TpMandato == TpMandato.Camara)
                .Where(x => x.CdCargo == cdCargo)
                .Where(x => x.CdCamara == cdCamara)
                .WherePeriodo(periodo)
                .FirstOrDefault();

            if (mandato != null)
            {
                throw new NBoxException("Já existe um mandato de {0} no período {1} nesta câmara especializada", mandato.Cargo.CdCargo, mandato.GetPeriodo());
            }
        }

        /// <summary>
        /// Verifica se a pessoa já não está na câmara num periodo valido
        /// </summary>
        private void ValidaMandatoRepetido(int idPessoa, int cdCamara, Periodo periodo)
        {
            var mandato = db.Query<Mandato>()
                .Include(x => x.Pessoa)
                .Include(x => x.Cargo)
                .Where(x => x.TpMandato == TpMandato.Camara)
                .Where(x => x.CdCamara == cdCamara)
                .Where(x => x.IdPessoa == idPessoa)
                .WherePeriodo(periodo)
                .FirstOrDefault();

            if (mandato != null)
            {
                throw new NBoxException("{0} já é {1} no período {2} nesta câmara especializada",
                    mandato.Pessoa.NoPessoa,
                    mandato.Cargo.CdCargo,
                    mandato.GetPeriodo());
            }
        }

        #endregion
    }
}