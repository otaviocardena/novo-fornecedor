﻿using NBox.Services;
using NBox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Creasp;
using NPoco;
using Creasp.Core;
using NPoco.Linq;

namespace Creasp.Siplen
{
    public class MandatoService : DataService
    {
        public MandatoService(ServiceContext context) : base(context) { }

        /// <summary>
        /// Exclui um mandato de qualquer tipo (não permite excluir se tiver suplente)
        /// </summary>
        public virtual void ExcluiMandato(int idMandato)
        {
            try
            {
                var sups = db.Query<Mandato>()
                    .Where(x => x.IdMandatoTit == idMandato)
                    .ToList();

                if (sups.Count > 0)
                {
                    throw new NBoxException("Exclua primeiro os suplentes deste mandato");
                }

                db.AuditDelete<Mandato>(idMandato);
            }
            catch (System.Data.SqlClient.SqlException ex)
            {
                throw new NBoxException("Não é possível excluir um membro, pois ele está associado a um evento.");
            }
        }

        /// <summary>
        /// Verifica se a lista de mandatos tem cargos faltantes conforme lista de cargos informada
        /// </summary>
        public void AdicionaCargosFaltantes(List<Mandato> mandatos, params string[] cargos)
        {
            AdicionaCargosFaltantes(mandatos, db.Query<Cargo>().Where(x => cargos.Contains(x.CdCargo)).ToList());
        }

        /// <summary>
        /// Verifica se a lista de mandatos tem cargos faltantes conforme lista de cargos informada
        /// </summary>
        public void AdicionaCargosFaltantes(List<Mandato> mandatos, List<Cargo> cargos)
        {
            foreach (var cargo in cargos)
            {
                if (!mandatos.Exists(x => x.CdCargo == cargo.CdCargo))
                {
                    mandatos.Add(new Mandato { CdCargo = cargo.CdCargo, Cargo = cargo });
                }
            }

            mandatos.Sort(new Comparison<Mandato>((l, r) => l.Cargo.NrOrdem.CompareTo(r.Cargo.NrOrdem)));
        }

        /// <summary>
        /// Conta as faltas dos conselheiros dentro do tipo de mandato dentro do periodo informado (DtCorte - 1 ano)
        /// </summary>
        public IEnumerable<Mandato> ContaFaltas(IEnumerable<Mandato> mandatos, string tpMandato, DateTime dtCorte, bool contaAusencia = false)
        {
            var tudo = db.Query<EventoParticipante>()
                .Include(x => x.Evento)
                .Where(x => x.Evento.FgContaFalta == true)
                .Where(x => x.Evento.DtEvento >= dtCorte.AddYears(-1) && x.Evento.DtEvento <= dtCorte)
                .Where(x => x.TpMandato == tpMandato)
                .ToList();

            var faltasTit = tudo
                .Where(x => x.IdMandatoTit != null)
                .Where(x => x.TpPresencaTit == TpPresenca.Falta)
                .Select(x => x.IdMandatoTit.Value)
                .ToList();

            var justTit = tudo
                .Where(x => x.IdMandatoTit != null)
                .Where(x => x.TpPresencaTit == TpPresenca.Justificado)
                .Select(x => x.IdMandatoTit.Value)
                .ToList();

            var faltasSupl = tudo
                .Where(x => x.IdMandatoSup != null)
                .Where(x => x.TpPresencaSup == TpPresenca.Falta)
                .Select(x => x.IdMandatoSup.Value)
                .ToList();

            var justSupl = tudo
                .Where(x => x.IdMandatoSup != null)
                .Where(x => x.TpPresencaSup == TpPresenca.Justificado)
                .Select(x => x.IdMandatoSup.Value)
                .ToList();

            // contem todos os mandatos que tiveram falta no periodo
            var faltas = new List<int>();
            faltas.AddRange(faltasTit);
            faltas.AddRange(faltasSupl);

            //contem todos os mantados que tiveram justificadas no periodo
            var justificadas = new List<int>();
            justificadas.AddRange(justTit);
            justificadas.AddRange(justSupl);

            foreach (var mandato in mandatos)
            {
                mandato.NrFaltas = faltas.Count(x => mandato.IdMandato == x);
                mandato.NrFaltasJustificadas = justificadas.Count(x => mandato.IdMandato == x);

                yield return mandato;
            }
        }

        /// <summary>
        /// Monta o par Titular < - > Suplente para uma lista de mandatos (usa uma lista como entrada por precisa dos dados em memoria pois executa varias rotinas de where/count)
        /// </summary>
        public IEnumerable<TitularSuplenteDTO> MontaTitularSuplente(List<Mandato> mandatos)
        {
            foreach(var titular in mandatos.Where(x => x.CdCargo != CdCargo.Suplente))
            {
                var suplentes = mandatos.Where(x => x.CdCargo == CdCargo.Suplente && x.IdMandatoTit == titular.IdMandato);

                if (suplentes.Count() == 0)
                {
                    yield return new TitularSuplenteDTO { IdMandatoTit = titular.IdMandato, Titular = titular };
                }
                else
                {
                    foreach (var suplente in suplentes)
                    {
                        yield return new TitularSuplenteDTO
                        {
                            IdMandatoTit = titular.IdMandato,
                            IdMandatoSup = suplente.IdMandato,
                            Titular = titular,
                            Suplente = suplente
                        };
                    }
                }
            }
        }
    }
}