﻿<%@ Page Title="Plenário Futuro" Resource="siplen" Language="C#" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();

    protected override void OnRegister()
    {
        var validsClaims = new List<string> { "ALL", "ALL.USUARIO" };
        if (!validsClaims.Contains(Auth.Current<CreaspUser>().Claim))
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        grdList.RegisterDataSource(() =>
        {
            return db.Indicacoes.ListaPlenarioFuturo(
                    false,
                    cmbMandato.SelectedValue.To<int>(),
                    chkSomenteSemMandato.Checked,
                    ucEntidade.IdEntidade,
                    txtNoPessoa.Text,
                    cmbCamara.SelectedValue.To<int?>());
        });

        RegisterRestorePageState(() => grdList.DataBind());

        RegisterResource("siplen.plenfut").Disabled(btnNotificar, lnkPosse, lnkNovaIndicacao).GridColumns(grdList, 4, 5);
    }

    protected override void OnPrepareForm()
    {
        cmbCamara.Bind(db.Query<Camara>().WherePeriodo(Periodo.Hoje).OrderBy(x => x.NoCamara).ToEnumerable());

        cmbMandato.Items.Clear();

        for(var ano = DateTime.Now.Year - 2; ano <= DateTime.Now.Year + 3; ano++)
        {
            cmbMandato.Items.Add(new ListItem(string.Format("{0} - {1}", ano, ano + 2), ano.ToString()));
        }

        cmbMandato.SelectedValue = DateTime.Now.Year.ToString();

        for(var ano = DateTime.Today.Year; ano > DateTime.Today.Year - 20; ano--)
        {
            cmbMandato.Items.Add(new ListItem(string.Format("{0} - {1}", ano, ano + 2), ano.ToString()));
        }

        SetDefaultButton(btnPesquisar);

        btnPesquisar_Click(null, null);
    }

    protected void btnPesquisar_Click(object sender, EventArgs e)
    {
        grdList.DataBind();
    }

    protected void grdList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        var idIndicacao = e.DataKey<int>(sender);

        if(e.CommandName == "alt")
        {
            Redirect("PlenFut.Indicacao.aspx?IdIndicacao=" + idIndicacao);
        }
        else if(e.CommandName == "del")
        {
            db.Indicacoes.ExcluiIndicacao(idIndicacao);
            ShowInfoBox("Indicação excluida com sucesso");
            grdList.DataBind();
        }
    }

    protected void ucPeriodo_PeriodoChanged(object sender, Periodo e)
    {
        grdList.DataBind();
    }

    protected void btnNotificar_Click(object sender, EventArgs e)
    {
        ucEmail.AbrePopup("PosseConselheiro");
    }

</script>
<asp:Content ContentPlaceHolderID="title" runat="server">
        
</asp:Content>
<asp:Content ContentPlaceHolderID="content" runat="server">

    <div class="mt-30 c-primary-blue">
        <span>Plenário Futuro</span>
        <box:NHyperLink runat="server" ID="lnkNovaIndicacao" ButtonTheme="Success" CssClass="right new ml-10" Icon="plus" Text="Nova indicação" NavigateUrl="PlenFut.Indicacao.aspx" ToolTip="Abrir uma nova vaga com indicação do conselheiro titular e suplente" />
        <box:NButton runat="server" ID="btnPesquisar" Icon="search" CssClass="right ml-10 " Text="Pesquisar" OnClick="btnPesquisar_Click" />
        <box:NHyperLink runat="server" ID="lnkPosse" ButtonTheme="Default" Icon="th-list" CssClass="right ml-10 default-bt" Text="Posse" NavigateUrl="PlenFut.Posse.aspx" ToolTip="Posse dos indicados para conselheiro" />
        <box:NButton runat="server" ID="btnNotificar" Icon="mail" Text="Notificar" CssClass="right" OnClick="btnNotificar_Click" ToolTip="Enviar email de solicitação de documentação para posse de conselheiros" />
    </div>

    <div class="form-div">
        <div class="sub-form-row">
            <div class="div-sub-form form-uc" style="width: 150px">
                <div><span>Mandato</span></div>
                <box:NDropDownList runat="server" ID="cmbMandato" Width="120" Required="true" />
            </div>
            <div class="div-sub-form" style="margin-top: 37px">
                <box:NCheckBox runat="server" ID="chkSomenteSemMandato" Text="Somente indicações sem posse" CssClass="mt-25" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form">
                <div><span>Conselheiro</span></div>
                <box:NTextBox runat="server" ID="txtNoPessoa" TextCapitalize="Upper" MaxLength="80" Width="100%" />
            </div>
            <div class="div-sub-form ml-10">
                <div><span>Câmara</span></div>
                <box:NDropDownList runat="server" ID="cmbCamara" Width="100%" />
            </div>
        </div>
        <div class="sub-form-row">
            <div class="div-sub-form form-uc">
                <div><span>Entidade</span></div>
                <uc:Entidade runat="server" ID="ucEntidade" />
            </div>
        </div>
    </div>

    <box:NGridView runat="server" ID="grdList" AllowGrouping="true" DataKeyNames="IdIndicacao" OnRowCommand="grdList_RowCommand" ItemType="Creasp.Siplen.Indicacao">
        <Columns>
            <asp:BoundField DataField="Camara.NoCamara" />
            <asp:TemplateField HeaderStyle-Width="50" ItemStyle-HorizontalAlign="Center">
                <ItemTemplate><%# Item.AnoIni + " " + Item.AnoFim %></span>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Titular" HeaderStyle-Width="50%">
                <ItemTemplate>
                    <div class="left-right">
                        <div>
                            <%# Item.Titular == null ? "[SEM INDICAÇÃO]" : Item.Titular.NoPessoa %>
                            <small runat="server" visible=<%# Item.Titular != null %>>&nbsp;&nbsp;(<%# Item.Titular?.NrCreasp %>)</small>
                        </div>
                        <box:NLabel runat="server" Theme="Info" Visible=<%# Item.Titular != null %> Text=<%# Item.Titular?.NoTituloAbr %>  ToolTip=<%# Item.Titular?.NoTitulo %> CssClass="no-titulo"  />
                    </div>
                    <div class="left-right space-top">
                        <small><%# Item.Entidade.NoEntidade %></small>
                        <small class="n-label" title="Data de posse"><%# Item.DtPosseTit?.ToString("d") %></small>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Suplente" HeaderStyle-Width="50%">
                <ItemTemplate>
                    <div class="left-right">
                        <div>
                            <%# Item.Suplente == null ? "[SEM INDICAÇÃO]" : Item.Suplente.NoPessoa %>
                            <small runat="server" visible=<%# Item.Suplente != null %>>&nbsp;&nbsp;(<%# Item.Suplente?.NrCreasp %>)</small>
                        </div>
                        <box:NLabel runat="server" Theme="Info" Visible=<%# Item.Suplente != null %> Text=<%# Item.Suplente?.NoTituloAbr %> ToolTip=<%# Item.Suplente?.NoTitulo %> CssClass="no-titulo" />
                    </div>
                    <div class="left-right space-top">
                        <small>&nbsp;</small>
                        <small class="n-label" title="Data de posse"><%# Item.DtPosseSup?.ToString("d") %></small>
                    </div>
                </ItemTemplate>
            </asp:TemplateField>
            <box:NButtonField Icon="pencil" CommandName="alt" Theme="Info" ToolTip="Consultar/Alterar indicação" EnabledExpression="!DtPosseSup" />
            <box:NButtonField Icon="cancel" CommandName="del" Theme="Danger" ToolTip="Excluir indicação" EnabledExpression="!DtPosseSup" OnClientClick="return confirm('Confirma excluir esta indicação?')" />
        </Columns>
        <EmptyDataTemplate>Não existem conselheiros para o filtro acima</EmptyDataTemplate>
    </box:NGridView>

    <uc:Email runat="server" ID="ucEmail" />

    <script src="../Content/Scripts/sidebar.js"></script>
    <script>selectButton("spl-plenFut"); showReload("siplen");</script>

</asp:Content>