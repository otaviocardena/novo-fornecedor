﻿<%@ Page Language="C#" %>
<script runat="server">

    private static Random rnd = new Random(DateTime.Now.Millisecond);
    private string img = "00";
    private CreaspContext db = new CreaspContext();
    private string version;

    protected override void OnRegister()
    {
        RegisterClosePopup("vinc", args =>
        {
            if(args == "OK")
            {
                Redirect("~/Login.aspx");
            }
            else
            {
                ShowErrorBox("Usuário não vinculado");
                Auth.Logout();
                Redirect("~/Login.aspx");
            }
        });

        SetDefaultButton(btnLogin);
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        img = rnd.Next(1, 8).ToString("00");

        if(Request.QueryString["action"] == "logout")
        {
            Auth.Logout();
            Redirect("~/Login.aspx", false, true);
        }

        if(Request.QueryString["ReturnUrl"]?.ToString()?.ToLower() == "/fornecedor/fornecedor.novo.aspx")
        {
            Auth.Login("tempFornecedor", "", false);
            Redirect(Request.QueryString["ReturnUrl"]);
        }

        var nbox = typeof(NBox.Core.Converter).Assembly;
        var creasp = typeof(Creasp.Siplen.Cargo).Assembly;

        version = Factory.Get<ICache>().Get("versions", () =>
        {
            return string.Format("NBox: {0} - {1:dd/MM/yyyy HH:mm}<br/>Creasp: {2} - {3:dd/MM/yyyy HH:mm}",
                nbox.GetName().Version.ToString(4),
                nbox.GetLinkerTime(),
                creasp.GetName().Version.ToString(4),
                creasp.GetLinkerTime());
        });

    }

    protected override void OnPrepareForm()
    {
        txtLogin.Focus();

        SetDefaultButton(btnLogin);
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {
        try
        {
            Auth.Login(txtLogin.Text, txtSenha.Text, chkPersistir.Checked);

            // verifica se o usuario possui vinculo com algum login - se não possui, deve abrir a tela de vinculo
            if (Auth.Current<CreaspUser>().IdPessoa == 0 && !Auth.Current<CreaspUser>().Fornecedor)
            {
                OpenPopup("vinc", "admin/VinculaLogin.aspx", 750);
            }
            else
            {
                Redirect(Request.QueryString["ReturnUrl"] ?? "~/Default.aspx", false, true);
            }
        }
        catch(Exception ex)
        {
            this.ShowErrorBox(ex.Message);
        }
    }

</script>
<html>
    <head runat="server">
        <title>CREA-SP :: Login</title>
        <link href="Content/Styles/login.less" rel="stylesheet" />
    </head>
    <body>
        <form runat="server">
            <div class="login top-bottom">
                <div class="img-wrapper">
                    <img src="/Content/Images/LogoCreasp.png" />
                </div>
                <div class="login-side">
                    <box:NTextBox runat="server" ID="txtLogin" autofocus placeholder="Usuário 490385" Required="true" />
                    <box:NTextBox runat="server" ID="txtSenha" TextMode="Password" Required="true" placeholder="Senha *****" />
                    <box:NCheckBox runat="server" ID="chkPersistir" Text="Manter-se conectado" />
                    <box:NButton runat="server" ID="btnLogin" Text="Entrar" OnClick="btnLogin_Click" Theme="Primary" CssClass="btn-long btn-login" />
                    <box:NLinkButton runat="server" ID="lnkReseteSenha" Text="Esqueceu sua senha?" CssClass="reset-senha"/>
                </div>
            </div>
        </form>
        <div class="busybox"><div class="overlay"><label><i class="icon-clock"></i>Aguarde, processando...</label></div></div>
    </body>
</html>