﻿<%@ WebService Language="C#" Class="SicopWS" %>

using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using Creasp.Core;

[WebService(Namespace = "http://creasp.numeria.com/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
public class SicopWS : Creasp.WebServices.SicopWS
{
}