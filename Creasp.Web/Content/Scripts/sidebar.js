﻿function showSideMenu() {
    var menu = document.querySelector('.sidebar').classList;
    var container = document.querySelector('.container-sidebar').classList;
    if (menu.contains('show-side-menu')) {
        menu.remove('show-side-menu');
        container.remove('show');
    } else {
        menu.add('show-side-menu');
        container.add('show');
    }
}

function selectButton(id) {
    var a = document.querySelectorAll('.sub-ul a');

    a.forEach(element => {
        element.style.color = "#333333";
        element.style.fontWeight = "normal";
    });

    var aSelected = document.getElementById(id).style;
    aSelected.color = "#FABB00";
    aSelected.fontWeight = "bold";
}

function show(item) {
    var li = getItem(item);
    var nerp = getItem('nerp');
    var siplen = getItem('siplen');

    if (li.contains("show")) {
        li.remove("show");
        getUl(item).remove('show');
    } else {
        if (nerp.contains('show')) {
            nerp.remove('show');
            getUl('nerp').remove('show');
        }
        if (siplen.contains('show')) {
            siplen.remove('show');
            getUl('siplen').remove('show');
        }
        li.add("show");
        getUl(item).add('show');
    }
}

function showReload(item) {
    var li = getItem(item);
    var nerp = getItem('nerp');
    var siplen = getItem('siplen');

    if (li.contains("show")) {
        
    } else {
        if (nerp.contains('show')) {
            nerp.remove('show');
            getUl('nerp').remove('show');
        }
        if (siplen.contains('show')) {
            siplen.remove('show');
            getUl('siplen').remove('show');
        }
        li.add("show");
        getUl(item).add('show');
    }
}

function getItem(item) {
    return document.querySelector('.option-' + item + ' .main-li').classList;
}

function getUl(item) {
    return document.querySelector('.option-' + item + ' .sub-ul').classList;
}