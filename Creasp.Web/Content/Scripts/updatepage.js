﻿try {
    //Create class in div
    var x = document.querySelector(".n-grid-footer").parentNode;
    x.setAttribute('id', 'table-wrapper');
}
catch{

}

console.log('Teste');

try {

    //Remove text drop-menu
    var registros = document.querySelector('.n-grid-paging');
    var vet = [];
    let spanSelected = null;

    for (let i = 0; i <= 3; i++) vet.push(registros.getElementsByTagName('a')[i]);

    spanSelected = registros.getElementsByTagName('span')[0];

    for (let i = 0; i < vet.length; i++) {
        if (Number(vet[i].innerText) > Number(spanSelected.innerText)) {
            vet.splice(i, 0, spanSelected);
            break;
        } else if (Number(spanSelected.innerText) > Number(vet[vet.length - 1].innerText)) {
            vet.splice(vet.length, 0, spanSelected);
            break;
        }
    }

    var p = document.createElement('p');
    p.setAttribute('class', 'm-view');
    p.innerText = "VISUALIZAR";

    vet.splice(0, 0, p);

    registros.innerHTML = ""
    for (let i = 0; i <= 5; i++) registros.appendChild(vet[i])

    //Calc count of pages
    let totalReg = Number(document.querySelector('.n-grid-total').innerText.split(':')[1]);
    let pageCurrent = Number(document.querySelector('.n-grid-pager tr td span').innerText);
    var countTotalPages = Math.round(totalReg / Number(spanSelected.innerText));

    document.querySelector('.n-grid-pager td table').setAttribute("style", "display: none");
    let aBack = () => {
        if (pageCurrent <= 1) {
            return `<span><i class="fas fa-chevron-left c-arrow-not-selected"></i></span>`;
        }

        return `<a href="javascript: __doPostBack('ctl00$content$grdList', 'Page$${pageCurrent - 1}')"><i class="fas fa-chevron-left select-true"></i></a>`;
    }

    let aNext = () => {

        if (pageCurrent >= countTotalPages) {
            return `<span><i class="fas fa-chevron-right c-arrow-not-selected"></i></span>`;
        }

        return `<a href="javascript: __doPostBack('ctl00$content$grdList', 'Page$${pageCurrent + 1}')"><i class="fas fa-chevron-right select-true"></i></a>`;
    }


    let divPager = document.createElement('div');
    divPager.setAttribute('class', 'n-paginator');
    divPager.innerHTML = `<span>${pageCurrent} - ${countTotalPages} de ${totalReg}</span>
    ${aBack()}
    ${aNext()}`;
    let tablePaginator = document.querySelector('.n-grid-pager td');
    tablePaginator.appendChild(divPager);

}
catch {

}


try {
    //Add icon arrow down
    var gridTotal = document.querySelector('.n-grid-total');

    var i = document.createElement('i');
    i.setAttribute('class', 'fas fa-sort-amount-down-alt ml-8 c-primary-yellow');
    i.innerHTML = '&nbsp;&nbsp;&nbsp;';

    gridTotal.appendChild(i);
} catch {
}