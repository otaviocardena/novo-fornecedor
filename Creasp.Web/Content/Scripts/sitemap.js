﻿(function ($) {

    // implementa a seleção da treeview principal (utilizado em ajax - pois em full-get já preenche)
    $(document).on('click', '.sitemap a', function () {
        $('.sitemap li.selected').removeClass('selected');
        $(this).parent().addClass('selected');
        window.scrollTo(0, 0);
    });

    // abre ou fecha um grupo do menu
    $(document).on('click', '.sitemap span', function () {
        $(this).parent().toggleClass('open');
    });

    // atualiza quando o usuario clica nos botões de navegação
    window.addEventListener('popstate', function (e) {
        $('.sitemap li.selected').removeClass('selected');
        var href = location.pathname.match(/^[^\.]+/); // lê tudo até o primeiro ponto
        var app = location.pathname.match(/^\/(\w+)\//); // lê só o nome do app ~/<app>/Pages/...

        if (href && app) {
            $('.sitemap a[href*="' + href[0] + '."]')
                .parent().addClass('selected'); // marca como ativo
        }
    });

    $(document).on('focus', '#input-last-focus', function () {
        $('form:first *:input[type!=hidden]:not(:disabled):first').focus();
    }).on('focus', '#input-first-focus', function () {
        $('form:first *:input[type!=hidden]:not(:disabled):last').focus();
    });

    $(document).on('change', '.ano-base select', function () {
        var nrAnoBase = $(this).val();
        Cookies.set('ano', nrAnoBase);
        window.top.location = '/Default.aspx';
    });

})(jQuery);

function GridFilter(input) {
    var $rows = $(input).parents('table').find('tbody > tr');
    var val = '^(?=.*\\b' + $.trim(input.value).split(/\s+/).join('\\b)(?=.*\\b') + ').*$';
    var reg = RegExp(val, 'i');
    var text;

    $rows.show().filter(function () {
        text = $(this).text().replace(/\s+/g, ' ');
        return !reg.test(text);
    }).hide();

}