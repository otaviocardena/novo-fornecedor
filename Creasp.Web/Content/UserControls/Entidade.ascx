﻿<%@ Control Language="C#" ClassName="EntidadeUC" EnableViewState="true" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();

    public event EventHandler EntidadeChanged;

    public int? IdEntidade { get { return txtEntidade.Value.To<int?>(); } }

    public bool Enabled { get { return txtEntidade.Enabled; } set { txtEntidade.Enabled = btnEC.Enabled = btnIES.Enabled = btnPesquisar.Enabled = value; } }
    public bool Required { get { return txtEntidade.Required; } set { txtEntidade.Required = value; } }

    public static string tpEntidadeAtual { get; set; }

    public void Page_Init(object sender, EventArgs e)
    {
        txtEntidade.RegisterDataSource(c =>
        {
            return db.Query<Entidade>()
            .Where(x => x.TpEntidade == tpEntidadeAtual)
            .Where(x => x.FgAtivo == true && x.FgRepresentante == true)
            .Where(x => x.NoEntidade.Contains(c))
            .OrderBy(x => x.CdRepasse).ThenBy(x => x.NoEntidade)
            .ToList()
            .Select(x => new { x.IdEntidade, NoEntidade = x.CdRepasse == null ? x.NoEntidade : string.Format("{0} - {1}", x.CdRepasse, x.NoEntidade) });
        });
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            tpEntidadeAtual = TpEntidade.EntidadeClasse;
            SelecionaTipoEntidade(TpEntidade.EntidadeClasse);
        }
    }

    public void LoadEntidade(int idEntidade)
    {
        var entidade = db.SingleById<Entidade>(idEntidade);
        SelecionaTipoEntidade(entidade.TpEntidade);
        txtEntidade.Value = entidade.IdEntidade.ToString();
        txtEntidade.Text = entidade.NoEntidade;
    }

    protected void btnTipo_Click(object sender, CommandEventArgs e)
    {
        SelecionaTipoEntidade(e.CommandArgument.ToString());

        txtEntidade.Text = string.Empty;
        txtEntidade.Value = string.Empty;

        if (EntidadeChanged != null)
        {
            EntidadeChanged(this, EventArgs.Empty);
        }
    }

    private void SelecionaTipoEntidade(string tpEntidade)
    {
        tpEntidadeAtual = tpEntidade;
        btnIES.Theme = tpEntidade == TpEntidade.InstituicaoEnsino ? ThemeStyle.Info : ThemeStyle.Default;
        btnEC.Theme = tpEntidade == TpEntidade.EntidadeClasse ? ThemeStyle.Info : ThemeStyle.Default;
    }

    protected void cmbEntidade_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (EntidadeChanged != null)
        {
            EntidadeChanged(this, EventArgs.Empty);
        }
    }

    protected void btnPesquisar_Click(object sender, EventArgs e)
    {
        pnlPesquisarIeEc.Open();
        pnlPesquisarIeEc.Title = tpEntidadeAtual == TpEntidade.InstituicaoEnsino ? "Pesquisar Instituição de Ensino" : "Pesquisar Entidade de Classe";

        BindGridLista(String.Empty);
    }

    protected void grdLista_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int rowIndex = e.CommandArgument.To<int>();
        txtEntidade.Value = e.DataKey<string>(sender, "IdEntidade").ToString();
        txtEntidade.Text = e.DataKey<string>(sender, "NoEntidade").ToString();

        pnlPesquisarIeEc.Close();
    }

    private void BindGridLista(string noEntidade)
    {
        grdLista.DataSource = db.Query<Entidade>()
            .Where(x => x.TpEntidade == tpEntidadeAtual)
            .Where(x => x.FgAtivo == true && x.FgRepresentante == true)
            .Where(x => String.IsNullOrEmpty(noEntidade) || x.NoEntidade.ToUpper().Contains(noEntidade.ToUpper()))
            .OrderBy(x => x.CdRepasse).ThenBy(x => x.NoEntidade)
            .ToList()
            .Select(x => new { x.IdEntidade, NoEntidade = x.CdRepasse == null ? x.NoEntidade : string.Format("{0} - {1}", x.CdRepasse, x.NoEntidade) });
        grdLista.DataBind();
    }

    protected void btnFiltroLista_Click(object sender, EventArgs e)
    {
        BindGridLista(txtFiltroLista.Text);
    }

    protected void txtEntidade_TextChanged(object sender, EventArgs e)
    {
        if (EntidadeChanged != null)
        {
            EntidadeChanged(this, EventArgs.Empty);
        }
    }
</script>
<div class="input-group" id="<%= ClientID %>">
    <box:NButton runat="server" Theme="Default" Icon="graduation-cap" ToolTip="Instituições de ensino" ID="btnIES" OnCommand="btnTipo_Click" CommandArgument="I" CausesValidation="false" />
    <box:NButton runat="server" Theme="Info" Icon="industrial-building" ToolTip="Entidades de classe" ID="btnEC" OnCommand="btnTipo_Click" CommandArgument="E" CausesValidation="false" />
    <box:NAutocomplete runat="server" ID="txtEntidade" Width="90%" DataTextField="NoEntidade" DataValueField="IdEntidade" OnTextChanged="txtEntidade_TextChanged" AutoPostBack="true" Template="{Text}" />
    <box:NButton runat="server" ID="btnPesquisar" Icon="search" Theme="Info" OnClick="btnPesquisar_Click" ToolTip="Pesquisar IE ou EC" CausesValidation="false" />
</div>

<box:NPopup runat="server" ID="pnlPesquisarIeEc" Title="Pesquisar" Width="900px" Height="400px">
    <box:NTab runat="server" ID="tabIdent" Text="Identificação" Selected="true">

        <div class="form-div">
            <div class="sub-form-row">
                <div class="div-sub-form form-uc">
                    <div><span>Identificação</span></div>
                    <div class="input-group">
                        <box:NTextBox runat="server" ID="txtFiltroLista" Width="94%" OnEnter="btnFiltroLista_Click" />
                        <box:NButton runat="server" style="" ID="btnFiltroLista" Icon="search" Theme="Info" OnClick="btnFiltroLista_Click" ToolTip="Filtrar"  />
                    </div>
                   
                </div>
            </div>
        </div>

        <box:NGridView runat="server" ID="grdLista" DataKeyNames="IdEntidade, NoEntidade" OnRowCommand="grdLista_RowCommand">
            <Columns>
                <asp:BoundField DataField="NoEntidade" HeaderText="Nome" />
                <box:NButtonField Icon="ok" CommandName="sel" Theme="Info" ToolTip="Selecionar" />
            </Columns>
        </box:NGridView>

    </box:NTab>
</box:NPopup>
