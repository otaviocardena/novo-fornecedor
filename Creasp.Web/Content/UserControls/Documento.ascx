﻿<%@ Control Language="C#" ClassName="DocumentoUC" %>
<script runat="server">

    private CreaspContext db = new CreaspContext();

    public event EventHandler DocumentoChanged;

    public int? IdPessoa { get { return ViewState["IdPessoa"].To<int?>(); } set { ViewState["IdPessoa"] = value; } }
    public int IdRef { get { return ViewState["IdRef"].To<int>(); } set { ViewState["IdRef"] = value; } }
    public string TpRef { get { return ViewState["TpRef"].To<string>(); } set { ViewState["TpRef"] = value; } }
    public bool CanEdit { get { return ViewState["CanEdit"].To<bool>(); } set { ViewState["CanEdit"] = value; } }

    public string Titulo { get { return capTitulo.InnerText; } set { capTitulo.InnerText = value; } }

    protected override void OnRegister()
    {
        Page.RegisterClosePopup((sender, args) => MontaTela());

        grdDocs.RegisterDataSource(() =>
        {
            var docs = db.Documentos.ListaDocumentos(TpRef, IdRef);
            //trDocs.Visible = docs.Count > 0;
            return docs;
        });
    }

    public void MontaTela(string tpRef, int idRef, int? idPessoa, bool canEdit = true)
    {
        TpRef = tpRef;
        IdRef = idRef;
        IdPessoa = idPessoa;
        CanEdit = canEdit;

        MontaTela();
    }

    private void MontaTela()
    {
        var tpRef = TpRef;

        // quando a documentação for de indicação, utilizada a lista de tipos de Conselheiros (pois quando dá posse migra os documentos)
        if (TpRef == TpRefDoc.Indicacao)
        {
            tpRef = TpRefDoc.Conselheiro;
        }

        trUpload.Visible = CanEdit;

        cmbTipoDoc.Bind(db.Query<TipoDocumento>()
            .Where(x => x.TpRef == tpRef)
            .OrderBy(x => x.NrOrdem)
            .ToList().Select(x => new { x.CdTipoDoc, x.NoTipoDoc }), false, true);

        grdDocs.DataBind();
    }

    protected void uplDocs_FileUpload(object sender, EventArgs e)
    {
        var cnt = 0;

        if (cmbTipoDoc.HasValue() == false) throw new NBoxException("Nenhum tipo de documento selecionado");

        if(uplDocs.PostedFiles.Count > 0)
        {
            foreach(var file in uplDocs.PostedFiles)
            {
                if(string.IsNullOrEmpty(file.FileName)) continue;

                db.Documentos.EnviaDocumento(TpRef, IdRef, IdPessoa, cmbTipoDoc.SelectedValue.To<int>(), file);

                cnt++;
            }

            grdDocs.DataBind();

            Page.ShowInfoBox("Foram enviados " + cnt + " arquivos com sucesso");

            if (DocumentoChanged != null) DocumentoChanged(this, EventArgs.Empty);
        }
    }

    protected void btnDownload_Command(object sender, CommandEventArgs e)
    {
        db.Documentos.Download(Page, e.CommandArgument.To<int>());
    }

    protected void btnTipoDoc_Click(object sender, EventArgs e)
    {
        // Apenas no caso de indicação, utiliza o tipo Conselheiro
        var tpRef = TpRef == TpRefDoc.Indicacao ? TpRefDoc.Conselheiro : TpRef;

        Page.OpenPopup("tp", "~/Admin/TipoDocumento.aspx?TpRef=" + tpRef, 750);
    }

    protected void grdDocs_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "del")
        {
            db.Documentos.ExcluiDocumento(e.DataKey<int>(sender));

            Page.ShowInfoBox("Arquivo excluido com sucesso");

            grdDocs.DataBind();

            if (DocumentoChanged != null) DocumentoChanged(this, EventArgs.Empty);
        }
        else if (e.CommandName == "download")
        {
            db.Documentos.Download(Page, e.DataKey<int>(sender));
        }
    }

    protected void grdDocs_DataBound(object sender, EventArgs e)
    {
        grdDocs.MergeRows(0);

        if(CanEdit == false)
        {
            grdDocs.Columns[3].Visible = false;
        }
    }

</script>

<div class="mt-30 c-primary-blue">
    <span>Documentação</span>
</div>

<div class="form-div">
    <div class="sub-form-row margin-top-0" runat="server" id="capTitulo">
        <div class="div-sub-form" runat="server" id="trUpload">
            <div><span>Enviar documento</span></div>
            <div class="left-right">
                <box:NDropDownList runat="server" ID="cmbTipoDoc" Width="100%" />
                <box:NFileUpload runat="server" Text="Anexar" Icon="upload" ButtonTheme="Primary" CssClass="space-left" Width="120" ID="uplDocs" AutoPostBack="true" AllowMultiple="true" OnFileUpload="uplDocs_FileUpload" />
                <box:NButton runat="server" ID="btnTipoDoc" Width="54" CssClass="space-left" Theme="Default" Icon="cog" OnClick="btnTipoDoc_Click" ToolTip="Editar tipos de documento" CausesValidation="false" />
            </div>
        </div>
    </div>
</div>

<box:NGridView runat="server" ID="grdDocs" DataKeyNames="IdDocumento" ShowHeader="false" CssClass="no-margin-top" ShowTotal="false" OnRowCommand="grdDocs_RowCommand" ShowZebra="false" ShowRowHover="false" OnDataBound="grdDocs_DataBound">
    <Columns>
        <asp:BoundField DataField="Tipo.NoTipoDoc" HeaderStyle-Width="150" ItemStyle-CssClass="nowrap-ellipsis" />
        <asp:BoundField DataField="NoArquivo" ItemStyle-CssClass="nowrap-ellipsis" />
        <box:NButtonField Icon="download" Theme="Info" CommandName="download" />
        <box:NButtonField Icon="cancel" Theme="Danger" CommandName="del" OnClientClick="return confirm('Confirma excluir este arquivo?\n\nATENÇÃO: Esta operação não poderá ser desfeita');" />
    </Columns>
    <EmptyDataTemplate>
        Não existe nenhum documento anexado
    </EmptyDataTemplate>
</box:NGridView>

