﻿<%@ Control Language="C#" ClassName="PessoaUC" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();

    public event EventHandler PessoaChanged;

    public int? IdPessoa { get { return txtPessoa.Value.To<int?>(); } }

    public string NoPessoa { get { return txtPessoa.Text; } }

    public bool EditOnly { get { return ViewState["EditOnly"].To<bool>(false); } set { ViewState["EditOnly"] = value; } }

    public bool InsertOnly { get { return ViewState["InsertOnly"].To<bool>(false); } set { ViewState["InsertOnly"] = value; } }

    public bool WithoutEdit { get { return ViewState["WithoutEdit"].To<bool>(false); } set { ViewState["WithoutEdit"] = value; } }

    public bool Enabled { get { return txtPessoa.Enabled; } set { txtPessoa.Enabled = value; } }

    public bool Required { get { return txtPessoa.Required; } set { txtPessoa.Required = value; } }

    public string ValidationGroup { get { return txtPessoa.ValidationGroup; } set { txtPessoa.ValidationGroup = value; } }

    // retorna o objeto pessoa associado
    public Pessoa Pessoa => IdPessoa == null ? null : db.SingleById<Pessoa>(IdPessoa);

    // id da pessoa que está sendo editada
    private int? IdPessoaEdit { get { return ViewState["IdPessoaEdit"].To<int?>(); } set { ViewState["IdPessoaEdit"] = value; } }

    // Indica que a pessoa deve ser um profissional (NrCreasp != null)
    public bool IsProfissional { get; set; }

    // Indica que a pessoa é funcionario (NrMatricula != null)
    public bool IsFuncionario { get; set; }

    // Indica que a pessoa é usuario (NoLogin != null)
    public bool IsUsuario { get; set; }

    // Retorna o nome conforme todas as opções da tela (label, textbox ou combo)
    private string NomePessoa
    {
        get
        {
            var nome = txtNoPessoa.Visible ? txtNoPessoa.Text : lblNoPessoa.Text;
            if (cmbNoPessoa.Visible) nome = cmbNoPessoa.SelectedValue;
            return nome;
        }
    }

    public void Page_Init(object sender, EventArgs e)
    {
        txtPessoa.RegisterDataSource((q) =>
        {
            var isNum = Regex.IsMatch(q, @"^\d+$");

            return db.Query<Pessoa>()
                .Where(isNum && IsProfissional, x => x.NrCreasp.StartsWith(q)) // Se numero e profissional, busca pelo creasp
                .Where(isNum && !IsProfissional, x => x.NrCpf.StartsWith(q)) // Se numero e não profissional, busca pelo CPF
                .Where(!isNum, x => x.NoPessoa.Contains(q.ToLike())) // Se string, busca pelo nome
                .Where(IsProfissional, x => x.NrCreasp != null)
                .Where(IsFuncionario, x => x.NrMatricula != null)
                .Where(IsUsuario, x => x.NoLogin != null)
                .OrderBy(x => x.NoPessoa)
                .ToEnumerable()
                .Take(10)
                .Select(x => new { x.IdPessoa, x.NoPessoa, x.NoTituloAbr, x.NrCreasp, x.NrCpfFmt, x.NrMatricula });
        });

        Page.RegisterResource("siplen.pessoa").Disabled(btnSalvar, frmMain, frmEnd, frmBanco);
    }

    public void Page_Load(object sender, EventArgs e)
    {
        if (IsProfissional)
        {
            txtPessoa.Template = "{NoPessoa}<span class='small'>({NoTituloAbr})</span><span class='right'>{NrCreasp}</span>";
        }
        else if (IsFuncionario)
        {
            txtPessoa.Template = "{NoPessoa}<span class='right'>{NrMatricula}</span>";
        }
        else
        {
            txtPessoa.Template = "{NoPessoa}<span class='right'>{NrCpfFmt}</span>";
        }
    }

    public void LoadPessoa(int? idPessoa)
    {
        if (idPessoa == null)
        {
            txtPessoa.Text = txtPessoa.Value = "";
        }
        else
        {
            var p = db.Pessoas.BuscaPessoa(idPessoa.Value, false);
            txtPessoa.Text = p.NoPessoa;
            txtPessoa.Value = p.IdPessoa.ToString();
        }
    }

    public void LoadPessoaByCpf(string cpf)
    {
        if (cpf == null)
        {
            txtPessoa.Text = txtPessoa.Value = "";
        }
        else
        {
            LoadPessoa(db.Query<Pessoa>().SingleOrDefault(x => x.NrCpf == cpf)?.IdPessoa);
        }
    }

    protected void txtPessoa_TextChanged(object sender, EventArgs e)
    {
        LoadPessoa(txtPessoa.Value.To<int?>());
        if (PessoaChanged != null) PessoaChanged(this, EventArgs.Empty);

        // ajusta o foco do controle
        if (txtPessoa.Text.Length > 0) btnClear.Focus(); else btnAdd.Focus();
    }

    public void Page_PreRender(object sender, EventArgs e)
    {
        if (Enabled)
        {
            btnAdd.Visible = txtPessoa.Value.Length == 0;
            btnEdit.Visible = btnClear.Visible = txtPessoa.Value.Length > 0;
        }
        else
        {
            btnAdd.Visible = btnEdit.Visible = btnClear.Visible = false;
        }

        if (EditOnly)
        {
            btnAdd.Visible = btnClear.Visible = txtPessoa.Enabled = false;
            btnEdit.Visible = IdPessoa.HasValue;
        }
        if (InsertOnly)
        {
            // não renderiza nada, serve apenas para cadastrar (via popup de cadadstro) uma nova pessoa
            txtPessoa.Visible = btnClear.Visible = btnAdd.Visible = btnEdit.Visible = false;
        }
        if (WithoutEdit)
        {
            btnEdit.Visible = false;
        }
    }

    /// <summary>
    /// Botão + para abrir tela de adicionar nova pessoa
    /// </summary>
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        radCPF.Checked = true;
        radTipoPessoa_CheckedChanged(null, null);
        pnlAdd.Open(btnContinuar, txtNumeroAdd);
    }

    /// <summary>
    /// Abre a tela de editar uma pessoa.
    /// </summary>
    public void EditPessoa(int idPessoa)
    {
        LoadPessoa(idPessoa);
        btnEdit_Click(null, null);
    }

    /// <summary>
    /// Abre a tela de cadastro de nova pessoa
    /// </summary>
    public void InsertPessoa()
    {
        LoadPessoa(null);
        btnAdd_Click(null, null);
    }

    protected void btnEdit_Click(object sender, EventArgs e)
    {
        IdPessoaEdit = IdPessoa.Value;

        var p = db.Pessoas.BuscaPessoa(IdPessoaEdit.Value, true);

        MontaTela(p);

        pnlEdit.Open(btnSalvar);
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        txtPessoa.Value = "";
        txtPessoa.Focus();
        if (PessoaChanged != null) PessoaChanged(this, EventArgs.Empty);
    }

    protected void radTipoPessoa_CheckedChanged(object sender, EventArgs e)
    {
        txtNumeroAdd.Text = "";
        txtNumeroAdd.Focus();

        if (radCPF.Checked)
        {
            txtNumeroAdd.MaskType = MaskType.Custom;
            txtNumeroAdd.MaskFormat = "999.999.999-99";
        }
        else if (radCNPJ.Checked)
        {
            txtNumeroAdd.MaskType = MaskType.Custom;
            txtNumeroAdd.MaskFormat = "99.999.999/9999-99";
        }
        else if (radCreasp.Checked)
        {
            txtNumeroAdd.MaskType = MaskType.Custom;
            txtNumeroAdd.MaskFormat = "R9999999999";
        }
        else if (radMatricula.Checked)
        {
            txtNumeroAdd.MaskType = MaskType.Integer;
            txtNumeroAdd.MaxLength = 6;
        }
    }

    protected void txtNrCep_TextChanged(object sender, EventArgs e)
    {
        if (txtNrCep.Text.Length > 0)
        {
            var cep = db.Ceps.BuscaCep(txtNrCep.Text.UnMask());

            if (cep == null)
            {
                txtNrCep.Text = "";
                lblCidade.Text = "";
                Page.ShowErrorBox(txtNrCep, "CEP não encontrado");
            }
            else
            {
                lblCidade.Text = cep.NoCidade + " / " + cep.CdUf;
                txtNoLogradouro.Text = cep.NoLogradouro;
            }
        }
        else
        {
            lblCidade.Text = "";
        }
    }

    protected void lnkCancelar_Click(object sender, EventArgs e)
    {
        pnlAdd.Close();
        pnlEdit.Close();
    }

    protected void btnContinuar_Click(object sender, EventArgs e)
    {
        txtNumeroAdd.Validate(radCPF.Checked).Cpf();
        txtNumeroAdd.Validate(radCNPJ.Checked).Cnpj();

        var p = db.Pessoas.BuscaPessoaExterna(
            radCPF.Checked || radCNPJ.Checked ? txtNumeroAdd.Text.UnMask() : null,
            radCreasp.Checked ? txtNumeroAdd.Text.UnMask() : null,
            radMatricula.Checked ? txtNumeroAdd.Text.To<int?>() : null);

        IdPessoaEdit = p.IdPessoa == 0 ? null : (int?)p.IdPessoa;

        MontaTela(p);

        pnlAdd.Close();
        pnlEdit.Open(btnSalvar);
    }

    private void MontaTela(Pessoa p)
    {
        pnlEdit.Controls.ClearControls();

        lblNoPessoa.Text = txtNoPessoa.Text = p.NoPessoa;
        cmbNoPessoa.BindMultiColumn(db.Pessoas.ListaNomesDisponiveis(p.NrCpf).Select(x => new { id = x.Key, n = x.Key, s = x.Value }), false);

        try
        {
            cmbNoPessoa.SelectedValue = p.NoPessoa;
        }
        catch (Exception)
        {
        }

        lblCreasp.Text = p.NrCreasp;
        lblCpf.Text = p.NrCpfFmt;
        lblTpSexo.Text = p.TpSexo;
        lblNrMatricula.Text = string.Format("{0}", p.NrMatricula);
        lblNoTitulo.Text = p.NoTitulo;
        lblNoEmail.Text = p.NoEmail;
        txtNoHistorico.Text = p.NoHistorico;
        txtCdTokenRedmine.Text = p.CdTokenRedmine;
        trTokenRedmine.Visible = p.NoLogin != null; // token visivel apenas para usuários

        txtNrCep.SetText(p.NrCep);
        lblCidade.Text = p.Cep == null ? "" : $"{p.Cep.NoCidade} / {p.Cep.CdUf}";
        txtNoLogradouro.Text = p.NoLogradouro;
        txtNrEndereco.Text = p.NrEndereco;
        txtNoComplemento.Text = p.NoComplemento;
        txtNoBairro.Text = p.NoBairro;
        lblNoEndCorresp.Text = p.NoEndCorresp;

        BuscaDadosBancarios();

        txtNoPessoa.Visible = string.IsNullOrEmpty(p.NoPessoa);
        txtNoPessoa.Required = txtNoPessoa.Visible;
        lblNoPessoa.Visible = !string.IsNullOrEmpty(p.NoPessoa) && cmbNoPessoa.Items.Count == 1;
        cmbNoPessoa.Visible = divHelp.Visible = cmbNoPessoa.Items.Count > 1;

        btnSalvar.Text = p.IdPessoa == 0 ? "Incluir" : "Salvar";

        chkAltBanco.Checked = false;
        chkAltBanco_CheckedChanged(null, null);

        Page.SetFocus(txtNoPessoa, txtNoHistorico);
    }

    protected void btnSalvar_Click(object sender, EventArgs e)
    {
        txtNoLogradouro.Validate(txtNrCep.Text.Length > 0).Required();
        txtNrAgencia.Validate(txtNrBanco.Text.Length > 0).Required();
        txtNrConta.Validate(txtNrBanco.Text.Length > 0).Required();

        var p = IdPessoaEdit == null ? new Pessoa() : db.Pessoas.BuscaPessoa(IdPessoaEdit.Value, false);

        p.NrCpf = lblCpf.Text.UnMask();
        p.NrCreasp = lblCreasp.Text;
        p.NrMatricula = lblNrMatricula.Text.To<int?>();
        p.TpSexo = lblTpSexo.Text.To<string>();
        p.NoPessoa = this.NomePessoa;
        p.NoEmail = lblNoEmail.Text.To<string>();
        p.NrCep = txtNrCep.Text.UnMask();
        p.NoLogradouro = txtNoLogradouro.Text;
        p.NrEndereco = txtNrEndereco.Text;
        p.NoComplemento = txtNoComplemento.Text;
        p.NoBairro = txtNoBairro.Text;
        p.NoHistorico = txtNoHistorico.Text;
        p.CdTokenRedmine = txtCdTokenRedmine.Text;

        if (txtNrBanco.Text.Length > 0 && chkAltBanco.Checked)
        {
            p.DadosBancarios = new DadosBancarios
            {
                NrBanco = txtNrBanco.Text,
                NoBanco = lblNoBanco.Text,
                NrAgencia = txtNrAgencia.Text,
                NrAgenciaDv = txtNrAgenciaDv.Text,
                NrConta = txtNrConta.Text,
                NrContaDv = txtNrContaDv.Text,
                FgContaCorrente = chkContaCorrente.Checked
            };
        }

        var sb = new StringBuilder();

        db.Pessoas.Salva(p.IdPessoa == 0, p, sb);

        if (sb.Length > 0)
        {
            Page.ShowInfoBox(sb.ToString());
        }

        pnlEdit.Close();
        txtPessoa.Value = p.IdPessoa.ToString();
        txtPessoa.Text = p.NoPessoa;

        if (PessoaChanged != null) PessoaChanged(this, EventArgs.Empty);
    }

    protected void txtNrBanco_TextChanged(object sender, EventArgs e)
    {
        lblNoBanco.Text = "[A definir pelo SISCONT]";
    }

    protected void chkAltBanco_CheckedChanged(object sender, EventArgs e)
    {
        txtNrBanco.Enabled = txtNrAgencia.Enabled = txtNrAgenciaDv.Enabled =
            txtNrConta.Enabled = txtNrContaDv.Enabled = chkContaCorrente.Enabled = chkAltBanco.Checked;
    }

    private void BuscaDadosBancarios()
    {
        var siscont = Factory.Get<ISiscont>();
        var p = siscont.BuscaPessoas(lblCpf.Text, this.NomePessoa).FirstOrDefault();

        if (p == null || p.DadosBancarios == null)
        {
            LimpaDadosBancarios();
        }
        else
        {
            txtNrBanco.Text = p.DadosBancarios.NrBanco.To<string>();
            lblNoBanco.Text = p.DadosBancarios.NoBanco;
            txtNrAgencia.Text = p.DadosBancarios.NrAgencia;
            txtNrAgenciaDv.Text = p.DadosBancarios.NrAgenciaDv;
            txtNrConta.Text = p.DadosBancarios.NrConta;
            txtNrContaDv.Text = p.DadosBancarios.NrContaDv;
            chkContaCorrente.Checked = p.DadosBancarios.FgContaCorrente;
        }
    }

    private void LimpaDadosBancarios()
    {
        txtNrBanco.Text = "";
        lblNoBanco.Text = "";
        txtNrAgencia.Text = "";
        txtNrAgenciaDv.Text = "";
        txtNrConta.Text = "";
        txtNrContaDv.Text = "";
        chkContaCorrente.Checked = false;
    }

    protected void cmbNoPessoa_SelectedIndexChanged(object sender, EventArgs e)
    {
        this.BuscaDadosBancarios();
    }

</script>
<div class="input-group" id="<%= ClientID %>">
    <box:NAutocomplete runat="server" ID="txtPessoa" Width="100%" AutoPostBack="true" TextCapitalize="Upper"
        DataValueField="IdPessoa" DataTextField="NoPessoa" OnTextChanged="txtPessoa_TextChanged" />
    <box:NButton runat="server" ID="btnClear" Icon="cancel" Theme="Default" OnClick="btnClear_Click" ToolTip="Limpar" CausesValidation="false" />
    <box:NButton runat="server" ID="btnAdd" Icon="search" Theme="Info" OnClick="btnAdd_Click" ToolTip="Pesquisar ou incluir nova pessoa" CausesValidation="false" CssClass="btn-radius" />
    <box:NButton runat="server" ID="btnEdit" Icon="pencil" Theme="Info" OnClick="btnEdit_Click" ToolTip="Editar dados" CausesValidation="false" />
</div>
<style>
    .autocomplete-suggestion .right {
        font-size: 90%;
        color: gray;
        float: right;
    }

    .autocomplete-suggestion .small {
        font-size: 80%;
        color: gray;
        position: relative;
        top: -1px;
        left: 5px;
    }
</style>
<box:NPopup runat="server" ID="pnlAdd" Title="Pesquisar ou incluir nova pessoa">

    <box:NTab runat="server" ID="tabIdent" Text="Identificação" Selected="true">
        <div class="form-div">
            <div class="sub-form">
                <div><span>Tipo de Identificação</span></div>
                <div>
                    <box:NRadioButton runat="server" ID="radCPF" GroupName="tp" Text="<u>C</u>PF" Checked="true" AutoPostBack="true" OnCheckedChanged="radTipoPessoa_CheckedChanged" AccessKey="C" />
                    <box:NRadioButton runat="server" ID="radCNPJ" GroupName="tp" Text="C<u>N</u>PJ" AutoPostBack="true" OnCheckedChanged="radTipoPessoa_CheckedChanged" AccessKey="N" />
                    <box:NRadioButton runat="server" ID="radCreasp" GroupName="tp" Text="CRE<u>A</u>SP" AutoPostBack="true" OnCheckedChanged="radTipoPessoa_CheckedChanged" AccessKey="A" />
                    <box:NRadioButton runat="server" ID="radMatricula" GroupName="tp" Text="<u>M</u>atricula" AutoPostBack="true" OnCheckedChanged="radTipoPessoa_CheckedChanged" AccessKey="M" />
                </div>
            </div>
            <div class="sub-form-row">
                <div class="div-sub-form">
                    <div><span>Número</span></div>
                    <box:NTextBox runat="server" ID="txtNumeroAdd" MaskType="Integer" Width="180" Required="true" ValidationGroup="addp" />
                </div>
            </div>
        </div>
        <box:NButton runat="server" ID="btnContinuar" Text="Continuar" Theme="Default" OnClick="btnContinuar_Click" ValidationGroup="addp" CssClass="btn-popup" />
        <div class="help">Para incluir novas pessoas é necessário informar algum dos códigos de identificação. O sistema irá consultar nos sistemas externos CREANET, SISCONT e SENIOR RH para preencher todos os dados da pessoa.</div>
    </box:NTab>

</box:NPopup>
<box:NPopup runat="server" ID="pnlEdit" Title="Confirmar dados" Width="900" Height="480">

    <box:NTab runat="server" ID="tabMain" Text="Dados pessoais" Selected="true">

        <div class="form-div">
            <div class="sub-form" style="margin-bottom: 15px">
                <div><span>Nome</span></div>
                <box:NTextBox runat="server" ID="txtNoPessoa" MaxLength="100" TextCapitalize="Upper" />
                <box:NLabel runat="server" ID="lblNoPessoa" CssClass="field" />
                <box:NDropDownList runat="server" ID="cmbNoPessoa" Width="84%" Visible="false" AutoPostBack="true" OnSelectedIndexChanged="cmbNoPessoa_SelectedIndexChanged" />
                <div runat="server" id="divHelp" class="help">
                    Ao clicar em <u>Salvar</u> o nome desta pessoa será atualizado pelo nome selecionado acima.
                </div>
            </div>

            <div class="sub-form-row">
                <div class="div-sub-form">
                    <div><span>CPF/CNPJ</span></div>
                    <box:NLabel runat="server" ID="lblCpf" CssClass="field field-small" />
                </div>

                <div class="div-sub-form">
                    <div><span>CREASP</span></div>
                    <box:NLabel runat="server" ID="lblCreasp" CssClass="field field-small" />
                </div>

                <div class="div-sub-form">
                    <div><span>Matricula</span></div>
                    <box:NLabel runat="server" ID="lblNrMatricula" CssClass="field field-small" />
                    <box:NLabel runat="server" ID="lblTpSexo" Visible="false" />
                </div>
            </div>

            <div class="sub-form" id="frmMain" runat="server">
                <div><span>Título(s)</span></div>
                <box:NLabel runat="server" ID="lblNoTitulo" CssClass="field" />
            </div>

            <div class="sub-form">
                <div><span>Email</span></div>
                <box:NLabel runat="server" ID="lblNoEmail" CssClass="field" />
            </div>

            <div class="sub-form" id="trTokenRedmine" runat="server">
                <div><span>Token</span></div>
                <box:NTextBox runat="server" ID="txtCdTokenRedmine" MaxLength="40" Width="320" TextCapitalize="Lower" />
                <small>Apenas para usuários da ferramenta de Helpdesk</small>
            </div>

            <div class="sub-form">
                <div><span>Observações</span></div>
                <box:NTextBox runat="server" ID="txtNoHistorico" MaxLength="2000" Width="84%" TextMode="MultiLine" TextCapitalize="Upper" Rows="4" />
            </div>
        </div>

        <box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnSalvar" Text="Incluir" Icon="ok" Theme="Primary" OnClick="btnSalvar_Click" CausesValidation="false" />
        </box:NToolBar>
    </box:NTab>

    <box:NTab runat="server" ID="tabEnd" Text="Endereços">
        <div class="form-div" id="frmEnd" runat="server">
            <div class="sub-form">
                <div><span>CEP</span></div>
                <div style="display: flex">
                    <box:NTextBox runat="server" ID="txtNrCep" MaskType="Custom" MaskFormat="99.999-999" AutoPostBack="true" OnTextChanged="txtNrCep_TextChanged" Width="100" />
                    <box:NLabel runat="server" ID="lblCidade" CssClass="align-self"/>
                </div>
            </div>

            <div class="sub-form">
                <div><span>Logradouro</span></div>
                <div>
                    <box:NTextBox runat="server" ID="txtNoLogradouro" Width="100%" MaxLength="80" TextCapitalize="Upper" /></div>
            </div>

            <div class="sub-form">
                <div><span>Número</span></div>
                <div>
                    <box:NTextBox runat="server" ID="txtNrEndereco" Width="100%" MaxLength="10" TextCapitalize="Upper" /></div>
            </div>

            <div class="sub-form">
                <div><span>Complemento</span></div>
                <div>
                    <box:NTextBox runat="server" ID="txtNoComplemento" Width="100%" MaxLength="40" TextCapitalize="Upper" /></div>
            </div>

            <div class="sub-form">
                <div><span>Bairro</span></div>
                <div>
                    <box:NTextBox runat="server" ID="txtNoBairro" Width="100%" MaxLength="100" TextCapitalize="Upper" /></div>
            </div>

            <div class="sub-form">
                <div><span>Endereço de Correspondência</span></div>
                <div>
                    <box:NLabel runat="server" ID="lblNoEndCorresp" Width="100%" MaxLength="100" CssClass="field pre" /></div>
            </div>
        </div>

        <%--<table class="form">
            <caption>Endereço de correspondência</caption>
            <tr>
                <td>Endereço</td>
                <td>
                    <box:NLabel runat="server" ID="lblNoEndCorresp" Width="100%" CssClass="field pre" /></td>
            </tr>
        </table>--%>

        <box:NToolBar runat="server">
            <box:NButton runat="server" Text="Salvar" Icon="ok" Theme="Primary" OnClick="btnSalvar_Click" CausesValidation="false" />
        </box:NToolBar>

    </box:NTab>

    <box:NTab runat="server" ID="tabBanco" Text="Dados bancários">

        <div class="form-div" id="frmBanco" runat="server">
            <div class="sub-form" style="margin-bottom:15px">
                <box:NCheckBox runat="server" ID="chkAltBanco" Text="Alterar dados bancários no SISCONT" AutoPostBack="true" OnCheckedChanged="chkAltBanco_CheckedChanged" />
            </div>

            <div class="sub-form-row">
                <div class="div-sub-form">
                    <div><span>Banco</span></div>
                    <box:NTextBox runat="server" ID="txtNrBanco" MaxLength="5" Width="170" AutoPostBack="true" OnTextChanged="txtNrBanco_TextChanged" />&nbsp;&nbsp;&nbsp;
                    <box:NLabel runat="server" ID="lblNoBanco" />
                </div>

                <div class="div-sub-form">
                    <div><span>Agência</span></div>
                    <box:NTextBox runat="server" ID="txtNrAgencia" TextCapitalize="Upper" MaxLength="10" Width="120" />
                    -
                    <box:NTextBox runat="server" ID="txtNrAgenciaDv" TextCapitalize="Upper" MaxLength="2" Width="50" Style="text-align: center" />
                </div>

                <div class="div-sub-form">
                    <div><span>Conta</span></div>
                    <box:NTextBox runat="server" ID="txtNrConta" TextCapitalize="Upper" MaxLength="10" Width="120" />
                    -
                    <box:NTextBox runat="server" ID="txtNrContaDv" TextCapitalize="Upper" MaxLength="2" Width="50" Style="text-align: center" />
                </div>
                <div class="div-sub-form align-self">
                    <box:NCheckBox runat="server" ID="chkContaCorrente" Text="Conta corrente" Checked="true" CssClass="margin-left" />
                </div>
            </div>
        </div>

        <box:NToolBar runat="server">
            <box:NButton runat="server" Text="Salvar" Icon="ok" Theme="Primary" OnClick="btnSalvar_Click" CausesValidation="false" />
        </box:NToolBar>

        <div class="help">
            Os dados de conta bancárias são armazenados somente no sistema SISCONT. O SICOP não armazenam dados de conta bancária.
        </div>
    </box:NTab>

</box:NPopup>
