﻿<%@ Control Language="C#" ClassName="CidadeUC" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();

    public event EventHandler CidadeChanged;

    public int? CdCidade { get { return txtCidade.Value.To<int?>(); } }

    public string NoCidade { get { return txtCidade.Text; } }

    public bool EditOnly { get { return ViewState["EditOnly"].To<bool>(false); } set { ViewState["EditOnly"] = value; } }

    public bool InsertOnly { get { return ViewState["InsertOnly"].To<bool>(false); } set { ViewState["InsertOnly"] = value; } }

    public bool Enabled { get { return txtCidade.Enabled; } set { txtCidade.Enabled = value; } }

    public bool Required { get { return txtCidade.Required; } set { txtCidade.Required = value; } }

    public string ValidationGroup { get { return txtCidade.ValidationGroup; } set { txtCidade.ValidationGroup = value; } }

    // retorna um objeto cep associado à cidade
    public Cep Cep => CdCidade == null ? null : db.FirstOrDefault<Cep>(c => c.CdCidade == CdCidade);

    private string NomeCidade
    {
        get
        {
            return txtCidade.Text;
        }
    }

    public void Page_Init(object sender, EventArgs e)
    {
        txtCidade.RegisterDataSource((q) =>
        {
            var isNum = Regex.IsMatch(q, @"^\d+$");

            return db.Query<Cep>()
                .Where(isNum, x => x.NrCep.StartsWith(q)) // Se for numero, busca pelo cep
                .Where(!isNum, x => x.NoCidade.Contains(q.Trim().ToLike())) // Se string, busca pelo nome
                .Where(c => c.CdCidade != 0)
                .ToList()
                .GroupBy(c => c.CdCidade)
                .Select(c => c.First())
                .OrderBy(x => x.NoCidade)
                .Take(10)
                .Select(x => new { x.NoCidade, x.NoBairro, x.NoLogradouro, x.NrCep, x.NrCepFmt, x.CdCidade, x.CdUf });
        });
    }

    public void Page_Load(object sender, EventArgs e)
    {
        txtCidade.Template = "{NoCidade}<span class=\"right\">({CdCidade}) - {CdUf}</span>";
    }

    public void LoadCidade(string nrCep)
    {
        if (string.IsNullOrEmpty(nrCep))
        {
            txtCidade.Text = txtCidade.Value = "";
        }
        else
        {
            var c = db.Ceps.BuscaCep(nrCep);
            txtCidade.Text = c.NoCidade;
            txtCidade.Value = c.CdCidade.ToString();
        }
    }

    public void LoadCidadeByCodigo(int? cdCodigo)
    {
        if (cdCodigo == null)
        {
            txtCidade.Text = txtCidade.Value = "";
        }
        else
        {
            LoadCidade(db.Query<Cep>().FirstOrDefault(x => x.CdCidade == cdCodigo)?.NrCep);
        }
    }

    protected void txtCidade_TextChanged(object sender, EventArgs e)
    {
        LoadCidadeByCodigo(txtCidade.Value.To<int?>());
        if (CidadeChanged != null) CidadeChanged(this, EventArgs.Empty);
    }

</script>
<div class="input-group" id="<%= ClientID %>">
    <box:NAutocomplete runat="server" ID="txtCidade" Width="100%" AutoPostBack="true" TextCapitalize="Upper"
        DataValueField="CdCidade" DataTextField="NoCidade" OnTextChanged="txtCidade_TextChanged" />
</div>
<style>
    .autocomplete-suggestion .right {
        font-size: 90%;
        color: gray;
        float: right;
    }

    .autocomplete-suggestion .small {
        font-size: 80%;
        color: gray;
        position: relative;
        top: -1px;
        left: 5px;
    }
</style>
