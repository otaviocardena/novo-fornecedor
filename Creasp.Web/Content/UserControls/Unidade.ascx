﻿<%@ Control Language="C#" ClassName="UnidadeUC" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();

    public event EventHandler UnidadeChanged;

    public int? CdUnidade { get { return txtUnidade.Value.To<int?>(); } }

    public bool Enabled { get { return txtUnidade.Enabled; } set { txtUnidade.Enabled = value; } }
    public bool Required { get { return txtUnidade.Required; } set { txtUnidade.Required = value; } }

    protected override void OnRegister()
    {
        txtUnidade.RegisterDataSource((q) => db.Unidades.ListaUnidades(null, q));
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        txtUnidade.AutoPostBack = UnidadeChanged != null;
    }

    public void LoadUnidade(int? cdUnidade)
    {
        if(cdUnidade == null)
        {
            txtUnidade.Text = txtUnidade.Value = "";
        }
        else
        {
            var unid = db.SingleById<Unidade>(cdUnidade);
            txtUnidade.Text = unid.NoUnidade;
            txtUnidade.Value = cdUnidade.ToString();
        }
    }

    protected void txtUnidade_TextChanged(object sender, EventArgs e)
    {
        if(UnidadeChanged != null)
        {
            UnidadeChanged(this, EventArgs.Empty);
        }
    }

</script>
<box:NAutocomplete runat="server" ID="txtUnidade" CssClass="unitInput" TextCapitalize="Upper" Width="90"
    Template="{NoUnidade} ({NrCentroCustoFmt})"
    DataValueField="CdUnidade" DataTextField="NoUnidade" OnTextChanged="txtUnidade_TextChanged"/>
