﻿<%@ Control Language="C#" ClassName="CentroCustoUC" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();

    public event EventHandler CentroCustoChanged;

    public int? IdCentroCusto { get { return txtCentroCusto.Value.To<int?>(); } }

    public bool Enabled { get { return txtCentroCusto.Enabled; } set { txtCentroCusto.Enabled = value; } }
    public bool Required { get { return txtCentroCusto.Required; } set { txtCentroCusto.Required = value; } }
    public string ValidationGroup { get { return txtCentroCusto.ValidationGroup; } set { txtCentroCusto.ValidationGroup = value; } }
    public string TipoCentroCusto { get; set; } = TpCentroCusto.SubArea;

    public CentroCusto CentroCusto => db.SingleById<CentroCusto>(this.IdCentroCusto);

    protected override void OnRegister()
    {
        txtCentroCusto.RegisterDataSource((q) => db.CentroCustos.ListaCentroCusto(q, TipoCentroCusto));
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        txtCentroCusto.AutoPostBack = CentroCustoChanged != null;
    }

    public void LoadCentroCusto(int? idCentroCusto)
    {
        if(idCentroCusto == null)
        {
            txtCentroCusto.Text = txtCentroCusto.Value = "";
        }
        else
        {
            var cc = db.SingleById<CentroCusto>(idCentroCusto);
            txtCentroCusto.Text = cc.NoCentroCustoFmt;
            txtCentroCusto.Value = idCentroCusto.ToString();
        }
    }

    public void LoadCentroCustoByNrCentroCusto(string nrCentroCusto)
    {
        if(nrCentroCusto == null)
        {
            txtCentroCusto.Text = txtCentroCusto.Value = "";
        }
        else
        {
            var cc = db.Query<CentroCusto>()
                .Where(x => x.NrAno == db.NrAnoBase)
                .Where(x => x.FgAtivo)
                .Where(x => x.NrCentroCusto == nrCentroCusto)
                .OrderByDescending(x => x.IdCentroCusto)
                .FirstOrDefault();

            txtCentroCusto.Text = cc.NoCentroCustoFmt;
            txtCentroCusto.Value = cc.IdCentroCusto.ToString();
        }
    }

    protected void txtCentroCusto_TextChanged(object sender, EventArgs e)
    {
        if(CentroCustoChanged != null)
        {
            CentroCustoChanged(this, EventArgs.Empty);
        }
    }

</script>
<box:NAutocomplete runat="server" ID="txtCentroCusto" TextCapitalize="Upper" Width="100%"
    Template="<small style='display:inline-block;width:80px'>{NrCentroCustoFmt}</small>{NoCentroCusto}"
    DataValueField="IdCentroCusto" DataTextField="NoCentroCustoFmt" OnTextChanged="txtCentroCusto_TextChanged" />
<div class="help">Para pesquisar por código, utilize apenas números iniciando por zero. Ex: 01020301</div>