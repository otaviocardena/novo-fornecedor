﻿<%@ Control Language="C#" %>
<script runat="server">

    public event EventHandler<ReportFormat> Click;

    public string ReportName { get { return ViewState["ReportName"] as string; } set { ViewState["ReportName"] = value; } }

    public void Open(string reportName)
    {
        optPDF.Checked = true;
        pnlPopup.Open(btnExibir, btnExibir);
        ReportName = reportName;
    }

    protected void btnFechar_Click(object sender, EventArgs e)
    {
        pnlPopup.Close();
    }

    protected void btnExibir_Click(object sender, EventArgs e)
    {
        pnlPopup.Close();

        if(Click != null)
        {
            Click(this, optPDF.Checked ? ReportFormat.Pdf :
                optDOC.Checked ? ReportFormat.Word : ReportFormat.Excel);
        }
    }

</script>
<box:NPopup runat="server" ID="pnlPopup" Visible="false" Title="Imprimir relatório" Width="350px">
    <div class="mt-20 c-primary-blue">
        <span>Formatos disponíveis</span>
    </div>

    <div class="form-div no-margin-top">
        <div class="sub-form-row mt-20">
            <div class="div-sub-form">
                <box:NRadioButton runat="server" ID="optPDF" Text="PDF" GroupName="exp" />
                <box:NRadioButton runat="server" ID="optDOC" Text="Word" GroupName="exp" />
                <box:NRadioButton runat="server" ID="optXLS" Text="Excel" GroupName="exp" />
            </div>
        </div>
        <div class="sub-form-row no-margin-top">
            <div class="div-sub-form">
                <box:NButton runat="server" ID="btnExibir" Text="Exibir"  OnClick="btnExibir_Click" Theme="Primary" />
                <box:NButton runat="server" Text="Cancelar" Theme="default" OnClick="btnFechar_Click" CssClass="margin-left bt-cancel" />
            </div>
        </div>
    </div>
</box:NPopup>