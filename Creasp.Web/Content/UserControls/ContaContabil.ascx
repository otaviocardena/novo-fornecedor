﻿<%@ Control Language="C#" ClassName="ContaContabilUC" %>
<script runat="server">

    protected CreaspContext db = new CreaspContext();

    public event EventHandler ContaContabilChanged;

    public int? IdContaContabil { get { return txtContaContabil.Value.To<int?>(); } }

    public bool Enabled { get { return txtContaContabil.Enabled; } set { txtContaContabil.Enabled = value; } }
    public bool FgAnalitico { get; set; } = true;
    public bool Required { get { return txtContaContabil.Required; } set { txtContaContabil.Required = value; } }
    public string ValidationGroup { get { return txtContaContabil.ValidationGroup; } set { txtContaContabil.ValidationGroup = value; } }

    public ContaContabil ContaContabil { get { return db.SingleById<ContaContabil>(IdContaContabil); } }

    // Indica que deve ser filtrado apenas para as subcontas: Ex: "61" somente as conta tipo 6.1
    public string NrContaFiltro { get; set; } = null;

    protected override void OnRegister()
    {
        txtContaContabil.RegisterDataSource((q) => db.Query<ContaContabil>()
            .Where(x => x.NrAno == db.NrAnoBase)
            .Where(x => x.FgAnalitico == FgAnalitico && x.FgAtivo)
            .Where(x => x.NoContaContabil.StartsWith(q.ToLike()) || x.NrContaContabil.StartsWith(q.Replace(".", "")))
            .Where(NrContaFiltro != null, x => x.NrContaContabil.StartsWith(NrContaFiltro))
            .Limit(10)
            .ToList());
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        txtContaContabil.AutoPostBack = ContaContabilChanged != null;
    }

    public void LoadContaContabil(int? idContaContabil)
    {
        if(idContaContabil == null)
        {
            txtContaContabil.Text = txtContaContabil.Value = "";
        }
        else
        {
            var cc = db.SingleById<ContaContabil>(idContaContabil);
            txtContaContabil.Text = cc.NoContaContabilFmt;
            txtContaContabil.Value = idContaContabil.ToString();
        }
    }

    public void ClearContaContabil()
    {
        txtContaContabil.Value = "";
        txtContaContabil.Text = "";
    }

    protected void txtContaContabil_TextChanged(object sender, EventArgs e)
    {
        if(ContaContabilChanged != null)
        {
            ContaContabilChanged(this, EventArgs.Empty);
        }
    }

</script>
<box:NAutocomplete runat="server" ID="txtContaContabil" TextCapitalize="Upper" Width="100%"
    Template="<small style='display:inline-block;width:160px'>{NrContaContabilFmt}</small>{NoContaContabil}"
    DataValueField="IdContaContabil" DataTextField="NoContaContabilFmt" OnTextChanged="txtContaContabil_TextChanged" />
<div class="help">Para pesquisar por código, utilize apenas números. Ex: 62111010101006</div>