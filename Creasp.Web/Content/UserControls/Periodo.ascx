﻿<%@ Control Language="C#" ClassName="PeriodoUC" %>
<script runat="server">

    public event EventHandler PeriodoChanged;

    // Indica que deve usar o modo de Textbox
    public bool ShowTextBox { get; set; }

    public bool Required { get { return txtDtIni.Required; } set { txtDtIni.Required = txtDtFim.Required = value; } }

    public bool Enabled { get { return txtDtIni.Enabled; } set { drpPeriodo.Enabled = txtDtIni.Enabled = txtDtFim.Enabled = value; } }

    protected override void OnPrepareForm()
    {
        var atual = (DateTime.Today.Year - 2) + " - " + DateTime.Today.Year;
        drpPeriodo.Items.Insert(4, new NDropDownItem { Key = atual, Text = atual });
    }

    public Periodo Periodo
    {
        get
        {
            return new Periodo(txtDtIni.Text.To(Periodo.MinDate), txtDtFim.Text.To(Periodo.MaxDate));
        }
        set
        {
            txtDtIni.SetText(value.Inicio == Periodo.MinDate ? null : (object)value.Inicio);
            txtDtFim.SetText(value.Fim == Periodo.MaxDate ? null : (object)value.Fim);
            drpPeriodo.Text = value.ToString(false, true);
            //drpPeriodo.ToolTip = value.ToString(false, false);
        }
    }

    protected void drpPeriodo_ItemClick(object sender, NDropDownItem e)
    {
        if (e.Key == "Custom")
        {
            txtCustomDtIni.Text = "";
            txtCustomDtFim.Text = "";
            pnlPeriodo.Open(btnDefinir, txtCustomDtIni);
        }
        else
        {
            Periodo = new Periodo(e.Key);

            if (PeriodoChanged != null) PeriodoChanged(this, EventArgs.Empty);
        }
    }

    protected void btnDefinir_Click(object sender, EventArgs e)
    {
        if (!txtCustomDtFim.HasValue()) txtCustomDtFim.Text = txtCustomDtIni.Text;

        txtCustomDtIni.Validate().LessThanOrEqual(txtCustomDtFim.Text.To<DateTime>());

        Periodo = new Periodo(txtCustomDtIni.Text.To<DateTime>(), txtCustomDtFim.Text.To<DateTime>());

        if (PeriodoChanged != null) PeriodoChanged(this, EventArgs.Empty);

        pnlPeriodo.Close();
    }

    protected void pnlPeriodo_ClosePopup(object sender, EventArgs e)
    {
        if (PeriodoChanged != null) PeriodoChanged(this, EventArgs.Empty);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        if(ShowTextBox)
        {
            drpPeriodo.Visible = false;
        }
        else
        {
            txtDtIni.Visible = txtDtFim.Visible = ltrA.Visible = false;
        }
    }

</script>
<box:NDropDown runat="server" ID="drpPeriodo" Icon="calendar" Text="Sem período" AutoPostBack="true" OnItemClick="drpPeriodo_ItemClick">
    <box:NDropDownItem Key="Hoje" Text="Hoje" />
    <box:NDropDownItem Key="EsteMes" Text="Este mês" />
    <box:NDropDownItem Key="MesPassado" Text="Mês passado" />
    <box:NDropDownItem Key="EsteAno" Text="Este ano" />
    <box:NDropDownItem Key="Vazio" Text="Sem período" />
    <box:NDropDownItem Key="Custom" Text="<i>Período customizado</i>" />
</box:NDropDown>
<box:NTextBox runat="server" ID="txtDtIni" MaskType="Date" /><asp:Literal runat="server" ID="ltrA" Text="&nbsp;a&nbsp;&nbsp;" /><box:NTextBox runat="server" ID="txtDtFim" MaskType="Date" />

<box:NPopup runat="server" ID="pnlPeriodo" Title="Definir período" Width="500" OnClosePopup="pnlPeriodo_ClosePopup">

    <div class="mt-30 c-primary-blue" style="width: 96%;">
        <span>Intervalo de datas</span>
        <box:NButton runat="server" ID="btnDefinir" Text="Definir" Icon="calendar" Theme="Info" OnClick="btnDefinir_Click" ValidationGroup="per" CssClass="right"/>
    </div>

    <div class="form-div">
        <div class="sub-form-row form-uc-radius">
            <div class="div-sub-form">
                <div><span style="padding-top: 10px;">Data de início</span></div>
                <box:NTextBox runat="server" ID="txtCustomDtIni" MaskType="Date" Required="true" ValidationGroup="per" CssClass="w-85" />
            </div>
            <div class="div-sub-form">
                <div><span>Data de fim</span></div>
                <box:NTextBox runat="server" ID="txtCustomDtFim" MaskType="Date" CssClass="w-85 mar-left-8" />
            </div>
        </div>
    </div>

    <div class="help">Informe a data de início e final do período. Se a data de fim ficar em branco o periodo será de apenas 1 dia (data de inicio)</div>

</box:NPopup>