﻿<%@ Control Language="C#" ClassName="EmailUC" %>
<script runat="server">

    public CreaspContext db = new CreaspContext();

    public string TpEmail { get { return ViewState["TpEmail"].To<string>(); } set { ViewState["TpEmail"] = value; } }

    public void AbrePopup(string tpEmail)
    {
        TpEmail = tpEmail;

        var ds = GetSource();

        chkDest.Bind(ds.GetDestinatarios()
            .Select(x =>
            {
                var l = new ListItem(x.NoPessoa, x.IdPessoa.ToString(), x.NoEmail != null);
                l.Attributes["title"] = x.NoEmail;
                return l;
            }),
            false);

        var tmpl = db.SingleById<EmailTemplate>(ds.CdTemplate);
        drpSalvar.Items.Clear();
        drpSalvar.Items.Add(new NDropDownItem { Text = "Salvar modelo", Key = ds.CdTemplate });
        txtAssunto.Text = tmpl.NoTitulo;
        txtTemplate.Text = tmpl.NoMensagem;
        txtNoEmailResposta.Text = tmpl.NoEmailResposta;
        lblCampos.Text = string.Join(", ", ds.ListaCampos);
        btnSel_Command(null, new CommandEventArgs("true", null));

        pnlEmail.Open(btnEnviar, txtAssunto);
    }

    private IEmailDataSource GetSource()
    {
        if(TpEmail == "Convocacao")
        {
            var idEvento = Request.QueryString["IdEvento"].To<int>();
            return new EmailConvocacaoDataSource(idEvento);
        }
        else if(TpEmail == "PosseInspetor")
        {
            return new EmailPosseInspetorDataSource();
        }
        else if(TpEmail == "PosseConselheiro")
        {
            return new EmailPosseConselheiroDataSource();
        }

        throw new NBoxException("Tipo de email não encontrado");
    }

    protected void btnEnviar_Click(object sender, EventArgs e)
    {
        txtAssunto.Validate().MinLength(10);
        txtTemplate.Validate().MinLength(10);
        txtNoEmailResposta.Validate(txtNoEmailResposta.HasValue()).Email();

        // separa apenas os selecionados
        var ids = chkDest.Items.Cast<ListItem>()
            .Where(x => x.Selected)
            .Select(x => x.Value.To<int>())
            .ToList();

        if (ids.Count == 0) throw new NBoxException("Nenhum destinatário selecionado");

        var db = new CreaspContext();

        db.Emails.GeraEmails(GetSource(),
            ids,
            txtAssunto.Text,
            txtTemplate.Text,
            txtNoEmailResposta.Text.To<string>());

        Page.ShowInfoBox("Emails enviados com sucesso");
        pnlEmail.Close();
    }

    protected void btnSel_Command(object sender, CommandEventArgs e)
    {
        foreach(var item in chkDest.Items.Cast<ListItem>())
        {
            if(item.Enabled) item.Selected = e.CommandName.To<bool>();
        }
    }

    protected void tabPreview_TabChanged(object sender, EventArgs e)
    {
        var idPessoa = chkDest.Items.Cast<ListItem>().Where(x => x.Selected).Select(x => x.Value).FirstOrDefault().To<int?>();
        if (idPessoa == null) throw new NBoxException("Nenhum destinatário selecionado");

        var ds = GetSource();
        var dados = ds.GetDados(idPessoa.Value);

        try
        {
            dvPreview.InnerText = Nustache.Core.Render.StringToString(txtTemplate.Text, dados, new Nustache.Core.RenderContextBehaviour { HtmlEncoder = (s) => s });
        }
        catch(Exception ex)
        {
            dvPreview.InnerText = ex.Message;
        }

    }

    protected void drpSalvar_ItemClick(object sender, NDropDownItem e)
    {
        txtAssunto.Validate().Required();
        txtTemplate.Validate().Required();
        txtNoEmailResposta.Validate().Required();

        db.Emails.AtualizaTemplate(e.Key, txtAssunto.Text, txtTemplate.Text, txtNoEmailResposta.Text);
        Page.ShowInfoBox("Modelo atualizado com sucesso");
    }

</script>
<box:NPopup runat="server" ID="pnlEmail" Title="Notificação por email" Width="1200">

    <box:NTab runat="server" ID="tabTmpl" Text="Modelo" Selected="true">

        <box:NToolBar runat="server">
            <box:NButton runat="server" ID="btnEnviar" Icon="mail" Text="Enviar emails" Theme="Primary" OnClick="btnEnviar_Click" />
            <box:NDropDown runat="server" CssClass="right" ID="drpSalvar" Icon="cog" AutoPostBack="true" OnItemClick="drpSalvar_ItemClick" />
        </box:NToolBar>

        <div class="left-right">
            <div style="width:100%; align-self: flex-start">
                <div class="mt-30 c-primary-blue">
                    <span>Email</span>
                </div>

                <div class="form-div">
                    <div class="sub-form-row margin-top-0">
                        <div class="div-sub-form">
                            <div><span>Assunto</span></div>
                            <box:NTextBox runat="server" ID="txtAssunto" Width="100%" Required="true" MaxLength="100" />
                        </div>
                    </div>
                    <div class="sub-form-row margin-top-0">
                        <div class="div-sub-form">
                            <div><span>Mensagem</span></div>
                            <box:NTextBox runat="server" ID="txtTemplate" TextMode="MultiLine" Rows="18" Required="true" Width="100%" CssClass="mono text-email" />
                        </div>
                    </div>
                    <div class="sub-form-row margin-top-0">
                        <div class="div-sub-form">
                            <div><span>Responder para</span></div>
                            <box:NTextBox runat="server" ID="txtNoEmailResposta" Width="100%" placeholder="responder@creasp.org.br" />
                        </div>
                    </div>
                    <div class="sub-form-row margin-top-0">
                        <div class="div-sub-form">
                            <div><span>Campos</span></div>
                            <box:NLabel runat="server" ID="lblCampos" CssClass="field" />
                        </div>
                    </div>
                </div>
            </div>
            <div style="width: 400px; align-self: flex-start; max-height: 607px; overflow-y: auto;" class="margin-left nowrap-ellipsis">
                <div class="form-div">
                    <div class="sub-form-row margin-top-0">
                        <div class="div-sub-form">
                            <div><span>Destinatários</span></div>
                            <box:NLinkButton runat="server" Text="Selecionar" Icon="ok" CommandName="true" OnCommand="btnSel_Command" />&nbsp;&nbsp;
                            <box:NLinkButton runat="server" CssClass="cl-grey" Text="Desmarcar" Icon="cancel" CommandName="false" OnCommand="btnSel_Command" />
                        </div>
                    </div>
                    <div class="sub-form-row margin-top-0">
                        <div class="div-sub-form">
                            <box:NCheckBoxList runat="server" ID="chkDest" />
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </box:NTab>

    <box:NTab runat="server" ID="tabPreview" Text="Visualizar" AutoPostBack="true" OnTabChanged="tabPreview_TabChanged">
        <div class="field mono" id="dvPreview" runat="server" style="background-color: white; border: 1px solid silver; padding: 20px; margin-top: 20px; white-space: pre-wrap;"></div>
    </box:NTab>

    <box:NTab runat="server" Text="Ajuda">
        <div class="field mono mt-20">
            <p>O sistema de envio de emails utiliza um processador de modelos chamado Mustache, que processa e transforma seus modelos em documentos finais.</p>
            <ul>
                <li>Para substituição direta, utilize: {{NomeCampo}}</li>
                <li>Para bloco de texto condicional, utilize: {{#CampoCondicional}}Este texto só será exibido se a condição do campo for verdadeira{{/CampoCondicional}}</li>
                <li>Para listas, utilize: {{#Lista}}Item da lista {{.}}{{/Lista}}</li>
            </ul>
            <p>Ver mais em:</p>
            <p><a target="_blank" href="https://mustache.github.io/mustache.5.html">Manual do Mustache</a></p>
        </div>
    </box:NTab>

</box:NPopup>